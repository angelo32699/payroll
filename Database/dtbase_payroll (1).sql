-- phpMyAdmin SQL Dump
-- version 5.2.1
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: May 16, 2024 at 05:03 AM
-- Server version: 10.4.32-MariaDB
-- PHP Version: 8.2.12

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `dtbase_payroll`
--

-- --------------------------------------------------------

--
-- Table structure for table `add_overtime`
--

CREATE TABLE `add_overtime` (
  `ao_id` int(10) NOT NULL,
  `ot_description` varchar(100) NOT NULL,
  `emp_no` int(10) NOT NULL,
  `code` varchar(100) NOT NULL,
  `hours_of_ot` decimal(65,2) NOT NULL,
  `overtime_pay` decimal(65,2) NOT NULL,
  `date_to` varchar(100) NOT NULL,
  `date_from` varchar(100) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_general_ci;

-- --------------------------------------------------------

--
-- Table structure for table `annual_tax_inc`
--

CREATE TABLE `annual_tax_inc` (
  `pay_trans_id` int(100) NOT NULL,
  `annual_tax_inc` decimal(65,2) NOT NULL,
  `pay_details_wtax` decimal(65,2) NOT NULL,
  `max_ctr` int(100) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_general_ci;

-- --------------------------------------------------------

--
-- Table structure for table `audit_logs`
--

CREATE TABLE `audit_logs` (
  `logs_id` int(11) NOT NULL,
  `user_id` int(11) NOT NULL,
  `tran_date` varchar(100) NOT NULL,
  `transaction` varchar(1000) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_general_ci;

--
-- Dumping data for table `audit_logs`
--

INSERT INTO `audit_logs` (`logs_id`, `user_id`, `tran_date`, `transaction`) VALUES
(28, 1, '5/14/2024 10:08 am', 'Successfully Login.'),
(29, 2, '5/14/2024 10:11 am', 'Successfully Login.'),
(30, 1, '5/14/2024 10:27 am', 'Successfully Login.'),
(31, 2, '5/14/2024 10:39 am', 'Successfully Login.'),
(32, 2, '5/14/2024 01:43 pm', 'Successfully Login.'),
(33, 2, '5/14/2024 01:44 pm', 'Successfully Login.'),
(34, 1, '5/14/2024 01:46 pm', 'Successfully Login.'),
(35, 2, '5/14/2024 01:55 pm', 'Successfully Login.'),
(36, 1, '5/14/2024 01:55 pm', 'Successfully Login.'),
(37, 2, '5/14/2024 08:00 pm', 'Successfully Login.'),
(38, 2, '5/14/2024', 'Successfully Logout.'),
(39, 1, '5/14/2024 08:01 pm', 'Successfully Login.'),
(40, 1, '5/14/2024', 'Successfully Logout.'),
(41, 1, '5/14/2024 08:03 pm', 'Successfully Login.'),
(42, 1, '5/14/2024 08:03 pm', 'Successfully Logout.'),
(43, 1, '5/14/2024 08:03 pm', 'Successfully Login.'),
(44, 1, '5/14/2024 08:04 pm', 'Successfully Logout.'),
(45, 1, '5/14/2024 08:05 pm', 'Successfully Login.'),
(46, 1, '5/14/2024 08:05 pm', 'Successfully Logout.'),
(47, 1, '5/14/2024 08:05 pm', 'Successfully Login.'),
(48, 1, '5/14/2024 08:06 pm', 'Successfully Logout.'),
(49, 1, '5/14/2024 08:06 pm', 'Successfully Login.'),
(50, 1, '5/14/2024 08:06 pm', 'Successfully Logout.'),
(51, 1, '5/14/2024 08:07 pm', 'Successfully Login.'),
(52, 1, '5/14/2024 08:07 pm', 'Successfully Logout.'),
(53, 1, '5/14/2024 08:07 pm', 'Successfully Login.'),
(54, 1, '5/14/2024 08:08 pm', 'Successfully Logout.'),
(55, 1, '5/14/2024 08:08 pm', 'Successfully Login.'),
(56, 1, '5/14/2024 08:09 pm', 'Successfully Logout.'),
(57, 2, '5/14/2024 08:14 pm', 'Successfully Login.'),
(58, 2, '5/14/2024 08:16 pm', 'Successfully Logout.'),
(59, 2, '5/14/2024 08:17 pm', 'Successfully Login.'),
(60, 2, '5/14/2024 08:19 pm', 'Successfully Logout.'),
(61, 2, '5/14/2024 08:19 pm', 'Successfully Login.'),
(62, 2, '5/14/2024 08:20 pm', 'Successfully Logout.'),
(63, 1, '5/14/2024 08:20 pm', 'Successfully Login.'),
(64, 1, '5/14/2024 08:20 pm', 'Successfully Logout.'),
(65, 2, '5/14/2024 08:21 pm', 'Successfully Login.'),
(66, 2, '5/14/2024 08:21 pm', 'Successfully Logout.'),
(67, 2, '5/14/2024 08:21 pm', 'Successfully Login.'),
(68, 2, '5/14/2024 08:21 pm', 'Successfully Logout.'),
(69, 2, '5/14/2024 08:22 pm', 'Successfully Login.'),
(70, 2, '5/14/2024 08:23 pm', 'Successfully Logout.'),
(71, 2, '5/14/2024 08:27 pm', 'Successfully Login.'),
(72, 2, '5/14/2024 08:27 pm', 'Successfully Logout.'),
(73, 2, '5/14/2024 08:34 pm', 'Successfully Login.'),
(74, 2, '5/14/2024 08:34 pm', 'Successfully Logout.'),
(75, 2, '5/14/2024 08:36 pm', 'Successfully Login.'),
(76, 2, '5/14/2024 08:36 pm', 'Successfully Logout.'),
(77, 2, '5/14/2024 08:37 pm', 'Successfully Login.'),
(78, 2, '5/14/2024 08:37 pm', 'Successfully Logout.'),
(79, 2, '5/14/2024 08:43 pm', 'Successfully Login.'),
(80, 2, '5/14/2024 08:44 pm', 'Successfully Logout.'),
(81, 2, '5/14/2024 08:47 pm', 'Successfully Login.'),
(82, 2, '5/14/2024 08:48 pm', 'Successfully Logout.'),
(83, 2, '5/14/2024 08:49 pm', 'Successfully Login.'),
(84, 2, '5/14/2024 08:50 pm', 'Successfully Logout.'),
(85, 2, '5/14/2024 08:51 pm', 'Successfully Login.'),
(86, 2, '5/14/2024 08:52 pm', 'Successfully Logout.'),
(87, 2, '5/14/2024 08:53 pm', 'Successfully Login.'),
(88, 2, '5/14/2024 08:54 pm', 'Successfully Logout.'),
(89, 2, '5/14/2024 08:57 pm', 'Successfully Login.'),
(90, 2, '5/14/2024 08:58 pm', 'Successfully Logout.'),
(91, 2, '5/14/2024 08:59 pm', 'Successfully Login.'),
(92, 2, '5/14/2024 08:59 pm', 'Successfully Logout.'),
(93, 2, '5/14/2024 09:00 pm', 'Successfully Login.'),
(94, 2, '5/14/2024 09:02 pm', 'Successfully Logout.'),
(95, 2, '5/14/2024 09:45 pm', 'Successfully Login.'),
(96, 2, '5/14/2024 09:45 pm', 'Successfully Logout.'),
(97, 2, '5/14/2024 09:48 pm', 'Successfully Login.'),
(98, 2, '5/14/2024 09:48 pm', 'Successfully Logout.'),
(99, 2, '5/14/2024 09:49 pm', 'Successfully Login.'),
(100, 2, '5/14/2024 09:49 pm', 'Successfully Logout.'),
(101, 2, '5/14/2024 10:01 pm', 'Successfully Login.'),
(102, 2, '5/14/2024 10:01 pm', 'Successfully Logout.'),
(103, 2, '5/14/2024 10:06 pm', 'Successfully Login.'),
(104, 2, '5/14/2024 10:07 pm', 'Successfully Logout.'),
(105, 2, '5/14/2024 10:08 pm', 'Successfully Login.'),
(106, 2, '5/14/2024 10:08 pm', 'Successfully Logout.'),
(107, 2, '5/14/2024 10:09 pm', 'Successfully Login.'),
(108, 2, '5/14/2024 10:09 pm', 'Successfully Logout.'),
(109, 2, '5/14/2024 10:11 pm', 'Successfully Login.'),
(110, 2, '5/14/2024 10:12 pm', 'Successfully Logout.'),
(111, 2, '5/14/2024 10:13 pm', 'Successfully Login.'),
(112, 2, '5/14/2024 10:13 pm', 'Successfully Logout.'),
(113, 2, '5/14/2024 10:14 pm', 'Successfully Login.'),
(114, 2, '5/14/2024 10:15 pm', 'Successfully Logout.'),
(115, 2, '5/14/2024 10:27 pm', 'Successfully Login.'),
(116, 2, '5/14/2024 10:27 pm', 'Successfully Logout.'),
(117, 2, '5/14/2024 11:52 pm', 'Successfully Login.'),
(118, 2, '5/14/2024 11:54 pm', 'Successfully Logout.'),
(119, 2, '5/15/2024 12:00 am', 'Successfully Login.'),
(120, 2, '5/15/2024 12:01 am', 'Successfully Logout.'),
(121, 2, '5/15/2024 12:21 am', 'Successfully Login.'),
(122, 2, '5/15/2024 12:21 am', 'Successfully Logout.'),
(123, 2, '5/15/2024 12:27 am', 'Successfully Login.'),
(124, 2, '5/15/2024 12:27 am', 'Successfully Logout.'),
(125, 2, '5/15/2024 12:34 am', 'Successfully Login.'),
(126, 2, '5/15/2024 12:35 am', 'Successfully Logout.'),
(127, 2, '5/15/2024 12:37 am', 'Successfully Login.'),
(128, 2, '5/15/2024 12:38 am', 'Successfully Logout.'),
(129, 2, '5/15/2024 12:52 am', 'Successfully Login.'),
(130, 2, '5/15/2024 12:52 am', 'Successfully Logout.'),
(131, 2, '5/15/2024 12:59 am', 'Successfully Login.'),
(132, 2, '5/15/2024 01:00 am', 'Successfully Logout.'),
(133, 2, '5/15/2024 01:01 am', 'Successfully Login.'),
(134, 2, '5/15/2024 01:02 am', 'Successfully Logout.'),
(135, 2, '5/15/2024 01:02 am', 'Successfully Login.'),
(136, 2, '5/15/2024 01:03 am', 'Successfully Logout.'),
(137, 2, '5/15/2024 01:04 am', 'Successfully Login.'),
(138, 2, '5/15/2024 01:04 am', 'Successfully Logout.'),
(139, 2, '5/15/2024 01:05 am', 'Successfully Login.'),
(140, 2, '5/15/2024 01:07 am', 'Successfully Logout.'),
(141, 2, '5/15/2024 01:09 am', 'Successfully Login.'),
(142, 2, '5/15/2024 01:09 am', 'Successfully Logout.'),
(143, 2, '5/15/2024 01:17 am', 'Successfully Login.'),
(144, 2, '5/15/2024 01:18 am', 'Successfully Logout.'),
(145, 2, '5/15/2024 01:19 am', 'Successfully Login.'),
(146, 2, '5/15/2024 01:20 am', 'Successfully Logout.'),
(147, 2, '5/15/2024 01:21 am', 'Successfully Login.'),
(148, 2, '5/15/2024 01:21 am', 'Successfully Logout.'),
(149, 2, '5/15/2024 01:23 am', 'Successfully Login.'),
(150, 2, '5/15/2024 01:24 am', 'Successfully Logout.'),
(151, 2, '5/15/2024 01:26 am', 'Successfully Login.'),
(152, 2, '5/15/2024 01:27 am', 'Successfully Logout.'),
(153, 2, '5/15/2024 07:57 am', 'Successfully Login.'),
(154, 2, '5/15/2024 07:58 am', 'Successfully Logout.'),
(155, 2, '5/15/2024 08:04 am', 'Successfully Login.'),
(156, 2, '5/15/2024 08:05 am', 'Successfully Logout.'),
(157, 2, '5/15/2024 08:06 am', 'Successfully Login.'),
(158, 2, '5/15/2024 08:08 am', 'Successfully Logout.'),
(159, 2, '5/15/2024 08:09 am', 'Successfully Login.'),
(160, 2, '5/15/2024 08:10 am', 'Successfully Logout.'),
(161, 2, '5/15/2024 08:11 am', 'Successfully Login.'),
(162, 2, '5/15/2024 08:13 am', 'Successfully Logout.'),
(163, 2, '5/15/2024 08:15 am', 'Successfully Login.'),
(164, 2, '5/15/2024 08:15 am', 'Successfully Logout.'),
(165, 2, '5/15/2024 08:21 am', 'Successfully Login.'),
(166, 2, '5/15/2024 08:22 am', 'Successfully Logout.'),
(167, 2, '5/15/2024 08:26 am', 'Successfully Login.'),
(168, 2, '5/15/2024 08:26 am', 'Successfully Logout.'),
(169, 2, '5/15/2024 08:29 am', 'Successfully Login.'),
(170, 2, '5/15/2024 08:30 am', 'Successfully Logout.'),
(171, 2, '5/15/2024 08:32 am', 'Successfully Login.'),
(172, 2, '5/15/2024 08:33 am', 'Successfully Logout.'),
(173, 2, '5/15/2024 08:34 am', 'Successfully Login.'),
(174, 2, '5/15/2024 08:34 am', 'Successfully Logout.'),
(175, 2, '5/15/2024 08:35 am', 'Successfully Login.'),
(176, 2, '5/15/2024 08:37 am', 'Successfully Logout.'),
(177, 2, '5/15/2024 08:38 am', 'Successfully Login.'),
(178, 2, '5/15/2024 08:39 am', 'Successfully Logout.'),
(179, 2, '5/15/2024 08:44 am', 'Successfully Login.'),
(180, 2, '5/15/2024 08:44 am', 'Successfully Logout.'),
(181, 2, '5/15/2024 08:46 am', 'Successfully Login.'),
(182, 2, '5/15/2024 08:46 am', 'Successfully Logout.'),
(183, 2, '5/15/2024 08:48 am', 'Successfully Login.'),
(184, 2, '5/15/2024 08:50 am', 'Successfully Logout.'),
(185, 2, '5/15/2024 08:50 am', 'Successfully Login.'),
(186, 2, '5/15/2024 08:51 am', 'Successfully Logout.'),
(187, 2, '5/15/2024 08:55 am', 'Successfully Login.'),
(188, 2, '5/15/2024 08:57 am', 'Successfully Logout.'),
(189, 2, '5/15/2024 08:59 am', 'Successfully Login.'),
(190, 2, '5/15/2024 09:01 am', 'Successfully Logout.'),
(191, 2, '5/15/2024 09:01 am', 'Successfully Login.'),
(192, 2, '5/15/2024 09:01 am', 'Successfully Logout.'),
(193, 2, '5/15/2024 09:03 am', 'Successfully Login.'),
(194, 2, '5/15/2024 09:04 am', 'Successfully Logout.'),
(195, 2, '5/15/2024 09:06 am', 'Successfully Login.'),
(196, 2, '5/15/2024 09:07 am', 'Successfully Logout.'),
(197, 2, '5/15/2024 09:14 am', 'Successfully Login.'),
(198, 2, '5/15/2024 09:14 am', 'Successfully Logout.'),
(199, 2, '5/15/2024 09:35 am', 'Successfully Login.'),
(200, 2, '5/15/2024 09:36 am', 'Successfully Logout.'),
(201, 2, '5/15/2024 09:37 am', 'Successfully Login.'),
(202, 2, '5/15/2024 09:37 am', 'Successfully Logout.'),
(203, 2, '5/15/2024 09:50 am', 'Successfully Login.'),
(204, 2, '5/15/2024 09:50 am', 'Successfully Logout.'),
(205, 2, '5/15/2024 09:50 am', 'Successfully Login.'),
(206, 2, '5/15/2024 09:54 am', 'Successfully Logout.'),
(207, 2, '5/15/2024 09:56 am', 'Successfully Login.'),
(208, 2, '5/15/2024 09:56 am', 'Successfully Logout.'),
(209, 2, '5/15/2024 09:58 am', 'Successfully Login.'),
(210, 2, '5/15/2024 09:59 am', 'Successfully Logout.'),
(211, 2, '5/15/2024 10:00 am', 'Successfully Login.'),
(212, 2, '5/15/2024 10:01 am', 'Successfully Logout.'),
(213, 2, '5/15/2024 10:03 am', 'Successfully Login.'),
(214, 2, '5/15/2024 10:03 am', 'Successfully Logout.'),
(215, 2, '5/15/2024 10:04 am', 'Successfully Login.'),
(216, 2, '5/15/2024 10:04 am', 'Successfully Logout.'),
(217, 2, '5/15/2024 10:05 am', 'Successfully Login.'),
(218, 2, '5/15/2024 10:06 am', 'Successfully Logout.'),
(219, 2, '5/15/2024 10:09 am', 'Successfully Login.'),
(220, 2, '5/15/2024 10:10 am', 'Successfully Logout.'),
(221, 2, '5/15/2024 10:19 am', 'Successfully Login.'),
(222, 2, '5/15/2024 10:20 am', 'Successfully Logout.'),
(223, 2, '5/15/2024 10:20 am', 'Successfully Login.'),
(224, 2, '5/15/2024 10:21 am', 'Successfully Logout.'),
(225, 2, '5/15/2024 10:22 am', 'Successfully Login.'),
(226, 2, '5/15/2024 10:22 am', 'Successfully Logout.'),
(227, 2, '5/15/2024 10:27 am', 'Successfully Login.'),
(228, 2, '5/15/2024 10:30 am', 'Successfully Logout.'),
(229, 2, '5/15/2024 10:32 am', 'Successfully Login.'),
(230, 2, '5/15/2024 10:32 am', 'Successfully Logout.'),
(231, 2, '5/15/2024 10:33 am', 'Successfully Login.'),
(232, 2, '5/15/2024 10:34 am', 'Successfully Logout.'),
(233, 2, '5/15/2024 10:36 am', 'Successfully Login.'),
(234, 2, '5/15/2024 10:38 am', 'Successfully Logout.'),
(235, 2, '5/15/2024 11:03 am', 'Successfully Login.'),
(236, 2, '5/15/2024 11:03 am', 'Successfully Logout.'),
(237, 2, '5/15/2024 11:03 am', 'Successfully Login.'),
(238, 2, '5/15/2024 11:04 am', 'Successfully Logout.'),
(239, 2, '5/15/2024 11:07 am', 'Successfully Login.'),
(240, 2, '5/15/2024 11:09 am', 'Successfully Logout.'),
(241, 2, '5/15/2024 11:10 am', 'Successfully Login.'),
(242, 2, '5/15/2024 11:11 am', 'Successfully Logout.'),
(243, 2, '5/15/2024 11:22 am', 'Successfully Login.'),
(244, 2, '5/15/2024 11:23 am', 'Successfully Logout.'),
(245, 2, '5/15/2024 11:24 am', 'Successfully Login.'),
(246, 2, '5/15/2024 11:25 am', 'Successfully Logout.'),
(247, 2, '5/15/2024 11:26 am', 'Successfully Login.'),
(248, 2, '5/15/2024 11:27 am', 'Successfully Logout.'),
(249, 2, '5/15/2024 11:28 am', 'Successfully Login.'),
(250, 2, '5/15/2024 11:28 am', 'Successfully Logout.'),
(251, 2, '5/15/2024 11:28 am', 'Successfully Login.'),
(252, 2, '5/15/2024 11:29 am', 'Successfully Logout.'),
(253, 2, '5/15/2024 11:33 am', 'Successfully Login.'),
(254, 2, '5/15/2024 11:34 am', 'Successfully Logout.'),
(255, 2, '5/15/2024 11:41 am', 'Successfully Login.'),
(256, 2, '5/15/2024 11:44 am', 'Successfully Logout.'),
(257, 2, '5/15/2024 11:46 am', 'Successfully Login.'),
(258, 2, '5/15/2024 11:47 am', 'Successfully Logout.'),
(259, 2, '5/15/2024 11:48 am', 'Successfully Login.'),
(260, 2, '5/15/2024 11:48 am', 'Successfully Logout.'),
(261, 2, '5/15/2024 12:02 pm', 'Successfully Login.'),
(262, 2, '5/15/2024 12:03 pm', 'Successfully Logout.'),
(263, 2, '5/15/2024 12:06 pm', 'Successfully Login.'),
(264, 2, '5/15/2024 12:08 pm', 'Successfully Logout.'),
(265, 2, '5/15/2024 12:13 pm', 'Successfully Login.'),
(266, 2, '5/15/2024 12:13 pm', 'Successfully Logout.'),
(267, 2, '5/15/2024 12:14 pm', 'Successfully Login.'),
(268, 2, '5/15/2024 12:15 pm', 'Successfully Logout.'),
(269, 2, '5/15/2024 12:17 pm', 'Successfully Login.'),
(270, 2, '5/15/2024 12:17 pm', 'Successfully Logout.'),
(271, 2, '5/15/2024 12:19 pm', 'Successfully Login.'),
(272, 2, '5/15/2024 12:23 pm', 'Successfully Login.'),
(273, 2, '5/15/2024 12:24 pm', 'Successfully Logout.'),
(274, 2, '5/15/2024 12:25 pm', 'Successfully Login.'),
(275, 2, '5/15/2024 12:26 pm', 'Successfully Logout.'),
(276, 2, '5/15/2024 12:27 pm', 'Successfully Login.'),
(277, 2, '5/15/2024 12:29 pm', 'Successfully Logout.'),
(278, 2, '5/15/2024 12:49 pm', 'Successfully Login.'),
(279, 2, '5/15/2024 01:04 pm', 'Successfully Login.'),
(280, 2, '5/15/2024 01:04 pm', 'Successfully Logout.'),
(281, 2, '5/15/2024 01:05 pm', 'Successfully Login.'),
(282, 2, '5/15/2024 01:05 pm', 'Successfully Logout.'),
(283, 2, '5/15/2024 01:06 pm', 'Successfully Login.'),
(284, 2, '5/15/2024 01:06 pm', 'Successfully Login.'),
(285, 2, '5/15/2024 01:06 pm', 'Successfully Logout.'),
(286, 2, '5/15/2024 01:07 pm', 'Successfully Login.'),
(287, 2, '5/15/2024 01:08 pm', 'Successfully Logout.'),
(288, 2, '5/15/2024 01:09 pm', 'Successfully Login.'),
(289, 2, '5/15/2024 01:09 pm', 'Successfully Logout.'),
(290, 2, '5/15/2024 01:09 pm', 'Successfully Login.'),
(291, 2, '5/15/2024 01:10 pm', 'Successfully Logout.'),
(292, 2, '5/15/2024 01:12 pm', 'Successfully Login.'),
(293, 2, '5/15/2024 01:12 pm', 'Successfully Logout.'),
(294, 2, '5/15/2024 01:13 pm', 'Successfully Login.'),
(295, 2, '5/15/2024 01:14 pm', 'Successfully Logout.'),
(296, 2, '5/15/2024 01:15 pm', 'Successfully Login.'),
(297, 2, '5/15/2024 01:15 pm', 'Successfully Logout.'),
(298, 2, '5/15/2024 01:15 pm', 'Successfully Login.'),
(299, 2, '5/15/2024 01:17 pm', 'Successfully Logout.'),
(300, 2, '5/15/2024 01:19 pm', 'Successfully Login.'),
(301, 2, '5/15/2024 01:20 pm', 'Successfully Login.'),
(302, 2, '5/15/2024 01:20 pm', 'Successfully Logout.'),
(303, 2, '5/15/2024 01:24 pm', 'Successfully Login.'),
(304, 2, '5/15/2024 01:24 pm', 'Successfully Logout.'),
(305, 2, '5/15/2024 01:26 pm', 'Successfully Login.'),
(306, 2, '5/15/2024 01:26 pm', 'Successfully Logout.'),
(307, 2, '5/15/2024 01:28 pm', 'Successfully Login.'),
(308, 2, '5/15/2024 01:28 pm', 'Successfully Logout.'),
(309, 2, '5/15/2024 01:31 pm', 'Successfully Login.'),
(310, 2, '5/15/2024 01:32 pm', 'Successfully Logout.'),
(311, 2, '5/15/2024 01:38 pm', 'Successfully Login.'),
(312, 2, '5/15/2024 01:38 pm', 'Successfully Logout.'),
(313, 2, '5/15/2024 01:38 pm', 'Successfully Login.'),
(314, 2, '5/15/2024 01:39 pm', 'Successfully Logout.'),
(315, 2, '5/15/2024 01:39 pm', 'Successfully Login.'),
(316, 2, '5/15/2024 01:39 pm', 'Successfully Logout.'),
(317, 2, '5/15/2024 01:41 pm', 'Successfully Login.'),
(318, 2, '5/15/2024 01:41 pm', 'Successfully Logout.'),
(319, 2, '5/15/2024 02:00 pm', 'Successfully Login.'),
(320, 2, '5/15/2024 02:02 pm', 'Successfully Logout.'),
(321, 2, '5/15/2024 02:03 pm', 'Successfully Login.'),
(322, 2, '5/15/2024 02:04 pm', 'Successfully Logout.'),
(323, 2, '5/15/2024 02:07 pm', 'Successfully Login.'),
(324, 2, '5/15/2024 02:07 pm', 'Successfully Logout.'),
(325, 2, '5/15/2024 02:08 pm', 'Successfully Login.'),
(326, 2, '5/15/2024 02:08 pm', 'Successfully Logout.'),
(327, 2, '5/15/2024 02:08 pm', 'Successfully Login.'),
(328, 2, '5/15/2024 02:10 pm', 'Successfully Logout.'),
(329, 2, '5/15/2024 02:12 pm', 'Successfully Login.'),
(330, 2, '5/15/2024 02:13 pm', 'Successfully Logout.'),
(331, 2, '5/15/2024 02:14 pm', 'Successfully Login.'),
(332, 2, '5/15/2024 02:16 pm', 'Successfully Logout.'),
(333, 2, '5/15/2024 02:19 pm', 'Successfully Login.'),
(334, 2, '5/15/2024 02:22 pm', 'Successfully Logout.'),
(335, 2, '5/15/2024 02:23 pm', 'Successfully Login.'),
(336, 2, '5/15/2024 02:24 pm', 'Successfully Logout.'),
(337, 2, '5/15/2024 02:27 pm', 'Successfully Login.'),
(338, 2, '5/15/2024 02:30 pm', 'Successfully Logout.'),
(339, 2, '5/15/2024 02:32 pm', 'Successfully Login.'),
(340, 2, '5/15/2024 02:35 pm', 'Successfully Logout.'),
(341, 2, '5/15/2024 02:36 pm', 'Successfully Login.'),
(342, 2, '5/15/2024 02:36 pm', 'Successfully Logout.'),
(343, 2, '5/15/2024 02:36 pm', 'Successfully Login.'),
(344, 2, '5/15/2024 02:38 pm', 'Successfully Logout.'),
(345, 2, '5/15/2024 02:39 pm', 'Successfully Login.'),
(346, 2, '5/15/2024 02:41 pm', 'Successfully Logout.'),
(347, 2, '5/15/2024 02:42 pm', 'Successfully Login.'),
(348, 2, '5/15/2024 02:46 pm', 'Successfully Logout.'),
(349, 2, '5/15/2024 02:49 pm', 'Successfully Login.'),
(350, 2, '5/15/2024 02:49 pm', 'Successfully Logout.'),
(351, 2, '5/15/2024 02:50 pm', 'Successfully Login.'),
(352, 2, '5/15/2024 02:50 pm', 'Successfully Logout.'),
(353, 2, '5/15/2024 02:52 pm', 'Successfully Login.'),
(354, 2, '5/15/2024 02:53 pm', 'Successfully Logout.'),
(355, 2, '5/15/2024 02:56 pm', 'Successfully Login.'),
(356, 2, '5/15/2024 02:56 pm', 'Successfully Logout.'),
(357, 2, '5/15/2024 03:01 pm', 'Successfully Login.'),
(358, 2, '5/15/2024 03:04 pm', 'Successfully Logout.'),
(359, 2, '5/15/2024 03:07 pm', 'Successfully Login.'),
(360, 2, '5/15/2024 03:08 pm', 'Successfully Logout.'),
(361, 2, '5/15/2024 03:10 pm', 'Successfully Login.'),
(362, 2, '5/15/2024 03:12 pm', 'Successfully Logout.'),
(363, 2, '5/15/2024 03:12 pm', 'Successfully Login.'),
(364, 2, '5/15/2024 03:13 pm', 'Successfully Logout.'),
(365, 2, '5/15/2024 03:14 pm', 'Successfully Login.'),
(366, 2, '5/15/2024 03:14 pm', 'Successfully Logout.'),
(367, 2, '5/15/2024 03:15 pm', 'Successfully Login.'),
(368, 2, '5/15/2024 03:16 pm', 'Successfully Logout.'),
(369, 2, '5/15/2024 03:19 pm', 'Successfully Login.'),
(370, 2, '5/15/2024 03:20 pm', 'Successfully Logout.'),
(371, 2, '5/15/2024 03:20 pm', 'Successfully Login.'),
(372, 2, '5/15/2024 03:21 pm', 'Successfully Logout.'),
(373, 2, '5/15/2024 03:22 pm', 'Successfully Login.'),
(374, 2, '5/15/2024 03:22 pm', 'Successfully Logout.'),
(375, 1, '5/15/2024 03:22 pm', 'Successfully Login.'),
(376, 1, '5/15/2024 03:22 pm', 'Successfully Logout.'),
(377, 2, '5/15/2024 03:22 pm', 'Successfully Login.'),
(378, 2, '5/15/2024 03:24 pm', 'Successfully Logout.'),
(379, 2, '5/15/2024 03:25 pm', 'Successfully Login.'),
(380, 2, '5/15/2024 03:25 pm', 'Successfully Logout.'),
(381, 2, '5/15/2024 03:26 pm', 'Successfully Login.'),
(382, 2, '5/15/2024 03:37 pm', 'Successfully Logout.'),
(383, 2, '5/15/2024 03:44 pm', 'Successfully Login.'),
(384, 2, '5/15/2024 03:44 pm', 'Successfully Logout.'),
(385, 2, '5/15/2024 03:56 pm', 'Successfully Login.'),
(386, 2, '5/15/2024 03:57 pm', 'Successfully Logout.'),
(387, 2, '5/15/2024 04:18 pm', 'Successfully Login.'),
(388, 2, '5/15/2024 04:23 pm', 'Successfully Logout.'),
(389, 2, '5/15/2024 04:24 pm', 'Successfully Login.'),
(390, 2, '5/15/2024 04:25 pm', 'Successfully Logout.'),
(391, 2, '5/15/2024 04:30 pm', 'Successfully Login.'),
(392, 2, '5/15/2024 04:30 pm', 'Successfully Logout.'),
(393, 2, '5/15/2024 04:49 pm', 'Successfully Login.'),
(394, 2, '5/15/2024 04:50 pm', 'Successfully Login.'),
(395, 2, '5/15/2024 04:51 pm', 'Successfully Logout.'),
(396, 2, '5/15/2024 04:53 pm', 'Successfully Login.'),
(397, 2, '5/15/2024 04:53 pm', 'Successfully Logout.'),
(398, 2, '5/15/2024 04:55 pm', 'Successfully Login.'),
(399, 2, '5/15/2024 04:55 pm', 'Successfully Logout.'),
(400, 2, '5/15/2024 04:57 pm', 'Successfully Login.'),
(401, 2, '5/15/2024 05:10 pm', 'Successfully Login.'),
(402, 2, '5/15/2024 05:12 pm', 'Successfully Logout.'),
(403, 2, '5/15/2024 05:13 pm', 'Successfully Login.'),
(404, 2, '5/15/2024 05:15 pm', 'Successfully Logout.'),
(405, 2, '5/15/2024 05:19 pm', 'Successfully Login.'),
(406, 2, '5/15/2024 05:20 pm', 'Successfully Logout.'),
(407, 2, '5/15/2024 05:20 pm', 'Successfully Login.'),
(408, 2, '5/15/2024 05:22 pm', 'Successfully Logout.'),
(409, 2, '5/15/2024 05:22 pm', 'Successfully Login.'),
(410, 2, '5/15/2024 05:22 pm', 'Successfully Logout.'),
(411, 2, '5/15/2024 05:23 pm', 'Successfully Login.'),
(412, 2, '5/15/2024 05:26 pm', 'Successfully Logout.'),
(413, 2, '5/15/2024 05:28 pm', 'Successfully Login.'),
(414, 2, '5/15/2024 05:29 pm', 'Successfully Logout.'),
(415, 2, '5/15/2024 05:31 pm', 'Successfully Login.'),
(416, 2, '5/15/2024 05:40 pm', 'Successfully Logout.'),
(417, 2, '5/15/2024 05:41 pm', 'Successfully Login.'),
(418, 2, '5/15/2024 05:43 pm', 'Successfully Logout.'),
(419, 2, '5/15/2024 05:59 pm', 'Successfully Login.'),
(420, 2, '5/15/2024 05:59 pm', 'Successfully Logout.'),
(421, 2, '5/15/2024 11:08 pm', 'Successfully Login.'),
(422, 2, '5/15/2024 11:13 pm', 'Successfully Logout.'),
(423, 2, '5/15/2024 11:13 pm', 'Successfully Login.'),
(424, 2, '5/15/2024 11:13 pm', 'Successfully Logout.'),
(425, 2, '5/15/2024 11:16 pm', 'Successfully Login.'),
(426, 2, '5/15/2024 11:17 pm', 'Successfully Logout.'),
(427, 2, '5/15/2024 11:18 pm', 'Successfully Login.'),
(428, 2, '5/15/2024 11:18 pm', 'Successfully Logout.'),
(429, 2, '5/16/2024 12:11 am', 'Successfully Login.'),
(430, 2, '5/16/2024 12:13 am', 'Successfully Logout.'),
(431, 2, '5/16/2024 12:15 am', 'Successfully Login.'),
(432, 2, '5/16/2024 12:16 am', 'Successfully Logout.'),
(433, 2, '5/16/2024 12:17 am', 'Successfully Login.'),
(434, 2, '5/16/2024 12:18 am', 'Successfully Logout.'),
(435, 2, '5/16/2024 12:19 am', 'Successfully Login.'),
(436, 2, '5/16/2024 12:25 am', 'Successfully Logout.'),
(437, 2, '5/16/2024 12:25 am', 'Successfully Login.'),
(438, 2, '5/16/2024 12:31 am', 'Successfully Logout.'),
(439, 1, '5/16/2024 12:31 am', 'Successfully Login.'),
(440, 1, '5/16/2024 12:32 am', 'Successfully Logout.'),
(441, 2, '5/16/2024 12:32 am', 'Successfully Login.'),
(442, 2, '5/16/2024 12:33 am', 'Successfully Logout.'),
(443, 2, '5/16/2024 12:37 am', 'Successfully Login.'),
(444, 2, '5/16/2024 12:38 am', 'Successfully Logout.'),
(445, 2, '5/16/2024 12:42 am', 'Successfully Login.'),
(446, 2, '5/16/2024 12:43 am', 'Successfully Logout.'),
(447, 2, '5/16/2024 12:50 am', 'Successfully Login.'),
(448, 2, '5/16/2024 12:50 am', 'Successfully Logout.'),
(449, 2, '5/16/2024 12:51 am', 'Successfully Login.'),
(450, 2, '5/16/2024 12:52 am', 'Successfully Logout.'),
(451, 2, '5/16/2024 01:00 am', 'Successfully Login.'),
(452, 2, '5/16/2024 01:01 am', 'Successfully Logout.'),
(453, 2, '5/16/2024 01:02 am', 'Successfully Login.'),
(454, 2, '5/16/2024 01:03 am', 'Successfully Logout.'),
(455, 2, '5/16/2024 01:04 am', 'Successfully Login.'),
(456, 2, '5/16/2024 01:05 am', 'Successfully Logout.'),
(457, 2, '5/16/2024 01:08 am', 'Successfully Login.'),
(458, 2, '5/16/2024 01:08 am', 'Successfully Logout.'),
(459, 2, '5/16/2024 01:14 am', 'Successfully Login.'),
(460, 2, '5/16/2024 01:15 am', 'Successfully Logout.'),
(461, 2, '5/16/2024 01:16 am', 'Successfully Login.'),
(462, 2, '5/16/2024 01:21 am', 'Successfully Logout.'),
(463, 2, '5/16/2024 01:23 am', 'Successfully Login.'),
(464, 2, '5/16/2024 01:24 am', 'Successfully Logout.'),
(465, 2, '5/16/2024 07:28 am', 'Successfully Login.'),
(466, 2, '5/16/2024 07:29 am', 'Successfully Logout.'),
(467, 2, '5/16/2024 07:37 am', 'Successfully Login.'),
(468, 2, '5/16/2024 07:38 am', 'Successfully Logout.'),
(469, 2, '5/16/2024 07:56 am', 'Successfully Login.'),
(470, 2, '5/16/2024 08:01 am', 'Successfully Logout.'),
(471, 2, '5/16/2024 08:08 am', 'Successfully Login.'),
(472, 2, '5/16/2024 08:10 am', 'Successfully Logout.'),
(473, 2, '5/16/2024 08:11 am', 'Successfully Login.'),
(474, 2, '5/16/2024 08:11 am', 'Successfully Logout.'),
(475, 2, '5/16/2024 08:14 am', 'Successfully Login.'),
(476, 2, '5/16/2024 08:14 am', 'Successfully Logout.'),
(477, 2, '5/16/2024 08:16 am', 'Successfully Login.'),
(478, 2, '5/16/2024 08:16 am', 'Successfully Logout.'),
(479, 2, '5/16/2024 08:28 am', 'Successfully Login.'),
(480, 2, '5/16/2024 08:29 am', 'Successfully Logout.'),
(481, 2, '5/16/2024 08:29 am', 'Successfully Login.'),
(482, 2, '5/16/2024 08:30 am', 'Successfully Logout.'),
(483, 2, '5/16/2024 08:32 am', 'Successfully Login.'),
(484, 2, '5/16/2024 08:32 am', 'Successfully Logout.'),
(485, 2, '5/16/2024 08:33 am', 'Successfully Login.'),
(486, 2, '5/16/2024 08:33 am', 'Successfully Logout.'),
(487, 2, '5/16/2024 08:33 am', 'Successfully Login.'),
(488, 2, '5/16/2024 08:34 am', 'Successfully Logout.'),
(489, 2, '5/16/2024 08:36 am', 'Successfully Login.'),
(490, 2, '5/16/2024 08:39 am', 'Successfully Logout.'),
(491, 2, '5/16/2024 08:41 am', 'Successfully Login.'),
(492, 2, '5/16/2024 08:41 am', 'Successfully Logout.'),
(493, 2, '5/16/2024 08:43 am', 'Successfully Login.'),
(494, 2, '5/16/2024 08:44 am', 'Successfully Logout.'),
(495, 1, '5/16/2024 08:44 am', 'Successfully Login.'),
(496, 1, '5/16/2024 08:45 am', 'Successfully Logout.'),
(497, 2, '5/16/2024 08:45 am', 'Successfully Login.'),
(498, 2, '5/16/2024 08:49 am', 'Successfully Logout.'),
(499, 2, '5/16/2024 08:54 am', 'Successfully Login.'),
(500, 2, '5/16/2024 08:59 am', 'Successfully Logout.'),
(501, 2, '5/16/2024 09:02 am', 'Successfully Login.'),
(502, 2, '5/16/2024 09:02 am', 'Successfully Logout.'),
(503, 2, '5/16/2024 09:03 am', 'Successfully Login.'),
(504, 2, '5/16/2024 09:03 am', 'Successfully Logout.'),
(505, 2, '5/16/2024 09:04 am', 'Successfully Login.'),
(506, 2, '5/16/2024 09:04 am', 'Successfully Logout.'),
(507, 2, '5/16/2024 09:05 am', 'Successfully Login.'),
(508, 2, '5/16/2024 09:08 am', 'Successfully Login.'),
(509, 2, '5/16/2024 09:08 am', 'Successfully Logout.'),
(510, 2, '5/16/2024 09:10 am', 'Successfully Login.'),
(511, 2, '5/16/2024 09:11 am', 'Successfully Logout.'),
(512, 2, '5/16/2024 09:13 am', 'Successfully Login.'),
(513, 2, '5/16/2024 09:13 am', 'Successfully Logout.'),
(514, 2, '5/16/2024 09:13 am', 'Successfully Login.'),
(515, 2, '5/16/2024 09:13 am', 'Successfully Logout.'),
(516, 2, '5/16/2024 09:15 am', 'Successfully Login.'),
(517, 2, '5/16/2024 09:15 am', 'Successfully Logout.'),
(518, 2, '5/16/2024 09:16 am', 'Successfully Login.'),
(519, 2, '5/16/2024 09:16 am', 'Successfully Logout.'),
(520, 2, '5/16/2024 09:17 am', 'Successfully Login.'),
(521, 2, '5/16/2024 09:17 am', 'Successfully Logout.'),
(522, 2, '5/16/2024 09:17 am', 'Successfully Login.'),
(523, 2, '5/16/2024 09:18 am', 'Successfully Logout.'),
(524, 2, '5/16/2024 09:19 am', 'Successfully Login.'),
(525, 2, '5/16/2024 09:19 am', 'Successfully Logout.'),
(526, 2, '5/16/2024 09:20 am', 'Successfully Login.'),
(527, 2, '5/16/2024 09:20 am', 'Successfully Logout.'),
(528, 2, '5/16/2024 09:21 am', 'Successfully Login.'),
(529, 2, '5/16/2024 09:21 am', 'Successfully Logout.'),
(530, 2, '5/16/2024 09:22 am', 'Successfully Login.'),
(531, 2, '5/16/2024 09:22 am', 'Successfully Logout.'),
(532, 2, '5/16/2024 09:25 am', 'Successfully Login.'),
(533, 2, '5/16/2024 09:26 am', 'Successfully Logout.'),
(534, 2, '5/16/2024 09:32 am', 'Successfully Login.'),
(535, 2, '5/16/2024 09:32 am', 'Successfully Login.'),
(536, 2, '5/16/2024 09:33 am', 'Successfully Logout.'),
(537, 2, '5/16/2024 09:34 am', 'Successfully Login.'),
(538, 2, '5/16/2024 09:35 am', 'Successfully Login.'),
(539, 2, '5/16/2024 09:35 am', 'Successfully Logout.'),
(540, 2, '5/16/2024 09:36 am', 'Successfully Login.'),
(541, 2, '5/16/2024 09:36 am', 'Successfully Login.'),
(542, 2, '5/16/2024 09:36 am', 'Successfully Logout.'),
(543, 2, '5/16/2024 09:38 am', 'Successfully Login.'),
(544, 2, '5/16/2024 09:38 am', 'Successfully Logout.'),
(545, 2, '5/16/2024 09:38 am', 'Successfully Login.'),
(546, 2, '5/16/2024 09:39 am', 'Successfully Logout.'),
(547, 2, '5/16/2024 09:39 am', 'Successfully Login.'),
(548, 2, '5/16/2024 09:39 am', 'Successfully Logout.'),
(549, 2, '5/16/2024 09:39 am', 'Successfully Login.'),
(550, 2, '5/16/2024 09:40 am', 'Successfully Logout.'),
(551, 2, '5/16/2024 09:41 am', 'Successfully Login.'),
(552, 2, '5/16/2024 09:42 am', 'Successfully Logout.'),
(553, 2, '5/16/2024 09:42 am', 'Successfully Login.'),
(554, 2, '5/16/2024 09:42 am', 'Successfully Logout.'),
(555, 2, '5/16/2024 09:43 am', 'Successfully Login.'),
(556, 2, '5/16/2024 09:43 am', 'Successfully Logout.'),
(557, 2, '5/16/2024 09:44 am', 'Successfully Login.'),
(558, 2, '5/16/2024 09:44 am', 'Successfully Logout.'),
(559, 2, '5/16/2024 09:44 am', 'Successfully Login.'),
(560, 2, '5/16/2024 09:45 am', 'Successfully Logout.'),
(561, 2, '5/16/2024 09:46 am', 'Successfully Login.'),
(562, 2, '5/16/2024 09:46 am', 'Successfully Logout.'),
(563, 2, '5/16/2024 09:47 am', 'Successfully Login.'),
(564, 2, '5/16/2024 09:47 am', 'Successfully Logout.'),
(565, 2, '5/16/2024 09:53 am', 'Successfully Login.'),
(566, 2, '5/16/2024 09:54 am', 'Successfully Login.'),
(567, 2, '5/16/2024 09:54 am', 'Successfully Logout.'),
(568, 2, '5/16/2024 09:55 am', 'Successfully Login.'),
(569, 2, '5/16/2024 09:55 am', 'Successfully Logout.'),
(570, 2, '5/16/2024 09:57 am', 'Successfully Login.'),
(571, 2, '5/16/2024 09:57 am', 'Successfully Logout.'),
(572, 2, '5/16/2024 09:58 am', 'Successfully Login.'),
(573, 2, '5/16/2024 09:58 am', 'Successfully Logout.'),
(574, 2, '5/16/2024 09:59 am', 'Successfully Login.'),
(575, 2, '5/16/2024 10:00 am', 'Successfully Login.'),
(576, 2, '5/16/2024 10:06 am', 'Successfully Logout.'),
(577, 2, '5/16/2024 10:15 am', 'Successfully Login.'),
(578, 2, '5/16/2024 10:15 am', 'Successfully Logout.'),
(579, 2, '5/16/2024 10:48 am', 'Successfully Login.'),
(580, 2, '5/16/2024 10:49 am', 'Successfully Login.'),
(581, 2, '5/16/2024 10:49 am', 'Successfully Logout.'),
(582, 2, '5/16/2024 10:50 am', 'Successfully Login.'),
(583, 2, '5/16/2024 10:50 am', 'Successfully Logout.'),
(584, 2, '5/16/2024 10:52 am', 'Successfully Login.'),
(585, 2, '5/16/2024 10:53 am', 'Successfully Logout.'),
(586, 2, '5/16/2024 10:53 am', 'Successfully Login.'),
(587, 2, '5/16/2024 10:54 am', 'Successfully Logout.'),
(588, 2, '5/16/2024 10:57 am', 'Successfully Login.'),
(589, 2, '5/16/2024 11:02 am', 'Successfully Logout.');

-- --------------------------------------------------------

--
-- Table structure for table `audit_logs_old_salary`
--

CREATE TABLE `audit_logs_old_salary` (
  `salary_id` int(10) NOT NULL,
  `emp_no` int(10) NOT NULL,
  `pay_mode` varchar(100) NOT NULL,
  `con_level` varchar(100) NOT NULL,
  `cat` varchar(100) NOT NULL,
  `pay_terms` varchar(100) NOT NULL,
  `salary` decimal(65,2) NOT NULL,
  `regular_pay` decimal(65,2) NOT NULL,
  `daily_rate` decimal(65,2) NOT NULL,
  `daliy_hour_rate` decimal(65,2) NOT NULL,
  `bank_name` varchar(100) NOT NULL,
  `bank_acc` varchar(100) NOT NULL,
  `bank_acc2` varchar(100) NOT NULL,
  `tax_sub` decimal(65,2) NOT NULL,
  `sss_ee` decimal(65,2) NOT NULL,
  `sss_ee_monthly` decimal(65,2) NOT NULL,
  `sss_er` decimal(65,2) NOT NULL,
  `sss_er_monthly` decimal(65,2) NOT NULL,
  `pag_ibig_ee` decimal(65,2) NOT NULL,
  `pag_ibig_ee_monthly` decimal(65,2) NOT NULL,
  `pag_ibig_er` decimal(65,2) NOT NULL,
  `pag_ibig_er_monthly` decimal(65,2) NOT NULL,
  `philhealth_ee` decimal(65,2) NOT NULL,
  `philhealth_ee_monthly` decimal(65,2) NOT NULL,
  `philhealth_er` decimal(65,2) NOT NULL,
  `philhealth_er_monthly` decimal(65,2) NOT NULL,
  `acc_code` varchar(100) NOT NULL,
  `gl_no` varchar(100) NOT NULL,
  `13thmonthpay` decimal(65,2) NOT NULL,
  `14thmonthpay` decimal(65,2) NOT NULL,
  `15thmonthpay` decimal(65,2) NOT NULL,
  `mntbonus` decimal(65,2) NOT NULL,
  `emp_status` varchar(100) NOT NULL,
  `date_hire` varchar(100) NOT NULL,
  `date_resign` varchar(100) NOT NULL,
  `designation` varchar(100) NOT NULL,
  `department` varchar(100) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_general_ci;

--
-- Dumping data for table `audit_logs_old_salary`
--

INSERT INTO `audit_logs_old_salary` (`salary_id`, `emp_no`, `pay_mode`, `con_level`, `cat`, `pay_terms`, `salary`, `regular_pay`, `daily_rate`, `daliy_hour_rate`, `bank_name`, `bank_acc`, `bank_acc2`, `tax_sub`, `sss_ee`, `sss_ee_monthly`, `sss_er`, `sss_er_monthly`, `pag_ibig_ee`, `pag_ibig_ee_monthly`, `pag_ibig_er`, `pag_ibig_er_monthly`, `philhealth_ee`, `philhealth_ee_monthly`, `philhealth_er`, `philhealth_er_monthly`, `acc_code`, `gl_no`, `13thmonthpay`, `14thmonthpay`, `15thmonthpay`, `mntbonus`, `emp_status`, `date_hire`, `date_resign`, `designation`, `department`) VALUES
(0, 0, '', '', '', '', 45000.00, 0.00, 0.00, 0.00, '', '', '', 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, '', '', 0.00, 0.00, 0.00, 0.00, '', '', '', '', ''),
(0, 0, '', '', '', '', 0.00, 0.00, 0.00, 0.00, '', '', '', 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, '', '', 0.00, 0.00, 0.00, 0.00, '', '', '', '', ''),
(0, 0, '', '', '', '', 0.00, 0.00, 0.00, 0.00, '', '', '', 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, '', '', 0.00, 0.00, 0.00, 0.00, '', '', '', '', ''),
(0, 0, '', '', '', '', 0.00, 0.00, 0.00, 0.00, '', '', '', 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, '', '', 0.00, 0.00, 0.00, 0.00, '', '', '', '', ''),
(0, 0, '', '', '', '', 0.00, 0.00, 0.00, 0.00, '', '', '', 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, '', '', 0.00, 0.00, 0.00, 0.00, '', '', '', '', ''),
(0, 0, '', '', '', '', 0.00, 0.00, 0.00, 0.00, '', '', '', 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, '', '', 0.00, 0.00, 0.00, 0.00, '', '', '', '', ''),
(0, 0, '', '', '', '', 35000.00, 0.00, 0.00, 0.00, '', '', '', 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, '', '', 0.00, 0.00, 0.00, 0.00, '', '', '', '', ''),
(0, 0, '', '', '', '', 35000.00, 0.00, 0.00, 0.00, '', '', '', 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, '', '', 0.00, 0.00, 0.00, 0.00, '', '', '', '', ''),
(0, 0, '', '', '', '', 35000.00, 0.00, 0.00, 0.00, '', '', '', 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, '', '', 0.00, 0.00, 0.00, 0.00, '', '', '', '', ''),
(0, 0, '', '', '', '', 35000.00, 0.00, 0.00, 0.00, '', '', '', 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, '', '', 0.00, 0.00, 0.00, 0.00, '', '', '', '', ''),
(0, 0, '', '', '', '', 35000.00, 0.00, 0.00, 0.00, '', '', '', 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, '', '', 0.00, 0.00, 0.00, 0.00, '', '', '', '', ''),
(0, 0, '', '', '', '', 35000.00, 0.00, 0.00, 0.00, '', '', '', 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, '', '', 0.00, 0.00, 0.00, 0.00, '', '', '', '', ''),
(0, 0, '', '', '', '', 35000.00, 0.00, 0.00, 0.00, '', '', '', 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, '', '', 0.00, 0.00, 0.00, 0.00, '', '', '', '', ''),
(0, 0, '', '', '', '', 35000.00, 0.00, 0.00, 0.00, '', '', '', 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, '', '', 0.00, 0.00, 0.00, 0.00, '', '', '', '', ''),
(0, 0, '', '', '', '', 35000.00, 0.00, 0.00, 0.00, '', '', '', 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, '', '', 0.00, 0.00, 0.00, 0.00, '', '', '', '', ''),
(0, 0, '', '', '', '', 40000.00, 0.00, 0.00, 0.00, '', '', '', 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, '', '', 0.00, 0.00, 0.00, 0.00, '', '', '', '', ''),
(0, 0, '', '', '', '', 35000.00, 0.00, 0.00, 0.00, '', '', '', 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, '', '', 0.00, 0.00, 0.00, 0.00, '', '', '', '', ''),
(0, 0, '', '', '', '', 35000.00, 0.00, 0.00, 0.00, '', '', '', 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, '', '', 0.00, 0.00, 0.00, 0.00, '', '', '', '', ''),
(0, 0, '', '', '', '', 35000.00, 0.00, 0.00, 0.00, '', '', '', 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, '', '', 0.00, 0.00, 0.00, 0.00, '', '', '', '', ''),
(0, 0, '', '', '', '', 35000.00, 0.00, 0.00, 0.00, '', '', '', 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, '', '', 0.00, 0.00, 0.00, 0.00, '', '', '', '', ''),
(0, 0, '', '', '', '', 35000.00, 0.00, 0.00, 0.00, '', '', '', 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, '', '', 0.00, 0.00, 0.00, 0.00, '', '', '', '', ''),
(0, 0, '', '', '', '', 35000.00, 0.00, 0.00, 0.00, '', '', '', 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, '', '', 0.00, 0.00, 0.00, 0.00, '', '', '', '', ''),
(0, 0, '', '', '', '', 40000.00, 0.00, 0.00, 0.00, '', '', '', 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, '', '', 0.00, 0.00, 0.00, 0.00, '', '', '', '', ''),
(0, 0, '', '', '', '', 35000.00, 0.00, 0.00, 0.00, '', '', '', 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, '', '', 0.00, 0.00, 0.00, 0.00, '', '', '', '', ''),
(0, 0, '', '', '', '', 35000.00, 0.00, 0.00, 0.00, '', '', '', 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, '', '', 0.00, 0.00, 0.00, 0.00, '', '', '', '', ''),
(0, 0, '', '', '', '', 35000.00, 0.00, 0.00, 0.00, '', '', '', 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, '', '', 0.00, 0.00, 0.00, 0.00, '', '', '', '', ''),
(0, 0, '', '', '', '', 35000.00, 0.00, 0.00, 0.00, '', '', '', 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, '', '', 0.00, 0.00, 0.00, 0.00, '', '', '', '', ''),
(0, 0, '', '', '', '', 35000.00, 0.00, 0.00, 0.00, '', '', '', 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, '', '', 0.00, 0.00, 0.00, 0.00, '', '', '', '', ''),
(0, 0, '', '', '', '', 35000.00, 0.00, 0.00, 0.00, '', '', '', 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, '', '', 0.00, 0.00, 0.00, 0.00, '', '', '', '', ''),
(0, 0, '', '', '', '', 35000.00, 0.00, 0.00, 0.00, '', '', '', 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, '', '', 0.00, 0.00, 0.00, 0.00, '', '', '', '', ''),
(0, 0, '', '', '', '', 35000.00, 0.00, 0.00, 0.00, '', '', '', 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, '', '', 0.00, 0.00, 0.00, 0.00, '', '', '', '', ''),
(0, 0, '', '', '', '', 35000.00, 0.00, 0.00, 0.00, '', '', '', 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, '', '', 0.00, 0.00, 0.00, 0.00, '', '', '', '', ''),
(0, 0, '', '', '', '', 35000.00, 0.00, 0.00, 0.00, '', '', '', 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, '', '', 0.00, 0.00, 0.00, 0.00, '', '', '', '', ''),
(0, 0, '', '', '', '', 35000.00, 0.00, 0.00, 0.00, '', '', '', 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, '', '', 0.00, 0.00, 0.00, 0.00, '', '', '', '', ''),
(0, 0, '', '', '', '', 40000.00, 0.00, 0.00, 0.00, '', '', '', 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, '', '', 0.00, 0.00, 0.00, 0.00, '', '', '', '', ''),
(0, 0, '', '', '', '', 35000.00, 0.00, 0.00, 0.00, '', '', '', 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, '', '', 0.00, 0.00, 0.00, 0.00, '', '', '', '', ''),
(0, 0, '', '', '', '', 35000.00, 0.00, 0.00, 0.00, '', '', '', 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, '', '', 0.00, 0.00, 0.00, 0.00, '', '', '', '', ''),
(0, 0, '', '', '', '', 35000.00, 0.00, 0.00, 0.00, '', '', '', 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, '', '', 0.00, 0.00, 0.00, 0.00, '', '', '', '', ''),
(0, 0, '', '', '', '', 35000.00, 0.00, 0.00, 0.00, '', '', '', 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, '', '', 0.00, 0.00, 0.00, 0.00, '', '', '', '', ''),
(0, 0, '', '', '', '', 35000.00, 0.00, 0.00, 0.00, '', '', '', 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, '', '', 0.00, 0.00, 0.00, 0.00, '', '', '', '', ''),
(0, 0, '', '', '', '', 35000.00, 0.00, 0.00, 0.00, '', '', '', 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, '', '', 0.00, 0.00, 0.00, 0.00, '', '', '', '', ''),
(0, 0, '', '', '', '', 35000.00, 0.00, 0.00, 0.00, '', '', '', 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, '', '', 0.00, 0.00, 0.00, 0.00, '', '', '', '', ''),
(0, 0, '', '', '', '', 35000.00, 0.00, 0.00, 0.00, '', '', '', 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, '', '', 0.00, 0.00, 0.00, 0.00, '', '', '', '', ''),
(0, 0, '', '', '', '', 35000.00, 0.00, 0.00, 0.00, '', '', '', 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, '', '', 0.00, 0.00, 0.00, 0.00, '', '', '', '', ''),
(0, 0, '', '', '', '', 35000.00, 0.00, 0.00, 0.00, '', '', '', 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, '', '', 0.00, 0.00, 0.00, 0.00, '', '', '', '', ''),
(0, 0, '', '', '', '', 35000.00, 0.00, 0.00, 0.00, '', '', '', 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, '', '', 0.00, 0.00, 0.00, 0.00, '', '', '', '', ''),
(0, 0, '', '', '', '', 40000.00, 0.00, 0.00, 0.00, '', '', '', 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, '', '', 0.00, 0.00, 0.00, 0.00, '', '', '', '', ''),
(0, 0, '', '', '', '', 35000.00, 0.00, 0.00, 0.00, '', '', '', 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, '', '', 0.00, 0.00, 0.00, 0.00, '', '', '', '', ''),
(0, 0, '', '', '', '', 20000.00, 0.00, 0.00, 0.00, '', '', '', 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, '', '', 0.00, 0.00, 0.00, 0.00, '', '', '', '', ''),
(0, 0, '', '', '', '', 20000.00, 0.00, 0.00, 0.00, '', '', '', 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, '', '', 0.00, 0.00, 0.00, 0.00, '', '', '', '', ''),
(0, 0, '', '', '', '', 35000.00, 0.00, 0.00, 0.00, '', '', '', 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, '', '', 0.00, 0.00, 0.00, 0.00, '', '', '', '', ''),
(0, 0, '', '', '', '', 35000.00, 0.00, 0.00, 0.00, '', '', '', 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, '', '', 0.00, 0.00, 0.00, 0.00, '', '', '', '', ''),
(0, 0, '', '', '', '', 35000.00, 0.00, 0.00, 0.00, '', '', '', 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, '', '', 0.00, 0.00, 0.00, 0.00, '', '', '', '', ''),
(0, 0, '', '', '', '', 35000.00, 0.00, 0.00, 0.00, '', '', '', 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, '', '', 0.00, 0.00, 0.00, 0.00, '', '', '', '', ''),
(0, 0, '', '', '', '', 40000.00, 0.00, 0.00, 0.00, '', '', '', 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, '', '', 0.00, 0.00, 0.00, 0.00, '', '', '', '', ''),
(0, 0, '', '', '', '', 40000.00, 0.00, 0.00, 0.00, '', '', '', 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, '', '', 0.00, 0.00, 0.00, 0.00, '', '', '', '', ''),
(0, 0, '', '', '', '', 40000.00, 0.00, 0.00, 0.00, '', '', '', 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, '', '', 0.00, 0.00, 0.00, 0.00, '', '', '', '', ''),
(0, 0, '', '', '', '', 40000.00, 0.00, 0.00, 0.00, '', '', '', 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, '', '', 0.00, 0.00, 0.00, 0.00, '', '', '', '', ''),
(0, 0, '', '', '', '', 40000.00, 0.00, 0.00, 0.00, '', '', '', 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, '', '', 0.00, 0.00, 0.00, 0.00, '', '', '', '', ''),
(0, 0, '', '', '', '', 40000.00, 0.00, 0.00, 0.00, '', '', '', 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, '', '', 0.00, 0.00, 0.00, 0.00, '', '', '', '', ''),
(0, 0, '', '', '', '', 40000.00, 0.00, 0.00, 0.00, '', '', '', 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, '', '', 0.00, 0.00, 0.00, 0.00, '', '', '', '', ''),
(0, 0, '', '', '', '', 40000.00, 0.00, 0.00, 0.00, '', '', '', 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, '', '', 0.00, 0.00, 0.00, 0.00, '', '', '', '', ''),
(0, 0, '', '', '', '', 35000.00, 0.00, 0.00, 0.00, '', '', '', 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, '', '', 0.00, 0.00, 0.00, 0.00, '', '', '', '', ''),
(0, 0, '', '', '', '', 40000.00, 0.00, 0.00, 0.00, '', '', '', 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, '', '', 0.00, 0.00, 0.00, 0.00, '', '', '', '', ''),
(0, 0, '', '', '', '', 40000.00, 0.00, 0.00, 0.00, '', '', '', 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, '', '', 0.00, 0.00, 0.00, 0.00, '', '', '', '', ''),
(0, 0, '', '', '', '', 40000.00, 0.00, 0.00, 0.00, '', '', '', 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, '', '', 0.00, 0.00, 0.00, 0.00, '', '', '', '', ''),
(0, 0, '', '', '', '', 35000.00, 0.00, 0.00, 0.00, '', '', '', 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, '', '', 0.00, 0.00, 0.00, 0.00, '', '', '', '', ''),
(0, 0, '', '', '', '', 35000.00, 0.00, 0.00, 0.00, '', '', '', 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, '', '', 0.00, 0.00, 0.00, 0.00, '', '', '', '', ''),
(0, 0, '', '', '', '', 35000.00, 0.00, 0.00, 0.00, '', '', '', 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, '', '', 0.00, 0.00, 0.00, 0.00, '', '', '', '', '');

-- --------------------------------------------------------

--
-- Table structure for table `bank_remittance`
--

CREATE TABLE `bank_remittance` (
  `bank_id` int(10) NOT NULL,
  `emp_id` varchar(100) NOT NULL,
  `emp_name` varchar(100) NOT NULL,
  `bank_acc` varchar(100) NOT NULL,
  `net_pay` decimal(65,2) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_general_ci;

-- --------------------------------------------------------

--
-- Table structure for table `bonus`
--

CREATE TABLE `bonus` (
  `id` int(10) NOT NULL,
  `bonus` varchar(100) NOT NULL,
  `date` varchar(10) NOT NULL,
  `date_from` varchar(10) NOT NULL,
  `date_to` varchar(10) NOT NULL,
  `period` int(1) NOT NULL,
  `emp_name` varchar(100) NOT NULL,
  `salary` decimal(65,2) NOT NULL,
  `department` varchar(100) NOT NULL,
  `date_hired` varchar(100) NOT NULL,
  `work_mos` decimal(65,2) NOT NULL,
  `gross_amnt` decimal(65,2) NOT NULL,
  `wtax` decimal(65,2) NOT NULL,
  `deduction` decimal(65,2) NOT NULL,
  `net_pay` decimal(65,2) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_general_ci;

-- --------------------------------------------------------

--
-- Table structure for table `bonus_details`
--

CREATE TABLE `bonus_details` (
  `bonus_id` int(100) NOT NULL,
  `bonus` varchar(100) NOT NULL,
  `yyyymm` varchar(100) NOT NULL,
  `date_period` varchar(100) NOT NULL,
  `emp_id` varchar(100) NOT NULL,
  `emp_no` varchar(100) NOT NULL,
  `salary_id` int(100) NOT NULL,
  `work_mos` int(10) NOT NULL,
  `gross_amnt` decimal(65,2) NOT NULL,
  `wtax` decimal(65,2) NOT NULL,
  `deduction` decimal(65,2) NOT NULL,
  `netpay` decimal(65,2) NOT NULL,
  `year_end` varchar(100) NOT NULL DEFAULT 'False'
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_general_ci;

-- --------------------------------------------------------

--
-- Table structure for table `comp_details`
--

CREATE TABLE `comp_details` (
  `comp_name` varchar(100) NOT NULL,
  `address` varchar(100) NOT NULL,
  `tphone_num` varchar(100) NOT NULL,
  `con_person` varchar(100) NOT NULL,
  `weblink` varchar(100) NOT NULL,
  `tin` varchar(100) NOT NULL,
  `sss` varchar(100) NOT NULL,
  `philhealth` varchar(100) NOT NULL,
  `HDMF` varchar(100) NOT NULL,
  `maker` varchar(100) NOT NULL,
  `checker` varchar(100) NOT NULL,
  `approver` varchar(100) NOT NULL,
  `rate` int(11) NOT NULL,
  `monthly` tinyint(1) NOT NULL,
  `semi_monthly` tinyint(1) NOT NULL,
  `weekly` tinyint(1) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_general_ci;

--
-- Dumping data for table `comp_details`
--

INSERT INTO `comp_details` (`comp_name`, `address`, `tphone_num`, `con_person`, `weblink`, `tin`, `sss`, `philhealth`, `HDMF`, `maker`, `checker`, `approver`, `rate`, `monthly`, `semi_monthly`, `weekly`) VALUES
('ACCENTURE INC15', 'Unit 11 & 12, Oneconvio Business Center, 932 Aurora Blvd, cor 20th Ave, Quezon City', '0927-228-0356', 'Juan Dela Cruz', 'www.bytesfusionlink.com', '123-456-789-000', '123-456-789-010', '123-456-789-020', '123-456-789-030', 'Angelo C. Fernandez', 'Francisco Valentin Rualo', 'Juan Dela Cruz', 3, 1, 1, 1);

-- --------------------------------------------------------

--
-- Table structure for table `department`
--

CREATE TABLE `department` (
  `dep_id` int(10) NOT NULL,
  `dep_code` varchar(100) NOT NULL,
  `dep_desc` varchar(100) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_general_ci;

--
-- Dumping data for table `department`
--

INSERT INTO `department` (`dep_id`, `dep_code`, `dep_desc`) VALUES
(1, 'SA', 'Sales'),
(2, 'ACCTNG', 'Accounting'),
(3, 'FIN', 'Finance'),
(9, 'HA', 'HR & Admin'),
(10, 'PS', 'Purchasing'),
(11, 'IT', 'Information Technology'),
(12, 'HRM', 'Prod');

-- --------------------------------------------------------

--
-- Table structure for table `employee`
--

CREATE TABLE `employee` (
  `emp_no` int(11) NOT NULL,
  `emp_id` varchar(100) NOT NULL,
  `lname` varchar(100) NOT NULL,
  `fname` varchar(100) NOT NULL,
  `mname` varchar(100) NOT NULL,
  `email` varchar(100) NOT NULL,
  `gender` varchar(10) NOT NULL,
  `birthdate` varchar(100) NOT NULL,
  `cp_email` varchar(100) NOT NULL,
  `tel_no` varchar(100) NOT NULL,
  `mothers_name` varchar(100) NOT NULL,
  `mothers_occ` varchar(100) NOT NULL,
  `fathers_name` varchar(100) NOT NULL,
  `fathers_occ` varchar(100) NOT NULL,
  `address` varchar(100) NOT NULL,
  `home_address` varchar(100) NOT NULL,
  `contact_no` varchar(11) NOT NULL,
  `image` longblob NOT NULL,
  `pob` varchar(100) NOT NULL,
  `status_type` varchar(100) NOT NULL,
  `nationality` varchar(100) NOT NULL,
  `spouse_name` varchar(100) NOT NULL,
  `religion` varchar(100) NOT NULL,
  `elementary` varchar(100) NOT NULL,
  `high_school` varchar(100) NOT NULL,
  `college` varchar(100) NOT NULL,
  `course` varchar(100) NOT NULL,
  `work_experience` varchar(1000) NOT NULL,
  `primary_con` varchar(100) NOT NULL,
  `contact_num` varchar(11) NOT NULL,
  `relationship` varchar(100) NOT NULL,
  `secondary_con` varchar(100) NOT NULL,
  `contact_num2` varchar(11) NOT NULL,
  `relationship2` varchar(100) NOT NULL,
  `status` varchar(100) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1 COLLATE=latin1_swedish_ci;

--
-- Dumping data for table `employee`
--

INSERT INTO `employee` (`emp_no`, `emp_id`, `lname`, `fname`, `mname`, `email`, `gender`, `birthdate`, `cp_email`, `tel_no`, `mothers_name`, `mothers_occ`, `fathers_name`, `fathers_occ`, `address`, `home_address`, `contact_no`, `image`, `pob`, `status_type`, `nationality`, `spouse_name`, `religion`, `elementary`, `high_school`, `college`, `course`, `work_experience`, `primary_con`, `contact_num`, `relationship`, `secondary_con`, `contact_num2`, `relationship2`, `status`) VALUES
(196, '000-000-001', 'Dela Cruzs', 'Juan', 'Garcia', 'delacruzjuan@gmail.com', 'Male', '5/16/2024', 'delacruzjuan@gmail.com', '', '', '', '', '', '3135 Esperanza St. Brgy Kasilawan Makati City', '', '09382873827', 0xffd8ffe000104a46494600010101000000000000ffdb004300090607121212151212121616151616161619151716151a1818181517181818151615181d2820181a251b151721312125292b2e2e2e171f3338332d37282d2e2bffdb0043010a0a0a0e0d0e1a10101a2d1f1d1e2b2d2d2d2b2d2b2d2d2d2d2b2d2d2b2d2d2d2d2b2d2d2d2d2d2d2d2d2d2d2d2d2d2d2d2d2d2d2d2b2d2b2d2d2d2b2d2d2b38ffc000110800e100e103012200021101031101ffc4001c0001000105010100000000000000000000000502030406070108ffc4003e10000201020207050604030803000000000001020311042105061231415161718191a1b113223252c1f00742d1e114627215233334738292b22453f1ffc400190101010101010100000000000000000000000102030405ffc4002111010003000202030101010000000000000001021103211231044151328122ffda000c03010002110311003f00ee2000000000000000000000000000000000000000000000000000000000000000000000000000000000c4d25a4a8e1e0ea56a91a715c64eddc96f6fa239de9bfc5fa51728e168ba96dd39bd98f6a8acdaedb16226526621d3ca25562b7c92ed68f9db4d7e2063712ed2ace31f9295e0bbf65ddf7b66b7571d36f393bbe2dc9fd4d78b3e4faba334f734ca8f94a9e92ab0ce35271ec9b5f5b92184d6fc6d396d4713593eb524d7836d3ef43c0f27d3a0e21a17f16715056aaa159755b13f18e4fc0e87abbaff0083c55a2e5eca6f2d9a96b36f8467b9be99324d6562d0db00065a0000000000000000000000000000000000000d735c75be868fa779bdbaafe0a49d9bfe697cb1ea666b0eb1e1f051daaf3b369b8c167295ac9ecae3bd1f36eb6eb14f17889d69bbb792e4a2af64ba246ab0cccaad64d65ad8aab2ab5a6db6f25c22be58afca883a9887ccb126d8d835a910c9a72b71eeb97609bcdbdfc0c4a71b59b2fcaa64f3c8698ae5453debd2fe173d824bbbefeec514ea27c17df42a9d9e7f45ea34c5d4d2e3df9fa9910af259dcc0577ffdf2ccae126bf42a63a56a5fe22d5c2c552ab175a9656f79edc3a46f75b3fcbbba9d8f42e98a38ba6aad195e2f83c9a7ca4b81f2c42af4fa336dd49d669e16ac6519b49b4a707f0ca2df9324d756271f44828a53da8a97349f8959cdb00000000000000000000000000000f24ed9b3d22b5ab1ff00c3e12bd5f969cadfd4d5a3e6d01f3f6be6b1cf1989a9372f7139469a5b9453c9a5cda577d5f61a73bb6656326db664e86c1edcad6ef3a5a7219a57ca71668601cad64ccfa5a066f83f236ed1da2d2b3360a1854b723c96e69fa7d1a7c68ceda450d4d9ca37caf6ddfa925435154924dd8de70f4b2332852311c96959e2a47d3428fe1f47e6fbe87b5750a16b2799d1540b53a46a6d6fd66295fc72dc5ea34a3f0b57e9c484c4e81a9076b1d92a46c476370d196f48472da1678292e3d88c3ce3bd15e12566b35ebe36373d67d1f1d8725bd5ae692bdd935bb2566bc4f4f1dfca1e2e6e3f097d1df8758ef6d81a4dbbb85e9bcd3b6cbcb77f2d8d98e5ff008278bbc6bd3b7c92dfc6cd3cbb91d405bdb15f40008a0000000000000000000000001aa7e2854b68daf95efb0b8e579c737636b357fc4dbff66e22dca1e1ed2372c7b49f4f9a2b47327b55e369321aa53cfbf3364d58a793977780e5fe5bf8f1b786db817c095a246e1125bdd893a325ccf0e3eaec33e848cea4cc3a267538bea6ab0e56956e45ba932b922d4e2698863d431311132aa348b156a47e65e2671d35ace9e8ff007734739ab177ea9d8ea3a5e9269ae0ee739c651d99b5d7ed9df83f1e5f951ea5d33f037e3c465f9619f0f8a5f7dc75c39c7e0a60d2c3d5aad7bd2a9b3b5d229656ed6fc4e8e76b7b792be80019680000000000000000000000002135d70ceae071108abb74dd9755677eeb5c9b20717acd4a339d3d973d9bc65671ec6b65f01b8b159b750f99f1717768d9a8d4587a4afbed64b9b3174a60a3fc6ce9c2fb1ed1eca92b3d9bed67dc4c62308a4d5d6511cb31d3a7056637f513fc4d6a99a5297a22ff00fe6535b4a3249676d9cbc3ea6753d2d0c3b508c1ce6f7463cdf06ca96bed65b71787a4b65db61d496d3b6d6d6cfb9676d9b6f5bd5ae62367d43a5a223dd97742eb8568b51a94d35cf34fc0dff01a55548a92e26913ab4ab371953746b2df17e393e2b324744ce517639dadfe3b569f7badb5e2d235dd3bad5ec9b8c61b4fb72f433e69b89038cc3c536e6b2f53316c59a6fa40d7d67c4d56d461967f95bf32a9e22b35fde539a76c9eccf3ee6b3326aeb54684a2a1878a524ed3a92d88fba9f15193ced6596f66453d72da8ed55c3ca11db70db8c94e2a4b83b25e275ef371c32373c91f83d2eeea9d4bdb349bbdfa2fa11ba7e8da717cd7a5bf5362c461215ad28d9a7ef2688dd64a3684256bda56f15fb178e63c8e5acf84ebac7e18e0fd9e029dd59cdca7bb3cdd979236c343c16b0d4a7469d38c631d88462a115b4ed14966d9b8e8ac67b6a51a96b36b35d51af2899719e3b5636596002b00000000000000000000000001cbb4c68c8caa6d66a4e53f7964d77f81d44d3758b09b1557cb3da6bb5abdbc8e7c91d3d1f1ed969733c6e1bff2eedddc69b4df3bbc9f6dafe049d1c06dc4691a2bdb65c62b3ec6f2f325b00b2462d3d43d148cb4a330fa123196d34df55bd1970d0f87dadb745ca57bddc5ddb5c5f026a1045d5031132dcc42331583551c5ca0ef1778b6eef7ddf73e3da78a928caf6b6649d4764476d5e44b4eb55867d792d94626230ca6acd65bff006ec326b2c90a0c9bda6748cc568d854b2a94b6d2dcde76bf6bbf05e051fd8d4b63d9c69ecc73f7774537bdd89f54ee15237b2ce435fc3e8854de4ddb9119ac386bd29ae5697fc5dfd0dbb11146bfa5acd49738bf42d7fa66ff00cca5f014d5a2f7bbabbed372d5c8b50972dacbebf4351c0c53b462b7b8a4badd3fa1bfe168a8454570f5e26f8e3bd73f916eb174007578c00000000000000000000000022b59309ed284be687be9f2b6ff002254c7d2145ce9ce31dee2d212b59c971ed29071aa9b7bd6ee4f2be66668dafbfefc8ccd65d0d5547da3a724a1bddb249f1bf6d8d770988d991c6d1d3db4b772dca8b329344361713732dd7b9cdd6616349625b6a11cafc7d6c5185a2a324ae63e91a1b695a5b328bba76f1444bd1b8984bda53af29679c27ef47b9bce3e362c46acdb21b9e3231fcac8fab782da57b72212b2c555b4769d25c64b65beebdd7919786d1f557bb52bce70bfe6b6d3e975c0b30cc4fd2770b5ef6b9952922365cd702a55f233ab8636ad8d6f173bdfb1f99218faf7c8c6c2e0e7527b308b94b7d96f76ccdd7db17f4db755f07fdec6f9ec46f77cd65f537321340e8d9c1edcd5b2b25c73e7626ceb58c878f9676c000d3980000000000000000000000000003174a617dad1a94fe6835df6cbcce29528b4da7c1dadd87753946b9e03d8e267f2cfdf5dfbfcee66de9d78a72711d809b48c9ad8cd956f33030d3b3b73337f8653b46496cf9b5d4f3fdbdb3b9d28a1a423276be7c7efef8927879c3739ab9afd7d5da7095e129d9e7b2e6daf179a25307a3e9e578c97fb9bf537bf84537da5bdd56f796ee662e271105f9beee5c5a3a8a5c7fe445e2345d37f92cb87bd26fd4a78571769e914db4a59f149afbcfe866c65757dc47d2d05469ada8c6d2cb3dedf6b7bcce9bd98d8c5a619ac4c311c2eee6cfa8b86fef2a54e5151ef93fd8d65bc8e89aaf81f654237f8a5ef3efdcbc0e9c71f6e3cf6eb12e003abca000000000000000000000000000000001a6fe2560b6a8c2aadf0959f64bf75e66e4681f88bac892785859df29bebbd4575cb319ab1392d16956cc97c355bd8d4a5896992980c7753cf7abdf4b3694b69667af057dcdaf4f02d60eadd19b4eb25d0cc37330c5a7a324b7cd97e18551ede6cc955cb356ba2ccb3b0b4f7bb9878aa8914e3b18a395c85c5639b2446acce363d5ea1edb11086f57bbec8e6cea6718d01a66586a91a895db766b9aded791d8b0b888d484671778c926bbcf4d6321e1e59db2e800ae600000000000000000000000000792924aedd92e2c0f41af6375c30f07b31da9bfe55978b353d35ad956b3718bd88728bcdf6cb8972535bce9cd334e8529cb6e3b697bb1babb93dd972e3dc710d61c4372a726eee5573efbb667e2714dbb37c2e46e96a2e4a9749bffab353d415eed0b78ac1dd5d182a3289b261e178e65bab81badc793c9f43c58383d2d28e4c96a1a697321eae8f6b714c306c7464a765a5d73fbf131ebe99bacb7f4fbed23e38266451c00e8c5875a5277ded99786c0b79b24b09a352cd9993a6a2ae34c6b388ff00334a9f4949f925eaceb3a838adaa0e0dabc24ecaf9d9abeee57b9cbb054b6f113aaf7463b2bbdfec8cea38994738c9a69e4d3b33d348dabc3cd39776b073dd19adb5a1653f7e3d77f74bf5362c36b761e5652528f6abaf2fd078cb3ad801452a8a494a2d34f34d6e65645000000000000016ebd68c22e53928c566db7648d2f4cebd6f8e1e3fef97ac63faf8162352671bb54a8a2af26925c5bb220f19adb86a6ed794dff2c72f1764739c6692ab5bde9d494b8d9b76ee5b91636cd78a6b76c7ebd26ad460d3e73b7924cd5f48e96af55fbf5252e97cbc16446bdfd4af3e9f7d83a83da9936aecc58cb36b9999285f896251b3c82e30b1ef6529f2dfd8ee65422a708be4fd5166ba52567b9a7eacaf43cd6cb8df735e4ec49ee16b3968967d0c3f032a853e664d1a1e05cf64789f49895b065b8e157225a112af668222de19722ed3a1c9121b07becca8b0a9596645694aced6449e2aa644557a5bafbe4ecbeac477384ce46cace1e0a14eef8bbb31154bc92eb77d8b365cd253bb508ee5e9b8a30d4b3bbe892e576b8f3b1ed88c87cdb4ecea424f71539874fbbd3c0a654df2f0cff72e9892d1da6eb51b6c4da5cb7af066d3a3b5ce0ffc58ecf58ddaf0e0686ecf2f2fd99eed8c894ee1d5f03a62856ca15137c9e4fc1ef33ce36a64de87d66ad46c9bdb87cb27bbb1f033355d749043e8ad63a15f24f625f2cb8f63dcc9832d00003916b36b1cf1155abda9a6d421d9c5f36fc88a8c1efdc63e2d595f8e4fd5332a84eebcbbcdb2ae10cb7fdfaf995a82ededbbf53c5b82615e9454762b3c9c6e80f2f7459a899561e5c18aaac4186deeeff5679a2a0d549c79abafbed3d9f0eff523f4a69458684aaf14928af9a4ef65f57d101be687a9b69a7f95f93ddf7d0cf9d039d6a06b4caa5d556bdac5b6f72538377cb85d3fa3e674ea4e334a51774feedd1f43cfc95c97b38afb0c354ac5e544c9d82b8c0e78ebac4f645152266ca25a944b89a8d9d1e2c8158b8d494a719269394125c365d9a6b83e3de656b5e9b8d2a551473946126dadc9add1eadcacbbce61aa38f9c31128d47775b3bf0f68af65d2e9b8f81db8ab9dbcfcd7dff986eb4e2db6df1c8c8c3ef8aeadf7455bd5f916653caeaf9991835be5cfdd5d8b7f9b67679998d9edca5be05514155357c996dd35c1b5df979dcaa52b2b9e4170029f66f9af06bcd3fa14cafcbc2cff00465f2dc9efee02dc65cbefb8da7406b5ca9b54eb3da873bfbd1ebd5769adb82e3f7916a31cd3eff0ddea5f6ce3aeff0068d1ff00db0ff947f5072af6bd9e009e2ba86a9ef4a57e6a3e28f3093f7b679af3894279c973a897916ea4b66719755e797d48a959145cbab32d4d145772a458da2f419063d68d9dd15ed6d2b97248c6a91d9796e606255a8a379369249b6de492e6d9a5e3eabc5d45b29fb28651bf1e736b9bf436fada0e55bfc5a89c784229ecf6be6cbf86d0d0a7b91469f1d1f2a2d54a6ed38bba7d573e86f3ab7ac0eac36a0dc26b29c2f7b3ec7be2f83ee30f158320f19a1ea426aad16e3259a6bd1ae2ba322ee3abe8ed3319fbb3b465cff2bfd192ad9c8e3acd2825edf0f515f2dba76945bec934d7667de6c1a1b5f30f16a9d59ca09eef6b094767fdff000b5dff00a1cadc7f70ef4e5fa96f2f2c9103a7b4bec5e9d3f89e4df2e8baf5e1dbbb2b49e91b2b41a6dabdd704f734f9be1e26b78c8e572529bdcaf2f2675081d27efc5c2f9c9dbb967dd9d886afa25a5759359a7d566993b421b751f4f5bddfe9dc4cbc3c766d2591d9e643e1eabace36566e2a4ff95b49b7ddc3b89d841455970562c6070b1a5176def8bdfd2e6542251ea8955cf1b0078f33d8713c422c0a9c8b7c3b5945577697de45c7c00f6abc9f8789676fdfb74f46bf56555e79a5dafc32f5663cdfbcba492f18dbea058fe2018b6056755cbe27feac7e85ac6fdf88065a84bd2dcbb8f24780a2d4cbb4c020aa45ac47c2001730ff000a2b99e802c543c97c20011d5ffc09ff0052f523f4dff968f6fd002899d56ff2943fd287a23331fbbbfea78008cd01f0a2627c0f401ec8b80006780101ee3c40145a87c7dc5d88006354f8dff4affb328a9f12fea800047800ac3fffd9, 'Makati City', 'Single', 'Filipino', 'n/a', 'Roman Catholic', 'Maximo', '', '', '', '', '', '', '', '', '', '', 'Active');
INSERT INTO `employee` (`emp_no`, `emp_id`, `lname`, `fname`, `mname`, `email`, `gender`, `birthdate`, `cp_email`, `tel_no`, `mothers_name`, `mothers_occ`, `fathers_name`, `fathers_occ`, `address`, `home_address`, `contact_no`, `image`, `pob`, `status_type`, `nationality`, `spouse_name`, `religion`, `elementary`, `high_school`, `college`, `course`, `work_experience`, `primary_con`, `contact_num`, `relationship`, `secondary_con`, `contact_num2`, `relationship2`, `status`) VALUES
(2, '000-000-002', 'Fernandez', 'Angelo', 'Canatoy', 'angelo.fernandez@gmail.com', 'Male', '5/16/2024', 'angelo.fernandez@company.com', '8252295', '', '', '', '', 'Unit 3 Paves Residences Sta Ana Manila', '', '09290453840', 0xffd8ffe000104a46494600010101000000000000ffdb0043000503040404030504040405050506070c08070707070f0b0b090c110f1212110f111113161c1713141a1511111821181a1d1d1f1f1f13172224221e241c1e1f1effdb0043010505050706070e08080e1e1411141e1e1e1e1e1e1e1e1e1e1e1e1e1e1e1e1e1e1e1e1e1e1e1e1e1e1e1e1e1e1e1e1e1e1e1e1e1e1e1e1e1e1e1e1e1e1e1e1e1effc00011080200020003011100021101031101ffc4001d000100020203010100000000000000000000030402050106070809ffc40044100001030303020404030703020405050001000203041121051231064113225161073271811491a108234252b1c1d11562e172f016243334538292a2f143445463b2ffc4001a010101010101010100000000000000000000010203040506ffc40029110101000202030100020202030003000000010211032104123141135105223261142342153371ffda000c03010002110311003f00fb2d01010101010101010101010101010101010101010101010101010104553530d3b5ae9a46b1ae76d05c6c2e83264b1b896b5ed247605046faca564a6274f1891a3716ee1703d6c8359a8754e854262151aa52b5d21f2b7c41723d6c82487a97429633247a9d339839787f97f35362fd2d5d3555336a69e68e485c2ed7b5d7047d551c9a9a71933c5ffd6104ad7070b82083c141ca020202020202020202020202020202020202020202020202020202020202020202020202020202020202020202020202020202020d4ebfaed2e93039d207c9286dc3182ff4bfa20f1df881f1edfa040e645a216cd6bb4996f7f7e2ddb82a7647876bff00b5275ccd3bc69d341045c6d31027f30b5aaba79feb3f18fae752a874951ac553da482d8dd21da2de82eacc4d695b47f8b1d6f4351bff00d6ab261b8b8b65713bbefcdbd92e3d0975bf89bd475fa836ae1d5b5181e23d99aa37efdfd3d94f5146a3aeb5eaba52caad4659a47003c591d7706b6f617f44d2c8aba6758f50e9d0eca4d56a5b1976fd824c129ea8d8d57c4deae9e014536bb56da58859913252c60fa016e54f5d2e9941f117a89b03a366a3396def7738e1d7b8e5346a3b8f4afed05d77a34b0b25d41f5b0b08bc6e7fcc15f54d3df3e1efed37a06b1534ba7eb1433d04cf3b5f2b9db81f7b05934f6ad3baa747d4a31269d51f8a6936bb0707defc2a8dd83717b590101010101010101010101010101010101010101010101010101010101010101010101010101010101010101010101010473cf141119269191b1bcb9c6c020e87d63f167a4743a4a9f0b56a3aba9858e2e63276d9847671f5bf6c9434f92fe247c75d77579e711be9a38e4ef0126ff7279fa849174f22d7baa35bd669c41595f2cb06fdcd8c9c03c616a4d2e9a87c66369dc6eef4f454ed1b23126e26e6dc9406c8d6c81b7be40e55361b389737363f4ca8312fc581cdb07d15d031ae9002081639c222561eefda719b72a096270c8644e2383dd059630b99e601a00fba9f1538f0fc8db8da31dd0faed3a1f5c7546872452691aed6d3bd80359fbd24068e058f659d0f7ef84bfb48eaf31669dd5669a794b83193066d73beb6efee89a7d23d2fd4da5ebd4c24a3a8678c079e12eb39bf6feea4a8ddb1c1c2e3e8a8e501010101010101010101010101010101010101010101010101010101010101010101010101010101010101047354430b37cd2c71b6f6dce7001079c7c43f8bbd3dd3a65a5a3d4b4ea9ae8da7730ce0869b12063e8a0f95be227c7aea8d51d514da3ea757474f5199835e2c1ddc37d1a9256a47886a3a94f533bdee79739c72ebf2b72486d55a1cf779dd703b0ee553e24780c7d89cff004411e5ceb8c34724aa33b810eddc45c137ee82189ad0e0725070ebf8e5a0e2d805119b2ef04cac0d7371ff0057b20e5f6dc5af7580f7c22b28844dcb46e2e3dd0d2c324a90c0c8dad03b8170a11cb5936ef3b4341f5b944acc40481e504dff0087948a91af747f3b5e6dfa20999525aef101c0fb10b366ce9e83d1df15ba87447b364fe3c6d000de7cecb7f2bb90b3ad27d7d37f07be3ae9bae99a9f5673e29ae1c4bad81ea3391ec9f0d3dd696786a6064f048d9237805ae69b8215448808080808080808080808080808080808080808080808080808080808080808080808080808080808083cbbe287c6be97e8c35b465d255ea54a2cf898df2b4fb9f5f60a6d647c6bf143e2df5375aeb12544d5b514f46c7930d3326218c1f416cab27eaf4f3e9ea6799e6596476e772b5a84557c8482d6f65742bb0020816bfaf6fa2a2781a184b8f60a1a441cd7484b81713ee8326dc308e5c7f20831601b89e4772551c06b9cff002ddc473eea09194cf7e5c1ad3ee5089bf084f0770f4ba4a23928e4e5cd20a4a5612814efdae63c96e05f854670cb3020b5b7fa282ec664905dc0717c9ba233901b583037dc211c005ccb02c27dbba0c5c65d9b2db9ad5208ec6321cc75bd08e0aaabb495f514f52c92195d1bdb62d735dc596743e8ff0080bf1eb54a3d5a1a2eac94cda74acd8e9d91dded231bdc073ee79535a4b1f5be95a8d0eab4115769d5515552cec0f8e58dd76b87a8445a6f083940404040404040404040404040404040404040404040404040404040404040404040404040414f55d469b4da47545493b470d68bb9df4083c4be38fc59d43a7f48649494eea764f318a295d92d7004dc81f4e14a4edf17f54f526a3d41accda9ea552679a67b9ee36b02e3decb52371a16b8905ce3eeb48093702463b0422bbae6c1bf554674c1a5d6388d9fa9f55089016bf78370eb5ac92697ea360003458dec89a4c21738d83b07d90d2514ec00073fe8022ac45063737ca3d6d653604b23276fcdcdec8910be47bdc46ee390115c367903ade21b7a59212382e73da43c03df29b3e38f0c13604b5bec50d268888cd9ad36ee4a6d3495f213605c481c5d37b34c1cc8fe66817bfaa6d589682080483f546748c12304927b8238576ba66c9232d2c919b6f90e1d8a827a4aaaaa7797432b98fb676badb87a84a3d17e157c61ea3e8fd49d2b2b1d5148f7874d4d2f04fa8f43eb6e56743ec5f85df18341eb2d3e190834750f20398e70205efc9ec316b952547a531ed7b03d8e05a45c106e085519037e101010101010101010101010101010101010101010101010101010101010101010101010704802e4e1079975b758682fd6a9e8e9b55a5a899c0b1b136407ccdf981f43dfeca0f93ff697d766d43a84520aa8a5829c900c2ef26e3c9015c676b8c78a838683c765b698924ee1d9108c0d836e4dbf5418460be47340f95b654d278227362b8f28dd604faa8bd326c406ebbb162771f5522398a305ad6807dc81c2bf4a99a7c361681671fea91625a7636dbdcd2f1c0f750d150f782e3ef603b056444001792727dc29163311edb0e1c79ba0c482dbded73c5934388a273858fd4029a4d8e7784eb03bdfe9d82682492579bbcdbfdad16ba6844496df75c1f4ba6976e58f75ae6f6553691f234000db3c109a4d381386e5ed36054d2a573a291c5cc7b413d88c27ce8faae49b6d1820dc7b210325cee38772856fba4fa86bb42d4195ba7d4c90bdb6f95c6c7d88ee14b367d7d0bf0ebf686ac1534f4d288a10e7318e864dce6dc9b12cf41decb1bd269f46742f5b69dab8753bea62f15d23bc221d870bf1ea0ab2a3ba0208b8375410101010101010101010101010101010101010101010101010101010101010101009006507cf1fb40fc74a7e9e7cdd37a46d96a26004b3472f9a117371ec4dbed7527649b7cabd55d6f2d7ebcdd4b4f260708fc3b168e2e7f3faf2b5315d3aa57d74b5ae9249e573dcf24924e6eb5a5d2830f9cdf22e2e9b183f779f6f20a0bb4d03b05d602d62a52458f018d0f3724bb160384faaab235b159a243719b0561fae03ec0f94116cdca2fe3391e1b102c23243b09b4d318cef70603dee4fb651349dd51b5a1a31b5c05bfaff004516322f1392e27cc4dfedc2a9160f85044dbdb1e637fd1414db2978749616ec4f2aae99b2db016b41948fb04884b21166b08bff0013fdd5fc34c62612d2f0c37bf24607f95060d25b72e7106c6c6c90410966eb805cef757f11645dc0599f73d94839fde580bdcf1808ac641e5dbbc127b121136aad93c376416dbee1162773dae88e05c116f5fb28b3b5775f75c707949f1074ae6df3716c2ba467054491b839af20b4defdc2961b77ce97f89fd45a6be8ff00f3ef73a8a4dd13cd8b803c83ea383959f51f6a7ecf9d7f1f56e800c921f17b6f3971e091ed7bacc47ab8cad02020202020202020202020202020202020202020202020202020202020202020e8ff00187ada83a4ba3abaa4d4c1f8e746638212fc9791dfd05bba9b5936fce3ea3d526d435392a24792e9242e75cdfdfbade33a56ae47d9dbbdd518b5d7248c8be3dd5231c0bdb17183fd920b7a45217bdf248086333f75293f57cb1ad89d2bae403827ba9a542e91e23bdb1efdee9f0549641627c848e3d9582a39c36f981041cd87aa0c4938683801159c72ec6e3e6e7288c0ca6c72504b4938698c3bd727d0144fa92b672e0d6dfb6516230edde1c6d276b467dca8ab134e0176d0775ec3d954d3180b9efc9b8febee87ea79a6bb3c3648431a2ee77f61ee89a40e91b6e0dc8ee79f6fa22306546e3660b1e2e0607dd555da73b19e5dc49fe2511c863b70b026ddca7c18d44776dcd85b848aa0f21a4e5174e29a4607d9c6e0f22dc7b844892500fcb8b64159c6b5f889c6fe53dc2d32c002ddd73dae0aa919b5d67070f40a1a77bf863f11b5be88d561add32504424b844f1769bf37fc966e248fb17e03fc6d8fae4328aba16c356c680f2d07ce4df23dbb2cfc47b6aa0808080808080808080808080808080808080808080808080808080808082b6a7570d0d0cd553cac8a38d85c5cf7580b7ba0fcdff89dd575da8f536a2e92be7ac6baa5ef63deff00e12e38f71e8922fc74390ee717d96d55e43b8b736baa698c2e3b4ffb4e51225d9bc5c70eeca2b6b4c76d33a216b936bdf9b2966d58c92b76fef1f78d9601a3f88a7c14e67cf282fbb403cdce14dc598b9a6a47be32e6f9c0fe45cf2cfb75c70aa35503e298b5d7b119c5974c72dc62e3a60cb870bdc8b2a927f6c881916c93cdd252e2c00cf384b4912585c5aea6d74ce52d91cc3dc36c53d93d74ce2dc43b6dca7b2e8000c9fbe14858ca30e9080d0434f757d8b8f5b4b246c6c6db0dd6f7c7d5264ce903c17bbff4c177724d82bb349e9438b8b46c00fd82269b2f1238006b9cdddeb6ba9b3d55dee73da5c3739b7c59515a571b12e241faf090d2a3a6373e5bf6c9eca910b1c0bb758e3dd136d8020c21ae162dc8bfba8aad38b38db81fd1589518b8001e0f288c5a4871b59516607761f65163b1743f546abd29d454baa6993bd8e89f720fcbcf752cda3efcf801f12a1f883d352cef1b2b2964f0a4ff00fb31f30fd5611e96a8202020202020202020202020202020202020202020202020202020201c041f3d7ed5ff001021a3e979b43a496464ef96de231c2c6dc83eca7d58f87ab6405cfb138ff2ba454513c98007766e3eca8c1c6cd0e6e6c8302d3f33060f21116e82c23748ebf96fb428d48e5b33bc3bdf9ba9f1a88839d24818dbe1672bd378e3b6d74fd25f54e01ce706f17da480bcb972e9eac38771b9a0d1aaa91cc6195ad8dcef2b9ec3b5c173bcb1d71e2b17f53e8eaeafa77ccd7c2f923f280c06ceb77ff94c3c8f5a99f06e6dd319a7c94f50fa7ad85d0b8701e32bd1ef2cdc79e716af6945078ae2f8e32d683c29fc9a59c7b586e80641be36c818d367796fb563f99bfe06d5bd251f82f7196f6e2e6c45973ffc8b1d278f8e913fa65ed0d7b62fdd5f6990f03dd6bf9b69fc0a92e99f827b9ae95808c81c8215fe5f662f17aa28f4a965a79246c47dcdd6ff00927c4fe3b66d27fa6be2d8c630ed71b35d6f98fa27f2c6670e5fa960d25be24624376b8d8fdb91f558cb96eba74c7864fab157d3d1b6aa58dc5c0795ecb9bdc1c7f5599cd74b7826d5bfd16488978659dc8fa2dce662f0266e94e0dbbe1e45ef9b95672337890cf48f846d6b00005c00b78f24ac5e269ea5ce2e2db9cff08ecbb6376e194d28543ece77878ecb6c38a7693702e7ec844e1ce0c6df91fd9346d83de4f9b17ba08bb7bdd134e5bc8f5ee8896136773f922c5c988100909b66c3dfea921a7d6dfb13361d234d7cd5133213a83c8dae7df8e307e53cfd573fd47d5838541010101010101010101010101010101010101010101010101010101051d7a7a6a5d1aaea2aea9b490c7139cf9dc6c2316e507e777c57eaad435fd7eb22aaaea5aa8a299c192d3b36b6568bd8e72acd46a4d3cd676ed2eb5f3eab71223f958df628ac49d86c72d3c1540037f291ed6444ee25b18edbad658ae911c7e665b82b36b58c762e9cd3d953280d6bae79f2dd78b9b934f7f0716e3d63a57488a082ee85d23c1b016e31caf0e59ededc38f4ec0744656b6d52c8f65adb4e6cb9ccf4ebe8e0749c00ff00e57746d1cb63796b5ded657dd9f48c2a7a2e9aa5c4d5d2473939dcf3723e8ae3cb67ea7f1cac0f4469cc6d852b40ef8ec9fcb6feace38a63a54514d7822dcce058e40f4ce0a7bed671b3ff00c351bcb5ce63dad6bb7161e1c7b2beecff001ad49a2f88d0d2c173cedc2cfb56a62d375074ac53b0b5f113b62748db7208f4fcd6b0e4b19cf8e56925d16a6288b4462aa2e1ce06ce603c123d56e672d62f1f4d6474ed10470be9ede09dce17ce0daeb5f6b1f9a4cfd2bf13116b4ddd7dc1e0648ed7ff002ae396bea5c76ad340c2585ed7455311b5c13b5df62ba473b10dd8d6cad9a9e48e4372d713b9a3d2c7babe9ff69edff4b60134c03a32096e491cfdd4f9526da8d503dac168ef619b62e16f1b18cb1aea5a8491f8c5ac696eeee470bd7c7d478f93f5ac948126ce7d4aef3b70f8bd0538f0b787dcd892aecd22a87017b116bd9415dcfb8b9007b0ec83126cab2e597dfec82588e4f6b0516270f8db246f7125c3e5c5c0f7b22bb9f4b7596b5a6500a0a5ad9e28e4b105b26d1704589f52b3a89a7dfdf053aba3eafe84a3ad73f75640c6c359e606d2019371cdc58fdd6623bbaa0808080808080808080808080808080808080808080808080808083e76fdb0fade3a3e943a0d2bdc1f3b8b1eede40361900773c7eaa7d58f8aaa257be70777a9385b9d2abcd6cfa10a8aef0463b763e8a8c581c6ed2a6d64da431be323cb8f5b29edb5d3898b8580beeb63dae9b56dba7b46aad46a5b1c31920db27b05e5e6e598c7b3c7e1b957b6743f4945a753b25a86874a40ed80be5f2727b3eae184c669dda9a9db19f96cd3ce385c36e922f430b6f71c29b696a285ad6ded63ea96ecd276c5b85c616764818091c02aed75a44ea36937da07d0a4a698fe0580dec7eeaeea69c3a85a41b0e15decd694350a27131c8306375f8e41162125d163afcfa6369deef05a769c8f5ffbfaaded9d3afeaba70133656795c2f7696d85fd8fa1b71f915d30cbf1cb3c7b56a76c0d76e8d8299d9bc6e3e571f63dbecb5db1d2e06504ccb54b4b31f3117b7dd49ecbfeb5acd5e934df0cb03e17f162304656b1cb266cc74d656bf4f619185ed2dbd88f12f65d27b7d73beaeb75d1c5217474af99c06435a375addcaeb8db3eb95d7e35ff00f87ab2a899656f80cb65cf173ff0ba7f3cc7a8e7782dfad16aba4c94aedc5c1e2f6bfa95df8b9bd9c39383d66d51b21647e18e6f72bb4b1e6f543212e047dd5952c443b82b49f1c5ee78b59465246704a76a91a76b0e05f9560cdad69b171276e4a88b9465f23da0465eebfca3fb269a7d67fb1dd76a94dab5432adc29a92a236c71c1b079cb722d6ef6392562ced97d5c38404040404040404040404040404040404040404040404040404040ec83e19fdb1a5a4775d40ca6a9a8aa708dcd96595f70e75ee4307668bdbea92b5269f3f4e4ef758e5a30b639710e05a791fa848201825aec8561a58a36b09f3607636e172cdd306f5b47e2c2d0c11b8117b9c5979ee7aaf4cc3717345e999b51a8019087b410378182573cf9f51db8f836f5fe8fe97a6d3616d980bbb9b775f3f3cee57b7d0e3c261351dba184016030171b5da4598d96360b1b6b4b5143618b8fa2434b11463971bfd55d922765bb65655206def8574806023015573e183d9193c2042110cf4e1cd2d22f7562e9adaaa00f6905b6b775ade898eda9a9d25c410ff337dc5d5952cfc69eaf4189e0b5c4341cdc05d266e3706aa4e91697de3ab2dffa0596e73567f8223ffc14e91a77d74eebe017592735fe99fe08ca1e8a8e9e2733f11bc3b9dec0b397356b1e1c62cd374d5242e37678a48c9771ed858bc96b738e454d7347a614d2c92b2c00cb9a2dfd131c96e12bcd753d06a6b6a0b218e571390646d8345f92bd7c5cbeb375e3e5e2b974b51f467874f283144e6ddb9e6d617e7eeadf259ffc69223d43a21b152092205ee683b801faab8f936ded2f8d34eada974b6a1034ccd89ae88f05a71ff0bd5c7e447973f1afe34324123090e6dbd97a2672bcb78ec6396b78b02b52ed8b073acee559d2396c840b0385746d7a8dcf89d1cf148f63e27870734f9beca11f4bfecbfd60dd4b5aa6d3b56619ece219246078c4f2d68ef6b5f8f45cf5a1f66b2c5a36f16551ca0202020202020202020202020202020202020202020202020208ab36fe165de5c1bb0dc83902c83f397e3b57c55bf10b51a98df50435c5969be6c77f6571f8d479cedb92efe63cad0c5f6e41b11dd0704070dc71d91646d343a07cf386edb8f65e7e5cf4f47161b772d3ba71c1ec1f837b9b210d73af6dbf9af1e5cbd3db871bd43a63488a8a8d9146d02d85e2cf2f67b70c74ecb0461acb00b9bac588dbd82e75b91662680a2ac476e1152b728692c6dce32889836c1534cdadf556768e5ad042bd4239d814570e8f93609088248ef736cff5562fc40e85b636042692d549694137c5be8aee31a40ea317c91ffd29b11fe0597bd825bb2473f8363721aa0af353800e14593a6b2ae983e32c90070be30928a22898c7c8e2d167802decb73262c579b4fa77b05a300970b81ec6eaefb471f818cc44346db820d93761a68f58d1010f92090c6e23ceddb76bfea174c7373cb0747d6ba6cc8e749269b2b0fac66ed23e8785e9c39acfd79b3e1dba66b7a73693ca22919ff5765ede2e4f6787978fd7e34db6d870cf65ea8f2326345fefdbbaa2dc726c16b5c9eca4fa8dcf47eaf59a1752d16a5435ada59e9e66be294dc061f7b7659b371674fd12f823d7157d73d24dd4ab74c3413c4e11c803b732436f99becb311df55410101010101010101010101010101010101010101010101010415f2b20a39667b831ac6171711702ddd07e6a7c63d4595fd69a848c1f2ccf6837e46e27f3567c6a751d476d99b7d05d6844222627588b9279544b1465c5adc673f658cba8d60ef7d1da24d2d399a17b5ae16163e97f55e0e5ce3dfc38d7a4e8549331bb276b8c80795c4ee042f16763ddc78bb652401acb765c6bbce971806db5962b73b66cc2c369e336ee8a923bee0522ad4638c2a2d44d1b6ca2256b317b0baa8cdb1db95a89bdb8d96e026963200770122b8207baa93a62e6a84412301c150567447beeb27e2461e1d8dc054d31f0cacaeba0b0007055ecd6d56a621625655ae9a3009552ce942665af8ca220630871b0b83cfd56a5674c9b106938e55da30a985a63f4494692be0f2385cadcac59b8f24f8871b7f17e147c8f98afa3e34fdaf9de4fcd3a518880eb822c6cbe86df374c1b8ed7c5c9565db2c80b920f02d70b5f889a12d9266838030a2beeefd8f6bf5a8fa09ba7d540da9d358fbd35442e17693921ed3622fc8e472b9c4af7b0aa080808080808080808080808080808080808080808080808083a5fc61d56aa8ba2abe0d3d8e7554d1ec327f0c0c3f3486fe82f61dca847e70ebd24736a93cac2e2c3212d73cf98f98d89f7b2dcf8daa4ff00fb873470e36fd15466d68738b33b41c9f5413d3444d45c0e4068feab19d6f07adf41d235d44cdf1804819f55f339af6fa7c33a7a1d1c4d6b0d805e4af6e13516d86cdc2c575899831cac55c676cd9faac6dd2268be53eaa1a5985a02aab50004dedc72a4176360e481ecb4ced33597e30ac8330de55886df4549d31da7d134ae0e2c109db870bb7dd424d212371b8f44d0c5d1dfb29088bc3f543eb17305b8411bc0f745bd2ad53458d94256b256e4fba8aa350c3d9195702c485623223194348673e5382ac674d3d5b9a5ae173f92e98b37e3cb7ae29236d58f0c973c8de6ebe878f6e9f37c89abd3a5cb4e5a5e0fccde57b77d3c367d6bf65ad9b0b15d7171a8f8b8be5699d26a6f983b8c650f8fb5bf61bd7a7ace9faed1a4f3b291a1cc71765b73f2fadb858d15f4b22080808080808080808080808080808080808080808080808083a37c748deff85bae430b097cd0169dbcf193fa2847e6fd6d9f592818dafb5adc596fe37a0c7bcb5d8cdcfd1544a0b47980c16e14245fd0da249e31fc56b10b9677a75c276f65e9283c1823601c347e657cce5afa9c33ad3b8d38b330bcb5ecc7e246f2b35d227665726e7c48d1dd4589a3faa35f166107854dae538cdb844fabd15881f5eeb71ceff6b0c8eeac86d9968f4e10db1fa228024e846e19574b18edc286d8ed49366d896dd349b00b670aeba4432db75963e2c9b5494806dc234a931f2927ba8694a41e623d3951552aad6cd9114f17b0ca26875ac9b152a0d9a42b19aa12b77022eba4acd74feaad3e296b18f6b434b63fef85e9e2cece9e4e5c37dbcd3508cefa9b717238eebe96177a7ccca775a47b4dbcd60036dfaaf4e2f2dfd559059c3193dd519c4e2c363917b1088faeff0061fd6744ada8a8d3a681b4dacc0c0f825663c78ed6735dea460ac534fad11040404040404040404040404040404040404040404040404041d2be396a4fd2be14ebf5b148d8e46d239ac71f538c7ba11f9b535c54486f7bb89b9ef9e56db48d1bf7318dbd984244a92360230458016b2955b7e9d80beb85da0019715c396ea3bf14dd7b268bfbad39b2020b9dc15f2f92eebeaf163a8ed14a4f82d279b2f3daf4c662c0dc2c56f1588cf658744adc125458922c949dac5a805ca0b718ec8cc5d849c765a8956a375b9f45b61207020d8ab0ec0134b3e0072838dbe81158edf64b12312c17ee9a37d2370b62c8ac7041c232827e3e8b15ac54a7bee192b2d2a4f81ce15dac559319fcd67628d48dc081caa456da5b736411c8ef2a466aa4f96ab19aa6782ba465d6fa95aeb3dedc10de7eebb71bcfc974f2fd72c1f2b0fae17d3e2f92be5f2755a0a8686f6c642f563f1e4aad3b40f7b1160aca95881624dd687d41fb0b54d2c3afea6ca9da4bc46212612e01e09fe21f29b7ae0ae77ea3ecf1c22080808080808080808080808080808080808080808080808083aa7c5bd3dfaa7c3ed5e8191eff00169dc1d806c00249ca0fcd6ae2d1572340033c7a656a7c699d386b4bdcde48bdfd94b751ad6d6a92104f9c0000eeb9dad49b769e95a7f0df770cbf8baf37365d3d3c18bd5b46859e046cb0b345c0f75f3f2afa7c71bb6605ad6c2f3d7a24e997d4a9bdc6a268966ba26673eca2268ed6ee8b1660b045ab111707717caa8bb11371644d2c32f703b7bad4db0959c72b511237eaa91c9c774585915c38242460ec5ee8ce91daf743e23208070a5e8da097e536cac35ad29cd7eea2caa535fd1162ac87f25154e419bfba2a09511526cf7f756255199d9b12b518b3f509e16b14b3a687a8481138737175df8abcdcb36f31ea0616d5ca08bf98657d4e2ff8be572fd686aa31e61cd9d8faaed2dd3cf676a550ddc6c3241cad637b4bf10b9a778375d197d25fb124f5b4bd615068e9993ee84b6563640d7399cdec79b1e0fd973bf4afb6a376f6076d2db8bd8f21196480808080808080808080808080808080808080808080808083ab7c592f6fc3bd69d187ee6d2bc8da6ddbd907e6b6a1018ab67648d735cd91c0b48c8b1e0fbad69bd74ca95b6bdc6076b7256326b16d29a2dcdbdac09dab9ba48eefa34663a6634805ce766fc816ff000bc5c97baf6f14d47a0e8877303b816ecbc793dfc71b81c5d71aef1ce3bacb5134560b2d256a27eac4780534b166102c8ab11f3929116e10a8b31dfbadc62a768365632cc7a5eff4562b902fc2a4656c5f9f650605a5492aed1b9b75a46205c614a30737d72a255795b6b9b2c56f1529864f750f8a53f1e8a3514e4be6c84539317ec1155e4760a7c45390dc94450aa8b7b9aeeed3756258c2c6e42ded1a7d7e3fdd6e00fd575c2b8f24dc79deb11099efbb6c48fd415f478f2e9f2f930edd6f5088b5e5a1a002eb85e9c6ee3cb963aad6ccded6c92ba631cef4ae2fbcf702d85d187d59fb0b5019f50afd469aaa98494e447343237ce58472d2b9fd4afb0154101010101010101010101010101010101010101010101010106a3ac9b3bba5b5214b4eda89ff000eff000e373ac0bad8cfebf641f9a9d5103e9b5caa649309e43239ef78eee26e7ef9565d46e76862bec6b2d82edc7eab9ef6e91bfd362638444816ddb88b70172bfaeb8cf8ef1a0d0fe2a56b9e0d9c0bae3b37801783932d47d0e3c76ef1410b6185ac68b00bcd957b308b6cb1bae56bb6998cacaa68c5b2a1b491dc944f8b513490ab52ad44d36ca1b5885a4f64d26d7216fa655936cdab51b4fa05d263a6769036e78caba4f666d6807d8a9a5f6dc7239e32a8cbb2b07047250de9839a32a436c36e534bb62462eb288646dda7952ae2a33b45b0b3a56baa073952c59549e30a2ed4e6e08210daa4879482b3ad7ba2c569b83dd58cd57232ac6556a18248cb1c305758c65dbcff005ea4f0277b0b6c0925b7ee17bb8727cee79a756d461bd9d63e6c803b2f5e174f1e6d3d44645c9e6ebacae59453734dc968cd974c7e39be9ffd86b51a2a1d6eb69e695c27ac01ad61166e3870f539231916eeb3fa97e3ec955910101010101010101010101010101010101010101010101010435bff00b39b17fddbbb5fb7a20fcdbf891a3d6e99d5b5a350a7929a59a67482193e7682717b7a8ca7e378b57130170205ef61c2e56bac6f29c787e1c6d3d80bdf95cabb62f4ce90a7732884920f31e17cfe4bbafa7c53a7638be5e179ebd3844acc02b0e8e4151369a2248ca6936963c760acc69b5fa7b62ee0b531676b90b338ca4c5655b898037b2b3136b31b6ce0b52699dad3580004dbeeb5a4976958ceddd5912d4cf8246e40b1b5d2e29328c435ce172d171c90b3a6b6e36a9f176c76e0f6485ac0b6e0dc2ba36e58c07b5cfd5349b0c6d6e4a9a5f656a80034d94d12b5d51c9f759d69a976d7d4c66ff552c5954e461b654b0da8d4b48b959d6d65d35f2f2466c962ed03f85176ab3775a919dabb959d0824e3d56b162c68ba9e8454d11923b07b05f8e47a2f470e7aaf3f2e1b8f3ead6f90388b9185f470ee3e5e5d5692b05afb8026f9fd575c5c728d7bd843bb60aeb8b9d7d9bfb14691a79e9a96b6631beb58f3fba706ee873822d92083df8fba8c57d2aaa08080808080808080808080808080808080808080808080808083e28fdb174e9293e22c754f0d6b278cbd8dbe79c928d62f1986ee2d2db7ca385cebb4765d2236c95f1336070bdf95e7e4bac6bd1c7375eb1a7c622a58e3b5b0be6e576fa984e9762f972b964ef8fc66e200b70a2dac43fd4ad48c6d23678d8cb9770acc19b9694ceaad7cdb1a7dfdedfd975c78dcaf2ae43aa4111cca09e2c32b5fc559fe56c69b5801a0820823babfc7a59cabd4facd310048fb3bbe70a7a37391b7a4d429676d9b2b0923d54f54f6db6314d13da2c2f6eddd590dd48246f030b35a9bb13c73b8e477e42bb4d32b8de2d8c66ddd4d2ca89f82403dd634d6d8b4def7eca8e0d80ec8ae096b45c2335149303f74845599fbae094d0ab2edda73c268f65399d15ae5ca6b67b69ada8ab8721ae048ec9ea4ce355595ac0d1e6fcd2607f235b255309be2d74bc693936cb731f1dda42e7eae932daa4b8b9498e8dabbb83c258d4a825c149f12d5795a2489cc3fc42cb78dd56729d3ce3a8e98d2d7c8cbddae1cdbdd7d3e0bbc5f279f1d64eb554db48f69f5b5d7a23cd628ccdd9675ae01fd16f173c9f6efec5335355fc3c9f75218aae9aa3c23296d8cacb02d3f606cac73af7e5504040404040404040404040404040404040404040404040404041f2d7edbba248eabd2b58642e731d198a49390d00df8fba2c7ce7a6b37d9b182f37f4b5f385c73eabd3876ed7d0f4c64d598d7b32cb127d17979af4f578f3fd9e98c5f3ebe962983ac3dd65d27510d44cd8d85ef70000bfd15c66d9cb2d34151aec5e2165ede82f93efecbd38f13cb9726daf76bf355cbf87a289ce7ded71c05d7f8e63db9dcad4c7c4a788b3c4114873668dce27dcf016a698cb684ce18c73def803ef80f98907f2c2e931db9fb2acbd562198c42363d8d3fc045be972b538f6cfbea90f59c523cb27a49626f67c6eb81f7b2cde38e987256ef4ed566958d928aa0ccd3963ee0907d0ae396323d18e776edd43ad4ee85ae738ee1820f23eab8d8ed8d6da8f5a2ec49bd87d0f0573bd3ac6f283500ff0099d637fcd4d9ead98aa6937c27b33eae4ca0d8e53648e0cb7e3f2ba35a46f96e304614d9146a2b6c0873804da69af93536071687027bfaab0ad3eafd4b1d242f79160dc9eeba638b95cf4eb0df88103f7861736c6de62003f45d670b95e58865eb1a7a83613343dd8dbbb056a70edcaf3aa54750421ae2fbb5d6bb48785a9c3d27f335b36b92bb70248776bd8823effd94fe389395832b8f86768d8fb5c8bfea3fc2c7aba4cba561d4534043641e1bae01dc2d7f7f653d256a67945aa7ea18e497c3906d27001c5cff00758bc6d4e4b2f6dac72b268b7c6eb82b8652c7a70cb718387bf6588ded0bb82b719ae99d790b84f0c800d8fb8257b3c6af07938feba557c63c6b60765eec6f4f05509985d3b631c38edcfd574c1cb27deffb25c32c7f09a97f134e619daf31bc1193b700fe56564d39dedebeaa08080808080808080808080808080808080808080808080808083cc7f69fa0656fc1bd65de087be0636469b5cb4070bd9163e1fd19db1c7180ee3dd71e47a78fa7a07c3b85ae6d4540f5007e4bc3cffd3dde3cdeebbac43baf1d7bb1662f95237f1d7fa922aaa88fc18e46b187e639fcacbb71ea38726eab5168103632df379b924f98ff0085bbc8e7306ce0d1d9041e140d30b6dcb6c5c7eea7bd6bd06e91b9ae8ed2481ddc9cad4e4a9708b147d1d473483c584befd9f90ba4e6ae57862fd47c3ed1cb1c190181d9276bb07dbd92f3a4e08d6c3d19414723dcc6b88772d27ca47f759bcfb74c7834e29ba7e8691ee30c5e18760b5a480b95e575c78b4b9044e84168c81c5d67ddbf4b190f13712d3b72972dae334dee8d5529686ca0020f2b965d3ac9b6fe09c1b026eb1ec9eab51ca78fd56e647ab23279727b29ecd7af4a75755b011700acfb2faf4ebda9d53cdec73eb75d25db9d8d43a7dad2fb58ade3a6329b756d6a0d42ba473dc3737219176cf04fd175c7391c72c2d688749d73e4186003e67b85ff0020bace68e3971570fe92ae8de031ec2d1edb7fcaef8f3e2f3e5c192683a6f5cc08a285cc18f366feeade6c1270e5123fa77588e52256309383b4582e7fc923a4e2b556a3a7b526805950c8de38b936fa2e7fc98d749c764674df8e0dfc3d64114d1dec1db0b8b572cb5f8ef85d7d66fa4a6746627c4fda7b161001f50572de51d358e5d2de891cd1dd8f7175b1b8ff0017a1faab9652c30c7d6b6970470b8d769da270ecae29674d37545389f4c78da1ce6645d7a387ebcbcd378bcdaba2c17937b3ac47aafa38dfc7cbca7ea950d38a9d56280876c73c075bb0be4fdb95db1ea3865f5fa41f0874bacd1be1d68fa657edf1e9e9dac2e1cb87627ded65639bb62a0808080808080808080808080808080808080808080808080808359d57a647acf4dea1a5ccc1232a607465a4daf718fd507e735440ea2d66ae90fcf14ee69ed90485c727a38ebd37a0212cd083fbbe439f5b617cde7bfecfa9e3cff576b637cabcb6bdb23970b32eaca5509221e2ee7027e856e5e9cac5a81a032c1a3ee6e9b6b1c56191b6f73fd566dd2cc16e16c63d2eb3eed4c57e927644e04b96b1c92e0bb2ea14623b19630efaab6b38cd283aa28de48f15a7ee173dd8e8ad3474c6fb1caf5612d547c432470b9ba45770b153db4ba8b740f21d609b5d37d48459b9e4a258db44dbc60dbb2d4f8c7ea37b5db6e47b235f234f5c482492b2d4eda1ac9b2eb9c2b32b19b8c6b8bcc8eb36c42d7ba638ad53d39b672a7b52e316e1a5b9e415a99562c5b8a8e207cd6fad96e64c7ac5c6430b1b6180acc92e3152b9b0b8618377624ad5c931c1a4ac8038f99a0ffd3ff2b3efdadc1aca8a3764b436d6ef82afb25c1ad9e9e31705e6fdda4e159db3f1c4236e011ec54b34dcbb586dcb6ee162b15bc60f1759956c6bf506efa791bead2bd1c77b79f92755e575170da963f167afa53f2be57f7177e18e9151af75de95a5d2b774951501807a81cdfdad75de7c79f27e9369707e174ea7a6b5bc289acb5efc0b2d392ca020202020202020202020202020202020202020202020202020207641f06fed03a0b343f8c9abd2c0cdb4f2913b73fce3715cb37a389dafa6294c1a2d2445a46d8c123eabe4f35ff6dbecf04d62dd323c15e6af4cbd31737909b457919b78b7e4b52a69831c1a15b74b221aed560a28f748f1b8fcad1c95cfbc9bf91d7b56ead7400de46c37e1a3ccf5df8f837f5cb2e6d35751a9f515669f2d6d3d34cca68d85ef9a536160bd9878f2fe3c39f93dfd75687ac35693f105acbed697583775bdcfa0c2ef3c797a72cb9b5db2a2ebcd504426a9a589f093605a08b9ef62a65e36970f237f1dbb4dea39a6822a88dd514ed91b76b5e2ec3db0578b9386637b7b38b97dbe5760d33597ceef0a51b24ed6f95df45e5cb8ecedebc339f1b68e6ddcae6eab74246f595760a12d1c8baacb7f436d96ce42de35cec73581b67638f64aaeadaabc02ecacd6a3acd6bcdc8ec521f58d232de628a92aabe1a48f74f206df00772ac96f519b969a6a8ea77075a189b1b072f95d6fd177c382dfae3973c8d5567c43d3695af0ed49d24adfe18e3c0fb95e8c3c47972f26a8c3f14e95ec2ef1270d06c6e005d3ff00134c4f2b6d852f5fd3d61021ad8c936c48db7eab965e3ea3ae1cff00daf37a90123c58eedfe661baf35e2d3d13936b74fa85355b0ba1983fb1f50b3658b3562ad4ed713b8075bd42e98d62c471b37156d591335a40cff45ceb73a8e0fcbeca4e8faa530f35bdd76c1c391e4daeb5d4fa9d6333b779be17d5e3d658c7c9cfacabd3bf638d0ffd57e2ec354fdde15042e9c9031bb803f5fd1758f364fbb82db9880808080808080808080808080808080808080808080808083ad75b75869dd314cd3501d3d5482f153b0f99dee4f61eeb872f363c7375e8f1fc6cb9aea3ccaabe32f503252f8f43a0f0af86191fbadf55e3ff00f21dfc7d39fe2a6bebbbfc3ff89ba1f543ff0007203a6ea43ffdb4ee167fbb1dc3be9caf5f17918723e7f3f87c9c3fff001e19fb5c5203f13b432045ff009980016f989dd6371f92d72de98e09bac6998d1d85858617c6cebee71cd45d0df2ae16bb48aef1929b59104ecdcd292e975d35d349e1b48e6cadec934e8dd515ee64ef7b6f24e46d8dbe9fe17a387073e5cb511741e82cabd723a8d5a564b9bedbddb7f45eec2e32e9e3cf8f2f4b5ec9d73a536a3e1f56414318deda73b43464dbb2f46137dbe4dde3976f9dba63a8ebba6bfd61b4b410548d4a85f47278adb96077247a14e3e4f5bdbd5cdc5fc98f4683ad555674c52f49545152fe1195bf886ce5bfbc0e3c8bfa2e9972ccae9cf8bc7b84dbdf7a77a7b4d9fa7194d531b1d1866d6e2c2cbc79cf6c9e9c778e3d3a8eb1d3159a449f89a626a29ae4ecbf99a01e47aae39f1ebe3ddc39dca76dbe987c7a264cdf335c39f75e3b8c7aa55ed389de7d970bd5759f1bca1792e1ec869d974f903596e56b1acd8c6baa4786e012d4983a7eab3ee79599dad8d15465d7f55489fc32d80b80b9b2e93162e5a75b750ea1ac569147199017f866770f234f3b42f5f0f1ede5e6e4f59baf40d1be1b69f168cea8aaff00cd556d0edcfc807d00ecbdd3098be2f273e59d786534ba374df516a751ab68add4637433c2d88e3c37bae1aefb2f4639497b2e39658f4ea74daa69afe8993453a3b4ea26b04ccaebf9832d62cb2d72e724e93c7e3cfdb75df7e0f74ad06b5a4d70aea369635c0364b9b93ede965c37feae9cb95c72e9aeeaed1abfa72b1c28e5925801f91c6f85c72e3c327a78b933915345d59d33c491bfc1a8663ebec42f272f17a3dbc59fb3b9d2563a7843de36bbf887baf34e9db5b8d8d102efb959cae9ac674b52b7160b328c047e5b2bb14e58fcc7d975c2b9673a792f5bb1edd7aa22b60c9803fefdd7d6e0ff008be3f375957d6ffb19f41cbd3dd2353d43a8c5b2b35320460f2d887e99395e88f26576f7fbaac880808080808080808080808080808080808080808080808083e68f8a7a9cda8757574ad79b098c51fb35b8fec57c2f2b3b97257eaffc6f049c51a9819fba0d97248e570cb1d3d366aa39f4d8a517b59c3208e42ccb67c63292fd74dea0a09e4eb6d1669a69a6b39d73238b88da09192bdbc1cb6e176f9fe4714c739eaeed4c3002f366ef8c5b6e5b65c6bab0923ef648d48af2b48b944d3575b03a4b86e2eb589f1a6a9d12325ce7303afce395df1cb4e794db1a6d38c0db5380ccf0b5ecb8f4dee9d5faad3c3e1b67706919b8b85d70e4cf1f95cf3e0e2cfec68e4e88d3eaf5096aa4b31d2f98b00706924f603baeb3932fd72bc38cea1a6f43e9f423c560739c5c0da5196d967f9326e7163fd376fa698b361ab9b68e0072e36ddfd779863af885d4954f03c3ad9eeccb771dc3f559b9d6a6b1f91c6814d5943f888aaa40f6caede006d834fafdd632b2f6cfb6f26d74e04971ec4af3bb46ea81a77e1234ddc4f2d8f8bfdd5974ce90d73cba2295ac63ab6a649738fba91326b1e323d95f8c38d63c7934c752d2ff00ea4a0b4b81b587aae9c7f7b633e91e8f4daa5053b443571d3102d68998e2d9bdff00ecaef8f2e53e39e58e19f5937147ad752d2b4b595fe337b873415da72e7fdb85f1386fe3a675674c1d72b27ac31fe1eaa537259f213dee1759e465fb18ff00c5c71ff8d7501d0152c7c779da01cc966f6f657f9a5fb1278f7f2bd3345aea5e9ed0d9a769d46e6359f3b9fcb9c7b9532f23ad473c7c0de5bcabaa75219b507991cdbb8f1e8179ff0092daf74e1c719a8ea50688f655995976926e6d9bad65cbd6ab9e3c5acb71d974df15a447234fd6cbcb93bcf8ecd42cb331d972caedbc569d1dcacc3d4732c2cab1546a5a012ba6158c9e6dd57e1c1d75413c9078d1b5f1c8f8c63786bb23f45f5b833f5c2d7c9e7c2e5c9a8fa575df8e35d3695151749e8834c70606996a087f863b063463ee7f25cf3f33ad62e9c3fe36dbbcaba58eb2eb89e513cdd4da93a406f7126d03ec30bcd7cacefebdf8f83c526b4f4af851f14b5497558346ea59d9531cee11c5545a1af6b8f01d6c1078baf4f079572beb93c3e5f8130c7db07b885f45f20404040404040404040404040404040404040404040418c8e0c6b9cee00b952de8936f9675add53ac39e1a499257bc0f62e257e7b96ef91fb5f167af132a86163769164bf1b64c0f8834b812c7707dfd1635d38e5fd3ae6b0c12f5869c6c7f7704ae1f5385df86c98d78f9ff00e51bb870172ceba63f169ae5caba44a05c70b2d21923bdfbe158914e58f6ade297e2131870b10aefa244069ece25a1496c5d2c5392305aba4cec4b8c5e6c9e4da5a6cb5fcb599c72a394b2c486053f91b982a48d73ce1bf929babd462c85e32e240e15918b97f4cace7e05edd9632cb5f130c6fdabf4708636c395876936dad132c524697c39a2c09009e07aa68d23a9cc642b26c8ebda84792a42b58e8cd88efd95ac3901db41232dc2637559cb1dc65e770bb4aed3b728ce299ec369011ee933b8b5eb2ac3246b9b620382e9392337162e8e9de37103d32adcd9d2955454e01b80b1736a4ad1d6303de5b0b2f7c5d73f66f48e0d3b6e5c33dd3d99d278a91bbc35a3852de88d9410ed0b9ba48b223001ba9a5569cd8956315aea8e1d72bae0e793a76ab4ad9fad7490e6ddaf69dc3d6c57b2e5ae2af1e38ef9a3bf329a38e2f942f04dbe9460417dc3059bf45b91b936874ea77c7acc4e66e0ef11841bf7dc16f0bace33cd27f1d7d9b15fc317e6c2ebef4f8fc85fac9541010101010101010101010101010101010101010106b3aa653074dea530362ca59083e9e52b9f2758575e19be491f38c637ead1341c860caf81bde6fd9c9ae24bad11f8e2c6fafe699d670f8da434ac9f427c6fb02492d77a3870b78e3bc5e7ceeb274bab8cbb5d8652cb16c0f04fbdc6138ba95c397fe517e238c159ca2e29a3395cac74956d9f2ac371cecbab88866877026cb5111082c32afd58c9b4c1c8a919a7973c58616a4da6d6e3d24119badfab3fc8c86991b7240fbad4c6466e76a3969a289bf28252d923337546588bdc6c31ecb8e59eddb0c34921a601bb8858696616016c2d48d2e4361cad6976b436db36bf6516768a73e5520d456b410e279462d6b9ccb9bf757e8e443b861674686c259e6b615c72d3171dae430b24162d0bb4bb8e36583f4c69f92e3e89a8b33a81da649ffc4729eb5a992b4ba6137dc49faac6965466858cfe145e984b4e00511841006b8badca949f163c3f6595db1779410a2c52ab362b519ad74f7cfbaed8b95690c3bbab74d9b69b32390dfd0aef965ffade7c27fec76e8a3330321368c607baf27e6dee95cd1eddce078056b175ee45bd31ad6eb549211e5654464dfd3785d38bbce38f3fff00aebeb56f0befbf2420202020202020202020202020202020202020202020d4f5744f9fa5f538a31773a96403f22b9f2cde15dbc7bae497fedf3b51bdadd6a371b069039f65f9f9d66fd95ef8d157cbe2ea44e326f8532bb4c7a8ed5a7cd4d43a6de70d712db86edeebd18598e3dbc994b9e5d3a56ab2326d58c8c635976703b9bf2b38ddeeb1cb8eac631152a4588b2573b1ade97e117faae7a6e55a862be7b234e5f07b2436c1d4decb49194505bb729a5db614b01e6df45d318e76acb858102d75b73d2098120df014b646a4509a3de6c572b96dd719a47e0868ecb11bda392db0b72ace964db88ecd0005b8ab111f65a5c62c8e067851a45506e3d14d27f6d5561cff005518517101d60ac9a166109ea8b51c41e3859d33b490c25a7d125d259b5b8db8b7a2eb8ddc72b34e763b37b1ce021f10cb0075f0a7d5954e4839c2c35b8a92d39bf0a26d8329c8070a6da8c4c65bca8d6956a7009088d6d43ee4ad473c94e5e0dd76c5cf2f8a0c8e47ead4ce65ae1aebdd74caff00ab384ff77648039b0edbf95796de9eec74c68df6ac7348571ad49b956a1711560b4e43db6faee0ba71dff7639a7feb7d6b0dfc36df9b05fa09f1f8fac954101010101010101010101010101010101010101010612b04913a37e5af05a7e854b37165d5dbe64ea0a5750ea734190fa69dccfb0242fcf7363eb957ecbc4cfdf8e295339b26a0c2fb96dc5ecb18fd74cfa8b1a9d73df33ae6ed07016ae5bba671c648d4efdf54e71e76dbf55d71ea3cbcdff2899870b3f8e716a0e54d74abf072173ae98f6d9d2ac35a580c0e38487e1e15c9f45527492280736c2b0ab4c68db60385adb3a6323319c2beda848acf05ce2070b195db726983a30d1723d96555aa6c01bf60ac5d283cf949255d2fc431b8b9f70b722af53679575556580edb951b965453e5a56a466b515eefe8b3627e35be266d7461720938562b694be9e8b3595c6301ed9b2ca398c6d243b81c292e8ca6e2d44d06e0fa2e92b969c3a0241b0488a92d35b9e14ea37103a9b1ee166918ba00d69245d65a8a3520589c2cb4d455fcae5ac59ad53f0e2175c639e4af2f06cba62c5414c6d5ac3ec56f29d35c3375d9b4c74523bc175897e01f42b8e13bd577cb726e35b52c753ea5b09b10b166abae1771b7d0698d66bd414c3266aa899ffdc175e0c779c72f2b2f5e2b5f57b458597e81f90728080808080808080808080808080808080808080808083c07e30511a3eb1ad3c32a03666e3d458fea17c6f370d66fd37f8ae4df149fd3a6e9e6f523d579318fa397ea3af25b50e04e2ea4eab5ff00caad39bcef3ec177c3b8f073ff00ca2dc7cacd627c5987959591b0a6e4058ae98b654b86d9674d2d46795058636e3dd21a4f1b0f0168d276c7821110cd6b90a2c8accf98db2a35a4de097464936f454696bdc5af78252353b69ea67bb8b476e56f189f19d29bfbade95b2a76df8c05613a59b763fd534d4e90ce2cd489f5aaae682c715348ebf50f2d9307ba6995aa3983801759bd353e379a679a40d26ea7d46e6384ed2e3837534cb878b3bdd412d338139e55958b1799b6d7e4775a6348e6636d7b6166d6b19b539581a6c165a914aa70d295646a6a48c8e01598ba6aab0e080b512b552df715db171cbe2acdc60ae9239ed59807e29b7ecd2ae77a6f83fe4d9d04db2405a720dc2e1b92bd7adcacb529bc5d5771c13ca9977578be3b97c28a475775fe8ec68b88e5333bd835a4ff0085eaf0f1de71e3ff00239faf0d8fa602fb4fcc08080808080808080808080808080808080808080808083ca3e3fd05e3d375268eee81c7f51fdd7cff003b1ea57d7ff13c9acae2f24a43e14f7f75f2e755f7f2f8e35f6f97c76036392a65f76bc77ad35ba5c825dcf06e385e8e3ff8bc5e47593671736f458ae78acc5f35965b5f8166b58afd3bf16586d61afb14d2c5ca775cff004490fc5c85c0616a2699be40060a949151c4bdc6eb2d692d3c4304ad482c39a3610aeb6cef4eafafb7c12e27d14d69ac2edd6dae3248bae13a5ad9d0b7d78b2d53f1b1801b1b1c2455da68dcfb794dfe8ac365540ed84ede14d12c68abda431dea83ad568b1fba8caab2a0c53b4378fe8962c770d01e1ef8c83cae5f297a76a8a3dcc75b9b2d49bdb315658ed7b8c8c2cd6b48d82d27382a44b3a4909919545e5eeda5a1bb7b7d56e5e99f55a95c0c6402a122954bb3ecb3f8b1aeab7d98565646a67782835b5479cad48c65d46aa7759dfa2ed1c6ab4d80785b9db96d51c6d3defc355cfe3b78dff35bd35d795b6f55e6b1eeb3aa9a76876a5b8f21699c34f60fd9d74cf1b5bd43557025b4d008587fdce373fa0fd57d1f070fb5f17fcb727cc5ee2be93e288080808080808080808080808080808080808080808083a87c5da1fc6f43d6380bbe99cd9db8f439fd095e6f271df1d7b3c0e4f4e68f9fded032be25fafd54ee2473c494a6278ca6ba72caeab594f03207b8305ae6ebb71fc79b9f7725c8be652b316e0f5f459adaec66c30b15b916617022c565b89e377729a6b4b747259c00ce521a5d8de3d55f88e4b8eeb0e028b23968dc480145d2d53c60e4e42dc8c5ba615b2889a48200f75b934cebd9d2fa8aa9f51273724dae9adb53a8a34b0d9b7b2d3522fd30b0c2cb57a6c74eb7882fc7a2d44bf1dbb750d3c2d7318ddc5bfaaf47fac8f34f6b545b343317b646db7727d16269d3295d4b5b6864af0c3700ae75d31aead5c2ee5915aa290f87bc0c85a4d373d275858fdb8b8c5895cecd5d97b7a0e9d2ef89a2c01fd4a4bff4cb9ad8458b9a3eaa64b8de941cdb5cacc6feb1ddb73c82a6d34e7c42d06e716c2d7e33f556496f73e8b3f8ba6b2b651c5f29a591ad91d82429a2a84e795ac58ad6541f3019372bb63db864ad3bb0b78b97c6b6b5cfe23037916e533bac5dbc7eb26cba7e2f0e174b2bbcc3d7d579b4f5e59efa8b507ef2a1d2ade309d47d21f0174ffc1f404352e1e7ad99f393ed7da3f40becf8b87ae11f99ff0021c9efcd7fe9dfd7a5e2101010101010101010101010101010101010101010101055d529195ba6d4d1bf2d9e27467ee0859ca6e58d71e5eb94af972a237412c94f20f3c4f2c3f506dfd97c0cf0d5d3f5d8726f0963089ae06e7214d692ddd6355b4b9a5a2d8cadf1fc70e6fac623956b18af416039bdd62bae2bad0395cdb9b4919c9f648d44ac776575d2ac531b497bacce9a9f17a275de3d1559f1605ae14244d03338cdd5c52acb6e1a718e1698ba69b5a99c23739c4124aba59dbab560bc8dbf7c95b93509dd5da489be1f9bd14db77a886aa68617d83da0fa129239dca26a3ac008ce15d2cb1b035a4b32e5764911c954436e1c8ad16a9524ee01d72a25ba50a187f1121de54d24bb4d5f08644423ab53a70732b7c46b88b62d7c25f8c3d1ba7ea0ba9dae26d8ce5665db16370e2646ed0427d274a7531168fb28de3549e486e565ad442f905ecaed9d29543c343883cac91a974924923c48d0d603e537e42d55de904ae06fe830b359aa720b121566b5b547cff45d7171ca29ca6ee5bc5c6f4ce8a9e195ce74b6f27cbf55393e3af0dd57327eede5a2e1a7d1624e9da5ed7a898640228812f9086347b9360984ddd379e7eb8edf5cf4c69edd2ba7b4fd35a2df86a76466dea067f5bafbb863eb8c8fc97265ef9dad92e8c080808080808080808080808080808080808080808080807841f38fc42a3652f5b6ab1b3e5f1cb85bb6e17b7eabe2f9335c963f4de15b970cad237bdc72bcd93d58c4158db35beab7c6e5cdf1144b55c22f53702eb35d715d605c9d6548cfd6c92ac657b386515346e23fb256b15e81f6522c5c61b8e795748b9000d6f71eb75a9d25ed9caff293709b65a7d5d9be3202b3649a757af3b4b49ec7d175fc31eb266ea9268dc21367db05738e963c935cf87fae6b15f2564baad50949b8b486c3e995ebe3e4d4f8f37270ccbe3b4f49e97d45a253f855b5cfae886078b9701ec7fcace7665f8638dc7f5bf3a8bc721df92c4c62dcec7126a521658075fd1344cf6e9bd68feafa88cb744f06169e5ee6ddd6f65ac3d65ff64cf0cb29d553f878eeb3a2d5d91ead50eaaa6930e0f1969f5055e5b859feb138b8f3c7ed7a46a64184faaf3bd58fc69e823739c4b3372afe33faef7a0b4b299bbb185cfe27eb74c7002c73f54d9a4550e1bac2f652d593a6baa5c01595faa72bb25c13628d5bc6d363957446bcbcdc9babb2a123959acab486e5c7185596b2a40dce3f65d7171c94a5f9acba62e35953bb241c5d67923b707c58780e8b70e41b15ce74ed63b47c24d3ff00d4faf34aa473498db3099dff004b06efec17a7c6c3db38f1f99c9e9c55f5605f65f9d10101010101010101010101010101010101010101010101010786fc63d2e6a4eac92b4b3f7158d0f63bb6e02c47d70be579785997b3f41fe33965e3f4fe9d2c30f75e3d3e9ed8d4c05f13dd6c80ae3f5cb93bc54a3e174bf1e4c5729cdad95875c5718f5cabb44d1e565adb223b9b1b1c2d6c4b1f211a5c8b9591718e16bded6e1555864ae38b8e7d525674cc9be01c1e48567d454ab1b9b95b84ad0ea7007ee01ab52ac9d3af4c2a68de6cd73a3fe8aaeffb4f4fa890df96c7be16a654d44df88325c3ae6fc2d7d62d93e326d3c4e3703b6566f4c6e54725331b771098ca9b915855450b8b48b7b2dedd31d570ed469c0b9da0faac7d6ac8d7cf51256c85915cb7d5674bf1bbd1681d1ed27f2519aed5456659a562a6971cf21b851629cb21ddcacd6e2ad449b81f5509d29caeb34a41aea976ebdb8553e453673652968f07b1c239aa498dcacecad754727dd75c5c725070f32eb8b8df8b469ad0324b7232b197d7ab8a7fab18ee1bdfd0ac6b6eba7abfecdd4465eaaadadd9e5a7a5b6eb705ce03fa02bdfe163ddb5f23fc9e526323e805f49f1440404040404040404040404040404040404040404040404041acea3d12835dd31f435f1ee61cb5c30e63bd41f55cf3e3c739aae9c5cb97165ed8bc63abba2b54d02574a1a6ae86fe59d8dc8ffa876fe8be6f2f8d961dcf8fbde379f872cd65d575b7bff766368dce760068b927e8bcd31af5e571d5dd6a9d0cb048e8a78df1c8d36735edb169f70ba5964ede3c6cb7a598173fc77c56587165cabae29e177651b4bc353644919f300462c9b1658565a89db2600bab2aca96276d9493f929f13f13194dcdac16e33a433cb76d87256b691ac9c8b93dc2d46bdba6ae7001dce1bafdb95627b4d29bb6971b36df65654b5935bb082c1badcd96f1bd386795fc591231ade402a5a4f8c9a6e5c36deeafe24ad4d7d38cb9c002decb172dba4dcedae1009000e8efeb8ca9b6e64d8e9d1b18086b76d946e56de966da2e0f3d94caea0dac1501cde7efeeb1b4d2d367dcce6f8e50882a240317596a2a990e4a162bcce1b0dca446ba5376fb5d6950116b9f5534cb175f69fa2258ab38cfb5913f1aea8e7e8bae3f1c725323cc576c5c6b71086c94cd008360162ef6f5e16488a3a19a7a98e9a9a274b34ae0d631a2e5c4f6098e372ea2e5c93196d7d31f09ba4bff000af4c882a0035f5244b5241bd8f6683e802fadc1c538f1d3f35e573ff367bfc7715dde6101010101010101010101010101010101010101010101010101062e68734b5c0107907ba82ac3a5e9d0cde343414b1c9fced89a0fe7652618c6ae7959ab5e33f1cb4afc275545a831b68eb62049b7f1b707f4b2f0f9586aedf47c1cfad3a3c230bc0fab12dfce0dd7376c7e268dde70167f1a5969e2eb225181742336c9628d48e44b6272a1f891938df93c843f19cb502dc8b2e98cdb1bd2bbaa72eb70064dd6a4d31ecab3b8b83addb9f65a85c95a4b380008e2eab3b42f1b4dad7e7853e972d32690582c6d75a62ddaacd239b231b6dde6e6cb53b12d35446daa1724dc1fa5d2c676835225c4dc001dc92b9e9bf6daa80d8e302f668192548d6fa59600c8b734820e41f5567774b6a58646ec36cdb212cdb53343475f56353745e15e98b373640783e8426584d6e2e19efeb7b154ee66e691eeb9569848fbbb735d922d62b1b59a8c5cf005bd309b5daacc4b9d7e02b0889ed1b55da5aaefb07fd3b2bf07171b0e7b2692a9d45c036ee92315ae94f988f75da3956ffe14e94cd5faff004ba4963124225f1256b85c16b4126ffa2f578f8fb651e1f272b8e15ecfad7c1ed06b2b9f53a7d54fa6b643b9f0c6d0e8c1ff00683c7d17b32f1b1caede2e3f3f9319abdb7bd17d03a1f4cc86a69d8fa9ac22df889ac5cd1fed1c05be3e1c70f8e5cde567cbf5db57679c4040404040404040404040404040404040404040404040404040404041d1be34e93f8fe90755c6dbcb4320947aed3877f63f65e7e7c7db17a7c5cfd337874385f26c7ddc2f490ff55cabbcf891a6d92b0d44ec7dc7f549162563ee3384591c39f62338596a307c81a727959fd5fc60e94dc06dfd0656a39e57a64f95bb8b41712326cbb611c6d711cad326d036f7208eeb559951ba570c00d2f763e890b5082632f2f70f62a88be66b7f97b9ba24ed9b0b434f02c784dafaa9d4cad325dce18c262b669cb1cd37e7dbd96bd93d5c55bb71bdfbfaacd262821790ed96b84274b05ec9008810dda303dd2432c90b660cdd66edda793c258cccb4c85482c6be2c826c54b1d31cf6b904e1a0b1ae1c70b3176863aa702584e415cf2c5b996d65b292de7f25cfe3a4d3926e08bdc2d4a7c600fe4b511048d0e7dbd154632e1a6de88294ff0029bad631cf26be6165d7171b751ea9fb3469be2f50ea5aa3db714f008d87d1ce3fe02fa1e263faf95e765d48f7c5ee7ce1010101010101010101010101010101010101010101010101010101010101041a8534759453d2ca2f1cd1b98e1ec45966cdcd2e37576f98abe965d3f53a9a1985a4a790c6e1f42be372e3eb74fd07067ed8ca37ccd5e7cabd98f45feeb9b51230d89bfd904ed22d7ba2ca8a47d8151bfaaf2c96049360a48cdba8a70d43bc52375fbaef319a79ee7bab5e21b5c92df51ff2ae334c5a9fc363ddb83ec76e2c701364aad278a7c8d759c0f9dc39fb2b12b17925bb79b0b23500dc5c10d6fa929a75c715c8a2a0113775530b88c857d5b932fe9aeaed3fc5778903c39bebc84d58ba4b4da6ba38fc496401bc5c94d5a924415c29da7c38ea1ae239213d5af5ff00a5301e1fb9c6e3d477472cb1481c3713fc478b7a28e5632276173766ebaace914c5ed8f6c11fc8703d6c9bfed3bdee2cc7610f88e680e7b33ecb9baedafa8700d123490cbe54fbd1bd2ed04a5f1eebdc765cabb635718ffe2ffb0a4add72c70395b888653fbdbf7255f8ced0c8ec11cab115652ba48c550972fb2de31c33afa4be04688748e84867959b67af79a87dc58ed3868fc87eabebf8f8eb07c3f273f6cebbf2eef388080808080808080808080808080808080808080808080808080808080808083c5be38689f83d721d66165a2ac6ed92dffc46ff00916fc97cff002f0d5dbe9f83cbd5c5d0e3385f3728fb1857271dd7374720fa0e0a9a58903ae2f74488e57017ecb2d29d4124e2db6d95bc1cb3aa9bc6ec38875efbbd17691e7b52890bdc77bb04633ff78553695b2860230eb7ba926c951c73c51b4e4eefaaba6f18a951a988afb6df52acc2d76c64fd6ae7d49f23bccfe5759c6e9339118a879fe33f9abe9a75c73da7a7af9e2f96475beaa5c1d25c6fd455fa8d43dc6f2badf553d5378cf8d77e25e1ff0031fadd6a60e77358a6d41f19ff00d4fb5d2f1edced8bd06a6d73c38daeb9de3b1cac8b7f8e0e37dc00b6166c72f8cff13e605845f804f659d25acaa27739ad21d7db81ea4a4896aad40646c2f7993cbc8b292b5f8b9a5bbf739b8161cae59c75e3abf01680eefeeb9c8ecc1cfb1365a444e7dc855962f24b08eeb50aab2b8069f6e5748e592e744e872751f5451e97183b259078ae1fc318cb8fe4bd5c187b64f1f919fa616beb3a6863a7a78e089a1b1c6d0d63476005805f5a4d47c2fa91504040404040404040404040404040404040404040404040404040404040404041a4eb6d123d7fa76a74f701e211ba171fe178e3fc7dd73e4c3df1b1d38b3fe3ca57ceaf8e4a79e4a799859246e2d7b4f62390be2e78ead8fd07167329b676b8bf65c2cd3d53e3102c4d941934f395095149871bf2a34a550ecd88c01723d574c1c73563b41734b89ddc7b2ed26de6b584b2ec6117b372ac3eb53a857550c53349208e46174e3c67ea5e9afa8add64b08688997ef65df1983784b51d253d5c80be79c39c795758c7ab1c6d6d29f4b6ccdb39ce75bd12651df1e2bbed64686d70bb5cf6e6e33c2cdca3d18f148e25d2ea1aeb31e001cddab1eced8f0aacfa5d4bbe5737eb6ba7b4a4f1a529f407ed2e9647bbe82c16e6719cbc7feaa2a9d05acb964920c77cad4b2bcb978f67eb595ba7d4c1078b1557980e36f297d5e7bc3935d06a3ab3a5108113811f358852e383cd9ccbe37ba7495d14379f69b58e0decb96584fc7296cbaad9c350e70dbdcfaae3669bfacc97990e0bf700318b2cd935b596acd1364fc49dc5d66b703b05cf3f8e9c7f5783c87168f4cae33a8f4461bb39ec16a256208de05f36574cedcbc8030b51146a5f77103baed8471cebdcbf67be96341a54bd4355196cf5836400f6881c9fb9fd02fa9e371fae3baf8be5f2fbe5eb1eaebd4f1880808080808080808080808080808080808080808080808080808080808080808083c67e37689050ea94fabd380dfc692d95b6c6f02fbbee3fa2f9fe571fec7d3f0797ee35d0a375dabe6671f5f0a3b07858d37f581362aa4632641ef759d35f5526bfa707d56b173c955f1ddc1e3682063d57695e7d471e1eedc6f82702cacba5919328dae717b8f1e8afb35318c64a461670d29ece98d51751b5aeb34596e66f46192ed0b5d0b8839056e57799cadbd2b5929da059cb73b74f6d76e25646490093ef658b1db1e5e9135ad1736c5b8522ff00224759b11b05b8cdcda8d4e60c659ad04a6f518fbbdb56e8f746e07ea3e8b16bcf967fd2ac1491b24bb1a2fcdd73b93cb92fc7087337016ba932ae371da46c3626d652e54f5d397b64b0d8d3bbb29be93d5769c59a59721dcb85f2b8e4e98b299ce6c6e73402eb602c47662d27bf36b95afc66b91f31c65547131b36decba6336c5adc7c3ae969baaba962a4b16d2c3692a9fe8cbf1f53c2f5f070fb57cff279fd23ea1a6862a6a7641031b1c51b4358d1c003002fab269f1adda5541010101010101010101010101010101010101010101010101010101010101010101079efc7aa5f13a2d954d177535531c0fa03707fb2f37933fd1eaf0eff00be9e2b4b335ed0e07ea3d0af99963b7d8c32fc5a3620fbaf3d963d12a377093b544e7592c22294e0906de898b37b42d6b5ce278365add8c489a08c9759c3b72aedac7158f08e7b8eca7b35eaad5105817026e9324b14c969c116f45a2677166dc8b838bad4b63739a7ea686a8c4413b490b7337599cbf2b3fc7826e5bcad7bb7329fdb8755b36e1a2ea4e45997fda07d6970f9081c2bfc8d7bc9faa533c3a425d6b7b959f773cb927f6a9300d67cfcf60b3bb5e7cb965f886004bf17246162f4c4bb6d628f6b40b64ae7eced8e334999181822e2cb52ed8a8e4f9c069b0b67dd4b524db266d1c76e7dd62d590bdc7b248bb7365ad336b918e56a629be904ee2413d977e3c76e3c99ea57d3df0eba6e93a6fa6e9e9a067efe46092a2523ccf7917cfb0e02fafc784c31d47c0e5e4b9e56d7655d1cc4040404040404040404040404040404040404040404040404040404040404040404041d5be2c537e27e1f6aec16bb620f17ff006b815c79a6f0aede3dd7247cd504a6293d41e42f932e9f774db4120736edc8532c5ac32fc1c795c7d7576ebb5790f743e2227760e3d9699da3919be46ddc5bb48208525d1a5f89c2c2d8feea6fa6e467b8837be3859fc6e470e05cd2d22c4acc10494c5c05b95b99562cda84f48f0496920ae98e5d395e36aab29ab375993102cbae394ae796167c6adccaff0016e2470b9fe6ecbacf5d39ff00ba2fc4ea0e898237b9ce26dca7faaef934b4c8ebe4b9f1fcbe97fcd6378b5acd2b29ea1eddaf2ebb781659b9484c72bf5720a09bc40f93e5b63eab8de4fe9da71591968fa754d2cb33aa6a3c60f9373316da3d14cf9256f8f8ee3bdb6b6c1365891d36e375c1bdac02dc92473b504d28c35accf73653d7a6265da163ec480717b9f753f1a95335fb864ab2091b6bad489b624d8124d975c7173cb3d23d007faaf57695a6c798e5ad89aeff0077985d76e3ef2923cdcbd616d7d86d16185f563e2395410101010101010101010101010101010101010101010101010101010101010101010106a7abe1151d2daa42e170ea493fff0024ae7c9378d6f8aeb395f298c9c2f8b5fa2c7e2d534a6339cb4f216b1cbf132c572f76ee6e414cb131cffb42f382b969d76840f3e4fb80a5f8919020e6e76fbace9ade924472083709deb4d4b2a7de0b6e32424896b389db9f603030a5c5666b51c58c66fc0f4516743e10e06ed04a45aa92d235c32c3eebace9955928e026db4deeb71cd1ff00a5c04fc9b4718536d48e5ba542d7ef6649162536d48b30d0b7f880f6b058bdacd44df8560c5aeb934ab58df0db76b2e7b2b2336a9c22405c64703738b0e02d5f8cced8d43046df1038b40249b2b32db394d2849531b9d6612eb1e55f8ce3fdb0f13cc33ca98b4b113ae17498ed8b96938706b37136b2eb8e0e3966d5d7d6190163090cee7d55cb2d75170c37dd765f815442bbe28e96d700e1017ce6ff00ed69b7eabaf8f379c79fcbcb5c75f588e17d47c610101010101010101010101010101010101010101010101010101010101010101010101057d4a3f174fa98f9df13dbf982b37b9571faf91dc2d2387a1217c3cbebf49c7ff14ad1858db71253ca637ed37dabb617f2b8e78feac4adb80f69b82ae587e98726fa56e01e4fdd71ca3ae376e05be6b5b0b9c74b12b0b7c3b0b077ba7d4974c83b3df7039365a9d44da7a602e2f820a9b46ce0b5c5c8bfb2ce9d26534ca43e7b017c5c5d6b1c59b9542e91ce3868b5b3ecbacc5cfdb48046d26c5d6b653b86d2b626d8baf72022cc91c8dd809be3d40e14d2fb3863ee093736e14fa4c994336e8f7b8d85d66e3db53253ae74721da0820e0d8aceac264ac7634ec0de142648249f692d7003174d1f976d4b0441a43596be55bbacce9139ae7bbc846ef75d70c6b1965a5c616c6c2e71b05e8c7193ebcf72fc8a95550e9bca2e183b29964de187f6a8f0b8edd9ea5fb2fd209badebaa89ff00dbd11b7d5ce03fb15eef1277b7cef3eeb191f482fa0f94202020202020202020202020202020202020202020202020202020202020202020202020e1e37348f51652fc23e45af6ecd46a63fe599e3f2715f0f966b2afd270dde11cc6b8bae87b49185d31a9639a79dccf21c83eabd38ddc79b2c74e2a43b7170f96d7002978f6ce39e869dccfaaf2e58fadaf5e396e2488e7cc6c07eab9cdaa56b9a0606127654f4fb48db8b774d545c866118041dc3b63dd6e44b47cee779ee2fdfe8ba4c59b931f105c82ccbac72b526a31bda39032d675b6db040e3d93ea6d8c32bac323dbe89fa4dc4cd9c18ec5a4df959b5b9bb103f00ee773f74d8ac653e0b9aeb1ce411c80a24a8257b43ee3b8003b0b3daed5a791b131fb4b0bde6edb1b5fb249b4b75f14c4ee70717d81db7c656729ae9d31bd76aae70b900fdfd16b0c3759cb3d4491b1b112f71c771ea57b31c24faf2e596fa432c8e91dec380b39e4e9860c6c02e16ede891149c10b307b2feca900fc6ebd53dc3228ff00324ff65f4bc39d57caf3ef71ef4bdcf9a20202020202020202020202020202020202020202020202020202020202020202020202020f93fa921f07a9f5387f92ae51ff00de57c4e69acabf45e35de115a30bcff1e9485b8e131a9a43232f95df0cff001cb3c5c31d7058ecfd576c72d3cf963b059b76eebdbba6786fb8619eba624dc8b3adf6e579ae3a7699ecfc416ccd8c425cd2092eb8b0fb2ccc7f6afb2cb6716b87d85c5d4d1edf8904c4124b81b700adce99db9dc410fcdcf1728b3e386caf6c81c24711e856a645c529a8dccde5e00e41537b4b3a4315502f7b49370316ee9b48e3f1918b80e6eefe5be5166489f33dee045b68fcc29b6a4dfeab4f3d9c43c86936bb88c2335839ec0010f696badef725674229e566c21f621b918c82a2b5b56e059e1c7201ece5d78f0dfd63933d7c72cdde40434b410085eac7098cdd79bdee5751cb9a4c8e25c4971e3d3e8b9e796de8e3c198658775c2d7a319a71b458ac6db910cade55895eeff00b2d5306e87acd5587ef2a98c06dd9adbff0075f53c39feaf89e7dff791eccbd8f08808080808080808080808080808080808080808080808080808080808080808080808239e68a088cb348d630724949d8e8bd7bd74dd2749967a405b7f246e3f33ddec3b2c7267e9375d38b8ef265a8f039ea65adac96ae737966797bcfb9e57c6e5bed76fbfc38fae3a48c6e179efd778ced85256b4e1cdb8e16f1c99b103e3b1b8e575c7271b83104024ed17efeebb639b8e5821919b41958c717bb9cded6ecba5c6651ca5b8b9dc6570730d996c7bfa85e6cb0d3b6396c73ad208dc3c84ddd61dbd16246b263348f32b0c71df760fa0b2b274952b65bb06e75ed8c77516560eae6b6470758d80c5f3957d76bee8ab6a1db1db411d89f40912ddb87cac31b98d0e700dbf3f7575a4da2dcd6daeeb5c0b9039c7f94fc4fd4cd94ed7873b24e6dfe548d6f4d71a992432b1bbcb770bd877e6eb5ad33b4335454c0e79658b5f722c78ff00b0acc66496e987e225923b35e1bbb696d85c9f55ac78b759b9b83139b3b84837c80870bffc2f4632631c6ef2bd2db230c04349b76bf65cb3e4dbd1c7c7a481a7baf3daf4e334cc3706cb1b6f4e362caa19c60adc66b75d0bd71abf4956914750ff00c248edd34272d776bdbd57b38796e0f9de4704e49bfd7b9f48fc58d2f55b453edf107cdb0d9c3ff94ff65f470ce65371f2b3e2cb1baaefba76ab41a83375254c727fb6f670fb2db0ba80808080808080808080808080808080808080808080808080808080808080804d9069f59d7a9e8a2708478f281c03e51f52ac83acbaa2af54632a6ade4878bb5830d68fa2d49a1e53f172b3c6d7a0d3daebc74b1ef70ff73bfe17ccf373ef4fabe071f5ecea94bf2af15f8fa18fd5c8c2e35d633b6162370b6159460e6ade35cec42f8fbf0bae39698b8ed139a6c45cb4f63e8ba639e9c72e3da13fb9c398d6300f98700aeb32997d70b8dc3e38dbbc81b8dc66d7e7fe166f1e9667bfa862748d9dcc2e6866fb0ce4fbacdc7a6a5ffb72fddbb7ef2369f30031cae7ead7b30a96c73c8d63181c080370e79bf2acc746f69486b584b87cd72e6f1659937577d308646b637786d045ecd777b7f756e04cfa62e8dc4daffbc26e716056a63a676e216c8d0f8e570682ec0b72afae899310d6b98f2c2dbb5fb45bbb53d0f69514a1a0b83db76b0102fdcdbfe3f55bc70d317340d6f8bb1b0c61918f30777fcbfbae97398c666172fa9a284378bb9c7e671e4fb95c72cf6f46384899accf0b8dbb76c67491acb5eeb16b72320dcacb416e08ca2556a91662de0c5f8d4cb994aeb5cb19b47246e76d73259219586ec96376d730fa82b7c7cb70f8c72714c9eb3f0dbac7f1d0c3a76a7776a51b6de2b3cbe2b47f17b1f50be9f072cce3e4f3705c2bd528fa8751a266f6d48ab8072c98799a3eabd1eaf33b0e9bd57a6d490c99ff008690f67fcbf9acfad1bd8a4648c0f8ded734f05a6e14192020202020202020202020202020202020202020202020202020201200b9360106beab56a586ed6133bff9599fd5593635357535d577f19fe0404fc8c393f52b5314da95640c741e047186b5e437d4ad0cfc10d6d80b002c0283c13ae65f1bacf557ff002cc183ecd0be1f9377c95f7fc39ae28d6d28b8fbae57e3b4bdaec630b8bbc481a3ff00c2c2cdb2d97456259838375622273174959b11ba306eb52b1a46598f62b53266e3b432c01cd76c718dce16b85d70e4b1c72e2951086c0891bbc1b005a73ee57499cae5fc7623958c919b439d18b8bdf18055d4a9dc474ac7b650fdd1ed048bf752e3b846521f1dcf04b835a6c4949896d579632cf280e2cb794fdb015f588e5fe396b036c0c6f0e37b9c5bfaa7ac27b236c323643bae771173bbebff007f7577098e559c11969dcd67862e4daf7c958b948de3c77f597822e4b897926f72b17936e98f1c8c9ace400b9edd262cdac1c2cdadc8cdadf458db7a48198cac6da90db8ba6d74e0b458deeaced2aa55fc86cba62e55a677feabb92b79338466c6dfdd736dbae886ba3eb0d29cdef3ec3f42085e9f173d66f279784f4af7d6c368decdbcb485f6e3e1b8a485afa4639edfe107857489d95357416928a77c7ead06ed3f652cdabb2e93d4a648da2ba200f77c7c7e4b3ea3b053d443511ef8646bdbea0acfc1220202020202020202020202020202020202020202020208ea27860617cd23236fab8d90696b3a92004b28e27ceefe622cd5641441d43527dea257363fe46e02d489b5c6fe1e95be1c6d0f7db80b420b39ee2e773fd111cb631e3c7707b904aa337c632a2c7cedd6b4eea7eb2d6a377ffcb7387d08042f83e4cd7257e87c4bbe28d7d18b85cede9d67d5f8c6172aed134631decb9b71906faf2839db71c2830732fd96a566c44e8fd96e566c4658b49a605becac4d312c16e159535b626107857deb3e918ba00790159c9625e262601fcb8fa2d4e467f8d87807b1b0fa29fc87f1b0303bba7b9fc6c4c20648fcd4f65f5d31f0ae54dacc5cf87dd676d4c74363c614dac8cdb1fb296b5190673858db4cc33bf2a2c36775208a4185d318cdaa1598695d31ae75a96b2f23bea96984598a3b1586b4ed3f0e685d55d63a7b40b88de6477d002bd3e263be4797cccb5c75eef1c2378042fb91f011450ed883438e01c7dd691148cb307241f651638a7fdcc9b4fca7841b18d9345fbea394b1dfed29ad8db699afb9c7c3ad8ecf1cb9bfe166e3fd1b6f29ea219db78a46bbe872b1ad2a5404040404040404040404040404040404040410cd55043f3bc5fd02b26c6bea7517b816c2e118f5b5cad4c536d3d7523aa097b9ef91deae2ae955a8e86b19261966fa92a0d932195a08924b03d9aaa240d6b1b660562398da6e703ebe8a8cf67ef2371bfcd6290fc672b2d753f123c3fe32e9eea6ead3541b68eae06b81b7f136e0ff0065f1fcdc359edf73c0cf787aba8d236c178f6f6c8bd18fd566ba629a31f5fc9726e250dc728b1c81ff00614d06d48307302b2b3a42e8f9cadcbd33519603d96a5461b05aca9f8e430765065e1959b56063c613d8d2231abb4d317c764d9a579a1738ad4a9a70c8b68b14d921e1a8b196cb0e165647219edf459573b70708b1c8192a2e87371c2d699bd2bc83056a2550aa6937056a33a518a23bf852d5c62e4715b1659957f1e9ff000534a265acd5e46d9ad68822f7eeefecbea781c7f727c8ff0021c9f318f518e2b12ef40bea47cb44e86d180f60e30554da9544459804db9ca832310922e3211639a390b0ec79c24349eaa370b4a0ee03b85676693533cbdbba390b5dd8836213aa9f17a9b58aba63b2a99e333f9c60acdc3fa5db7145a951d58b4528ddfcaec158b156d4040404040404040404040404041c39cd6b6ee2001dca7d159d5d0e431c1e7f456636a6d04934d25fcd66fa05b98489bda0743bb0e1755580a62de321119b180708a907ba231735b65045e180490a8463cdea504924778cf25dc856119b807b038771750d3a47c5cd0ffd4fa68d5c4c2ea8a17191b6e4b0e1c3f2cfd978fcbe3f7c371ecf0b93d3378cd33001ecbe2befcee2cb0762a55895a07dd61a895a3370165a676bdd51ced36ca8382d54d302c04246513996be2cb72f48c0b47a2bb471b5048d17eca1196d07b2cb7a46f68049b2b19b3485c092aa21734abb34e367e49b346cf64d9a362ceda8e76fe6a5aba0314d91cec0395676a8de2ddeeac63685e2e2cb5115648ef8b5d36444d82c78eeb36ace9774fa29aaaae2a6819ba595c18c6fa93c2e9c585cee9cf93298636d7d05d37a445a36894da7459f099e777f33bb9fcd7e87878fd31d47e6f9793df3b6b69e1feeb02fbb0bb7c71da37c62c76db3dae82a4ec064b5b03dd45631b4b4909a092107cc06534ae6025976bb2d3ca48307c261937b0d9a7828a999339cddae44d20963f36e6f6416a8f57aba5f2990bd9e8fcfeab37183694dd494aef2d431d11fe61e60b371a6db7a6a982a19be0959237fda6eb3daa54040404040404040411cd3c508bc8f0df6ee9ad8d7546abda167ff0033bfc2dcc536d7cd2cb29dd2bdcefaf0ac9a060b95a45989c47070a2ac472faf28894381e42016b5c30830da47ba0c0b6d9bd9483122e2d7fb8418b458fa2a2c440ed16b12839636ce7466dea10473c6d735cd73439ae1620f70a592c25d3c23adba7dda06bb2c0d69fc2ca4c94eeec5bfcbf50be0f95c3fc79eff001fa0f139a7261abf5a460f37aaf2c7b2256e14ad44acfaa8b12b428ac8371dca8b1c16aa698b99dfd158951fa82ab3a60581589a63b73eaa9f1937f2486d962c51768a400e7f544b5116e6c11186dee8b1cedf60a6cd38db9e13a00cfb053639f0f1951a63b7db08b189b80522542feeb519ac0b6feeaa48c7c20545d0c8b3c050f8f50f847d3058dff5eae8ec48b52b5c3b777ff60bec785e3ea7b57c6f3bc8f6be98bd2191dcd82fa73a7ca6658d7123b345b1ca1b57905afce3836e50552db95142cb61003316b26d6312cf54de95c8c0b1cb4f6288c1d0f25870ac3f11b8117045d111b85ee2ff006282bbd83b8cfb22a38e4961937c4f7b1c3bb4d8a9a1b6a1ea7aa8086d53054307f10c387f958f536ec3a76b1415c0086601ff00c8ec159b34ad820202020e1ce6b45dce007a92829d46a9490dc6fdee1d9b9566346b2a756a896ed84089bebc95a98ff68aa1c5e6f238b8fadd688c835db6e061048d65db92aa33632c32504cd0385066061066096a0cdb20e1065769c02830730d8d8f29a1806db9c20e1dce104f01c5904ae8c5aede5b9ba86dc38073770eea9b68babf40875ed21f472599337cd0496f91dfe3b15e7e7e19c98d8efe3f35e2cb6f0eafa3a8a0ad9692ae27473c4edae07fef85f073e3b85b2bf45c5c933c77113561d12b429f16246f7b28d4663feca8acb6df9456245c59565196ab1348dcd2394891c01d958390dfbaa806e4809046e1ea112c63b108c36f7b6166d6a4db8da5c71c22fc3658e72a0c9ade6c8392d165162270e7282277b656e328cb795af8946b3d966fc22464648c0598b7a8edff000ffa3e4d66a456d6b4b34f8dc39fff00548ec3dbd4afa1e2f8b73bed7e3e6f97e5cc27ae3f5ec2c89ac6363634358d16681c00bede38facd47c4b7dbb4ac66d18192b4cb8786600007a1520ab30b9b707be7ba2c88436c7010725a13438dbec801a0dd4d2b12cc64268db0db6e321588e0b4106fc7aa2a37305ac728885f1817b1415e68cfff0085162a4b1dae8aab202d3706c47718506cf4cea3d428acc7bfc78bf964e7f359b88ed1a5f5269f56431eff00c3c87b3ce0fd0a9a1b9690e170411d885075c9b5baa92e236b6207d05cadcc626d4e596698de595eefa95a831630fba2246b0f7fc82095ac7720d9162466f1c8dc13e0cf6bb96f1e8aa336edb58e0a9b198c0b22e9983ee88e6df54020a036f920d90641cee2d7520cc136bd9062003925363365870a8b2c376e7b28481f29248f29e7dbdd43f58bd963ec5586dd53ae3a4e9f5ea6f163db15746db4721e1c3f95dedfd1797c8f1a724ebebd7e379378affd3c82ae92a286a9f4b550ba19a336735ddbfe17c3e4e3b85d57dee3e5c7931dc6207a2c3a3965fd146a3317be54566151c907283073536b3489c3b15633ad31b0bfa22686919c8562322dc15463b7ba9b1896a2b02dbe54d1b7001b229b7d5419068164183cd828b10bb286d1ede4616e4676c76a2338d84a27c774e84e8f9b577b6aeb03e2a069bdf832fb0f6f75eef13c4b9dde5f1e0f2bcc984b8e3f5eb74f045042c8208dac8d8006b5a2c000bed618c9351f0ee572bba9dacb64ad32e4b486937e7b8ec82098f3739fa2115dcdbbaff7458c765ee47740dbd902d8b04183858820dae6c8ac883744705bf4411967605062e663b9f641198ee82231a8a825a7b8c8458ab2d29f4414e5a67349b7e482b96387b20bda76b1a869eefdccc4b3f91f96a9a1ba6446d73955225645ea8a9e38bd122246c7d8a23211fa708396b36dd07207a84190603c2006380b8283368f516419347ba0c8019ee81603b2803d6e0207deca0e4016e5072c076ab04911b1caa2c8b77cac8c4f9458fca78f648237b307d16bea7c687a9fa6e835da6db50cf0ea183f77334799bec7d47b2f3f378f8f2c7a7839f2e2bb8f26ea1d0351d0ea0b2ae126127c9334791dfe0fb15f1b9bc6cf8abedf8fe561cb1ac6f7e579b4f57d6414ad448df645db3b20c5c3e9f9208dccc148b186db03ca2570d00f3654d330df45369eae2c47a596a54b1c6c53669196594dae82d174346c08ba60703850412137394830b039cdd54ac48c66eb519650c4e91e18c6b9ce71b06817256a6372f8ce59cc66de8dd15d04f79656eb8cd8c19653773ff57f85f4bc7f0bff00ac9f2bc9f3bee383d2a263591b6363435ad16000b0017d4c71926a3e55bb4ac6db95a8cb218c907d9062f36b9c58f210557d9ceb9458c40b5fe8838da2e8047242831b630a806823b20638210035a7b0418968ff0028302cb5fba0c4c773741c6c16ca0e1d15c15151986e08ca1104b4e0f21154a6a506f6082a494a6d9083b2b5bec9194a1984a2560b0c954496c283202de8100b6c8ae00ec83203194472d0a0e76a006e3d10721b7419017364d0e1cc1ec8390d00268736164d0c060fdd22a46f0aa248e4b0b1fb28261916e6eb231b6dc1f97fa2b062e68faab13e20aaa682aa9dd4f5313258de2ce639b7054cb1994d56b1cee3771e75d53f0fa48b7d5686e2f68c9a679c8ffa4ff62be673f83fb83ea78fe7ff00f39ba2c914b04ce8668df148d36731c2c42f9796171baafad8724ca6e326804ff459d37b66061656570708b183c1b5c2ab119ee106230aa241dd22b8231fdd1276e05cfa20c4b706e831b28317101040f71375150fad96872d03394918db75a074d6a7adb80a5876c23e699e2cc1fe7ecbd5c3e367c95e4e7f2b1e37a8f49f49e9da1b0481a2a6ac8ccce191f41d82fafc3e2e3c71f1b9bcacb92bb23012bd4f2d4ac6db948891a3bf71c20c5eec5f8050577127f24588ede8838039e1071620e42b008c7d948002b071b7ba805a3d107161d95d801f441c6d41c06800e1361616c70839dbdf2a0c5cd16f54823732fd9510be307d945d2bc90b7d07d511ffd9, 'Makati City', 'Married', 'Filipino', 'n/a', 'Roman Catholic', 'Maximo Estrella Elementary School', '', '', '', '', '', '', '', '', '', '', 'Active');
INSERT INTO `employee` (`emp_no`, `emp_id`, `lname`, `fname`, `mname`, `email`, `gender`, `birthdate`, `cp_email`, `tel_no`, `mothers_name`, `mothers_occ`, `fathers_name`, `fathers_occ`, `address`, `home_address`, `contact_no`, `image`, `pob`, `status_type`, `nationality`, `spouse_name`, `religion`, `elementary`, `high_school`, `college`, `course`, `work_experience`, `primary_con`, `contact_num`, `relationship`, `secondary_con`, `contact_num2`, `relationship2`, `status`) VALUES
(3, '000-000-003', 'Manalastas', 'Janiah', 'Garcia', 'janiahmanalasta@gmail.com', 'Male', '5/15/2024', 'janiahmanalasta@company.com', '', '', '', '', '', 'Navotas, Gasak', '', '09828372873', 0x89504e470d0a1a0a0000000d4948445200000200000002000806000000f478d4fa0000000467414d410000b18f0bfc6105000000097048597300000ec200000ec20115284a800000001974455874536f667477617265007777772e696e6b73636170652e6f72679bee3c1a00002ed449444154785eeddd07942c659d86714432179028204124895e0541248b280b088a8a4a92b828b84b9445b280a2082a224124090a06b208224852941c452f822859969c332ceebeaf70f7deb9f39f99aafebeaaaeea7a7ee73ce7ec51ac9e19b6baababbe30150000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000020ddd46a6ef54eb58a5a5f6dad7653fba983dfe8fbea58759c3afd8d7efac67fe6beab26feb37ba81dd5a66a6db5ac5a48cdac0000400dc6a9f1eae36a27e50feab3d54dea11f5bf35f7a2ba47fd4e9da4f6575ba80f2a5f24bc5901008082a651ef569f5507a8f3d49dea9f2afa206e6aafa85b95ef30f8f7f085cb3b1400009de7dbf6feb0df56fd58dda2fcc1197da00e4a8fa94bd4d7d5ba6a760500c0409b49adaef651bf524fa8e843b24bf9cec604e531095baac5140000ade7dbdefe86ef5bf97e6e1e7d08d2d0ee521e90e8c720b32900001acf23e4fdbcfb68e50fb2e8038e8af7b2ba54edae3c08120080c69841f943ff64f5ac8a3ec8284fbea83a5cadaa0000a8ddf46ae287fe332afab0a26ae3620000509b95d489ea69157d28517ff20c0aaf8fc0ac020040361e88e6817c37abe8c3879ad34bca6b0faca9dea4000028cd53f67ea218bddfcefea6f6545e22190080514dab3cfdec5a157da850fbf25d018fd5f0a24b00000c318bda59ddaba20f116a7f5e74e862e5c19b3c1e00808e5b501daa18d4d7adbc89d226cacb3103003ac4cf85bde52dcff7bbdd5f94773164f74200187073297ff0bfa0a20f04ea66de8fc01702dc1100800133a7faa67a4e451f0044ce8f063c460000d0721ed5efc17d4faae80d9f28ea32b58c0200b4901783f1addde80d9e68ac5e539e3e38af0200b4c092ea7c15bda91395cd8f8d0e50def40900d0405eb2f748f5aa8adec88952ba53adad00000db29eba4f456fdc4439f35e03f32800401fbd55f9396df4464d54554f286f10c58a8200d0075eb3ff5115bd4113d5d1e5ca634e0000355840794df7e80d99a8ee9e575f54dc0d00800a6da01e53d11b31513fbb50cdaf00001979b7be6355f4c64bd4941e51eb2b004006cbab3b54f4864bd4c43c30759c0200f4c0cf54f756cceba73676ab7aa7020094e05bfe67a9e88d95a82d3dab3654008002fcadc9fbb4476fa8446dcce357bc31150060041b29b6eca541ecf76a3e050098cc34ea5015bd71120d4a0f280f6a050088474b9fa7a2374ca241eb45b5b102804ef32dd11b54f4464934a8fd53798b6100e8a4f1ea5e15bd411275a11314830301748af7557f5a456f8a445dea376a560500036f2bc5e23e4493f263b0b915000c2cef9af69a8ade0489badc6dca3b5d02c0c0d95d456f7c44f47af7a8c515000c8c3d54f4864744437b50bd570140ab79431f16f869665e71f109f5f264ff1935a3c7d4720a005aeb4815bdc15175f91be485ea18b597da44adacdea1e650236d533bbb9a572da9d6549f5707aa53d4d5ea7915bd1e55d3e38a3b01005ae91015bdb151be5e5297a96faa0dd482aa2a5eae7969e50b83e3d40415fd4c94af87d5520a005a633f15bda1517a8fa893d56755bfe78f2facb655a72b6f7d1bfdbc94d643ca3b640240e3fd978adec8a8f79e52bea5bfa2f2b88a269a49f971c3458aa99e79f38a996f5700d0589ee7ef75cea337312a97ff8ebf559b2b7fb8b689ef0c78ad7b4f6b8b7e372adf9d6a7e05008db3a9e29b5f7afe1b9ea6fcbcbdeddeacfcff177f56d1ef4ae5ba59cda200a03156531e9016bd6951b15e517eb63f8883befcd8e2e3ea1a15fdee543c0ffa9c4e0140df79da98a72c456f5654ac73d562aa0b3c5be12e15fd1da85827aaa68e0501d01173a93b54f4264563e7bfdd7aaa6bfc0d7667f58c8afe2e34761e6301007d31a3f20231d19b138dde0beacbaaeb7bc17bf39bb355f437a2d1f320d1ad1500d4cab71fcf50d11b138dde8dea5d0a936ca99e56d1df8b46cecb38afa200a036fbaae80d8946cea3fb0f570ce08a2da43cc02dfadbd1c879a120b61106508bb515d3fdcae5f5f93d5302a39b5af9e292ffff2a971fc54daf00a0325ee0e55115bd09519ce76efbef86e2d6555efd30fa7b52dc8f150054620675838ade7c28ee54d5b655fc9a620975bb8afeae14b79d0280ec7ea4a2371d8af38648ccd54e33a7ba52457f5f1a9e07052eaf00201b6f011bbde1d0f03c3deb4baadf1651eb28cfb73f5a5daa7c07c76bca7b9bd9e7947fde27d4fdcadfb6fddfff421dacfe5dadac6657fd344e3138b0787f532c170c208b45150bb614cb1ffe3baa7ef04631de22f858759f8a7ebe5ef345838febe3f7e382c003dc7ea9a29f8d86e725a501208917aab95e456f3234348f5cdf4ad5e96dcadff0ffa8a29fa98afc7b5eacb65033abba78fae4392afa9968789b2900e8d9412a7a73a1e1eda2eae01df6bcefbe6fe9f77bba9c17eff9a15a46d5c1170197a8e867a1a13dab3c9012004a5b55fd8f8ade5c6868df5655f3dd187feb6eeac878df15f09881aacdaa6e51d1cf4043f3dd3b169e0250ca6cea1e15bda9d0d03cd5cf0bd854c5c7fe82cafd5cbfaa3c606f3955253ffa68cbdfa3df79360a0014e6dbbad19b090ded0a55e50a6cbeb57e958a5ebbc9f9d18407a2791a5f559656cfabe8f569529e1a385e01c0983ea43c9a3d7a33a1493daeaa5ae1cfd3b88e546d7f04e375eaab1c8ce6ddf0a2d7a5a15dab3c76040046e455eb3c8f387a13a149f902697d5505ef14384145afdbd6ce547eac54859354f49a34b4dd14008cc883d9a2370f1a9a674754c183fc06f5b6f65f956fdbe736a3aa731a645b7b492da50060180fdc7a55456f1e34a96bd4342a278fd43e4545af3748bda03c8531b7772b7fc045af4993ba5cb13c358021fc81e65deba2370d9ad42bea3d2a272fa673818a5e6f10f3e3932fabdc0e50d1ebd1d0b65400f0ffbc9a5cf4664143fb86ca690ed5c651fe393a5ce5fc36eabb28b7aae8b568521e9859d5780c002de30fa1c754f4664193ba43f979732e73a9bfa8e8b5bad2512aa7d5143358c6ee5b0a00fef5261cbd49d0d0d654b978b64557bff94fd9fe2a27d6b0183baf0db0a402d0619e72c6c0bfb1fb8dcac54bfa76e9997f917652b978574416081abb8b14800ef39b40f4e64093f22de5e5552e5d18ed5f362f7894735d85efa8e875686855ad6501a0e17cf2476f0a34342f6293cb1755f41a34d5544faa45540e1e5fe19d0aa3d7a149796d86dc535a01349c9705edfa00b422f99ba91f93e4e0e9839e071fbd0ebdde752ad7ee751e5b10bd060dcdcb2903e890cd55f46640433b4be5304edda6a2d7a0a17d57e5f016f59c8a5e8326e55d3fabdcd00a4083f8db7f53f7946f5a6ba81cbcb14f747c1a9ec75c7843aa1c8e53d16bd0d07654003a601b15bd09d0d0bca84c8e856a96556ddfd5afeebc1992674ba4f212c1ac0b30768f28ef40096080f94df52e15bd09d0d03c602fd5d4ea6a151d9f466f579583d7bf8f8e4f43db4b011860ffa1a2939f86f68cf273fb545f50d1f169ecfcefc073fa536da8a2e3d3d01e5739fe7f1e40037974f5fd2a3af969683f55a97cb7c503aca2e353b10e53a9bc7cf3b32a3a3e0ded4b0ac000f22e60d1494fc3fb944af579151d9b8ae715fde651a94e55d1f16968fe82906b1a26800661bbdf62f943c76bf5a7f04c0b6f1e141d9fca75904af519151d9b86b785023040d652d1c94ec3f3b7c5549ba8e8d8543eafe8e739fd297c41c79a00c5f202611ebc0a60405ca8a2939d86b7b14a75a98a8e4dbdb5ad4ae5459da263d3f03ea6000c002f41cb5ce8e2cda752bc4d31ef3f6f7f50a9bce360746c1a9ea74e02180027a9e824a7e1f9b97daabd55746cea3d5fc02eaa522ca3a26353dc7805a0c5e6542faae804a7e11daf52b1c9523579739f147eaeed1d07a363d3f08e50005a6c17159ddc14973a02dadf52a3e3527ade2930d5af55746c1ade536a6605a0a5bca67a747253dc622a052bff5597c755a4ce06d85745c7a638af6501a0855656d1494d712f29cfdf4ff133151d9bf2b4be4af169151d97e272dc7501d0070cfe2bd79f54aa0754746cca53ead2c0de1d303a2e8ddc720a408bcca6bca25d744253dc192ac5222a3a2ee5eb5a95627af5aa8a8e4d71472b002de2ad6ca3939946eeeb2ac53a2a3a2ee5cb03d352b14473b91e55ded80a404bfc4e4527338ddc562ac5ce2a3a2ee56d5e95e202151d97466e5d05a005bc921d2bd1956f3d95c2b74aa3e352de5657294e51d17169e4fc3703d0027c13edad95548a4b54745cca9ba75aa6f89e8a8e4b23e78d9458130068812b557412d3e82da152b0dd723deda9527845c1e8b8347a3936c90250a105151bfff4d6dc2ac5df55745ccadb37548a1d54745c1abd73148006fb928a4e5e1a3b4f114bf1b08a8e4b794b5da3decb3d47c7a5d1f39e223329000dc5edffde9b41a560dd857af2025729b654d17169ec5257620450913914a3ff7b2ff5db0d7ffb7afab94af1ef2a3a2e8dddb10a40036da2a293968a354ea5785a45c7a5bca56ed9cc864dbde7a5aedfa40034cc8f5574d252b1bc7c728a7fa8e8b894b743558aed54745c2ad6fb148006f155f9432a3a61a9586f55296e57d171296f07a8143ba9e8b8542c6fa90ca041bc635774b252f1dea35278ebd4e8b894b7dd540aeff9101d978a75b502d020be2a8f4e562ade9a2a85e74947c7a5bca52e4873828a8e4bc5f26e8ab328000d71b98a4e562adee7548a4354745cca5bea33e8f354745c2a9e77be04d000d3a9175474a252f1bc88520aa697559f57b94c9dad71bd8a8e4dc54b5d8d1140262baae824a5721da652aca2a2e352beee57a9182c9bde150a4003785054749252b92e52296657afa9e8d894a70b558ab954745c2ad7cb8a65818106385b45272995ebbf55aa5b54746ccad35e2ac58754745c2adf1a0a409f714b335f73aa147e8c101d97f2b4824ab1bd8a8e4be5f3b6ca00fa6831159d9cd45babab14de2c253a2ea5f78c9a46a5f8818a8e4de5fb9502d0479ba9e8e4a4defab24ae1e5843d4f3a3a36a5e5e97ba96e54d1b1a97c5efa1a401f7d47452727f5d6af552a1f233a36a5e57dfc53bc45b16363de5297cf0690c023d7a313937aeb5935ad4ac1ae8cf97b4ea5ae3ef729151d9b7a6f2d05a04f1e54d18949bdb7924a31a37a4a45c7a6de3a45a53a4245c7a6dedb4301e883b9557452525afba8543f54d1b1a9b7727cd39ca0a26353effd5c01e8838fa8e8a4a4b46e50a996565eb6363a3e95eb2f6a6a956271151d9bd2f216d800fa6057159d94949e3f30529dafa26353b93cd325d55754746c4acb832abd1709809a9da8a29392d2cbf118803d1ad2bb53a5cefdb73fabe8f8945e8e8b650025b1057075f9032387cb54747c2ad61754aa77aae8d894a78f2a0035bb57452724e5c9cff1532da7987bde5b1eb4973a25d30e52d1f1294f3b2a0035f21b231f2cd57682cae1fb2a3a3e8d9c07507ae39e54d3ab8755f41a94a7ef2900355a54452723e5eb25358f4a35abf24e83d16b50dc8f550e9f57d1f1295fec0900d48c2980f594bafdec44ecd950bcc755ae2566d99eb9fa980a08d46c1b159d8c94b7fb558ee7d076b28a5e8326e55bff9f54397c5845af41797b5101a8d1812a3a19297fdba91c66565ed4267a0d7abd43552eccc0a82fef8209a026ac01505f7e7e3f93ca61bc7a4145afd3f5ae53b91695595745af41d5b484025093735474225235e5dcf4c4bb05bea6a2d7e96a7ed4b2a0cac1cb06dfaca2d7697adef5f02ee5e5a82f5517ab0bd4e96fe4dd3ffd9f5da5fccfdca6bc836574ac3a5b4d01a8c9152a3a11a99a9e50b3ab5cfe5345afd3c5bc73e27b552e1babe8759a90a7eedeaace54072b8fe5595d2dac52ee328d535ef0c85327b750df541e9d7fb78a7e8edc7d4601a889affca31391aa2bf77c67bf4947afd3a5fc386455958bc759f81b74f45afdc8db757bc73cefdbf141e50feaba791aeacaeacbca1705556c55bdbd025093475574225275f9dbdb07542e6f5247a9e8b5bad0f32af732b21e4418bd565df982e63cb5b3f2780fff3b6e9a37ab65952f4a3c5032c782625f53006ae0679cac02d89f3caf3cd7b4c0893cbe207aad41ce8f54727ef3b7f7ab7e9c17cfa853d56795ef40b4cddcca8f22bc7ba517bf8a7ec7b1fa8102508339547412523dedad72db5abdaaa2d71bb43cab22e7337ff38e81750efcf3204e0fc6f3788319d4a0f0743e8f4ff9a38a7eef913a4501a8c1622a3a09a99ebcf0c97b546eebab2755f49a83d2352ad768ffc9eda7a2d7cb9d9fe9fb76f7226ad0793b6b4f37f6a39ae86f317967290035f0b3c5e824a4faf2f2a7b3a8dc16529ee215bd669bf30a7f87ab5cf3fc27e715ffaabef5ef7fdfde9ad89b0b758d67bfecafbc4473f4b7719eaa08a0061ec0139d84546f1edd5d057f481ea6fca119bd6edbf20787ef6e54615ee56fe5d1ebe6e826e5a5893deea6eb7cc1bbbb7a484df977ba5c01a8816fcd4d7902527ffa0f55152faef22715bd6e5bf2e235b936f6999247b357b5dcafbff16fa89a388abfdfbc5ec16e6af2e984d72b0035f07ce2c9dfaca87f79d47495aba079709ba7943dada2d76f6afe005d5355e9bb2a7aed94fcedd65b08fbef8ed179f6c031ca8f5fbcb811801afc9b8adebca83f794adbbb5595e6575e88a8c880ac7e76a7f2b3f2dc5325a7e439ecd1ebf79a3fc48e506c6a53ded2ca830501d4603d15bd8951ffba4f2da0aa36973a40356db6c0dfd5b6aa8e6fceb9f752f0a0cb65140034de062a7a23a3fef667f5165507bf8e3f70bd2744bf060b7ae31acfff5e4bd53548ce23fe5f56d1cf53364fe7f4f2b80cf003d01a5c0034b71b95bfa5d76951e5695ade4eb7eae9705ef5ce4bdd7ac399bad7b5f746377efde8e72a9b0757f2ad1f40ebf008a0d9f94e80a7a7f583ef0c78ca9dc70b5cab52377ef1a38d0bd55ecab34ffa3538ee63cadfd8a39fb14cbe5b7288aa7a8c020054824180cdcf3bd2bd4335812f46fcedd983f3bcef8077213c529da43c4def58e54d74beaa7c4bdccbdb7aad897eec5c17f174bc5754f4772e93ef1e347ddb5a3f8ef0e87a6fedbb8af24c8a7595f71970febffd9ff9bff33fe37f9647184087300db01dddab72af79df35de6636c7630d6fe2e425b49bc2776a56573ba8e3d4a5ca33287ab9d0f1ffc683302f513e968fe9f70866340003682515bd1150f3f26debcd15caf192bbc7abe86f5a363fc2a862d9e632e6519b29df6d99a072ce621829bf861f47f9353fa77cb70040cbb11470bbf273e7839557aec3d8fc41f53b15fd2dcbe66fc4fd1ab7b094f226455e25af8e0ffcb1f2cfe081a25f517e7c00a085bce84c748253b3f37eebfe268891f9d6f5032afafb95c91f765f5275f3bf5faf97df86259cfd58c44bfa7267006811b6036e6f0f2b4fe3c450de53df0311737c53f69881ad549d56501e50996b8d823af3cf7c9afa8002d07073a8e844a6f6e40f8b39155e5f4ad6df46a3bf53d9fce1ef350aeab2aaf2ba08d1cfd2c6bcb0d4c7158086f2b49faa177ca16af3b7aeafabaef35ef3de4a36fa1b95cda3e1eb9ae6e7a9785e4238fa3906a12b952f04d80d1168a0475574e252b37b561dae165498643975b2eaf5c2d6032debb8edefd503fde118fd0c83d81f1453598186b94d45272c35335fb07935bdbaf60a682b0f703d4395dddfc05b26576966e5991cafaae8f50739ffcebe689d550168003fab8b4e566a562f28afbcc7a22ce5785cc0b92afa9b4ed93754957c2bdc4b2247afdda5fe5bd539be02c008ce51d1494acdc8a3d9bdd42eb7fad378f7bfd106087a37c2aa9e537b53a7411ae0972b5f98318015e8a31faae8e4a4fe77b562a7b97cbc8092b73e7e4c4dfe77f6023b33aa2a784a9cf77398fcf56852be23e2fd0800f4c1812a3a31a97f3dafbcd90e2bfe55c33306bcacadc7073ca41650b9f96e82c713e4d87c68d0f3d88003149b110135db46452725f5a75faa2a3e90309ca7e0793f8cdc7c8171818afefdd2c879854b06b70235f2b3d1e864a47a7b5a6daad06ef3a93faae8df318dddad8a0b60a026de6b3e3a11a9bebcb1caa2aa0dfcedd6031297549e73bf869ab8cffc3acadfaafddcdbffdd22cabbf1758537c6b94745ff8efb910790deaf3cd5f70675b1f2a05fe7ffdbff99ff3bff3365a74b56d9ddcaffff05a062d32a5603ec4f7ed3fd8e9a4e35894766fbce9007cc7d4b9da53c82de6313a2df63acfc9cdd1f367ebce1dfd7db1abf47f56b77bd2af8a2a79f8b6a7960e3d96a5fe58b314f7f2c33b0d1ffacff371b2a1fc3c77a5c45af55478fa8e515808a35e95b4b577a4aada79ac077813c2fdb03e37c0bb6ae6f832f295f181ca1d657fdde6bbf571f515e9931fa1dabca03e73ccec03b157aa6481503e87cccf7a95dd585aaeec58bfc37f58528800ae5da339d8af577e53ddefbc5b30b7cabde1ff839b6cccd953f60bc64acf7be1fafdac0df52ebfcf0f7059a6788ccabeae6cdc37c57a8cec5c37cd7a98a819a00dec05a00f5f55bd58fc54ffca1ef6faac728df5e8d7eb6a6e5bdf0f7540bab26f233ff3a6efbfbc2c8fb1b34692d7dff2c5e3ca98ebb02feffd72514800aeca2a2138ff276bcf2988b3abd55edaf1e54d1cfd486fc48c23bfd7987bea6ac8d30bff260b5e8e7cd95777af4077f933ffcdeaebcbeff8b2afa1d72e50583580d13a8005301abcf1bc0d4c9cf857d8bdf7b08443f4f5bf3230b2f1ae3dbd1fde2d79ea0a29f2f5767aa367de0f92e8d070e46bf4bae3c1095750280cce656d1094779da47d5c5cff673ed8bdfe49e5107a9ba3f10bcc25fd1cd857ac9e3433cadb2adfc65a2ca1d46bd5850557b36009de51dbaa2138e7acfb7ae3d4abb0eef52a7abe8e718e43c55cd83e2aa5acb7f4a1e9310fd1ca979cebe777b9c41b59dff5d1ca2aa9a4db29b029091a7f944271bf596dffcb65355f3aa69de2dd01f20d1cfd195fea1b652557e3b5c5155b1b6bf07b97d540d1a0f3aad62ec89071eb2811090d1b75574b2516ff95b69953c98f02b6ad09ef1a776995a5ce5e61510ab582fc353703da070507910aa571d8c7ef7943c2890ad84814c3653d18946e5f3ea7955f22a7a37aae8b5e9f511e91e779173c64515fbf97bfaed20ad863812ff8ebe4b15fd0d52f2ca920032f05af4d14946e53a4155751bda6fa4fe60f3f4b0e8b569687e2490c3262a3a7e4a9e3ad7a5c16cfe5d3d7b23fa5ba4e4658f0164d0e6b9e24dc8df12ab9aa7eeb9e05e36377a5d1adec32ac7c0c05955ce01b21e1be27537baca8362730e0ef4d88fb62e210d348a377d894e321abbdbd56caa0a1e20f6a48a5e97e2f656397c4f45c7ef35afabdf75dbabe86fd36bde600a4022bf394527188d9ef7f2f734bcdc7cdbd48309bb3ec2bf6ccfa91c03c43cd622e752b75eb700aff334c1e86fd44bfe77e49d0c012458414527188d9cb752ae620ad73875868a5e9346ef4895ca175f57aae8f8bde4017ffd7ae6efa9a2cb292f12f58937f2ffedffec6daa1ffcb7c83930f0f70a4002ef4bcfb4b272e5bad53c394f9dfaa38a5e8fc62ec7a639deaa393a762f7903a8ba46fb7b554f0f8c3b5a79cc48919d0abdaaa2ffd9ef2befb73097aa83ff263957ad5c5b0148c0d6c0c5f39cf3dcfbb0fbdb9ac71344af4763778dca21d796b75ee4a7ea79fe1eece80f7d0f42cdf1c8c28f9cfcfb7bebdfaa07d8795be35c838faf520012789a597472d1d03c286f2195937756f35af0d1eb51b1b651a9d650d1b1cbe60fd22abf95fa9bbaa7d63da1a2d7cf91ef0e78caa23fa8abb28eca35cee5830a408f9655d1894543f3add29cbcb7bca73445af45c5f2e3ab1cdf582f51d1f1cbe6b5fdab30bdf22a901eec18bd6e15f951821f77f9316115720d0afc8d02d0230fce613d80d13b51e5e4a56b1f52d16b51f17ea15279bdffe8d865fb9baa62631fdf9db84345af59477e3cb5bacacd8f31ee54d16b966d7905a0473f52d18945af7f507b5df85c3c5dedaf2a7a2d2ad7a62a55ae1d157d5b3b272f30f535d58429a19ef9b2bfcabde855ae81973f55007ab4b18a4e2ccaf3213391bff5e49c6ad6e55e525eb52fc53c2ac732cb67aa9cbcc0d4a52a7aad7e76914afd9b4fc97771a2d72ad3f38ad501811ecda17c951f9d5c5dcebb9ae5e2d903acbc98af5fab54bbabe8d865f285c8822a97f95493a784deac720e10f440d81c1761b9f681003a29d734a841c93bcce5dc66f65015bd0ef596d7984ff527151dbb4cc7a85cfce1efb104d1eb3429ff8c392f02bca156f43a65f21d13003df28625d189d5d5beae72f10c82e835a8f75217fff12c8ce8b865f25db3c5540ebeb57e938a5ea789f9e2e92d2a07ef4c9aba9e81c74ae4bc1303748a172f69c280a326f4a8caf5acd36f4a8fabe875a8b7bcf35fea32bbfba9e8d8653a59e5e0c1754d7ce63f561e13906b612c0fe48b5ea34c7b2a003dfa838a4eacae956b07372f7dcaa395fc9daa525da7a2639729c712c4e6d1fed1f1db906707e4b08c8a8e5f2656060412eca8a213ab4bdda3bcf04a0e07aae83528add4fdf53dfa3ff56ed78d2a07cff36ff39d373f06c9b54e40eae047ff2c1ed00ca0071e84d4f5c7005bab1c5653ccaca8a655558acd5474dc32a55e84982f3407614d88db548e1503736c4fbe9102d023ef64169d585de82e9563b19369d50415bd06a5e58baa99558ae35474eca279c05a8e51f039c62134a5bd542aef8a993a18d03b1c02e89177048b4eac2eb493cae1bf54747c4acf1756a9522fce2e50a9bcb14f956bfb7b20eb2dea7ce535133c6aff3115fdb339f22642396ebf7b606174fca2f97706d023afa855644ff141cb3bac8d53a9fccdf02915bd06a5973a00d053d7521f73e518245ac5f810cfcff75d85a5d5483cd8ce03f7aad885d23b15a6da4d45c72e9affdde65ead10e8146f7e139d5c83dc412a871cd39968e452d767f080b5e8b865f287680a5f687a7be9e8d8bde4fd2afe53f9d15351fe6777509e52191db3977c119dfa786639151dbb4ca96344804e5b494527d6a0e6a548bd0e422a0ffcfba78a5e83f2943a48d38f79a2e316cd6b3aa4ce7ddf5245c7ee254f7d4b198fe0471197abe8d8bde40196293c06c71712d1b18be68b2100099abc1e79eebc235c0e5d1e405957a9dfee5207007a3f875497a8e8d8653b4de598b2ea63e4da1531c7fefca91b04fd400148b0b38a4eae41ccdb92a6f20753746cca5bea72afa92beeedab52ccad724cb5f5ae92b9d6ab301fcb7713a2d72a93676978cbeb14a9b3233c901040028fe8f58638d10936483da8bc625f2a8f0c8f8e4f794bfdd0bb5345c72dda67550affefa3e396c9cffc3d652e373f4a784445af59a60d540acfe58f8e5bb43b1480445d180c78884af57e151d9bf2f6b44ae167f7afa8e8d8454b5dfef768151db74c553ee3ceb11ae8512a45eab2c01ed3936b8f02a0b3c6ab411fd4b6944a95facc928ae5a96b293ce02d3a6ed17ceb7e4695e206151dbb689eea5766b47f595ed12ff52e89f75948e19904a9ef3b2c090c6430c8b7b6bd384aaa451423ffebe91a9522750be007540aef6098bac6869f8f57cdf3f9a3d72e5aea9d1af3a3b9e8d8455b420148b4a68a4eb04128c79effa96f9654bccb548a555474dca279cdfb146f53d171cb34da223fb9bc4f45af5da6d469b5b7abe8b845f354660019dca4a293acedada052f81b5deaed522ade852ac55a2a3a6ed1526f6d2fafa2e316cdcbfbd6c1ff7fedf50ea29fa1685ed027c5f52a3a6ed1fcc50540069babe8246b731e499d3a50c85bb946c7a66a3a57a5f8988a8e5b344f214cf161151db76837abba784dfde86728da87548ad43535d6550032f0a0a3fb5474a2b5b51354aa1fa9e8d8544da98bf0787a5a74dca29da3527c4245c72d9a37f5a94bead89ff5558a5faae8b845fb940290c9762a3ad1dada862a85472a7771d3a47e96ba6263ea1cfcb6bf7e19a92b03a6ae97d0efd7073019df0518a4e7dda98394fc0d273a2e55171700f5e10200c010de88253ad9da56ea7c723b5245c7a6eae202a03e5c000018c23b75a54ecf69427e769feaaf2a3a3655171700f5e10200c0309f53d109d7a6bea0522caca2e352b57101501f2e00000ce3a973135474d2b5a5d4f5dcb755d171a9dab800a80f17000042de3e373ae9dad0ab2a7547b9d43727eaadb67f007301505cbf5f1fc028daba4740ea72ae76978a8e4dd5c605407db800003022efa297bab56a3f4a5d4c669c62f39ffec405407db8000030aac35574f235b903558a1555745caa3e2e00eac30500805179cfedc75474023635cf6248e11904d171a9fab800a80f170000c6b4bd8a4ec0a6f65195820580fa171700f5e10200c098a65137a8e8246c62de112e45ea2e65d47b5c00d4870b000085bc4f797a5d742236addd54af7cb1f38c8a8e4bd5c705407db8000050d8c12a3a119b96bfc1f7ea832a3a26d5131700f5e10200406133aa3b54743236294fe15b5af5c2fbc147c7a47ae202a03e5c000028650dd58639f2d7aae954199f56d1b1a8beb800a80f1700004a3b5e452764d3f21bccb4aa88d515cffefb5fdb3f80b90028aedfaf0fa007b3a9bb557452362ddf0918af46e2bb047ba91755f4bfa77ae302a03e5c0000e8c92aea7f547462362d3fb2f0be06bba8b5941f636ca4beab1e52d1ff86fa131700f5e1020040cfbea6a21393a8d7b800a80f1700007ae639f357a9e8e424ea252e00eac305008024ef504fabe804252a1b1700f5e1020040b2ad54748212958d0b80fa700100208bb64c0da466c705407db8000090c5f4ea3a159da84445e302a03e5c0000c86641f5888a4e56a2227101501f2e000064f561d596f501a8797101501f2e000064b7a78a4e58a2b1e202a03e5c0000c8ee4dea54159db444a3c505407db80000508919d4152a3a7189468a0b80fa700100a03273aabfaae8e4258ae202a03e5c0000a894570a7c58452730d194b5fd03781d151db76827a8ba700100a072ab2ab6dba522b5fd02e05d2a3a6ed1f65375e10200402d3654afa9e844269a58db2f003c00f67e151dbb482baaba700100a036de33c07bf3472733916bfb0580f95b7c74ecb1ba594dadeac20500805aeda8a29399c80dc205c03875a78a8e3f525e3c6b7555272e0000d46e5f159dd04467a9141ba8e8b8454b7dfd89dead1e53d16b4c99ef8aeda0eae6df35fa798ae6bf750a2e00808e3a4845273575bb5fab146ba9e8b845fb8dcac53360ae51d1eb4ccc7b677c5af5c3452afa998af66f2a05170040871da1a2139bbadbe52ac54a2a3a6ed1ae52397950e027d569ea5ef5b27a48f9f7dc55cdaafae56a15fd0d8a963a60910b00a0e3bea6a2939bbad9f52ac578151db768b7abaeb843457f83a279ca630a2e00004cb5878a4e70ea5e7f5129deaea2e316ed15358d1a74d3aa5755f437289ab7ff4ec10500807fd94d314590ee5329a657a9db512fae06dd3b55f4bb17cd170fd3a9145c0000f87fdb2a160bea76fe6049fd065e760ade9475e183652315fdee45f3e383545c0000186273e5dbb0d1094fdd681195c23309a2e316ed2835e88e51d1ef5eb473552a2e00000cf361f5a48a4e7a1afcfcef3fc5612a3a6ed152c721b441ea00c06fab545c0000087921957b5474e2d360b78d4ae1ff7d74dc322da10655eaf37fe765bd537101006044f3a91b5474f2d3e0f60d95c283f8a2e396c9d3530795ffbed1ef5ca6d4c734c605008051cdac7ea9a237001accce57a93c9b203a76d1ee56756ecc5397372b2f4814fdce45bb4be5c005008031f94deb10c534c16ee435f4bd825e8a1fa9e8d865fa8c1a341babe8772dd3092a072e000014f671f5948ade0c68b04a7d06bf858a8e5ba69b54ea854893f877b94545bf6b99365539700100a0147f304c50d11b020d4efe004f31bb7a4945c72e53bf36eaa9c2862afa1dcbf4829a4de5c0050080d2665167aae84d8106a3efab5467a8e8d865f25802efefdf763e67ee57d1ef58a69fab5cb80000d013dfcef4f2c12c1a3498e55869ce8f8ca26397cde34fdaee5015fd6e655b57e5f25315bd46d13ea10074d872ca3bb8456f10d4eebc16440a6f78e3fdf6a36397c97b0ba42e4ed44fdeb73fc712dbdec638e74649a91725cb2b001d37a33a5c456f12d4def651a9726d37ed0f3faf4bd1366f550faae8772adb7e2a27dfc28f5ea748cfa9191400fccb27d5a32a7ac3a0f6759d4a35a77a5645c72fdb35caeb52b485c72ef86f18fd2e657b5a7960654e1e97d0eb92df3f510030c4fcea0215bd6950bbf2ba0f0ba8545eb73e3a7e2f79a3213f5a683aff8c17aae877e8a56faa2aecaea2d71b2dcfee584c0140c8b717733cffa5feb6874a35af7a5145c7ef25af4ce9c74e4de59fcdbbf5453f7b2f3dafe65155f0225f652fd8b75600302adfb23c56456f22d48ebcb77f8e2579bfaaa2e3f7da1f54ee5be239cca1ae54d1cfdc6b5f51559a499da6a2d79e3c7ff3e7c31f40291f55ec2cd8ded651a9fcadd81713d1f17bed6f6a59d514ef57b97f474fc79c5ed5c1d3fa3cce62ca25bf3de0cfcffcb9ed0fa0271e10e56947ac1bd0be7ccb3d07cf618f8e9f92bf95eea4fab971906fa3efa272ac7c38656babba79b6c56a6a7df501d5e4c72d005a6449f52b15bdd95133f33cfc85540e55ad2079adf29a1475f35cf8aab6ccf66d790018381f517f52d11b1f35afa3540e9e1698ba55f048f942c5b7aa5317302a62bcfa99cab1c04f941f9935718c030064e1a952be7dfbb88ade04a939bdac165639acacaa7c14e40fe573949722ce3965703ae5e7e47e2452e5d6d8fedbaca00060e07941124f3763abe16677bccaa597f9e7bde4a9a89e89b291ea652a9d57f2f31efec7a9ba16b9da550140a7f8f6f001caab9e456f8cd4df7c8bdd633872f06652be851ebd4e55f95bfb5dcaf3debfa77cd1b99dfadc1bf9fff67fe6ffce8bf8f89fadf29b7ed4c9ca7f1b00e8247feb3a4ce55c3c86f294733b5adf9e67d5c8495dacfc9801003acfcb0a7b2b581e0d34abb5542e5e8426f7c2396dcc7b0678aa2c0060327e63dc59b1985033ba5be5dc94676e75b38a5eab0bdda8e65200801178b1178fecbe5a456fa4545fb937a7f145de452a7aad41eeb76a36050028c82b9579c0d40b2a7a63a56af354b5f7aa9cbce46d9135e907a5b3157bea03408ffced695b75938ade64a9bafeacfc0c3f272fabfb2d55f7e8fb3af3ef7690eae7f2c5003050bc2cabe76b3fa9a2375ecadf49aa0a6baa8754f49a6dce6b09784f04004005fc2dd21f207e44c09a02d557d516b10baa2b54f49a6decf76a010500a8819fb17ae0a02f069e51d11b33a5e5f51a965155f06df22dd4632a7aed36e43b529ec5e20b5300401f785b53df7e3d52792ff9e8cd9a8ae79501af52fbaa5c2b048e640ee5257ddb3436c03fab2f3c7b5982180050a1c5d40eea7cf5bc8adec469680fa853d4a6ca4b37d76d45e57f5f4dbe10f0cf769ef25efa008086f3a3825595d7823f57b5f99673cefea1fc81ff79b5b86a0a4f41f4b76bdf85887eee7ee45d08fdc1efc1a8008096f2862c4ba96dd489ca2bd5794bdce88d7f50f280c9cb94975ffeb45a48359defe27c5579e39ee877aaa33b9537b15a540100069037af19af36515e09ef57ea5e157d2834392fe273ab3a5deda73ea3fc0cbfcdf3d27dc1e6c5a18e51fe408e7eef9cfd5dfd40f9ae11bbf70140477970e1bbd47a6a7bf56d75a6ba413dac7c6b38fa10a9aa97d47dea1ae50f797fa3ffa25a5bf9367e17769c7bbbf2f4443fbe98a052eedef8efe963f891838fb9b00200a0100f985b42ada43ea6b654bbaabdd5c16f7484f24877e70fee8979b19d89ffb9bfe1fa9ff5ffce03187d9c4fa9d5951f59ccae309ca7e0f916bd6781eca4f651fe3bfaeff993379af8b7f57fe77fc6ffacff374cdf0300000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000090c35453fd1fe30bc4565d7de2950000000049454e44ae426082, 'Makati City', 'Single', 'Filipino', '', 'Iglesia ni Cristo', '', '', '', '', '', '', '', '', '', '', '', 'Active'),
(4, '000-000-004', 'Salazar', 'Rodel', 'Sotto', 'rodelsalazar@gmail.com', 'Male', '5/16/2024', 'rodelsalazar@company.ccm', '8736232', '', '', '', '', '3135 Esperanza St. Makati City', '', '09923827382', 0x89504e470d0a1a0a0000000d4948445200000200000002000806000000f478d4fa0000000467414d410000b18f0bfc6105000000097048597300000ec100000ec101b8916bed0000001974455874536f667477617265007777772e696e6b73636170652e6f72679bee3c1a00002ed449444154785eeddd07942c659d86714432179028204124895e0541248b280b088a8a4a92b828b84b9445b280a2082a224124090a06b208224852941c452f822859969c332ceebeaf70f7deb9f39f99aafebeaaaeea7a7ee73ce7ec51ac9e19b6baababbe30150000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000020ddd46a6ef54eb58a5a5f6dad7653fba983dfe8fbea58759c3afd8d7efac67fe6beab26feb37ba81dd5a66a6db5ac5a48cdac0000400dc6a9f1eae36a27e50feab3d54dea11f5bf35f7a2ba47fd4e9da4f6575ba80f2a5f24bc5901008082a651ef569f5507a8f3d49dea9f2afa206e6aafa85b95ef30f8f7f085cb3b1400009de7dbf6feb0df56fd58dda2fcc1197da00e4a8fa94bd4d7d5ba6a760500c0409b49adaef651bf524fa8e843b24bf9cec604e531095baac5140000ade7dbdefe86ef5bf97e6e1e7d08d2d0ee521e90e8c720b32900001acf23e4fdbcfb68e50fb2e8038e8af7b2ba54edae3c08120080c69841f943ff64f5ac8a3ec8284fbea83a5cadaa0000a8ddf46ae287fe332afab0a26ae3620000509b95d489ea69157d28517ff20c0aaf8fc0ac020040361e88e6817c37abe8c3879ad34bca6b0faca9dea4000028cd53f67ea218bddfcefea6f6545e22190080514dab3cfdec5a157da850fbf25d018fd5f0a24b00000c318bda59ddaba20f116a7f5e74e862e5c19b3c1e00808e5b501daa18d4d7adbc89d226cacb3103003ac4cf85bde52dcff7bbdd5f94773164f74200187073297ff0bfa0a20f04ea66de8fc01702dc1100800133a7faa67a4e451f0044ce8f063c460000d0721ed5efc17d4faae80d9f28ea32b58c0200b4901783f1addde80d9e68ac5e539e3e38af0200b4c092ea7c15bda91395cd8f8d0e50def40900d0405eb2f748f5aa8adec88952ba53adad00000db29eba4f456fdc4439f35e03f32800401fbd55f9396df4464d54554f286f10c58a8200d0075eb3ff5115bd4113d5d1e5ca634e0000355840794df7e80d99a8ee9e575f54dc0d00800a6da01e53d11b31513fbb50cdaf00001979b7be6355f4c64bd4941e51eb2b004006cbab3b54f4864bd4c43c30759c0200f4c0cf54f756cceba73676ab7aa7020094e05bfe67a9e88d95a82d3dab3654008002fcadc9fbb4476fa8446dcce357bc31150060041b29b6eca541ecf76a3e050098cc34ea5015bd71120d4a0f280f6a050088474b9fa7a2374ca241eb45b5b102804ef32dd11b54f4464934a8fd53798b6100e8a4f1ea5e15bd411275a11314830301748af7557f5a456f8a445dea376a560500036f2bc5e23e4493f263b0b915000c2cef9af69a8ade0489badc6dca3b5d02c0c0d95d456f7c44f47af7a8c515000c8c3d54f4864744437b50bd570140ab79431f16f869665e71f109f5f264ff1935a3c7d4720a005aeb4815bdc15175f91be485ea18b597da44adacdea1e650236d533bbb9a572da9d6549f5707aa53d4d5ea7915bd1e55d3e38a3b01005ae91015bdb151be5e5297a96faa0dd482aa2a5eae7969e50b83e3d40415fd4c94af87d5520a005a633f15bda1517a8fa893d56755bfe78f2facb655a72b6f7d1bfdbc94d643ca3b640240e3fd978adec8a8f79e52bea5bfa2f2b88a269a49f971c3458aa99e79f38a996f5700d0589ee7ef75cea337312a97ff8ebf559b2b7fb8b689ef0c78ad7b4f6b8b7e372adf9d6a7e05008db3a9e29b5f7afe1b9ea6fcbcbdeddeacfcff177f56d1ef4ae5ba59cda200a03156531e9016bd6951b15e517eb63f8883befcd8e2e3ea1a15fdee543c0ffa9c4e0140df79da98a72c456f5654ac73d562aa0b3c5be12e15fd1da85827aaa68e0501d01173a93b54f4264563e7bfdd7aaa6bfc0d7667f58c8afe2e34761e6301007d31a3f20231d19b138dde0beacbaaeb7bc17bf39bb355f437a2d1f320d1ad1500d4cab71fcf50d11b138dde8dea5d0a936ca99e56d1df8b46cecb38afa200a036fbaae80d8946cea3fb0f570ce08a2da43cc02dfadbd1c879a120b61106508bb515d3fdcae5f5f93d5302a39b5af9e292ffff2a971fc54daf00a0325ee0e55115bd09519ce76efbef86e2d6555efd30fa7b52dc8f150054620675838ade7c28ee54d5b655fc9a620975bb8afeae14b79d0280ec7ea4a2371d8af38648ccd54e33a7ba52457f5f1a9e07052eaf00201b6f011bbde1d0f03c3deb4baadf1651eb28cfb73f5a5daa7c07c76bca7b9bd9e7947fde27d4fdcadfb6fddfff421dacfe5dadac6657fd344e3138b0787f532c170c208b45150bb614cb1ffe3baa7ef04631de22f858759f8a7ebe5ef345838febe3f7e382c003dc7ea9a29f8d86e725a501208917aab95e456f3234348f5cdf4ad5e96dcadff0ffa8a29fa98afc7b5eacb65033abba78fae4392afa9968789b2900e8d9412a7a73a1e1eda2eae01df6bcefbe6fe9f77bba9c17eff9a15a46d5c1170197a8e867a1a13dab3c9012004a5b55fd8f8ade5c6868df5655f3dd187feb6eeac878df15f09881aacdaa6e51d1cf4043f3dd3b169e0250ca6cea1e15bda9d0d03cd5cf0bd854c5c7fe82cafd5cbfaa3c606f3955253ffa68cbdfa3df79360a0014e6dbbad19b090ded0a55e50a6cbeb57e958a5ebbc9f9d18407a2791a5f559656cfabe8f569529e1a385e01c0983ea43c9a3d7a33a1493daeaa5ae1cfd3b88e546d7f04e375eaab1c8ce6ddf0a2d7a5a15dab3c76040046e455eb3c8f387a13a149f902697d5505ef14384145afdbd6ce547eac54859354f49a34b4dd14008cc883d9a2370f1a9a674754c183fc06f5b6f65f956fdbe736a3aa731a645b7b492da50060180fdc7a55456f1e34a96bd4342a278fd43e4545af3748bda03c8531b7772b7fc045af4993ba5cb13c358021fc81e65deba2370d9ad42bea3d2a272fa673818a5e6f10f3e3932fabdc0e50d1ebd1d0b65400f0ffbc9a5cf4664143fb86ca690ed5c651fe393a5ce5fc36eabb28b7aae8b568521e9859d5780c002de30fa1c754f4664193ba43f979732e73a9bfa8e8b5bad2512aa7d5143358c6ee5b0a00fef5261cbd49d0d0d654b978b64557bff94fd9fe2a27d6b0183baf0db0a402d0619e72c6c0bfb1fb8dcac54bfa76e9997f917652b978574416081abb8b14800ef39b40f4e64093f22de5e5552e5d18ed5f362f7894735d85efa8e875686855ad6501a0e17cf2476f0a34342f6293cb1755f41a34d5544faa45540e1e5fe19d0aa3d7a149796d86dc535a01349c9705edfa00b422f99ba91f93e4e0e9839e071fbd0ebdde752ad7ee751e5b10bd060dcdcb2903e890cd55f46640433b4be5304edda6a2d7a0a17d57e5f016f59c8a5e8326e55d3fabdcd00a4083f8db7f53f7946f5a6ba81cbcb14f747c1a9ec75c7843aa1c8e53d16bd0d07654003a601b15bd09d0d0bca84c8e856a96556ddfd5afeebc1992674ba4f212c1ac0b30768f28ef40096080f94df52e15bd09d0d03c602fd5d4ea6a151d9f466f579583d7bf8f8e4f43db4b011860ffa1a2939f86f68cf273fb545f50d1f169ecfcefc073fa536da8a2e3d3d01e5739fe7f1e40037974f5fd2a3af969683f55a97cb7c503aca2e353b10e53a9bc7cf3b32a3a3e0ded4b0ac000f22e60d1494fc3fb944af579151d9b8ae715fde651a94e55d1f16968fe82906b1a26800661bbdf62f943c76bf5a7f04c0b6f1e141d9fca75904af519151d9b86b785023040d652d1c94ec3f3b7c5549ba8e8d8543eafe8e739fd297c41c79a00c5f202611ebc0a60405ca8a2939d86b7b14a75a98a8e4dbdb5ad4ae5459da263d3f03ea6000c002f41cb5ce8e2cda752bc4d31ef3f6f7f50a9bce360746c1a9ea74e02180027a9e824a7e1f9b97daabd55746cea3d5fc02eaa522ca3a26353dc7805a0c5e6542faae804a7e11daf52b1c9523579739f147eaeed1d07a363d3f08e50005a6c17159ddc14973a02dadf52a3e3527ade2930d5af55746c1ade536a6605a0a5bca67a747253dc622a052bff5597c755a4ce06d85745c7a638af6501a0855656d1494d712f29cfdf4ff133151d9bf2b4be4af169151d97e272dc7501d0070cfe2bd79f54aa0754746cca53ead2c0de1d303a2e8ddc720a408bcca6bca25d744253dc192ac5222a3a2ee5eb5a95627af5aa8a8e4d71472b002de2ad6ca3939946eeeb2ac53a2a3a2ee5cb03d352b14473b91e55ded80a404bfc4e4527338ddc562ac5ce2a3a2ee56d5e95e202151d97466e5d05a005bc921d2bd1956f3d95c2b74aa3e352de5657294e51d17169e4fc3703d0027c13edad95548a4b54745cca9ba75aa6f89e8a8e4b23e78d9458130068812b557412d3e82da152b0dd723deda9527845c1e8b8347a3936c90250a105151bfff4d6dc2ac5df55745ccadb37548a1d54745c1abd73148006fb928a4e5e1a3b4f114bf1b08a8e4b794b5da3decb3d47c7a5d1f39e223329000dc5edffde9b41a560dd857af2025729b654d17169ec5257620450913914a3ff7b2ff5db0d7ffb7afab94af1ef2a3a2e8dddb10a40036da2a293968a354ea5785a45c7a5bca56ed9cc864dbde7a5aedfa40034cc8f5574d252b1bc7c728a7fa8e8b894b743558aed54745c2ad6fb148006f155f9432a3a61a9586f55296e57d171296f07a8143ba9e8b8542c6fa90ca041bc635774b252f1dea35278ebd4e8b894b7dd540aeff9101d978a75b502d020be2a8f4e562ade9a2a85e74947c7a5bca52e4873828a8e4bc5f26e8ab328000d71b98a4e562adee7548a4354745cca5bea33e8f354745c2a9e77be04d000d3a9175474a252f1bc88520aa697559f57b94c9dad71bd8a8e4dc54b5d8d1140262baae824a5721da652aca2a2e352beee57a9182c9bde150a4003785054749252b92e52296657afa9e8d894a70b558ab954745c2ad7cb8a65818106385b45272995ebbf55aa5b54746ccad35e2ac58754745c2adf1a0a409f714b335f73aa147e8c101d97f2b4824ab1bd8a8e4be5f3b6ca00fa6831159d9cd45babab14de2c253a2ea5f78c9a46a5f8818a8e4de5fb9502d0479ba9e8e4a4defab24ae1e5843d4f3a3a36a5e5e97ba96e54d1b1a97c5efa1a401f7d47452727f5d6af552a1f233a36a5e57dfc53bc45b16363de5297cf0690c023d7a313937aeb5935ad4ac1ae8cf97b4ea5ae3ef729151d9b7a6f2d05a04f1e54d18949bdb7924a31a37a4a45c7a6de3a45a53a4245c7a6dedb4301e883b9557452525afba8543f54d1b1a9b7727cd39ca0a26353effd5c01e8838fa8e8a4a4b46e50a996565eb6363a3e95eb2f6a6a956271151d9bd2f216d800fa6057159d94949e3f30529dafa26353b93cd325d55754746c4acb832abd1709809a9da8a29392d2cbf118803d1ad2bb53a5cefdb73fabe8f8945e8e8b650025b1057075f9032387cb54747c2ad61754aa77aae8d894a78f2a0035bb57452724e5c9cff1532da7987bde5b1eb4973a25d30e52d1f1294f3b2a0035f21b231f2cd57682cae1fb2a3a3e8d9c07507ae39e54d3ab8755f41a94a7ef2900355a54452723e5eb25358f4a35abf24e83d16b50dc8f550e9f57d1f1295fec0900d48c2980f594bafdec44ecd950bcc755ae2566d99eb9fa980a08d46c1b159d8c94b7fb558ee7d076b28a5e8326e55bff9f54397c5845af41797b5101a8d1812a3a19297fdba91c66565ed4267a0d7abd43552eccc0a82fef8209a026ac01505f7e7e3f93ca61bc7a4145afd3f5ae53b91695595745af41d5b484025093735474225235e5dcf4c4bb05bea6a2d7e96a7ed4b2a0cac1cb06dfaca2d7697adef5f02ee5e5a82f5517ab0bd4e96fe4dd3ffd9f5da5fccfdca6bc836574ac3a5b4d01a8c9152a3a11a99a9e50b3ab5cfe5345afd3c5bc73e27b552e1babe8759a90a7eedeaace54072b8fe5595d2dac52ee328d535ef0c85327b750df541e9d7fb78a7e8edc7d4601a889affca31391aa2bf77c67bf4947afd3a5fc386455958bc759f81b74f45afdc8db757bc73cefdbf141e50feaba791aeacaeacbca1705556c55bdbd025093475574225275f9dbdb07542e6f5247a9e8b5bad0f32af732b21e4418bd565df982e63cb5b3f2780fff3b6e9a37ab65952f4a3c5032c782625f53006ae0679cac02d89f3caf3cd7b4c0893cbe207aad41ce8f54727ef3b7f7ab7e9c17cfa853d56795ef40b4cddcca8f22bc7ba517bf8a7ec7b1fa8102508339547412523dedad72db5abdaaa2d71bb43cab22e7337ff38e81750efcf3204e0fc6f3788319d4a0f0743e8f4ff9a38a7eef913a4501a8c1622a3a09a99ebcf0c97b546eebab2755f49a83d2352ad768ffc9eda7a2d7cb9d9fe9fb76f7226ad0793b6b4f37f6a39ae86f317967290035f0b3c5e824a4faf2f2a7b3a8dc16529ee215bd669bf30a7f87ab5cf3fc27e715ffaabef5ef7fdfde9ad89b0b758d67bfecafbc4473f4b7719eaa08a0061ec0139d84546f1edd5d057f481ea6fca119bd6edbf20787ef6e54615ee56fe5d1ebe6e826e5a5893deea6eb7cc1bbbb7a484df977ba5c01a8816fcd4d7902527ffa0f55152faef22715bd6e5bf2e235b936f6999247b357b5dcafbff16fa89a388abfdfbc5ec16e6af2e984d72b0035f07ce2c9dfaca87f79d47495aba079709ba7943dada2d76f6afe005d5355e9bb2a7aed94fcedd65b08fbef8ed179f6c031ca8f5fbcb811801afc9b8adebca83f794adbbb5595e6575e88a8c880ac7e76a7f2b3f2dc5325a7e439ecd1ebf79a3fc48e506c6a53ded2ca830501d4603d15bd8951ffba4f2da0aa36973a40356db6c0dfd5b6aa8e6fceb9f752f0a0cb65140034de062a7a23a3fef667f5165507bf8e3f70bd2744bf060b7ae31acfff5e4bd53548ce23fe5f56d1cf53364fe7f4f2b80cf003d01a5c0034b71b95bfa5d76951e5695ade4eb7eae9705ef5ce4bdd7ac399bad7b5f746377efde8e72a9b0757f2ad1f40ebf008a0d9f94e80a7a7f583ef0c78ca9dc70b5cab52377ef1a38d0bd55ecab34ffa3538ee63cadfd8a39fb14cbe5b7288aa7a8c020054824180cdcf3bd2bd4335812f46fcedd983f3bcef8077213c529da43c4def58e54d74beaa7c4bdccbdb7aad897eec5c17f174bc5754f4772e93ef1e347ddb5a3f8ef0e87a6fedbb8af24c8a7595f71970febffd9ff9bff33fe37f9647184087300db01dddab72af79df35de6636c7630d6fe2e425b49bc2776a56573ba8e3d4a5ca33287ab9d0f1ffc683302f513e968fe9f70866340003682515bd1150f3f26debcd15caf192bbc7abe86f5a363fc2a862d9e632e6519b29df6d99a072ce621829bf861f47f9353fa77cb70040cbb11470bbf273e7839557aec3d8fc41f53b15fd2dcbe66fc4fd1ab7b094f226455e25af8e0ffcb1f2cfe081a25f517e7c00a085bce84c748253b3f37eebfe268891f9d6f5032afafb95c91f765f5275f3bf5faf97df86259cfd58c44bfa7267006811b6036e6f0f2b4fe3c450de53df0311737c53f69881ad549d56501e50996b8d823af3cf7c9afa8002d07073a8e844a6f6e40f8b39155e5f4ad6df46a3bf53d9fce1ef350aeab2aaf2ba08d1cfd2c6bcb0d4c7158086f2b49faa177ca16af3b7aeafabaef35ef3de4a36fa1b95cda3e1eb9ae6e7a9785e4238fa3906a12b952f04d80d1168a0475574e252b37b561dae165498643975b2eaf5c2d6032debb8edefd503fde118fd0c83d81f1453598186b94d45272c35335fb07935bdbaf60a682b0f703d4395dddfc05b26576966e5991cafaae8f50739ffcebe689d550168003fab8b4e566a562f28afbcc7a22ce5785cc0b92afa9b4ed93754957c2bdc4b2247afdda5fe5bd539be02c008ce51d1494acdc8a3d9bdd42eb7fad378f7bfd106087a37c2aa9e537b53a7411ae0972b5f98318015e8a31faae8e4a4fe77b562a7b97cbc8092b73e7e4c4dfe77f6023b33aa2a784a9cf77398fcf56852be23e2fd0800f4c1812a3a31a97f3dafbcd90e2bfe55c33306bcacadc7073ca41650b9f96e82c713e4d87c68d0f3d88003149b110135db46452725f5a75faa2a3e90309ca7e0793f8cdc7c8171818afefdd2c879854b06b70235f2b3d1e864a47a7b5a6daad06ef3a93faae8df318dddad8a0b60a026de6b3e3a11a9bebcb1caa2aa0dfcedd6031297549e73bf869ab8cffc3acadfaafddcdbffdd22cabbf1758537c6b94745ff8efb910790deaf3cd5f70675b1f2a05fe7ffdbff99ff3bff3365a74b56d9ddcaffff05a062d32a5603ec4f7ed3fd8e9a4e35894766fbce9007cc7d4b9da53c82de6313a2df63acfc9cdd1f367ebce1dfd7db1abf47f56b77bd2af8a2a79f8b6a7960e3d96a5fe58b314f7f2c33b0d1ffacff371b2a1fc3c77a5c45af55478fa8e515808a35e95b4b577a4aada79ac077813c2fdb03e37c0bb6ae6f832f295f181ca1d657fdde6bbf571f515e9931fa1dabca03e73ccec03b157aa6481503e87cccf7a95dd585aaeec58bfc37f58528800ae5da339d8af577e53ddefbc5b30b7cabde1ff839b6cccd953f60bc64acf7be1fafdac0df52ebfcf0f7059a6788ccabeae6cdc37c57a8cec5c37cd7a98a819a00dec05a00f5f55bd58fc54ffca1ef6faac728df5e8d7eb6a6e5bdf0f7540bab26f233ff3a6efbfbc2c8fb1b34692d7dff2c5e3ca98ebb02feffd72514800aeca2a2138ff276bcf2988b3abd55edaf1e54d1cfd486fc48c23bfd7987bea6ac8d30bff260b5e8e7cd95777af4077f933ffcdeaebcbeff8b2afa1d72e50583580d13a8005301abcf1bc0d4c9cf857d8bdf7b08443f4f5bf3230b2f1ae3dbd1fde2d79ea0a29f2f5767aa367de0f92e8d070e46bf4bae3c1095750280cce656d1094779da47d5c5cff673ed8bdfe49e5107a9ba3f10bcc25fd1cd857ac9e3433cadb2adfc65a2ca1d46bd5850557b36009de51dbaa2138e7acfb7ae3d4abb0eef52a7abe8e718e43c55cd83e2aa5acb7f4a1e9310fd1ca979cebe777b9c41b59dff5d1ca2aa9a4db29b029091a7f944271bf596dffcb65355f3aa69de2dd01f20d1cfd195fea1b652557e3b5c5155b1b6bf07b97d540d1a0f3aad62ec89071eb2811090d1b75574b2516ff95b69953c98f02b6ad09ef1a776995a5ce5e61510ab582fc353703da070507910aa571d8c7ef7943c2890ad84814c3653d18946e5f3ea7955f22a7a37aae8b5e9f511e91e779173c64515fbf97bfaed20ad863812ff8ebe4b15fd0d52f2ca920032f05af4d14946e53a4155751bda6fa4fe60f3f4b0e8b569687e2490c3262a3a7e4a9e3ad7a5c16cfe5d3d7b23fa5ba4e4658f0164d0e6b9e24dc8df12ab9aa7eeb9e05e36377a5d1adec32ac7c0c05955ce01b21e1be27537baca8362730e0ef4d88fb62e210d348a377d894e321abbdbd56caa0a1e20f6a48a5e97e2f656397c4f45c7ef35afabdf75dbabe86fd36bde600a4022bf394527188d9ef7f2f734bcdc7cdbd48309bb3ec2bf6ccfa91c03c43cd622e752b75eb700aff334c1e86fd44bfe77e49d0c012458414527188d9cb752ae620ad73875868a5e9346ef4895ca175f57aae8f8bde4017ffd7ae6efa9a2cb292f12f58937f2ffedffec6daa1ffcb7c83930f0f70a4002ef4bcfb4b272e5bad53c394f9dfaa38a5e8fc62ec7a639deaa393a762f7903a8ba46fb7b554f0f8c3b5a79cc48919d0abdaaa2ffd9ef2befb73097aa83ff263957ad5c5b0148c0d6c0c5f39cf3dcfbb0fbdb9ac71344af4763778dca21d796b75ee4a7ea79fe1eece80f7d0f42cdf1c8c28f9cfcfb7bebdfaa07d8795be35c838faf520012789a597472d1d03c286f2195937756f35af0d1eb51b1b651a9d650d1b1cbe60fd22abf95fa9bbaa7d63da1a2d7cf91ef0e78caa23fa8abb28eca35cee5830a408f9655d1894543f3add29cbcb7bca73445af45c5f2e3ab1cdf582f51d1f1cbe6b5fdab30bdf22a901eec18bd6e15f951821f77f9316115720d0afc8d02d0230fce613d80d13b51e5e4a56b1f52d16b51f17ea15279bdffe8d865fb9baa62631fdf9db84345af59477e3cb5bacacd8f31ee54d16b966d7905a0473f52d18945af7f507b5df85c3c5dedaf2a7a2d2ad7a62a55ae1d157d5b3b272f30f535d58429a19ef9b2bfcabde855ae81973f55007ab4b18a4e2ccaf3213391bff5e49c6ad6e55e525eb52fc53c2ac732cb67aa9cbcc0d4a52a7aad7e76914afd9b4fc97771a2d72ad3f38ad501811ecda17c951f9d5c5dcebb9ae5e2d903acbc98af5fab54bbabe8d865f285c8822a97f95493a784deac720e10f440d81c1761b9f681003a29d734a841c93bcce5dc66f65015bd0ef596d7984ff527151dbb4cc7a85cfce1efb104d1eb3429ff8c392f02bca156f43a65f21d13003df28625d189d5d5beae72f10c82e835a8f75217fff12c8ce8b865f25db3c5540ebeb57e938a5ea789f9e2e92d2a07ef4c9aba9e81c74ae4bc1303748a172f69c280a326f4a8caf5acd36f4a8fabe875a8b7bcf35fea32bbfba9e8d8653a59e5e0c1754d7ce63f561e13906b612c0fe48b5ea34c7b2a003dfa838a4eacae956b07372f7dcaa395fc9daa525da7a2639729c712c4e6d1fed1f1db906707e4b08c8a8e5f2656060412eca8a213ab4bdda3bcf04a0e07aae83528add4fdf53dfa3ff56ed78d2a07cff36ff39d373f06c9b54e40eae047ff2c1ed00ca0071e84d4f5c7005bab1c5653ccaca8a655558acd5474dc32a55e84982f3407614d88db548e1503736c4fbe9102d023ef64169d585de82e9563b19369d50415bd06a5e58baa99558ae35474eca279c05a8e51f039c62134a5bd542aef8a993a18d03b1c02e89177048b4eac2eb493cae1bf54747c4acf1756a9522fce2e50a9bcb14f956bfb7b20eb2dea7ce535133c6aff3115fdb339f22642396ebf7b606174fca2f97706d023afa855644ff141cb3bac8d53a9fccdf02915bd06a5973a00d053d7521f73e518245ac5f810cfcff75d85a5d5483cd8ce03f7aad885d23b15a6da4d45c72e9affdde65ead10e8146f7e139d5c83dc412a871cd39968e452d767f080b5e8b865f287680a5f687a7be9e8d8bde4fd2afe53f9d15351fe6777509e52191db3977c119dfa786639151dbb4ca96344804e5b494527d6a0e6a548bd0e422a0ffcfba78a5e83f2943a48d38f79a2e316cd6b3aa4ce7ddf5245c7ee254f7d4b198fe0471197abe8d8bde40196293c06c71712d1b18be68b2100099abc1e79eebc235c0e5d1e405957a9dfee5207007a3f875497a8e8d8653b4de598b2ea63e4da1531c7fefca91b04fd400148b0b38a4eae41ccdb92a6f20753746cca5bea72afa92beeedab52ccad724cb5f5ae92b9d6ab301fcb7713a2d72a93676978cbeb14a9b3233c901040028fe8f58638d10936483da8bc625f2a8f0c8f8e4f794bfdd0bb5345c72dda67550affefa3e396c9cffc3d652e373f4a784445af59a60d540acfe58f8e5bb43b1480445d180c78884af57e151d9bf2f6b44ae167f7afa8e8d8454b5dfef768151db74c553ee3ceb11ae8512a45eab2c01ed3936b8f02a0b3c6ab411fd4b6944a95facc928ae5a96b293ce02d3a6ed17ceb7e4695e206151dbb689eea5766b47f595ed12ff52e89f75948e19904a9ef3b2c090c6430c8b7b6bd384aaa451423ffebe91a9522750be007540aef6098bac6869f8f57cdf3f9a3d72e5aea9d1af3a3b9e8d8455b420148b4a68a4eb04128c79effa96f9654bccb548a555474dca279cdfb146f53d171cb34da223fb9bc4f45af5da6d469b5b7abe8b845f354660019dca4a293acedada052f81b5deaed522ade852ac55a2a3a6ed1526f6d2fafa2e316cdcbfbd6c1ff7fedf50ea29fa1685ed027c5f52a3a6ed1fcc50540069babe8246b731e499d3a50c85bb946c7a66a3a57a5f8988a8e5b344f214cf161151db76837abba784dfde86728da87548ad43535d6550032f0a0a3fb5474a2b5b51354aa1fa9e8d8544da98bf0787a5a74dca29da3527c4245c72d9a37f5a94bead89ff5558a5faae8b845fb940290c9762a3ad1dada862a85472a7771d3a47e96ba6263ea1cfcb6bf7e19a92b03a6ae97d0efd7073019df0518a4e7dda98394fc0d273a2e55171700f5e10200c010de88253ad9da56ea7c723b5245c7a6eae202a03e5c000018c23b75a54ecf69427e769feaaf2a3a3655171700f5e10200c0309f53d109d7a6bea0522caca2e352b57101501f2e00000ce3a973135474d2b5a5d4f5dcb755d171a9dab800a80f17000042de3e373ae9dad0ab2a7547b9d43727eaadb67f007301505cbf5f1fc028daba4740ea72ae76978a8e4dd5c605407db800003022efa297bab56a3f4a5d4c669c62f39ffec405407db8000030aac35574f235b903558a1555745caa3e2e00eac30500805179cfedc75474023635cf6248e11904d171a9fab800a80f170000c6b4bd8a4ec0a6f65195820580fa171700f5e10200c098a65137a8e8246c62de112e45ea2e65d47b5c00d4870b000085bc4f797a5d742236addd54af7cb1f38c8a8e4bd5c705407db8000050d8c12a3a119b96bfc1f7ea832a3a26d5131700f5e10200406133aa3b54743236294fe15b5af5c2fbc147c7a47ae202a03e5c000028650dd58639f2d7aae954199f56d1b1a8beb800a80f1700004a3b5e452764d3f21bccb4aa88d515cffefb5fdb3f80b90028aedfaf0fa007b3a9bb557452362ddf0918af46e2bb047ba91755f4bfa77ae302a03e5c0000e8c92aea7f547462362d3fb2f0be06bba8b5941f636ca4beab1e52d1ff86fa131700f5e1020040cfbea6a21393a8d7b800a80f1700007ae639f357a9e8e424ea252e00eac305008024ef504fabe804252a1b1700f5e1020040b2ad54748212958d0b80fa700100208bb64c0da466c705407db8000090c5f4ea3a159da84445e302a03e5c0000c86641f5888a4e56a2227101501f2e000064f561d596f501a8797101501f2e000064b7a78a4e58a2b1e202a03e5c0000c8ee4dea54159db444a3c505407db80000508919d4152a3a7189468a0b80fa700100a03273aabfaae8e4258ae202a03e5c0000a894570a7c58452730d194b5fd03781d151db76827a8ba700100a072ab2ab6dba522b5fd02e05d2a3a6ed1f65375e10200402d3654afa9e844269a58db2f003c00f67e151dbb482baaba700100a036de33c07bf3472733916bfb0580f95b7c74ecb1ba594dadeac20500805aeda8a29399c80dc205c03875a78a8e3f525e3c6b7555272e0000d46e5f159dd04467a9141ba8e8b8454b7dfd89dead1e53d16b4c99ef8aeda0eae6df35fa798ae6bf750a2e00808e3a4845273575bb5fab146ba9e8b845fb8dcac53360ae51d1eb4ccc7b677c5af5c3452afa998af66f2a05170040871da1a2139bbadbe52ac54a2a3a6ed1ae52397950e027d569ea5ef5b27a48f9f7dc55cdaafae56a15fd0d8a963a60910b00a0e3bea6a2939bbad9f52ac578151db768b7abaeb843457f83a279ca630a2e00004cb5878a4e70ea5e7f5129deaea2e316ed15358d1a74d3aa5755f437289ab7ff4ec10500807fd94d314590ee5329a657a9db512fae06dd3b55f4bb17cd170fd3a9145c0000f87fdb2a160bea76fe6049fd065e760ade9475e183652315fdee45f3e383545c0000186273e5dbb0d1094fdd681195c23309a2e316ed2835e88e51d1ef5eb473552a2e00000cf361f5a48a4e7a1afcfcef3fc5612a3a6ed152c721b441ea00c06fab545c0000087921957b5474e2d360b78d4ae1ff7d74dc322da10655eaf37fe765bd537101006044f3a91b5474f2d3e0f60d95c283f8a2e396c9d3530795ffbed1ef5ca6d4c734c605008051cdac7ea9a237001accce57a93c9b203a76d1ee56756ecc5397372b2f4814fdce45bb4be5c005008031f94deb10c534c16ee435f4bd825e8a1fa9e8d865fa8c1a341babe8772dd3092a072e000014f671f5948ade0c68b04a7d06bf858a8e5ba69b54ea854893f877b94545bf6b99365539700100a0147f304c50d11b020d4efe004f31bb7a4945c72e53bf36eaa9c2862afa1dcbf4829a4de5c0050080d2665167aae84d8106a3efab5467a8e8d865f25802efefdf763e67ee57d1ef58a69fab5cb80000d013dfcef4f2c12c1a3498e55869ce8f8ca26397cde34fdaee5015fd6e655b57e5f25315bd46d13ea10074d872ca3bb8456f10d4eebc16440a6f78e3fdf6a36397c97b0ba42e4ed44fdeb73fc712dbdec638e74649a91725cb2b001d37a33a5c456f12d4def651a9726d37ed0f3faf4bd1366f550faae8772adb7e2a27dfc28f5ea748cfa9191400fccb27d5a32a7ac3a0f6759d4a35a77a5645c72fdb35caeb52b485c72ef86f18fd2e657b5a7960654e1e97d0eb92df3f510030c4fcea0215bd6950bbf2ba0f0ba8545eb73e3a7e2f79a3213f5a683aff8c17aae877e8a56faa2aecaea2d71b2dcfee584c0140c8b717733cffa5feb6874a35af7a5145c7ef25af4ce9c74e4de59fcdbbf5453f7b2f3dafe65155f0225f652fd8b75600302adfb23c56456f22d48ebcb77f8e2579bfaaa2e3f7da1f54ee5be239cca1ae54d1cfdc6b5f51559a499da6a2d79e3c7ff3e7c31f40291f55ec2cd8ded651a9fcadd81713d1f17bed6f6a59d514ef57b97f474fc79c5ed5c1d3fa3cce62ca25bf3de0cfcffcb9ed0fa0271e10e56947ac1bd0be7ccb3d07cf618f8e9f92bf95eea4fab971906fa3efa272ac7c38656babba79b6c56a6a7df501d5e4c72d005a6449f52b15bdd95133f33cfc85540e55ad2079adf29a1475f35cf8aab6ccf66d790018381f517f52d11b1f35afa3540e9e1698ba55f048f942c5b7aa5317302a62bcfa99cab1c04f941f9935718c030064e1a952be7dfbb88ade04a939bdac165639acacaa7c14e40fe573949722ce3965703ae5e7e47e2452e5d6d8fedbaca00060e07941124f3763abe16677bccaa597f9e7bde4a9a89e89b291ea652a9d57f2f31efec7a9ba16b9da550140a7f8f6f001caab9e456f8cd4df7c8bdd633872f06652be851ebd4e55f95bfb5dcaf3debfa77cd1b99dfadc1bf9fff67fe6ffce8bf8f89fadf29b7ed4c9ca7f1b00e8247feb3a4ce55c3c86f294733b5adf9e67d5c8495dacfc9801003acfcb0a7b2b581e0d34abb5542e5e8426f7c2396dcc7b0678aa2c0060327e63dc59b1985033ba5be5dc94676e75b38a5eab0bdda8e65200801178b1178fecbe5a456fa4545fb937a7f145de452a7aad41eeb76a36050028c82b9579c0d40b2a7a63a56af354b5f7aa9cbce46d9135e907a5b3157bea03408ffced695b75938ade64a9bafeacfc0c3f272fabfb2d55f7e8fb3af3ef7690eae7f2c5003050bc2cabe76b3fa9a2375ecadf49aa0a6baa8754f49a6dce6b09784f04004005fc2dd21f207e44c09a02d557d516b10baa2b54f49a6decf76a010500a8819fb17ae0a02f069e51d11b33a5e5f51a965155f06df22dd4632a7aed36e43b529ec5e20b5300401f785b53df7e3d52792ff9e8cd9a8ae79501af52fbaa5c2b048e640ee5257ddb3436c03fab2f3c7b5982180050a1c5d40eea7cf5bc8adec469680fa853d4a6ca4b37d76d45e57f5f4dbe10f0cf769ef25efa008086f3a3825595d7823f57b5f99673cefea1fc81ff79b5b86a0a4f41f4b76bdf85887eee7ee45d08fdc1efc1a8008096f2862c4ba96dd489ca2bd5794bdce88d7f50f280c9cb94975ffeb45a48359defe27c5579e39ee877aaa33b9537b15a540100069037af19af36515e09ef57ea5e157d2834392fe273ab3a5deda73ea3fc0cbfcdf3d27dc1e6c5a18e51fe408e7eef9cfd5dfd40f9ae11bbf70140477970e1bbd47a6a7bf56d75a6ba413dac7c6b38fa10a9aa97d47dea1ae50f797fa3ffa25a5bf9367e17769c7bbbf2f4443fbe98a052eedef8efe963f891838fb9b00200a0100f985b42ada43ea6b654bbaabdd5c16f7484f24877e70fee8979b19d89ffb9bfe1fa9ff5ffce03187d9c4fa9d5951f59ccae309ca7e0f916bd6781eca4f651fe3bfaeff993379af8b7f57fe77fc6ffacff374cdf0300000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000090c35453fd1fe30bc4565d7de2950000000049454e44ae426082, 'Makati City', 'Single', 'Filipino', 'n/a', 'Iglesia ni Cristo', '', '', '', '', '', '', '', '', '', '', '', 'Active');
INSERT INTO `employee` (`emp_no`, `emp_id`, `lname`, `fname`, `mname`, `email`, `gender`, `birthdate`, `cp_email`, `tel_no`, `mothers_name`, `mothers_occ`, `fathers_name`, `fathers_occ`, `address`, `home_address`, `contact_no`, `image`, `pob`, `status_type`, `nationality`, `spouse_name`, `religion`, `elementary`, `high_school`, `college`, `course`, `work_experience`, `primary_con`, `contact_num`, `relationship`, `secondary_con`, `contact_num2`, `relationship2`, `status`) VALUES
(5, '000-000-005', 'Balista', 'Remy', 'Bulgar', 'remybalista@gmail.com', 'Male', '5/16/2024', 'remybalista@company.com', '', '', '', '', '', 'Marikina', '', '09293829832', 0x89504e470d0a1a0a0000000d4948445200000200000002000806000000f478d4fa0000000467414d410000b18f0bfc6105000000097048597300000ebc00000ebc0195bc72490000001974455874536f667477617265007777772e696e6b73636170652e6f72679bee3c1a00002ed449444154785eeddd07942c659d86714432179028204124895e0541248b280b088a8a4a92b828b84b9445b280a2082a224124090a06b208224852941c452f822859969c332ceebeaf70f7deb9f39f99aafebeaaaeea7a7ee73ce7ec51ac9e19b6baababbe30150000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000020ddd46a6ef54eb58a5a5f6dad7653fba983dfe8fbea58759c3afd8d7efac67fe6beab26feb37ba81dd5a66a6db5ac5a48cdac0000400dc6a9f1eae36a27e50feab3d54dea11f5bf35f7a2ba47fd4e9da4f6575ba80f2a5f24bc5901008082a651ef569f5507a8f3d49dea9f2afa206e6aafa85b95ef30f8f7f085cb3b1400009de7dbf6feb0df56fd58dda2fcc1197da00e4a8fa94bd4d7d5ba6a760500c0409b49adaef651bf524fa8e843b24bf9cec604e531095baac5140000ade7dbdefe86ef5bf97e6e1e7d08d2d0ee521e90e8c720b32900001acf23e4fdbcfb68e50fb2e8038e8af7b2ba54edae3c08120080c69841f943ff64f5ac8a3ec8284fbea83a5cadaa0000a8ddf46ae287fe332afab0a26ae3620000509b95d489ea69157d28517ff20c0aaf8fc0ac020040361e88e6817c37abe8c3879ad34bca6b0faca9dea4000028cd53f67ea218bddfcefea6f6545e22190080514dab3cfdec5a157da850fbf25d018fd5f0a24b00000c318bda59ddaba20f116a7f5e74e862e5c19b3c1e00808e5b501daa18d4d7adbc89d226cacb3103003ac4cf85bde52dcff7bbdd5f94773164f74200187073297ff0bfa0a20f04ea66de8fc01702dc1100800133a7faa67a4e451f0044ce8f063c460000d0721ed5efc17d4faae80d9f28ea32b58c0200b4901783f1addde80d9e68ac5e539e3e38af0200b4c092ea7c15bda91395cd8f8d0e50def40900d0405eb2f748f5aa8adec88952ba53adad00000db29eba4f456fdc4439f35e03f32800401fbd55f9396df4464d54554f286f10c58a8200d0075eb3ff5115bd4113d5d1e5ca634e0000355840794df7e80d99a8ee9e575f54dc0d00800a6da01e53d11b31513fbb50cdaf00001979b7be6355f4c64bd4941e51eb2b004006cbab3b54f4864bd4c43c30759c0200f4c0cf54f756cceba73676ab7aa7020094e05bfe67a9e88d95a82d3dab3654008002fcadc9fbb4476fa8446dcce357bc31150060041b29b6eca541ecf76a3e050098cc34ea5015bd71120d4a0f280f6a050088474b9fa7a2374ca241eb45b5b102804ef32dd11b54f4464934a8fd53798b6100e8a4f1ea5e15bd411275a11314830301748af7557f5a456f8a445dea376a560500036f2bc5e23e4493f263b0b915000c2cef9af69a8ade0489badc6dca3b5d02c0c0d95d456f7c44f47af7a8c515000c8c3d54f4864744437b50bd570140ab79431f16f869665e71f109f5f264ff1935a3c7d4720a005aeb4815bdc15175f91be485ea18b597da44adacdea1e650236d533bbb9a572da9d6549f5707aa53d4d5ea7915bd1e55d3e38a3b01005ae91015bdb151be5e5297a96faa0dd482aa2a5eae7969e50b83e3d40415fd4c94af87d5520a005a633f15bda1517a8fa893d56755bfe78f2facb655a72b6f7d1bfdbc94d643ca3b640240e3fd978adec8a8f79e52bea5bfa2f2b88a269a49f971c3458aa99e79f38a996f5700d0589ee7ef75cea337312a97ff8ebf559b2b7fb8b689ef0c78ad7b4f6b8b7e372adf9d6a7e05008db3a9e29b5f7afe1b9ea6fcbcbdeddeacfcff177f56d1ef4ae5ba59cda200a03156531e9016bd6951b15e517eb63f8883befcd8e2e3ea1a15fdee543c0ffa9c4e0140df79da98a72c456f5654ac73d562aa0b3c5be12e15fd1da85827aaa68e0501d01173a93b54f4264563e7bfdd7aaa6bfc0d7667f58c8afe2e34761e6301007d31a3f20231d19b138dde0beacbaaeb7bc17bf39bb355f437a2d1f320d1ad1500d4cab71fcf50d11b138dde8dea5d0a936ca99e56d1df8b46cecb38afa200a036fbaae80d8946cea3fb0f570ce08a2da43cc02dfadbd1c879a120b61106508bb515d3fdcae5f5f93d5302a39b5af9e292ffff2a971fc54daf00a0325ee0e55115bd09519ce76efbef86e2d6555efd30fa7b52dc8f150054620675838ade7c28ee54d5b655fc9a620975bb8afeae14b79d0280ec7ea4a2371d8af38648ccd54e33a7ba52457f5f1a9e07052eaf00201b6f011bbde1d0f03c3deb4baadf1651eb28cfb73f5a5daa7c07c76bca7b9bd9e7947fde27d4fdcadfb6fddfff421dacfe5dadac6657fd344e3138b0787f532c170c208b45150bb614cb1ffe3baa7ef04631de22f858759f8a7ebe5ef345838febe3f7e382c003dc7ea9a29f8d86e725a501208917aab95e456f3234348f5cdf4ad5e96dcadff0ffa8a29fa98afc7b5eacb65033abba78fae4392afa9968789b2900e8d9412a7a73a1e1eda2eae01df6bcefbe6fe9f77bba9c17eff9a15a46d5c1170197a8e867a1a13dab3c9012004a5b55fd8f8ade5c6868df5655f3dd187feb6eeac878df15f09881aacdaa6e51d1cf4043f3dd3b169e0250ca6cea1e15bda9d0d03cd5cf0bd854c5c7fe82cafd5cbfaa3c606f3955253ffa68cbdfa3df79360a0014e6dbbad19b090ded0a55e50a6cbeb57e958a5ebbc9f9d18407a2791a5f559656cfabe8f569529e1a385e01c0983ea43c9a3d7a33a1493daeaa5ae1cfd3b88e546d7f04e375eaab1c8ce6ddf0a2d7a5a15dab3c76040046e455eb3c8f387a13a149f902697d5505ef14384145afdbd6ce547eac54859354f49a34b4dd14008cc883d9a2370f1a9a674754c183fc06f5b6f65f956fdbe736a3aa731a645b7b492da50060180fdc7a55456f1e34a96bd4342a278fd43e4545af3748bda03c8531b7772b7fc045af4993ba5cb13c358021fc81e65deba2370d9ad42bea3d2a272fa673818a5e6f10f3e3932fabdc0e50d1ebd1d0b65400f0ffbc9a5cf4664143fb86ca690ed5c651fe393a5ce5fc36eabb28b7aae8b568521e9859d5780c002de30fa1c754f4664193ba43f979732e73a9bfa8e8b5bad2512aa7d5143358c6ee5b0a00fef5261cbd49d0d0d654b978b64557bff94fd9fe2a27d6b0183baf0db0a402d0619e72c6c0bfb1fb8dcac54bfa76e9997f917652b978574416081abb8b14800ef39b40f4e64093f22de5e5552e5d18ed5f362f7894735d85efa8e875686855ad6501a0e17cf2476f0a34342f6293cb1755f41a34d5544faa45540e1e5fe19d0aa3d7a149796d86dc535a01349c9705edfa00b422f99ba91f93e4e0e9839e071fbd0ebdde752ad7ee751e5b10bd060dcdcb2903e890cd55f46640433b4be5304edda6a2d7a0a17d57e5f016f59c8a5e8326e55d3fabdcd00a4083f8db7f53f7946f5a6ba81cbcb14f747c1a9ec75c7843aa1c8e53d16bd0d07654003a601b15bd09d0d0bca84c8e856a96556ddfd5afeebc1992674ba4f212c1ac0b30768f28ef40096080f94df52e15bd09d0d03c602fd5d4ea6a151d9f466f579583d7bf8f8e4f43db4b011860ffa1a2939f86f68cf273fb545f50d1f169ecfcefc073fa536da8a2e3d3d01e5739fe7f1e40037974f5fd2a3af969683f55a97cb7c503aca2e353b10e53a9bc7cf3b32a3a3e0ded4b0ac000f22e60d1494fc3fb944af579151d9b8ae715fde651a94e55d1f16968fe82906b1a26800661bbdf62f943c76bf5a7f04c0b6f1e141d9fca75904af519151d9b86b785023040d652d1c94ec3f3b7c5549ba8e8d8543eafe8e739fd297c41c79a00c5f202611ebc0a60405ca8a2939d86b7b14a75a98a8e4dbdb5ad4ae5459da263d3f03ea6000c002f41cb5ce8e2cda752bc4d31ef3f6f7f50a9bce360746c1a9ea74e02180027a9e824a7e1f9b97daabd55746cea3d5fc02eaa522ca3a26353dc7805a0c5e6542faae804a7e11daf52b1c9523579739f147eaeed1d07a363d3f08e50005a6c17159ddc14973a02dadf52a3e3527ade2930d5af55746c1ade536a6605a0a5bca67a747253dc622a052bff5597c755a4ce06d85745c7a638af6501a0855656d1494d712f29cfdf4ff133151d9bf2b4be4af169151d97e272dc7501d0070cfe2bd79f54aa0754746cca53ead2c0de1d303a2e8ddc720a408bcca6bca25d744253dc192ac5222a3a2ee5eb5a95627af5aa8a8e4d71472b002de2ad6ca3939946eeeb2ac53a2a3a2ee5cb03d352b14473b91e55ded80a404bfc4e4527338ddc562ac5ce2a3a2ee56d5e95e202151d97466e5d05a005bc921d2bd1956f3d95c2b74aa3e352de5657294e51d17169e4fc3703d0027c13edad95548a4b54745cca9ba75aa6f89e8a8e4b23e78d9458130068812b557412d3e82da152b0dd723deda9527845c1e8b8347a3936c90250a105151bfff4d6dc2ac5df55745ccadb37548a1d54745c1abd73148006fb928a4e5e1a3b4f114bf1b08a8e4b794b5da3decb3d47c7a5d1f39e223329000dc5edffde9b41a560dd857af2025729b654d17169ec5257620450913914a3ff7b2ff5db0d7ffb7afab94af1ef2a3a2e8dddb10a40036da2a293968a354ea5785a45c7a5bca56ed9cc864dbde7a5aedfa40034cc8f5574d252b1bc7c728a7fa8e8b894b743558aed54745c2ad6fb148006f155f9432a3a61a9586f55296e57d171296f07a8143ba9e8b8542c6fa90ca041bc635774b252f1dea35278ebd4e8b894b7dd540aeff9101d978a75b502d020be2a8f4e562ade9a2a85e74947c7a5bca52e4873828a8e4bc5f26e8ab328000d71b98a4e562adee7548a4354745cca5bea33e8f354745c2a9e77be04d000d3a9175474a252f1bc88520aa697559f57b94c9dad71bd8a8e4dc54b5d8d1140262baae824a5721da652aca2a2e352beee57a9182c9bde150a4003785054749252b92e52296657afa9e8d894a70b558ab954745c2ad7cb8a65818106385b45272995ebbf55aa5b54746ccad35e2ac58754745c2adf1a0a409f714b335f73aa147e8c101d97f2b4824ab1bd8a8e4be5f3b6ca00fa6831159d9cd45babab14de2c253a2ea5f78c9a46a5f8818a8e4de5fb9502d0479ba9e8e4a4defab24ae1e5843d4f3a3a36a5e5e97ba96e54d1b1a97c5efa1a401f7d47452727f5d6af552a1f233a36a5e57dfc53bc45b16363de5297cf0690c023d7a313937aeb5935ad4ac1ae8cf97b4ea5ae3ef729151d9b7a6f2d05a04f1e54d18949bdb7924a31a37a4a45c7a6de3a45a53a4245c7a6dedb4301e883b9557452525afba8543f54d1b1a9b7727cd39ca0a26353effd5c01e8838fa8e8a4a4b46e50a996565eb6363a3e95eb2f6a6a956271151d9bd2f216d800fa6057159d94949e3f30529dafa26353b93cd325d55754746c4acb832abd1709809a9da8a29392d2cbf118803d1ad2bb53a5cefdb73fabe8f8945e8e8b650025b1057075f9032387cb54747c2ad61754aa77aae8d894a78f2a0035bb57452724e5c9cff1532da7987bde5b1eb4973a25d30e52d1f1294f3b2a0035f21b231f2cd57682cae1fb2a3a3e8d9c07507ae39e54d3ab8755f41a94a7ef2900355a54452723e5eb25358f4a35abf24e83d16b50dc8f550e9f57d1f1295fec0900d48c2980f594bafdec44ecd950bcc755ae2566d99eb9fa980a08d46c1b159d8c94b7fb558ee7d076b28a5e8326e55bff9f54397c5845af41797b5101a8d1812a3a19297fdba91c66565ed4267a0d7abd43552eccc0a82fef8209a026ac01505f7e7e3f93ca61bc7a4145afd3f5ae53b91695595745af41d5b484025093735474225235e5dcf4c4bb05bea6a2d7e96a7ed4b2a0cac1cb06dfaca2d7697adef5f02ee5e5a82f5517ab0bd4e96fe4dd3ffd9f5da5fccfdca6bc836574ac3a5b4d01a8c9152a3a11a99a9e50b3ab5cfe5345afd3c5bc73e27b552e1babe8759a90a7eedeaace54072b8fe5595d2dac52ee328d535ef0c85327b750df541e9d7fb78a7e8edc7d4601a889affca31391aa2bf77c67bf4947afd3a5fc386455958bc759f81b74f45afdc8db757bc73cefdbf141e50feaba791aeacaeacbca1705556c55bdbd025093475574225275f9dbdb07542e6f5247a9e8b5bad0f32af732b21e4418bd565df982e63cb5b3f2780fff3b6e9a37ab65952f4a3c5032c782625f53006ae0679cac02d89f3caf3cd7b4c0893cbe207aad41ce8f54727ef3b7f7ab7e9c17cfa853d56795ef40b4cddcca8f22bc7ba517bf8a7ec7b1fa8102508339547412523dedad72db5abdaaa2d71bb43cab22e7337ff38e81750efcf3204e0fc6f3788319d4a0f0743e8f4ff9a38a7eef913a4501a8c1622a3a09a99ebcf0c97b546eebab2755f49a83d2352ad768ffc9eda7a2d7cb9d9fe9fb76f7226ad0793b6b4f37f6a39ae86f317967290035f0b3c5e824a4faf2f2a7b3a8dc16529ee215bd669bf30a7f87ab5cf3fc27e715ffaabef5ef7fdfde9ad89b0b758d67bfecafbc4473f4b7719eaa08a0061ec0139d84546f1edd5d057f481ea6fca119bd6edbf20787ef6e54615ee56fe5d1ebe6e826e5a5893deea6eb7cc1bbbb7a484df977ba5c01a8816fcd4d7902527ffa0f55152faef22715bd6e5bf2e235b936f6999247b357b5dcafbff16fa89a388abfdfbc5ec16e6af2e984d72b0035f07ce2c9dfaca87f79d47495aba079709ba7943dada2d76f6afe005d5355e9bb2a7aed94fcedd65b08fbef8ed179f6c031ca8f5fbcb811801afc9b8adebca83f794adbbb5595e6575e88a8c880ac7e76a7f2b3f2dc5325a7e439ecd1ebf79a3fc48e506c6a53ded2ca830501d4603d15bd8951ffba4f2da0aa36973a40356db6c0dfd5b6aa8e6fceb9f752f0a0cb65140034de062a7a23a3fef667f5165507bf8e3f70bd2744bf060b7ae31acfff5e4bd53548ce23fe5f56d1cf53364fe7f4f2b80cf003d01a5c0034b71b95bfa5d76951e5695ade4eb7eae9705ef5ce4bdd7ac399bad7b5f746377efde8e72a9b0757f2ad1f40ebf008a0d9f94e80a7a7f583ef0c78ca9dc70b5cab52377ef1a38d0bd55ecab34ffa3538ee63cadfd8a39fb14cbe5b7288aa7a8c020054824180cdcf3bd2bd4335812f46fcedd983f3bcef8077213c529da43c4def58e54d74beaa7c4bdccbdb7aad897eec5c17f174bc5754f4772e93ef1e347ddb5a3f8ef0e87a6fedbb8af24c8a7595f71970febffd9ff9bff33fe37f9647184087300db01dddab72af79df35de6636c7630d6fe2e425b49bc2776a56573ba8e3d4a5ca33287ab9d0f1ffc683302f513e968fe9f70866340003682515bd1150f3f26debcd15caf192bbc7abe86f5a363fc2a862d9e632e6519b29df6d99a072ce621829bf861f47f9353fa77cb70040cbb11470bbf273e7839557aec3d8fc41f53b15fd2dcbe66fc4fd1ab7b094f226455e25af8e0ffcb1f2cfe081a25f517e7c00a085bce84c748253b3f37eebfe268891f9d6f5032afafb95c91f765f5275f3bf5faf97df86259cfd58c44bfa7267006811b6036e6f0f2b4fe3c450de53df0311737c53f69881ad549d56501e50996b8d823af3cf7c9afa8002d07073a8e844a6f6e40f8b39155e5f4ad6df46a3bf53d9fce1ef350aeab2aaf2ba08d1cfd2c6bcb0d4c7158086f2b49faa177ca16af3b7aeafabaef35ef3de4a36fa1b95cda3e1eb9ae6e7a9785e4238fa3906a12b952f04d80d1168a0475574e252b37b561dae165498643975b2eaf5c2d6032debb8edefd503fde118fd0c83d81f1453598186b94d45272c35335fb07935bdbaf60a682b0f703d4395dddfc05b26576966e5991cafaae8f50739ffcebe689d550168003fab8b4e566a562f28afbcc7a22ce5785cc0b92afa9b4ed93754957c2bdc4b2247afdda5fe5bd539be02c008ce51d1494acdc8a3d9bdd42eb7fad378f7bfd106087a37c2aa9e537b53a7411ae0972b5f98318015e8a31faae8e4a4fe77b562a7b97cbc8092b73e7e4c4dfe77f6023b33aa2a784a9cf77398fcf56852be23e2fd0800f4c1812a3a31a97f3dafbcd90e2bfe55c33306bcacadc7073ca41650b9f96e82c713e4d87c68d0f3d88003149b110135db46452725f5a75faa2a3e90309ca7e0793f8cdc7c8171818afefdd2c879854b06b70235f2b3d1e864a47a7b5a6daad06ef3a93faae8df318dddad8a0b60a026de6b3e3a11a9bebcb1caa2aa0dfcedd6031297549e73bf869ab8cffc3acadfaafddcdbffdd22cabbf1758537c6b94745ff8efb910790deaf3cd5f70675b1f2a05fe7ffdbff99ff3bff3365a74b56d9ddcaffff05a062d32a5603ec4f7ed3fd8e9a4e35894766fbce9007cc7d4b9da53c82de6313a2df63acfc9cdd1f367ebce1dfd7db1abf47f56b77bd2af8a2a79f8b6a7960e3d96a5fe58b314f7f2c33b0d1ffacff371b2a1fc3c77a5c45af55478fa8e515808a35e95b4b577a4aada79ac077813c2fdb03e37c0bb6ae6f832f295f181ca1d657fdde6bbf571f515e9931fa1dabca03e73ccec03b157aa6481503e87cccf7a95dd585aaeec58bfc37f58528800ae5da339d8af577e53ddefbc5b30b7cabde1ff839b6cccd953f60bc64acf7be1fafdac0df52ebfcf0f7059a6788ccabeae6cdc37c57a8cec5c37cd7a98a819a00dec05a00f5f55bd58fc54ffca1ef6faac728df5e8d7eb6a6e5bdf0f7540bab26f233ff3a6efbfbc2c8fb1b34692d7dff2c5e3ca98ebb02feffd72514800aeca2a2138ff276bcf2988b3abd55edaf1e54d1cfd486fc48c23bfd7987bea6ac8d30bff260b5e8e7cd95777af4077f933ffcdeaebcbeff8b2afa1d72e50583580d13a8005301abcf1bc0d4c9cf857d8bdf7b08443f4f5bf3230b2f1ae3dbd1fde2d79ea0a29f2f5767aa367de0f92e8d070e46bf4bae3c1095750280cce656d1094779da47d5c5cff673ed8bdfe49e5107a9ba3f10bcc25fd1cd857ac9e3433cadb2adfc65a2ca1d46bd5850557b36009de51dbaa2138e7acfb7ae3d4abb0eef52a7abe8e718e43c55cd83e2aa5acb7f4a1e9310fd1ca979cebe777b9c41b59dff5d1ca2aa9a4db29b029091a7f944271bf596dffcb65355f3aa69de2dd01f20d1cfd195fea1b652557e3b5c5155b1b6bf07b97d540d1a0f3aad62ec89071eb2811090d1b75574b2516ff95b69953c98f02b6ad09ef1a776995a5ce5e61510ab582fc353703da070507910aa571d8c7ef7943c2890ad84814c3653d18946e5f3ea7955f22a7a37aae8b5e9f511e91e779173c64515fbf97bfaed20ad863812ff8ebe4b15fd0d52f2ca920032f05af4d14946e53a4155751bda6fa4fe60f3f4b0e8b569687e2490c3262a3a7e4a9e3ad7a5c16cfe5d3d7b23fa5ba4e4658f0164d0e6b9e24dc8df12ab9aa7eeb9e05e36377a5d1adec32ac7c0c05955ce01b21e1be27537baca8362730e0ef4d88fb62e210d348a377d894e321abbdbd56caa0a1e20f6a48a5e97e2f656397c4f45c7ef35afabdf75dbabe86fd36bde600a4022bf394527188d9ef7f2f734bcdc7cdbd48309bb3ec2bf6ccfa91c03c43cd622e752b75eb700aff334c1e86fd44bfe77e49d0c012458414527188d9cb752ae620ad73875868a5e9346ef4895ca175f57aae8f8bde4017ffd7ae6efa9a2cb292f12f58937f2ffedffec6daa1ffcb7c83930f0f70a4002ef4bcfb4b272e5bad53c394f9dfaa38a5e8fc62ec7a639deaa393a762f7903a8ba46fb7b554f0f8c3b5a79cc48919d0abdaaa2ffd9ef2befb73097aa83ff263957ad5c5b0148c0d6c0c5f39cf3dcfbb0fbdb9ac71344af4763778dca21d796b75ee4a7ea79fe1eece80f7d0f42cdf1c8c28f9cfcfb7bebdfaa07d8795be35c838faf520012789a597472d1d03c286f2195937756f35af0d1eb51b1b651a9d650d1b1cbe60fd22abf95fa9bbaa7d63da1a2d7cf91ef0e78caa23fa8abb28eca35cee5830a408f9655d1894543f3add29cbcb7bca73445af45c5f2e3ab1cdf582f51d1f1cbe6b5fdab30bdf22a901eec18bd6e15f951821f77f9316115720d0afc8d02d0230fce613d80d13b51e5e4a56b1f52d16b51f17ea15279bdffe8d865fb9baa62631fdf9db84345af59477e3cb5bacacd8f31ee54d16b966d7905a0473f52d18945af7f507b5df85c3c5dedaf2a7a2d2ad7a62a55ae1d157d5b3b272f30f535d58429a19ef9b2bfcabde855ae81973f55007ab4b18a4e2ccaf3213391bff5e49c6ad6e55e525eb52fc53c2ac732cb67aa9cbcc0d4a52a7aad7e76914afd9b4fc97771a2d72ad3f38ad501811ecda17c951f9d5c5dcebb9ae5e2d903acbc98af5fab54bbabe8d865f285c8822a97f95493a784deac720e10f440d81c1761b9f681003a29d734a841c93bcce5dc66f65015bd0ef596d7984ff527151dbb4cc7a85cfce1efb104d1eb3429ff8c392f02bca156f43a65f21d13003df28625d189d5d5beae72f10c82e835a8f75217fff12c8ce8b865f25db3c5540ebeb57e938a5ea789f9e2e92d2a07ef4c9aba9e81c74ae4bc1303748a172f69c280a326f4a8caf5acd36f4a8fabe875a8b7bcf35fea32bbfba9e8d8653a59e5e0c1754d7ce63f561e13906b612c0fe48b5ea34c7b2a003dfa838a4eacae956b07372f7dcaa395fc9daa525da7a2639729c712c4e6d1fed1f1db906707e4b08c8a8e5f2656060412eca8a213ab4bdda3bcf04a0e07aae83528add4fdf53dfa3ff56ed78d2a07cff36ff39d373f06c9b54e40eae047ff2c1ed00ca0071e84d4f5c7005bab1c5653ccaca8a655558acd5474dc32a55e84982f3407614d88db548e1503736c4fbe9102d023ef64169d585de82e9563b19369d50415bd06a5e58baa99558ae35474eca279c05a8e51f039c62134a5bd542aef8a993a18d03b1c02e89177048b4eac2eb493cae1bf54747c4acf1756a9522fce2e50a9bcb14f956bfb7b20eb2dea7ce535133c6aff3115fdb339f22642396ebf7b606174fca2f97706d023afa855644ff141cb3bac8d53a9fccdf02915bd06a5973a00d053d7521f73e518245ac5f810cfcff75d85a5d5483cd8ce03f7aad885d23b15a6da4d45c72e9affdde65ead10e8146f7e139d5c83dc412a871cd39968e452d767f080b5e8b865f287680a5f687a7be9e8d8bde4fd2afe53f9d15351fe6777509e52191db3977c119dfa786639151dbb4ca96344804e5b494527d6a0e6a548bd0e422a0ffcfba78a5e83f2943a48d38f79a2e316cd6b3aa4ce7ddf5245c7ee254f7d4b198fe0471197abe8d8bde40196293c06c71712d1b18be68b2100099abc1e79eebc235c0e5d1e405957a9dfee5207007a3f875497a8e8d8653b4de598b2ea63e4da1531c7fefca91b04fd400148b0b38a4eae41ccdb92a6f20753746cca5bea72afa92beeedab52ccad724cb5f5ae92b9d6ab301fcb7713a2d72a93676978cbeb14a9b3233c901040028fe8f58638d10936483da8bc625f2a8f0c8f8e4f794bfdd0bb5345c72dda67550affefa3e396c9cffc3d652e373f4a784445af59a60d540acfe58f8e5bb43b1480445d180c78884af57e151d9bf2f6b44ae167f7afa8e8d8454b5dfef768151db74c553ee3ceb11ae8512a45eab2c01ed3936b8f02a0b3c6ab411fd4b6944a95facc928ae5a96b293ce02d3a6ed17ceb7e4695e206151dbb689eea5766b47f595ed12ff52e89f75948e19904a9ef3b2c090c6430c8b7b6bd384aaa451423ffebe91a9522750be007540aef6098bac6869f8f57cdf3f9a3d72e5aea9d1af3a3b9e8d8455b420148b4a68a4eb04128c79effa96f9654bccb548a555474dca279cdfb146f53d171cb34da223fb9bc4f45af5da6d469b5b7abe8b845f354660019dca4a293acedada052f81b5deaed522ade852ac55a2a3a6ed1526f6d2fafa2e316cdcbfbd6c1ff7fedf50ea29fa1685ed027c5f52a3a6ed1fcc50540069babe8246b731e499d3a50c85bb946c7a66a3a57a5f8988a8e5b344f214cf161151db76837abba784dfde86728da87548ad43535d6550032f0a0a3fb5474a2b5b51354aa1fa9e8d8544da98bf0787a5a74dca29da3527c4245c72d9a37f5a94bead89ff5558a5faae8b845fb940290c9762a3ad1dada862a85472a7771d3a47e96ba6263ea1cfcb6bf7e19a92b03a6ae97d0efd7073019df0518a4e7dda98394fc0d273a2e55171700f5e10200c010de88253ad9da56ea7c723b5245c7a6eae202a03e5c000018c23b75a54ecf69427e769feaaf2a3a3655171700f5e10200c0309f53d109d7a6bea0522caca2e352b57101501f2e00000ce3a973135474d2b5a5d4f5dcb755d171a9dab800a80f17000042de3e373ae9dad0ab2a7547b9d43727eaadb67f007301505cbf5f1fc028daba4740ea72ae76978a8e4dd5c605407db800003022efa297bab56a3f4a5d4c669c62f39ffec405407db8000030aac35574f235b903558a1555745caa3e2e00eac30500805179cfedc75474023635cf6248e11904d171a9fab800a80f170000c6b4bd8a4ec0a6f65195820580fa171700f5e10200c098a65137a8e8246c62de112e45ea2e65d47b5c00d4870b000085bc4f797a5d742236addd54af7cb1f38c8a8e4bd5c705407db8000050d8c12a3a119b96bfc1f7ea832a3a26d5131700f5e10200406133aa3b54743236294fe15b5af5c2fbc147c7a47ae202a03e5c000028650dd58639f2d7aae954199f56d1b1a8beb800a80f1700004a3b5e452764d3f21bccb4aa88d515cffefb5fdb3f80b90028aedfaf0fa007b3a9bb557452362ddf0918af46e2bb047ba91755f4bfa77ae302a03e5c0000e8c92aea7f547462362d3fb2f0be06bba8b5941f636ca4beab1e52d1ff86fa131700f5e1020040cfbea6a21393a8d7b800a80f1700007ae639f357a9e8e424ea252e00eac305008024ef504fabe804252a1b1700f5e1020040b2ad54748212958d0b80fa700100208bb64c0da466c705407db8000090c5f4ea3a159da84445e302a03e5c0000c86641f5888a4e56a2227101501f2e000064f561d596f501a8797101501f2e000064b7a78a4e58a2b1e202a03e5c0000c8ee4dea54159db444a3c505407db80000508919d4152a3a7189468a0b80fa700100a03273aabfaae8e4258ae202a03e5c0000a894570a7c58452730d194b5fd03781d151db76827a8ba700100a072ab2ab6dba522b5fd02e05d2a3a6ed1f65375e10200402d3654afa9e844269a58db2f003c00f67e151dbb482baaba700100a036de33c07bf3472733916bfb0580f95b7c74ecb1ba594dadeac20500805aeda8a29399c80dc205c03875a78a8e3f525e3c6b7555272e0000d46e5f159dd04467a9141ba8e8b8454b7dfd89dead1e53d16b4c99ef8aeda0eae6df35fa798ae6bf750a2e00808e3a4845273575bb5fab146ba9e8b845fb8dcac53360ae51d1eb4ccc7b677c5af5c3452afa998af66f2a05170040871da1a2139bbadbe52ac54a2a3a6ed1ae52397950e027d569ea5ef5b27a48f9f7dc55cdaafae56a15fd0d8a963a60910b00a0e3bea6a2939bbad9f52ac578151db768b7abaeb843457f83a279ca630a2e00004cb5878a4e70ea5e7f5129deaea2e316ed15358d1a74d3aa5755f437289ab7ff4ec10500807fd94d314590ee5329a657a9db512fae06dd3b55f4bb17cd170fd3a9145c0000f87fdb2a160bea76fe6049fd065e760ade9475e183652315fdee45f3e383545c0000186273e5dbb0d1094fdd681195c23309a2e316ed2835e88e51d1ef5eb473552a2e00000cf361f5a48a4e7a1afcfcef3fc5612a3a6ed152c721b441ea00c06fab545c0000087921957b5474e2d360b78d4ae1ff7d74dc322da10655eaf37fe765bd537101006044f3a91b5474f2d3e0f60d95c283f8a2e396c9d3530795ffbed1ef5ca6d4c734c605008051cdac7ea9a237001accce57a93c9b203a76d1ee56756ecc5397372b2f4814fdce45bb4be5c005008031f94deb10c534c16ee435f4bd825e8a1fa9e8d865fa8c1a341babe8772dd3092a072e000014f671f5948ade0c68b04a7d06bf858a8e5ba69b54ea854893f877b94545bf6b99365539700100a0147f304c50d11b020d4efe004f31bb7a4945c72e53bf36eaa9c2862afa1dcbf4829a4de5c0050080d2665167aae84d8106a3efab5467a8e8d865f25802efefdf763e67ee57d1ef58a69fab5cb80000d013dfcef4f2c12c1a3498e55869ce8f8ca26397cde34fdaee5015fd6e655b57e5f25315bd46d13ea10074d872ca3bb8456f10d4eebc16440a6f78e3fdf6a36397c97b0ba42e4ed44fdeb73fc712dbdec638e74649a91725cb2b001d37a33a5c456f12d4def651a9726d37ed0f3faf4bd1366f550faae8772adb7e2a27dfc28f5ea748cfa9191400fccb27d5a32a7ac3a0f6759d4a35a77a5645c72fdb35caeb52b485c72ef86f18fd2e657b5a7960654e1e97d0eb92df3f510030c4fcea0215bd6950bbf2ba0f0ba8545eb73e3a7e2f79a3213f5a683aff8c17aae877e8a56faa2aecaea2d71b2dcfee584c0140c8b717733cffa5feb6874a35af7a5145c7ef25af4ce9c74e4de59fcdbbf5453f7b2f3dafe65155f0225f652fd8b75600302adfb23c56456f22d48ebcb77f8e2579bfaaa2e3f7da1f54ee5be239cca1ae54d1cfdc6b5f51559a499da6a2d79e3c7ff3e7c31f40291f55ec2cd8ded651a9fcadd81713d1f17bed6f6a59d514ef57b97f474fc79c5ed5c1d3fa3cce62ca25bf3de0cfcffcb9ed0fa0271e10e56947ac1bd0be7ccb3d07cf618f8e9f92bf95eea4fab971906fa3efa272ac7c38656babba79b6c56a6a7df501d5e4c72d005a6449f52b15bdd95133f33cfc85540e55ad2079adf29a1475f35cf8aab6ccf66d790018381f517f52d11b1f35afa3540e9e1698ba55f048f942c5b7aa5317302a62bcfa99cab1c04f941f9935718c030064e1a952be7dfbb88ade04a939bdac165639acacaa7c14e40fe573949722ce3965703ae5e7e47e2452e5d6d8fedbaca00060e07941124f3763abe16677bccaa597f9e7bde4a9a89e89b291ea652a9d57f2f31efec7a9ba16b9da550140a7f8f6f001caab9e456f8cd4df7c8bdd633872f06652be851ebd4e55f95bfb5dcaf3debfa77cd1b99dfadc1bf9fff67fe6ffce8bf8f89fadf29b7ed4c9ca7f1b00e8247feb3a4ce55c3c86f294733b5adf9e67d5c8495dacfc9801003acfcb0a7b2b581e0d34abb5542e5e8426f7c2396dcc7b0678aa2c0060327e63dc59b1985033ba5be5dc94676e75b38a5eab0bdda8e65200801178b1178fecbe5a456fa4545fb937a7f145de452a7aad41eeb76a36050028c82b9579c0d40b2a7a63a56af354b5f7aa9cbce46d9135e907a5b3157bea03408ffced695b75938ade64a9bafeacfc0c3f272fabfb2d55f7e8fb3af3ef7690eae7f2c5003050bc2cabe76b3fa9a2375ecadf49aa0a6baa8754f49a6dce6b09784f04004005fc2dd21f207e44c09a02d557d516b10baa2b54f49a6decf76a010500a8819fb17ae0a02f069e51d11b33a5e5f51a965155f06df22dd4632a7aed36e43b529ec5e20b5300401f785b53df7e3d52792ff9e8cd9a8ae79501af52fbaa5c2b048e640ee5257ddb3436c03fab2f3c7b5982180050a1c5d40eea7cf5bc8adec469680fa853d4a6ca4b37d76d45e57f5f4dbe10f0cf769ef25efa008086f3a3825595d7823f57b5f99673cefea1fc81ff79b5b86a0a4f41f4b76bdf85887eee7ee45d08fdc1efc1a8008096f2862c4ba96dd489ca2bd5794bdce88d7f50f280c9cb94975ffeb45a48359defe27c5579e39ee877aaa33b9537b15a540100069037af19af36515e09ef57ea5e157d2834392fe273ab3a5deda73ea3fc0cbfcdf3d27dc1e6c5a18e51fe408e7eef9cfd5dfd40f9ae11bbf70140477970e1bbd47a6a7bf56d75a6ba413dac7c6b38fa10a9aa97d47dea1ae50f797fa3ffa25a5bf9367e17769c7bbbf2f4443fbe98a052eedef8efe963f891838fb9b00200a0100f985b42ada43ea6b654bbaabdd5c16f7484f24877e70fee8979b19d89ffb9bfe1fa9ff5ffce03187d9c4fa9d5951f59ccae309ca7e0f916bd6781eca4f651fe3bfaeff993379af8b7f57fe77fc6ffacff374cdf0300000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000090c35453fd1fe30bc4565d7de2950000000049454e44ae426082, 'Makati City', 'Single', 'Filipino', 'n/a', 'Iglesia ni Cristo', '', '', '', '', '', '', '', '', '', '', '', 'Active'),
(6, '000-000-006', 'Nogaliza', 'Jayson', 'Morales', 'nogalizajayson@gmail.com', 'Male', '5/16/2024', 'nogalizajayson@gmail.com', '', '', '', '', '', '', '', '', 0x89504e470d0a1a0a0000000d4948445200000200000002000806000000f478d4fa0000000467414d410000b18f0bfc6105000000097048597300000ebc00000ebc0195bc72490000001974455874536f667477617265007777772e696e6b73636170652e6f72679bee3c1a00002ed449444154785eeddd07942c659d86714432179028204124895e0541248b280b088a8a4a92b828b84b9445b280a2082a224124090a06b208224852941c452f822859969c332ceebeaf70f7deb9f39f99aafebeaaaeea7a7ee73ce7ec51ac9e19b6baababbe30150000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000020ddd46a6ef54eb58a5a5f6dad7653fba983dfe8fbea58759c3afd8d7efac67fe6beab26feb37ba81dd5a66a6db5ac5a48cdac0000400dc6a9f1eae36a27e50feab3d54dea11f5bf35f7a2ba47fd4e9da4f6575ba80f2a5f24bc5901008082a651ef569f5507a8f3d49dea9f2afa206e6aafa85b95ef30f8f7f085cb3b1400009de7dbf6feb0df56fd58dda2fcc1197da00e4a8fa94bd4d7d5ba6a760500c0409b49adaef651bf524fa8e843b24bf9cec604e531095baac5140000ade7dbdefe86ef5bf97e6e1e7d08d2d0ee521e90e8c720b32900001acf23e4fdbcfb68e50fb2e8038e8af7b2ba54edae3c08120080c69841f943ff64f5ac8a3ec8284fbea83a5cadaa0000a8ddf46ae287fe332afab0a26ae3620000509b95d489ea69157d28517ff20c0aaf8fc0ac020040361e88e6817c37abe8c3879ad34bca6b0faca9dea4000028cd53f67ea218bddfcefea6f6545e22190080514dab3cfdec5a157da850fbf25d018fd5f0a24b00000c318bda59ddaba20f116a7f5e74e862e5c19b3c1e00808e5b501daa18d4d7adbc89d226cacb3103003ac4cf85bde52dcff7bbdd5f94773164f74200187073297ff0bfa0a20f04ea66de8fc01702dc1100800133a7faa67a4e451f0044ce8f063c460000d0721ed5efc17d4faae80d9f28ea32b58c0200b4901783f1addde80d9e68ac5e539e3e38af0200b4c092ea7c15bda91395cd8f8d0e50def40900d0405eb2f748f5aa8adec88952ba53adad00000db29eba4f456fdc4439f35e03f32800401fbd55f9396df4464d54554f286f10c58a8200d0075eb3ff5115bd4113d5d1e5ca634e0000355840794df7e80d99a8ee9e575f54dc0d00800a6da01e53d11b31513fbb50cdaf00001979b7be6355f4c64bd4941e51eb2b004006cbab3b54f4864bd4c43c30759c0200f4c0cf54f756cceba73676ab7aa7020094e05bfe67a9e88d95a82d3dab3654008002fcadc9fbb4476fa8446dcce357bc31150060041b29b6eca541ecf76a3e050098cc34ea5015bd71120d4a0f280f6a050088474b9fa7a2374ca241eb45b5b102804ef32dd11b54f4464934a8fd53798b6100e8a4f1ea5e15bd411275a11314830301748af7557f5a456f8a445dea376a560500036f2bc5e23e4493f263b0b915000c2cef9af69a8ade0489badc6dca3b5d02c0c0d95d456f7c44f47af7a8c515000c8c3d54f4864744437b50bd570140ab79431f16f869665e71f109f5f264ff1935a3c7d4720a005aeb4815bdc15175f91be485ea18b597da44adacdea1e650236d533bbb9a572da9d6549f5707aa53d4d5ea7915bd1e55d3e38a3b01005ae91015bdb151be5e5297a96faa0dd482aa2a5eae7969e50b83e3d40415fd4c94af87d5520a005a633f15bda1517a8fa893d56755bfe78f2facb655a72b6f7d1bfdbc94d643ca3b640240e3fd978adec8a8f79e52bea5bfa2f2b88a269a49f971c3458aa99e79f38a996f5700d0589ee7ef75cea337312a97ff8ebf559b2b7fb8b689ef0c78ad7b4f6b8b7e372adf9d6a7e05008db3a9e29b5f7afe1b9ea6fcbcbdeddeacfcff177f56d1ef4ae5ba59cda200a03156531e9016bd6951b15e517eb63f8883befcd8e2e3ea1a15fdee543c0ffa9c4e0140df79da98a72c456f5654ac73d562aa0b3c5be12e15fd1da85827aaa68e0501d01173a93b54f4264563e7bfdd7aaa6bfc0d7667f58c8afe2e34761e6301007d31a3f20231d19b138dde0beacbaaeb7bc17bf39bb355f437a2d1f320d1ad1500d4cab71fcf50d11b138dde8dea5d0a936ca99e56d1df8b46cecb38afa200a036fbaae80d8946cea3fb0f570ce08a2da43cc02dfadbd1c879a120b61106508bb515d3fdcae5f5f93d5302a39b5af9e292ffff2a971fc54daf00a0325ee0e55115bd09519ce76efbef86e2d6555efd30fa7b52dc8f150054620675838ade7c28ee54d5b655fc9a620975bb8afeae14b79d0280ec7ea4a2371d8af38648ccd54e33a7ba52457f5f1a9e07052eaf00201b6f011bbde1d0f03c3deb4baadf1651eb28cfb73f5a5daa7c07c76bca7b9bd9e7947fde27d4fdcadfb6fddfff421dacfe5dadac6657fd344e3138b0787f532c170c208b45150bb614cb1ffe3baa7ef04631de22f858759f8a7ebe5ef345838febe3f7e382c003dc7ea9a29f8d86e725a501208917aab95e456f3234348f5cdf4ad5e96dcadff0ffa8a29fa98afc7b5eacb65033abba78fae4392afa9968789b2900e8d9412a7a73a1e1eda2eae01df6bcefbe6fe9f77bba9c17eff9a15a46d5c1170197a8e867a1a13dab3c9012004a5b55fd8f8ade5c6868df5655f3dd187feb6eeac878df15f09881aacdaa6e51d1cf4043f3dd3b169e0250ca6cea1e15bda9d0d03cd5cf0bd854c5c7fe82cafd5cbfaa3c606f3955253ffa68cbdfa3df79360a0014e6dbbad19b090ded0a55e50a6cbeb57e958a5ebbc9f9d18407a2791a5f559656cfabe8f569529e1a385e01c0983ea43c9a3d7a33a1493daeaa5ae1cfd3b88e546d7f04e375eaab1c8ce6ddf0a2d7a5a15dab3c76040046e455eb3c8f387a13a149f902697d5505ef14384145afdbd6ce547eac54859354f49a34b4dd14008cc883d9a2370f1a9a674754c183fc06f5b6f65f956fdbe736a3aa731a645b7b492da50060180fdc7a55456f1e34a96bd4342a278fd43e4545af3748bda03c8531b7772b7fc045af4993ba5cb13c358021fc81e65deba2370d9ad42bea3d2a272fa673818a5e6f10f3e3932fabdc0e50d1ebd1d0b65400f0ffbc9a5cf4664143fb86ca690ed5c651fe393a5ce5fc36eabb28b7aae8b568521e9859d5780c002de30fa1c754f4664193ba43f979732e73a9bfa8e8b5bad2512aa7d5143358c6ee5b0a00fef5261cbd49d0d0d654b978b64557bff94fd9fe2a27d6b0183baf0db0a402d0619e72c6c0bfb1fb8dcac54bfa76e9997f917652b978574416081abb8b14800ef39b40f4e64093f22de5e5552e5d18ed5f362f7894735d85efa8e875686855ad6501a0e17cf2476f0a34342f6293cb1755f41a34d5544faa45540e1e5fe19d0aa3d7a149796d86dc535a01349c9705edfa00b422f99ba91f93e4e0e9839e071fbd0ebdde752ad7ee751e5b10bd060dcdcb2903e890cd55f46640433b4be5304edda6a2d7a0a17d57e5f016f59c8a5e8326e55d3fabdcd00a4083f8db7f53f7946f5a6ba81cbcb14f747c1a9ec75c7843aa1c8e53d16bd0d07654003a601b15bd09d0d0bca84c8e856a96556ddfd5afeebc1992674ba4f212c1ac0b30768f28ef40096080f94df52e15bd09d0d03c602fd5d4ea6a151d9f466f579583d7bf8f8e4f43db4b011860ffa1a2939f86f68cf273fb545f50d1f169ecfcefc073fa536da8a2e3d3d01e5739fe7f1e40037974f5fd2a3af969683f55a97cb7c503aca2e353b10e53a9bc7cf3b32a3a3e0ded4b0ac000f22e60d1494fc3fb944af579151d9b8ae715fde651a94e55d1f16968fe82906b1a26800661bbdf62f943c76bf5a7f04c0b6f1e141d9fca75904af519151d9b86b785023040d652d1c94ec3f3b7c5549ba8e8d8543eafe8e739fd297c41c79a00c5f202611ebc0a60405ca8a2939d86b7b14a75a98a8e4dbdb5ad4ae5459da263d3f03ea6000c002f41cb5ce8e2cda752bc4d31ef3f6f7f50a9bce360746c1a9ea74e02180027a9e824a7e1f9b97daabd55746cea3d5fc02eaa522ca3a26353dc7805a0c5e6542faae804a7e11daf52b1c9523579739f147eaeed1d07a363d3f08e50005a6c17159ddc14973a02dadf52a3e3527ade2930d5af55746c1ade536a6605a0a5bca67a747253dc622a052bff5597c755a4ce06d85745c7a638af6501a0855656d1494d712f29cfdf4ff133151d9bf2b4be4af169151d97e272dc7501d0070cfe2bd79f54aa0754746cca53ead2c0de1d303a2e8ddc720a408bcca6bca25d744253dc192ac5222a3a2ee5eb5a95627af5aa8a8e4d71472b002de2ad6ca3939946eeeb2ac53a2a3a2ee5cb03d352b14473b91e55ded80a404bfc4e4527338ddc562ac5ce2a3a2ee56d5e95e202151d97466e5d05a005bc921d2bd1956f3d95c2b74aa3e352de5657294e51d17169e4fc3703d0027c13edad95548a4b54745cca9ba75aa6f89e8a8e4b23e78d9458130068812b557412d3e82da152b0dd723deda9527845c1e8b8347a3936c90250a105151bfff4d6dc2ac5df55745ccadb37548a1d54745c1abd73148006fb928a4e5e1a3b4f114bf1b08a8e4b794b5da3decb3d47c7a5d1f39e223329000dc5edffde9b41a560dd857af2025729b654d17169ec5257620450913914a3ff7b2ff5db0d7ffb7afab94af1ef2a3a2e8dddb10a40036da2a293968a354ea5785a45c7a5bca56ed9cc864dbde7a5aedfa40034cc8f5574d252b1bc7c728a7fa8e8b894b743558aed54745c2ad6fb148006f155f9432a3a61a9586f55296e57d171296f07a8143ba9e8b8542c6fa90ca041bc635774b252f1dea35278ebd4e8b894b7dd540aeff9101d978a75b502d020be2a8f4e562ade9a2a85e74947c7a5bca52e4873828a8e4bc5f26e8ab328000d71b98a4e562adee7548a4354745cca5bea33e8f354745c2a9e77be04d000d3a9175474a252f1bc88520aa697559f57b94c9dad71bd8a8e4dc54b5d8d1140262baae824a5721da652aca2a2e352beee57a9182c9bde150a4003785054749252b92e52296657afa9e8d894a70b558ab954745c2ad7cb8a65818106385b45272995ebbf55aa5b54746ccad35e2ac58754745c2adf1a0a409f714b335f73aa147e8c101d97f2b4824ab1bd8a8e4be5f3b6ca00fa6831159d9cd45babab14de2c253a2ea5f78c9a46a5f8818a8e4de5fb9502d0479ba9e8e4a4defab24ae1e5843d4f3a3a36a5e5e97ba96e54d1b1a97c5efa1a401f7d47452727f5d6af552a1f233a36a5e57dfc53bc45b16363de5297cf0690c023d7a313937aeb5935ad4ac1ae8cf97b4ea5ae3ef729151d9b7a6f2d05a04f1e54d18949bdb7924a31a37a4a45c7a6de3a45a53a4245c7a6dedb4301e883b9557452525afba8543f54d1b1a9b7727cd39ca0a26353effd5c01e8838fa8e8a4a4b46e50a996565eb6363a3e95eb2f6a6a956271151d9bd2f216d800fa6057159d94949e3f30529dafa26353b93cd325d55754746c4acb832abd1709809a9da8a29392d2cbf118803d1ad2bb53a5cefdb73fabe8f8945e8e8b650025b1057075f9032387cb54747c2ad61754aa77aae8d894a78f2a0035bb57452724e5c9cff1532da7987bde5b1eb4973a25d30e52d1f1294f3b2a0035f21b231f2cd57682cae1fb2a3a3e8d9c07507ae39e54d3ab8755f41a94a7ef2900355a54452723e5eb25358f4a35abf24e83d16b50dc8f550e9f57d1f1295fec0900d48c2980f594bafdec44ecd950bcc755ae2566d99eb9fa980a08d46c1b159d8c94b7fb558ee7d076b28a5e8326e55bff9f54397c5845af41797b5101a8d1812a3a19297fdba91c66565ed4267a0d7abd43552eccc0a82fef8209a026ac01505f7e7e3f93ca61bc7a4145afd3f5ae53b91695595745af41d5b484025093735474225235e5dcf4c4bb05bea6a2d7e96a7ed4b2a0cac1cb06dfaca2d7697adef5f02ee5e5a82f5517ab0bd4e96fe4dd3ffd9f5da5fccfdca6bc836574ac3a5b4d01a8c9152a3a11a99a9e50b3ab5cfe5345afd3c5bc73e27b552e1babe8759a90a7eedeaace54072b8fe5595d2dac52ee328d535ef0c85327b750df541e9d7fb78a7e8edc7d4601a889affca31391aa2bf77c67bf4947afd3a5fc386455958bc759f81b74f45afdc8db757bc73cefdbf141e50feaba791aeacaeacbca1705556c55bdbd025093475574225275f9dbdb07542e6f5247a9e8b5bad0f32af732b21e4418bd565df982e63cb5b3f2780fff3b6e9a37ab65952f4a3c5032c782625f53006ae0679cac02d89f3caf3cd7b4c0893cbe207aad41ce8f54727ef3b7f7ab7e9c17cfa853d56795ef40b4cddcca8f22bc7ba517bf8a7ec7b1fa8102508339547412523dedad72db5abdaaa2d71bb43cab22e7337ff38e81750efcf3204e0fc6f3788319d4a0f0743e8f4ff9a38a7eef913a4501a8c1622a3a09a99ebcf0c97b546eebab2755f49a83d2352ad768ffc9eda7a2d7cb9d9fe9fb76f7226ad0793b6b4f37f6a39ae86f317967290035f0b3c5e824a4faf2f2a7b3a8dc16529ee215bd669bf30a7f87ab5cf3fc27e715ffaabef5ef7fdfde9ad89b0b758d67bfecafbc4473f4b7719eaa08a0061ec0139d84546f1edd5d057f481ea6fca119bd6edbf20787ef6e54615ee56fe5d1ebe6e826e5a5893deea6eb7cc1bbbb7a484df977ba5c01a8816fcd4d7902527ffa0f55152faef22715bd6e5bf2e235b936f6999247b357b5dcafbff16fa89a388abfdfbc5ec16e6af2e984d72b0035f07ce2c9dfaca87f79d47495aba079709ba7943dada2d76f6afe005d5355e9bb2a7aed94fcedd65b08fbef8ed179f6c031ca8f5fbcb811801afc9b8adebca83f794adbbb5595e6575e88a8c880ac7e76a7f2b3f2dc5325a7e439ecd1ebf79a3fc48e506c6a53ded2ca830501d4603d15bd8951ffba4f2da0aa36973a40356db6c0dfd5b6aa8e6fceb9f752f0a0cb65140034de062a7a23a3fef667f5165507bf8e3f70bd2744bf060b7ae31acfff5e4bd53548ce23fe5f56d1cf53364fe7f4f2b80cf003d01a5c0034b71b95bfa5d76951e5695ade4eb7eae9705ef5ce4bdd7ac399bad7b5f746377efde8e72a9b0757f2ad1f40ebf008a0d9f94e80a7a7f583ef0c78ca9dc70b5cab52377ef1a38d0bd55ecab34ffa3538ee63cadfd8a39fb14cbe5b7288aa7a8c020054824180cdcf3bd2bd4335812f46fcedd983f3bcef8077213c529da43c4def58e54d74beaa7c4bdccbdb7aad897eec5c17f174bc5754f4772e93ef1e347ddb5a3f8ef0e87a6fedbb8af24c8a7595f71970febffd9ff9bff33fe37f9647184087300db01dddab72af79df35de6636c7630d6fe2e425b49bc2776a56573ba8e3d4a5ca33287ab9d0f1ffc683302f513e968fe9f70866340003682515bd1150f3f26debcd15caf192bbc7abe86f5a363fc2a862d9e632e6519b29df6d99a072ce621829bf861f47f9353fa77cb70040cbb11470bbf273e7839557aec3d8fc41f53b15fd2dcbe66fc4fd1ab7b094f226455e25af8e0ffcb1f2cfe081a25f517e7c00a085bce84c748253b3f37eebfe268891f9d6f5032afafb95c91f765f5275f3bf5faf97df86259cfd58c44bfa7267006811b6036e6f0f2b4fe3c450de53df0311737c53f69881ad549d56501e50996b8d823af3cf7c9afa8002d07073a8e844a6f6e40f8b39155e5f4ad6df46a3bf53d9fce1ef350aeab2aaf2ba08d1cfd2c6bcb0d4c7158086f2b49faa177ca16af3b7aeafabaef35ef3de4a36fa1b95cda3e1eb9ae6e7a9785e4238fa3906a12b952f04d80d1168a0475574e252b37b561dae165498643975b2eaf5c2d6032debb8edefd503fde118fd0c83d81f1453598186b94d45272c35335fb07935bdbaf60a682b0f703d4395dddfc05b26576966e5991cafaae8f50739ffcebe689d550168003fab8b4e566a562f28afbcc7a22ce5785cc0b92afa9b4ed93754957c2bdc4b2247afdda5fe5bd539be02c008ce51d1494acdc8a3d9bdd42eb7fad378f7bfd106087a37c2aa9e537b53a7411ae0972b5f98318015e8a31faae8e4a4fe77b562a7b97cbc8092b73e7e4c4dfe77f6023b33aa2a784a9cf77398fcf56852be23e2fd0800f4c1812a3a31a97f3dafbcd90e2bfe55c33306bcacadc7073ca41650b9f96e82c713e4d87c68d0f3d88003149b110135db46452725f5a75faa2a3e90309ca7e0793f8cdc7c8171818afefdd2c879854b06b70235f2b3d1e864a47a7b5a6daad06ef3a93faae8df318dddad8a0b60a026de6b3e3a11a9bebcb1caa2aa0dfcedd6031297549e73bf869ab8cffc3acadfaafddcdbffdd22cabbf1758537c6b94745ff8efb910790deaf3cd5f70675b1f2a05fe7ffdbff99ff3bff3365a74b56d9ddcaffff05a062d32a5603ec4f7ed3fd8e9a4e35894766fbce9007cc7d4b9da53c82de6313a2df63acfc9cdd1f367ebce1dfd7db1abf47f56b77bd2af8a2a79f8b6a7960e3d96a5fe58b314f7f2c33b0d1ffacff371b2a1fc3c77a5c45af55478fa8e515808a35e95b4b577a4aada79ac077813c2fdb03e37c0bb6ae6f832f295f181ca1d657fdde6bbf571f515e9931fa1dabca03e73ccec03b157aa6481503e87cccf7a95dd585aaeec58bfc37f58528800ae5da339d8af577e53ddefbc5b30b7cabde1ff839b6cccd953f60bc64acf7be1fafdac0df52ebfcf0f7059a6788ccabeae6cdc37c57a8cec5c37cd7a98a819a00dec05a00f5f55bd58fc54ffca1ef6faac728df5e8d7eb6a6e5bdf0f7540bab26f233ff3a6efbfbc2c8fb1b34692d7dff2c5e3ca98ebb02feffd72514800aeca2a2138ff276bcf2988b3abd55edaf1e54d1cfd486fc48c23bfd7987bea6ac8d30bff260b5e8e7cd95777af4077f933ffcdeaebcbeff8b2afa1d72e50583580d13a8005301abcf1bc0d4c9cf857d8bdf7b08443f4f5bf3230b2f1ae3dbd1fde2d79ea0a29f2f5767aa367de0f92e8d070e46bf4bae3c1095750280cce656d1094779da47d5c5cff673ed8bdfe49e5107a9ba3f10bcc25fd1cd857ac9e3433cadb2adfc65a2ca1d46bd5850557b36009de51dbaa2138e7acfb7ae3d4abb0eef52a7abe8e718e43c55cd83e2aa5acb7f4a1e9310fd1ca979cebe777b9c41b59dff5d1ca2aa9a4db29b029091a7f944271bf596dffcb65355f3aa69de2dd01f20d1cfd195fea1b652557e3b5c5155b1b6bf07b97d540d1a0f3aad62ec89071eb2811090d1b75574b2516ff95b69953c98f02b6ad09ef1a776995a5ce5e61510ab582fc353703da070507910aa571d8c7ef7943c2890ad84814c3653d18946e5f3ea7955f22a7a37aae8b5e9f511e91e779173c64515fbf97bfaed20ad863812ff8ebe4b15fd0d52f2ca920032f05af4d14946e53a4155751bda6fa4fe60f3f4b0e8b569687e2490c3262a3a7e4a9e3ad7a5c16cfe5d3d7b23fa5ba4e4658f0164d0e6b9e24dc8df12ab9aa7eeb9e05e36377a5d1adec32ac7c0c05955ce01b21e1be27537baca8362730e0ef4d88fb62e210d348a377d894e321abbdbd56caa0a1e20f6a48a5e97e2f656397c4f45c7ef35afabdf75dbabe86fd36bde600a4022bf394527188d9ef7f2f734bcdc7cdbd48309bb3ec2bf6ccfa91c03c43cd622e752b75eb700aff334c1e86fd44bfe77e49d0c012458414527188d9cb752ae620ad73875868a5e9346ef4895ca175f57aae8f8bde4017ffd7ae6efa9a2cb292f12f58937f2ffedffec6daa1ffcb7c83930f0f70a4002ef4bcfb4b272e5bad53c394f9dfaa38a5e8fc62ec7a639deaa393a762f7903a8ba46fb7b554f0f8c3b5a79cc48919d0abdaaa2ffd9ef2befb73097aa83ff263957ad5c5b0148c0d6c0c5f39cf3dcfbb0fbdb9ac71344af4763778dca21d796b75ee4a7ea79fe1eece80f7d0f42cdf1c8c28f9cfcfb7bebdfaa07d8795be35c838faf520012789a597472d1d03c286f2195937756f35af0d1eb51b1b651a9d650d1b1cbe60fd22abf95fa9bbaa7d63da1a2d7cf91ef0e78caa23fa8abb28eca35cee5830a408f9655d1894543f3add29cbcb7bca73445af45c5f2e3ab1cdf582f51d1f1cbe6b5fdab30bdf22a901eec18bd6e15f951821f77f9316115720d0afc8d02d0230fce613d80d13b51e5e4a56b1f52d16b51f17ea15279bdffe8d865fb9baa62631fdf9db84345af59477e3cb5bacacd8f31ee54d16b966d7905a0473f52d18945af7f507b5df85c3c5dedaf2a7a2d2ad7a62a55ae1d157d5b3b272f30f535d58429a19ef9b2bfcabde855ae81973f55007ab4b18a4e2ccaf3213391bff5e49c6ad6e55e525eb52fc53c2ac732cb67aa9cbcc0d4a52a7aad7e76914afd9b4fc97771a2d72ad3f38ad501811ecda17c951f9d5c5dcebb9ae5e2d903acbc98af5fab54bbabe8d865f285c8822a97f95493a784deac720e10f440d81c1761b9f681003a29d734a841c93bcce5dc66f65015bd0ef596d7984ff527151dbb4cc7a85cfce1efb104d1eb3429ff8c392f02bca156f43a65f21d13003df28625d189d5d5beae72f10c82e835a8f75217fff12c8ce8b865f25db3c5540ebeb57e938a5ea789f9e2e92d2a07ef4c9aba9e81c74ae4bc1303748a172f69c280a326f4a8caf5acd36f4a8fabe875a8b7bcf35fea32bbfba9e8d8653a59e5e0c1754d7ce63f561e13906b612c0fe48b5ea34c7b2a003dfa838a4eacae956b07372f7dcaa395fc9daa525da7a2639729c712c4e6d1fed1f1db906707e4b08c8a8e5f2656060412eca8a213ab4bdda3bcf04a0e07aae83528add4fdf53dfa3ff56ed78d2a07cff36ff39d373f06c9b54e40eae047ff2c1ed00ca0071e84d4f5c7005bab1c5653ccaca8a655558acd5474dc32a55e84982f3407614d88db548e1503736c4fbe9102d023ef64169d585de82e9563b19369d50415bd06a5e58baa99558ae35474eca279c05a8e51f039c62134a5bd542aef8a993a18d03b1c02e89177048b4eac2eb493cae1bf54747c4acf1756a9522fce2e50a9bcb14f956bfb7b20eb2dea7ce535133c6aff3115fdb339f22642396ebf7b606174fca2f97706d023afa855644ff141cb3bac8d53a9fccdf02915bd06a5973a00d053d7521f73e518245ac5f810cfcff75d85a5d5483cd8ce03f7aad885d23b15a6da4d45c72e9affdde65ead10e8146f7e139d5c83dc412a871cd39968e452d767f080b5e8b865f287680a5f687a7be9e8d8bde4fd2afe53f9d15351fe6777509e52191db3977c119dfa786639151dbb4ca96344804e5b494527d6a0e6a548bd0e422a0ffcfba78a5e83f2943a48d38f79a2e316cd6b3aa4ce7ddf5245c7ee254f7d4b198fe0471197abe8d8bde40196293c06c71712d1b18be68b2100099abc1e79eebc235c0e5d1e405957a9dfee5207007a3f875497a8e8d8653b4de598b2ea63e4da1531c7fefca91b04fd400148b0b38a4eae41ccdb92a6f20753746cca5bea72afa92beeedab52ccad724cb5f5ae92b9d6ab301fcb7713a2d72a93676978cbeb14a9b3233c901040028fe8f58638d10936483da8bc625f2a8f0c8f8e4f794bfdd0bb5345c72dda67550affefa3e396c9cffc3d652e373f4a784445af59a60d540acfe58f8e5bb43b1480445d180c78884af57e151d9bf2f6b44ae167f7afa8e8d8454b5dfef768151db74c553ee3ceb11ae8512a45eab2c01ed3936b8f02a0b3c6ab411fd4b6944a95facc928ae5a96b293ce02d3a6ed17ceb7e4695e206151dbb689eea5766b47f595ed12ff52e89f75948e19904a9ef3b2c090c6430c8b7b6bd384aaa451423ffebe91a9522750be007540aef6098bac6869f8f57cdf3f9a3d72e5aea9d1af3a3b9e8d8455b420148b4a68a4eb04128c79effa96f9654bccb548a555474dca279cdfb146f53d171cb34da223fb9bc4f45af5da6d469b5b7abe8b845f354660019dca4a293acedada052f81b5deaed522ade852ac55a2a3a6ed1526f6d2fafa2e316cdcbfbd6c1ff7fedf50ea29fa1685ed027c5f52a3a6ed1fcc50540069babe8246b731e499d3a50c85bb946c7a66a3a57a5f8988a8e5b344f214cf161151db76837abba784dfde86728da87548ad43535d6550032f0a0a3fb5474a2b5b51354aa1fa9e8d8544da98bf0787a5a74dca29da3527c4245c72d9a37f5a94bead89ff5558a5faae8b845fb940290c9762a3ad1dada862a85472a7771d3a47e96ba6263ea1cfcb6bf7e19a92b03a6ae97d0efd7073019df0518a4e7dda98394fc0d273a2e55171700f5e10200c010de88253ad9da56ea7c723b5245c7a6eae202a03e5c000018c23b75a54ecf69427e769feaaf2a3a3655171700f5e10200c0309f53d109d7a6bea0522caca2e352b57101501f2e00000ce3a973135474d2b5a5d4f5dcb755d171a9dab800a80f17000042de3e373ae9dad0ab2a7547b9d43727eaadb67f007301505cbf5f1fc028daba4740ea72ae76978a8e4dd5c605407db800003022efa297bab56a3f4a5d4c669c62f39ffec405407db8000030aac35574f235b903558a1555745caa3e2e00eac30500805179cfedc75474023635cf6248e11904d171a9fab800a80f170000c6b4bd8a4ec0a6f65195820580fa171700f5e10200c098a65137a8e8246c62de112e45ea2e65d47b5c00d4870b000085bc4f797a5d742236addd54af7cb1f38c8a8e4bd5c705407db8000050d8c12a3a119b96bfc1f7ea832a3a26d5131700f5e10200406133aa3b54743236294fe15b5af5c2fbc147c7a47ae202a03e5c000028650dd58639f2d7aae954199f56d1b1a8beb800a80f1700004a3b5e452764d3f21bccb4aa88d515cffefb5fdb3f80b90028aedfaf0fa007b3a9bb557452362ddf0918af46e2bb047ba91755f4bfa77ae302a03e5c0000e8c92aea7f547462362d3fb2f0be06bba8b5941f636ca4beab1e52d1ff86fa131700f5e1020040cfbea6a21393a8d7b800a80f1700007ae639f357a9e8e424ea252e00eac305008024ef504fabe804252a1b1700f5e1020040b2ad54748212958d0b80fa700100208bb64c0da466c705407db8000090c5f4ea3a159da84445e302a03e5c0000c86641f5888a4e56a2227101501f2e000064f561d596f501a8797101501f2e000064b7a78a4e58a2b1e202a03e5c0000c8ee4dea54159db444a3c505407db80000508919d4152a3a7189468a0b80fa700100a03273aabfaae8e4258ae202a03e5c0000a894570a7c58452730d194b5fd03781d151db76827a8ba700100a072ab2ab6dba522b5fd02e05d2a3a6ed1f65375e10200402d3654afa9e844269a58db2f003c00f67e151dbb482baaba700100a036de33c07bf3472733916bfb0580f95b7c74ecb1ba594dadeac20500805aeda8a29399c80dc205c03875a78a8e3f525e3c6b7555272e0000d46e5f159dd04467a9141ba8e8b8454b7dfd89dead1e53d16b4c99ef8aeda0eae6df35fa798ae6bf750a2e00808e3a4845273575bb5fab146ba9e8b845fb8dcac53360ae51d1eb4ccc7b677c5af5c3452afa998af66f2a05170040871da1a2139bbadbe52ac54a2a3a6ed1ae52397950e027d569ea5ef5b27a48f9f7dc55cdaafae56a15fd0d8a963a60910b00a0e3bea6a2939bbad9f52ac578151db768b7abaeb843457f83a279ca630a2e00004cb5878a4e70ea5e7f5129deaea2e316ed15358d1a74d3aa5755f437289ab7ff4ec10500807fd94d314590ee5329a657a9db512fae06dd3b55f4bb17cd170fd3a9145c0000f87fdb2a160bea76fe6049fd065e760ade9475e183652315fdee45f3e383545c0000186273e5dbb0d1094fdd681195c23309a2e316ed2835e88e51d1ef5eb473552a2e00000cf361f5a48a4e7a1afcfcef3fc5612a3a6ed152c721b441ea00c06fab545c0000087921957b5474e2d360b78d4ae1ff7d74dc322da10655eaf37fe765bd537101006044f3a91b5474f2d3e0f60d95c283f8a2e396c9d3530795ffbed1ef5ca6d4c734c605008051cdac7ea9a237001accce57a93c9b203a76d1ee56756ecc5397372b2f4814fdce45bb4be5c005008031f94deb10c534c16ee435f4bd825e8a1fa9e8d865fa8c1a341babe8772dd3092a072e000014f671f5948ade0c68b04a7d06bf858a8e5ba69b54ea854893f877b94545bf6b99365539700100a0147f304c50d11b020d4efe004f31bb7a4945c72e53bf36eaa9c2862afa1dcbf4829a4de5c0050080d2665167aae84d8106a3efab5467a8e8d865f25802efefdf763e67ee57d1ef58a69fab5cb80000d013dfcef4f2c12c1a3498e55869ce8f8ca26397cde34fdaee5015fd6e655b57e5f25315bd46d13ea10074d872ca3bb8456f10d4eebc16440a6f78e3fdf6a36397c97b0ba42e4ed44fdeb73fc712dbdec638e74649a91725cb2b001d37a33a5c456f12d4def651a9726d37ed0f3faf4bd1366f550faae8772adb7e2a27dfc28f5ea748cfa9191400fccb27d5a32a7ac3a0f6759d4a35a77a5645c72fdb35caeb52b485c72ef86f18fd2e657b5a7960654e1e97d0eb92df3f510030c4fcea0215bd6950bbf2ba0f0ba8545eb73e3a7e2f79a3213f5a683aff8c17aae877e8a56faa2aecaea2d71b2dcfee584c0140c8b717733cffa5feb6874a35af7a5145c7ef25af4ce9c74e4de59fcdbbf5453f7b2f3dafe65155f0225f652fd8b75600302adfb23c56456f22d48ebcb77f8e2579bfaaa2e3f7da1f54ee5be239cca1ae54d1cfdc6b5f51559a499da6a2d79e3c7ff3e7c31f40291f55ec2cd8ded651a9fcadd81713d1f17bed6f6a59d514ef57b97f474fc79c5ed5c1d3fa3cce62ca25bf3de0cfcffcb9ed0fa0271e10e56947ac1bd0be7ccb3d07cf618f8e9f92bf95eea4fab971906fa3efa272ac7c38656babba79b6c56a6a7df501d5e4c72d005a6449f52b15bdd95133f33cfc85540e55ad2079adf29a1475f35cf8aab6ccf66d790018381f517f52d11b1f35afa3540e9e1698ba55f048f942c5b7aa5317302a62bcfa99cab1c04f941f9935718c030064e1a952be7dfbb88ade04a939bdac165639acacaa7c14e40fe573949722ce3965703ae5e7e47e2452e5d6d8fedbaca00060e07941124f3763abe16677bccaa597f9e7bde4a9a89e89b291ea652a9d57f2f31efec7a9ba16b9da550140a7f8f6f001caab9e456f8cd4df7c8bdd633872f06652be851ebd4e55f95bfb5dcaf3debfa77cd1b99dfadc1bf9fff67fe6ffce8bf8f89fadf29b7ed4c9ca7f1b00e8247feb3a4ce55c3c86f294733b5adf9e67d5c8495dacfc9801003acfcb0a7b2b581e0d34abb5542e5e8426f7c2396dcc7b0678aa2c0060327e63dc59b1985033ba5be5dc94676e75b38a5eab0bdda8e65200801178b1178fecbe5a456fa4545fb937a7f145de452a7aad41eeb76a36050028c82b9579c0d40b2a7a63a56af354b5f7aa9cbce46d9135e907a5b3157bea03408ffced695b75938ade64a9bafeacfc0c3f272fabfb2d55f7e8fb3af3ef7690eae7f2c5003050bc2cabe76b3fa9a2375ecadf49aa0a6baa8754f49a6dce6b09784f04004005fc2dd21f207e44c09a02d557d516b10baa2b54f49a6decf76a010500a8819fb17ae0a02f069e51d11b33a5e5f51a965155f06df22dd4632a7aed36e43b529ec5e20b5300401f785b53df7e3d52792ff9e8cd9a8ae79501af52fbaa5c2b048e640ee5257ddb3436c03fab2f3c7b5982180050a1c5d40eea7cf5bc8adec469680fa853d4a6ca4b37d76d45e57f5f4dbe10f0cf769ef25efa008086f3a3825595d7823f57b5f99673cefea1fc81ff79b5b86a0a4f41f4b76bdf85887eee7ee45d08fdc1efc1a8008096f2862c4ba96dd489ca2bd5794bdce88d7f50f280c9cb94975ffeb45a48359defe27c5579e39ee877aaa33b9537b15a540100069037af19af36515e09ef57ea5e157d2834392fe273ab3a5deda73ea3fc0cbfcdf3d27dc1e6c5a18e51fe408e7eef9cfd5dfd40f9ae11bbf70140477970e1bbd47a6a7bf56d75a6ba413dac7c6b38fa10a9aa97d47dea1ae50f797fa3ffa25a5bf9367e17769c7bbbf2f4443fbe98a052eedef8efe963f891838fb9b00200a0100f985b42ada43ea6b654bbaabdd5c16f7484f24877e70fee8979b19d89ffb9bfe1fa9ff5ffce03187d9c4fa9d5951f59ccae309ca7e0f916bd6781eca4f651fe3bfaeff993379af8b7f57fe77fc6ffacff374cdf0300000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000090c35453fd1fe30bc4565d7de2950000000049454e44ae426082, 'Makati City', 'Single', 'Filipino', '', 'Aethiest', '', '', '', '', '', '', '', '', '', '', '', 'Active');
INSERT INTO `employee` (`emp_no`, `emp_id`, `lname`, `fname`, `mname`, `email`, `gender`, `birthdate`, `cp_email`, `tel_no`, `mothers_name`, `mothers_occ`, `fathers_name`, `fathers_occ`, `address`, `home_address`, `contact_no`, `image`, `pob`, `status_type`, `nationality`, `spouse_name`, `religion`, `elementary`, `high_school`, `college`, `course`, `work_experience`, `primary_con`, `contact_num`, `relationship`, `secondary_con`, `contact_num2`, `relationship2`, `status`) VALUES
(7, '000-000-007', 'Fernando', 'Fernandez', 'Dionisio', 'fernandofernandez@gmail.com', 'Male', '5/16/2024', 'fernandofernandez@gmail.com', '78233212', '', '', '', '', '', '', '09283827382', 0x89504e470d0a1a0a0000000d4948445200000200000002000806000000f478d4fa0000000467414d410000b18f0bfc6105000000097048597300000ec200000ec20115284a800000001974455874536f667477617265007777772e696e6b73636170652e6f72679bee3c1a00002ed449444154785eeddd07942c659d86714432179028204124895e0541248b280b088a8a4a92b828b84b9445b280a2082a224124090a06b208224852941c452f822859969c332ceebeaf70f7deb9f39f99aafebeaaaeea7a7ee73ce7ec51ac9e19b6baababbe30150000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000020ddd46a6ef54eb58a5a5f6dad7653fba983dfe8fbea58759c3afd8d7efac67fe6beab26feb37ba81dd5a66a6db5ac5a48cdac0000400dc6a9f1eae36a27e50feab3d54dea11f5bf35f7a2ba47fd4e9da4f6575ba80f2a5f24bc5901008082a651ef569f5507a8f3d49dea9f2afa206e6aafa85b95ef30f8f7f085cb3b1400009de7dbf6feb0df56fd58dda2fcc1197da00e4a8fa94bd4d7d5ba6a760500c0409b49adaef651bf524fa8e843b24bf9cec604e531095baac5140000ade7dbdefe86ef5bf97e6e1e7d08d2d0ee521e90e8c720b32900001acf23e4fdbcfb68e50fb2e8038e8af7b2ba54edae3c08120080c69841f943ff64f5ac8a3ec8284fbea83a5cadaa0000a8ddf46ae287fe332afab0a26ae3620000509b95d489ea69157d28517ff20c0aaf8fc0ac020040361e88e6817c37abe8c3879ad34bca6b0faca9dea4000028cd53f67ea218bddfcefea6f6545e22190080514dab3cfdec5a157da850fbf25d018fd5f0a24b00000c318bda59ddaba20f116a7f5e74e862e5c19b3c1e00808e5b501daa18d4d7adbc89d226cacb3103003ac4cf85bde52dcff7bbdd5f94773164f74200187073297ff0bfa0a20f04ea66de8fc01702dc1100800133a7faa67a4e451f0044ce8f063c460000d0721ed5efc17d4faae80d9f28ea32b58c0200b4901783f1addde80d9e68ac5e539e3e38af0200b4c092ea7c15bda91395cd8f8d0e50def40900d0405eb2f748f5aa8adec88952ba53adad00000db29eba4f456fdc4439f35e03f32800401fbd55f9396df4464d54554f286f10c58a8200d0075eb3ff5115bd4113d5d1e5ca634e0000355840794df7e80d99a8ee9e575f54dc0d00800a6da01e53d11b31513fbb50cdaf00001979b7be6355f4c64bd4941e51eb2b004006cbab3b54f4864bd4c43c30759c0200f4c0cf54f756cceba73676ab7aa7020094e05bfe67a9e88d95a82d3dab3654008002fcadc9fbb4476fa8446dcce357bc31150060041b29b6eca541ecf76a3e050098cc34ea5015bd71120d4a0f280f6a050088474b9fa7a2374ca241eb45b5b102804ef32dd11b54f4464934a8fd53798b6100e8a4f1ea5e15bd411275a11314830301748af7557f5a456f8a445dea376a560500036f2bc5e23e4493f263b0b915000c2cef9af69a8ade0489badc6dca3b5d02c0c0d95d456f7c44f47af7a8c515000c8c3d54f4864744437b50bd570140ab79431f16f869665e71f109f5f264ff1935a3c7d4720a005aeb4815bdc15175f91be485ea18b597da44adacdea1e650236d533bbb9a572da9d6549f5707aa53d4d5ea7915bd1e55d3e38a3b01005ae91015bdb151be5e5297a96faa0dd482aa2a5eae7969e50b83e3d40415fd4c94af87d5520a005a633f15bda1517a8fa893d56755bfe78f2facb655a72b6f7d1bfdbc94d643ca3b640240e3fd978adec8a8f79e52bea5bfa2f2b88a269a49f971c3458aa99e79f38a996f5700d0589ee7ef75cea337312a97ff8ebf559b2b7fb8b689ef0c78ad7b4f6b8b7e372adf9d6a7e05008db3a9e29b5f7afe1b9ea6fcbcbdeddeacfcff177f56d1ef4ae5ba59cda200a03156531e9016bd6951b15e517eb63f8883befcd8e2e3ea1a15fdee543c0ffa9c4e0140df79da98a72c456f5654ac73d562aa0b3c5be12e15fd1da85827aaa68e0501d01173a93b54f4264563e7bfdd7aaa6bfc0d7667f58c8afe2e34761e6301007d31a3f20231d19b138dde0beacbaaeb7bc17bf39bb355f437a2d1f320d1ad1500d4cab71fcf50d11b138dde8dea5d0a936ca99e56d1df8b46cecb38afa200a036fbaae80d8946cea3fb0f570ce08a2da43cc02dfadbd1c879a120b61106508bb515d3fdcae5f5f93d5302a39b5af9e292ffff2a971fc54daf00a0325ee0e55115bd09519ce76efbef86e2d6555efd30fa7b52dc8f150054620675838ade7c28ee54d5b655fc9a620975bb8afeae14b79d0280ec7ea4a2371d8af38648ccd54e33a7ba52457f5f1a9e07052eaf00201b6f011bbde1d0f03c3deb4baadf1651eb28cfb73f5a5daa7c07c76bca7b9bd9e7947fde27d4fdcadfb6fddfff421dacfe5dadac6657fd344e3138b0787f532c170c208b45150bb614cb1ffe3baa7ef04631de22f858759f8a7ebe5ef345838febe3f7e382c003dc7ea9a29f8d86e725a501208917aab95e456f3234348f5cdf4ad5e96dcadff0ffa8a29fa98afc7b5eacb65033abba78fae4392afa9968789b2900e8d9412a7a73a1e1eda2eae01df6bcefbe6fe9f77bba9c17eff9a15a46d5c1170197a8e867a1a13dab3c9012004a5b55fd8f8ade5c6868df5655f3dd187feb6eeac878df15f09881aacdaa6e51d1cf4043f3dd3b169e0250ca6cea1e15bda9d0d03cd5cf0bd854c5c7fe82cafd5cbfaa3c606f3955253ffa68cbdfa3df79360a0014e6dbbad19b090ded0a55e50a6cbeb57e958a5ebbc9f9d18407a2791a5f559656cfabe8f569529e1a385e01c0983ea43c9a3d7a33a1493daeaa5ae1cfd3b88e546d7f04e375eaab1c8ce6ddf0a2d7a5a15dab3c76040046e455eb3c8f387a13a149f902697d5505ef14384145afdbd6ce547eac54859354f49a34b4dd14008cc883d9a2370f1a9a674754c183fc06f5b6f65f956fdbe736a3aa731a645b7b492da50060180fdc7a55456f1e34a96bd4342a278fd43e4545af3748bda03c8531b7772b7fc045af4993ba5cb13c358021fc81e65deba2370d9ad42bea3d2a272fa673818a5e6f10f3e3932fabdc0e50d1ebd1d0b65400f0ffbc9a5cf4664143fb86ca690ed5c651fe393a5ce5fc36eabb28b7aae8b568521e9859d5780c002de30fa1c754f4664193ba43f979732e73a9bfa8e8b5bad2512aa7d5143358c6ee5b0a00fef5261cbd49d0d0d654b978b64557bff94fd9fe2a27d6b0183baf0db0a402d0619e72c6c0bfb1fb8dcac54bfa76e9997f917652b978574416081abb8b14800ef39b40f4e64093f22de5e5552e5d18ed5f362f7894735d85efa8e875686855ad6501a0e17cf2476f0a34342f6293cb1755f41a34d5544faa45540e1e5fe19d0aa3d7a149796d86dc535a01349c9705edfa00b422f99ba91f93e4e0e9839e071fbd0ebdde752ad7ee751e5b10bd060dcdcb2903e890cd55f46640433b4be5304edda6a2d7a0a17d57e5f016f59c8a5e8326e55d3fabdcd00a4083f8db7f53f7946f5a6ba81cbcb14f747c1a9ec75c7843aa1c8e53d16bd0d07654003a601b15bd09d0d0bca84c8e856a96556ddfd5afeebc1992674ba4f212c1ac0b30768f28ef40096080f94df52e15bd09d0d03c602fd5d4ea6a151d9f466f579583d7bf8f8e4f43db4b011860ffa1a2939f86f68cf273fb545f50d1f169ecfcefc073fa536da8a2e3d3d01e5739fe7f1e40037974f5fd2a3af969683f55a97cb7c503aca2e353b10e53a9bc7cf3b32a3a3e0ded4b0ac000f22e60d1494fc3fb944af579151d9b8ae715fde651a94e55d1f16968fe82906b1a26800661bbdf62f943c76bf5a7f04c0b6f1e141d9fca75904af519151d9b86b785023040d652d1c94ec3f3b7c5549ba8e8d8543eafe8e739fd297c41c79a00c5f202611ebc0a60405ca8a2939d86b7b14a75a98a8e4dbdb5ad4ae5459da263d3f03ea6000c002f41cb5ce8e2cda752bc4d31ef3f6f7f50a9bce360746c1a9ea74e02180027a9e824a7e1f9b97daabd55746cea3d5fc02eaa522ca3a26353dc7805a0c5e6542faae804a7e11daf52b1c9523579739f147eaeed1d07a363d3f08e50005a6c17159ddc14973a02dadf52a3e3527ade2930d5af55746c1ade536a6605a0a5bca67a747253dc622a052bff5597c755a4ce06d85745c7a638af6501a0855656d1494d712f29cfdf4ff133151d9bf2b4be4af169151d97e272dc7501d0070cfe2bd79f54aa0754746cca53ead2c0de1d303a2e8ddc720a408bcca6bca25d744253dc192ac5222a3a2ee5eb5a95627af5aa8a8e4d71472b002de2ad6ca3939946eeeb2ac53a2a3a2ee5cb03d352b14473b91e55ded80a404bfc4e4527338ddc562ac5ce2a3a2ee56d5e95e202151d97466e5d05a005bc921d2bd1956f3d95c2b74aa3e352de5657294e51d17169e4fc3703d0027c13edad95548a4b54745cca9ba75aa6f89e8a8e4b23e78d9458130068812b557412d3e82da152b0dd723deda9527845c1e8b8347a3936c90250a105151bfff4d6dc2ac5df55745ccadb37548a1d54745c1abd73148006fb928a4e5e1a3b4f114bf1b08a8e4b794b5da3decb3d47c7a5d1f39e223329000dc5edffde9b41a560dd857af2025729b654d17169ec5257620450913914a3ff7b2ff5db0d7ffb7afab94af1ef2a3a2e8dddb10a40036da2a293968a354ea5785a45c7a5bca56ed9cc864dbde7a5aedfa40034cc8f5574d252b1bc7c728a7fa8e8b894b743558aed54745c2ad6fb148006f155f9432a3a61a9586f55296e57d171296f07a8143ba9e8b8542c6fa90ca041bc635774b252f1dea35278ebd4e8b894b7dd540aeff9101d978a75b502d020be2a8f4e562ade9a2a85e74947c7a5bca52e4873828a8e4bc5f26e8ab328000d71b98a4e562adee7548a4354745cca5bea33e8f354745c2a9e77be04d000d3a9175474a252f1bc88520aa697559f57b94c9dad71bd8a8e4dc54b5d8d1140262baae824a5721da652aca2a2e352beee57a9182c9bde150a4003785054749252b92e52296657afa9e8d894a70b558ab954745c2ad7cb8a65818106385b45272995ebbf55aa5b54746ccad35e2ac58754745c2adf1a0a409f714b335f73aa147e8c101d97f2b4824ab1bd8a8e4be5f3b6ca00fa6831159d9cd45babab14de2c253a2ea5f78c9a46a5f8818a8e4de5fb9502d0479ba9e8e4a4defab24ae1e5843d4f3a3a36a5e5e97ba96e54d1b1a97c5efa1a401f7d47452727f5d6af552a1f233a36a5e57dfc53bc45b16363de5297cf0690c023d7a313937aeb5935ad4ac1ae8cf97b4ea5ae3ef729151d9b7a6f2d05a04f1e54d18949bdb7924a31a37a4a45c7a6de3a45a53a4245c7a6dedb4301e883b9557452525afba8543f54d1b1a9b7727cd39ca0a26353effd5c01e8838fa8e8a4a4b46e50a996565eb6363a3e95eb2f6a6a956271151d9bd2f216d800fa6057159d94949e3f30529dafa26353b93cd325d55754746c4acb832abd1709809a9da8a29392d2cbf118803d1ad2bb53a5cefdb73fabe8f8945e8e8b650025b1057075f9032387cb54747c2ad61754aa77aae8d894a78f2a0035bb57452724e5c9cff1532da7987bde5b1eb4973a25d30e52d1f1294f3b2a0035f21b231f2cd57682cae1fb2a3a3e8d9c07507ae39e54d3ab8755f41a94a7ef2900355a54452723e5eb25358f4a35abf24e83d16b50dc8f550e9f57d1f1295fec0900d48c2980f594bafdec44ecd950bcc755ae2566d99eb9fa980a08d46c1b159d8c94b7fb558ee7d076b28a5e8326e55bff9f54397c5845af41797b5101a8d1812a3a19297fdba91c66565ed4267a0d7abd43552eccc0a82fef8209a026ac01505f7e7e3f93ca61bc7a4145afd3f5ae53b91695595745af41d5b484025093735474225235e5dcf4c4bb05bea6a2d7e96a7ed4b2a0cac1cb06dfaca2d7697adef5f02ee5e5a82f5517ab0bd4e96fe4dd3ffd9f5da5fccfdca6bc836574ac3a5b4d01a8c9152a3a11a99a9e50b3ab5cfe5345afd3c5bc73e27b552e1babe8759a90a7eedeaace54072b8fe5595d2dac52ee328d535ef0c85327b750df541e9d7fb78a7e8edc7d4601a889affca31391aa2bf77c67bf4947afd3a5fc386455958bc759f81b74f45afdc8db757bc73cefdbf141e50feaba791aeacaeacbca1705556c55bdbd025093475574225275f9dbdb07542e6f5247a9e8b5bad0f32af732b21e4418bd565df982e63cb5b3f2780fff3b6e9a37ab65952f4a3c5032c782625f53006ae0679cac02d89f3caf3cd7b4c0893cbe207aad41ce8f54727ef3b7f7ab7e9c17cfa853d56795ef40b4cddcca8f22bc7ba517bf8a7ec7b1fa8102508339547412523dedad72db5abdaaa2d71bb43cab22e7337ff38e81750efcf3204e0fc6f3788319d4a0f0743e8f4ff9a38a7eef913a4501a8c1622a3a09a99ebcf0c97b546eebab2755f49a83d2352ad768ffc9eda7a2d7cb9d9fe9fb76f7226ad0793b6b4f37f6a39ae86f317967290035f0b3c5e824a4faf2f2a7b3a8dc16529ee215bd669bf30a7f87ab5cf3fc27e715ffaabef5ef7fdfde9ad89b0b758d67bfecafbc4473f4b7719eaa08a0061ec0139d84546f1edd5d057f481ea6fca119bd6edbf20787ef6e54615ee56fe5d1ebe6e826e5a5893deea6eb7cc1bbbb7a484df977ba5c01a8816fcd4d7902527ffa0f55152faef22715bd6e5bf2e235b936f6999247b357b5dcafbff16fa89a388abfdfbc5ec16e6af2e984d72b0035f07ce2c9dfaca87f79d47495aba079709ba7943dada2d76f6afe005d5355e9bb2a7aed94fcedd65b08fbef8ed179f6c031ca8f5fbcb811801afc9b8adebca83f794adbbb5595e6575e88a8c880ac7e76a7f2b3f2dc5325a7e439ecd1ebf79a3fc48e506c6a53ded2ca830501d4603d15bd8951ffba4f2da0aa36973a40356db6c0dfd5b6aa8e6fceb9f752f0a0cb65140034de062a7a23a3fef667f5165507bf8e3f70bd2744bf060b7ae31acfff5e4bd53548ce23fe5f56d1cf53364fe7f4f2b80cf003d01a5c0034b71b95bfa5d76951e5695ade4eb7eae9705ef5ce4bdd7ac399bad7b5f746377efde8e72a9b0757f2ad1f40ebf008a0d9f94e80a7a7f583ef0c78ca9dc70b5cab52377ef1a38d0bd55ecab34ffa3538ee63cadfd8a39fb14cbe5b7288aa7a8c020054824180cdcf3bd2bd4335812f46fcedd983f3bcef8077213c529da43c4def58e54d74beaa7c4bdccbdb7aad897eec5c17f174bc5754f4772e93ef1e347ddb5a3f8ef0e87a6fedbb8af24c8a7595f71970febffd9ff9bff33fe37f9647184087300db01dddab72af79df35de6636c7630d6fe2e425b49bc2776a56573ba8e3d4a5ca33287ab9d0f1ffc683302f513e968fe9f70866340003682515bd1150f3f26debcd15caf192bbc7abe86f5a363fc2a862d9e632e6519b29df6d99a072ce621829bf861f47f9353fa77cb70040cbb11470bbf273e7839557aec3d8fc41f53b15fd2dcbe66fc4fd1ab7b094f226455e25af8e0ffcb1f2cfe081a25f517e7c00a085bce84c748253b3f37eebfe268891f9d6f5032afafb95c91f765f5275f3bf5faf97df86259cfd58c44bfa7267006811b6036e6f0f2b4fe3c450de53df0311737c53f69881ad549d56501e50996b8d823af3cf7c9afa8002d07073a8e844a6f6e40f8b39155e5f4ad6df46a3bf53d9fce1ef350aeab2aaf2ba08d1cfd2c6bcb0d4c7158086f2b49faa177ca16af3b7aeafabaef35ef3de4a36fa1b95cda3e1eb9ae6e7a9785e4238fa3906a12b952f04d80d1168a0475574e252b37b561dae165498643975b2eaf5c2d6032debb8edefd503fde118fd0c83d81f1453598186b94d45272c35335fb07935bdbaf60a682b0f703d4395dddfc05b26576966e5991cafaae8f50739ffcebe689d550168003fab8b4e566a562f28afbcc7a22ce5785cc0b92afa9b4ed93754957c2bdc4b2247afdda5fe5bd539be02c008ce51d1494acdc8a3d9bdd42eb7fad378f7bfd106087a37c2aa9e537b53a7411ae0972b5f98318015e8a31faae8e4a4fe77b562a7b97cbc8092b73e7e4c4dfe77f6023b33aa2a784a9cf77398fcf56852be23e2fd0800f4c1812a3a31a97f3dafbcd90e2bfe55c33306bcacadc7073ca41650b9f96e82c713e4d87c68d0f3d88003149b110135db46452725f5a75faa2a3e90309ca7e0793f8cdc7c8171818afefdd2c879854b06b70235f2b3d1e864a47a7b5a6daad06ef3a93faae8df318dddad8a0b60a026de6b3e3a11a9bebcb1caa2aa0dfcedd6031297549e73bf869ab8cffc3acadfaafddcdbffdd22cabbf1758537c6b94745ff8efb910790deaf3cd5f70675b1f2a05fe7ffdbff99ff3bff3365a74b56d9ddcaffff05a062d32a5603ec4f7ed3fd8e9a4e35894766fbce9007cc7d4b9da53c82de6313a2df63acfc9cdd1f367ebce1dfd7db1abf47f56b77bd2af8a2a79f8b6a7960e3d96a5fe58b314f7f2c33b0d1ffacff371b2a1fc3c77a5c45af55478fa8e515808a35e95b4b577a4aada79ac077813c2fdb03e37c0bb6ae6f832f295f181ca1d657fdde6bbf571f515e9931fa1dabca03e73ccec03b157aa6481503e87cccf7a95dd585aaeec58bfc37f58528800ae5da339d8af577e53ddefbc5b30b7cabde1ff839b6cccd953f60bc64acf7be1fafdac0df52ebfcf0f7059a6788ccabeae6cdc37c57a8cec5c37cd7a98a819a00dec05a00f5f55bd58fc54ffca1ef6faac728df5e8d7eb6a6e5bdf0f7540bab26f233ff3a6efbfbc2c8fb1b34692d7dff2c5e3ca98ebb02feffd72514800aeca2a2138ff276bcf2988b3abd55edaf1e54d1cfd486fc48c23bfd7987bea6ac8d30bff260b5e8e7cd95777af4077f933ffcdeaebcbeff8b2afa1d72e50583580d13a8005301abcf1bc0d4c9cf857d8bdf7b08443f4f5bf3230b2f1ae3dbd1fde2d79ea0a29f2f5767aa367de0f92e8d070e46bf4bae3c1095750280cce656d1094779da47d5c5cff673ed8bdfe49e5107a9ba3f10bcc25fd1cd857ac9e3433cadb2adfc65a2ca1d46bd5850557b36009de51dbaa2138e7acfb7ae3d4abb0eef52a7abe8e718e43c55cd83e2aa5acb7f4a1e9310fd1ca979cebe777b9c41b59dff5d1ca2aa9a4db29b029091a7f944271bf596dffcb65355f3aa69de2dd01f20d1cfd195fea1b652557e3b5c5155b1b6bf07b97d540d1a0f3aad62ec89071eb2811090d1b75574b2516ff95b69953c98f02b6ad09ef1a776995a5ce5e61510ab582fc353703da070507910aa571d8c7ef7943c2890ad84814c3653d18946e5f3ea7955f22a7a37aae8b5e9f511e91e779173c64515fbf97bfaed20ad863812ff8ebe4b15fd0d52f2ca920032f05af4d14946e53a4155751bda6fa4fe60f3f4b0e8b569687e2490c3262a3a7e4a9e3ad7a5c16cfe5d3d7b23fa5ba4e4658f0164d0e6b9e24dc8df12ab9aa7eeb9e05e36377a5d1adec32ac7c0c05955ce01b21e1be27537baca8362730e0ef4d88fb62e210d348a377d894e321abbdbd56caa0a1e20f6a48a5e97e2f656397c4f45c7ef35afabdf75dbabe86fd36bde600a4022bf394527188d9ef7f2f734bcdc7cdbd48309bb3ec2bf6ccfa91c03c43cd622e752b75eb700aff334c1e86fd44bfe77e49d0c012458414527188d9cb752ae620ad73875868a5e9346ef4895ca175f57aae8f8bde4017ffd7ae6efa9a2cb292f12f58937f2ffedffec6daa1ffcb7c83930f0f70a4002ef4bcfb4b272e5bad53c394f9dfaa38a5e8fc62ec7a639deaa393a762f7903a8ba46fb7b554f0f8c3b5a79cc48919d0abdaaa2ffd9ef2befb73097aa83ff263957ad5c5b0148c0d6c0c5f39cf3dcfbb0fbdb9ac71344af4763778dca21d796b75ee4a7ea79fe1eece80f7d0f42cdf1c8c28f9cfcfb7bebdfaa07d8795be35c838faf520012789a597472d1d03c286f2195937756f35af0d1eb51b1b651a9d650d1b1cbe60fd22abf95fa9bbaa7d63da1a2d7cf91ef0e78caa23fa8abb28eca35cee5830a408f9655d1894543f3add29cbcb7bca73445af45c5f2e3ab1cdf582f51d1f1cbe6b5fdab30bdf22a901eec18bd6e15f951821f77f9316115720d0afc8d02d0230fce613d80d13b51e5e4a56b1f52d16b51f17ea15279bdffe8d865fb9baa62631fdf9db84345af59477e3cb5bacacd8f31ee54d16b966d7905a0473f52d18945af7f507b5df85c3c5dedaf2a7a2d2ad7a62a55ae1d157d5b3b272f30f535d58429a19ef9b2bfcabde855ae81973f55007ab4b18a4e2ccaf3213391bff5e49c6ad6e55e525eb52fc53c2ac732cb67aa9cbcc0d4a52a7aad7e76914afd9b4fc97771a2d72ad3f38ad501811ecda17c951f9d5c5dcebb9ae5e2d903acbc98af5fab54bbabe8d865f285c8822a97f95493a784deac720e10f440d81c1761b9f681003a29d734a841c93bcce5dc66f65015bd0ef596d7984ff527151dbb4cc7a85cfce1efb104d1eb3429ff8c392f02bca156f43a65f21d13003df28625d189d5d5beae72f10c82e835a8f75217fff12c8ce8b865f25db3c5540ebeb57e938a5ea789f9e2e92d2a07ef4c9aba9e81c74ae4bc1303748a172f69c280a326f4a8caf5acd36f4a8fabe875a8b7bcf35fea32bbfba9e8d8653a59e5e0c1754d7ce63f561e13906b612c0fe48b5ea34c7b2a003dfa838a4eacae956b07372f7dcaa395fc9daa525da7a2639729c712c4e6d1fed1f1db906707e4b08c8a8e5f2656060412eca8a213ab4bdda3bcf04a0e07aae83528add4fdf53dfa3ff56ed78d2a07cff36ff39d373f06c9b54e40eae047ff2c1ed00ca0071e84d4f5c7005bab1c5653ccaca8a655558acd5474dc32a55e84982f3407614d88db548e1503736c4fbe9102d023ef64169d585de82e9563b19369d50415bd06a5e58baa99558ae35474eca279c05a8e51f039c62134a5bd542aef8a993a18d03b1c02e89177048b4eac2eb493cae1bf54747c4acf1756a9522fce2e50a9bcb14f956bfb7b20eb2dea7ce535133c6aff3115fdb339f22642396ebf7b606174fca2f97706d023afa855644ff141cb3bac8d53a9fccdf02915bd06a5973a00d053d7521f73e518245ac5f810cfcff75d85a5d5483cd8ce03f7aad885d23b15a6da4d45c72e9affdde65ead10e8146f7e139d5c83dc412a871cd39968e452d767f080b5e8b865f287680a5f687a7be9e8d8bde4fd2afe53f9d15351fe6777509e52191db3977c119dfa786639151dbb4ca96344804e5b494527d6a0e6a548bd0e422a0ffcfba78a5e83f2943a48d38f79a2e316cd6b3aa4ce7ddf5245c7ee254f7d4b198fe0471197abe8d8bde40196293c06c71712d1b18be68b2100099abc1e79eebc235c0e5d1e405957a9dfee5207007a3f875497a8e8d8653b4de598b2ea63e4da1531c7fefca91b04fd400148b0b38a4eae41ccdb92a6f20753746cca5bea72afa92beeedab52ccad724cb5f5ae92b9d6ab301fcb7713a2d72a93676978cbeb14a9b3233c901040028fe8f58638d10936483da8bc625f2a8f0c8f8e4f794bfdd0bb5345c72dda67550affefa3e396c9cffc3d652e373f4a784445af59a60d540acfe58f8e5bb43b1480445d180c78884af57e151d9bf2f6b44ae167f7afa8e8d8454b5dfef768151db74c553ee3ceb11ae8512a45eab2c01ed3936b8f02a0b3c6ab411fd4b6944a95facc928ae5a96b293ce02d3a6ed17ceb7e4695e206151dbb689eea5766b47f595ed12ff52e89f75948e19904a9ef3b2c090c6430c8b7b6bd384aaa451423ffebe91a9522750be007540aef6098bac6869f8f57cdf3f9a3d72e5aea9d1af3a3b9e8d8455b420148b4a68a4eb04128c79effa96f9654bccb548a555474dca279cdfb146f53d171cb34da223fb9bc4f45af5da6d469b5b7abe8b845f354660019dca4a293acedada052f81b5deaed522ade852ac55a2a3a6ed1526f6d2fafa2e316cdcbfbd6c1ff7fedf50ea29fa1685ed027c5f52a3a6ed1fcc50540069babe8246b731e499d3a50c85bb946c7a66a3a57a5f8988a8e5b344f214cf161151db76837abba784dfde86728da87548ad43535d6550032f0a0a3fb5474a2b5b51354aa1fa9e8d8544da98bf0787a5a74dca29da3527c4245c72d9a37f5a94bead89ff5558a5faae8b845fb940290c9762a3ad1dada862a85472a7771d3a47e96ba6263ea1cfcb6bf7e19a92b03a6ae97d0efd7073019df0518a4e7dda98394fc0d273a2e55171700f5e10200c010de88253ad9da56ea7c723b5245c7a6eae202a03e5c000018c23b75a54ecf69427e769feaaf2a3a3655171700f5e10200c0309f53d109d7a6bea0522caca2e352b57101501f2e00000ce3a973135474d2b5a5d4f5dcb755d171a9dab800a80f17000042de3e373ae9dad0ab2a7547b9d43727eaadb67f007301505cbf5f1fc028daba4740ea72ae76978a8e4dd5c605407db800003022efa297bab56a3f4a5d4c669c62f39ffec405407db8000030aac35574f235b903558a1555745caa3e2e00eac30500805179cfedc75474023635cf6248e11904d171a9fab800a80f170000c6b4bd8a4ec0a6f65195820580fa171700f5e10200c098a65137a8e8246c62de112e45ea2e65d47b5c00d4870b000085bc4f797a5d742236addd54af7cb1f38c8a8e4bd5c705407db8000050d8c12a3a119b96bfc1f7ea832a3a26d5131700f5e10200406133aa3b54743236294fe15b5af5c2fbc147c7a47ae202a03e5c000028650dd58639f2d7aae954199f56d1b1a8beb800a80f1700004a3b5e452764d3f21bccb4aa88d515cffefb5fdb3f80b90028aedfaf0fa007b3a9bb557452362ddf0918af46e2bb047ba91755f4bfa77ae302a03e5c0000e8c92aea7f547462362d3fb2f0be06bba8b5941f636ca4beab1e52d1ff86fa131700f5e1020040cfbea6a21393a8d7b800a80f1700007ae639f357a9e8e424ea252e00eac305008024ef504fabe804252a1b1700f5e1020040b2ad54748212958d0b80fa700100208bb64c0da466c705407db8000090c5f4ea3a159da84445e302a03e5c0000c86641f5888a4e56a2227101501f2e000064f561d596f501a8797101501f2e000064b7a78a4e58a2b1e202a03e5c0000c8ee4dea54159db444a3c505407db80000508919d4152a3a7189468a0b80fa700100a03273aabfaae8e4258ae202a03e5c0000a894570a7c58452730d194b5fd03781d151db76827a8ba700100a072ab2ab6dba522b5fd02e05d2a3a6ed1f65375e10200402d3654afa9e844269a58db2f003c00f67e151dbb482baaba700100a036de33c07bf3472733916bfb0580f95b7c74ecb1ba594dadeac20500805aeda8a29399c80dc205c03875a78a8e3f525e3c6b7555272e0000d46e5f159dd04467a9141ba8e8b8454b7dfd89dead1e53d16b4c99ef8aeda0eae6df35fa798ae6bf750a2e00808e3a4845273575bb5fab146ba9e8b845fb8dcac53360ae51d1eb4ccc7b677c5af5c3452afa998af66f2a05170040871da1a2139bbadbe52ac54a2a3a6ed1ae52397950e027d569ea5ef5b27a48f9f7dc55cdaafae56a15fd0d8a963a60910b00a0e3bea6a2939bbad9f52ac578151db768b7abaeb843457f83a279ca630a2e00004cb5878a4e70ea5e7f5129deaea2e316ed15358d1a74d3aa5755f437289ab7ff4ec10500807fd94d314590ee5329a657a9db512fae06dd3b55f4bb17cd170fd3a9145c0000f87fdb2a160bea76fe6049fd065e760ade9475e183652315fdee45f3e383545c0000186273e5dbb0d1094fdd681195c23309a2e316ed2835e88e51d1ef5eb473552a2e00000cf361f5a48a4e7a1afcfcef3fc5612a3a6ed152c721b441ea00c06fab545c0000087921957b5474e2d360b78d4ae1ff7d74dc322da10655eaf37fe765bd537101006044f3a91b5474f2d3e0f60d95c283f8a2e396c9d3530795ffbed1ef5ca6d4c734c605008051cdac7ea9a237001accce57a93c9b203a76d1ee56756ecc5397372b2f4814fdce45bb4be5c005008031f94deb10c534c16ee435f4bd825e8a1fa9e8d865fa8c1a341babe8772dd3092a072e000014f671f5948ade0c68b04a7d06bf858a8e5ba69b54ea854893f877b94545bf6b99365539700100a0147f304c50d11b020d4efe004f31bb7a4945c72e53bf36eaa9c2862afa1dcbf4829a4de5c0050080d2665167aae84d8106a3efab5467a8e8d865f25802efefdf763e67ee57d1ef58a69fab5cb80000d013dfcef4f2c12c1a3498e55869ce8f8ca26397cde34fdaee5015fd6e655b57e5f25315bd46d13ea10074d872ca3bb8456f10d4eebc16440a6f78e3fdf6a36397c97b0ba42e4ed44fdeb73fc712dbdec638e74649a91725cb2b001d37a33a5c456f12d4def651a9726d37ed0f3faf4bd1366f550faae8772adb7e2a27dfc28f5ea748cfa9191400fccb27d5a32a7ac3a0f6759d4a35a77a5645c72fdb35caeb52b485c72ef86f18fd2e657b5a7960654e1e97d0eb92df3f510030c4fcea0215bd6950bbf2ba0f0ba8545eb73e3a7e2f79a3213f5a683aff8c17aae877e8a56faa2aecaea2d71b2dcfee584c0140c8b717733cffa5feb6874a35af7a5145c7ef25af4ce9c74e4de59fcdbbf5453f7b2f3dafe65155f0225f652fd8b75600302adfb23c56456f22d48ebcb77f8e2579bfaaa2e3f7da1f54ee5be239cca1ae54d1cfdc6b5f51559a499da6a2d79e3c7ff3e7c31f40291f55ec2cd8ded651a9fcadd81713d1f17bed6f6a59d514ef57b97f474fc79c5ed5c1d3fa3cce62ca25bf3de0cfcffcb9ed0fa0271e10e56947ac1bd0be7ccb3d07cf618f8e9f92bf95eea4fab971906fa3efa272ac7c38656babba79b6c56a6a7df501d5e4c72d005a6449f52b15bdd95133f33cfc85540e55ad2079adf29a1475f35cf8aab6ccf66d790018381f517f52d11b1f35afa3540e9e1698ba55f048f942c5b7aa5317302a62bcfa99cab1c04f941f9935718c030064e1a952be7dfbb88ade04a939bdac165639acacaa7c14e40fe573949722ce3965703ae5e7e47e2452e5d6d8fedbaca00060e07941124f3763abe16677bccaa597f9e7bde4a9a89e89b291ea652a9d57f2f31efec7a9ba16b9da550140a7f8f6f001caab9e456f8cd4df7c8bdd633872f06652be851ebd4e55f95bfb5dcaf3debfa77cd1b99dfadc1bf9fff67fe6ffce8bf8f89fadf29b7ed4c9ca7f1b00e8247feb3a4ce55c3c86f294733b5adf9e67d5c8495dacfc9801003acfcb0a7b2b581e0d34abb5542e5e8426f7c2396dcc7b0678aa2c0060327e63dc59b1985033ba5be5dc94676e75b38a5eab0bdda8e65200801178b1178fecbe5a456fa4545fb937a7f145de452a7aad41eeb76a36050028c82b9579c0d40b2a7a63a56af354b5f7aa9cbce46d9135e907a5b3157bea03408ffced695b75938ade64a9bafeacfc0c3f272fabfb2d55f7e8fb3af3ef7690eae7f2c5003050bc2cabe76b3fa9a2375ecadf49aa0a6baa8754f49a6dce6b09784f04004005fc2dd21f207e44c09a02d557d516b10baa2b54f49a6decf76a010500a8819fb17ae0a02f069e51d11b33a5e5f51a965155f06df22dd4632a7aed36e43b529ec5e20b5300401f785b53df7e3d52792ff9e8cd9a8ae79501af52fbaa5c2b048e640ee5257ddb3436c03fab2f3c7b5982180050a1c5d40eea7cf5bc8adec469680fa853d4a6ca4b37d76d45e57f5f4dbe10f0cf769ef25efa008086f3a3825595d7823f57b5f99673cefea1fc81ff79b5b86a0a4f41f4b76bdf85887eee7ee45d08fdc1efc1a8008096f2862c4ba96dd489ca2bd5794bdce88d7f50f280c9cb94975ffeb45a48359defe27c5579e39ee877aaa33b9537b15a540100069037af19af36515e09ef57ea5e157d2834392fe273ab3a5deda73ea3fc0cbfcdf3d27dc1e6c5a18e51fe408e7eef9cfd5dfd40f9ae11bbf70140477970e1bbd47a6a7bf56d75a6ba413dac7c6b38fa10a9aa97d47dea1ae50f797fa3ffa25a5bf9367e17769c7bbbf2f4443fbe98a052eedef8efe963f891838fb9b00200a0100f985b42ada43ea6b654bbaabdd5c16f7484f24877e70fee8979b19d89ffb9bfe1fa9ff5ffce03187d9c4fa9d5951f59ccae309ca7e0f916bd6781eca4f651fe3bfaeff993379af8b7f57fe77fc6ffacff374cdf0300000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000090c35453fd1fe30bc4565d7de2950000000049454e44ae426082, 'Makati City', 'Single', 'Filipino', 'n/a', 'Muslim', '', '', '', '', '', '', '', '', '', '', '', 'Active');

-- --------------------------------------------------------

--
-- Table structure for table `leave`
--

CREATE TABLE `leave` (
  `leave_id` int(100) NOT NULL,
  `leave_code` varchar(100) NOT NULL,
  `leave_desc` varchar(100) NOT NULL,
  `total_leave` int(100) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_general_ci;

--
-- Dumping data for table `leave`
--

INSERT INTO `leave` (`leave_id`, `leave_code`, `leave_desc`, `total_leave`) VALUES
(1, 'VL', 'Vacation Leave', 15),
(2, 'SL', 'Sick Leave', 15),
(3, 'Calamity Leave', 'CL', 5),
(4, 'MLN', 'Maternity Leave Normal', 7),
(5, 'MLC', 'Maternity Leave Cesarean', 75),
(6, 'CL', 'Compassionate Leave', 7),
(7, 'OBT', 'On Business Trip', 30);

-- --------------------------------------------------------

--
-- Table structure for table `leave_balance`
--

CREATE TABLE `leave_balance` (
  `leave_balance_id` int(11) NOT NULL,
  `emp_no` int(10) NOT NULL,
  `sick_leave` decimal(65,2) NOT NULL,
  `vacation_leave` decimal(65,2) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_general_ci;

--
-- Dumping data for table `leave_balance`
--

INSERT INTO `leave_balance` (`leave_balance_id`, `emp_no`, `sick_leave`, `vacation_leave`) VALUES
(103, 196, 5.00, 12.00),
(130, 2, 20.00, 30.00),
(147, 3, 0.00, 0.00),
(148, 4, 0.00, 0.00),
(149, 5, 0.00, 0.00),
(150, 6, 0.00, 0.00),
(151, 7, 0.00, 0.00);

-- --------------------------------------------------------

--
-- Table structure for table `loan`
--

CREATE TABLE `loan` (
  `loan_id` int(10) NOT NULL,
  `emp_no` int(10) NOT NULL,
  `comp_loan_amnt` decimal(65,2) NOT NULL,
  `comp_bal` decimal(65,2) NOT NULL,
  `comp_month_ded` decimal(65,2) NOT NULL,
  `sss_loan_amnt` decimal(65,2) NOT NULL,
  `sss_bal` decimal(65,2) NOT NULL,
  `sss_month_ded` decimal(65,2) NOT NULL,
  `pag_ibig_loan_amnt` decimal(65,2) NOT NULL,
  `pag_ibig_bal` decimal(65,2) NOT NULL,
  `pag_ibig_month_ded` decimal(65,2) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_general_ci;

--
-- Dumping data for table `loan`
--

INSERT INTO `loan` (`loan_id`, `emp_no`, `comp_loan_amnt`, `comp_bal`, `comp_month_ded`, `sss_loan_amnt`, `sss_bal`, `sss_month_ded`, `pag_ibig_loan_amnt`, `pag_ibig_bal`, `pag_ibig_month_ded`) VALUES
(98, 196, 1.00, 2.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00),
(125, 2, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00),
(142, 3, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00),
(143, 4, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00),
(144, 5, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00),
(145, 6, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00),
(146, 7, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00);

-- --------------------------------------------------------

--
-- Table structure for table `monthly_tax`
--

CREATE TABLE `monthly_tax` (
  `mt_id` int(10) NOT NULL,
  `pay_trans_id` int(10) NOT NULL,
  `semi_monthly_tax` decimal(65,2) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1 COLLATE=latin1_swedish_ci;

-- --------------------------------------------------------

--
-- Table structure for table `others_id`
--

CREATE TABLE `others_id` (
  `other_id` int(10) NOT NULL,
  `emp_no` int(10) NOT NULL,
  `sss_no` varchar(100) NOT NULL,
  `pag_ibig_no` varchar(100) NOT NULL,
  `tin_no` varchar(100) NOT NULL,
  `philhealth_no` varchar(100) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_general_ci;

--
-- Dumping data for table `others_id`
--

INSERT INTO `others_id` (`other_id`, `emp_no`, `sss_no`, `pag_ibig_no`, `tin_no`, `philhealth_no`) VALUES
(79, 196, '123', '123', '123', '123'),
(119, 2, '123456789', '123456789', '1234567898', '12345678999'),
(136, 3, '', '', '', ''),
(137, 4, '', '', '', ''),
(138, 5, '', '', '', ''),
(139, 6, '', '', '', ''),
(140, 7, '', '', '', '');

-- --------------------------------------------------------

--
-- Table structure for table `other_details_main`
--

CREATE TABLE `other_details_main` (
  `religion_name` varchar(100) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_general_ci;

--
-- Dumping data for table `other_details_main`
--

INSERT INTO `other_details_main` (`religion_name`) VALUES
('Roman Catholic'),
('Iglesia ni Cristo'),
('Aethiest'),
('Muslim');

-- --------------------------------------------------------

--
-- Table structure for table `overtime`
--

CREATE TABLE `overtime` (
  `ot_id` int(11) NOT NULL,
  `ot_code` varchar(100) NOT NULL,
  `ot_desc` varchar(100) NOT NULL,
  `ot1` decimal(65,2) NOT NULL,
  `ot2` decimal(65,2) NOT NULL,
  `ot3` decimal(65,2) NOT NULL,
  `ot4` decimal(65,2) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_general_ci;

--
-- Dumping data for table `overtime`
--

INSERT INTO `overtime` (`ot_id`, `ot_code`, `ot_desc`, `ot1`, `ot2`, `ot3`, `ot4`) VALUES
(1, 'OD', 'Ordinary Day (MON-FRI)', 1.25, 0.00, 0.00, 0.00),
(2, 'ODRDND\r\n', 'Ordinary Day (Night Diff.)\r\n', 1.25, 1.10, 0.00, 0.00),
(3, 'RD\r\n', 'Rest Day (Sat & Sun)\r\n', 1.30, 0.00, 0.00, 0.00),
(4, 'RDND\r\n', 'Rest Day (Night Diff.)\r\n', 1.30, 1.30, 1.10, 0.00),
(5, 'RH\r\n', 'Regular Holiday (Mon-Fri)\r\n', 2.00, 0.00, 0.00, 0.00),
(6, 'RHE\r\n', 'Regular Holiday(Excess 8hrs.)\r\n', 2.00, 1.30, 0.00, 0.00),
(7, 'RHND\r\n', 'Regular Holiday(Night Diff.)\r\n', 2.00, 1.30, 1.10, 0.00),
(8, 'SPH\r\n', 'Special Holiday (Mon-Fri)\r\n', 1.30, 0.00, 0.00, 0.00),
(9, 'SPHE\r\n', 'Special Holiday (Excess 8hrs.)\r\n', 1.30, 1.30, 1.10, 0.00),
(10, 'RHSS\r\n', 'Regular Holiday (Sat-Sun)\r\n', 2.00, 1.30, 0.00, 0.00),
(11, 'RHSSE\r\n', 'Regular Holiday (Sat-Sun Excess 8hrs)\r\n', 2.00, 1.30, 1.30, 0.00),
(12, 'RHSSND\r\n', 'Regular Holiday (Sat-Sun_Night Diff.)\r\n', 2.00, 1.30, 1.30, 1.10);

-- --------------------------------------------------------

--
-- Table structure for table `pag_ibig`
--

CREATE TABLE `pag_ibig` (
  `no` int(10) NOT NULL,
  `min_amnt` decimal(65,2) NOT NULL,
  `max_amnt` decimal(65,2) NOT NULL,
  `emplyr_cont` decimal(65,2) NOT NULL,
  `emply_cont` decimal(65,2) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_general_ci;

--
-- Dumping data for table `pag_ibig`
--

INSERT INTO `pag_ibig` (`no`, `min_amnt`, `max_amnt`, `emplyr_cont`, `emply_cont`) VALUES
(1, 0.00, 999999.99, 100.00, 100.00);

-- --------------------------------------------------------

--
-- Table structure for table `payslip`
--

CREATE TABLE `payslip` (
  `pay_id` int(10) NOT NULL,
  `emp_id` varchar(100) NOT NULL,
  `fullname` varchar(100) NOT NULL,
  `date_from` varchar(100) NOT NULL,
  `date_to` varchar(100) NOT NULL,
  `pay_date` varchar(100) NOT NULL,
  `department` varchar(100) NOT NULL,
  `absences` decimal(65,2) NOT NULL,
  `monthly_pay` decimal(65,2) NOT NULL,
  `basic_pay` decimal(65,2) NOT NULL,
  `tax_sub` decimal(65,2) NOT NULL,
  `ot_pay` decimal(65,2) NOT NULL,
  `other_inc` decimal(65,2) NOT NULL,
  `adj_refund` decimal(65,2) NOT NULL,
  `gross_pay` decimal(65,2) NOT NULL,
  `net_pay` decimal(65,2) DEFAULT NULL,
  `ytd_gross` decimal(65,2) DEFAULT NULL,
  `ytd_tax` decimal(65,2) DEFAULT NULL,
  `daily_rate` decimal(65,2) NOT NULL,
  `wtax` decimal(65,2) DEFAULT NULL,
  `sss_ee` decimal(65,2) NOT NULL,
  `pag_ibig_ee` decimal(65,2) NOT NULL,
  `philhealth_ee` decimal(65,2) NOT NULL,
  `other_ded` decimal(65,2) NOT NULL,
  `comp_loan` decimal(65,2) NOT NULL,
  `sss_loan` decimal(65,2) NOT NULL,
  `pag_ibig_loan` decimal(65,2) NOT NULL,
  `gross_ded` decimal(65,2) DEFAULT NULL,
  `comp_bal` decimal(65,2) NOT NULL,
  `sss_bal` decimal(65,2) NOT NULL,
  `pag_ibig_bal` decimal(65,2) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_general_ci;

-- --------------------------------------------------------

--
-- Table structure for table `pay_details`
--

CREATE TABLE `pay_details` (
  `pd_id` int(10) NOT NULL,
  `yyyymm` varchar(100) NOT NULL,
  `date_period` varchar(100) NOT NULL,
  `emp_id` varchar(100) NOT NULL,
  `fullname` varchar(100) NOT NULL,
  `department` varchar(100) NOT NULL DEFAULT 'none',
  `emp_status` varchar(100) NOT NULL DEFAULT 'none',
  `absences` int(100) NOT NULL DEFAULT 0,
  `month_pay` decimal(65,2) NOT NULL DEFAULT 0.00,
  `regular_pay` decimal(65,2) NOT NULL DEFAULT 0.00,
  `daily_rate` decimal(65,2) NOT NULL DEFAULT 0.00,
  `tax_sub` decimal(65,2) NOT NULL DEFAULT 0.00,
  `others_pay_det` decimal(65,2) NOT NULL DEFAULT 0.00,
  `deductions` decimal(65,2) NOT NULL DEFAULT 0.00,
  `overtime_pay_det` decimal(65,2) NOT NULL DEFAULT 0.00,
  `gross_inc` decimal(65,2) NOT NULL DEFAULT 0.00,
  `gross_ded` decimal(65,2) DEFAULT 0.00,
  `net_pay` decimal(65,2) DEFAULT NULL,
  `wtax` decimal(65,2) DEFAULT 0.00,
  `other_ded_det` decimal(65,2) NOT NULL DEFAULT 0.00,
  `sss_ee` decimal(65,2) NOT NULL DEFAULT 0.00,
  `sss_er` decimal(65,2) NOT NULL DEFAULT 0.00,
  `philhealth_ee` decimal(65,2) NOT NULL DEFAULT 0.00,
  `philhealth_er` decimal(65,2) NOT NULL DEFAULT 0.00,
  `pag_ibig_ee` decimal(65,2) NOT NULL DEFAULT 0.00,
  `pag_ibig_er` decimal(65,2) NOT NULL DEFAULT 0.00,
  `company_loan` decimal(65,2) NOT NULL DEFAULT 0.00,
  `pag_ibig_loan` decimal(65,2) NOT NULL DEFAULT 0.00,
  `sss_loan` decimal(65,2) NOT NULL DEFAULT 0.00,
  `comp_bal` decimal(65,2) NOT NULL DEFAULT 0.00,
  `sss_bal` decimal(65,2) NOT NULL DEFAULT 0.00,
  `pag_ibig_bal` decimal(65,2) NOT NULL DEFAULT 0.00,
  `year_end` varchar(10) NOT NULL DEFAULT 'False',
  `ctr` int(100) NOT NULL DEFAULT 0
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_general_ci;

-- --------------------------------------------------------

--
-- Table structure for table `pay_trans`
--

CREATE TABLE `pay_trans` (
  `pay_trans_id` int(10) NOT NULL,
  `emp_no` int(10) NOT NULL,
  `emp_id` varchar(100) NOT NULL,
  `sal_id` int(10) NOT NULL,
  `loan_id` int(10) NOT NULL,
  `ot_pay` decimal(65,2) NOT NULL DEFAULT 0.00,
  `date` varchar(100) NOT NULL,
  `date_from` varchar(100) NOT NULL,
  `date_to` varchar(100) NOT NULL,
  `date_period` int(1) NOT NULL,
  `other_inc` decimal(65,2) NOT NULL,
  `deductions` decimal(65,2) NOT NULL,
  `other_ded` decimal(65,2) NOT NULL,
  `wtax` decimal(65,2) NOT NULL DEFAULT 0.00,
  `gross_pay` decimal(65,2) NOT NULL DEFAULT 0.00,
  `gross_ded` decimal(65,2) NOT NULL DEFAULT 0.00,
  `net_pay` decimal(65,2) NOT NULL DEFAULT 0.00,
  `absences` varchar(100) NOT NULL DEFAULT '0',
  `process_tag` varchar(100) NOT NULL DEFAULT 'False'
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_general_ci;

-- --------------------------------------------------------

--
-- Table structure for table `philhealth`
--

CREATE TABLE `philhealth` (
  `no` int(10) NOT NULL,
  `min_amnt` decimal(65,2) NOT NULL,
  `max_amnt` decimal(65,2) NOT NULL,
  `emplyr_cont` decimal(65,2) NOT NULL,
  `emply_cont` decimal(65,2) NOT NULL,
  `percent` decimal(65,2) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_general_ci;

--
-- Dumping data for table `philhealth`
--

INSERT INTO `philhealth` (`no`, `min_amnt`, `max_amnt`, `emplyr_cont`, `emply_cont`, `percent`) VALUES
(1, 10000.01, 59999.99, 0.00, 0.00, 3.00),
(2, 60000.00, 9999999.00, 900.00, 900.00, 0.00);

-- --------------------------------------------------------

--
-- Table structure for table `rec_tax`
--

CREATE TABLE `rec_tax` (
  `rec_tax_id` int(100) NOT NULL,
  `emp_id` varchar(100) NOT NULL,
  `non_tax` decimal(65,2) NOT NULL DEFAULT 0.00,
  `sss_annual` decimal(65,2) NOT NULL DEFAULT 0.00,
  `philhealth_annual` decimal(65,2) NOT NULL DEFAULT 0.00,
  `pag_ibig_annual` decimal(65,2) NOT NULL DEFAULT 0.00,
  `gross_inc_annual` decimal(65,2) NOT NULL DEFAULT 0.00,
  `wtax_annual_temp` decimal(65,2) NOT NULL DEFAULT 0.00,
  `total_non_tax_inc` decimal(65,2) NOT NULL DEFAULT 0.00,
  `tax_inc` decimal(65,2) NOT NULL DEFAULT 0.00
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_general_ci;

-- --------------------------------------------------------

--
-- Table structure for table `salary`
--

CREATE TABLE `salary` (
  `salary_id` int(10) NOT NULL,
  `emp_no` int(10) NOT NULL,
  `pay_mode` varchar(100) NOT NULL,
  `con_level` varchar(100) NOT NULL,
  `salary` decimal(65,2) NOT NULL,
  `daily_rate` decimal(65,2) NOT NULL,
  `daily_hour_rate` decimal(65,2) NOT NULL,
  `min_rate` decimal(10,2) NOT NULL,
  `pay_rate` varchar(100) NOT NULL,
  `pay_sched` varchar(100) NOT NULL,
  `rice_allowance` decimal(65,2) NOT NULL,
  `laundry_allowance` decimal(65,2) NOT NULL,
  `meal_allowance` decimal(65,2) NOT NULL,
  `medical_benefits` decimal(65,2) NOT NULL,
  `other_allowance` decimal(65,2) NOT NULL,
  `bank_name` varchar(100) NOT NULL,
  `bank_acc` varchar(100) NOT NULL,
  `bank_name2` varchar(100) NOT NULL,
  `bank_acc2` varchar(100) NOT NULL,
  `sss_ee` decimal(65,2) NOT NULL,
  `sss_ee_monthly` decimal(65,2) NOT NULL,
  `sss_er` decimal(65,2) NOT NULL,
  `sss_er_monthly` decimal(65,2) NOT NULL,
  `pag_ibig_ee` decimal(65,2) NOT NULL,
  `pag_ibig_ee_monthly` decimal(65,2) NOT NULL,
  `pag_ibig_er` decimal(65,2) NOT NULL,
  `pag_ibig_er_monthly` decimal(65,2) NOT NULL,
  `philhealth_ee` decimal(65,2) NOT NULL,
  `philhealth_ee_monthly` decimal(65,2) NOT NULL,
  `philhealth_er` decimal(65,2) NOT NULL,
  `philhealth_er_monthly` decimal(65,2) NOT NULL,
  `acc_code` varchar(100) NOT NULL,
  `13thmonthpay` decimal(65,2) NOT NULL,
  `14thmonthpay` decimal(65,2) NOT NULL,
  `15thmonthpay` decimal(65,2) NOT NULL,
  `mntbonus` decimal(65,2) NOT NULL,
  `emp_status` varchar(100) NOT NULL,
  `date_hired` varchar(100) NOT NULL,
  `date_resign` varchar(100) NOT NULL,
  `reason` varchar(100) NOT NULL,
  `designation` varchar(100) NOT NULL,
  `department` varchar(100) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_general_ci;

--
-- Dumping data for table `salary`
--

INSERT INTO `salary` (`salary_id`, `emp_no`, `pay_mode`, `con_level`, `salary`, `daily_rate`, `daily_hour_rate`, `min_rate`, `pay_rate`, `pay_sched`, `rice_allowance`, `laundry_allowance`, `meal_allowance`, `medical_benefits`, `other_allowance`, `bank_name`, `bank_acc`, `bank_name2`, `bank_acc2`, `sss_ee`, `sss_ee_monthly`, `sss_er`, `sss_er_monthly`, `pag_ibig_ee`, `pag_ibig_ee_monthly`, `pag_ibig_er`, `pag_ibig_er_monthly`, `philhealth_ee`, `philhealth_ee_monthly`, `philhealth_er`, `philhealth_er_monthly`, `acc_code`, `13thmonthpay`, `14thmonthpay`, `15thmonthpay`, `mntbonus`, `emp_status`, `date_hired`, `date_resign`, `reason`, `designation`, `department`) VALUES
(99, 196, 'Cash', 'Rank and File', 35000.00, 1463.41, 182.93, 3.05, 'Monthly', 'Semi-Monthly', 800.00, 400.00, 300.00, 0.00, 0.00, '', '', '', '', 562.50, 1125.00, 1077.50, 2155.00, 50.00, 100.00, 50.00, 100.00, 262.50, 525.00, 131.25, 262.50, '', 35000.00, 35000.00, 35000.00, 70000.00, 'Regular', '5/16/2024', '5/16/2024', 'Terminated', 'Jr Programmer', 'Sales'),
(125, 2, 'ATM', 'Rank and File', 40000.00, 1672.47, 209.06, 3.48, 'Monthly', 'Semi-Monthly', 100.00, 2000.00, 300.00, 400.00, 500.00, 'BDO Unibank, Inc.', '1234567', 'United Coconut Planters Bank', '1234567', 562.50, 1125.00, 1077.50, 2155.00, 50.00, 100.00, 50.00, 100.00, 300.00, 600.00, 300.00, 600.00, '', 40000.00, 40000.00, 40000.00, 80000.00, 'Regular', '5/16/2024', '5/16/2024', 'Transferred', 'Java Developer', 'Information Technology'),
(140, 3, 'Cash', 'Rank and File', 18000.00, 752.61, 94.08, 1.57, 'Monthly', 'Semi-Monthly', 0.00, 0.00, 0.00, 0.00, 0.00, '', '', '', '', 405.00, 810.00, 780.00, 1560.00, 50.00, 100.00, 50.00, 100.00, 135.00, 270.00, 135.00, 270.00, '', 18000.00, 18000.00, 18000.00, 36000.00, 'Probationary', '5/15/2024', '', '', 'Security Engineer', 'Information Technology'),
(141, 4, 'Bank Account', 'Rank and File', 20000.00, 20000.00, 2500.00, 41.67, 'Daily', 'Semi-Monthly', 0.00, 0.00, 0.00, 0.00, 0.00, 'BDO Unibank, Inc.', '29382732', '', '', 450.00, 900.00, 865.00, 1730.00, 50.00, 100.00, 50.00, 100.00, 150.00, 300.00, 150.00, 300.00, '', 20000.00, 20000.00, 20000.00, 40000.00, 'Regular', '5/16/2024', '', '', 'Sales Agent', 'Sales'),
(142, 5, 'Cash', 'Rank and File', 18500.00, 773.52, 96.69, 1.61, 'Monthly', 'Semi-Monthly', 0.00, 0.00, 0.00, 0.00, 0.00, '', '', '', '', 416.25, 832.50, 801.25, 1602.50, 50.00, 100.00, 50.00, 100.00, 138.75, 277.50, 138.75, 277.50, '', 18500.00, 18500.00, 18500.00, 37000.00, 'Regular', '5/16/2024', '', '', 'Accounting Officer', 'Accounting'),
(143, 6, 'Cash', 'Rank and File', 16000.00, 668.99, 83.62, 1.39, 'Monthly', 'Semi-Monthly', 0.00, 0.00, 0.00, 0.00, 0.00, '', '', '', '', 360.00, 720.00, 695.00, 1390.00, 50.00, 100.00, 50.00, 100.00, 120.00, 240.00, 120.00, 240.00, '', 16000.00, 16000.00, 16000.00, 32000.00, 'Regular', '5/16/2024', '', '', 'Purchasing', 'Accounting'),
(144, 7, 'Cash', 'Rank and File', 20000.00, 20000.00, 2500.00, 41.67, 'Daily', 'Semi-Monthly', 0.00, 0.00, 0.00, 0.00, 0.00, '', '', '', '', 450.00, 900.00, 865.00, 1730.00, 50.00, 100.00, 50.00, 100.00, 150.00, 300.00, 150.00, 300.00, '', 20000.00, 20000.00, 20000.00, 40000.00, 'Regular', '5/16/2024', '', '', 'Manager', 'Accounting');

-- --------------------------------------------------------

--
-- Table structure for table `sss`
--

CREATE TABLE `sss` (
  `no` int(10) NOT NULL,
  `min_amnt` decimal(65,2) NOT NULL,
  `max_amnt` decimal(65,2) NOT NULL,
  `emplyr_cont` decimal(65,2) NOT NULL,
  `emply_cont` decimal(65,2) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1 COLLATE=latin1_swedish_ci;

--
-- Dumping data for table `sss`
--

INSERT INTO `sss` (`no`, `min_amnt`, `max_amnt`, `emplyr_cont`, `emply_cont`) VALUES
(1, 9750.00, 10249.00, 860.00, 450.00),
(2, 10250.00, 10749.99, 902.50, 472.50),
(3, 10750.00, 11249.99, 945.00, 495.00),
(4, 11250.00, 11749.99, 987.50, 517.50),
(5, 11750.00, 12249.99, 1030.00, 540.00),
(6, 12250.00, 12749.99, 1072.50, 562.50),
(7, 12750.00, 13249.99, 1115.00, 585.00),
(8, 13250.00, 13749.99, 1157.50, 607.50),
(9, 13750.00, 14249.99, 1200.00, 630.00),
(10, 14250.00, 14749.99, 1242.50, 652.50),
(11, 14750.00, 15249.99, 1305.00, 675.00),
(12, 15250.00, 15749.99, 1347.50, 697.50),
(18, 15750.00, 16249.99, 1390.00, 720.00),
(19, 16250.00, 16749.99, 1432.50, 742.50),
(20, 16750.00, 17249.99, 1475.00, 765.00),
(21, 17250.00, 17749.99, 1517.50, 787.50),
(22, 17750.00, 18249.99, 1560.00, 810.00),
(23, 18250.00, 18749.99, 1602.50, 832.50),
(24, 18750.00, 19249.99, 1645.00, 855.00),
(25, 19250.00, 19749.99, 1687.50, 877.50),
(26, 19750.00, 20249.99, 1730.00, 900.00),
(27, 20250.00, 20749.99, 1772.50, 922.50),
(28, 20750.00, 21249.99, 1815.00, 945.00),
(29, 21250.00, 21749.99, 1857.50, 967.50),
(30, 21750.00, 22249.99, 1900.00, 990.00),
(31, 22250.00, 22749.99, 1942.50, 1012.50),
(32, 22750.00, 23249.99, 1985.00, 1035.00),
(33, 23250.00, 23749.99, 2027.50, 1057.50),
(34, 23750.00, 24249.99, 2070.00, 1080.00),
(35, 24250.00, 24749.99, 2112.50, 1102.50),
(36, 24750.00, 999999.99, 2155.00, 1125.00);

-- --------------------------------------------------------

--
-- Table structure for table `sum_bonus`
--

CREATE TABLE `sum_bonus` (
  `emp_id` varchar(100) NOT NULL,
  `bonus_netpay` decimal(65,2) NOT NULL DEFAULT 0.00
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_general_ci;

-- --------------------------------------------------------

--
-- Table structure for table `sum_table`
--

CREATE TABLE `sum_table` (
  `emp_id` varchar(100) NOT NULL,
  `sum_tax` decimal(65,2) NOT NULL DEFAULT 0.00,
  `sum_gross` decimal(65,2) NOT NULL DEFAULT 0.00,
  `max_ctr` int(2) NOT NULL DEFAULT 0
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_general_ci;

-- --------------------------------------------------------

--
-- Table structure for table `tax`
--

CREATE TABLE `tax` (
  `no` int(10) NOT NULL,
  `min_amnt` decimal(65,2) NOT NULL,
  `max_amnt` decimal(65,2) NOT NULL,
  `tax_due` decimal(65,2) NOT NULL,
  `less_amnt` decimal(65,2) NOT NULL,
  `percent` decimal(65,2) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_general_ci;

--
-- Dumping data for table `tax`
--

INSERT INTO `tax` (`no`, `min_amnt`, `max_amnt`, `tax_due`, `less_amnt`, `percent`) VALUES
(1, 0.01, 250000.00, 0.00, 0.00, 0.00),
(2, 250000.01, 400000.00, 0.00, 250000.00, 20.00),
(3, 400000.01, 800000.00, 30000.00, 400000.00, 25.00),
(4, 800000.01, 2000000.00, 130000.00, 800000.00, 30.00),
(5, 2000000.01, 8000000.00, 490000.00, 2000000.00, 32.00),
(6, 8000000.01, 999000000.00, 2410000.00, 8000000.00, 35.00);

-- --------------------------------------------------------

--
-- Table structure for table `tax_inc`
--

CREATE TABLE `tax_inc` (
  `tax_id` int(100) NOT NULL,
  `emp_id` varchar(100) NOT NULL,
  `tax_inc` decimal(65,2) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_general_ci;

-- --------------------------------------------------------

--
-- Table structure for table `update_wtax_year_end`
--

CREATE TABLE `update_wtax_year_end` (
  `emp_id` varchar(100) NOT NULL DEFAULT 'none',
  `total_wtax` decimal(65,2) NOT NULL DEFAULT 0.00
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_general_ci;

-- --------------------------------------------------------

--
-- Table structure for table `user_account`
--

CREATE TABLE `user_account` (
  `user_id` int(10) NOT NULL,
  `type_id` int(11) NOT NULL,
  `emp_name` varchar(100) NOT NULL,
  `emp_id` varchar(100) NOT NULL,
  `username` varchar(100) NOT NULL,
  `password` varchar(100) NOT NULL,
  `acc_stat` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1 COLLATE=latin1_swedish_ci;

--
-- Dumping data for table `user_account`
--

INSERT INTO `user_account` (`user_id`, `type_id`, `emp_name`, `emp_id`, `username`, `password`, `acc_stat`) VALUES
(1, 1, 'admin1', '000-000-001', 'admin1', 'admin1', 0),
(2, 2, 'user2', '000-000-002', 'user2', 'user2', 0),
(3, 3, 'user2', '000-000-003', 'user2', 'user2', 0);

-- --------------------------------------------------------

--
-- Table structure for table `user_type`
--

CREATE TABLE `user_type` (
  `type_id` int(10) NOT NULL,
  `user_type` varchar(100) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_general_ci;

--
-- Dumping data for table `user_type`
--

INSERT INTO `user_type` (`type_id`, `user_type`) VALUES
(1, 'Administrator'),
(2, 'PayMaster(Main User)'),
(3, 'Alternate User');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `add_overtime`
--
ALTER TABLE `add_overtime`
  ADD PRIMARY KEY (`ao_id`);

--
-- Indexes for table `audit_logs`
--
ALTER TABLE `audit_logs`
  ADD PRIMARY KEY (`logs_id`);

--
-- Indexes for table `bank_remittance`
--
ALTER TABLE `bank_remittance`
  ADD PRIMARY KEY (`bank_id`);

--
-- Indexes for table `bonus`
--
ALTER TABLE `bonus`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `bonus_details`
--
ALTER TABLE `bonus_details`
  ADD PRIMARY KEY (`bonus_id`);

--
-- Indexes for table `department`
--
ALTER TABLE `department`
  ADD PRIMARY KEY (`dep_id`);

--
-- Indexes for table `leave`
--
ALTER TABLE `leave`
  ADD PRIMARY KEY (`leave_id`);

--
-- Indexes for table `leave_balance`
--
ALTER TABLE `leave_balance`
  ADD PRIMARY KEY (`leave_balance_id`),
  ADD KEY `emp_no` (`emp_no`);

--
-- Indexes for table `loan`
--
ALTER TABLE `loan`
  ADD PRIMARY KEY (`loan_id`),
  ADD KEY `emp_no` (`emp_no`);

--
-- Indexes for table `monthly_tax`
--
ALTER TABLE `monthly_tax`
  ADD PRIMARY KEY (`mt_id`);

--
-- Indexes for table `others_id`
--
ALTER TABLE `others_id`
  ADD PRIMARY KEY (`other_id`);

--
-- Indexes for table `overtime`
--
ALTER TABLE `overtime`
  ADD PRIMARY KEY (`ot_id`);

--
-- Indexes for table `pag_ibig`
--
ALTER TABLE `pag_ibig`
  ADD PRIMARY KEY (`no`);

--
-- Indexes for table `payslip`
--
ALTER TABLE `payslip`
  ADD PRIMARY KEY (`pay_id`);

--
-- Indexes for table `pay_details`
--
ALTER TABLE `pay_details`
  ADD PRIMARY KEY (`pd_id`);

--
-- Indexes for table `pay_trans`
--
ALTER TABLE `pay_trans`
  ADD PRIMARY KEY (`pay_trans_id`);

--
-- Indexes for table `philhealth`
--
ALTER TABLE `philhealth`
  ADD PRIMARY KEY (`no`);

--
-- Indexes for table `rec_tax`
--
ALTER TABLE `rec_tax`
  ADD PRIMARY KEY (`rec_tax_id`);

--
-- Indexes for table `salary`
--
ALTER TABLE `salary`
  ADD PRIMARY KEY (`salary_id`);

--
-- Indexes for table `sss`
--
ALTER TABLE `sss`
  ADD PRIMARY KEY (`no`);

--
-- Indexes for table `tax`
--
ALTER TABLE `tax`
  ADD PRIMARY KEY (`no`);

--
-- Indexes for table `tax_inc`
--
ALTER TABLE `tax_inc`
  ADD PRIMARY KEY (`tax_id`);

--
-- Indexes for table `user_account`
--
ALTER TABLE `user_account`
  ADD PRIMARY KEY (`user_id`);

--
-- Indexes for table `user_type`
--
ALTER TABLE `user_type`
  ADD PRIMARY KEY (`type_id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `add_overtime`
--
ALTER TABLE `add_overtime`
  MODIFY `ao_id` int(10) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=387;

--
-- AUTO_INCREMENT for table `audit_logs`
--
ALTER TABLE `audit_logs`
  MODIFY `logs_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=590;

--
-- AUTO_INCREMENT for table `bank_remittance`
--
ALTER TABLE `bank_remittance`
  MODIFY `bank_id` int(10) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3834;

--
-- AUTO_INCREMENT for table `bonus`
--
ALTER TABLE `bonus`
  MODIFY `id` int(10) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=949;

--
-- AUTO_INCREMENT for table `bonus_details`
--
ALTER TABLE `bonus_details`
  MODIFY `bonus_id` int(100) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=523;

--
-- AUTO_INCREMENT for table `department`
--
ALTER TABLE `department`
  MODIFY `dep_id` int(10) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=13;

--
-- AUTO_INCREMENT for table `leave`
--
ALTER TABLE `leave`
  MODIFY `leave_id` int(100) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=8;

--
-- AUTO_INCREMENT for table `leave_balance`
--
ALTER TABLE `leave_balance`
  MODIFY `leave_balance_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=152;

--
-- AUTO_INCREMENT for table `loan`
--
ALTER TABLE `loan`
  MODIFY `loan_id` int(10) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=147;

--
-- AUTO_INCREMENT for table `monthly_tax`
--
ALTER TABLE `monthly_tax`
  MODIFY `mt_id` int(10) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=66521;

--
-- AUTO_INCREMENT for table `others_id`
--
ALTER TABLE `others_id`
  MODIFY `other_id` int(10) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=141;

--
-- AUTO_INCREMENT for table `overtime`
--
ALTER TABLE `overtime`
  MODIFY `ot_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=15;

--
-- AUTO_INCREMENT for table `pag_ibig`
--
ALTER TABLE `pag_ibig`
  MODIFY `no` int(10) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `payslip`
--
ALTER TABLE `payslip`
  MODIFY `pay_id` int(10) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5348;

--
-- AUTO_INCREMENT for table `pay_details`
--
ALTER TABLE `pay_details`
  MODIFY `pd_id` int(10) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7455;

--
-- AUTO_INCREMENT for table `pay_trans`
--
ALTER TABLE `pay_trans`
  MODIFY `pay_trans_id` int(10) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6020;

--
-- AUTO_INCREMENT for table `philhealth`
--
ALTER TABLE `philhealth`
  MODIFY `no` int(10) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT for table `rec_tax`
--
ALTER TABLE `rec_tax`
  MODIFY `rec_tax_id` int(100) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=1131;

--
-- AUTO_INCREMENT for table `salary`
--
ALTER TABLE `salary`
  MODIFY `salary_id` int(10) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=145;

--
-- AUTO_INCREMENT for table `sss`
--
ALTER TABLE `sss`
  MODIFY `no` int(10) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=38;

--
-- AUTO_INCREMENT for table `tax`
--
ALTER TABLE `tax`
  MODIFY `no` int(10) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;

--
-- AUTO_INCREMENT for table `tax_inc`
--
ALTER TABLE `tax_inc`
  MODIFY `tax_id` int(100) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=603;

--
-- AUTO_INCREMENT for table `user_account`
--
ALTER TABLE `user_account`
  MODIFY `user_id` int(10) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT for table `user_type`
--
ALTER TABLE `user_type`
  MODIFY `type_id` int(10) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
