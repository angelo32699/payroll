﻿using CrystalDecisions.CrystalReports.Engine;
using CrystalDecisions.Windows.Forms;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Payroll
{
    public partial class BIR_Alphalist : Form
    {
        ReportDocument report = new ReportDocument();
        connect con = new connect();

        public BIR_Alphalist()
        {
            InitializeComponent();
        }

        private void BIR_Alphalist_Load(object sender, EventArgs e)
        {
            crystalReportViewer1.ToolPanelView = ToolPanelViewType.None;
            crystalReportViewer2.ToolPanelView = ToolPanelViewType.None;
        }

        private void tabPage1_Enter(object sender, EventArgs e)
        {
            report.Load(@"C:\Temp\Payroll\Payroll\Reports\BIR2316.rpt");
            DataSet ds = new DataSet();
            con.dataget("select * from pay_details");
            con.mda.Fill(ds, "pay_details");
            report.SetDataSource(ds);
            crystalReportViewer1.ReportSource = report;
        }

        private void tabPage2_Enter(object sender, EventArgs e)
        {
            report.Load(@"C:\Temp\Payroll\Payroll\Reports\Alphalist.rpt");
            DataSet ds = new DataSet();
            con.dataget("select * from pay_details");
            con.mda.Fill(ds, "pay_details");
            report.SetDataSource(ds);
            crystalReportViewer2.ReportSource = report;
        }
    }
}
