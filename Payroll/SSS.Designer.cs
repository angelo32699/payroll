﻿namespace Payroll
{
    partial class SSS
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(SSS));
            this.lv_SSS = new System.Windows.Forms.ListView();
            this.label13 = new System.Windows.Forms.Label();
            this.tb_search = new System.Windows.Forms.TextBox();
            this.label4 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.label1 = new System.Windows.Forms.Label();
            this.tb_employee_contribution = new System.Windows.Forms.TextBox();
            this.tb_employer_contribution = new System.Windows.Forms.TextBox();
            this.tb_maximum_amount = new System.Windows.Forms.TextBox();
            this.tb_minimum_amount = new System.Windows.Forms.TextBox();
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.btn_delete = new Payroll.RJButtons.RJButton();
            this.btn_Update = new Payroll.RJButtons.RJButton();
            this.btn_add = new Payroll.RJButtons.RJButton();
            this.label5 = new System.Windows.Forms.Label();
            this.btn_export = new Payroll.RJButtons.RJButton();
            this.groupBox1.SuspendLayout();
            this.SuspendLayout();
            // 
            // lv_SSS
            // 
            this.lv_SSS.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lv_SSS.FullRowSelect = true;
            this.lv_SSS.GridLines = true;
            this.lv_SSS.HideSelection = false;
            this.lv_SSS.Location = new System.Drawing.Point(4, 226);
            this.lv_SSS.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.lv_SSS.Name = "lv_SSS";
            this.lv_SSS.Size = new System.Drawing.Size(1012, 360);
            this.lv_SSS.TabIndex = 0;
            this.lv_SSS.UseCompatibleStateImageBehavior = false;
            this.lv_SSS.Click += new System.EventHandler(this.lv_SSS_Click);
            // 
            // label13
            // 
            this.label13.AutoSize = true;
            this.label13.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label13.Location = new System.Drawing.Point(644, 191);
            this.label13.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label13.Name = "label13";
            this.label13.Size = new System.Drawing.Size(72, 22);
            this.label13.TabIndex = 60;
            this.label13.Text = "Search:";
            // 
            // tb_search
            // 
            this.tb_search.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.tb_search.ForeColor = System.Drawing.Color.Black;
            this.tb_search.Location = new System.Drawing.Point(723, 186);
            this.tb_search.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.tb_search.Name = "tb_search";
            this.tb_search.Size = new System.Drawing.Size(294, 28);
            this.tb_search.TabIndex = 59;
            this.tb_search.TextChanged += new System.EventHandler(this.tb_search_TextChanged);
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Font = new System.Drawing.Font("Arial", 9.75F);
            this.label4.Location = new System.Drawing.Point(748, 24);
            this.label4.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(207, 23);
            this.label4.TabIndex = 71;
            this.label4.Text = "Employee Contribution";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Font = new System.Drawing.Font("Arial", 9.75F);
            this.label3.Location = new System.Drawing.Point(498, 23);
            this.label3.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(203, 23);
            this.label3.TabIndex = 70;
            this.label3.Text = "Employer Contribution";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Font = new System.Drawing.Font("Arial", 9.75F);
            this.label2.Location = new System.Drawing.Point(253, 23);
            this.label2.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(164, 23);
            this.label2.TabIndex = 69;
            this.label2.Text = "Maximum Amount";
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("Arial", 9.75F);
            this.label1.Location = new System.Drawing.Point(10, 27);
            this.label1.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(158, 23);
            this.label1.TabIndex = 68;
            this.label1.Text = "Minimum Amount";
            // 
            // tb_employee_contribution
            // 
            this.tb_employee_contribution.Enabled = false;
            this.tb_employee_contribution.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.tb_employee_contribution.ForeColor = System.Drawing.Color.Black;
            this.tb_employee_contribution.Location = new System.Drawing.Point(753, 56);
            this.tb_employee_contribution.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.tb_employee_contribution.Name = "tb_employee_contribution";
            this.tb_employee_contribution.Size = new System.Drawing.Size(228, 28);
            this.tb_employee_contribution.TabIndex = 67;
            this.tb_employee_contribution.Text = "0.00";
            this.tb_employee_contribution.TextChanged += new System.EventHandler(this.tb_employee_contribution_TextChanged);
            this.tb_employee_contribution.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.tb_employee_contribution_KeyPress);
            // 
            // tb_employer_contribution
            // 
            this.tb_employer_contribution.Enabled = false;
            this.tb_employer_contribution.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.tb_employer_contribution.ForeColor = System.Drawing.Color.Black;
            this.tb_employer_contribution.Location = new System.Drawing.Point(502, 55);
            this.tb_employer_contribution.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.tb_employer_contribution.Name = "tb_employer_contribution";
            this.tb_employer_contribution.Size = new System.Drawing.Size(228, 28);
            this.tb_employer_contribution.TabIndex = 66;
            this.tb_employer_contribution.Text = "0.00";
            this.tb_employer_contribution.TextChanged += new System.EventHandler(this.tb_employer_contribution_TextChanged);
            this.tb_employer_contribution.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.tb_employer_contribution_KeyPress);
            // 
            // tb_maximum_amount
            // 
            this.tb_maximum_amount.Enabled = false;
            this.tb_maximum_amount.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.tb_maximum_amount.ForeColor = System.Drawing.Color.Black;
            this.tb_maximum_amount.Location = new System.Drawing.Point(258, 57);
            this.tb_maximum_amount.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.tb_maximum_amount.Name = "tb_maximum_amount";
            this.tb_maximum_amount.Size = new System.Drawing.Size(228, 28);
            this.tb_maximum_amount.TabIndex = 65;
            this.tb_maximum_amount.Text = "0.00";
            this.tb_maximum_amount.TextChanged += new System.EventHandler(this.tb_maximum_amount_TextChanged);
            this.tb_maximum_amount.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.tb_maximum_amount_KeyPress);
            // 
            // tb_minimum_amount
            // 
            this.tb_minimum_amount.Enabled = false;
            this.tb_minimum_amount.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.tb_minimum_amount.ForeColor = System.Drawing.Color.Black;
            this.tb_minimum_amount.Location = new System.Drawing.Point(15, 57);
            this.tb_minimum_amount.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.tb_minimum_amount.Name = "tb_minimum_amount";
            this.tb_minimum_amount.Size = new System.Drawing.Size(228, 28);
            this.tb_minimum_amount.TabIndex = 64;
            this.tb_minimum_amount.Text = "0.00";
            this.tb_minimum_amount.TextChanged += new System.EventHandler(this.tb_minimum_amount_TextChanged);
            this.tb_minimum_amount.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.tb_minimum_amount_KeyPress);
            // 
            // groupBox1
            // 
            this.groupBox1.Controls.Add(this.btn_delete);
            this.groupBox1.Controls.Add(this.btn_Update);
            this.groupBox1.Controls.Add(this.btn_add);
            this.groupBox1.Controls.Add(this.label2);
            this.groupBox1.Controls.Add(this.label4);
            this.groupBox1.Controls.Add(this.tb_employer_contribution);
            this.groupBox1.Controls.Add(this.label3);
            this.groupBox1.Controls.Add(this.tb_minimum_amount);
            this.groupBox1.Controls.Add(this.label1);
            this.groupBox1.Controls.Add(this.tb_employee_contribution);
            this.groupBox1.Controls.Add(this.tb_maximum_amount);
            this.groupBox1.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.groupBox1.Location = new System.Drawing.Point(6, 11);
            this.groupBox1.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Padding = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.groupBox1.Size = new System.Drawing.Size(1010, 154);
            this.groupBox1.TabIndex = 72;
            this.groupBox1.TabStop = false;
            this.groupBox1.Text = "SSS Table";
            // 
            // btn_delete
            // 
            this.btn_delete.BackColor = System.Drawing.Color.Transparent;
            this.btn_delete.BackgroundColor = System.Drawing.Color.Transparent;
            this.btn_delete.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(192)))), ((int)(((byte)(0)))), ((int)(((byte)(0)))));
            this.btn_delete.BorderRadius = 8;
            this.btn_delete.BorderSize = 1;
            this.btn_delete.Enabled = false;
            this.btn_delete.FlatAppearance.BorderSize = 0;
            this.btn_delete.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btn_delete.Font = new System.Drawing.Font("Microsoft Sans Serif", 8F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btn_delete.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(192)))), ((int)(((byte)(0)))), ((int)(((byte)(0)))));
            this.btn_delete.Image = global::Payroll.Properties.Resources.icons8_delete_30;
            this.btn_delete.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.btn_delete.Location = new System.Drawing.Point(607, 98);
            this.btn_delete.Name = "btn_delete";
            this.btn_delete.Size = new System.Drawing.Size(175, 50);
            this.btn_delete.TabIndex = 191;
            this.btn_delete.Text = "DELETE";
            this.btn_delete.TextColor = System.Drawing.Color.FromArgb(((int)(((byte)(192)))), ((int)(((byte)(0)))), ((int)(((byte)(0)))));
            this.btn_delete.UseVisualStyleBackColor = false;
            this.btn_delete.Click += new System.EventHandler(this.btn_delete_Click);
            this.btn_delete.MouseEnter += new System.EventHandler(this.btn_delete_MouseEnter);
            this.btn_delete.MouseLeave += new System.EventHandler(this.btn_delete_MouseLeave);
            this.btn_delete.MouseHover += new System.EventHandler(this.btn_delete_MouseHover);
            // 
            // btn_Update
            // 
            this.btn_Update.BackColor = System.Drawing.Color.Transparent;
            this.btn_Update.BackgroundColor = System.Drawing.Color.Transparent;
            this.btn_Update.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(192)))), ((int)(((byte)(0)))), ((int)(((byte)(0)))));
            this.btn_Update.BorderRadius = 8;
            this.btn_Update.BorderSize = 1;
            this.btn_Update.Enabled = false;
            this.btn_Update.FlatAppearance.BorderSize = 0;
            this.btn_Update.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btn_Update.Font = new System.Drawing.Font("Microsoft Sans Serif", 8F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btn_Update.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(192)))), ((int)(((byte)(0)))), ((int)(((byte)(0)))));
            this.btn_Update.Image = global::Payroll.Properties.Resources.Button_UPDATE_Red__4___1___1_;
            this.btn_Update.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.btn_Update.Location = new System.Drawing.Point(425, 98);
            this.btn_Update.Name = "btn_Update";
            this.btn_Update.Size = new System.Drawing.Size(175, 50);
            this.btn_Update.TabIndex = 189;
            this.btn_Update.Text = "EDIT";
            this.btn_Update.TextColor = System.Drawing.Color.FromArgb(((int)(((byte)(192)))), ((int)(((byte)(0)))), ((int)(((byte)(0)))));
            this.btn_Update.UseVisualStyleBackColor = false;
            this.btn_Update.Click += new System.EventHandler(this.btn_Update_Click);
            this.btn_Update.MouseEnter += new System.EventHandler(this.btn_Update_MouseEnter);
            this.btn_Update.MouseLeave += new System.EventHandler(this.btn_Update_MouseLeave);
            this.btn_Update.MouseHover += new System.EventHandler(this.btn_Update_MouseHover);
            // 
            // btn_add
            // 
            this.btn_add.BackColor = System.Drawing.Color.Transparent;
            this.btn_add.BackgroundColor = System.Drawing.Color.Transparent;
            this.btn_add.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(192)))), ((int)(((byte)(0)))), ((int)(((byte)(0)))));
            this.btn_add.BorderRadius = 8;
            this.btn_add.BorderSize = 1;
            this.btn_add.FlatAppearance.BorderSize = 0;
            this.btn_add.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btn_add.Font = new System.Drawing.Font("Microsoft Sans Serif", 8F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btn_add.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(192)))), ((int)(((byte)(0)))), ((int)(((byte)(0)))));
            this.btn_add.Image = global::Payroll.Properties.Resources.Button_ADD_Red__1___1_;
            this.btn_add.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.btn_add.Location = new System.Drawing.Point(228, 98);
            this.btn_add.Name = "btn_add";
            this.btn_add.Size = new System.Drawing.Size(175, 50);
            this.btn_add.TabIndex = 188;
            this.btn_add.Text = "ADD";
            this.btn_add.TextColor = System.Drawing.Color.FromArgb(((int)(((byte)(192)))), ((int)(((byte)(0)))), ((int)(((byte)(0)))));
            this.btn_add.UseVisualStyleBackColor = false;
            this.btn_add.Click += new System.EventHandler(this.btn_add_Click);
            this.btn_add.MouseEnter += new System.EventHandler(this.btn_add_MouseEnter);
            this.btn_add.MouseLeave += new System.EventHandler(this.btn_add_MouseLeave);
            this.btn_add.MouseHover += new System.EventHandler(this.btn_add_MouseHover);
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label5.Location = new System.Drawing.Point(7, 170);
            this.label5.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(18, 20);
            this.label5.TabIndex = 82;
            this.label5.Text = "0";
            this.label5.Visible = false;
            // 
            // btn_export
            // 
            this.btn_export.BackColor = System.Drawing.Color.Transparent;
            this.btn_export.BackgroundColor = System.Drawing.Color.Transparent;
            this.btn_export.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(192)))), ((int)(((byte)(0)))), ((int)(((byte)(0)))));
            this.btn_export.BorderRadius = 8;
            this.btn_export.BorderSize = 1;
            this.btn_export.FlatAppearance.BorderSize = 0;
            this.btn_export.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btn_export.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btn_export.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(192)))), ((int)(((byte)(0)))), ((int)(((byte)(0)))));
            this.btn_export.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.btn_export.Location = new System.Drawing.Point(798, 590);
            this.btn_export.Name = "btn_export";
            this.btn_export.Size = new System.Drawing.Size(219, 44);
            this.btn_export.TabIndex = 186;
            this.btn_export.Text = "Export to Excel";
            this.btn_export.TextColor = System.Drawing.Color.FromArgb(((int)(((byte)(192)))), ((int)(((byte)(0)))), ((int)(((byte)(0)))));
            this.btn_export.UseVisualStyleBackColor = false;
            this.btn_export.Click += new System.EventHandler(this.btn_export_Click);
            this.btn_export.MouseEnter += new System.EventHandler(this.btn_export_MouseEnter);
            this.btn_export.MouseLeave += new System.EventHandler(this.btn_export_MouseLeave);
            this.btn_export.MouseHover += new System.EventHandler(this.btn_export_MouseHover);
            // 
            // SSS
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(9F, 20F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.AutoSize = true;
            this.ClientSize = new System.Drawing.Size(1024, 639);
            this.Controls.Add(this.btn_export);
            this.Controls.Add(this.label5);
            this.Controls.Add(this.label13);
            this.Controls.Add(this.tb_search);
            this.Controls.Add(this.lv_SSS);
            this.Controls.Add(this.groupBox1);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedToolWindow;
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.MaximizeBox = false;
            this.Name = "SSS";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "SSS Table";
            this.Load += new System.EventHandler(this.SSS_Load);
            this.groupBox1.ResumeLayout(false);
            this.groupBox1.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.ListView lv_SSS;
        private System.Windows.Forms.Label label13;
        private System.Windows.Forms.TextBox tb_search;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label1;
        internal System.Windows.Forms.TextBox tb_employee_contribution;
        internal System.Windows.Forms.TextBox tb_employer_contribution;
        internal System.Windows.Forms.TextBox tb_maximum_amount;
        internal System.Windows.Forms.TextBox tb_minimum_amount;
        private System.Windows.Forms.GroupBox groupBox1;
        private System.Windows.Forms.Label label5;
        private RJButtons.RJButton btn_add;
        private RJButtons.RJButton btn_Update;
        private RJButtons.RJButton btn_delete;
        private RJButtons.RJButton btn_export;
    }
}