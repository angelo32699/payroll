﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using MySql.Data.MySqlClient;
using System.Configuration;

namespace Payroll
{
    public partial class Payroll_Details : Form
    {
        MySqlCommand cmd;
        MySqlDataAdapter da;
        DataTable dt;

        public Payroll_Details()
        {
            InitializeComponent();
        }

        private void Payroll_Details_Load(object sender, EventArgs e)
        {
           
            LoadMonthdetails();
            LoadMonthperiod();
            view_date_period();

            display_payroll_Details("Select * from pay_details where yyyymm = '"+comboBox1.Text+"' and  date_period = '"+comboBox2.Text+ "' and year_end = 'False'");

        }
        public void display_payroll_Details(string sql1)
        {

            string myconnection = ConfigurationManager.ConnectionStrings["MyConstring"].ConnectionString;
            MySqlConnection connection = new MySqlConnection(myconnection);

            try
            {
                connection.Open();
                cmd = new MySqlCommand();
                cmd.Connection = connection;
                cmd.CommandText = sql1;
                da = new MySqlDataAdapter();
                da.SelectCommand = cmd;
                dt = new DataTable();
                da.Fill(dt);
                lv_payroll_details.Items.Clear();

                for (int i = 0; i < dt.Rows.Count; i++)
                {
                    DataRow dr = dt.Rows[i];
                    ListViewItem listitem = new ListViewItem(dr["pd_id"].ToString());
                  
                    listitem.SubItems.Add(dr["yyyymm"].ToString());
                    listitem.SubItems.Add(dr["date_period"].ToString());
                    listitem.SubItems.Add(dr["emp_id"].ToString());
                    listitem.SubItems.Add(dr["fullname"].ToString());
                    listitem.SubItems.Add(dr["department"].ToString());
                    listitem.SubItems.Add(dr["emp_status"].ToString());
                    listitem.SubItems.Add(dr["absences"].ToString());
                   
                    listitem.SubItems.Add(string.Format("{0:#,##0.00}", Convert.ToDecimal(dr["month_pay"])));
                    listitem.SubItems.Add(string.Format("{0:#,##0.00}", Convert.ToDecimal(dr["regular_pay"])));
                    listitem.SubItems.Add(string.Format("{0:#,##0.00}", Convert.ToDecimal(dr["daily_rate"])));
                    listitem.SubItems.Add(string.Format("{0:#,##0.00}", Convert.ToDecimal(dr["tax_sub"])));
                    listitem.SubItems.Add(string.Format("{0:#,##0.00}", Convert.ToDecimal(dr["others_pay_det"])));
                    listitem.SubItems.Add(string.Format("{0:#,##0.00}", Convert.ToDecimal(dr["deductions"])));
                    listitem.SubItems.Add(string.Format("{0:#,##0.00}", Convert.ToDecimal(dr["overtime_pay_det"])));
                    listitem.SubItems.Add(string.Format("{0:#,##0.00}", Convert.ToDecimal(dr["gross_inc"])));
                    listitem.SubItems.Add(string.Format("{0:#,##0.00}", Convert.ToDecimal(dr["gross_ded"])));
                    listitem.SubItems.Add(string.Format("{0:#,##0.00}", Convert.ToDecimal(dr["net_pay"])));
                    listitem.SubItems.Add(string.Format("{0:#,##0.00}", Convert.ToDecimal(dr["wtax"])));
                    listitem.SubItems.Add(string.Format("{0:#,##0.00}", Convert.ToDecimal(dr["other_ded_det"])));
                    listitem.SubItems.Add(string.Format("{0:#,##0.00}", Convert.ToDecimal(dr["sss_ee"])));
                    listitem.SubItems.Add(string.Format("{0:#,##0.00}", Convert.ToDecimal(dr["sss_er"])));
                    listitem.SubItems.Add(string.Format("{0:#,##0.00}", Convert.ToDecimal(dr["pag_ibig_ee"])));
                    listitem.SubItems.Add(string.Format("{0:#,##0.00}", Convert.ToDecimal(dr["pag_ibig_er"])));
                    listitem.SubItems.Add(string.Format("{0:#,##0.00}", Convert.ToDecimal(dr["philhealth_ee"])));
                    listitem.SubItems.Add(string.Format("{0:#,##0.00}", Convert.ToDecimal(dr["philhealth_er"])));
                    listitem.SubItems.Add(string.Format("{0:#,##0.00}", Convert.ToDecimal(dr["company_loan"])));
                    listitem.SubItems.Add(string.Format("{0:#,##0.00}", Convert.ToDecimal(dr["pag_ibig_loan"])));
                    listitem.SubItems.Add(string.Format("{0:#,##0.00}", Convert.ToDecimal(dr["sss_loan"])));
                    listitem.SubItems.Add(string.Format("{0:#,##0.00}", Convert.ToDecimal(dr["comp_bal"])));
                    listitem.SubItems.Add(string.Format("{0:#,##0.00}", Convert.ToDecimal(dr["sss_bal"])));
                    listitem.SubItems.Add(string.Format("{0:#,##0.00}", Convert.ToDecimal(dr["pag_ibig_bal"])));
                    lv_payroll_details.Items.Add(listitem);


                }

            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
            finally
            {
                connection.Close();
            }

            lv_payroll_details.View = View.Details;

            lv_payroll_details.Columns.Add(" No ", 0, HorizontalAlignment.Center);
            lv_payroll_details.Columns.Add(" MMYYYY ", 165, HorizontalAlignment.Center);
            lv_payroll_details.Columns.Add(" Payroll Period ", 165, HorizontalAlignment.Center);
            lv_payroll_details.Columns.Add(" Employee ID ", 140, HorizontalAlignment.Center);
            lv_payroll_details.Columns.Add(" Fullname ", 140, HorizontalAlignment.Center);
            lv_payroll_details.Columns.Add(" Department ", 165, HorizontalAlignment.Center);
            lv_payroll_details.Columns.Add(" Emp Status ", 165, HorizontalAlignment.Center);
            lv_payroll_details.Columns.Add(" Absences ", 165, HorizontalAlignment.Center);
            lv_payroll_details.Columns.Add(" Monthly Pay ", 165, HorizontalAlignment.Center);
            lv_payroll_details.Columns.Add(" Regular Pay ", 165, HorizontalAlignment.Center);
            lv_payroll_details.Columns.Add(" Daily Rate ", 165, HorizontalAlignment.Center);
            lv_payroll_details.Columns.Add(" Tax Subsidy ", 165, HorizontalAlignment.Center);
            lv_payroll_details.Columns.Add(" Other Inc ", 165, HorizontalAlignment.Center);
            lv_payroll_details.Columns.Add(" ADJ / Refund ", 165, HorizontalAlignment.Center);
            lv_payroll_details.Columns.Add(" Overtime Pay ", 165, HorizontalAlignment.Center);
            lv_payroll_details.Columns.Add(" Gross Pay ", 165, HorizontalAlignment.Center);
            lv_payroll_details.Columns.Add(" Gross Ded ", 165, HorizontalAlignment.Center);
            lv_payroll_details.Columns.Add(" Net Pay ", 165, HorizontalAlignment.Center);
            lv_payroll_details.Columns.Add(" Wtax ", 165, HorizontalAlignment.Center);
            lv_payroll_details.Columns.Add(" Other Ded ", 165, HorizontalAlignment.Center);
            lv_payroll_details.Columns.Add(" SSS EE ", 165, HorizontalAlignment.Center);
            lv_payroll_details.Columns.Add(" SSS ER ", 165, HorizontalAlignment.Center);
            lv_payroll_details.Columns.Add(" Pag-Ibig EE ", 165, HorizontalAlignment.Center);
            lv_payroll_details.Columns.Add(" Pag-Ibig ER ", 165, HorizontalAlignment.Center);
            lv_payroll_details.Columns.Add(" Philhealth EE ", 165, HorizontalAlignment.Center);
            lv_payroll_details.Columns.Add(" Philhealth ER ", 165, HorizontalAlignment.Center);
            lv_payroll_details.Columns.Add(" Company loan ", 165, HorizontalAlignment.Center);
            lv_payroll_details.Columns.Add(" Pag-Ibig Loan ", 165, HorizontalAlignment.Center);
            lv_payroll_details.Columns.Add(" SSS Loan ", 165, HorizontalAlignment.Center);
            lv_payroll_details.Columns.Add(" Comp Bal ", 165, HorizontalAlignment.Center);
            lv_payroll_details.Columns.Add(" SSS Bal ", 165, HorizontalAlignment.Center);
            lv_payroll_details.Columns.Add(" Pag-Ibig Bal ", 165, HorizontalAlignment.Center);
        }

        private void btn_print_Click(object sender, EventArgs e)
        {
            ToExcel();
        }
        private void LoadMonthdetails()
        {


            string myconnection = ConfigurationManager.ConnectionStrings["MyConstring"].ConnectionString;
            MySqlConnection connection = new MySqlConnection(myconnection);

            try
            {
                connection.Open();
                cmd = new MySqlCommand();
                cmd.Connection = connection;
                cmd.CommandText = "Select yyyymm FROM pay_details  where year_end = 'False'  group by yyyymm";
                da = new MySqlDataAdapter();
                da.SelectCommand = cmd;
                dt = new DataTable();
                da.Fill(dt);

                for (int i = 0; i < dt.Rows.Count; i++)
                {
                    DataRow dr = dt.Rows[i];



                    comboBox1.Items.Add((dr["yyyymm"].ToString()));

                }

            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
            finally
            {
                connection.Close();
            }
        }

            private void LoadMonthperiod()
            {


                string myconnection = ConfigurationManager.ConnectionStrings["MyConstring"].ConnectionString;
                MySqlConnection connection = new MySqlConnection(myconnection);

                try
                {
                    connection.Open();
                    cmd = new MySqlCommand();
                    cmd.Connection = connection;
                    cmd.CommandText = "Select date_period FROM pay_details  where year_end = 'False' group by date_period";
                    da = new MySqlDataAdapter();
                    da.SelectCommand = cmd;
                    dt = new DataTable();
                    da.Fill(dt);

                    for (int i = 0; i < dt.Rows.Count; i++)
                    {
                        DataRow dr = dt.Rows[i];



                        comboBox2.Items.Add((dr["date_period"].ToString()));

                    }

                }
                catch (Exception ex)
                {
                    MessageBox.Show(ex.Message);
                }
                finally
                {
                    connection.Close();
                }

            }


        private void ToExcel()
        {
            StringBuilder sb = new StringBuilder();
            Microsoft.Office.Interop.Excel.Application app = new Microsoft.Office.Interop.Excel.Application();
            app.Visible = true;
            Microsoft.Office.Interop.Excel.Workbook wb = app.Workbooks.Add(1);
            Microsoft.Office.Interop.Excel.Worksheet ws = (Microsoft.Office.Interop.Excel.Worksheet)wb.Worksheets[1];
            int i = 1;
            int i2 = 2;
            int x = 1;
            int x2 = 1;
            foreach (ColumnHeader ch in lv_payroll_details.Columns)
            {
                ws.Cells[x2, x] = ch.Text;
                x++;
            }

            foreach (ListViewItem lvi in lv_payroll_details.Items)
            {
                i = 1;
                foreach (ListViewItem.ListViewSubItem lvs in lvi.SubItems)
                {
                    ws.Cells[i2, i] = lvs.Text;
                    ws.Cells.Select();
                    ws.Cells.EntireColumn.AutoFit();
                    i++;
                }
                i2++;
            }
        }

        public void view_date_period()
        {

            string myconnection = ConfigurationManager.ConnectionStrings["MyConstring"].ConnectionString;
            MySqlConnection connection = new MySqlConnection(myconnection);

            try
            {
                connection.Open();
                cmd = new MySqlCommand();
                cmd.Connection = connection;
                cmd.CommandText = "Select * FROM pay_details where year_end = 'False'";
                da = new MySqlDataAdapter();
                da.SelectCommand = cmd;
                dt = new DataTable();
                da.Fill(dt);

                for (int i = 0; i < dt.Rows.Count; i++)
                {
                    DataRow dr = dt.Rows[i];


            

                    comboBox1.SelectedItem = (dr["yyyymm"].ToString());
                    comboBox2.SelectedItem = (dr["date_period"].ToString());





                }

            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
            finally
            {
                connection.Close();
            }
        }

        private void tb_search_TextChanged(object sender, EventArgs e)
        {
            lv_payroll_details.Columns.Clear();
            display_payroll_Details("Select DISTINCT * FROM pay_details WHERE year_end = 'False' and emp_id like '" + tb_search.Text + "%' or fullname like '" + tb_search.Text + "%' or yyyymm like  '" + tb_search.Text + "%'  or   date_period like  '" + tb_search.Text + "%' or department like '" + tb_search.Text + "%' or emp_status like '"+tb_search.Text+"%' or absences like  '"+tb_search.Text+ "%' or month_pay like  '" + tb_search.Text + "%' or regular_pay like  '" + tb_search.Text + "%' or daily_rate like  '" + tb_search.Text + "%' or tax_sub like  '" + tb_search.Text + "%' or others_pay_det like '" + tb_search.Text + "%' or deductions like '" + tb_search.Text + "%' or overtime_pay_det like  '" + tb_search.Text + "%' or gross_inc like  '" + tb_search.Text + "%' or gross_ded like  '" + tb_search.Text + "%' or net_pay like  '" + tb_search.Text + "%' or wtax like  '" + tb_search.Text + "%' or other_ded_det like  '" + tb_search.Text + "%' or sss_ee like '" + tb_search.Text + "%' or sss_er like  '" + tb_search.Text + "%' or philhealth_ee like  '" + tb_search.Text + "%' or philhealth_er like '" + tb_search.Text + "%' or pag_ibig_ee like '" + tb_search.Text + "%' or pag_ibig_er like '" + tb_search.Text + "%' or company_loan like '" + tb_search.Text + "%' or pag_ibig_loan like '" + tb_search.Text + "%' or sss_loan like '" + tb_search.Text + "%' or comp_bal like  '" + tb_search.Text + "%' or sss_bal like  '" + tb_search.Text + "%' or pag_ibig_bal like  '" + tb_search.Text + "%'");
        }

        private void comboBox1_SelectionChangeCommitted(object sender, EventArgs e)
        {
            lv_payroll_details.Columns.Clear();
            display_payroll_Details("Select * from pay_details where yyyymm = '" + comboBox1.Text + "' and year_end = 'False'");
        }

        private void comboBox2_SelectionChangeCommitted(object sender, EventArgs e)
        {
            lv_payroll_details.Columns.Clear();
            display_payroll_Details("Select * from pay_details where yyyymm = '" + comboBox1.Text + "' and  date_period = '" + comboBox2.Text + "' and year_end = 'False'");
        }

        private void btn_print_report_Click(object sender, EventArgs e)
        {
            Payroll_Details_Report payroll_Details_Report = new Payroll_Details_Report();
            payroll_Details_Report.ShowDialog();

        }

        private void btn_year_end_Click(object sender, EventArgs e)
        {

            string Year = DateTime.Parse(DateTime.Now.ToString()).Year.ToString();

            DialogResult result = MessageBox.Show("Are you sure you want to Year End "+Year+"?", "Year End" , MessageBoxButtons.YesNoCancel, MessageBoxIcon.Question);
            if (result == DialogResult.Yes)
            {

                string myconnection = ConfigurationManager.ConnectionStrings["MyConstring"].ConnectionString;
                MySqlConnection connection = new MySqlConnection(myconnection);

                try
                {
                    connection.Open();
                    cmd = new MySqlCommand();
                    cmd.Connection = connection;
                    cmd.CommandText = "UPDATE pay_details SET year_end  = 'True'";

                    da = new MySqlDataAdapter();
                    da.SelectCommand = cmd;
                    dt = new DataTable();
                    da.Fill(dt);
                }
                catch (Exception ex)
                {
                    MessageBox.Show(ex.Message);
                }
                finally
                {
                    connection.Close();
                }


                
                string myconnection2 = ConfigurationManager.ConnectionStrings["MyConstring"].ConnectionString;
                MySqlConnection connection2 = new MySqlConnection(myconnection2);

                try
                {
                    connection2.Open();
                    cmd = new MySqlCommand();
                    cmd.Connection = connection;
                    cmd.CommandText = "UPDATE bonus_details SET year_end  = 'True'";

                    da = new MySqlDataAdapter();
                    da.SelectCommand = cmd;
                    dt = new DataTable();
                    da.Fill(dt);
                }
                catch (Exception ex)
                {
                    MessageBox.Show(ex.Message);
                }
                finally
                {
                    connection2.Close();
                }

                display_payroll_Details("Select * from pay_details where yyyymm = '" + comboBox1.Text + "' and  date_period = '" + comboBox2.Text + "' and year_end = 'False'");
                           
                MessageBox.Show("Successfully Year End", "Year End", MessageBoxButtons.OK, MessageBoxIcon.Information);

            }
        }
    }
}

