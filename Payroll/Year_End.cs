﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Payroll
{
    public partial class Year_End : Form
    {
        public Year_End()
        {
            InitializeComponent();
        }

        private void Year_End_Load(object sender, EventArgs e)
        {
            display_year_end();
        }

        public void display_year_end()
        {

            lv_payroll_details.View = View.Details;

            lv_payroll_details.Columns.Add(" No ", 0, HorizontalAlignment.Center);
            lv_payroll_details.Columns.Add(" Employee ID ", 140, HorizontalAlignment.Center);
            lv_payroll_details.Columns.Add(" Fullname ", 140, HorizontalAlignment.Center);
            lv_payroll_details.Columns.Add(" Department ", 165, HorizontalAlignment.Center);
            lv_payroll_details.Columns.Add(" Annual Gross Pay ", 165, HorizontalAlignment.Center);
            lv_payroll_details.Columns.Add(" Annual SSS EE ", 165, HorizontalAlignment.Center);
            lv_payroll_details.Columns.Add(" Annual Philheath EE ", 165, HorizontalAlignment.Center);
            lv_payroll_details.Columns.Add(" Annual Pag-Ibig EE ", 165, HorizontalAlignment.Center);
            lv_payroll_details.Columns.Add(" Non Tax ", 165, HorizontalAlignment.Center);
            lv_payroll_details.Columns.Add(" Total Gross Ded ", 165, HorizontalAlignment.Center);
            lv_payroll_details.Columns.Add(" Total Tax Inc ", 165, HorizontalAlignment.Center);
            lv_payroll_details.Columns.Add(" Paid Annual Tax ", 165, HorizontalAlignment.Center);
            lv_payroll_details.Columns.Add(" Variance ", 165, HorizontalAlignment.Center);

        }

        private void btn_print_Click(object sender, EventArgs e)
        {
            ToExcel();
        }

        private void ToExcel()
        {
            StringBuilder sb = new StringBuilder();
            Microsoft.Office.Interop.Excel.Application app = new Microsoft.Office.Interop.Excel.Application();
            app.Visible = true;
            Microsoft.Office.Interop.Excel.Workbook wb = app.Workbooks.Add(1);
            Microsoft.Office.Interop.Excel.Worksheet ws = (Microsoft.Office.Interop.Excel.Worksheet)wb.Worksheets[1];
            int i = 1;
            int i2 = 2;
            int x = 1;
            int x2 = 1;
            foreach (ColumnHeader ch in lv_payroll_details.Columns)
            {
                ws.Cells[x2, x] = ch.Text;
                x++;
            }

            foreach (ListViewItem lvi in lv_payroll_details.Items)
            {
                i = 1;
                foreach (ListViewItem.ListViewSubItem lvs in lvi.SubItems)
                {
                    ws.Cells[i2, i] = lvs.Text;
                    ws.Cells.Select();
                    ws.Cells.EntireColumn.AutoFit();
                    i++;
                }
                i2++;
            }
        }
    }
    
}
