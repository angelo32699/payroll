﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using MySql.Data.MySqlClient;
using System.Configuration;
using System.Data.Common;

namespace Payroll
{
    public partial class Bonus_Transaction : Form
    {

        MySqlCommand cmd;
        MySqlDataAdapter da;
        DataTable dt;
        string myconnection = ConfigurationManager.ConnectionStrings["MyConstring"].ConnectionString;
        MySqlConnection connection;

        public Bonus_Transaction()
        {
            InitializeComponent();
            connection = new MySqlConnection(myconnection);
        }

        private void Bonus_Transaction_Load(object sender, EventArgs e)
        {

            LoadBonus();
            display_bonus_Details("Select * from bonus_details left join employee on bonus_details.emp_no = employee.emp_no left join salary on bonus_details.salary_id = salary.salary_id where bonus_details.year_end = 'False'");

        }

        public void display_bonus_Details(string sql1)
        {
      
            try
            {
                connection.Open();
                cmd = new MySqlCommand();
                cmd.Connection = connection;
                cmd.CommandText = sql1;
                da = new MySqlDataAdapter();
                da.SelectCommand = cmd;
                dt = new DataTable();
                da.Fill(dt);
                lv_bonus_details.Items.Clear();

                for (int i = 0; i < dt.Rows.Count; i++)
                {
                    DataRow dr = dt.Rows[i];
                    ListViewItem listitem = new ListViewItem(dr["bonus_id"].ToString());

                    listitem.SubItems.Add(dr["bonus"].ToString());
                    listitem.SubItems.Add(dr["yyyymm"].ToString());
                    listitem.SubItems.Add(dr["date_period"].ToString());
                    listitem.SubItems.Add(dr["emp_id"].ToString());
                    listitem.SubItems.Add(dr["lname"].ToString());
                    listitem.SubItems.Add(dr["fname"].ToString());
                    listitem.SubItems.Add(dr["mname"].ToString());
                    listitem.SubItems.Add(dr["department"].ToString());
                    listitem.SubItems.Add(string.Format("{0:#,##0.00}", Convert.ToDecimal(dr["salary"])));
                    listitem.SubItems.Add(dr["date_hired"].ToString());
                    listitem.SubItems.Add(dr["work_mos"].ToString());
                    listitem.SubItems.Add(string.Format("{0:#,##0.00}", Convert.ToDecimal(dr["gross_amnt"])));
                    listitem.SubItems.Add(string.Format("{0:#,##0.00}", Convert.ToDecimal(dr["wtax"])));
                    listitem.SubItems.Add(string.Format("{0:#,##0.00}", Convert.ToDecimal(dr["deduction"])));
                    listitem.SubItems.Add(string.Format("{0:#,##0.00}", Convert.ToDecimal(dr["netpay"])));
                    listitem.SubItems.Add(dr["year_end"].ToString());

                    lv_bonus_details.Items.Add(listitem);
                }

            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
            finally
            {
                connection.Close();
            }

            lv_bonus_details.View = View.Details;

            lv_bonus_details.Columns.Add(" No ", 0, HorizontalAlignment.Center);
            lv_bonus_details.Columns.Add(" Bonus ", 165, HorizontalAlignment.Center);
            lv_bonus_details.Columns.Add(" MMYYYY ", 165, HorizontalAlignment.Center);
            lv_bonus_details.Columns.Add(" Date Period ", 165, HorizontalAlignment.Center);
            lv_bonus_details.Columns.Add(" Employee ID ", 140, HorizontalAlignment.Center);
            lv_bonus_details.Columns.Add(" Lastname ", 140, HorizontalAlignment.Center);
            lv_bonus_details.Columns.Add(" Firstname ", 165, HorizontalAlignment.Center);
            lv_bonus_details.Columns.Add(" Middlename ", 165, HorizontalAlignment.Center);
            lv_bonus_details.Columns.Add(" Department ", 165, HorizontalAlignment.Center);
            lv_bonus_details.Columns.Add(" Salary ", 165, HorizontalAlignment.Center);
            lv_bonus_details.Columns.Add(" Date Hired ", 165, HorizontalAlignment.Center);
            lv_bonus_details.Columns.Add(" Work Mos ", 165, HorizontalAlignment.Center);
            lv_bonus_details.Columns.Add(" Gross Amount ", 165, HorizontalAlignment.Center);
            lv_bonus_details.Columns.Add(" Wtax ", 165, HorizontalAlignment.Center);
            lv_bonus_details.Columns.Add(" Deduction ", 165, HorizontalAlignment.Center);
            lv_bonus_details.Columns.Add(" Net Pay ", 165, HorizontalAlignment.Center);
            lv_bonus_details.Columns.Add(" Year End ", 0, HorizontalAlignment.Center);


        }

        private void ToExcel()
        {
            StringBuilder sb = new StringBuilder();
            Microsoft.Office.Interop.Excel.Application app = new Microsoft.Office.Interop.Excel.Application();
            app.Visible = true;
            Microsoft.Office.Interop.Excel.Workbook wb = app.Workbooks.Add(1);
            Microsoft.Office.Interop.Excel.Worksheet ws = (Microsoft.Office.Interop.Excel.Worksheet)wb.Worksheets[1];
            int i = 1;
            int i2 = 2;
            int x = 1;
            int x2 = 1;
            foreach (ColumnHeader ch in lv_bonus_details.Columns)
            {
                ws.Cells[x2, x] = ch.Text;
                x++;
            }

            foreach (ListViewItem lvi in lv_bonus_details.Items)
            {
                i = 1;
                foreach (ListViewItem.ListViewSubItem lvs in lvi.SubItems)
                {
                    ws.Cells[i2, i] = lvs.Text;
                    ws.Cells.Select();
                    ws.Cells.EntireColumn.AutoFit();
                    i++;
                }
                i2++;
            }
        }

        private void btn_print_Click(object sender, EventArgs e)
        {
            ToExcel();
        }

        private void LoadBonus()
        {

            try
            {
                connection.Open();
                cmd = new MySqlCommand();
                cmd.Connection = connection;
                cmd.CommandText = "Select bonus FROM bonus_details  where year_end = 'False'  group by bonus";
                da = new MySqlDataAdapter();
                da.SelectCommand = cmd;
                dt = new DataTable();
                da.Fill(dt);

                for (int i = 0; i < dt.Rows.Count; i++)
                {
                    DataRow dr = dt.Rows[i];



                    comboBox1.Items.Add((dr["bonus"].ToString()));

                }

            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
            finally
            {
                connection.Close();
            }
        }

        private void tb_search_TextChanged(object sender, EventArgs e)
        {
            lv_bonus_details.Columns.Clear();
            //display_bonus_Details("Select DISTINCT * FROM bonus_details WHERE year_end = 'False' and bonus  like '" + tb_search.Text + "%' or yyyymm like '" + tb_search.Text + "%' or date_period like  '" + tb_search.Text + "%'  or emp_id like  '" + tb_search.Text +  "%' or department like '" + tb_search.Text + "%' or salary like  '" + tb_search.Text + "%' or date_hired like  '" + tb_search.Text + "%' or work_mos like  '" + tb_search.Text + "%' or gross_amnt like  '" + tb_search.Text + "%' or wtax like  '" + tb_search.Text + "%' or deduction like '" + tb_search.Text + "%' or net_pay like '" + tb_search.Text + "%' or year_end like  '" + tb_search.Text + "%'");
            display_bonus_Details("Select DISTINCT * FROM bonus_details WHERE year_end = 'False' and bonus  like '" + tb_search.Text + "%'");


        }
    }
}
