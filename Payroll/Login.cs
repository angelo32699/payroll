﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using MySql.Data.MySqlClient;
using System.Configuration;
using System.Web.UI;
using System.IO;

namespace Payroll
{
    public partial class Login : Form
    {
        MySqlCommand cmd;
        MySqlDataAdapter da;
        DataTable dt;
        string user_id = null;

        string myconnection = ConfigurationManager.ConnectionStrings["MyConstring"].ConnectionString;
        MySqlConnection connection;
        public Login()
        {
            InitializeComponent();
            connection = new MySqlConnection(myconnection);   
            Init_Data();
      
        }


        private void Login_Load(object sender, EventArgs e)
        {
           
            chb_remember_me.FlatAppearance.BorderSize = 0;
            Display_Company_Name();
            

            Save_Data();

        }
  
        public void Display_Company_Name()
        {


            try
            {
                connection.Open();
                cmd = new MySqlCommand();
                cmd.Connection = connection;
                cmd.CommandText = "Select * FROM comp_details";
                da = new MySqlDataAdapter();
                da.SelectCommand = cmd;
                dt = new DataTable();
                da.Fill(dt);

                for (int i = 0; i < dt.Rows.Count; i++)
                {
                    DataRow dr = dt.Rows[i];

                    lbl_CompName.Text = (dr["comp_name"].ToString());
                }

            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
            finally
            {
                connection.Close();
            }

        }

        //check if connected from database
        public void DataConnection()
        {
            string ConnectionString = "datasource = localhost; username = root; password = ; database = payroll";
            MySqlConnection DatabaseConnect = new MySqlConnection(ConnectionString);

            try
            {
                DatabaseConnect.Open();
                MessageBox.Show("Successfully Connected From Database");
            }
            catch(Exception e)
            {
                MessageBox.Show(e.Message);
            }

            
        }
        public void Init_Data()
        {
            if (Properties.Settings.Default.username != string.Empty)
            {
                if (Properties.Settings.Default.remember == "yes")
                {
                    tb_username.Text = Properties.Settings.Default.username;
                    tb_password.Text = Properties.Settings.Default.password;
                    tb_username.ForeColor = Color.Black;
                    tb_password.ForeColor = Color.Black;
                    tb_password.PasswordChar = '*';
                    chb_remember_me.Checked = true;

                }
                else
                {
                    tb_username.Text = Properties.Settings.Default.username;
                }

            }
        }
        private void Save_Data()
        {
            if (chb_remember_me.Checked)
            {
                Properties.Settings.Default.username = tb_username.Text;
                Properties.Settings.Default.password = tb_password.Text;
                Properties.Settings.Default.remember = "yes";
                Properties.Settings.Default.Save();

                tb_username.TabStop = false;
                tb_password.TabStop = false;

            }
            else
            {
                Properties.Settings.Default.username = "";
                Properties.Settings.Default.password = "";
                Properties.Settings.Default.remember = "no";
                Properties.Settings.Default.Save();
            }
        }

        private void Login_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Enter)
            {
                proceed();
            }
        }
        private void tb_username_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Enter)
            {           
                proceed();
            }
        }

        private void tb_password_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Enter)
            {
                proceed();
            }
        }
        private void chb_remember_me_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Enter)
            {
                proceed();
            }
        }

        private void btn_cancel_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Enter)
            {
                proceed();
            }
        }

        private void btn_login_Click(object sender, EventArgs e)
        {
            proceed();
        }
       
        private void btn_clear_Click(object sender, EventArgs e)
        {
            if (tb_password.Text != "Username" || tb_password.Text != "Password")
            {
                tb_username.Text = "Username";
                tb_password.Text = "Password";
                tb_password.PasswordChar = '\0';
                tb_username.ForeColor = System.Drawing.ColorTranslator.FromHtml("#CC0000");
                tb_password.ForeColor = System.Drawing.ColorTranslator.FromHtml("#CC0000");

                chb_remember_me.Checked = false;

                Properties.Settings.Default.username = "";
                Properties.Settings.Default.password = "";
                Properties.Settings.Default.remember = "no";
                Properties.Settings.Default.Save();
            }
        }


        public void proceed()
        {
            
            MySqlDataAdapter sql = new MySqlDataAdapter("select user_id, username, password, count(*) from user_account where username = '" + tb_username.Text + "' and password = '" + tb_password.Text + "'", connection);
            DataTable dt = new DataTable();
            sql.Fill(dt);

            if (dt.Rows[0][1].ToString() == tb_username.Text && dt.Rows[0][2].ToString() == tb_password.Text)
            {
                user_id = dt.Rows[0][0].ToString();

                Save_Data();

                Insert_Audit_Logs();

                MessageBox.Show("Successfully login", "Login", MessageBoxButtons.OK, MessageBoxIcon.Information);
                this.Hide();
                Homepage homepage = new Homepage();
                homepage.username = tb_username.Text;
                homepage.user_id = user_id;
                homepage.ShowDialog();

               
            }
            else
            {
                MessageBox.Show("Incorrect username and password", "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
                
            }
        }

        private void Insert_Audit_Logs()
        {
            string myconnection = ConfigurationManager.ConnectionStrings["MyConstring"].ConnectionString;
            MySqlConnection connection = new MySqlConnection(myconnection);

            string date_today = DateTime.Now.ToString("M/d/yyyy hh:mm tt"); 

            String insertQuery = "INSERT INTO audit_logs(user_id, tran_date, transaction) VALUES('" + user_id + "', '" + date_today + "', '"+"Successfully Login."+"')";

             connection.Open();
             cmd = new MySqlCommand(insertQuery, connection);

             try
             {
                 if (cmd.ExecuteNonQuery() == 1)
                 {
                     //MessageBox.Show("Successfully Add Information", "Add Information", MessageBoxButtons.OK, MessageBoxIcon.Information);
                 }

             }
             catch (Exception ex)
             {
                 MessageBox.Show("Error" + ex);
             }

             connection.Close();
        }

        private void lbl_forgot_pwd_Click(object sender, EventArgs e)
        {
            MessageBox.Show("Please contact your administrator.", "Forgot Password", MessageBoxButtons.OK, MessageBoxIcon.Information);
        }

    
        private void chb_remember_me_Paint(object sender, PaintEventArgs e)
        {
            Point pt = new Point(e.ClipRectangle.Left + 2, e.ClipRectangle.Top + 4);
            Rectangle rect = new Rectangle(pt, new Size(20, 20));
            if (chb_remember_me.Checked)
            {
                using (Font wing = new Font("Wingdings", 14f))
                    e.Graphics.DrawString("ü", wing, Brushes.Brown, rect);
            }
            e.Graphics.DrawRectangle(Pens.Maroon, rect);
        }

        private void tb_username_Enter(object sender, EventArgs e)
        {
            if (tb_username.Text == "Username")
            {
                tb_username.Text = "";
                tb_username.ForeColor = Color.Black;
            }
        }

        private void tb_username_Leave(object sender, EventArgs e)
        {
            if (tb_username.Text == "")
            {
                tb_username.Text = "Username";
                tb_username.ForeColor = System.Drawing.ColorTranslator.FromHtml("#CC0000"); ;
            }
        }

        private void tb_password_Enter(object sender, EventArgs e)
        {
            if (tb_password.Text == "Password")
            {
                tb_password.Text = "";
                tb_password.ForeColor = Color.Black;
                tb_password.PasswordChar = '*';
           }
        }

        private void tb_password_Leave(object sender, EventArgs e)
        {
            if (tb_password.Text == "")
            {
                tb_password.Text = "Password";
                tb_password.ForeColor = System.Drawing.ColorTranslator.FromHtml("#CC0000"); ;
                tb_password.PasswordChar = '\0';
            }
        }

        private void btn_Exit_Click(object sender, EventArgs e)
        {
            Environment.Exit(0);
        }

        private void tb_username_TextChanged(object sender, EventArgs e)
        {
            tb_username.Text = tb_username.Text.Replace(Environment.NewLine, "");

          
        }

        private void tb_password_TextChanged(object sender, EventArgs e)
        {
            tb_password.Text = tb_password.Text.Replace(Environment.NewLine, "");
        }

        private void passHide_Click(object sender, EventArgs e)
        {
            tb_password.UseSystemPasswordChar = true;
            passHide.Visible = false;
            passShow.Visible = true;
        }

        private void passShow_Click(object sender, EventArgs e)
        {
            tb_password.UseSystemPasswordChar = false;
            passShow.Visible = false;
            passHide.Visible = true;
        }

        private void Login_Paint(object sender, PaintEventArgs e)
        {
            tb_username.BorderStyle = BorderStyle.None;
            tb_password.BorderStyle = BorderStyle.None;
            Pen p = new Pen(ColorTranslator.FromHtml("#C00000"));
            Graphics g = e.Graphics;
            int variance = 3;
            g.DrawRectangle(p, new Rectangle(tb_username.Location.X - variance, tb_username.Location.Y - variance, tb_username.Width + variance, tb_username.Height + variance));
            g.DrawRectangle(p, new Rectangle(tb_password.Location.X - variance, tb_password.Location.Y - variance, tb_password.Width + variance, tb_password.Height + variance));
        }

        
    }
}

