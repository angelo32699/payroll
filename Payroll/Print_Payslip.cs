﻿using MySql.Data.MySqlClient;
using Payroll.Reports;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Configuration;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using CrystalDecisions.CrystalReports.Engine;
using CrystalDecisions.Windows.Forms;

namespace Payroll
{
    public partial class Print_Payslip : Form
    {

        ReportDocument report = new ReportDocument();
        connect con = new connect();

      

        public Print_Payslip()
        {
            InitializeComponent();
        }

        private void Print_Payslip_Load(object sender, EventArgs e)
        {
            crystalReportViewer1.ToolPanelView = ToolPanelViewType.None;


            report.Load(@"C:\Temp\Payroll\Payroll\Reports\Payslip.rpt");
            DataSet ds = new DataSet();
            con.dataget("select * from payslip");
            con.mda.Fill(ds, "payslip");
            report.SetDataSource(ds);
            crystalReportViewer1.ReportSource = report;

           
          }

       }

    }


