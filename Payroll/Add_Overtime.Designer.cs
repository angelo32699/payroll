﻿namespace Payroll
{
    partial class Add_Overtime
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(Add_Overtime));
            this.lv_overtime = new System.Windows.Forms.ListView();
            this.label1 = new System.Windows.Forms.Label();
            this.tb_total_pay = new System.Windows.Forms.TextBox();
            this.label2 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.btn_update = new System.Windows.Forms.Button();
            this.btn_add = new System.Windows.Forms.Button();
            this.btn_delete = new System.Windows.Forms.Button();
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.tb_ot_hours = new System.Windows.Forms.TextBox();
            this.btn_clear = new System.Windows.Forms.Button();
            this.label9 = new System.Windows.Forms.Label();
            this.label8 = new System.Windows.Forms.Label();
            this.lbl_ot4 = new System.Windows.Forms.Label();
            this.lbl_ot3 = new System.Windows.Forms.Label();
            this.lbl_ot2 = new System.Windows.Forms.Label();
            this.lbl_ot1 = new System.Windows.Forms.Label();
            this.tb_ot_pay = new System.Windows.Forms.TextBox();
            this.label6 = new System.Windows.Forms.Label();
            this.label7 = new System.Windows.Forms.Label();
            this.label5 = new System.Windows.Forms.Label();
            this.lbl_code = new System.Windows.Forms.Label();
            this.cb_overtime_categories = new System.Windows.Forms.ComboBox();
            this.btn_apply = new System.Windows.Forms.Button();
            this.label4 = new System.Windows.Forms.Label();
            this.label10 = new System.Windows.Forms.Label();
            this.groupBox1.SuspendLayout();
            this.SuspendLayout();
            // 
            // lv_overtime
            // 
            this.lv_overtime.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lv_overtime.FullRowSelect = true;
            this.lv_overtime.GridLines = true;
            this.lv_overtime.HideSelection = false;
            this.lv_overtime.Location = new System.Drawing.Point(3, 238);
            this.lv_overtime.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.lv_overtime.Name = "lv_overtime";
            this.lv_overtime.Size = new System.Drawing.Size(532, 194);
            this.lv_overtime.TabIndex = 0;
            this.lv_overtime.UseCompatibleStateImageBehavior = false;
            this.lv_overtime.Click += new System.EventHandler(this.lv_overtime_Click);
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label1.Location = new System.Drawing.Point(19, 53);
            this.label1.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(142, 20);
            this.label1.TabIndex = 2;
            this.label1.Text = "Type of Overtime:";
            // 
            // tb_total_pay
            // 
            this.tb_total_pay.Location = new System.Drawing.Point(354, 446);
            this.tb_total_pay.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.tb_total_pay.Name = "tb_total_pay";
            this.tb_total_pay.Size = new System.Drawing.Size(178, 22);
            this.tb_total_pay.TabIndex = 3;
            this.tb_total_pay.Text = "0.00";
            this.tb_total_pay.TextChanged += new System.EventHandler(this.tb_total_pay_TextChanged);
            this.tb_total_pay.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.tb_total_pay_KeyPress);
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label2.Location = new System.Drawing.Point(19, 90);
            this.label2.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(152, 20);
            this.label2.TabIndex = 5;
            this.label2.Text = "Hours of Overtime:";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label3.Location = new System.Drawing.Point(296, 450);
            this.label3.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(51, 20);
            this.label3.TabIndex = 7;
            this.label3.Text = "Total:";
            // 
            // btn_update
            // 
            this.btn_update.Enabled = false;
            this.btn_update.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btn_update.Location = new System.Drawing.Point(133, 183);
            this.btn_update.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.btn_update.Name = "btn_update";
            this.btn_update.Size = new System.Drawing.Size(121, 36);
            this.btn_update.TabIndex = 90;
            this.btn_update.Text = "UPDATE";
            this.btn_update.UseVisualStyleBackColor = true;
            this.btn_update.Click += new System.EventHandler(this.btn_update_Click);
            // 
            // btn_add
            // 
            this.btn_add.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btn_add.Location = new System.Drawing.Point(4, 183);
            this.btn_add.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.btn_add.Name = "btn_add";
            this.btn_add.Size = new System.Drawing.Size(121, 36);
            this.btn_add.TabIndex = 91;
            this.btn_add.Text = "ADD";
            this.btn_add.UseVisualStyleBackColor = true;
            this.btn_add.Click += new System.EventHandler(this.btn_add_Click);
            // 
            // btn_delete
            // 
            this.btn_delete.Enabled = false;
            this.btn_delete.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btn_delete.Location = new System.Drawing.Point(263, 183);
            this.btn_delete.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.btn_delete.Name = "btn_delete";
            this.btn_delete.Size = new System.Drawing.Size(121, 36);
            this.btn_delete.TabIndex = 92;
            this.btn_delete.Text = "DELETE";
            this.btn_delete.UseVisualStyleBackColor = true;
            this.btn_delete.Click += new System.EventHandler(this.btn_delete_Click);
            // 
            // groupBox1
            // 
            this.groupBox1.Controls.Add(this.tb_ot_hours);
            this.groupBox1.Controls.Add(this.btn_clear);
            this.groupBox1.Controls.Add(this.label9);
            this.groupBox1.Controls.Add(this.label8);
            this.groupBox1.Controls.Add(this.lbl_ot4);
            this.groupBox1.Controls.Add(this.lbl_ot3);
            this.groupBox1.Controls.Add(this.lbl_ot2);
            this.groupBox1.Controls.Add(this.lbl_ot1);
            this.groupBox1.Controls.Add(this.tb_ot_pay);
            this.groupBox1.Controls.Add(this.label6);
            this.groupBox1.Controls.Add(this.label7);
            this.groupBox1.Controls.Add(this.label5);
            this.groupBox1.Controls.Add(this.btn_delete);
            this.groupBox1.Controls.Add(this.lbl_code);
            this.groupBox1.Controls.Add(this.btn_add);
            this.groupBox1.Controls.Add(this.label1);
            this.groupBox1.Controls.Add(this.btn_update);
            this.groupBox1.Controls.Add(this.label2);
            this.groupBox1.Controls.Add(this.cb_overtime_categories);
            this.groupBox1.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.groupBox1.Location = new System.Drawing.Point(5, 5);
            this.groupBox1.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Padding = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.groupBox1.Size = new System.Drawing.Size(539, 226);
            this.groupBox1.TabIndex = 93;
            this.groupBox1.TabStop = false;
            this.groupBox1.Text = "Add Overtime";
            // 
            // tb_ot_hours
            // 
            this.tb_ot_hours.Enabled = false;
            this.tb_ot_hours.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.tb_ot_hours.ForeColor = System.Drawing.Color.Black;
            this.tb_ot_hours.Location = new System.Drawing.Point(181, 91);
            this.tb_ot_hours.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.tb_ot_hours.Name = "tb_ot_hours";
            this.tb_ot_hours.Size = new System.Drawing.Size(285, 24);
            this.tb_ot_hours.TabIndex = 167;
            this.tb_ot_hours.Text = "0.00";
            this.tb_ot_hours.TextChanged += new System.EventHandler(this.tb_ot_hours_TextChanged);
            this.tb_ot_hours.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.tb_ot_hours_KeyPress);
            // 
            // btn_clear
            // 
            this.btn_clear.Enabled = false;
            this.btn_clear.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btn_clear.Location = new System.Drawing.Point(392, 183);
            this.btn_clear.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.btn_clear.Name = "btn_clear";
            this.btn_clear.Size = new System.Drawing.Size(139, 36);
            this.btn_clear.TabIndex = 166;
            this.btn_clear.Text = "CLEAR";
            this.btn_clear.UseVisualStyleBackColor = true;
            this.btn_clear.Click += new System.EventHandler(this.btn_cancel_Click);
            // 
            // label9
            // 
            this.label9.AutoSize = true;
            this.label9.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label9.Location = new System.Drawing.Point(189, 30);
            this.label9.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(46, 18);
            this.label9.TabIndex = 165;
            this.label9.Text = "label9";
            this.label9.Visible = false;
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label8.Location = new System.Drawing.Point(23, 37);
            this.label8.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(46, 17);
            this.label8.TabIndex = 164;
            this.label8.Text = "label8";
            this.label8.Visible = false;
            // 
            // lbl_ot4
            // 
            this.lbl_ot4.AutoSize = true;
            this.lbl_ot4.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lbl_ot4.Location = new System.Drawing.Point(416, 30);
            this.lbl_ot4.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.lbl_ot4.Name = "lbl_ot4";
            this.lbl_ot4.Size = new System.Drawing.Size(29, 18);
            this.lbl_ot4.TabIndex = 101;
            this.lbl_ot4.Text = "ot4";
            this.lbl_ot4.Visible = false;
            // 
            // lbl_ot3
            // 
            this.lbl_ot3.AutoSize = true;
            this.lbl_ot3.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lbl_ot3.Location = new System.Drawing.Point(360, 30);
            this.lbl_ot3.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.lbl_ot3.Name = "lbl_ot3";
            this.lbl_ot3.Size = new System.Drawing.Size(29, 18);
            this.lbl_ot3.TabIndex = 100;
            this.lbl_ot3.Text = "ot3";
            this.lbl_ot3.Visible = false;
            // 
            // lbl_ot2
            // 
            this.lbl_ot2.AutoSize = true;
            this.lbl_ot2.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lbl_ot2.Location = new System.Drawing.Point(308, 32);
            this.lbl_ot2.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.lbl_ot2.Name = "lbl_ot2";
            this.lbl_ot2.Size = new System.Drawing.Size(29, 18);
            this.lbl_ot2.TabIndex = 99;
            this.lbl_ot2.Text = "ot2";
            this.lbl_ot2.Visible = false;
            // 
            // lbl_ot1
            // 
            this.lbl_ot1.AutoSize = true;
            this.lbl_ot1.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lbl_ot1.Location = new System.Drawing.Point(261, 34);
            this.lbl_ot1.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.lbl_ot1.Name = "lbl_ot1";
            this.lbl_ot1.Size = new System.Drawing.Size(29, 18);
            this.lbl_ot1.TabIndex = 98;
            this.lbl_ot1.Text = "ot1";
            this.lbl_ot1.Visible = false;
            // 
            // tb_ot_pay
            // 
            this.tb_ot_pay.Enabled = false;
            this.tb_ot_pay.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.tb_ot_pay.ForeColor = System.Drawing.Color.Black;
            this.tb_ot_pay.Location = new System.Drawing.Point(183, 128);
            this.tb_ot_pay.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.tb_ot_pay.Name = "tb_ot_pay";
            this.tb_ot_pay.Size = new System.Drawing.Size(285, 24);
            this.tb_ot_pay.TabIndex = 97;
            this.tb_ot_pay.Text = "0.00";
            this.tb_ot_pay.TextChanged += new System.EventHandler(this.tb_ot_pay_TextChanged);
            this.tb_ot_pay.Enter += new System.EventHandler(this.tb_ot_pay_Enter);
            this.tb_ot_pay.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.tb_ot_pay_KeyPress);
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label6.Location = new System.Drawing.Point(53, 126);
            this.label6.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(109, 20);
            this.label6.TabIndex = 96;
            this.label6.Text = "Ovetime Pay:";
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label7.Location = new System.Drawing.Point(477, 98);
            this.label7.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(46, 17);
            this.label7.TabIndex = 95;
            this.label7.Text = "label7";
            this.label7.Visible = false;
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label5.Location = new System.Drawing.Point(477, 59);
            this.label5.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(46, 17);
            this.label5.TabIndex = 93;
            this.label5.Text = "label5";
            this.label5.Visible = false;
            // 
            // lbl_code
            // 
            this.lbl_code.AutoSize = true;
            this.lbl_code.Location = new System.Drawing.Point(450, 48);
            this.lbl_code.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.lbl_code.Name = "lbl_code";
            this.lbl_code.Size = new System.Drawing.Size(0, 20);
            this.lbl_code.TabIndex = 7;
            this.lbl_code.Visible = false;
            // 
            // cb_overtime_categories
            // 
            this.cb_overtime_categories.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cb_overtime_categories.Enabled = false;
            this.cb_overtime_categories.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.cb_overtime_categories.ForeColor = System.Drawing.Color.Black;
            this.cb_overtime_categories.FormattingEnabled = true;
            this.cb_overtime_categories.Location = new System.Drawing.Point(181, 53);
            this.cb_overtime_categories.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.cb_overtime_categories.Name = "cb_overtime_categories";
            this.cb_overtime_categories.Size = new System.Drawing.Size(287, 26);
            this.cb_overtime_categories.TabIndex = 1;
            this.cb_overtime_categories.SelectedIndexChanged += new System.EventHandler(this.cb_overtime_categories_SelectedIndexChanged);
            // 
            // btn_apply
            // 
            this.btn_apply.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btn_apply.Location = new System.Drawing.Point(8, 441);
            this.btn_apply.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.btn_apply.Name = "btn_apply";
            this.btn_apply.Size = new System.Drawing.Size(121, 36);
            this.btn_apply.TabIndex = 165;
            this.btn_apply.Text = "APPLY";
            this.btn_apply.UseVisualStyleBackColor = true;
            this.btn_apply.Click += new System.EventHandler(this.btn_apply_Click);
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(183, 441);
            this.label4.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(44, 16);
            this.label4.TabIndex = 166;
            this.label4.Text = "label4";
            this.label4.Visible = false;
            // 
            // label10
            // 
            this.label10.AutoSize = true;
            this.label10.Location = new System.Drawing.Point(180, 457);
            this.label10.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label10.Name = "label10";
            this.label10.Size = new System.Drawing.Size(51, 16);
            this.label10.TabIndex = 167;
            this.label10.Text = "label10";
            this.label10.Visible = false;
            // 
            // Add_Overtime
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 16F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(544, 486);
            this.Controls.Add(this.label10);
            this.Controls.Add(this.btn_apply);
            this.Controls.Add(this.label4);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.tb_total_pay);
            this.Controls.Add(this.lv_overtime);
            this.Controls.Add(this.groupBox1);
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.MaximizeBox = false;
            this.Name = "Add_Overtime";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Load += new System.EventHandler(this.Add_Overtime_Load);
            this.groupBox1.ResumeLayout(false);
            this.groupBox1.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.ListView lv_overtime;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Button btn_update;
        private System.Windows.Forms.Button btn_add;
        private System.Windows.Forms.Button btn_delete;
        private System.Windows.Forms.GroupBox groupBox1;
        private System.Windows.Forms.Label lbl_code;
        internal System.Windows.Forms.Label label5;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.Label lbl_ot4;
        private System.Windows.Forms.Label lbl_ot3;
        private System.Windows.Forms.Label lbl_ot2;
        private System.Windows.Forms.Label lbl_ot1;
        internal System.Windows.Forms.Label label8;
        private System.Windows.Forms.Button btn_apply;
        private System.Windows.Forms.TextBox tb_ot_pay;
        internal System.Windows.Forms.Label label9;
        private System.Windows.Forms.ComboBox cb_overtime_categories;
        private System.Windows.Forms.Button btn_clear;
        public System.Windows.Forms.TextBox tb_total_pay;
        private System.Windows.Forms.TextBox tb_ot_hours;
        internal System.Windows.Forms.Label label4;
        internal System.Windows.Forms.Label label10;
    }
}