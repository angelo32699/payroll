﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using MySql.Data.MySqlClient;
using System.Configuration;

namespace Payroll
{
    public partial class Pag_ibig : Form
    {
        MySqlCommand cmd;
        MySqlDataAdapter da;
        DataTable dt;
        public Pag_ibig()
        {
            InitializeComponent();
        }
        public void display_pag_ibig(string sql1)
        {

            string myconnection = ConfigurationManager.ConnectionStrings["MyConstring"].ConnectionString;
            MySqlConnection connection = new MySqlConnection(myconnection);

            try
            {
                connection.Open();
                cmd = new MySqlCommand();
                cmd.Connection = connection;
                cmd.CommandText = sql1;
                da = new MySqlDataAdapter();
                da.SelectCommand = cmd;
                dt = new DataTable();
                da.Fill(dt);
                lv_pag_ibig.Items.Clear();



                for (int i = 0; i < dt.Rows.Count; i++)
                {
                    DataRow dr = dt.Rows[i];
                    ListViewItem listitem = new ListViewItem(dr["no"].ToString());
                 
                    listitem.SubItems.Add(string.Format("{0:#,##0.00}", Convert.ToDecimal(dr["min_amnt"])));
                    listitem.SubItems.Add(string.Format("{0:#,##0.00}", Convert.ToDecimal(dr["max_amnt"])));
                    listitem.SubItems.Add(string.Format("{0:#,##0.00}", Convert.ToDecimal(dr["emply_cont"])));
                    listitem.SubItems.Add(string.Format("{0:#,##0.00}", Convert.ToDecimal(dr["emplyr_cont"])));
              

                    lv_pag_ibig.Items.Add(listitem);
                }

            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
            finally
            {
                connection.Close();
            }


            lv_pag_ibig.View = View.Details;

            lv_pag_ibig.Columns.Add(" No ", 0, HorizontalAlignment.Center);
            lv_pag_ibig.Columns.Add(" Minimum Amount ", 160, HorizontalAlignment.Center);
            lv_pag_ibig.Columns.Add(" Maximum Amount ", 160, HorizontalAlignment.Center);
            lv_pag_ibig.Columns.Add(" Employee Contribution ", 170, HorizontalAlignment.Center);
            lv_pag_ibig.Columns.Add(" Employer Contribution ", 170, HorizontalAlignment.Center);
          



        }

        private void Pag_ibig_Load(object sender, EventArgs e)
        {
            display_pag_ibig("Select * from pag_ibig");
        }

        private void lv_SSS_Click(object sender, EventArgs e)
        {
            tb_min_amount.Enabled = false;
            tb_max_amount.Enabled = false;
            tb_emp_cont.Enabled = false;
            tb_emplyr_cont.Enabled = false;


            btn_add.Enabled = false;
            btn_Update.Enabled = true;
            btn_delete.Enabled = true;
            btn_Update.Text = "EDIT";
            btn_add.Text = "ADD";


            label4.Text = lv_pag_ibig.SelectedItems[0].SubItems[0].Text;
            tb_min_amount.Text = lv_pag_ibig.SelectedItems[0].SubItems[1].Text;
            tb_max_amount.Text = lv_pag_ibig.SelectedItems[0].SubItems[2].Text;
            tb_emp_cont.Text = lv_pag_ibig.SelectedItems[0].SubItems[3].Text;
            tb_emplyr_cont.Text = lv_pag_ibig.SelectedItems[0].SubItems[4].Text;
           
        }

     
        private void tb_search_TextChanged(object sender, EventArgs e)
        {
            lv_pag_ibig.Columns.Clear();
            display_pag_ibig("Select DISTINCT * FROM pag_ibig WHERE  pag_ibig.min_amnt like '" + tb_search.Text + "%' or pag_ibig.max_amnt like '" + tb_search.Text + "%' or  pag_ibig.emplyr_cont like '" + tb_search.Text + "%'  or  pag_ibig.emply_cont like '" + tb_search.Text + "%'");
        }

        private void tb_minimum_amount_KeyPress(object sender, KeyPressEventArgs e)
        {
            if ((e.KeyChar < 32) || (e.KeyChar >= '0') && (e.KeyChar <= '9') || (e.KeyChar == ',') || (e.KeyChar == '.'))
                return;

            // only evident errors (like 'A' or '&') are restricted
            e.Handled = true;
        }

        private void tb_maximum_amount_KeyPress(object sender, KeyPressEventArgs e)
        {
            if ((e.KeyChar < 32) || (e.KeyChar >= '0') && (e.KeyChar <= '9') || (e.KeyChar == ',') || (e.KeyChar == '.'))
                return;

            // only evident errors (like 'A' or '&') are restricted
            e.Handled = true;
        }

        private void tb_emp_cont_KeyPress(object sender, KeyPressEventArgs e)
        {
            if ((e.KeyChar < 32) || (e.KeyChar >= '0') && (e.KeyChar <= '9') || (e.KeyChar == ',') || (e.KeyChar == '.'))
                return;

            // only evident errors (like 'A' or '&') are restricted
            e.Handled = true;
        }

        private void tb_employer_contribution_KeyPress(object sender, KeyPressEventArgs e)
        {
            if ((e.KeyChar < 32) || (e.KeyChar >= '0') && (e.KeyChar <= '9') || (e.KeyChar == ',') || (e.KeyChar == '.'))
                return;

            // only evident errors (like 'A' or '&') are restricted
            e.Handled = true;
        }

     

    
        public void insert_pag_ibig()
        {
            string myconnection = ConfigurationManager.ConnectionStrings["MyConstring"].ConnectionString;
            MySqlConnection connection = new MySqlConnection(myconnection);


            decimal min_amount = Convert.ToDecimal(tb_min_amount.Text);
            decimal max_amount = Convert.ToDecimal(tb_max_amount.Text);
            decimal emp_cont = Convert.ToDecimal(tb_emp_cont.Text);
            decimal empyr_cont = Convert.ToDecimal(tb_emplyr_cont.Text);

            string insertQuery = "INSERT INTO pag_ibig(min_amnt, max_amnt,  emply_cont, emplyr_cont,) VALUES('" + min_amount + "', '" + max_amount + "', '" + emp_cont + "', '" + empyr_cont + "')";

            connection.Open();
            MySqlCommand mySqlCommand = new MySqlCommand(insertQuery, connection);


            try
            {
                if (mySqlCommand.ExecuteNonQuery() == 1)
                {
                    //MessageBox.Show("Successfully Add Information", "Add Information", MessageBoxButtons.OK, MessageBoxIcon.Information);
                }
                else
                {

                }
            }
            catch (Exception ex)
            {
                MessageBox.Show("Error" + ex);
            }

            connection.Close();
        }
    
        public void update_pag_ibig()
        {
            string myconnection = ConfigurationManager.ConnectionStrings["MyConstring"].ConnectionString;
            MySqlConnection connection = new MySqlConnection(myconnection);

            decimal min_amount = Convert.ToDecimal(tb_min_amount.Text);
            decimal max_amount = Convert.ToDecimal(tb_max_amount.Text);
            decimal emp_cont = Convert.ToDecimal(tb_emp_cont.Text);
            decimal empyr_cont = Convert.ToDecimal(tb_emplyr_cont.Text);

            try
            {
                connection.Open();
                cmd = new MySqlCommand();
                cmd.Connection = connection;
                cmd.CommandText = "UPDATE pag_ibig SET min_amnt  = '" + min_amount + "', max_amnt = '" + max_amount + "' ,   emply_cont = '" + emp_cont + "', emplyr_cont = '" + empyr_cont + "'   WHERE no = '" + label4.Text + "'";
                da = new MySqlDataAdapter();
                da.SelectCommand = cmd;
                dt = new DataTable();
                da.Fill(dt);

            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
            finally
            {
                connection.Close();
            }
        }
        public void delete_pag_ibig()
        {
            string myconnection = ConfigurationManager.ConnectionStrings["MyConstring"].ConnectionString;
            MySqlConnection connection = new MySqlConnection(myconnection);

            try
            {
                connection.Open();
                cmd = new MySqlCommand();
                cmd.Connection = connection;
                cmd.CommandText = "Delete From pag_ibig where no = '" + label4.Text + "'";
                da = new MySqlDataAdapter();
                da.SelectCommand = cmd;
                dt = new DataTable();
                da.Fill(dt);

            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
            finally
            {
                connection.Close();
            }
        }
    
        private void tb_minimum_amount_TextChanged(object sender, EventArgs e){

            if (tb_min_amount.Text == "") {

                tb_min_amount.Text = "0.00";
            }
        }

        private void tb_maximum_amount_TextChanged(object sender, EventArgs e){
            if (tb_max_amount.Text == ""){

                tb_max_amount.Text = "0.00";
            }
        }

        private void tb_emp_cont_TextChanged(object sender, EventArgs e)
        {
            if (tb_emp_cont.Text == ""){

                tb_emp_cont.Text = "0.00";
            }
        }

        private void tb_employer_contribution_TextChanged(object sender, EventArgs e)
        {
            if (tb_emplyr_cont.Text == ""){

                tb_emplyr_cont.Text = "0.00";
            }
        }

        private void btn_add_Click(object sender, EventArgs e)
        {
            if (btn_add.Text == "ADD")
            {
                btn_add.Image = Properties.Resources.Button_SAVE_Red__1_;
                tb_min_amount.Enabled = true;
                tb_max_amount.Enabled = true;
                tb_emp_cont.Enabled = true;
                tb_emplyr_cont.Enabled = true;
                btn_add.Text = "SAVE";
            
            }else if(label4.Text != "0")
            {
                DialogResult dialogResult = MessageBox.Show("Are you sure want to update Pag-Ibig Table?", "Update SSS Table", MessageBoxButtons.YesNo, MessageBoxIcon.Question);
                if (dialogResult == DialogResult.Yes)
                {

                    update_pag_ibig();
                    lv_pag_ibig.Columns.Clear();
                    display_pag_ibig("Select * from pag_ibig");
                    MessageBox.Show("Successfully Update Pag_ibig Table", "Update SSS Table", MessageBoxButtons.OK, MessageBoxIcon.Information);
                    disable_all();
                }
            }
            else
            {
                              
                    insert_pag_ibig();
                    lv_pag_ibig.Columns.Clear();
                    display_pag_ibig("Select * from pag_ibig");
                    MessageBox.Show("Successfully Added", "Add Pag-Ibig Information", MessageBoxButtons.OK, MessageBoxIcon.Information);
                    disable_all();
                

            }
        }

        private void btn_Update_Click(object sender, EventArgs e)
        {
            if (btn_Update.Text == "EDIT")
            {

                tb_min_amount.Enabled = true;
                tb_max_amount.Enabled = true;
                tb_emp_cont.Enabled = true;
                tb_emplyr_cont.Enabled = true;

                btn_Update.Enabled = false;
                btn_add.Enabled = true;
                btn_add.Text = "SAVE";
                btn_add.Image = Properties.Resources.Button_SAVE_Red__1_;
            }
            
        }

        private void btn_delete_Click(object sender, EventArgs e)
        {
            DialogResult dialogResult = MessageBox.Show("Are you sure want to delete Pag-Ibig Table?", "Delete SSS Table", MessageBoxButtons.YesNo, MessageBoxIcon.Question);
            if (dialogResult == DialogResult.Yes)
            {
                delete_pag_ibig();
                lv_pag_ibig.Columns.Clear();
                display_pag_ibig("Select * from pag_ibig");
                MessageBox.Show("Successfully Delete Pag-Ibig Table", "Delete Pag-ibig Table", MessageBoxButtons.OK, MessageBoxIcon.Information);
                disable_all();
            }
        }

  
        private void btn_add_MouseEnter(object sender, EventArgs e)
        {
            btn_add.BackgroundColor = ColorTranslator.FromHtml("#FFAFAF");
            btn_add.BackColor = ColorTranslator.FromHtml("#FFAFAF");
            btn_add.ForeColor = Color.White;

        }

        private void btn_add_MouseHover(object sender, EventArgs e)
        {
            btn_add.BackgroundColor = ColorTranslator.FromHtml("#FFAFAF");
            btn_add.BackColor = ColorTranslator.FromHtml("#FFAFAF");
            btn_add.ForeColor = Color.White;
        }

        private void btn_add_MouseLeave(object sender, EventArgs e)
        {
            btn_add.BackgroundColor = Color.Transparent;
            btn_add.BackColor = Color.Transparent;
            btn_add.ForeColor = ColorTranslator.FromHtml("#C00000");
        }

        private void btn_Update_MouseEnter(object sender, EventArgs e)
        {
            btn_Update.BackgroundColor = ColorTranslator.FromHtml("#FFAFAF");
            btn_Update.BackColor = ColorTranslator.FromHtml("#FFAFAF");
            btn_Update.ForeColor = Color.White;
        }

        private void btn_Update_MouseHover(object sender, EventArgs e)
        {
            btn_Update.BackgroundColor = ColorTranslator.FromHtml("#FFAFAF");
            btn_Update.BackColor = ColorTranslator.FromHtml("#FFAFAF");
            btn_Update.ForeColor = Color.White;
        }

        private void btn_Update_MouseLeave(object sender, EventArgs e)
        {
            btn_Update.BackgroundColor = Color.Transparent;
            btn_Update.BackColor = Color.Transparent;
            btn_Update.ForeColor = ColorTranslator.FromHtml("#C00000");
        }

        private void btn_delete_MouseEnter(object sender, EventArgs e)
        {
            btn_delete.BackgroundColor = ColorTranslator.FromHtml("#FFAFAF");
            btn_delete.BackColor = ColorTranslator.FromHtml("#FFAFAF");
            btn_delete.ForeColor = Color.White;
        }

        private void btn_delete_MouseHover(object sender, EventArgs e)
        {
            btn_delete.BackgroundColor = ColorTranslator.FromHtml("#FFAFAF");
            btn_delete.BackColor = ColorTranslator.FromHtml("#FFAFAF");
            btn_delete.ForeColor = Color.White;
        }

        private void btn_delete_MouseLeave(object sender, EventArgs e)
        {
            btn_delete.BackgroundColor = Color.Transparent;
            btn_delete.BackColor = Color.Transparent;
            btn_delete.ForeColor = ColorTranslator.FromHtml("#C00000");
        }

        private void ToExcel()
        {
            StringBuilder sb = new StringBuilder();
            Microsoft.Office.Interop.Excel.Application app = new Microsoft.Office.Interop.Excel.Application();
            app.Visible = true;
            Microsoft.Office.Interop.Excel.Workbook wb = app.Workbooks.Add(1);
            Microsoft.Office.Interop.Excel.Worksheet ws = (Microsoft.Office.Interop.Excel.Worksheet)wb.Worksheets[1];
            int i = 1;
            int i2 = 2;
            int x = 1;
            int x2 = 1;
            foreach (ColumnHeader ch in lv_pag_ibig.Columns)
            {
                ws.Cells[x2, x] = ch.Text;
                x++;
            }

            foreach (ListViewItem lvi in lv_pag_ibig.Items)
            {
                i = 1;
                foreach (ListViewItem.ListViewSubItem lvs in lvi.SubItems)
                {
                    ws.Cells[i2, i] = lvs.Text;
                    ws.Cells.Select();
                    ws.Cells.EntireColumn.AutoFit();
                    i++;
                }
                i2++;
            }
        }

        private void btn_export_Click(object sender, EventArgs e)
        {
            ToExcel();
        }

        private void btn_export_MouseEnter(object sender, EventArgs e)
        {
            btn_export.BackgroundColor = ColorTranslator.FromHtml("#FFAFAF");
            btn_export.BackColor = ColorTranslator.FromHtml("#FFAFAF");
            btn_export.ForeColor = Color.White;
        }

        private void btn_export_MouseHover(object sender, EventArgs e)
        {
            btn_export.BackgroundColor = ColorTranslator.FromHtml("#FFAFAF");
            btn_export.BackColor = ColorTranslator.FromHtml("#FFAFAF");
            btn_export.ForeColor = Color.White;

        }

        private void btn_export_MouseLeave(object sender, EventArgs e)
        {
            btn_export.BackgroundColor = Color.Transparent;
            btn_export.BackColor = Color.Transparent;
            btn_export.ForeColor = ColorTranslator.FromHtml("#C00000");
        }

        private void disable_all()
        {
            btn_add.Image = Properties.Resources.Button_ADD_Red__1___1_;

            label4.Text = "0";

            btn_add.Text = "ADD";
            btn_Update.Text = "EDIT";
            tb_min_amount.Text = "";
            tb_max_amount.Text = "";
            tb_emp_cont.Text = "";
            tb_emplyr_cont.Text = "";
            tb_min_amount.Enabled = false;
            tb_max_amount.Enabled = false;
            tb_emp_cont.Enabled = false;
            tb_emplyr_cont.Enabled = false;

            btn_add.Enabled = true;
            btn_Update.Enabled = false;
            btn_delete.Enabled = false;
        }
    }
}