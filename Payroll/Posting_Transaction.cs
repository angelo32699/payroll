﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using MySql.Data.MySqlClient;
using System.Configuration;
using System.Threading;
using System.Globalization;

namespace Payroll
{
   public partial class Posting_Transaction : Form
    {
        MySqlCommand cmd;
        MySqlDataAdapter da;
        DataTable dt;

     
        private static string period;
        private static string pay_details_period;
        private static string pay_details_month;
        private static string process_tag;

        private static string date_from_convert;
        private static string date_to_convert;


        decimal total_bonus_netpay;
        decimal total_gross_pay_details;
        decimal total_details_total_wtax;

        public Posting_Transaction()
        {
            InitializeComponent();
        }
        
      

        private DateTime ConvertStringToDate()
        {
            throw new NotImplementedException();
        }

        public void insert_personal_information()
        {
            string myconnection = ConfigurationManager.ConnectionStrings["MyConstring"].ConnectionString;
            MySqlConnection connection = new MySqlConnection(myconnection);




            string insertQuery = ("INSERT INTO pay_trans (emp_no, sal_id, loan_id, emp_id) SELECT employee.emp_no, salary.salary_id, loan.loan_id,  employee.emp_id from employee left join salary on employee.emp_no = salary.emp_no left join loan on employee.emp_no = loan.emp_no where employee.status = 'Active'");



            connection.Open();
            MySqlCommand mySqlCommand = new MySqlCommand(insertQuery, connection);


            try
            {
                if (mySqlCommand.ExecuteNonQuery() == 1)
                {
                    //MessageBox.Show("Successfully Add Information", "Add Information", MessageBoxButtons.OK, MessageBoxIcon.Information);
                }
                connection.Close();

            }
            catch (Exception ex)
            {
                MessageBox.Show("Error" + ex);
            }
        }
        private void Posting_Transaction_Load(object sender, EventArgs e)
        {
            DateTime dt = DateTime.Now;
            string s = dt.ToString("M/d/yyyy");
            label6.Text = s;
        }
      
        public void delete_inserted_data()
        {

            //delete payslip
            string myconnection3 = ConfigurationManager.ConnectionStrings["MyConstring"].ConnectionString;
            MySqlConnection connection3 = new MySqlConnection(myconnection3);

            try
            {
                connection3.Open();
                cmd = new MySqlCommand();
                cmd.Connection = connection3;
                string query2 = "DELETE FROM payslip";
                cmd.CommandText = query2;
                da = new MySqlDataAdapter();
                da.SelectCommand = cmd;
                dt = new DataTable();
                da.Fill(dt);


            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
            finally
            {
                connection3.Close();
            }

            //delete bank remittance

            string myconnection4 = ConfigurationManager.ConnectionStrings["MyConstring"].ConnectionString;
            MySqlConnection connection4 = new MySqlConnection(myconnection4);

            try
            {
                connection4.Open();
                cmd = new MySqlCommand();
                cmd.Connection = connection4;
                string query2 = "DELETE FROM bank_remittance";
                cmd.CommandText = query2;
                da = new MySqlDataAdapter();
                da.SelectCommand = cmd;
                dt = new DataTable();
                da.Fill(dt);


            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
            finally
            {
                connection4.Close();
            }

            //delete sum table
            string myconnection5 = ConfigurationManager.ConnectionStrings["MyConstring"].ConnectionString;
            MySqlConnection connection5 = new MySqlConnection(myconnection5);

            try
            {
                connection5.Open();
                cmd = new MySqlCommand();
                cmd.Connection = connection5;
                string query2 = "DELETE FROM sum_table";
                cmd.CommandText = query2;
                da = new MySqlDataAdapter();
                da.SelectCommand = cmd;
                dt = new DataTable();
                da.Fill(dt);


            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
            finally
            {
                connection5.Close();
            }


            //delete annual_tax_inc
            string myconnection6 = ConfigurationManager.ConnectionStrings["MyConstring"].ConnectionString;
            MySqlConnection connection6 = new MySqlConnection(myconnection6);

            try
            {
                connection6.Open();
                cmd = new MySqlCommand();
                cmd.Connection = connection6;
                string query2 = "DELETE FROM annual_tax_inc";
                cmd.CommandText = query2;
                da = new MySqlDataAdapter();
                da.SelectCommand = cmd;
                dt = new DataTable();
                da.Fill(dt);


            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
            finally
            {
                connection6.Close();
            }

            //delete sum_bonus
            string myconnection7 = ConfigurationManager.ConnectionStrings["MyConstring"].ConnectionString;
            MySqlConnection connection7 = new MySqlConnection(myconnection7);

            try
            {
                connection7.Open();
                cmd = new MySqlCommand();
                cmd.Connection = connection7;
                string query2 = "DELETE FROM sum_bonus";
                cmd.CommandText = query2;
                da = new MySqlDataAdapter();
                da.SelectCommand = cmd;
                dt = new DataTable();
                da.Fill(dt);


            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
            finally
            {
                connection7.Close();
            }

            //delete sum_bonus
            string myconnection8 = ConfigurationManager.ConnectionStrings["MyConstring"].ConnectionString;
            MySqlConnection connection8 = new MySqlConnection(myconnection7);

            try
            {
                connection7.Open();
                cmd = new MySqlCommand();
                cmd.Connection = connection7;
                string query2 = "DELETE FROM add_overtime";
                cmd.CommandText = query2;
                da = new MySqlDataAdapter();
                da.SelectCommand = cmd;
                dt = new DataTable();
                da.Fill(dt);


            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
            finally
            {
                connection7.Close();
            }


        }
        public void deduction_loan_balance()
        {


            string myconnection = ConfigurationManager.ConnectionStrings["MyConstring"].ConnectionString;
            MySqlConnection connection = new MySqlConnection(myconnection);

            try
            {
                connection.Open();
                cmd = new MySqlCommand();
                cmd.Connection = connection;
                cmd.CommandText = "UPDATE loan set comp_bal =  (comp_bal) - (comp_month_ded), sss_bal = (sss_bal) - (sss_month_ded), pag_ibig_bal = (pag_ibig_bal) - (pag_ibig_month_ded)";
                da = new MySqlDataAdapter();
                da.SelectCommand = cmd;
                dt = new DataTable();
                da.Fill(dt);


            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
            finally
            {
                connection.Close();
            }
        }
        public void check_balanced()
        {

            string myconnection = ConfigurationManager.ConnectionStrings["MyConstring"].ConnectionString;
            MySqlConnection connection = new MySqlConnection(myconnection);


            connection.Open();
            cmd = new MySqlCommand();
            cmd.Connection = connection;
            cmd.CommandText = "UPDATE loan SET comp_loan_amnt = 0, comp_month_ded = 0, comp_bal = 0 where comp_bal = 0";
            da = new MySqlDataAdapter();
            da.SelectCommand = cmd;
            dt = new DataTable();
            da.Fill(dt);
            connection.Close();




            connection.Open();
            cmd = new MySqlCommand();
            cmd.Connection = connection;
            cmd.CommandText = "UPDATE loan SET sss_loan_amnt = 0, sss_month_ded = 0, sss_bal = 0 where sss_bal = 0";
            da = new MySqlDataAdapter();
            da.SelectCommand = cmd;
            dt = new DataTable();
            da.Fill(dt);
            connection.Close();


            connection.Open();
            cmd = new MySqlCommand();
            cmd.Connection = connection;
            cmd.CommandText = "UPDATE loan SET pag_ibig_loan_amnt = 0, pag_ibig_month_ded = 0, pag_ibig_bal = 0 where pag_ibig_bal = 0";
            da = new MySqlDataAdapter();
            da.SelectCommand = cmd;
            dt = new DataTable();
            da.Fill(dt);
            connection.Close();
        }


        public void check_posting()
        {
            string myconnection = ConfigurationManager.ConnectionStrings["MyConstring"].ConnectionString;
            MySqlConnection connection = new MySqlConnection(myconnection);

            try
            {
                connection.Open();
                cmd = new MySqlCommand();
                cmd.Connection = connection;
                cmd.CommandText = "Select * FROM pay_trans";
                da = new MySqlDataAdapter();
                da.SelectCommand = cmd;
                dt = new DataTable();
                da.Fill(dt);

                for (int i = 0; i < dt.Rows.Count; i++){
                    
                    DataRow dr = dt.Rows[i];
                    
                    date_from_convert = (dr["date_from"].ToString());                  
                    date_to_convert = (dr["date_to"].ToString());
                    period = (dr["date_period"].ToString());
                }
            }
            catch (Exception ex){
                
                MessageBox.Show(ex.Message);
            }
            finally{
                connection.Close();
            }
        }

        public void validation_check_pay_details()
        {
            string myconnection = ConfigurationManager.ConnectionStrings["MyConstring"].ConnectionString;
            MySqlConnection connection = new MySqlConnection(myconnection);

            try
            {
                connection.Open();
                cmd = new MySqlCommand();
                cmd.Connection = connection;
                cmd.CommandText = "Select * FROM pay_details";
                da = new MySqlDataAdapter();
                da.SelectCommand = cmd;
                dt = new DataTable();
                da.Fill(dt);

                for (int i = 0; i < dt.Rows.Count; i++)
                {
                    DataRow dr = dt.Rows[i];

                    pay_details_month = (dr["yyyymm"].ToString());
                    pay_details_period = (dr["date_period"].ToString());
                }
            }
            catch (Exception ex)
            {

                MessageBox.Show(ex.Message);
            }
            finally
            {
                connection.Close();
            }
        }

        public void check_payroll_tranction()
        {
            string myconnection = ConfigurationManager.ConnectionStrings["MyConstring"].ConnectionString;
            MySqlConnection connection = new MySqlConnection(myconnection);

            try
            {
                connection.Open();
                cmd = new MySqlCommand();
                cmd.Connection = connection;
                cmd.CommandText = "Select * FROM pay_trans where process_tag = 'False'";
                da = new MySqlDataAdapter();
                da.SelectCommand = cmd;
                dt = new DataTable();
                da.Fill(dt);

                for (int i = 0; i < dt.Rows.Count; i++)
                {
                    DataRow dr = dt.Rows[i];

                    process_tag = (dr["process_tag"].ToString());

                }

            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
            finally
            {
                connection.Close();
            }
        }

        public void update_grossded_netpay()
        {

            string myconnection = ConfigurationManager.ConnectionStrings["MyConstring"].ConnectionString;
            MySqlConnection connection = new MySqlConnection(myconnection);

            try
            {
                connection.Open();
                cmd = new MySqlCommand();
                cmd.Connection = connection;
                cmd.CommandText = "UPDATE pay_trans left join salary on pay_trans.sal_id = salary.salary_id left join loan on pay_trans.loan_id = loan.loan_id SET pay_trans.gross_ded = salary.sss_ee + salary.pag_ibig_ee + salary.philhealth_ee + loan.comp_month_ded + loan.pag_ibig_month_ded + loan.sss_month_ded + pay_trans.other_ded + pay_trans.wtax, pay_trans.net_pay = pay_trans.gross_pay + pay_trans.deductions -  pay_trans.gross_ded where pay_trans.process_tag = 'False'";
                da = new MySqlDataAdapter();
                da.SelectCommand = cmd;
                dt = new DataTable();
                da.Fill(dt);

            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
            finally
            {
                connection.Close();
            }
        }

        public void update_gross_pay()
        {
            string myconnection = ConfigurationManager.ConnectionStrings["MyConstring"].ConnectionString;
            MySqlConnection connection = new MySqlConnection(myconnection);

            try
            {
                connection.Open();
                cmd = new MySqlCommand();
                cmd.Connection = connection;
                cmd.CommandText = "UPDATE pay_trans left join salary on pay_trans.sal_id = salary.salary_id SET pay_trans.gross_pay = salary.regular_pay  + pay_trans.ot_pay + pay_trans.other_inc where pay_trans.process_tag = 'False'";
                da = new MySqlDataAdapter();
                da.SelectCommand = cmd;
                dt = new DataTable();
                da.Fill(dt);


            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
            finally
            {
                connection.Close();
            }
        }

        public void update_posting_date()
        {
            string myconnection = ConfigurationManager.ConnectionStrings["MyConstring"].ConnectionString;
            MySqlConnection connection = new MySqlConnection(myconnection);

            try
            {
                connection.Open();
                cmd = new MySqlCommand();
                cmd.Connection = connection;
                cmd.CommandText = "UPDATE pay_trans set date = '" + label6.Text + "', date_from = '" + dateTimePicker2.Text + "', date_to = '" + dateTimePicker1.Text + "', date_period = '" + domainUpDown1.Text + "' where process_tag = 'False'";
                da = new MySqlDataAdapter();
                da.SelectCommand = cmd;
                dt = new DataTable();
                da.Fill(dt);


            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
            finally
            {
                connection.Close();
            }
        }
        public void annual_tax_inc()
        {

            int ctr2 = 24;


            string myconnection = ConfigurationManager.ConnectionStrings["MyConstring"].ConnectionString;
            MySqlConnection connection = new MySqlConnection(myconnection);

            try
            {
                connection.Open();
                cmd = new MySqlCommand();
                cmd.Connection = connection;
                cmd.CommandText = "Select * from pay_trans left join employee on pay_trans.emp_no = employee.emp_no left join salary on pay_trans.sal_id = salary.salary_id left join loan on pay_trans.loan_id = loan.loan_id LEFT JOIN sum_table on sum_table.emp_id = pay_trans.emp_id left join sum_bonus on sum_bonus.emp_id = pay_trans.emp_id where pay_trans.process_tag = 'False' and salary.emp_status = 'Regular'";
                da = new MySqlDataAdapter();
                da.SelectCommand = cmd;
                dt = new DataTable();
                da.Fill(dt);


                for (int i = 0; i < dt.Rows.Count; i++)
                {
                    DataRow dr = dt.Rows[i];

                    string name = (dr["lname"].ToString());
                    string pay_trans_id = (dr["pay_trans_id"].ToString());
                    string basic_pay = (dr["salary"].ToString());
                    string ot_pay = (dr["ot_pay"].ToString());
                    string other_income = (dr["other_inc"].ToString());
                    string comp_loan = (dr["comp_month_ded"].ToString());
                    string sss_loan = (dr["sss_month_ded"].ToString());
                    string pag_ibig_loan = (dr["pag_ibig_month_ded"].ToString());
                    string tax_subsidy = (dr["tax_sub"].ToString());
                    string bonus1 = (dr["13thmonthpay"].ToString());
                    string bonus2 = (dr["14thmonthpay"].ToString());
                    string sss_ee = (dr["sss_ee"].ToString());
                    string pag_ibig_ee = (dr["pag_ibig_ee"].ToString());
                    string philhealth_ee = (dr["philhealth_ee"].ToString());
                    string non_tax = (dr["mntbonus"].ToString());

                    string ctr = (dr["max_ctr"].ToString());
                    string bonus_net_pay = (dr["bonus_netpay"].ToString());
                    string gross_pay_details = (dr["sum_gross"].ToString());
                    string wtax_pay_details = (dr["sum_tax"].ToString());

                    decimal cont_month_sal = Convert.ToDecimal(basic_pay);
                    decimal cont_tax_sub = Convert.ToDecimal(tax_subsidy);
                    decimal cont_ot_pay = Convert.ToDecimal(ot_pay);
                    decimal cont_other_income = Convert.ToDecimal(other_income);
                    decimal cont_comp_loan = Convert.ToDecimal(comp_loan);
                    decimal cont_sss_loan = Convert.ToDecimal(sss_loan);
                    decimal cont_pag_ibig_loan = Convert.ToDecimal(pag_ibig_loan);
                    decimal cont_bonus1 = Convert.ToDecimal(bonus1);
                    decimal cont_bonus2 = Convert.ToDecimal(bonus2);
                    decimal cont_sss_ee = Convert.ToDecimal(sss_ee);
                    decimal cont_pag_ibig_ee = Convert.ToDecimal(pag_ibig_ee);
                    decimal cont_philhealth_ee = Convert.ToDecimal(philhealth_ee);
                    decimal cont_non_tax = Convert.ToDecimal(non_tax);

                    int ctr1 = Convert.ToInt32(ctr);

                    decimal total_gross_pay_details1 = Convert.ToDecimal(gross_pay_details);
                    decimal total_wtax_pay_details1 = Convert.ToDecimal(wtax_pay_details);
                    decimal total_bonus_netpay1 = Convert.ToDecimal(bonus_net_pay);

                    //total bonus
                    decimal total_bonus = cont_bonus1 + cont_bonus2;

                    //bonus - existing bonus of employee
                    decimal total_bonus2 = total_bonus - total_bonus_netpay1;

                    //counter
                    int total_ctr = ctr2 - ctr1;

                    //semi_monthly * counter
                    decimal semi_monthly = (cont_month_sal / 2) * total_ctr;

                    //semi_monthly_tax * 2
                    decimal semi_monthly_tax_sub = (cont_tax_sub / 2) * total_ctr;

                    //decimal ot pay / 24
                    decimal cal_ot_pay = cont_ot_pay / 24;


                    //decimal other income / 24

                    decimal cal_other_inc = cont_other_income / 24;


                    //overall_annual_income
                    decimal total_overall_income = semi_monthly + cal_ot_pay + cal_other_inc + semi_monthly_tax_sub;



                    //total_taxable_income
                    decimal total_taxable_income = total_gross_pay_details1 + total_overall_income + total_bonus2;



                    //sss,pag - ibig, philheath ded
                    decimal monthly_sss = cont_sss_ee * 2;
                    decimal total_annual_sss = monthly_sss * 12;



                    decimal monthly_pag_ibig = cont_pag_ibig_ee * 2;
                    decimal total_annual_pag_ibig = monthly_pag_ibig * 12;



                    decimal monthly_philhealth = cont_philhealth_ee * 2;
                    decimal total_annual_philhealth = monthly_philhealth * 12;


                    //Total cOntribution deduction
                    decimal total_ded = total_annual_sss + total_annual_pag_ibig + total_annual_philhealth;



                    //total deduction = non tax - sss,pag - ibig, philhealth
                    decimal overall_ded = cont_non_tax + total_ded;



                    //annual taxable income
                    decimal annual_taxable_income = total_taxable_income - overall_ded;



                    string myconnection4 = ConfigurationManager.ConnectionStrings["MyConstring"].ConnectionString;
                    MySqlConnection connection4 = new MySqlConnection(myconnection4);


                    string insertQuery3 = "INSERT INTO annual_tax_inc(pay_trans_id, annual_tax_inc, pay_details_wtax, max_ctr)Values('" + pay_trans_id + "', '" + annual_taxable_income + "', '" + total_wtax_pay_details1 + "', '" + total_ctr + "')";


                    connection4.Open();
                    MySqlCommand mySqlCommand3 = new MySqlCommand(insertQuery3, connection4);



                    if (mySqlCommand3.ExecuteNonQuery() == 1)
                    {
                        //MessageBox.Show("Successfully Process", "Payroll Processing", MessageBoxButtons.OK, MessageBoxIcon.Information);

                    }
                    connection4.Close();

                }

            }

            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
            finally
            {
                connection.Close();
            }
        }

        public void Regular_Tax()
        {

            //Monthly Tax Regular Employee

            string myconnection3 = ConfigurationManager.ConnectionStrings["MyConstring"].ConnectionString;
            MySqlConnection connection3 = new MySqlConnection(myconnection3);



            cmd = new MySqlCommand();
            cmd.Connection = connection3;
            cmd.CommandText = "SELECT * FROM tax join annual_tax_inc WHERE tax.min_amnt <= annual_tax_inc.annual_tax_inc AND tax.max_amnt >= annual_tax_inc.annual_tax_inc";
            da = new MySqlDataAdapter();
            da.SelectCommand = cmd;
            dt = new DataTable();
            da.Fill(dt);


            for (int x = 0; x < dt.Rows.Count; x++)
            {
                DataRow dr2 = dt.Rows[x];



                string tax_due = (dr2["tax_due"].ToString());
                string less_amnt = (dr2["less_amnt"].ToString());
                string percent = (dr2["percent"].ToString());
                string pay_trans_id = (dr2["pay_trans_id"].ToString());

                string annual_tax_income = (dr2["annual_tax_inc"].ToString());
                string wtax_pay_details = (dr2["pay_details_wtax"].ToString());
                string ctr = (dr2["max_ctr"].ToString());


                decimal temp_tax1 = Convert.ToDecimal(tax_due);
                decimal total_less_amnt = Convert.ToDecimal(less_amnt);
                decimal total_percentage = Convert.ToDecimal(percent);
                decimal total_annual_taxable_income = Convert.ToDecimal(annual_tax_income);
                decimal total_wtax_pay_details = Convert.ToDecimal(wtax_pay_details);
                int max_ctr = Convert.ToInt32(ctr);

                decimal get_percentage = total_percentage / 100;

                decimal temp_tax2 = (total_annual_taxable_income - total_less_amnt) * get_percentage;

                decimal total_annual_tax = temp_tax1 + temp_tax2;
                decimal annual_tax = total_annual_tax - total_wtax_pay_details;
                decimal semi_monthly_tax = annual_tax / max_ctr;

                string myconnection4 = ConfigurationManager.ConnectionStrings["MyConstring"].ConnectionString;
                MySqlConnection connection4 = new MySqlConnection(myconnection4);


                string insertQuery3 = "INSERT INTO monthly_tax(pay_trans_id, semi_monthly_tax)Values('" + pay_trans_id + "', '" + semi_monthly_tax + "')";


                connection4.Open();
                MySqlCommand mySqlCommand3 = new MySqlCommand(insertQuery3, connection4);



                if (mySqlCommand3.ExecuteNonQuery() == 1)
                {
                    //MessageBox.Show("Successfully Process", "Payroll Processing", MessageBoxButtons.OK, MessageBoxIcon.Information);

                }
                connection4.Close();


            }
            connection3.Close();
        }

        //public void Consultant_tax()
        //{
        //    string myconnection = ConfigurationManager.ConnectionStrings["MyConstring"].ConnectionString;
        //    MySqlConnection connection = new MySqlConnection(myconnection);

        //    string date_from_month = dateTimePicker2.Value.ToString("MMyyyy");

        //    connection.Open();
        //    cmd = new MySqlCommand();
        //    cmd.Connection = connection;
        //    cmd.CommandText = "SELECT  * FROM  pay_trans left join salary on pay_trans.sal_id = salary.salary_id left join employee on employee.emp_no = pay_trans.emp_no WHERE pay_trans.process_tag = 'False'  AND salary.emp_status = 'Consultant'";
        //    da = new MySqlDataAdapter();
        //    da.SelectCommand = cmd;
        //    dt = new DataTable();
        //    da.Fill(dt);

        //    for (int i = 0; i < dt.Rows.Count; i++)
        //    {
        //        DataRow dr = dt.Rows[i];

        //        string emp_no = (dr["pay_trans_id"].ToString());
        //        string salary = (dr["regular_pay"].ToString());
               


        //        decimal basic_pay = Convert.ToDecimal(salary);


        //        decimal total_tax = basic_pay * tax_per;


        //        string myconnection2 = ConfigurationManager.ConnectionStrings["MyConstring"].ConnectionString;
        //        MySqlConnection connection2 = new MySqlConnection(myconnection2);


        //        string insertQuery2 = "INSERT INTO monthly_tax(pay_trans_id, semi_monthly_tax)Values('" + emp_no + "',  '" + total_tax + "')";


        //        connection2.Open();
        //        MySqlCommand mySqlCommand2 = new MySqlCommand(insertQuery2, connection2);



        //        if (mySqlCommand2.ExecuteNonQuery() == 1)
        //        {
        //            //MessageBox.Show("Successfully Process", "Payroll Processing", MessageBoxButtons.OK, MessageBoxIcon.Information);

        //        }
        //        connection2.Close();


        //    }
        //    connection.Close();
        //}


        public void update_wtax()
        {
            string myconnection = ConfigurationManager.ConnectionStrings["MyConstring"].ConnectionString;
            MySqlConnection connection = new MySqlConnection(myconnection);



            try
            {
                connection.Open();
                cmd = new MySqlCommand();
                cmd.Connection = connection;
                cmd.CommandText = "UPDATE pay_trans SET pay_trans.wtax = (SELECT monthly_tax.semi_monthly_tax from monthly_tax WHERE monthly_tax.pay_trans_id = pay_trans.pay_trans_id) where pay_trans.process_tag = 'False'";
                da = new MySqlDataAdapter();
                da.SelectCommand = cmd;
                dt = new DataTable();
                da.Fill(dt);

            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
            finally
            {
                connection.Close();
            }


        }

        public void sum_table()
        {
            string myconnection = ConfigurationManager.ConnectionStrings["MyConstring"].ConnectionString;
            MySqlConnection connection = new MySqlConnection(myconnection);


            
            cmd = new MySqlCommand();
            cmd.Connection = connection;
            cmd.CommandText = "SELECT pay_trans.emp_id, SUM(pay_details.gross_inc) AS gross_pay_details, SUM(pay_details.wtax) AS wtax_pay_details, COALESCE (MAX(pay_details.ctr), 0) as pay_details_ctr FROM pay_trans LEFT JOIN pay_details on pay_trans.emp_id = pay_details.emp_id where pay_trans.process_tag = 'False' GROUP BY pay_trans.emp_id";
            da = new MySqlDataAdapter();
            da.SelectCommand = cmd;
            dt = new DataTable();
            da.Fill(dt);

            for (int i = 0; i < dt.Rows.Count; i++)
            {
                DataRow dr = dt.Rows[i];

                string emp_id = (dr["emp_id"].ToString());
                string ctr = (dr["pay_details_ctr"].ToString());
                string gross_pay_details = (dr["gross_pay_details"].ToString());
                string wtax_pay_details = (dr["wtax_pay_details"].ToString());

                if (wtax_pay_details == "")
                {
                    total_details_total_wtax = 0;
                }
                else
                {
                    total_details_total_wtax = Convert.ToDecimal(wtax_pay_details);

                }

                if (gross_pay_details == "")
                {
                    total_gross_pay_details = 0;

                }
                else
                {
                    total_gross_pay_details = Convert.ToDecimal(gross_pay_details);
                }
                string myconnection3 = ConfigurationManager.ConnectionStrings["MyConstring"].ConnectionString;
                MySqlConnection connection3 = new MySqlConnection(myconnection3);

               string insertQuery2 = "INSERT INTO sum_table(emp_id, sum_tax, sum_gross, max_ctr)Values('" + emp_id + "',  '" + total_details_total_wtax + "', '" + total_gross_pay_details + "', '" + ctr + "')";

               connection3.Open();
                MySqlCommand mySqlCommand2 = new MySqlCommand(insertQuery2, connection3);


                if (mySqlCommand2.ExecuteNonQuery() == 1)
                {
                    //MessageBox.Show("Successfully Process", "Payroll Processing", MessageBoxButtons.OK, MessageBoxIcon.Information);

                }
                connection3.Close();
            }

            connection.Close();
        }

        public void sum_bonus()
        {
            string myconnection = ConfigurationManager.ConnectionStrings["MyConstring"].ConnectionString;
            MySqlConnection connection = new MySqlConnection(myconnection);

            cmd = new MySqlCommand();
            cmd.Connection = connection;
            cmd.CommandText = "SELECT pay_trans.emp_id, SUM(bonus_details.netpay) as bonus_netpay FROM pay_trans LEFT JOIN bonus_details ON pay_trans.emp_id = bonus_details.emp_id WHERE process_tag = 'False' GROUP BY pay_trans.emp_id";
            da = new MySqlDataAdapter();
            da.SelectCommand = cmd;
            dt = new DataTable();
            da.Fill(dt);

            for (int i = 0; i < dt.Rows.Count; i++)
            {
                DataRow dr = dt.Rows[i];

                string emp_id = (dr["emp_id"].ToString());
                string bonus_details_netpay = (dr["bonus_netpay"].ToString());

                if (bonus_details_netpay == "")
                {
                    total_bonus_netpay = 0;
                }
                else
                {
                    total_bonus_netpay = Convert.ToDecimal(bonus_details_netpay);

                }

                string myconnection3 = ConfigurationManager.ConnectionStrings["MyConstring"].ConnectionString;
                MySqlConnection connection3 = new MySqlConnection(myconnection3);

                string insertQuery2 = "INSERT INTO sum_bonus(emp_id, bonus_netpay)Values('" + emp_id + "',  '" + total_bonus_netpay + "')";

                connection3.Open();
                MySqlCommand mySqlCommand2 = new MySqlCommand(insertQuery2, connection3);

                if (mySqlCommand2.ExecuteNonQuery() == 1)
                {
                    //MessageBox.Show("Successfully Process", "Payroll Processing", MessageBoxButtons.OK, MessageBoxIcon.Information);

                }
                connection3.Close();

               }
                connection.Close();
            }

        private void btn_post_transaction_Click(object sender, EventArgs e)
        {

        }
    }
    }
 
