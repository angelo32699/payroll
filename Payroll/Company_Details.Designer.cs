﻿namespace Payroll
{
    partial class Company_Details
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(Company_Details));
            this.tabControl1 = new System.Windows.Forms.TabControl();
            this.tabPage1 = new System.Windows.Forms.TabPage();
            this.btn_Edit = new Payroll.RJButtons.RJButton();
            this.label28 = new System.Windows.Forms.Label();
            this.label19 = new System.Windows.Forms.Label();
            this.label16 = new System.Windows.Forms.Label();
            this.cb_Weekly = new System.Windows.Forms.CheckBox();
            this.cb_SemiMonthly = new System.Windows.Forms.CheckBox();
            this.cb_Monthly = new System.Windows.Forms.CheckBox();
            this.label10 = new System.Windows.Forms.Label();
            this.label9 = new System.Windows.Forms.Label();
            this.tb_Philhealth = new System.Windows.Forms.TextBox();
            this.rtb_Address = new System.Windows.Forms.RichTextBox();
            this.tb_Approver = new System.Windows.Forms.TextBox();
            this.tb_Checker = new System.Windows.Forms.TextBox();
            this.tb_Maker = new System.Windows.Forms.TextBox();
            this.tb_SSS = new System.Windows.Forms.TextBox();
            this.tb_HDMF = new System.Windows.Forms.TextBox();
            this.tb_Tin = new System.Windows.Forms.TextBox();
            this.tb_Weblink = new System.Windows.Forms.TextBox();
            this.tb_ConPerson = new System.Windows.Forms.TextBox();
            this.tb_TelNum = new System.Windows.Forms.TextBox();
            this.tb_CompName = new System.Windows.Forms.TextBox();
            this.label14 = new System.Windows.Forms.Label();
            this.label13 = new System.Windows.Forms.Label();
            this.label12 = new System.Windows.Forms.Label();
            this.label11 = new System.Windows.Forms.Label();
            this.label24 = new System.Windows.Forms.Label();
            this.label23 = new System.Windows.Forms.Label();
            this.label22 = new System.Windows.Forms.Label();
            this.label21 = new System.Windows.Forms.Label();
            this.label20 = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.label15 = new System.Windows.Forms.Label();
            this.label1 = new System.Windows.Forms.Label();
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.rb_FactorDay3 = new System.Windows.Forms.RadioButton();
            this.rb_FactorDay1 = new System.Windows.Forms.RadioButton();
            this.rb_FactorDay4 = new System.Windows.Forms.RadioButton();
            this.rb_FactorDay2 = new System.Windows.Forms.RadioButton();
            this.tabPage2 = new System.Windows.Forms.TabPage();
            this.btn_Edit2 = new Payroll.RJButtons.RJButton();
            this.label17 = new System.Windows.Forms.Label();
            this.cb_UserType = new System.Windows.Forms.ComboBox();
            this.tb_name = new System.Windows.Forms.TextBox();
            this.label39 = new System.Windows.Forms.Label();
            this.tb_Password = new System.Windows.Forms.TextBox();
            this.tb_Username = new System.Windows.Forms.TextBox();
            this.tb_EmployeeID = new System.Windows.Forms.TextBox();
            this.label32 = new System.Windows.Forms.Label();
            this.label31 = new System.Windows.Forms.Label();
            this.label30 = new System.Windows.Forms.Label();
            this.tabControl1.SuspendLayout();
            this.tabPage1.SuspendLayout();
            this.groupBox1.SuspendLayout();
            this.tabPage2.SuspendLayout();
            this.SuspendLayout();
            // 
            // tabControl1
            // 
            this.tabControl1.Controls.Add(this.tabPage1);
            this.tabControl1.Controls.Add(this.tabPage2);
            this.tabControl1.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.tabControl1.Location = new System.Drawing.Point(6, 9);
            this.tabControl1.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.tabControl1.Name = "tabControl1";
            this.tabControl1.SelectedIndex = 0;
            this.tabControl1.Size = new System.Drawing.Size(1160, 779);
            this.tabControl1.TabIndex = 0;
            // 
            // tabPage1
            // 
            this.tabPage1.Controls.Add(this.btn_Edit);
            this.tabPage1.Controls.Add(this.label28);
            this.tabPage1.Controls.Add(this.label19);
            this.tabPage1.Controls.Add(this.label16);
            this.tabPage1.Controls.Add(this.cb_Weekly);
            this.tabPage1.Controls.Add(this.cb_SemiMonthly);
            this.tabPage1.Controls.Add(this.cb_Monthly);
            this.tabPage1.Controls.Add(this.label10);
            this.tabPage1.Controls.Add(this.label9);
            this.tabPage1.Controls.Add(this.tb_Philhealth);
            this.tabPage1.Controls.Add(this.rtb_Address);
            this.tabPage1.Controls.Add(this.tb_Approver);
            this.tabPage1.Controls.Add(this.tb_Checker);
            this.tabPage1.Controls.Add(this.tb_Maker);
            this.tabPage1.Controls.Add(this.tb_SSS);
            this.tabPage1.Controls.Add(this.tb_HDMF);
            this.tabPage1.Controls.Add(this.tb_Tin);
            this.tabPage1.Controls.Add(this.tb_Weblink);
            this.tabPage1.Controls.Add(this.tb_ConPerson);
            this.tabPage1.Controls.Add(this.tb_TelNum);
            this.tabPage1.Controls.Add(this.tb_CompName);
            this.tabPage1.Controls.Add(this.label14);
            this.tabPage1.Controls.Add(this.label13);
            this.tabPage1.Controls.Add(this.label12);
            this.tabPage1.Controls.Add(this.label11);
            this.tabPage1.Controls.Add(this.label24);
            this.tabPage1.Controls.Add(this.label23);
            this.tabPage1.Controls.Add(this.label22);
            this.tabPage1.Controls.Add(this.label21);
            this.tabPage1.Controls.Add(this.label20);
            this.tabPage1.Controls.Add(this.label4);
            this.tabPage1.Controls.Add(this.label3);
            this.tabPage1.Controls.Add(this.label2);
            this.tabPage1.Controls.Add(this.label15);
            this.tabPage1.Controls.Add(this.label1);
            this.tabPage1.Controls.Add(this.groupBox1);
            this.tabPage1.Location = new System.Drawing.Point(4, 31);
            this.tabPage1.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.tabPage1.Name = "tabPage1";
            this.tabPage1.Padding = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.tabPage1.Size = new System.Drawing.Size(1152, 744);
            this.tabPage1.TabIndex = 0;
            this.tabPage1.Text = "Company Setup";
            this.tabPage1.UseVisualStyleBackColor = true;
            this.tabPage1.Click += new System.EventHandler(this.tabPage1_Click);
            // 
            // btn_Edit
            // 
            this.btn_Edit.BackColor = System.Drawing.Color.Transparent;
            this.btn_Edit.BackgroundColor = System.Drawing.Color.Transparent;
            this.btn_Edit.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(192)))), ((int)(((byte)(0)))), ((int)(((byte)(0)))));
            this.btn_Edit.BorderRadius = 8;
            this.btn_Edit.BorderSize = 1;
            this.btn_Edit.FlatAppearance.BorderSize = 0;
            this.btn_Edit.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btn_Edit.Font = new System.Drawing.Font("Microsoft Sans Serif", 8F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btn_Edit.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(192)))), ((int)(((byte)(0)))), ((int)(((byte)(0)))));
            this.btn_Edit.Image = global::Payroll.Properties.Resources.Button_UPDATE_Red__4___1___1_;
            this.btn_Edit.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.btn_Edit.Location = new System.Drawing.Point(483, 673);
            this.btn_Edit.Name = "btn_Edit";
            this.btn_Edit.Size = new System.Drawing.Size(175, 55);
            this.btn_Edit.TabIndex = 182;
            this.btn_Edit.Text = "EDIT";
            this.btn_Edit.TextColor = System.Drawing.Color.FromArgb(((int)(((byte)(192)))), ((int)(((byte)(0)))), ((int)(((byte)(0)))));
            this.btn_Edit.UseVisualStyleBackColor = false;
            this.btn_Edit.Click += new System.EventHandler(this.btn_Edit_Click);
            this.btn_Edit.MouseEnter += new System.EventHandler(this.btn_Edit_MouseEnter);
            this.btn_Edit.MouseLeave += new System.EventHandler(this.btn_Edit_MouseLeave);
            this.btn_Edit.MouseHover += new System.EventHandler(this.btn_Edit_MouseHover);
            // 
            // label28
            // 
            this.label28.AutoSize = true;
            this.label28.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label28.Location = new System.Drawing.Point(553, 566);
            this.label28.Name = "label28";
            this.label28.Size = new System.Drawing.Size(69, 22);
            this.label28.TabIndex = 93;
            this.label28.Text = "Weekly";
            // 
            // label19
            // 
            this.label19.AutoSize = true;
            this.label19.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label19.Location = new System.Drawing.Point(549, 519);
            this.label19.Name = "label19";
            this.label19.Size = new System.Drawing.Size(118, 22);
            this.label19.TabIndex = 92;
            this.label19.Text = "Semi-Monthly";
            // 
            // label16
            // 
            this.label16.AutoSize = true;
            this.label16.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label16.Location = new System.Drawing.Point(553, 474);
            this.label16.Name = "label16";
            this.label16.Size = new System.Drawing.Size(72, 22);
            this.label16.TabIndex = 91;
            this.label16.Text = "Monthly";
            // 
            // cb_Weekly
            // 
            this.cb_Weekly.AutoSize = true;
            this.cb_Weekly.Enabled = false;
            this.cb_Weekly.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.cb_Weekly.Location = new System.Drawing.Point(518, 568);
            this.cb_Weekly.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.cb_Weekly.Name = "cb_Weekly";
            this.cb_Weekly.Size = new System.Drawing.Size(22, 21);
            this.cb_Weekly.TabIndex = 19;
            this.cb_Weekly.UseVisualStyleBackColor = true;
            // 
            // cb_SemiMonthly
            // 
            this.cb_SemiMonthly.AutoSize = true;
            this.cb_SemiMonthly.Enabled = false;
            this.cb_SemiMonthly.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.cb_SemiMonthly.Location = new System.Drawing.Point(518, 519);
            this.cb_SemiMonthly.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.cb_SemiMonthly.Name = "cb_SemiMonthly";
            this.cb_SemiMonthly.Size = new System.Drawing.Size(22, 21);
            this.cb_SemiMonthly.TabIndex = 18;
            this.cb_SemiMonthly.UseVisualStyleBackColor = true;
            // 
            // cb_Monthly
            // 
            this.cb_Monthly.AutoSize = true;
            this.cb_Monthly.Enabled = false;
            this.cb_Monthly.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.cb_Monthly.Location = new System.Drawing.Point(517, 474);
            this.cb_Monthly.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.cb_Monthly.Name = "cb_Monthly";
            this.cb_Monthly.Size = new System.Drawing.Size(22, 21);
            this.cb_Monthly.TabIndex = 17;
            this.cb_Monthly.UseVisualStyleBackColor = true;
            // 
            // label10
            // 
            this.label10.AutoSize = true;
            this.label10.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label10.Location = new System.Drawing.Point(513, 438);
            this.label10.Name = "label10";
            this.label10.Size = new System.Drawing.Size(121, 22);
            this.label10.TabIndex = 90;
            this.label10.Text = "Pay Schedule";
            // 
            // label9
            // 
            this.label9.AutoSize = true;
            this.label9.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label9.Location = new System.Drawing.Point(545, 408);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(0, 22);
            this.label9.TabIndex = 89;
            // 
            // tb_Philhealth
            // 
            this.tb_Philhealth.Enabled = false;
            this.tb_Philhealth.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.tb_Philhealth.Location = new System.Drawing.Point(220, 389);
            this.tb_Philhealth.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.tb_Philhealth.Name = "tb_Philhealth";
            this.tb_Philhealth.Size = new System.Drawing.Size(250, 28);
            this.tb_Philhealth.TabIndex = 8;
            // 
            // rtb_Address
            // 
            this.rtb_Address.Enabled = false;
            this.rtb_Address.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.rtb_Address.Location = new System.Drawing.Point(216, 62);
            this.rtb_Address.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.rtb_Address.Name = "rtb_Address";
            this.rtb_Address.Size = new System.Drawing.Size(254, 85);
            this.rtb_Address.TabIndex = 2;
            this.rtb_Address.Text = "";
            // 
            // tb_Approver
            // 
            this.tb_Approver.Enabled = false;
            this.tb_Approver.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.tb_Approver.Location = new System.Drawing.Point(216, 616);
            this.tb_Approver.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.tb_Approver.Name = "tb_Approver";
            this.tb_Approver.Size = new System.Drawing.Size(254, 28);
            this.tb_Approver.TabIndex = 12;
            // 
            // tb_Checker
            // 
            this.tb_Checker.Enabled = false;
            this.tb_Checker.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.tb_Checker.Location = new System.Drawing.Point(216, 566);
            this.tb_Checker.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.tb_Checker.Name = "tb_Checker";
            this.tb_Checker.Size = new System.Drawing.Size(254, 28);
            this.tb_Checker.TabIndex = 11;
            // 
            // tb_Maker
            // 
            this.tb_Maker.Enabled = false;
            this.tb_Maker.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.tb_Maker.Location = new System.Drawing.Point(217, 521);
            this.tb_Maker.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.tb_Maker.Name = "tb_Maker";
            this.tb_Maker.Size = new System.Drawing.Size(253, 28);
            this.tb_Maker.TabIndex = 10;
            // 
            // tb_SSS
            // 
            this.tb_SSS.Enabled = false;
            this.tb_SSS.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.tb_SSS.Location = new System.Drawing.Point(220, 340);
            this.tb_SSS.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.tb_SSS.Name = "tb_SSS";
            this.tb_SSS.Size = new System.Drawing.Size(250, 28);
            this.tb_SSS.TabIndex = 7;
            // 
            // tb_HDMF
            // 
            this.tb_HDMF.Enabled = false;
            this.tb_HDMF.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.tb_HDMF.Location = new System.Drawing.Point(216, 432);
            this.tb_HDMF.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.tb_HDMF.Name = "tb_HDMF";
            this.tb_HDMF.Size = new System.Drawing.Size(254, 28);
            this.tb_HDMF.TabIndex = 9;
            // 
            // tb_Tin
            // 
            this.tb_Tin.Enabled = false;
            this.tb_Tin.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.tb_Tin.Location = new System.Drawing.Point(217, 294);
            this.tb_Tin.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.tb_Tin.Name = "tb_Tin";
            this.tb_Tin.Size = new System.Drawing.Size(253, 28);
            this.tb_Tin.TabIndex = 6;
            // 
            // tb_Weblink
            // 
            this.tb_Weblink.Enabled = false;
            this.tb_Weblink.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.tb_Weblink.Location = new System.Drawing.Point(217, 249);
            this.tb_Weblink.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.tb_Weblink.Name = "tb_Weblink";
            this.tb_Weblink.Size = new System.Drawing.Size(253, 28);
            this.tb_Weblink.TabIndex = 5;
            // 
            // tb_ConPerson
            // 
            this.tb_ConPerson.Enabled = false;
            this.tb_ConPerson.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.tb_ConPerson.Location = new System.Drawing.Point(220, 202);
            this.tb_ConPerson.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.tb_ConPerson.Name = "tb_ConPerson";
            this.tb_ConPerson.Size = new System.Drawing.Size(250, 28);
            this.tb_ConPerson.TabIndex = 4;
            // 
            // tb_TelNum
            // 
            this.tb_TelNum.Enabled = false;
            this.tb_TelNum.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.tb_TelNum.Location = new System.Drawing.Point(220, 161);
            this.tb_TelNum.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.tb_TelNum.Name = "tb_TelNum";
            this.tb_TelNum.Size = new System.Drawing.Size(250, 28);
            this.tb_TelNum.TabIndex = 3;
            // 
            // tb_CompName
            // 
            this.tb_CompName.Enabled = false;
            this.tb_CompName.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.tb_CompName.Location = new System.Drawing.Point(220, 22);
            this.tb_CompName.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.tb_CompName.Name = "tb_CompName";
            this.tb_CompName.Size = new System.Drawing.Size(250, 28);
            this.tb_CompName.TabIndex = 1;
            // 
            // label14
            // 
            this.label14.AutoSize = true;
            this.label14.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label14.Location = new System.Drawing.Point(60, 604);
            this.label14.Name = "label14";
            this.label14.Size = new System.Drawing.Size(88, 22);
            this.label14.TabIndex = 76;
            this.label14.Text = "Approver:";
            // 
            // label13
            // 
            this.label13.AutoSize = true;
            this.label13.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label13.Location = new System.Drawing.Point(60, 558);
            this.label13.Name = "label13";
            this.label13.Size = new System.Drawing.Size(82, 22);
            this.label13.TabIndex = 75;
            this.label13.Text = "Checker:";
            // 
            // label12
            // 
            this.label12.AutoSize = true;
            this.label12.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label12.Location = new System.Drawing.Point(60, 521);
            this.label12.Name = "label12";
            this.label12.Size = new System.Drawing.Size(133, 22);
            this.label12.TabIndex = 74;
            this.label12.Text = "Prepare/Maker:";
            // 
            // label11
            // 
            this.label11.AutoSize = true;
            this.label11.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label11.Location = new System.Drawing.Point(46, 474);
            this.label11.Name = "label11";
            this.label11.Size = new System.Drawing.Size(185, 22);
            this.label11.TabIndex = 73;
            this.label11.Text = "Payroll Signatories:";
            // 
            // label24
            // 
            this.label24.AutoSize = true;
            this.label24.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label24.Location = new System.Drawing.Point(107, 434);
            this.label24.Name = "label24";
            this.label24.Size = new System.Drawing.Size(66, 22);
            this.label24.TabIndex = 72;
            this.label24.Text = "HDMF:";
            // 
            // label23
            // 
            this.label23.AutoSize = true;
            this.label23.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label23.Location = new System.Drawing.Point(91, 342);
            this.label23.Name = "label23";
            this.label23.Size = new System.Drawing.Size(51, 22);
            this.label23.TabIndex = 71;
            this.label23.Text = "SSS:";
            // 
            // label22
            // 
            this.label22.AutoSize = true;
            this.label22.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label22.Location = new System.Drawing.Point(80, 395);
            this.label22.Name = "label22";
            this.label22.Size = new System.Drawing.Size(94, 22);
            this.label22.TabIndex = 70;
            this.label22.Text = "Philhealth:";
            // 
            // label21
            // 
            this.label21.AutoSize = true;
            this.label21.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label21.Location = new System.Drawing.Point(104, 300);
            this.label21.Name = "label21";
            this.label21.Size = new System.Drawing.Size(44, 22);
            this.label21.TabIndex = 69;
            this.label21.Text = "TIN:";
            // 
            // label20
            // 
            this.label20.AutoSize = true;
            this.label20.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label20.Location = new System.Drawing.Point(68, 249);
            this.label20.Name = "label20";
            this.label20.Size = new System.Drawing.Size(80, 22);
            this.label20.TabIndex = 68;
            this.label20.Text = "Website:";
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label4.Location = new System.Drawing.Point(25, 210);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(139, 22);
            this.label4.TabIndex = 67;
            this.label4.Text = "Contact Person:";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label3.Location = new System.Drawing.Point(25, 168);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(169, 22);
            this.label3.TabIndex = 65;
            this.label3.Text = "Telephone Number:";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label2.Location = new System.Drawing.Point(51, 68);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(81, 22);
            this.label2.TabIndex = 64;
            this.label2.Text = "Address:";
            // 
            // label15
            // 
            this.label15.AutoSize = true;
            this.label15.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label15.Location = new System.Drawing.Point(46, 22);
            this.label15.Name = "label15";
            this.label15.Size = new System.Drawing.Size(143, 22);
            this.label15.TabIndex = 60;
            this.label15.Text = "Company Name:";
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label1.Location = new System.Drawing.Point(45, 22);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(143, 22);
            this.label1.TabIndex = 61;
            this.label1.Text = "Company Name:";
            // 
            // groupBox1
            // 
            this.groupBox1.Controls.Add(this.rb_FactorDay3);
            this.groupBox1.Controls.Add(this.rb_FactorDay1);
            this.groupBox1.Controls.Add(this.rb_FactorDay4);
            this.groupBox1.Controls.Add(this.rb_FactorDay2);
            this.groupBox1.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.groupBox1.Location = new System.Drawing.Point(505, 22);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Size = new System.Drawing.Size(632, 395);
            this.groupBox1.TabIndex = 99;
            this.groupBox1.TabStop = false;
            this.groupBox1.Text = "Factor Days in a Year (For Monthly Rate Employees)";
            // 
            // rb_FactorDay3
            // 
            this.rb_FactorDay3.AutoSize = true;
            this.rb_FactorDay3.Enabled = false;
            this.rb_FactorDay3.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.rb_FactorDay3.Location = new System.Drawing.Point(24, 227);
            this.rb_FactorDay3.Name = "rb_FactorDay3";
            this.rb_FactorDay3.Size = new System.Drawing.Size(440, 48);
            this.rb_FactorDay3.TabIndex = 14;
            this.rb_FactorDay3.TabStop = true;
            this.rb_FactorDay3.Text = "287 - Employees who do not work and are not paid\r\n for one and a half rest day pe" +
    "r week";
            this.rb_FactorDay3.UseVisualStyleBackColor = true;
            // 
            // rb_FactorDay1
            // 
            this.rb_FactorDay1.AutoSize = true;
            this.rb_FactorDay1.Enabled = false;
            this.rb_FactorDay1.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.rb_FactorDay1.Location = new System.Drawing.Point(24, 46);
            this.rb_FactorDay1.Name = "rb_FactorDay1";
            this.rb_FactorDay1.Size = new System.Drawing.Size(544, 70);
            this.rb_FactorDay1.TabIndex = 13;
            this.rb_FactorDay1.TabStop = true;
            this.rb_FactorDay1.Text = "365 – Employees who are paid every day of the month, including\r\nunworked, rest da" +
    "ys, special days, and regular days.\r\n\r\n";
            this.rb_FactorDay1.TextAlign = System.Drawing.ContentAlignment.TopCenter;
            this.rb_FactorDay1.UseVisualStyleBackColor = true;
            // 
            // rb_FactorDay4
            // 
            this.rb_FactorDay4.AutoSize = true;
            this.rb_FactorDay4.Enabled = false;
            this.rb_FactorDay4.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.rb_FactorDay4.Location = new System.Drawing.Point(24, 318);
            this.rb_FactorDay4.Name = "rb_FactorDay4";
            this.rb_FactorDay4.Size = new System.Drawing.Size(552, 48);
            this.rb_FactorDay4.TabIndex = 16;
            this.rb_FactorDay4.TabStop = true;
            this.rb_FactorDay4.Text = "261: Employees who do not work and are not considered paid on\r\ntwo rest days per " +
    "week.";
            this.rb_FactorDay4.UseVisualStyleBackColor = true;
            // 
            // rb_FactorDay2
            // 
            this.rb_FactorDay2.AutoSize = true;
            this.rb_FactorDay2.Enabled = false;
            this.rb_FactorDay2.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.rb_FactorDay2.Location = new System.Drawing.Point(22, 138);
            this.rb_FactorDay2.Name = "rb_FactorDay2";
            this.rb_FactorDay2.Size = new System.Drawing.Size(521, 48);
            this.rb_FactorDay2.TabIndex = 15;
            this.rb_FactorDay2.TabStop = true;
            this.rb_FactorDay2.Text = "313: Employees who do not work and not considered paid on\r\none rest days per week" +
    ".\r\n";
            this.rb_FactorDay2.UseVisualStyleBackColor = true;
            // 
            // tabPage2
            // 
            this.tabPage2.Controls.Add(this.btn_Edit2);
            this.tabPage2.Controls.Add(this.label17);
            this.tabPage2.Controls.Add(this.cb_UserType);
            this.tabPage2.Controls.Add(this.tb_name);
            this.tabPage2.Controls.Add(this.label39);
            this.tabPage2.Controls.Add(this.tb_Password);
            this.tabPage2.Controls.Add(this.tb_Username);
            this.tabPage2.Controls.Add(this.tb_EmployeeID);
            this.tabPage2.Controls.Add(this.label32);
            this.tabPage2.Controls.Add(this.label31);
            this.tabPage2.Controls.Add(this.label30);
            this.tabPage2.Location = new System.Drawing.Point(4, 31);
            this.tabPage2.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.tabPage2.Name = "tabPage2";
            this.tabPage2.Padding = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.tabPage2.Size = new System.Drawing.Size(1152, 744);
            this.tabPage2.TabIndex = 1;
            this.tabPage2.Text = "User Setup";
            this.tabPage2.UseVisualStyleBackColor = true;
            // 
            // btn_Edit2
            // 
            this.btn_Edit2.BackColor = System.Drawing.Color.Transparent;
            this.btn_Edit2.BackgroundColor = System.Drawing.Color.Transparent;
            this.btn_Edit2.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(192)))), ((int)(((byte)(0)))), ((int)(((byte)(0)))));
            this.btn_Edit2.BorderRadius = 8;
            this.btn_Edit2.BorderSize = 1;
            this.btn_Edit2.FlatAppearance.BorderSize = 0;
            this.btn_Edit2.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btn_Edit2.Font = new System.Drawing.Font("Microsoft Sans Serif", 8F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btn_Edit2.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(192)))), ((int)(((byte)(0)))), ((int)(((byte)(0)))));
            this.btn_Edit2.Image = global::Payroll.Properties.Resources.Button_UPDATE_Red__4___1___1_;
            this.btn_Edit2.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.btn_Edit2.Location = new System.Drawing.Point(489, 660);
            this.btn_Edit2.Name = "btn_Edit2";
            this.btn_Edit2.Size = new System.Drawing.Size(175, 55);
            this.btn_Edit2.TabIndex = 183;
            this.btn_Edit2.Text = "EDIT";
            this.btn_Edit2.TextColor = System.Drawing.Color.FromArgb(((int)(((byte)(192)))), ((int)(((byte)(0)))), ((int)(((byte)(0)))));
            this.btn_Edit2.UseVisualStyleBackColor = false;
            this.btn_Edit2.Click += new System.EventHandler(this.btn_Edit2_Click);
            this.btn_Edit2.MouseEnter += new System.EventHandler(this.btn_Edit2_MouseEnter);
            this.btn_Edit2.MouseLeave += new System.EventHandler(this.btn_Edit2_MouseLeave);
            this.btn_Edit2.MouseHover += new System.EventHandler(this.btn_Edit2_MouseHover);
            // 
            // label17
            // 
            this.label17.AutoSize = true;
            this.label17.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label17.Location = new System.Drawing.Point(35, 40);
            this.label17.Name = "label17";
            this.label17.Size = new System.Drawing.Size(99, 22);
            this.label17.TabIndex = 97;
            this.label17.Text = "User Type:";
            // 
            // cb_UserType
            // 
            this.cb_UserType.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cb_UserType.Enabled = false;
            this.cb_UserType.FormattingEnabled = true;
            this.cb_UserType.Location = new System.Drawing.Point(209, 34);
            this.cb_UserType.Name = "cb_UserType";
            this.cb_UserType.Size = new System.Drawing.Size(309, 30);
            this.cb_UserType.TabIndex = 96;
            this.cb_UserType.SelectedIndexChanged += new System.EventHandler(this.cb_UserType_SelectedIndexChanged);
            // 
            // tb_name
            // 
            this.tb_name.Enabled = false;
            this.tb_name.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.tb_name.Location = new System.Drawing.Point(209, 80);
            this.tb_name.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.tb_name.Name = "tb_name";
            this.tb_name.Size = new System.Drawing.Size(309, 28);
            this.tb_name.TabIndex = 1;
            // 
            // label39
            // 
            this.label39.AutoSize = true;
            this.label39.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label39.Location = new System.Drawing.Point(32, 175);
            this.label39.Name = "label39";
            this.label39.Size = new System.Drawing.Size(97, 22);
            this.label39.TabIndex = 78;
            this.label39.Text = "Username:";
            // 
            // tb_Password
            // 
            this.tb_Password.Enabled = false;
            this.tb_Password.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.tb_Password.Location = new System.Drawing.Point(209, 222);
            this.tb_Password.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.tb_Password.Name = "tb_Password";
            this.tb_Password.Size = new System.Drawing.Size(309, 28);
            this.tb_Password.TabIndex = 4;
            // 
            // tb_Username
            // 
            this.tb_Username.Enabled = false;
            this.tb_Username.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.tb_Username.Location = new System.Drawing.Point(209, 173);
            this.tb_Username.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.tb_Username.Name = "tb_Username";
            this.tb_Username.Size = new System.Drawing.Size(309, 28);
            this.tb_Username.TabIndex = 3;
            // 
            // tb_EmployeeID
            // 
            this.tb_EmployeeID.Enabled = false;
            this.tb_EmployeeID.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.tb_EmployeeID.Location = new System.Drawing.Point(209, 124);
            this.tb_EmployeeID.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.tb_EmployeeID.Name = "tb_EmployeeID";
            this.tb_EmployeeID.Size = new System.Drawing.Size(309, 28);
            this.tb_EmployeeID.TabIndex = 2;
            // 
            // label32
            // 
            this.label32.AutoSize = true;
            this.label32.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label32.Location = new System.Drawing.Point(32, 218);
            this.label32.Name = "label32";
            this.label32.Size = new System.Drawing.Size(94, 22);
            this.label32.TabIndex = 71;
            this.label32.Text = "Password:";
            // 
            // label31
            // 
            this.label31.AutoSize = true;
            this.label31.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label31.Location = new System.Drawing.Point(32, 124);
            this.label31.Name = "label31";
            this.label31.Size = new System.Drawing.Size(111, 22);
            this.label31.TabIndex = 70;
            this.label31.Text = "Employee ID";
            // 
            // label30
            // 
            this.label30.AutoSize = true;
            this.label30.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label30.Location = new System.Drawing.Point(32, 84);
            this.label30.Name = "label30";
            this.label30.Size = new System.Drawing.Size(62, 22);
            this.label30.TabIndex = 69;
            this.label30.Text = "Name:";
            // 
            // Company_Details
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(9F, 20F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.AutoSize = true;
            this.ClientSize = new System.Drawing.Size(1172, 789);
            this.Controls.Add(this.tabControl1);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedToolWindow;
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.Name = "Company_Details";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Company Details";
            this.Load += new System.EventHandler(this.Company_Details_Load);
            this.tabControl1.ResumeLayout(false);
            this.tabPage1.ResumeLayout(false);
            this.tabPage1.PerformLayout();
            this.groupBox1.ResumeLayout(false);
            this.groupBox1.PerformLayout();
            this.tabPage2.ResumeLayout(false);
            this.tabPage2.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.TabControl tabControl1;
        private System.Windows.Forms.TabPage tabPage1;
        private System.Windows.Forms.TabPage tabPage2;
        private System.Windows.Forms.TextBox tb_Philhealth;
        private System.Windows.Forms.RichTextBox rtb_Address;
        private System.Windows.Forms.TextBox tb_SSS;
        private System.Windows.Forms.TextBox tb_HDMF;
        private System.Windows.Forms.TextBox tb_Tin;
        private System.Windows.Forms.TextBox tb_Weblink;
        private System.Windows.Forms.TextBox tb_ConPerson;
        private System.Windows.Forms.TextBox tb_TelNum;
        private System.Windows.Forms.TextBox tb_CompName;
        private System.Windows.Forms.Label label14;
        private System.Windows.Forms.Label label13;
        private System.Windows.Forms.Label label12;
        private System.Windows.Forms.Label label11;
        private System.Windows.Forms.Label label24;
        private System.Windows.Forms.Label label23;
        private System.Windows.Forms.Label label22;
        private System.Windows.Forms.Label label21;
        private System.Windows.Forms.Label label20;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label15;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label28;
        private System.Windows.Forms.Label label19;
        private System.Windows.Forms.Label label16;
        private System.Windows.Forms.CheckBox cb_Weekly;
        private System.Windows.Forms.CheckBox cb_SemiMonthly;
        private System.Windows.Forms.CheckBox cb_Monthly;
        private System.Windows.Forms.Label label10;
        private System.Windows.Forms.Label label39;
        private System.Windows.Forms.TextBox tb_Password;
        private System.Windows.Forms.TextBox tb_Username;
        private System.Windows.Forms.TextBox tb_EmployeeID;
        private System.Windows.Forms.Label label32;
        private System.Windows.Forms.Label label31;
        private System.Windows.Forms.Label label30;
        private System.Windows.Forms.TextBox tb_Approver;
        private System.Windows.Forms.TextBox tb_Checker;
        private System.Windows.Forms.TextBox tb_Maker;
        private System.Windows.Forms.TextBox tb_name;
        private System.Windows.Forms.Label label17;
        private System.Windows.Forms.ComboBox cb_UserType;
        private System.Windows.Forms.RadioButton rb_FactorDay2;
        private System.Windows.Forms.RadioButton rb_FactorDay1;
        private System.Windows.Forms.GroupBox groupBox1;
        private System.Windows.Forms.RadioButton rb_FactorDay4;
        private System.Windows.Forms.RadioButton rb_FactorDay3;
        private System.Windows.Forms.Label label9;
        private RJButtons.RJButton btn_Edit;
        private RJButtons.RJButton btn_Edit2;
    }
}