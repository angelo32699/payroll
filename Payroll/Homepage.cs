﻿using MySql.Data.MySqlClient;
using Payroll.Reports;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Configuration;
using System.Data;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Security.Policy;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Payroll
{

    public partial class Homepage : Form
    {
        MySqlCommand cmd;
        MySqlDataAdapter da;
        DataTable dt;

        public string user_id { get; set; }

        public string username { get; set; }


        public Homepage()
        {
            InitializeComponent();
            menuStrip1.Renderer = new MyRenderer();
        }

        private void sSSTableToolStripMenuItem_Click(object sender, EventArgs e)
        {
            SSS sss = new SSS();
            sss.ShowDialog();
        }

        private void taxTableToolStripMenuItem_Click(object sender, EventArgs e)
        {
            Tax tax = new Tax();
            tax.ShowDialog();
        }

        private void philhealthTableToolStripMenuItem_Click(object sender, EventArgs e)
        {
            Philhealth philhealth = new Philhealth();
            philhealth.ShowDialog();
        }

        private void pagIbigHDMFToolStripMenuItem_Click(object sender, EventArgs e)
        {
            Pag_ibig pag_Ibig = new Pag_ibig();
            pag_Ibig.ShowDialog();

        }


        private void overtimeToolStripMenuItem_Click(object sender, EventArgs e)
        {

            Overtime overtime = new Overtime();
            overtime.ShowDialog();
        }

        private void postingtransactionToolStripMenuItem_Click(object sender, EventArgs e)
        {
            Posting_Transaction posting_Transaction = new Posting_Transaction();
            posting_Transaction.ShowDialog();
        }

        private void payrollToolStripMenuItem_Click(object sender, EventArgs e)
        {

            Payroll_Transaction payroll_Transaction = new Payroll_Transaction();
            payroll_Transaction.ShowDialog();

        }

        private void payDetailsToolStripMenuItem_Click(object sender, EventArgs e)
        {
            Payroll_Processing payroll_Processing = new Payroll_Processing();
            payroll_Processing.ShowDialog();
        }

        public void Homepage_Load(object sender, EventArgs e)
        {
            lbl_time.Text = DateTime.Now.ToLongDateString();
            lbl_date.Text = DateTime.Now.ToLongDateString();
            Login login = new Login();
            lbl_user.Text = username;

            Display_Company_Details();
            Display_User_Type();
            Disable_Item();

            SetValuesOnSubItems(this.menuStrip1.Items.OfType<ToolStripMenuItem>().ToList());


            panel8.Left = (this.ClientSize.Width - panel8.Width) / 2;
            panel8.Top = (this.ClientSize.Height - panel8.Height) / 2;

        }
        private void Disable_Item()
        {
            if (lbl_UserType.Text == "Administrator")
            {
                DepartmentItem.Enabled = false;
                TaxTableItem.Enabled = false;
                SSSTableItem.Enabled = false;
                PhilhealthTableItem.Enabled = false;
                HDMFTableItem.Enabled = false;
                EmployeeMasterFileItem.Enabled = false;
                PostingTransactionItem.Enabled = false;
                PayrollTransactionItem.Enabled = false;
                PayrollProcessingItem.Enabled = false;
                BonusProcessItem.Enabled = false;
                BonusRegisterItem.Enabled = false;
                PayslipReportItem.Enabled = false;
                PayrollRegisterItem.Enabled = false;
                RegisterMonthlyReportItem.Enabled = false;
                BankRemittanceReportItem.Enabled = false;
                BonusRemittanceItem.Enabled = false;
                YearEndProcessItem.Enabled = false;
                BIRAphalistItem.Enabled = false;

            } else
            {
                SystemSetupItem.Enabled = false;
                AuditLogItem.Enabled = false;
                OvertimeTableItem.Enabled = false;
            }
        }

        private void timer1_Tick(object sender, EventArgs e)
        {
            lbl_time.Text = DateTime.Now.ToLongTimeString();
            timer1.Start();
        }


        private void payrollDetailToolStripMenuItem_Click(object sender, EventArgs e)
        {
            Payroll_Details payroll_Details = new Payroll_Details();
            payroll_Details.ShowDialog();
        }

        private void payrollMonthToolStripMenuItem_Click(object sender, EventArgs e)
        {
            Payroll_Month payroll_Month = new Payroll_Month();
            payroll_Month.ShowDialog();
        }

        private void payslipToolStripMenuItem_Click(object sender, EventArgs e)
        {
            Print_Payslip print_Payslip = new Print_Payslip();
            print_Payslip.ShowDialog();
        }



        private void departmentToolStripMenuItem_Click(object sender, EventArgs e)
        {
            Department department = new Department();
            department.ShowDialog();
        }

        private void bankRemittanceToolStripMenuItem_Click(object sender, EventArgs e)
        {
            Bank_Remittance bank_Remittance = new Bank_Remittance();
            bank_Remittance.ShowDialog();
        }

        private void nTHMONTHPROCESSToolStripMenuItem_Click(object sender, EventArgs e)
        {
            Bonus bonus = new Bonus();
            bonus.ShowDialog();
        }

        private void bonusRemittanceToolStripMenuItem_Click(object sender, EventArgs e)
        {
            Bonus_Report bonus_Report = new Bonus_Report();
            bonus_Report.ShowDialog();
        }

        private void postingBonusToolStripMenuItem_Click(object sender, EventArgs e)
        {
            Bonus_Transaction bonus_Transaction = new Bonus_Transaction();
            bonus_Transaction.ShowDialog();
        }

        private void yearEndProcessToolStripMenuItem_Click(object sender, EventArgs e)
        {
            Year_End yearEnd = new Year_End();
            yearEnd.ShowDialog();
        }

        private void lbl_logout_Click(object sender, EventArgs e)
        {
            DialogResult result = MessageBox.Show("Are you sure you want to Logout?", "Logout", MessageBoxButtons.YesNo, MessageBoxIcon.Question);
            if (result == DialogResult.Yes)
            {

                Insert_Audit_Logs();

                this.Hide();
                Login login = new Login();
                login.ShowDialog();

            }
        }

        private void Insert_Audit_Logs()
        {
            string myconnection = ConfigurationManager.ConnectionStrings["MyConstring"].ConnectionString;
            MySqlConnection connection = new MySqlConnection(myconnection);

            string date_today = DateTime.Now.ToString("M/d/yyyy hh:mm tt");

            String insertQuery = "INSERT INTO audit_logs(user_id, tran_date, transaction) VALUES('" + user_id + "', '" + date_today + "', '" + "Successfully Logout." + "')";

            connection.Open();
            cmd = new MySqlCommand(insertQuery, connection);

            try
            {
                if (cmd.ExecuteNonQuery() == 1)
                {
                    //MessageBox.Show("Successfully Add Information", "Add Information", MessageBoxButtons.OK, MessageBoxIcon.Information);
                }

            }
            catch (Exception ex)
            {
                MessageBox.Show("Error" + ex);
            }

            connection.Close();
        }

        private void auditLogToolStripMenuItem1_Click(object sender, EventArgs e)
        {
            Audit_Log audit_Log = new Audit_Log();
            audit_Log.ShowDialog();
        }

        private void postingTransactionToolStripMenuItem1_Click(object sender, EventArgs e)
        {
            Posting_Transaction posting_Transaction = new Posting_Transaction();
            posting_Transaction.ShowDialog();
        }

        private void payrollTransactionToolStripMenuItem_Click(object sender, EventArgs e)
        {
            Payroll_Transaction payroll_Transaction = new Payroll_Transaction();
              payroll_Transaction.ShowDialog();
            
        }

        private void payrollProcessingToolStripMenuItem_Click(object sender, EventArgs e)
        {
            Payroll_Processing payroll_Processing = new Payroll_Processing();
            payroll_Processing.ShowDialog();
        }

        private void monToolStripMenuItem_Click(object sender, EventArgs e)
        {
            Bonus bonus = new Bonus();
            bonus.ShowDialog();
        }

        private void bonusRegisterToolStripMenuItem_Click(object sender, EventArgs e)
        {
            Bonus_Transaction bonus_Transaction = new Bonus_Transaction();
            bonus_Transaction.ShowDialog();
        }

        private void payslipToolStripMenuItem1_Click(object sender, EventArgs e)
        {
           Print_Payslip print_Payslip = new Print_Payslip();
            print_Payslip.ShowDialog();
            
        }

        private void payrollRegisterToolStripMenuItem_Click(object sender, EventArgs e)
        {
            Payroll_Details payroll_Details = new Payroll_Details();
            payroll_Details.ShowDialog();
        }

        private void companyUsersToolStripMenuItem_Click(object sender, EventArgs e)
        {
            
                Company_Details company_Details = new Company_Details();
                company_Details.ShowDialog();
           
        }

        public void Display_User_Type()
        {
            string myconnection = ConfigurationManager.ConnectionStrings["MyConstring"].ConnectionString;
            MySqlConnection connection = new MySqlConnection(myconnection);

            try
            {
                connection.Open();
                cmd = new MySqlCommand();
                cmd.Connection = connection;
                cmd.CommandText = "SELECT * FROM `user_account` LEFT JOIN user_type ON user_account.type_id = user_type.type_id WHERE username = '"+lbl_user.Text+"'";
                da = new MySqlDataAdapter();
                da.SelectCommand = cmd;
                dt = new DataTable();
                da.Fill(dt);

                for (int i = 0; i < dt.Rows.Count; i++)
                {
                    DataRow dr = dt.Rows[i];

                    lbl_UserType.Text = (dr["user_type"].ToString());

                }

            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
            finally
            {
                connection.Close();
            }
        }

        public void Display_Company_Details()
        {
            string myconnection = ConfigurationManager.ConnectionStrings["MyConstring"].ConnectionString;
            MySqlConnection connection = new MySqlConnection(myconnection);

            try
            {
                connection.Open();
                cmd = new MySqlCommand();
                cmd.Connection = connection;
                cmd.CommandText = "Select * FROM comp_details";
                da = new MySqlDataAdapter();
                da.SelectCommand = cmd;
                dt = new DataTable();
                da.Fill(dt);

                for (int i = 0; i < dt.Rows.Count; i++)
                {
                    DataRow dr = dt.Rows[i];

                    lbl_CompName.Text = (dr["comp_name"].ToString());
                   
                }

            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
            finally
            {
                connection.Close();
            }

        }





        private void userManagementToolStripMenuItem_Click(object sender, EventArgs e)
        {
            
                Audit_Log audit_Log = new Audit_Log();
                audit_Log.ShowDialog();
                        
        }

        private void panel7_Paint(object sender, PaintEventArgs e)
        {

        }
        private class MyRenderer : ToolStripProfessionalRenderer
        {
            public MyRenderer() : base(new MyColors()) { }
        }

        private class MyColors : ProfessionalColorTable
        {
            public override Color MenuBorder
            {
                get { return ColorTranslator.FromHtml("#FFAFAF"); }
            }

            public override Color MenuItemPressedGradientBegin
            {
                get { return ColorTranslator.FromHtml("#FFAFAF"); }
            }
            public override Color MenuItemPressedGradientEnd
            {
                get { return ColorTranslator.FromHtml("#FFAFAF"); }
            }

            public override Color ToolStripBorder
            {
                get { return ColorTranslator.FromHtml("#FFAFAF"); }
            }

            public override Color MenuItemSelectedGradientBegin
            {
                get { return ColorTranslator.FromHtml("#FFAFAF"); }
            }



            public override Color MenuItemSelectedGradientEnd
            {
                get { return ColorTranslator.FromHtml("#FFAFAF"); }
            }
            public override Color MenuItemSelected
            {
                get { return ColorTranslator.FromHtml("#FFAFAF"); }
            }

            public override Color MenuItemBorder
            {
                get { return Color.FromArgb(192, 0, 0); }
            }
        }
            private void SetValuesOnSubItems(List<ToolStripMenuItem> items)
            {
                items.ForEach(item =>
                {
                    var dropdown = (ToolStripDropDownMenu)item.DropDown;
                    if (dropdown != null)
                    {
                        dropdown.ShowImageMargin = false;
                        SetValuesOnSubItems(item.DropDownItems.OfType<ToolStripMenuItem>().ToList());
                    }
                });
            }
        
        private void SystemSetupItem_Paint(object sender, PaintEventArgs e)
        {
            base.OnPaint(e);
            int borderWidth = 1;
            Color borderColor = Color.FromArgb(192, 0, 0);
            ControlPaint.DrawBorder(e.Graphics, e.ClipRectangle, borderColor,
                    borderWidth, ButtonBorderStyle.Solid, borderColor, borderWidth,
                    ButtonBorderStyle.Solid, borderColor, borderWidth, ButtonBorderStyle.Solid,
                    borderColor, borderWidth, ButtonBorderStyle.Solid);
        }

        private void AuditLogItem_Paint(object sender, PaintEventArgs e)
        {
            base.OnPaint(e);
            int borderWidth = 1;
            Color borderColor = Color.FromArgb(192, 0, 0);
            ControlPaint.DrawBorder(e.Graphics, e.ClipRectangle, borderColor,
                    borderWidth, ButtonBorderStyle.Solid, borderColor, borderWidth,
                    ButtonBorderStyle.Solid, borderColor, borderWidth, ButtonBorderStyle.Solid,
                    borderColor, borderWidth, ButtonBorderStyle.Solid);
        }

        private void DepartmentItem_Paint(object sender, PaintEventArgs e)
        {
            base.OnPaint(e);
            int borderWidth = 1;
            Color borderColor = Color.FromArgb(192, 0, 0);
            ControlPaint.DrawBorder(e.Graphics, e.ClipRectangle, borderColor,
                    borderWidth, ButtonBorderStyle.Solid, borderColor, borderWidth,
                    ButtonBorderStyle.Solid, borderColor, borderWidth, ButtonBorderStyle.Solid,
                    borderColor, borderWidth, ButtonBorderStyle.Solid);
        }

        private void TaxTableItem_Paint(object sender, PaintEventArgs e)
        {
            base.OnPaint(e);
            int borderWidth = 1;
            Color borderColor = Color.FromArgb(192, 0, 0);
            ControlPaint.DrawBorder(e.Graphics, e.ClipRectangle, borderColor,
                    borderWidth, ButtonBorderStyle.Solid, borderColor, borderWidth,
                    ButtonBorderStyle.Solid, borderColor, borderWidth, ButtonBorderStyle.Solid,
                    borderColor, borderWidth, ButtonBorderStyle.Solid);
        }

        private void SSSTableItem_Paint(object sender, PaintEventArgs e)
        {
            base.OnPaint(e);
            int borderWidth = 1;
            Color borderColor = Color.FromArgb(192, 0, 0);
            ControlPaint.DrawBorder(e.Graphics, e.ClipRectangle, borderColor,
                    borderWidth, ButtonBorderStyle.Solid, borderColor, borderWidth,
                    ButtonBorderStyle.Solid, borderColor, borderWidth, ButtonBorderStyle.Solid,
                    borderColor, borderWidth, ButtonBorderStyle.Solid);
        }

        private void PhilhealthTableItem_Paint(object sender, PaintEventArgs e)
        {
            base.OnPaint(e);
            int borderWidth = 1;
            Color borderColor = Color.FromArgb(192, 0, 0);
            ControlPaint.DrawBorder(e.Graphics, e.ClipRectangle, borderColor,
                    borderWidth, ButtonBorderStyle.Solid, borderColor, borderWidth,
                    ButtonBorderStyle.Solid, borderColor, borderWidth, ButtonBorderStyle.Solid,
                    borderColor, borderWidth, ButtonBorderStyle.Solid);
        }

        private void HDMFTableItem_Paint(object sender, PaintEventArgs e)
        {
            base.OnPaint(e);
            int borderWidth = 1;
            Color borderColor = Color.FromArgb(192, 0, 0);
            ControlPaint.DrawBorder(e.Graphics, e.ClipRectangle, borderColor,
                    borderWidth, ButtonBorderStyle.Solid, borderColor, borderWidth,
                    ButtonBorderStyle.Solid, borderColor, borderWidth, ButtonBorderStyle.Solid,
                    borderColor, borderWidth, ButtonBorderStyle.Solid);
        }

        private void EmployeeMasterFileItem_Paint(object sender, PaintEventArgs e)
        {
            base.OnPaint(e);
            int borderWidth = 1;
            Color borderColor = Color.FromArgb(192, 0, 0);
            ControlPaint.DrawBorder(e.Graphics, e.ClipRectangle, borderColor,
                    borderWidth, ButtonBorderStyle.Solid, borderColor, borderWidth,
                    ButtonBorderStyle.Solid, borderColor, borderWidth, ButtonBorderStyle.Solid,
                    borderColor, borderWidth, ButtonBorderStyle.Solid);
        }

        private void PostingTransactionItem_Paint(object sender, PaintEventArgs e)
        {
            base.OnPaint(e);
            int borderWidth = 1;
            Color borderColor = Color.FromArgb(192, 0, 0);
            ControlPaint.DrawBorder(e.Graphics, e.ClipRectangle, borderColor,
                    borderWidth, ButtonBorderStyle.Solid, borderColor, borderWidth,
                    ButtonBorderStyle.Solid, borderColor, borderWidth, ButtonBorderStyle.Solid,
                    borderColor, borderWidth, ButtonBorderStyle.Solid);
        }

        private void PayrollTransactionItem_Paint(object sender, PaintEventArgs e)
        {
            base.OnPaint(e);
            int borderWidth = 1;
            Color borderColor = Color.FromArgb(192, 0, 0);
            ControlPaint.DrawBorder(e.Graphics, e.ClipRectangle, borderColor,
                    borderWidth, ButtonBorderStyle.Solid, borderColor, borderWidth,
                    ButtonBorderStyle.Solid, borderColor, borderWidth, ButtonBorderStyle.Solid,
                    borderColor, borderWidth, ButtonBorderStyle.Solid);
        }

        private void PayrollProcessingItem_Paint(object sender, PaintEventArgs e)
        {
            base.OnPaint(e);
            int borderWidth = 1;
            Color borderColor = Color.FromArgb(192, 0, 0);
            ControlPaint.DrawBorder(e.Graphics, e.ClipRectangle, borderColor,
                    borderWidth, ButtonBorderStyle.Solid, borderColor, borderWidth,
                    ButtonBorderStyle.Solid, borderColor, borderWidth, ButtonBorderStyle.Solid,
                    borderColor, borderWidth, ButtonBorderStyle.Solid);
        }

        private void BonusProcessItem_Paint(object sender, PaintEventArgs e)
        {
            base.OnPaint(e);
            int borderWidth = 1;
            Color borderColor = Color.FromArgb(192, 0, 0);
            ControlPaint.DrawBorder(e.Graphics, e.ClipRectangle, borderColor,
                    borderWidth, ButtonBorderStyle.Solid, borderColor, borderWidth,
                    ButtonBorderStyle.Solid, borderColor, borderWidth, ButtonBorderStyle.Solid,
                    borderColor, borderWidth, ButtonBorderStyle.Solid);
        }

        private void BonusRegisterItem_Paint(object sender, PaintEventArgs e)
        {
            base.OnPaint(e);
            int borderWidth = 1;
            Color borderColor = Color.FromArgb(192, 0, 0);
            ControlPaint.DrawBorder(e.Graphics, e.ClipRectangle, borderColor,
                    borderWidth, ButtonBorderStyle.Solid, borderColor, borderWidth,
                    ButtonBorderStyle.Solid, borderColor, borderWidth, ButtonBorderStyle.Solid,
                    borderColor, borderWidth, ButtonBorderStyle.Solid);
        }

        private void PayslipReportItem_Paint(object sender, PaintEventArgs e)
        {
            base.OnPaint(e);
            int borderWidth = 1;
            Color borderColor = Color.FromArgb(192, 0, 0);
            ControlPaint.DrawBorder(e.Graphics, e.ClipRectangle, borderColor,
                    borderWidth, ButtonBorderStyle.Solid, borderColor, borderWidth,
                    ButtonBorderStyle.Solid, borderColor, borderWidth, ButtonBorderStyle.Solid,
                    borderColor, borderWidth, ButtonBorderStyle.Solid);
        }

        private void PayrollRegisterItem_Paint(object sender, PaintEventArgs e)
        {
            base.OnPaint(e);
            int borderWidth = 1;
            Color borderColor = Color.FromArgb(192, 0, 0);
            ControlPaint.DrawBorder(e.Graphics, e.ClipRectangle, borderColor,
                    borderWidth, ButtonBorderStyle.Solid, borderColor, borderWidth,
                    ButtonBorderStyle.Solid, borderColor, borderWidth, ButtonBorderStyle.Solid,
                    borderColor, borderWidth, ButtonBorderStyle.Solid);
        }

        private void RegisterMonthlyReportItem_Paint(object sender, PaintEventArgs e)
        {
            base.OnPaint(e);
            int borderWidth = 1;
            Color borderColor = Color.FromArgb(192, 0, 0);
            ControlPaint.DrawBorder(e.Graphics, e.ClipRectangle, borderColor,
                    borderWidth, ButtonBorderStyle.Solid, borderColor, borderWidth,
                    ButtonBorderStyle.Solid, borderColor, borderWidth, ButtonBorderStyle.Solid,
                    borderColor, borderWidth, ButtonBorderStyle.Solid);
        }

        private void BankRemittanceReportItem_Paint(object sender, PaintEventArgs e)
        {
            base.OnPaint(e);
            int borderWidth = 1;
            Color borderColor = Color.FromArgb(192, 0, 0);
            ControlPaint.DrawBorder(e.Graphics, e.ClipRectangle, borderColor,
                    borderWidth, ButtonBorderStyle.Solid, borderColor, borderWidth,
                    ButtonBorderStyle.Solid, borderColor, borderWidth, ButtonBorderStyle.Solid,
                    borderColor, borderWidth, ButtonBorderStyle.Solid);
        }

        private void BonusRemittanceItem_Paint(object sender, PaintEventArgs e)
        {
            base.OnPaint(e);
            int borderWidth = 1;
            Color borderColor = Color.FromArgb(192, 0, 0);
            ControlPaint.DrawBorder(e.Graphics, e.ClipRectangle, borderColor,
                    borderWidth, ButtonBorderStyle.Solid, borderColor, borderWidth,
                    ButtonBorderStyle.Solid, borderColor, borderWidth, ButtonBorderStyle.Solid,
                    borderColor, borderWidth, ButtonBorderStyle.Solid);
        }

        private void YearEndProcessItem_Paint(object sender, PaintEventArgs e)
        {
            base.OnPaint(e);
            int borderWidth = 1;
            Color borderColor = Color.FromArgb(192, 0, 0);
            ControlPaint.DrawBorder(e.Graphics, e.ClipRectangle, borderColor,
                    borderWidth, ButtonBorderStyle.Solid, borderColor, borderWidth,
                    ButtonBorderStyle.Solid, borderColor, borderWidth, ButtonBorderStyle.Solid,
                    borderColor, borderWidth, ButtonBorderStyle.Solid);
        }

        private void BIRAphalistItem_Paint(object sender, PaintEventArgs e)
        {
            base.OnPaint(e);
            int borderWidth = 1;
            Color borderColor = Color.FromArgb(192, 0, 0);
            ControlPaint.DrawBorder(e.Graphics, e.ClipRectangle, borderColor,
                    borderWidth, ButtonBorderStyle.Solid, borderColor, borderWidth,
                    ButtonBorderStyle.Solid, borderColor, borderWidth, ButtonBorderStyle.Solid,
                    borderColor, borderWidth, ButtonBorderStyle.Solid);
        }

        private void OvertimeTableItem_Paint(object sender, PaintEventArgs e)
        {
            base.OnPaint(e);
            int borderWidth = 1;
            Color borderColor = Color.FromArgb(192, 0, 0);
            ControlPaint.DrawBorder(e.Graphics, e.ClipRectangle, borderColor,
                    borderWidth, ButtonBorderStyle.Solid, borderColor, borderWidth,
                    ButtonBorderStyle.Solid, borderColor, borderWidth, ButtonBorderStyle.Solid,
                    borderColor, borderWidth, ButtonBorderStyle.Solid);
        }

        private void SystemSetupItem_MouseHover(object sender, EventArgs e)
        {
            SystemSetupItem.ForeColor = Color.White;
        }

        private void SystemSetupItem_MouseLeave(object sender, EventArgs e)
        {
            SystemSetupItem.ForeColor = Color.Black;
        }

        private void SystemSetupItem_MouseEnter(object sender, EventArgs e)
        {
            SystemSetupItem.ForeColor = Color.White;
        }

        private void AuditLogItem_MouseEnter(object sender, EventArgs e)
        {
            AuditLogItem.ForeColor = Color.White;
        }

        private void AuditLogItem_MouseHover(object sender, EventArgs e)
        {
            AuditLogItem.ForeColor = Color.White;
        }

        private void AuditLogItem_MouseLeave(object sender, EventArgs e)
        {
            AuditLogItem.ForeColor = Color.Black;
        }

        private void OvertimeTableItem_MouseEnter(object sender, EventArgs e)
        {
            OvertimeTableItem.ForeColor = Color.White;
        }

        private void OvertimeTableItem_MouseHover(object sender, EventArgs e)
        {
            OvertimeTableItem.ForeColor = Color.White;
        }

        private void OvertimeTableItem_MouseLeave(object sender, EventArgs e)
        {
            OvertimeTableItem.ForeColor = Color.Black;
        }

        private void DepartmentItem_MouseEnter(object sender, EventArgs e)
        {
            DepartmentItem.ForeColor = Color.White;
        }

        private void DepartmentItem_MouseHover(object sender, EventArgs e)
        {
            DepartmentItem.ForeColor = Color.White;
        }

        private void DepartmentItem_MouseLeave(object sender, EventArgs e)
        {
            DepartmentItem.ForeColor = Color.Black;
        }

        private void TaxTableItem_MouseEnter(object sender, EventArgs e)
        {
            TaxTableItem.ForeColor = Color.White;
        }

        private void TaxTableItem_MouseHover(object sender, EventArgs e)
        {
            TaxTableItem.ForeColor = Color.White;
        }

        private void TaxTableItem_MouseLeave(object sender, EventArgs e)
        {
            TaxTableItem.ForeColor = Color.Black;
        }

        private void SSSTableItem_MouseEnter(object sender, EventArgs e)
        {
            SSSTableItem.ForeColor = Color.White;
        }

        private void SSSTableItem_MouseHover(object sender, EventArgs e)
        {
            SSSTableItem.ForeColor = Color.White;
        }

        private void SSSTableItem_MouseLeave(object sender, EventArgs e)
        {
            SSSTableItem.ForeColor = Color.Black;
        }

        private void HDMFTableItem_MouseEnter(object sender, EventArgs e)
        {
            HDMFTableItem.ForeColor= Color.White;
        }

        private void HDMFTableItem_MouseHover(object sender, EventArgs e)
        {
            HDMFTableItem.ForeColor = Color.White;
        }

        private void HDMFTableItem_MouseLeave(object sender, EventArgs e)
        {
            HDMFTableItem.ForeColor = Color.Black;
        }

        private void Homepage_Paint(object sender, PaintEventArgs e)
        {

        }

        private void BIRAphalistItem_Click(object sender, EventArgs e)
        {
           BIR_Alphalist bIR_Alphalist = new BIR_Alphalist();
           bIR_Alphalist.Show();
        }

        private void BankRemittanceReportItem_Click(object sender, EventArgs e)
        {
            Bank_Remittance bank_Remittance = new Bank_Remittance();
            bank_Remittance.Show();
        }

        private void BonusRemittanceItem_Click(object sender, EventArgs e)
        {
            Bonus_Report report = new Bonus_Report();
            report.Show();
        }

        private void SSSTableItem_MouseDown(object sender, MouseEventArgs e)
        {
           
        }

        private void SSSTableItem_MouseMove(object sender, MouseEventArgs e)
        {
           
        }

        private void SSSTableItem_MouseUp(object sender, MouseEventArgs e)
        {
           
        }

        private void PhilhealthTableItem_MouseEnter(object sender, EventArgs e)
        {
            PhilhealthTableItem.ForeColor = Color.White;
        }

        private void PhilhealthTableItem_MouseHover(object sender, EventArgs e)
        {
            PhilhealthTableItem.ForeColor = Color.White;
        }

        private void PhilhealthTableItem_MouseLeave(object sender, EventArgs e)
        {
            PhilhealthTableItem.ForeColor = Color.Black;
        }

        private void EmployeeMasterFileItem_MouseEnter(object sender, EventArgs e)
        {
            EmployeeMasterFileItem.ForeColor = Color.White;
        }

        private void EmployeeMasterFileItem_MouseHover(object sender, EventArgs e)
        {
            EmployeeMasterFileItem.ForeColor = Color.White;
        }

        private void EmployeeMasterFileItem_MouseLeave(object sender, EventArgs e)
        {
            EmployeeMasterFileItem.ForeColor = Color.Black;
        }

       

        private void PostingTransactionItem_MouseEnter(object sender, EventArgs e)
        {
            PostingTransactionItem.ForeColor = Color.White;
        }

        private void PostingTransactionItem_MouseHover(object sender, EventArgs e)
        {
            PostingTransactionItem.ForeColor = Color.White;
        }

        private void PostingTransactionItem_MouseLeave(object sender, EventArgs e)
        {
            PostingTransactionItem.ForeColor = Color.Black;
        }

        private void PayrollTransactionItem_MouseEnter(object sender, EventArgs e)
        {
            PayrollTransactionItem.ForeColor = Color.White;
        }

        private void PayrollTransactionItem_MouseHover(object sender, EventArgs e)
        {
            PayrollTransactionItem.ForeColor = Color.White;
        }

        private void PayrollTransactionItem_MouseLeave(object sender, EventArgs e)
        {
            PayrollTransactionItem.ForeColor = Color.Black;
        }

        private void PayrollProcessingItem_MouseEnter(object sender, EventArgs e)
        {
            PayrollProcessingItem.ForeColor = Color.White;
        }

        private void PayrollProcessingItem_MouseHover(object sender, EventArgs e)
        {
            PayrollProcessingItem.ForeColor = Color.White;
        }

        private void PayrollProcessingItem_MouseLeave(object sender, EventArgs e)
        {
            PayrollProcessingItem.ForeColor = Color.Black;
        }

        private void BonusProcessItem_MouseEnter(object sender, EventArgs e)
        {
            BonusProcessItem.ForeColor = Color.White;
        }

        private void BonusProcessItem_MouseHover(object sender, EventArgs e)
        {
            BonusProcessItem.ForeColor = Color.White;
        }

        private void BonusProcessItem_MouseLeave(object sender, EventArgs e)
        {
            BonusProcessItem.ForeColor = Color.Black;
        }

        private void BonusRegisterItem_MouseEnter(object sender, EventArgs e)
        {
            BonusRegisterItem.ForeColor = Color.White;
        }

        private void BonusRegisterItem_MouseHover(object sender, EventArgs e)
        {
            BonusRegisterItem.ForeColor = Color.White;
        }

        private void BonusRegisterItem_MouseLeave(object sender, EventArgs e)
        {
            BonusRegisterItem.ForeColor = Color.Black;
        }

        private void PayslipReportItem_MouseEnter(object sender, EventArgs e)
        {
            PayslipReportItem.ForeColor = Color.White;
        }

        private void PayslipReportItem_MouseHover(object sender, EventArgs e)
        {
            PayslipReportItem.ForeColor = Color.White;
        }

        private void PayslipReportItem_MouseLeave(object sender, EventArgs e)
        {
            PayslipReportItem.ForeColor = Color.Black;
        }

        private void PayrollRegisterItem_MouseEnter(object sender, EventArgs e)
        {
            PayrollRegisterItem.ForeColor = Color.White;
        }

        private void PayrollRegisterItem_MouseHover(object sender, EventArgs e)
        {
            PayrollRegisterItem.ForeColor = Color.White;
        }

        private void PayrollRegisterItem_MouseLeave(object sender, EventArgs e)
        {
            PayrollRegisterItem.ForeColor = Color.Black;
        }

        private void RegisterMonthlyReportItem_MouseEnter(object sender, EventArgs e)
        {
            RegisterMonthlyReportItem.ForeColor = Color.White;
        }

        private void RegisterMonthlyReportItem_MouseHover(object sender, EventArgs e)
        {
            RegisterMonthlyReportItem.ForeColor = Color.White;
        }

        private void RegisterMonthlyReportItem_MouseLeave(object sender, EventArgs e)
        {
            RegisterMonthlyReportItem.ForeColor = Color.Black;
        }

        private void BankRemittanceReportItem_MouseEnter(object sender, EventArgs e)
        {
            BankRemittanceReportItem.ForeColor = Color.White;
        }

        private void BankRemittanceReportItem_MouseHover(object sender, EventArgs e)
        {
            BankRemittanceReportItem.ForeColor = Color.White;
        }

        private void BankRemittanceReportItem_MouseLeave(object sender, EventArgs e)
        {
            BankRemittanceReportItem.ForeColor = Color.Black;
        }

        private void BonusRemittanceItem_MouseEnter(object sender, EventArgs e)
        {
            BonusRemittanceItem.ForeColor = Color.White;
        }

        private void BonusRemittanceItem_MouseHover(object sender, EventArgs e)
        {
            BonusRemittanceItem.ForeColor = Color.White;
        }

        private void BonusRemittanceItem_MouseLeave(object sender, EventArgs e)
        {
            BonusRemittanceItem.ForeColor = Color.Black;
        }

        private void YearEndProcessItem_MouseEnter(object sender, EventArgs e)
        {
            YearEndProcessItem.ForeColor = Color.White;
        }

        private void YearEndProcessItem_MouseHover(object sender, EventArgs e)
        {
            YearEndProcessItem.ForeColor = Color.White;
        }

        private void YearEndProcessItem_MouseLeave(object sender, EventArgs e)
        {
            YearEndProcessItem.ForeColor = Color.Black;
        }

        private void BIRAphalistItem_MouseEnter(object sender, EventArgs e)
        {
            BIRAphalistItem.ForeColor = Color.White;
        }

        private void BIRAphalistItem_MouseHover(object sender, EventArgs e)
        {
            BIRAphalistItem.ForeColor = Color.White;
        }

        private void BIRAphalistItem_MouseLeave(object sender, EventArgs e)
        {
            BIRAphalistItem.ForeColor = Color.Black;
        }

        private void EmployeeMasterFileItem_Click(object sender, EventArgs e)
        {
           Employee_Info employee_Info = new Employee_Info();
           employee_Info.user_id = user_id;
           employee_Info.ShowDialog();

        }
    }
}