﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using MySql.Data.MySqlClient;
using System.Configuration;

namespace Payroll
{




    public partial class Payroll_Month : Form
    {
        MySqlCommand cmd;
        MySqlDataAdapter da;
        DataTable dt;

        public Payroll_Month()
        {
            InitializeComponent();
        }

        private void Payroll_Month_Load(object sender, EventArgs e)
        {
            
            LoadMonthdetails();
            view_date();
            display_payroll_Month("SELECT pd_id, emp_id, fullname, department, yyyymm, month_pay, SUM(regular_pay) as regular_pay, SUM(tax_sub) as tax_sub, SUM(gross_inc) as gross_inc, SUM(gross_ded) as gross_ded, SUM(net_pay) as net_pay, SUM(wtax) as wtax, SUM(sss_ee) as sss_ee, SUM(sss_er) as sss_er, SUM(pag_ibig_ee) as pag_ibig_ee, SUM(pag_ibig_er) as pag_ibig_er, SUM(philhealth_ee) as philhealth_ee, SUM(philhealth_er) as philhealth_er, SUM(company_loan) as company_loan, SUM(sss_loan) as sss_loan, SUM(pag_ibig_loan) as pag_ibig_loan FROM pay_details where year_end = 'False' GROUP BY emp_id, fullname");
          
        }
        public void display_payroll_Month(string sql1)
        {

            string myconnection = ConfigurationManager.ConnectionStrings["MyConstring"].ConnectionString;
            MySqlConnection connection = new MySqlConnection(myconnection);

            try
            {
                connection.Open();
                cmd = new MySqlCommand();
                cmd.Connection = connection;
                cmd.CommandText = sql1;
                da = new MySqlDataAdapter();
                da.SelectCommand = cmd;
                dt = new DataTable();
                da.Fill(dt);
                lv_payroll_month.Items.Clear();



                for (int i = 0; i < dt.Rows.Count; i++)
                {
                    DataRow dr = dt.Rows[i];
                    ListViewItem listitem = new ListViewItem(dr["pd_id"].ToString());
                    listitem.SubItems.Add(dr["emp_id"].ToString());
                    listitem.SubItems.Add(dr["fullname"].ToString());
                    listitem.SubItems.Add(dr["department"].ToString());
                    listitem.SubItems.Add(dr["yyyymm"].ToString());
                  
                    listitem.SubItems.Add(string.Format("{0:#,##0.00}", Convert.ToDecimal(dr["month_pay"])));
                    listitem.SubItems.Add(string.Format("{0:#,##0.00}", Convert.ToDecimal(dr["regular_pay"])));
                    listitem.SubItems.Add(string.Format("{0:#,##0.00}", Convert.ToDecimal(dr["tax_sub"])));
                    listitem.SubItems.Add(string.Format("{0:#,##0.00}", Convert.ToDecimal(dr["gross_inc"])));
                    listitem.SubItems.Add(string.Format("{0:#,##0.00}", Convert.ToDecimal(dr["gross_ded"])));
                    listitem.SubItems.Add(string.Format("{0:#,##0.00}", Convert.ToDecimal(dr["net_pay"])));
                    listitem.SubItems.Add(string.Format("{0:#,##0.00}", Convert.ToDecimal(dr["wtax"])));
                    listitem.SubItems.Add(string.Format("{0:#,##0.00}", Convert.ToDecimal(dr["sss_ee"])));
                    listitem.SubItems.Add(string.Format("{0:#,##0.00}", Convert.ToDecimal(dr["sss_er"])));
                    listitem.SubItems.Add(string.Format("{0:#,##0.00}", Convert.ToDecimal(dr["pag_ibig_ee"])));
                    listitem.SubItems.Add(string.Format("{0:#,##0.00}", Convert.ToDecimal(dr["pag_ibig_er"])));
                    listitem.SubItems.Add(string.Format("{0:#,##0.00}", Convert.ToDecimal(dr["philhealth_ee"])));
                    listitem.SubItems.Add(string.Format("{0:#,##0.00}", Convert.ToDecimal(dr["philhealth_ee"])));
                    listitem.SubItems.Add(string.Format("{0:#,##0.00}", Convert.ToDecimal(dr["company_loan"])));
                    listitem.SubItems.Add(string.Format("{0:#,##0.00}", Convert.ToDecimal(dr["pag_ibig_loan"])));
                    listitem.SubItems.Add(string.Format("{0:#,##0.00}", Convert.ToDecimal(dr["sss_loan"])));


                    lv_payroll_month.Items.Add(listitem);
                }

            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
            finally
            {
                connection.Close();
            }

            lv_payroll_month.View = View.Details;

            lv_payroll_month.Columns.Add(" No ", 0, HorizontalAlignment.Center);
            lv_payroll_month.Columns.Add(" Employee ID ", 140, HorizontalAlignment.Center);
            lv_payroll_month.Columns.Add(" Fullname ", 165, HorizontalAlignment.Center);
            lv_payroll_month.Columns.Add(" Department ", 165, HorizontalAlignment.Center);
            lv_payroll_month.Columns.Add(" MMYYYY ", 0, HorizontalAlignment.Center);
            lv_payroll_month.Columns.Add(" Monthly Pay ", 165, HorizontalAlignment.Center);
            lv_payroll_month.Columns.Add(" Regular Pay ", 165, HorizontalAlignment.Center);
            lv_payroll_month.Columns.Add(" Tax Sub ", 165, HorizontalAlignment.Center);
            lv_payroll_month.Columns.Add(" Gross_Pay ", 165, HorizontalAlignment.Center);
            lv_payroll_month.Columns.Add(" Gross_Ded ", 165, HorizontalAlignment.Center);
            lv_payroll_month.Columns.Add(" Net_Pay ", 165, HorizontalAlignment.Center);
            lv_payroll_month.Columns.Add(" Wtax ", 165, HorizontalAlignment.Center);
            lv_payroll_month.Columns.Add(" SSS EE ", 165, HorizontalAlignment.Center);
            lv_payroll_month.Columns.Add(" SSS ER ", 165, HorizontalAlignment.Center);
            lv_payroll_month.Columns.Add(" Pag-Ibig EE ", 165, HorizontalAlignment.Center);
            lv_payroll_month.Columns.Add(" Pag-Ibig ER ", 165, HorizontalAlignment.Center);
            lv_payroll_month.Columns.Add(" Philhealth EE ", 165, HorizontalAlignment.Center);
            lv_payroll_month.Columns.Add(" Philhealth ER ", 165, HorizontalAlignment.Center);
            lv_payroll_month.Columns.Add(" Company_loan ", 165, HorizontalAlignment.Center);
            lv_payroll_month.Columns.Add(" Pag-Ibig Loan ", 165, HorizontalAlignment.Center);
            lv_payroll_month.Columns.Add(" SSS Loan ", 165, HorizontalAlignment.Center);
          



        }

        private void btn_print_Click(object sender, EventArgs e)
        {
            ToExcel();
        }

        private void ToExcel()
        {
            StringBuilder sb = new StringBuilder();
            Microsoft.Office.Interop.Excel.Application app = new Microsoft.Office.Interop.Excel.Application();
            app.Visible = true;
            Microsoft.Office.Interop.Excel.Workbook wb = app.Workbooks.Add(1);
            Microsoft.Office.Interop.Excel.Worksheet ws = (Microsoft.Office.Interop.Excel.Worksheet)wb.Worksheets[1];
            int i = 1;
            int i2 = 2;
            int x = 1;
            int x2 = 1;
            foreach (ColumnHeader ch in lv_payroll_month.Columns)
            {
                ws.Cells[x2, x] = ch.Text;
                x++;
            }

            foreach (ListViewItem lvi in lv_payroll_month.Items)
            {
                i = 1;
                foreach (ListViewItem.ListViewSubItem lvs in lvi.SubItems)
                {
                    ws.Cells[i2, i] = lvs.Text;
                    ws.Cells.Select();
                    ws.Cells.EntireColumn.AutoFit();
                    i++;
                }
                i2++;
            }
        }

        private void LoadMonthdetails()
        {


            string myconnection = ConfigurationManager.ConnectionStrings["MyConstring"].ConnectionString;
            MySqlConnection connection = new MySqlConnection(myconnection);

            try
            {
                connection.Open();
                cmd = new MySqlCommand();
                cmd.Connection = connection;
                cmd.CommandText = "Select yyyymm FROM pay_details where year_end = 'False' group by yyyymm";
                da = new MySqlDataAdapter();
                da.SelectCommand = cmd;
                dt = new DataTable();
                da.Fill(dt);

                for (int i = 0; i < dt.Rows.Count; i++)
                {
                    DataRow dr = dt.Rows[i];



                    comboBox1.Items.Add((dr["yyyymm"].ToString()));

                }

            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
            finally
            {
                connection.Close();
            }
        }

        private void tb_search_TextChanged(object sender, EventArgs e)
        {
           lv_payroll_month.Columns.Clear();
            display_payroll_Month("SELECT DISTINCT pd_id, emp_id, fullname, department, emp_status, yyyymm, month_pay, SUM(regular_pay) as regular_pay, SUM(tax_sub) as tax_sub, SUM(gross_inc) as gross_inc, SUM(gross_ded) as gross_ded, SUM(net_pay) as net_pay, SUM(wtax) as wtax, SUM(sss_ee) as sss_ee, SUM(sss_er) as sss_er, SUM(pag_ibig_ee) as pag_ibig_ee, SUM(pag_ibig_er) as pag_ibig_er, SUM(philhealth_ee) as philhealth_ee, SUM(philhealth_er) as philhealth_er, SUM(company_loan) as company_loan, SUM(sss_loan) as sss_loan, SUM(pag_ibig_loan) as pag_ibig_loan FROM pay_details WHERE emp_id like '" + tb_search.Text + "%' or fullname like '" + tb_search.Text + "%' or yyyymm like  '" + tb_search.Text + "%'  or date_period like  '" + tb_search.Text + "%' or department like '" + tb_search.Text + "%' or emp_status like '"+tb_search.Text+"%' or month_pay like  '" + tb_search.Text + "%' or regular_pay like  '" + tb_search.Text + "%' or tax_sub like  '" + tb_search.Text + "%' or others_pay_det like '" + tb_search.Text + "%' or overtime_pay_det like  '" + tb_search.Text + "%'  or gross_inc like  '" + tb_search.Text + "%' or  gross_ded like  '" + tb_search.Text + "%' or net_pay like  '" + tb_search.Text + "%' or wtax like  '" + tb_search.Text + "%' or other_ded_det like  '" + tb_search.Text + "%' or sss_ee like '" + tb_search.Text + "%' or sss_er like  '" + tb_search.Text + "%' or philhealth_ee like  '" + tb_search.Text + "%' or philhealth_er like '" + tb_search.Text + "%' or pag_ibig_ee like '" + tb_search.Text + "%' or pag_ibig_er like '" + tb_search.Text + "%' or company_loan like '" + tb_search.Text + "%' or pag_ibig_loan like '" + tb_search.Text + "%' or sss_loan like '" + tb_search.Text + "%' GROUP BY emp_id, fullname, yyyymm");
        }

        public void view_date()
        {

            string myconnection = ConfigurationManager.ConnectionStrings["MyConstring"].ConnectionString;
            MySqlConnection connection = new MySqlConnection(myconnection);

            try
            {
                connection.Open();
                cmd = new MySqlCommand();
                cmd.Connection = connection;
                cmd.CommandText = "Select * FROM pay_details";
                da = new MySqlDataAdapter();
                da.SelectCommand = cmd;
                dt = new DataTable();
                da.Fill(dt);

                for (int i = 0; i < dt.Rows.Count; i++)
                {
                    DataRow dr = dt.Rows[i];

                    comboBox1.SelectedItem = (dr["yyyymm"].ToString());
                }

            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
            finally
            {
                connection.Close();
            }
        }
        private void comboBox1_SelectionChangeCommitted(object sender, EventArgs e)
        {
            lv_payroll_month.Columns.Clear();
            display_payroll_Month("SELECT pd_id, emp_id, fullname, department, yyyymm, month_pay, SUM(regular_pay) as regular_pay, SUM(tax_sub) as tax_sub, SUM(gross_inc) as gross_inc, SUM(gross_ded) as gross_ded, SUM(net_pay) as net_pay, SUM(wtax) as wtax, SUM(sss_ee) as sss_ee, SUM(sss_er) as sss_er, SUM(pag_ibig_ee) as pag_ibig_ee, SUM(pag_ibig_er) as pag_ibig_er, SUM(philhealth_ee) as philhealth_ee, SUM(philhealth_er) as philhealth_er, SUM(company_loan) as company_loan, SUM(sss_loan) as sss_loan, SUM(pag_ibig_loan) as pag_ibig_loan FROM pay_details where yyyymm = '" + comboBox1.Text + "' and year_end = 'False' GROUP BY emp_id, fullname, yyyymm");
        }

     
    }
}
