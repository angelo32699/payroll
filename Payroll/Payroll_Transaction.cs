﻿using MySql.Data.MySqlClient;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Configuration;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Payroll
{
    public partial class Payroll_Transaction : Form
    {

        MySqlCommand cmd;
        MySqlDataAdapter da;
        DataTable dt;

        decimal total_bonus_netpay;
        decimal total_gross_pay_details;
        decimal total_details_total_wtax;
        public Payroll_Transaction()
        {
            InitializeComponent();
        }

        private void Payroll_Transaction_Load(object sender, EventArgs e)
        {
            delete_data();
            sum_table();
            sum_bonus();
            annual_tax_inc();
            update_gross_pay();
            Regular_tax();
            Consultant_tax();
            update_wtax();
            lv_payroll_transaction.Columns.Clear();
            update_grossded_netpay();
            update_net_pay();
            display_payroll_transaction("Select * from pay_trans left join employee on pay_trans.emp_no = employee.emp_no left join salary on pay_trans.sal_id = salary.salary_id left join loan on pay_trans.loan_id = loan.loan_id where process_tag = 'False'");
        }

        public void display_payroll_transaction(string sql1)
        {

            string myconnection = ConfigurationManager.ConnectionStrings["MyConstring"].ConnectionString;
            MySqlConnection connection = new MySqlConnection(myconnection);

            try
            {
                connection.Open();
                cmd = new MySqlCommand();
                cmd.Connection = connection;
                cmd.CommandText = sql1;
                da = new MySqlDataAdapter();
                da.SelectCommand = cmd;
                dt = new DataTable();
                da.Fill(dt);
                lv_payroll_transaction.Items.Clear();



                for (int i = 0; i < dt.Rows.Count; i++)
                {
                    DataRow dr = dt.Rows[i];
                    ListViewItem listitem = new ListViewItem(dr["pay_trans_id"].ToString());
                    listitem.SubItems.Add(dr["date"].ToString());
                    listitem.SubItems.Add(dr["date_from"].ToString());
                    listitem.SubItems.Add(dr["date_to"].ToString());
                    listitem.SubItems.Add(dr["date_period"].ToString());
                    listitem.SubItems.Add(dr["emp_id"].ToString());
                    listitem.SubItems.Add(dr["lname"].ToString());
                    listitem.SubItems.Add(dr["fname"].ToString());
                    listitem.SubItems.Add(dr["mname"].ToString());
                    listitem.SubItems.Add(dr["department"].ToString());
                    listitem.SubItems.Add(dr["emp_status"].ToString());
                    listitem.SubItems.Add(string.Format("{0:#,##0.00}", Convert.ToDecimal(dr["salary"])));
                    listitem.SubItems.Add(string.Format("{0:#,##0.00}", Convert.ToDecimal(dr["other_inc"])));
                    listitem.SubItems.Add(string.Format("{0:#,##0.00}", Convert.ToDecimal(dr["ot_pay"])));
                    listitem.SubItems.Add(string.Format("{0:#,##0.00}", Convert.ToDecimal(dr["deductions"])));
                    listitem.SubItems.Add(string.Format("{0:#,##0.00}", Convert.ToDecimal(dr["tax_sub"])));
                    listitem.SubItems.Add(string.Format("{0:#,##0.00}", Convert.ToDecimal(dr["regular_pay"])));
                    listitem.SubItems.Add(string.Format("{0:#,##0.00}", Convert.ToDecimal(dr["daily_rate"])));
                    listitem.SubItems.Add(string.Format("{0:#,##0.00}", Convert.ToDecimal(dr["daily_hour_rate"])));
                    listitem.SubItems.Add(string.Format("{0:#,##0.00}", Convert.ToDecimal(dr["sss_ee"])));
                    listitem.SubItems.Add(string.Format("{0:#,##0.00}", Convert.ToDecimal(dr["sss_er"])));
                    listitem.SubItems.Add(string.Format("{0:#,##0.00}", Convert.ToDecimal(dr["pag_ibig_ee"])));
                    listitem.SubItems.Add(string.Format("{0:#,##0.00}", Convert.ToDecimal(dr["pag_ibig_er"])));
                    listitem.SubItems.Add(string.Format("{0:#,##0.00}", Convert.ToDecimal(dr["philhealth_ee"])));
                    listitem.SubItems.Add(string.Format("{0:#,##0.00}", Convert.ToDecimal(dr["philhealth_er"])));
                    listitem.SubItems.Add(string.Format("{0:#,##0.00}", Convert.ToDecimal(dr["comp_month_ded"])));
                    listitem.SubItems.Add(string.Format("{0:#,##0.00}", Convert.ToDecimal(dr["pag_ibig_month_ded"])));
                    listitem.SubItems.Add(string.Format("{0:#,##0.00}", Convert.ToDecimal(dr["sss_month_ded"])));
                    listitem.SubItems.Add(string.Format("{0:#,##0.00}", Convert.ToDecimal(dr["comp_bal"])));
                    listitem.SubItems.Add(string.Format("{0:#,##0.00}", Convert.ToDecimal(dr["sss_bal"])));
                    listitem.SubItems.Add(string.Format("{0:#,##0.00}", Convert.ToDecimal(dr["pag_ibig_bal"])));
                    listitem.SubItems.Add(string.Format("{0:#,##0.00}", Convert.ToDecimal(dr["other_ded"])));
                    listitem.SubItems.Add(string.Format("{0:#,##0.00}", Convert.ToDecimal(dr["wtax"])));
                    listitem.SubItems.Add(string.Format("{0:#,##0.00}", Convert.ToDecimal(dr["gross_pay"])));
                    listitem.SubItems.Add(string.Format("{0:#,##0.00}", Convert.ToDecimal(dr["gross_ded"])));
                    listitem.SubItems.Add(string.Format("{0:#,##0.00}", Convert.ToDecimal(dr["net_pay"])));
                    listitem.SubItems.Add(dr["absences"].ToString());
                    lv_payroll_transaction.Items.Add(listitem);
                }

            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
            finally
            {
                connection.Close();
            }

            lv_payroll_transaction.View = View.Details;

            lv_payroll_transaction.Columns.Add(" No ", 0, HorizontalAlignment.Center);
            lv_payroll_transaction.Columns.Add(" Date ", 165, HorizontalAlignment.Center);
            lv_payroll_transaction.Columns.Add(" Payroll Date From ", 165, HorizontalAlignment.Center);
            lv_payroll_transaction.Columns.Add(" Payroll Date To ", 165, HorizontalAlignment.Center);
            lv_payroll_transaction.Columns.Add(" Payroll Period ", 165, HorizontalAlignment.Center);
            lv_payroll_transaction.Columns.Add(" Employee ID ", 140, HorizontalAlignment.Center);
            lv_payroll_transaction.Columns.Add(" Lastname ", 140, HorizontalAlignment.Center);
            lv_payroll_transaction.Columns.Add(" Firstname ", 165, HorizontalAlignment.Center);
            lv_payroll_transaction.Columns.Add(" Middlename ", 165, HorizontalAlignment.Center);
            lv_payroll_transaction.Columns.Add(" Department ", 165, HorizontalAlignment.Center);
            lv_payroll_transaction.Columns.Add(" Employee Status ", 165, HorizontalAlignment.Center);
            lv_payroll_transaction.Columns.Add(" Monthly Pay ", 165, HorizontalAlignment.Center);
            lv_payroll_transaction.Columns.Add(" Other Income ", 165, HorizontalAlignment.Center);
            lv_payroll_transaction.Columns.Add(" O.T Pay ", 165, HorizontalAlignment.Center);
            lv_payroll_transaction.Columns.Add(" Deductions(-/+) ", 165, HorizontalAlignment.Center);
            lv_payroll_transaction.Columns.Add(" Tax Subsidy ", 165, HorizontalAlignment.Center);
            lv_payroll_transaction.Columns.Add(" Regular Pay ", 165, HorizontalAlignment.Center);
            lv_payroll_transaction.Columns.Add(" Daily Rate ", 165, HorizontalAlignment.Center);
            lv_payroll_transaction.Columns.Add(" Daily Hour Rate ", 165, HorizontalAlignment.Center);
            lv_payroll_transaction.Columns.Add(" Wtax% ", 0, HorizontalAlignment.Center);
            lv_payroll_transaction.Columns.Add(" SSS EE ", 165, HorizontalAlignment.Center);
            lv_payroll_transaction.Columns.Add(" SSS ER ", 165, HorizontalAlignment.Center);
            lv_payroll_transaction.Columns.Add(" Pag-Ibig EE ", 165, HorizontalAlignment.Center);
            lv_payroll_transaction.Columns.Add(" Pag-Ibig ER ", 165, HorizontalAlignment.Center);
            lv_payroll_transaction.Columns.Add(" Philhealth EE ", 165, HorizontalAlignment.Center);
            lv_payroll_transaction.Columns.Add(" Philhealth ER ", 165, HorizontalAlignment.Center);
            lv_payroll_transaction.Columns.Add(" Company Loan ded ", 165, HorizontalAlignment.Center);
            lv_payroll_transaction.Columns.Add(" Pag-Ibig Loan ded ", 165, HorizontalAlignment.Center);
            lv_payroll_transaction.Columns.Add(" S.S.S Loan ded ", 165, HorizontalAlignment.Center);
            lv_payroll_transaction.Columns.Add(" Company Loan Bal ", 165, HorizontalAlignment.Center);
            lv_payroll_transaction.Columns.Add(" SSS Loan Bal ", 165, HorizontalAlignment.Center);
            lv_payroll_transaction.Columns.Add(" Pag-Ibig Loan Bal ", 165, HorizontalAlignment.Center);
            lv_payroll_transaction.Columns.Add(" Other ded ", 165, HorizontalAlignment.Center);
            lv_payroll_transaction.Columns.Add(" Wtax ", 165, HorizontalAlignment.Center);
            lv_payroll_transaction.Columns.Add(" Gross Pay ", 165, HorizontalAlignment.Center);
            lv_payroll_transaction.Columns.Add(" Gross Ded ", 165, HorizontalAlignment.Center);
            lv_payroll_transaction.Columns.Add(" Net Pay ", 165, HorizontalAlignment.Center);
            lv_payroll_transaction.Columns.Add(" Absences ", 165, HorizontalAlignment.Center);

        }

        private void btn_add_overtime_Click(object sender, EventArgs e)
        {
            Add_Overtime add_Overtime = new Add_Overtime();

            add_Overtime.label8.Text = label8.Text;
            add_Overtime.label5.Text = lbl_daily_rate.Text;
            add_Overtime.label4.Text = label3.Text;
            add_Overtime.label10.Text = label5.Text;

            add_Overtime.ShowDialog();
            tb_ot_pay.Text = add_Overtime.tb_total_pay.Text;
            lv_payroll_transaction.Columns.Clear();
            display_payroll_transaction("Select * from pay_trans left join employee on pay_trans.emp_no = employee.emp_no left join salary on pay_trans.sal_id = salary.salary_id left join loan on pay_trans.loan_id = loan.loan_id where process_tag = 'False'");
        }

        private void lv_payroll_transaction_Click(object sender, EventArgs e)
        {
            btn_Update.Enabled = true;
            btn_clear.Enabled = true;


            label8.Text = lv_payroll_transaction.SelectedItems[0].SubItems[0].Text;
            tb_employee_id.Text = lv_payroll_transaction.SelectedItems[0].SubItems[5].Text;
            textBox1.Text = lv_payroll_transaction.SelectedItems[0].SubItems[6].Text;
            /*textBox2.Text = lv_payroll_transaction.SelectedItems[0].SubItems[7].Text;
            textBox3.Text = lv_payroll_transaction.SelectedItems[0].SubItems[8].Text;*/
            tb_other_income.Text = lv_payroll_transaction.SelectedItems[0].SubItems[12].Text;
            tb_ot_pay.Text = lv_payroll_transaction.SelectedItems[0].SubItems[13].Text;
            tb_refund.Text = lv_payroll_transaction.SelectedItems[0].SubItems[14].Text;
            tb_other_ded.Text = lv_payroll_transaction.SelectedItems[0].SubItems[32].Text;
            lbl_daily_rate.Text = lv_payroll_transaction.SelectedItems[0].SubItems[18].Text;
            label3.Text = lv_payroll_transaction.SelectedItems[0].SubItems[2].Text;
            label5.Text = lv_payroll_transaction.SelectedItems[0].SubItems[3].Text;
            //textBox6.Text = lv_payroll_transaction.SelectedItems[0].SubItems[9].Text;
            textBox8.Text = lv_payroll_transaction.SelectedItems[0].SubItems[11].Text;
            /*textBox10.Text = lv_payroll_transaction.SelectedItems[0].SubItems[15].Text;*/
            textBox14.Text = lv_payroll_transaction.SelectedItems[0].SubItems[16].Text;
            textBox11.Text = lv_payroll_transaction.SelectedItems[0].SubItems[34].Text;
            textBox12.Text = lv_payroll_transaction.SelectedItems[0].SubItems[36].Text;
            textBox26.Text = lv_payroll_transaction.SelectedItems[0].SubItems[20].Text;
            textBox7.Text = lv_payroll_transaction.SelectedItems[0].SubItems[22].Text;
            textBox9.Text = lv_payroll_transaction.SelectedItems[0].SubItems[24].Text;
            textBox27.Text = lv_payroll_transaction.SelectedItems[0].SubItems[26].Text;
            textBox18.Text = lv_payroll_transaction.SelectedItems[0].SubItems[27].Text;
            textBox17.Text = lv_payroll_transaction.SelectedItems[0].SubItems[28].Text;
            textBox5.Text = lv_payroll_transaction.SelectedItems[0].SubItems[33].Text;
            textBox20.Text = lv_payroll_transaction.SelectedItems[0].SubItems[29].Text;
            textBox21.Text = lv_payroll_transaction.SelectedItems[0].SubItems[30].Text;
            textBox22.Text = lv_payroll_transaction.SelectedItems[0].SubItems[31].Text;
            textBox13.Text = lv_payroll_transaction.SelectedItems[0].SubItems[35].Text;
            dateTimePicker1.Text = lv_payroll_transaction.SelectedItems[0].SubItems[2].Text;
            dateTimePicker2.Text = lv_payroll_transaction.SelectedItems[0].SubItems[3].Text;



            textBox4.Text = lv_payroll_transaction.SelectedItems[0].SubItems[37].Text;
        }

        private void tb_other_income_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (((e.KeyChar < 48 || e.KeyChar > 57) && e.KeyChar != 8 && e.KeyChar != 46))
            {
                e.Handled = true;
                return;
            }


            if (e.KeyChar == 46)
            {
                if ((sender as TextBox).Text.IndexOf(e.KeyChar) != -1)
                    e.Handled = true;
            }
        }

        private void tb_other_income_TextChanged(object sender, EventArgs e)
        {
            if (tb_other_income.Text == "")
            {
                tb_other_income.Text = "0.00";
            }
        }

        private void tb_other_ded_KeyPress(object sender, KeyPressEventArgs e)
        {
            if ((e.KeyChar < 32) || (e.KeyChar >= '0') && (e.KeyChar <= '9') || (e.KeyChar == ',') || (e.KeyChar == '.'))
                return;

            // only evident errors (like 'A' or '&') are restricted
            e.Handled = true;
        }

        public void update_payroll_transaction()
        {


            decimal refund = Convert.ToDecimal(tb_refund.Text);
            decimal other_ded = Convert.ToDecimal(tb_other_ded.Text);
            decimal ot_pay = Convert.ToDecimal(tb_ot_pay.Text);
            decimal other_inc = Convert.ToDecimal(tb_other_income.Text);


            string myconnection = ConfigurationManager.ConnectionStrings["MyConstring"].ConnectionString;
            MySqlConnection connection = new MySqlConnection(myconnection);

            try
            {
                connection.Open();
                cmd = new MySqlCommand();
                cmd.Connection = connection;
                cmd.CommandText = "UPDATE pay_trans SET  deductions = '" + refund + "', other_ded = '" + other_ded + "', ot_pay = '" + ot_pay + "' , other_inc = '" + other_inc + "', absences = '" + textBox4.Text + "'   WHERE pay_trans_id = '" + label8.Text + "'";

                da = new MySqlDataAdapter();
                da.SelectCommand = cmd;
                dt = new DataTable();
                da.Fill(dt);

            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
            finally
            {
                connection.Close();
            }
        }

        private void tb_search_TextChanged(object sender, EventArgs e)
        {
            lv_payroll_transaction.Columns.Clear();
            display_payroll_transaction("Select DISTINCT * FROM pay_trans left join employee on pay_trans.emp_no = employee.emp_no left join salary on pay_trans.sal_id = salary.salary_id left join loan on pay_trans.loan_id = loan.loan_id   WHERE  employee.emp_id like '" + tb_search.Text + "%' or employee.lname like '" + tb_search.Text + "%' or employee.fname like '" + tb_search.Text + "%' or  employee.mname like '" + tb_search.Text + "%'  or  pay_trans.date_from like '" + tb_search.Text + "%' or pay_trans.date_to like '" + tb_search.Text + "%' or pay_trans.date_period like '" + tb_search.Text + "%' or salary.department like '" + tb_search.Text + "%'  or salary.emp_status like '" + tb_search.Text + "%'  or  pay_trans.absences like '" + tb_search.Text + "%' or salary.regular_pay like '" + tb_search.Text + "%' or pay_trans.other_inc like '" + tb_search.Text + "%' or pay_trans.deductions like '" + tb_search.Text + "%'  or salary.tax_sub like '" + tb_search.Text + "%' or salary.daily_rate like '" + tb_search.Text + "%' or salary.daily_hour_rate like '" + tb_search.Text + "%' or pay_trans.wtax like '" + tb_search.Text + "%' or pay_trans.gross_pay like '" + tb_search.Text + "%' or pay_trans.gross_pay like  '" + tb_search.Text + "%' or pay_trans.gross_ded like  '" + tb_search.Text + "%' or pay_trans.net_pay like '" + tb_search.Text + "%' or pay_trans.ot_pay like '" + tb_search.Text + "%' or salary.sss_ee like '" + tb_search.Text + "%' or salary.sss_er like '" + tb_search.Text + "%' or salary.pag_ibig_ee like '" + tb_search.Text + "%' or salary.pag_ibig_er like '" + tb_search.Text + "%' or salary.philhealth_ee like '" + tb_search.Text + "%' or salary.philhealth_er like '" + tb_search.Text + "%' or loan.comp_month_ded like '" + tb_search.Text + "%' or loan.pag_ibig_month_ded like '" + tb_search.Text + "%' or loan.sss_month_ded like '" + tb_search.Text + "%' or loan.comp_bal like '" + tb_search.Text + "%' or loan.pag_ibig_bal like '" + tb_search.Text + "%' or loan.sss_bal like '" + tb_search.Text + "%' or pay_trans.other_ded like '" + tb_search.Text + "%'  or pay_trans.absences like '" + tb_search.Text + "%' and pay_trans.process_tag = 'False'");
        }

        private void btn_print_Click(object sender, EventArgs e)
        {
           
        }

        public void update_grossded_netpay()
        {


            string myconnection = ConfigurationManager.ConnectionStrings["MyConstring"].ConnectionString;
            MySqlConnection connection = new MySqlConnection(myconnection);



            try
            {
                connection.Open();
                cmd = new MySqlCommand();
                cmd.Connection = connection;
                cmd.CommandText = "UPDATE pay_trans left join salary on pay_trans.sal_id = salary.salary_id left join loan on pay_trans.loan_id = loan.loan_id  SET pay_trans.gross_ded = salary.sss_ee + salary.pag_ibig_ee + salary.philhealth_ee + loan.comp_month_ded + loan.pag_ibig_month_ded + loan.sss_month_ded + pay_trans.other_ded + pay_trans.wtax where process_tag = 'false'";
                da = new MySqlDataAdapter();
                da.SelectCommand = cmd;
                dt = new DataTable();
                da.Fill(dt);


            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
            finally
            {
                connection.Close();
            }


        }

        public void update_net_pay()
        {

            string myconnection = ConfigurationManager.ConnectionStrings["MyConstring"].ConnectionString;
            MySqlConnection connection = new MySqlConnection(myconnection);



            try
            {
                connection.Open();
                cmd = new MySqlCommand();
                cmd.Connection = connection;
                cmd.CommandText = "UPDATE pay_trans SET pay_trans.net_pay = pay_trans.gross_pay +  pay_trans.deductions - pay_trans.gross_ded where process_tag = 'false'";
                da = new MySqlDataAdapter();
                da.SelectCommand = cmd;
                dt = new DataTable();
                da.Fill(dt);


            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
            finally
            {
                connection.Close();
            }
        }




        public void update_gross_pay()
        {
            string myconnection = ConfigurationManager.ConnectionStrings["MyConstring"].ConnectionString;
            MySqlConnection connection = new MySqlConnection(myconnection);

            try
            {
                connection.Open();
                cmd = new MySqlCommand();
                cmd.Connection = connection;
                cmd.CommandText = "UPDATE pay_trans left join salary on pay_trans.sal_id = salary.salary_id  SET pay_trans.gross_pay = salary.regular_pay + pay_trans.ot_pay + pay_trans.other_inc WHERE process_tag = 'False'";
                da = new MySqlDataAdapter();
                da.SelectCommand = cmd;
                dt = new DataTable();
                da.Fill(dt);


            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
            finally
            {
                connection.Close();
            }
        }


        public void delete_data()
        {

            //delete monthly tax
            string myconnection2 = ConfigurationManager.ConnectionStrings["MyConstring"].ConnectionString;
            MySqlConnection connection2 = new MySqlConnection(myconnection2);

            try
            {
                connection2.Open();
                cmd = new MySqlCommand();
                cmd.Connection = connection2;
                string query2 = "DELETE FROM monthly_tax";
                cmd.CommandText = query2;
                da = new MySqlDataAdapter();
                da.SelectCommand = cmd;
                dt = new DataTable();
                da.Fill(dt);
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
            finally
            {
                connection2.Close();
            }
            //delete sum table
            string myconnection5 = ConfigurationManager.ConnectionStrings["MyConstring"].ConnectionString;
            MySqlConnection connection5 = new MySqlConnection(myconnection5);

            try
            {
                connection5.Open();
                cmd = new MySqlCommand();
                cmd.Connection = connection5;
                string query2 = "DELETE FROM sum_table";
                cmd.CommandText = query2;
                da = new MySqlDataAdapter();
                da.SelectCommand = cmd;
                dt = new DataTable();
                da.Fill(dt);
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
            finally
            {
                connection5.Close();
            }

            //delete annual_tax_inc
            string myconnection6 = ConfigurationManager.ConnectionStrings["MyConstring"].ConnectionString;
            MySqlConnection connection6 = new MySqlConnection(myconnection6);

            try
            {
                connection6.Open();
                cmd = new MySqlCommand();
                cmd.Connection = connection6;
                string query2 = "DELETE FROM annual_tax_inc";
                cmd.CommandText = query2;
                da = new MySqlDataAdapter();
                da.SelectCommand = cmd;
                dt = new DataTable();
                da.Fill(dt);


            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
            finally
            {
                connection6.Close();
            }


            //delete sum_bonus
            string myconnection7 = ConfigurationManager.ConnectionStrings["MyConstring"].ConnectionString;
            MySqlConnection connection7 = new MySqlConnection(myconnection7);

            try
            {
                connection7.Open();
                cmd = new MySqlCommand();
                cmd.Connection = connection7;
                string query2 = "DELETE FROM sum_bonus";
                cmd.CommandText = query2;
                da = new MySqlDataAdapter();
                da.SelectCommand = cmd;
                dt = new DataTable();
                da.Fill(dt);
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
            finally
            {
                connection7.Close();
            }
        }

        public void annual_tax_inc()
        {
            int ctr2 = 24;


            string myconnection = ConfigurationManager.ConnectionStrings["MyConstring"].ConnectionString;
            MySqlConnection connection = new MySqlConnection(myconnection);

            try
            {
                connection.Open();
                cmd = new MySqlCommand();
                cmd.Connection = connection;
                cmd.CommandText = "Select * from pay_trans left join employee on pay_trans.emp_no = employee.emp_no left join salary on pay_trans.sal_id = salary.salary_id left join loan on pay_trans.loan_id = loan.loan_id LEFT JOIN sum_table on sum_table.emp_id = pay_trans.emp_id left join sum_bonus on sum_bonus.emp_id = pay_trans.emp_id where pay_trans.process_tag = 'False' and salary.emp_status = 'Regular'";
                da = new MySqlDataAdapter();
                da.SelectCommand = cmd;
                dt = new DataTable();
                da.Fill(dt);


                for (int i = 0; i < dt.Rows.Count; i++)
                {
                    DataRow dr = dt.Rows[i];

                    string name = (dr["lname"].ToString());
                    string pay_trans_id = (dr["pay_trans_id"].ToString());
                    string basic_pay = (dr["salary"].ToString());
                    string ot_pay = (dr["ot_pay"].ToString());
                    string other_income = (dr["other_inc"].ToString());
                    string comp_loan = (dr["comp_month_ded"].ToString());
                    string sss_loan = (dr["sss_month_ded"].ToString());
                    string pag_ibig_loan = (dr["pag_ibig_month_ded"].ToString());
                    string tax_subsidy = (dr["tax_sub"].ToString());
                    string bonus1 = (dr["13thmonthpay"].ToString());
                    string bonus2 = (dr["14thmonthpay"].ToString());
                    string sss_ee = (dr["sss_ee"].ToString());
                    string pag_ibig_ee = (dr["pag_ibig_ee"].ToString());
                    string philhealth_ee = (dr["philhealth_ee"].ToString());
                    string non_tax = (dr["mntbonus"].ToString());

                    string ctr = (dr["max_ctr"].ToString());
                    string bonus_net_pay = (dr["bonus_netpay"].ToString());
                    string gross_pay_details = (dr["sum_gross"].ToString());
                    string wtax_pay_details = (dr["sum_tax"].ToString());

                    decimal cont_month_sal = Convert.ToDecimal(basic_pay);
                    decimal cont_tax_sub = Convert.ToDecimal(tax_subsidy);
                    decimal cont_ot_pay = Convert.ToDecimal(ot_pay);
                    decimal cont_other_income = Convert.ToDecimal(other_income);
                    decimal cont_comp_loan = Convert.ToDecimal(comp_loan);
                    decimal cont_sss_loan = Convert.ToDecimal(sss_loan);
                    decimal cont_pag_ibig_loan = Convert.ToDecimal(pag_ibig_loan);
                    decimal cont_bonus1 = Convert.ToDecimal(bonus1);
                    decimal cont_bonus2 = Convert.ToDecimal(bonus2);
                    decimal cont_sss_ee = Convert.ToDecimal(sss_ee);
                    decimal cont_pag_ibig_ee = Convert.ToDecimal(pag_ibig_ee);
                    decimal cont_philhealth_ee = Convert.ToDecimal(philhealth_ee);
                    decimal cont_non_tax = Convert.ToDecimal(non_tax);


                    int ctr1 = Convert.ToInt32(ctr);

                    decimal total_gross_pay_details1 = Convert.ToDecimal(gross_pay_details);
                    decimal total_wtax_pay_details1 = Convert.ToDecimal(wtax_pay_details);
                    decimal total_bonus_netpay1 = Convert.ToDecimal(bonus_net_pay);


                    //total bonus
                    decimal total_bonus = cont_bonus1 + cont_bonus2;


                    //bonus - existing bonus of employee
                    decimal total_bonus2 = total_bonus - total_bonus_netpay1;


                    //counter
                    int total_ctr = ctr2 - ctr1;


                    //semi_monthly * counter
                    decimal semi_monthly = (cont_month_sal / 2) * total_ctr;


                    //semi_monthly_tax * 2
                    decimal semi_monthly_tax_sub = (cont_tax_sub / 2) * total_ctr;


                    //decimal ot pay / 24
                    decimal cal_ot_pay = cont_ot_pay / 24;


                    //decimal other income / 24

                    decimal cal_other_inc = cont_other_income / 24;


                    //overall_annual_income
                    decimal total_overall_income = semi_monthly + cal_ot_pay + cal_other_inc + semi_monthly_tax_sub;


                    //total_taxable_income
                    decimal total_taxable_income = total_gross_pay_details1 + total_overall_income + total_bonus2;



                    //sss,pag - ibig, philheath ded
                    decimal monthly_sss = cont_sss_ee * 2;
                    decimal total_annual_sss = monthly_sss * 12;



                    decimal monthly_pag_ibig = cont_pag_ibig_ee * 2;
                    decimal total_annual_pag_ibig = monthly_pag_ibig * 12;



                    decimal monthly_philhealth = cont_philhealth_ee * 2;
                    decimal total_annual_philhealth = monthly_philhealth * 12;


                    //Total cOntribution deduction
                    decimal total_ded = total_annual_sss + total_annual_pag_ibig + total_annual_philhealth;



                    //total deduction = non tax - sss,pag - ibig, philhealth
                    decimal overall_ded = cont_non_tax + total_ded;



                    //annual taxable income
                    decimal annual_taxable_income = total_taxable_income - overall_ded;



                    string myconnection4 = ConfigurationManager.ConnectionStrings["MyConstring"].ConnectionString;
                    MySqlConnection connection4 = new MySqlConnection(myconnection4);


                    string insertQuery3 = "INSERT INTO annual_tax_inc(pay_trans_id, annual_tax_inc, pay_details_wtax, max_ctr)Values('" + pay_trans_id + "', '" + annual_taxable_income + "', '" + total_wtax_pay_details1 + "', '" + total_ctr + "')";


                    connection4.Open();
                    MySqlCommand mySqlCommand3 = new MySqlCommand(insertQuery3, connection4);



                    if (mySqlCommand3.ExecuteNonQuery() == 1)
                    {
                        //MessageBox.Show("Successfully Process", "Payroll Processing", MessageBoxButtons.OK, MessageBoxIcon.Information);

                    }
                    connection4.Close();

                }

            }

            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
            finally
            {
                connection.Close();
            }

        }






        public void Regular_tax()
        {
            //Monthly Tax Regular Employee

            string myconnection3 = ConfigurationManager.ConnectionStrings["MyConstring"].ConnectionString;
            MySqlConnection connection3 = new MySqlConnection(myconnection3);



            cmd = new MySqlCommand();
            cmd.Connection = connection3;
            cmd.CommandText = "SELECT * FROM tax join annual_tax_inc WHERE tax.min_amnt <= annual_tax_inc.annual_tax_inc AND tax.max_amnt >= annual_tax_inc.annual_tax_inc";
            da = new MySqlDataAdapter();
            da.SelectCommand = cmd;
            dt = new DataTable();
            da.Fill(dt);


            for (int x = 0; x < dt.Rows.Count; x++)
            {
                DataRow dr2 = dt.Rows[x];



                string tax_due = (dr2["tax_due"].ToString());
                string less_amnt = (dr2["less_amnt"].ToString());
                string percent = (dr2["percent"].ToString());
                string pay_trans_id = (dr2["pay_trans_id"].ToString());

                string annual_tax_income = (dr2["annual_tax_inc"].ToString());
                string wtax_pay_details = (dr2["pay_details_wtax"].ToString());
                string ctr = (dr2["max_ctr"].ToString());


                decimal temp_tax1 = Convert.ToDecimal(tax_due);
                decimal total_less_amnt = Convert.ToDecimal(less_amnt);
                decimal total_percentage = Convert.ToDecimal(percent);
                decimal total_annual_taxable_income = Convert.ToDecimal(annual_tax_income);
                decimal total_wtax_pay_details = Convert.ToDecimal(wtax_pay_details);
                int max_ctr = Convert.ToInt32(ctr);



                decimal get_percentage = total_percentage / 100;

                decimal temp_tax2 = (total_annual_taxable_income - total_less_amnt) * get_percentage;


                decimal total_annual_tax = temp_tax1 + temp_tax2;
                decimal annual_tax = total_annual_tax - total_wtax_pay_details;
                decimal semi_monthly_tax = annual_tax / max_ctr;




                string myconnection4 = ConfigurationManager.ConnectionStrings["MyConstring"].ConnectionString;
                MySqlConnection connection4 = new MySqlConnection(myconnection4);


                string insertQuery3 = "INSERT INTO monthly_tax(pay_trans_id, semi_monthly_tax)Values('" + pay_trans_id + "', '" + semi_monthly_tax + "')";


                connection4.Open();
                MySqlCommand mySqlCommand3 = new MySqlCommand(insertQuery3, connection4);



                if (mySqlCommand3.ExecuteNonQuery() == 1)
                {
                    //MessageBox.Show("Successfully Process", "Payroll Processing", MessageBoxButtons.OK, MessageBoxIcon.Information);

                }
                connection4.Close();


            }
            connection3.Close();
        }



        public void Consultant_tax()
        {
            string myconnection = ConfigurationManager.ConnectionStrings["MyConstring"].ConnectionString;
            MySqlConnection connection = new MySqlConnection(myconnection);



            cmd = new MySqlCommand();
            cmd.Connection = connection;
            cmd.CommandText = "SELECT  * FROM  pay_trans left join salary on pay_trans.sal_id = salary.salary_id left join employee on employee.emp_no = pay_trans.emp_no WHERE pay_trans.process_tag = 'False'  AND salary.emp_status = 'Consultant'";
            da = new MySqlDataAdapter();
            da.SelectCommand = cmd;
            dt = new DataTable();
            da.Fill(dt);

            for (int i = 0; i < dt.Rows.Count; i++)
            {
                DataRow dr = dt.Rows[i];

                string emp_no = (dr["pay_trans_id"].ToString());
                string salary = (dr["gross_pay"].ToString());
                string tax = (dr["wtax_per"].ToString());


                decimal basic_pay = Convert.ToDecimal(salary);
                decimal tax_per = Convert.ToDecimal(tax);


                decimal total_tax = basic_pay * tax_per;




                string myconnection2 = ConfigurationManager.ConnectionStrings["MyConstring"].ConnectionString;
                MySqlConnection connection2 = new MySqlConnection(myconnection2);


                string insertQuery2 = "INSERT INTO monthly_tax(pay_trans_id, semi_monthly_tax)Values('" + emp_no + "',  '" + total_tax + "')";


                connection2.Open();
                MySqlCommand mySqlCommand2 = new MySqlCommand(insertQuery2, connection2);



                if (mySqlCommand2.ExecuteNonQuery() == 1)
                {
                    //MessageBox.Show("Successfully Process", "Payroll Processing", MessageBoxButtons.OK, MessageBoxIcon.Information);

                }
                connection2.Close();


            }
            connection.Close();
        }

        public void update_wtax()
        {
            string myconnection = ConfigurationManager.ConnectionStrings["MyConstring"].ConnectionString;
            MySqlConnection connection = new MySqlConnection(myconnection);



            try
            {
                connection.Open();
                cmd = new MySqlCommand();
                cmd.Connection = connection;
                cmd.CommandText = "UPDATE pay_trans SET pay_trans.wtax = (SELECT monthly_tax.semi_monthly_tax from monthly_tax WHERE monthly_tax.pay_trans_id = pay_trans.pay_trans_id) where pay_trans.process_tag = 'False'";
                da = new MySqlDataAdapter();
                da.SelectCommand = cmd;
                dt = new DataTable();
                da.Fill(dt);

            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
            finally
            {
                connection.Close();
            }


        }

        public void sum_table()
        {
            string myconnection = ConfigurationManager.ConnectionStrings["MyConstring"].ConnectionString;
            MySqlConnection connection = new MySqlConnection(myconnection);



            cmd = new MySqlCommand();
            cmd.Connection = connection;
            cmd.CommandText = "SELECT pay_trans.emp_id, SUM(pay_details.gross_inc) AS gross_pay_details, SUM(pay_details.wtax) AS wtax_pay_details, COALESCE (MAX(pay_details.ctr), 0) as pay_details_ctr FROM pay_trans LEFT JOIN pay_details on pay_trans.emp_id = pay_details.emp_id where pay_trans.process_tag = 'False' GROUP BY pay_trans.emp_id";
            da = new MySqlDataAdapter();
            da.SelectCommand = cmd;
            dt = new DataTable();
            da.Fill(dt);

            for (int i = 0; i < dt.Rows.Count; i++)
            {
                DataRow dr = dt.Rows[i];

                string emp_id = (dr["emp_id"].ToString());
                string ctr = (dr["pay_details_ctr"].ToString());
                string gross_pay_details = (dr["gross_pay_details"].ToString());
                string wtax_pay_details = (dr["wtax_pay_details"].ToString());





                if (wtax_pay_details == "")
                {
                    total_details_total_wtax = 0;
                }
                else
                {
                    total_details_total_wtax = Convert.ToDecimal(wtax_pay_details);

                }

                if (gross_pay_details == "")
                {
                    total_gross_pay_details = 0;


                }
                else
                {
                    total_gross_pay_details = Convert.ToDecimal(gross_pay_details);


                }



                string myconnection3 = ConfigurationManager.ConnectionStrings["MyConstring"].ConnectionString;
                MySqlConnection connection3 = new MySqlConnection(myconnection3);



                string insertQuery2 = "INSERT INTO sum_table(emp_id, sum_tax, sum_gross, max_ctr)Values('" + emp_id + "',  '" + total_details_total_wtax + "', '" + total_gross_pay_details + "', '" + ctr + "')";

                connection3.Open();
                MySqlCommand mySqlCommand2 = new MySqlCommand(insertQuery2, connection3);


                if (mySqlCommand2.ExecuteNonQuery() == 1)
                {
                    //MessageBox.Show("Successfully Process", "Payroll Processing", MessageBoxButtons.OK, MessageBoxIcon.Information);

                }
                connection3.Close();
            }

            connection.Close();
        }

        public void sum_bonus()
        {
            string myconnection = ConfigurationManager.ConnectionStrings["MyConstring"].ConnectionString;
            MySqlConnection connection = new MySqlConnection(myconnection);



            cmd = new MySqlCommand();
            cmd.Connection = connection;
            cmd.CommandText = "SELECT pay_trans.emp_id, SUM(bonus_details.netpay) as bonus_netpay FROM pay_trans LEFT JOIN bonus_details ON pay_trans.emp_id = bonus_details.emp_id WHERE process_tag = 'False' GROUP BY pay_trans.emp_id";
            da = new MySqlDataAdapter();
            da.SelectCommand = cmd;
            dt = new DataTable();
            da.Fill(dt);

            for (int i = 0; i < dt.Rows.Count; i++)
            {
                DataRow dr = dt.Rows[i];

                string emp_id = (dr["emp_id"].ToString());
                string bonus_details_netpay = (dr["bonus_netpay"].ToString());

                if (bonus_details_netpay == "")
                {
                    total_bonus_netpay = 0;
                }
                else
                {
                    total_bonus_netpay = Convert.ToDecimal(bonus_details_netpay);

                }

                string myconnection3 = ConfigurationManager.ConnectionStrings["MyConstring"].ConnectionString;
                MySqlConnection connection3 = new MySqlConnection(myconnection3);

                string insertQuery2 = "INSERT INTO sum_bonus(emp_id, bonus_netpay)Values('" + emp_id + "',  '" + total_bonus_netpay + "')";


                connection3.Open();
                MySqlCommand mySqlCommand2 = new MySqlCommand(insertQuery2, connection3);



                if (mySqlCommand2.ExecuteNonQuery() == 1)
                {
                    //MessageBox.Show("Successfully Process", "Payroll Processing", MessageBoxButtons.OK, MessageBoxIcon.Information);

                }
                connection3.Close();

            }
            connection.Close();
        }
        private void ToExcel()
        {
            StringBuilder sb = new StringBuilder();
            Microsoft.Office.Interop.Excel.Application app = new Microsoft.Office.Interop.Excel.Application();
            app.Visible = true;
            Microsoft.Office.Interop.Excel.Workbook wb = app.Workbooks.Add(1);
            Microsoft.Office.Interop.Excel.Worksheet ws = (Microsoft.Office.Interop.Excel.Worksheet)wb.Worksheets[1];
            int i = 1;
            int i2 = 2;
            int x = 1;
            int x2 = 1;
            foreach (ColumnHeader ch in lv_payroll_transaction.Columns)
            {
                ws.Cells[x2, x] = ch.Text;
                x++;
            }

            foreach (ListViewItem lvi in lv_payroll_transaction.Items)
            {
                i = 1;
                foreach (ListViewItem.ListViewSubItem lvs in lvi.SubItems)
                {
                    ws.Cells[i2, i] = lvs.Text;
                    ws.Cells.Select();
                    ws.Cells.EntireColumn.AutoFit();
                    i++;
                }
                i2++;
            }
        }

        private void tb_ot_pay_TextChanged(object sender, EventArgs e)
        {
            if (tb_ot_pay.Text == "")
            {
                tb_ot_pay.Text = "0.00";
            }
        }

        private void tb_refund_TextChanged(object sender, EventArgs e)
        {
            if (tb_refund.Text == "")
            {
                tb_refund.Text = "0.00";
            }
        }

        private void tb_other_ded_TextChanged(object sender, EventArgs e)
        {
            if (tb_other_ded.Text == "")
            {
                tb_other_ded.Text = "0.00";
            }
        }

        private void tb_ot_pay_KeyPress(object sender, KeyPressEventArgs e)
        {
            if ((e.KeyChar < 32) || (e.KeyChar >= '0') && (e.KeyChar <= '9') || (e.KeyChar == ',') || (e.KeyChar == '.'))
                return;

            // only evident errors (like 'A' or '&') are restricted
            e.Handled = true;
        }

        private void btn_Update_Click(object sender, EventArgs e)
        {
            if (tb_employee_id.Text == "")
            {

                MessageBox.Show("Please Select Employee", "Warning", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
            else
            {
                if (btn_Update.Text == "EDIT")
                {

                    btn_Update.Text = "SAVE";

                    tb_ot_pay.Enabled = true;
                    tb_other_income.Enabled = true;
                    tb_other_ded.Enabled = true;
                    btn_add_overtime.Enabled = true;
                    tb_refund.Enabled = true;
                    textBox4.Enabled = true;
                }
                else
                {
                    DialogResult result = MessageBox.Show("Do you want to Update '" + tb_employee_id.Text + "'?", "Update Payroll Transaction", MessageBoxButtons.YesNoCancel, MessageBoxIcon.Question);
                    if (result == DialogResult.Yes)
                    {
                        delete_data();
                        update_payroll_transaction();
                        sum_table();
                        sum_bonus();
                        annual_tax_inc();
                        Regular_tax();
                        update_gross_pay();
                        Consultant_tax();
                        update_wtax();
                        lv_payroll_transaction.Columns.Clear();
                        update_grossded_netpay();
                        update_net_pay();

                        display_payroll_transaction("Select * from pay_trans left join employee on pay_trans.emp_no = employee.emp_no left join salary on pay_trans.sal_id = salary.salary_id left join loan on pay_trans.loan_id = loan.loan_id where process_tag = 'False'");
                        MessageBox.Show("Successfully Update!", "Update Payroll Transaction", MessageBoxButtons.OK, MessageBoxIcon.Information);
                    }
                }
            }
        }

        private void btn_clear_Click(object sender, EventArgs e)
        {
            btn_Update.Text = "EDIT";

            btn_clear.Enabled = false;
            btn_Update.Enabled = false;
            tb_other_ded.Enabled = false;
            tb_refund.Enabled = false;
            textBox4.Enabled = false;
            tb_other_income.Enabled = false;
            btn_add_overtime.Enabled = false;


            textBox1.Text = "";
            textBox2.Text = "";
            //textBox3.Text = "";
            tb_employee_id.Text = "";

            tb_other_income.Text = "0.00";
            tb_refund.Text = "0.00";

            tb_ot_pay.Text = "0.00";
            tb_other_ded.Text = "0.00";

            tb_other_income.Text = "0.00";
            tb_other_ded.Text = "0.00";
            tb_refund.Text = "0.00";
            lbl_daily_rate.Text = "0.00";
        }

        private void btn_export_Click(object sender, EventArgs e)
        {
            ToExcel();
        }
    }
}
