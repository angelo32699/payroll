﻿namespace Payroll
{
    partial class Philhealth
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(Philhealth));
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.btn_delete = new Payroll.RJButtons.RJButton();
            this.btn_Update = new Payroll.RJButtons.RJButton();
            this.btn_add = new Payroll.RJButtons.RJButton();
            this.label6 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.label5 = new System.Windows.Forms.Label();
            this.tb_employer_contribution = new System.Windows.Forms.TextBox();
            this.tb_emp_cont = new System.Windows.Forms.TextBox();
            this.label2 = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.tb_minimum_amount = new System.Windows.Forms.TextBox();
            this.label1 = new System.Windows.Forms.Label();
            this.tb_percent = new System.Windows.Forms.TextBox();
            this.tb_maximum_amount = new System.Windows.Forms.TextBox();
            this.lv_Philhealth = new System.Windows.Forms.ListView();
            this.label13 = new System.Windows.Forms.Label();
            this.tb_search = new System.Windows.Forms.TextBox();
            this.btn_export = new Payroll.RJButtons.RJButton();
            this.groupBox1.SuspendLayout();
            this.SuspendLayout();
            // 
            // groupBox1
            // 
            this.groupBox1.Controls.Add(this.btn_delete);
            this.groupBox1.Controls.Add(this.btn_Update);
            this.groupBox1.Controls.Add(this.btn_add);
            this.groupBox1.Controls.Add(this.label6);
            this.groupBox1.Controls.Add(this.label3);
            this.groupBox1.Controls.Add(this.label5);
            this.groupBox1.Controls.Add(this.tb_employer_contribution);
            this.groupBox1.Controls.Add(this.tb_emp_cont);
            this.groupBox1.Controls.Add(this.label2);
            this.groupBox1.Controls.Add(this.label4);
            this.groupBox1.Controls.Add(this.tb_minimum_amount);
            this.groupBox1.Controls.Add(this.label1);
            this.groupBox1.Controls.Add(this.tb_percent);
            this.groupBox1.Controls.Add(this.tb_maximum_amount);
            this.groupBox1.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.groupBox1.Location = new System.Drawing.Point(9, 9);
            this.groupBox1.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Padding = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.groupBox1.Size = new System.Drawing.Size(1088, 188);
            this.groupBox1.TabIndex = 78;
            this.groupBox1.TabStop = false;
            this.groupBox1.Text = "Philhealth Table";
            // 
            // btn_delete
            // 
            this.btn_delete.BackColor = System.Drawing.Color.Transparent;
            this.btn_delete.BackgroundColor = System.Drawing.Color.Transparent;
            this.btn_delete.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(192)))), ((int)(((byte)(0)))), ((int)(((byte)(0)))));
            this.btn_delete.BorderRadius = 8;
            this.btn_delete.BorderSize = 1;
            this.btn_delete.Enabled = false;
            this.btn_delete.FlatAppearance.BorderSize = 0;
            this.btn_delete.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btn_delete.Font = new System.Drawing.Font("Microsoft Sans Serif", 8F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btn_delete.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(192)))), ((int)(((byte)(0)))), ((int)(((byte)(0)))));
            this.btn_delete.Image = global::Payroll.Properties.Resources.icons8_delete_30;
            this.btn_delete.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.btn_delete.Location = new System.Drawing.Point(650, 118);
            this.btn_delete.Name = "btn_delete";
            this.btn_delete.Size = new System.Drawing.Size(175, 50);
            this.btn_delete.TabIndex = 191;
            this.btn_delete.Text = "DELETE";
            this.btn_delete.TextColor = System.Drawing.Color.FromArgb(((int)(((byte)(192)))), ((int)(((byte)(0)))), ((int)(((byte)(0)))));
            this.btn_delete.UseVisualStyleBackColor = false;
            this.btn_delete.Click += new System.EventHandler(this.btn_delete_Click);
            this.btn_delete.MouseEnter += new System.EventHandler(this.btn_delete_MouseEnter);
            this.btn_delete.MouseLeave += new System.EventHandler(this.btn_delete_MouseLeave);
            this.btn_delete.MouseHover += new System.EventHandler(this.btn_delete_MouseHover);
            // 
            // btn_Update
            // 
            this.btn_Update.BackColor = System.Drawing.Color.Transparent;
            this.btn_Update.BackgroundColor = System.Drawing.Color.Transparent;
            this.btn_Update.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(192)))), ((int)(((byte)(0)))), ((int)(((byte)(0)))));
            this.btn_Update.BorderRadius = 8;
            this.btn_Update.BorderSize = 1;
            this.btn_Update.Enabled = false;
            this.btn_Update.FlatAppearance.BorderSize = 0;
            this.btn_Update.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btn_Update.Font = new System.Drawing.Font("Microsoft Sans Serif", 8F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btn_Update.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(192)))), ((int)(((byte)(0)))), ((int)(((byte)(0)))));
            this.btn_Update.Image = global::Payroll.Properties.Resources.Button_UPDATE_Red__4___1___1_;
            this.btn_Update.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.btn_Update.Location = new System.Drawing.Point(461, 118);
            this.btn_Update.Name = "btn_Update";
            this.btn_Update.Size = new System.Drawing.Size(175, 50);
            this.btn_Update.TabIndex = 189;
            this.btn_Update.Text = "EDIT";
            this.btn_Update.TextColor = System.Drawing.Color.FromArgb(((int)(((byte)(192)))), ((int)(((byte)(0)))), ((int)(((byte)(0)))));
            this.btn_Update.UseVisualStyleBackColor = false;
            this.btn_Update.Click += new System.EventHandler(this.btn_Update_Click);
            this.btn_Update.MouseEnter += new System.EventHandler(this.btn_Update_MouseEnter);
            this.btn_Update.MouseLeave += new System.EventHandler(this.btn_Update_MouseLeave);
            this.btn_Update.MouseHover += new System.EventHandler(this.btn_Update_MouseHover);
            // 
            // btn_add
            // 
            this.btn_add.BackColor = System.Drawing.Color.Transparent;
            this.btn_add.BackgroundColor = System.Drawing.Color.Transparent;
            this.btn_add.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(192)))), ((int)(((byte)(0)))), ((int)(((byte)(0)))));
            this.btn_add.BorderRadius = 8;
            this.btn_add.BorderSize = 1;
            this.btn_add.FlatAppearance.BorderSize = 0;
            this.btn_add.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btn_add.Font = new System.Drawing.Font("Microsoft Sans Serif", 8F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btn_add.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(192)))), ((int)(((byte)(0)))), ((int)(((byte)(0)))));
            this.btn_add.Image = global::Payroll.Properties.Resources.Button_ADD_Red__1___1_;
            this.btn_add.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.btn_add.Location = new System.Drawing.Point(268, 118);
            this.btn_add.Name = "btn_add";
            this.btn_add.Size = new System.Drawing.Size(175, 50);
            this.btn_add.TabIndex = 188;
            this.btn_add.Text = "ADD";
            this.btn_add.TextColor = System.Drawing.Color.FromArgb(((int)(((byte)(192)))), ((int)(((byte)(0)))), ((int)(((byte)(0)))));
            this.btn_add.UseVisualStyleBackColor = false;
            this.btn_add.Click += new System.EventHandler(this.btn_add_Click);
            this.btn_add.MouseEnter += new System.EventHandler(this.btn_add_MouseEnter);
            this.btn_add.MouseLeave += new System.EventHandler(this.btn_add_MouseLeave);
            this.btn_add.MouseHover += new System.EventHandler(this.btn_add_MouseHover);
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label6.Location = new System.Drawing.Point(4, 92);
            this.label6.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(18, 20);
            this.label6.TabIndex = 82;
            this.label6.Text = "0";
            this.label6.Visible = false;
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Font = new System.Drawing.Font("Arial", 9.75F);
            this.label3.Location = new System.Drawing.Point(479, 32);
            this.label3.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(207, 23);
            this.label3.TabIndex = 80;
            this.label3.Text = "Employee Contribution";
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Font = new System.Drawing.Font("Arial", 9.75F);
            this.label5.Location = new System.Drawing.Point(719, 32);
            this.label5.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(203, 23);
            this.label5.TabIndex = 79;
            this.label5.Text = "Employer Contribution";
            // 
            // tb_employer_contribution
            // 
            this.tb_employer_contribution.Enabled = false;
            this.tb_employer_contribution.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.tb_employer_contribution.ForeColor = System.Drawing.Color.Black;
            this.tb_employer_contribution.Location = new System.Drawing.Point(723, 59);
            this.tb_employer_contribution.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.tb_employer_contribution.Name = "tb_employer_contribution";
            this.tb_employer_contribution.Size = new System.Drawing.Size(224, 28);
            this.tb_employer_contribution.TabIndex = 77;
            this.tb_employer_contribution.Text = "0.00";
            this.tb_employer_contribution.TextChanged += new System.EventHandler(this.tb_employer_contribution_TextChanged);
            this.tb_employer_contribution.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.tb_employer_contribution_KeyPress);
            // 
            // tb_emp_cont
            // 
            this.tb_emp_cont.Enabled = false;
            this.tb_emp_cont.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.tb_emp_cont.ForeColor = System.Drawing.Color.Black;
            this.tb_emp_cont.Location = new System.Drawing.Point(483, 59);
            this.tb_emp_cont.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.tb_emp_cont.Name = "tb_emp_cont";
            this.tb_emp_cont.Size = new System.Drawing.Size(224, 28);
            this.tb_emp_cont.TabIndex = 78;
            this.tb_emp_cont.Text = "0.00";
            this.tb_emp_cont.TextChanged += new System.EventHandler(this.tb_emp_cont_TextChanged);
            this.tb_emp_cont.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.tb_emp_cont_KeyPress);
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Font = new System.Drawing.Font("Arial", 9.75F);
            this.label2.Location = new System.Drawing.Point(249, 32);
            this.label2.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(164, 23);
            this.label2.TabIndex = 72;
            this.label2.Text = "Maximum Amount";
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Font = new System.Drawing.Font("Arial", 9.75F);
            this.label4.Location = new System.Drawing.Point(955, 32);
            this.label4.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(78, 23);
            this.label4.TabIndex = 71;
            this.label4.Text = "Percent";
            // 
            // tb_minimum_amount
            // 
            this.tb_minimum_amount.Enabled = false;
            this.tb_minimum_amount.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.tb_minimum_amount.ForeColor = System.Drawing.Color.Black;
            this.tb_minimum_amount.Location = new System.Drawing.Point(8, 60);
            this.tb_minimum_amount.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.tb_minimum_amount.Name = "tb_minimum_amount";
            this.tb_minimum_amount.Size = new System.Drawing.Size(224, 28);
            this.tb_minimum_amount.TabIndex = 64;
            this.tb_minimum_amount.Text = "0.00";
            this.tb_minimum_amount.TextChanged += new System.EventHandler(this.tb_minimum_amount_TextChanged);
            this.tb_minimum_amount.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.tb_minimum_amount_KeyPress);
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("Arial", 9.75F);
            this.label1.Location = new System.Drawing.Point(8, 32);
            this.label1.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(158, 23);
            this.label1.TabIndex = 68;
            this.label1.Text = "Minimum Amount";
            // 
            // tb_percent
            // 
            this.tb_percent.Enabled = false;
            this.tb_percent.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.tb_percent.ForeColor = System.Drawing.Color.Black;
            this.tb_percent.Location = new System.Drawing.Point(958, 60);
            this.tb_percent.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.tb_percent.Name = "tb_percent";
            this.tb_percent.Size = new System.Drawing.Size(121, 28);
            this.tb_percent.TabIndex = 67;
            this.tb_percent.Text = "0.00";
            this.tb_percent.TextChanged += new System.EventHandler(this.tb_percent_TextChanged);
            this.tb_percent.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.tb_percent_KeyPress);
            // 
            // tb_maximum_amount
            // 
            this.tb_maximum_amount.Enabled = false;
            this.tb_maximum_amount.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.tb_maximum_amount.ForeColor = System.Drawing.Color.Black;
            this.tb_maximum_amount.Location = new System.Drawing.Point(244, 60);
            this.tb_maximum_amount.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.tb_maximum_amount.Name = "tb_maximum_amount";
            this.tb_maximum_amount.Size = new System.Drawing.Size(224, 28);
            this.tb_maximum_amount.TabIndex = 65;
            this.tb_maximum_amount.Text = "0.00";
            this.tb_maximum_amount.TextChanged += new System.EventHandler(this.tb_maximum_amount_TextChanged);
            this.tb_maximum_amount.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.tb_maximum_amount_KeyPress);
            // 
            // lv_Philhealth
            // 
            this.lv_Philhealth.Font = new System.Drawing.Font("Arial", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lv_Philhealth.FullRowSelect = true;
            this.lv_Philhealth.GridLines = true;
            this.lv_Philhealth.HideSelection = false;
            this.lv_Philhealth.Location = new System.Drawing.Point(10, 242);
            this.lv_Philhealth.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.lv_Philhealth.Name = "lv_Philhealth";
            this.lv_Philhealth.Size = new System.Drawing.Size(1087, 333);
            this.lv_Philhealth.TabIndex = 77;
            this.lv_Philhealth.UseCompatibleStateImageBehavior = false;
            this.lv_Philhealth.View = System.Windows.Forms.View.Details;
            this.lv_Philhealth.Click += new System.EventHandler(this.lv_Philhealth_Click);
            // 
            // label13
            // 
            this.label13.AutoSize = true;
            this.label13.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label13.Location = new System.Drawing.Point(721, 212);
            this.label13.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label13.Name = "label13";
            this.label13.Size = new System.Drawing.Size(72, 22);
            this.label13.TabIndex = 80;
            this.label13.Text = "Search:";
            // 
            // tb_search
            // 
            this.tb_search.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.tb_search.ForeColor = System.Drawing.Color.Black;
            this.tb_search.Location = new System.Drawing.Point(800, 208);
            this.tb_search.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.tb_search.Name = "tb_search";
            this.tb_search.Size = new System.Drawing.Size(294, 28);
            this.tb_search.TabIndex = 79;
            this.tb_search.TextChanged += new System.EventHandler(this.tb_search_TextChanged);
            // 
            // btn_export
            // 
            this.btn_export.BackColor = System.Drawing.Color.Transparent;
            this.btn_export.BackgroundColor = System.Drawing.Color.Transparent;
            this.btn_export.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(192)))), ((int)(((byte)(0)))), ((int)(((byte)(0)))));
            this.btn_export.BorderRadius = 8;
            this.btn_export.BorderSize = 1;
            this.btn_export.FlatAppearance.BorderSize = 0;
            this.btn_export.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btn_export.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btn_export.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(192)))), ((int)(((byte)(0)))), ((int)(((byte)(0)))));
            this.btn_export.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.btn_export.Location = new System.Drawing.Point(878, 581);
            this.btn_export.Name = "btn_export";
            this.btn_export.Size = new System.Drawing.Size(219, 44);
            this.btn_export.TabIndex = 186;
            this.btn_export.Text = "Export to Excel";
            this.btn_export.TextColor = System.Drawing.Color.FromArgb(((int)(((byte)(192)))), ((int)(((byte)(0)))), ((int)(((byte)(0)))));
            this.btn_export.UseVisualStyleBackColor = false;
            this.btn_export.Click += new System.EventHandler(this.btn_export_Click);
            this.btn_export.MouseEnter += new System.EventHandler(this.btn_export_MouseEnter);
            this.btn_export.MouseLeave += new System.EventHandler(this.btn_export_MouseLeave);
            this.btn_export.MouseHover += new System.EventHandler(this.btn_export_MouseHover);
            // 
            // Philhealth
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(9F, 20F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.AutoSize = true;
            this.ClientSize = new System.Drawing.Size(1104, 632);
            this.Controls.Add(this.btn_export);
            this.Controls.Add(this.groupBox1);
            this.Controls.Add(this.lv_Philhealth);
            this.Controls.Add(this.label13);
            this.Controls.Add(this.tb_search);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedToolWindow;
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.MaximizeBox = false;
            this.Name = "Philhealth";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Philhealth Table";
            this.Load += new System.EventHandler(this.Philhealth_Load);
            this.groupBox1.ResumeLayout(false);
            this.groupBox1.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.GroupBox groupBox1;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label4;
        internal System.Windows.Forms.TextBox tb_minimum_amount;
        private System.Windows.Forms.Label label1;
        internal System.Windows.Forms.TextBox tb_percent;
        internal System.Windows.Forms.TextBox tb_maximum_amount;
        private System.Windows.Forms.ListView lv_Philhealth;
        private System.Windows.Forms.Label label13;
        private System.Windows.Forms.TextBox tb_search;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label label5;
        internal System.Windows.Forms.TextBox tb_employer_contribution;
        internal System.Windows.Forms.TextBox tb_emp_cont;
        private System.Windows.Forms.Label label6;
        private RJButtons.RJButton btn_add;
        private RJButtons.RJButton btn_Update;
        private RJButtons.RJButton btn_delete;
        private RJButtons.RJButton btn_export;
    }
}