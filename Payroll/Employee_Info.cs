﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using MySql.Data.MySqlClient;
using System.Configuration;
using System.Drawing.Drawing2D;
using System.Runtime.InteropServices;
using MySql.Data;
using System.Data.OleDb;
using System.Data.SqlClient;
using System.Data.Common;
using System.IO;
using System.Web.UI.WebControls.WebParts;
using System.Drawing.Text;



namespace Payroll
{
    public partial class Employee_Info : Form
    {
        MySqlCommand cmd;
        MySqlDataAdapter da;
        MySqlDataReader mdr;
        DataTable dt;
        FileStream fs;
        BinaryReader br;


       /*[DllImport("Gdi32.dll", EntryPoint = "CreateRoundRectRgn")]
        private static extern IntPtr CreateRoundRectRgn
       (
           int nLeftRect,     // x-coordinate of upper-left corner
           int nTopRect,      // y-coordinate of upper-left corner
           int nRightRect,    // x-coordinate of lower-right corner
           int nBottomRect,   // y-coordinate of lower-right corner
           int nWidthEllipse, // width of ellipse
           int nHeightEllipse // height of ellipse
       );*/


        public string user_id { get; set; }

          
           public Employee_Info()
            {
                InitializeComponent();
                
          /*      Region = System.Drawing.Region.FromHrgn(CreateRoundRectRgn(0, 0, Width, Height, 20, 20));
                this.FormBorderStyle = FormBorderStyle.None;*/
        }





            public MySqlConnection connection()
            {

                string myconnection = ConfigurationManager.ConnectionStrings["MyConstring"].ConnectionString;
                MySqlConnection connection = new MySqlConnection(myconnection);

                return connection;


            }
            private void Employee_Info_Load(object sender, EventArgs e)
            {

                Personal_Information("Select * from employee");
                EmployeeID();
                load_department();
                load_comp_details();
                load_religion("SELECT * FROM other_details_main");
            }
            public void Personal_Information(string sql1)
            {
                string myconnection = ConfigurationManager.ConnectionStrings["MyConstring"].ConnectionString;
                MySqlConnection connection = new MySqlConnection(myconnection);

                try
                {
                    connection.Open();
                    cmd = new MySqlCommand();
                    cmd.Connection = connection;
                    cmd.CommandText = sql1;
                    da = new MySqlDataAdapter();
                    da.SelectCommand = cmd;
                    dt = new DataTable();
                    da.Fill(dt);
                    lv_employee.Items.Clear();

                    for (int i = 0; i < dt.Rows.Count; i++)
                    {
                        DataRow dr = dt.Rows[i];
                        ListViewItem listitem = new ListViewItem(dr["emp_no"].ToString());

                        listitem.SubItems.Add(dr["status"].ToString());
                        listitem.SubItems.Add(dr["emp_id"].ToString());
                        listitem.SubItems.Add(dr["lname"].ToString());
                        listitem.SubItems.Add(dr["fname"].ToString());
                        listitem.SubItems.Add(dr["mname"].ToString());
                        listitem.SubItems.Add(dr["email"].ToString());
                        listitem.SubItems.Add(dr["pob"].ToString());
                        listitem.SubItems.Add(dr["status_type"].ToString());
                        listitem.SubItems.Add(dr["contact_no"].ToString());
                        listitem.SubItems.Add(dr["cp_email"].ToString());
                        listitem.SubItems.Add(dr["tel_no"].ToString());
                        listitem.SubItems.Add(dr["birthdate"].ToString());
                        listitem.SubItems.Add(dr["gender"].ToString());
                        listitem.SubItems.Add(dr["nationality"].ToString());
                        listitem.SubItems.Add(dr["spouse_name"].ToString());
                        listitem.SubItems.Add(dr["religion"].ToString());
                        listitem.SubItems.Add(dr["address"].ToString());
                        listitem.SubItems.Add(dr["image"].ToString());
                        listitem.SubItems.Add(dr["elementary"].ToString());
                        listitem.SubItems.Add(dr["high_school"].ToString());
                        listitem.SubItems.Add(dr["college"].ToString());
                        listitem.SubItems.Add(dr["course"].ToString());
                        listitem.SubItems.Add(dr["work_experience"].ToString());
                        listitem.SubItems.Add(dr["mothers_name"].ToString());
                        listitem.SubItems.Add(dr["mothers_occ"].ToString());
                        listitem.SubItems.Add(dr["fathers_name"].ToString());
                        listitem.SubItems.Add(dr["fathers_occ"].ToString());
                        listitem.SubItems.Add(dr["home_address"].ToString());
                        listitem.SubItems.Add(dr["primary_con"].ToString());
                        listitem.SubItems.Add(dr["contact_num"].ToString());
                        listitem.SubItems.Add(dr["relationship"].ToString());
                        listitem.SubItems.Add(dr["secondary_con"].ToString());
                        listitem.SubItems.Add(dr["contact_num2"].ToString());
                        listitem.SubItems.Add(dr["relationship2"].ToString());

                        lv_employee.Items.Add(listitem);
                    }

                }
                catch (Exception ex)
                {
                    MessageBox.Show(ex.Message);
                }
                finally
                {
                    connection.Close();
                }

                lv_employee.View = View.Details;
                lv_employee.Columns.Add("No.", 0, HorizontalAlignment.Center);
                lv_employee.Columns.Add(" Status ", 100, HorizontalAlignment.Center);
                lv_employee.Columns.Add(" Emp ID ", 100, HorizontalAlignment.Center);
                lv_employee.Columns.Add(" Lastname ", 100, HorizontalAlignment.Center);
                lv_employee.Columns.Add(" Firstname ", 100, HorizontalAlignment.Center);
                lv_employee.Columns.Add(" Middlename ", 100, HorizontalAlignment.Center);
                lv_employee.Columns.Add(" Email ", 100, HorizontalAlignment.Center);
                lv_employee.Columns.Add(" POB ", 100, HorizontalAlignment.Center);
                lv_employee.Columns.Add(" Status Type ", 100, HorizontalAlignment.Center);
                lv_employee.Columns.Add(" Contact_No. ", 100, HorizontalAlignment.Center);
                lv_employee.Columns.Add(" Company Email ", 100, HorizontalAlignment.Center);
                lv_employee.Columns.Add(" Telephone_No. ", 100, HorizontalAlignment.Center);
                lv_employee.Columns.Add(" Birthdate ", 100, HorizontalAlignment.Center);
                lv_employee.Columns.Add(" Gender ", 100, HorizontalAlignment.Center);
                lv_employee.Columns.Add(" Nationality ", 100, HorizontalAlignment.Center);
                lv_employee.Columns.Add(" Spouse Name ", 100, HorizontalAlignment.Center);
                lv_employee.Columns.Add(" Religion ", 100, HorizontalAlignment.Center);
                lv_employee.Columns.Add(" Address ", 100, HorizontalAlignment.Center);
                lv_employee.Columns.Add(" Image ", 0, HorizontalAlignment.Center);
                lv_employee.Columns.Add(" Elementary ", 100, HorizontalAlignment.Center);
                lv_employee.Columns.Add(" High School ", 100, HorizontalAlignment.Center);
                lv_employee.Columns.Add(" College ", 100, HorizontalAlignment.Center);
                lv_employee.Columns.Add(" Course ", 100, HorizontalAlignment.Center);
                lv_employee.Columns.Add(" Work Experience ", 100, HorizontalAlignment.Center);
                lv_employee.Columns.Add(" Mother's Name ", 100, HorizontalAlignment.Center);
                lv_employee.Columns.Add(" Mother's Occ ", 100, HorizontalAlignment.Center);
                lv_employee.Columns.Add(" Father's Name ", 100, HorizontalAlignment.Center);
                lv_employee.Columns.Add(" Father's Occ ", 100, HorizontalAlignment.Center);
                lv_employee.Columns.Add(" Home Address ", 100, HorizontalAlignment.Center);
                lv_employee.Columns.Add(" Primary Contact ", 100, HorizontalAlignment.Center);
                lv_employee.Columns.Add(" Contact No. ", 100, HorizontalAlignment.Center);
                lv_employee.Columns.Add(" Relationship ", 100, HorizontalAlignment.Center);
                lv_employee.Columns.Add(" Secondary Contact ", 100, HorizontalAlignment.Center);
                lv_employee.Columns.Add(" Contact No. ", 100, HorizontalAlignment.Center);
                lv_employee.Columns.Add(" Relationship ", 100, HorizontalAlignment.Center);
            }
            public void leave_balanced(string sql2)
            {
                string myconnection = ConfigurationManager.ConnectionStrings["MyConstring"].ConnectionString;
                MySqlConnection connection = new MySqlConnection(myconnection);

                try
                {
                    connection.Open();
                    cmd = new MySqlCommand();
                    cmd.Connection = connection;
                    cmd.CommandText = sql2;
                    da = new MySqlDataAdapter();
                    da.SelectCommand = cmd;
                    dt = new DataTable();
                    da.Fill(dt);
                    lv_employee.Items.Clear();



                    for (int i = 0; i < dt.Rows.Count; i++)
                    {
                        DataRow dr = dt.Rows[i];
                        ListViewItem listitem = new ListViewItem(dr["leave_balance_id"].ToString());

                        listitem.SubItems.Add(dr["emp_no"].ToString());
                        listitem.SubItems.Add(dr["emp_id"].ToString());
                        listitem.SubItems.Add(dr["lname"].ToString());
                        listitem.SubItems.Add(dr["fname"].ToString());
                        listitem.SubItems.Add(dr["mname"].ToString());
                        listitem.SubItems.Add(dr["sick_leave"].ToString());
                        listitem.SubItems.Add(dr["vacation_leave"].ToString());

                        lv_employee.Items.Add(listitem);
                    }

                }
                catch (Exception ex)
                {
                    MessageBox.Show(ex.Message);
                }
                finally
                {
                    connection.Close();
                }


                lv_employee.View = View.Details;
                lv_employee.Columns.Add("No.", 0, HorizontalAlignment.Center);
                lv_employee.Columns.Add("Emp no.", 0, HorizontalAlignment.Center);
                lv_employee.Columns.Add(" Emp ID ", 180, HorizontalAlignment.Center);
                lv_employee.Columns.Add(" Lastname ", 180, HorizontalAlignment.Center);
                lv_employee.Columns.Add(" Firstname ", 180, HorizontalAlignment.Center);
                lv_employee.Columns.Add(" Middlename ", 180, HorizontalAlignment.Center);
                lv_employee.Columns.Add(" Sick Leave ", 180, HorizontalAlignment.Center);
                lv_employee.Columns.Add(" Vacation Leave ", 180, HorizontalAlignment.Center);

            }


            public void Salary(string sql3)
            {
                string myconnection = ConfigurationManager.ConnectionStrings["MyConstring"].ConnectionString;
                MySqlConnection connection = new MySqlConnection(myconnection);

                try
                {
                    connection.Open();
                    cmd = new MySqlCommand();
                    cmd.Connection = connection;
                    cmd.CommandText = sql3;
                    da = new MySqlDataAdapter();
                    da.SelectCommand = cmd;
                    dt = new DataTable();
                    da.Fill(dt);
                    lv_employee.Items.Clear();



                    for (int i = 0; i < dt.Rows.Count; i++)
                    {
                        DataRow dr = dt.Rows[i];
                        ListViewItem listitem = new ListViewItem(dr["salary_id"].ToString());

                        listitem.SubItems.Add(dr["emp_no"].ToString());
                        listitem.SubItems.Add(dr["emp_id"].ToString());
                        listitem.SubItems.Add(dr["lname"].ToString());
                        listitem.SubItems.Add(dr["fname"].ToString());
                        listitem.SubItems.Add(dr["mname"].ToString());
                        listitem.SubItems.Add(dr["date_hired"].ToString());
                        listitem.SubItems.Add(dr["designation"].ToString());
                        listitem.SubItems.Add(dr["department"].ToString());
                        listitem.SubItems.Add(dr["con_level"].ToString());
                        listitem.SubItems.Add(dr["emp_status"].ToString());
                        listitem.SubItems.Add(dr["date_resign"].ToString());
                        listitem.SubItems.Add(dr["reason"].ToString());
                        listitem.SubItems.Add(dr["pay_rate"].ToString());
                        listitem.SubItems.Add(dr["pay_sched"].ToString());
                        listitem.SubItems.Add(string.Format("{0:#,##0.00}", Convert.ToDecimal(dr["salary"])));
                        listitem.SubItems.Add(string.Format("{0:#,##0.00}", Convert.ToDecimal(dr["daily_rate"])));
                        listitem.SubItems.Add(string.Format("{0:#,##0.00}", Convert.ToDecimal(dr["daily_hour_rate"])));
                        listitem.SubItems.Add(string.Format("{0:#,##0.00}", Convert.ToDecimal(dr["min_rate"])));
                        listitem.SubItems.Add(string.Format("{0:#,##0.00}", Convert.ToDecimal(dr["rice_allowance"])));
                        listitem.SubItems.Add(string.Format("{0:#,##0.00}", Convert.ToDecimal(dr["laundry_allowance"])));
                        listitem.SubItems.Add(string.Format("{0:#,##0.00}", Convert.ToDecimal(dr["meal_allowance"])));
                        listitem.SubItems.Add(string.Format("{0:#,##0.00}", Convert.ToDecimal(dr["medical_benefits"])));
                        listitem.SubItems.Add(string.Format("{0:#,##0.00}", Convert.ToDecimal(dr["other_allowance"])));
                        listitem.SubItems.Add(dr["pay_mode"].ToString());
                        listitem.SubItems.Add(dr["bank_name"].ToString());
                        listitem.SubItems.Add(dr["bank_acc"].ToString());
                        listitem.SubItems.Add(dr["bank_name2"].ToString());
                        listitem.SubItems.Add(dr["bank_acc2"].ToString());
                        listitem.SubItems.Add(string.Format("{0:#,##0.00}", Convert.ToDecimal(dr["13thmonthpay"])));
                        listitem.SubItems.Add(string.Format("{0:#,##0.00}", Convert.ToDecimal(dr["14thmonthpay"])));
                        listitem.SubItems.Add(string.Format("{0:#,##0.00}", Convert.ToDecimal(dr["15thmonthpay"])));
                        listitem.SubItems.Add(string.Format("{0:#,##0.00}", Convert.ToDecimal(dr["sss_ee_monthly"])));
                        listitem.SubItems.Add(string.Format("{0:#,##0.00}", Convert.ToDecimal(dr["sss_er_monthly"])));
                        listitem.SubItems.Add(string.Format("{0:#,##0.00}", Convert.ToDecimal(dr["pag_ibig_ee_monthly"])));
                        listitem.SubItems.Add(string.Format("{0:#,##0.00}", Convert.ToDecimal(dr["pag_ibig_er_monthly"])));
                        listitem.SubItems.Add(string.Format("{0:#,##0.00}", Convert.ToDecimal(dr["philhealth_ee_monthly"])));
                        listitem.SubItems.Add(string.Format("{0:#,##0.00}", Convert.ToDecimal(dr["philhealth_er_monthly"])));

                        lv_employee.Items.Add(listitem);

                    }

                }
                catch (Exception ex)
                {
                    MessageBox.Show(ex.Message);
                }
                finally
                {
                    connection.Close();
                }


                lv_employee.View = View.Details;
                lv_employee.Columns.Add("No.", 0, HorizontalAlignment.Center);
                lv_employee.Columns.Add("Emp No.", 0, HorizontalAlignment.Center);
                lv_employee.Columns.Add(" Emp ID ", 100, HorizontalAlignment.Center);
                lv_employee.Columns.Add(" Lastname ", 100, HorizontalAlignment.Center);
                lv_employee.Columns.Add(" Firstname ", 100, HorizontalAlignment.Center);
                lv_employee.Columns.Add(" Middlename ", 100, HorizontalAlignment.Center);
                lv_employee.Columns.Add(" Date Hired ", 100, HorizontalAlignment.Center);
                lv_employee.Columns.Add(" Designation ", 100, HorizontalAlignment.Center);
                lv_employee.Columns.Add(" Department ", 100, HorizontalAlignment.Center);
                lv_employee.Columns.Add(" Rank/Level ", 100, HorizontalAlignment.Center);
                lv_employee.Columns.Add(" Employee Status ", 100, HorizontalAlignment.Center);
                lv_employee.Columns.Add(" Date Resign ", 100, HorizontalAlignment.Center);
                lv_employee.Columns.Add(" Reason ", 100, HorizontalAlignment.Center);
                lv_employee.Columns.Add(" Pay Rate ", 100, HorizontalAlignment.Center);
                lv_employee.Columns.Add(" Payment Schedule ", 100, HorizontalAlignment.Center);
                lv_employee.Columns.Add(" Salary ", 100, HorizontalAlignment.Center);
                lv_employee.Columns.Add(" Daily Rate ", 100, HorizontalAlignment.Center);
                lv_employee.Columns.Add(" Hour Rate ", 100, HorizontalAlignment.Center);
                lv_employee.Columns.Add(" Minute Rate ", 100, HorizontalAlignment.Center);
                lv_employee.Columns.Add(" Rice Allowance ", 100, HorizontalAlignment.Center);
                lv_employee.Columns.Add(" Laundry Allowance ", 100, HorizontalAlignment.Center);
                lv_employee.Columns.Add(" Meal Allowance ", 100, HorizontalAlignment.Center);
                lv_employee.Columns.Add(" Medical Benefits ", 100, HorizontalAlignment.Center);
                lv_employee.Columns.Add(" Others ", 100, HorizontalAlignment.Center);
                lv_employee.Columns.Add(" Payroll Mode ", 100, HorizontalAlignment.Center);
                lv_employee.Columns.Add(" Primary Bank ", 100, HorizontalAlignment.Center);
                lv_employee.Columns.Add(" Bank Account ", 100, HorizontalAlignment.Center);
                lv_employee.Columns.Add(" Alternate Bank ", 100, HorizontalAlignment.Center);
                lv_employee.Columns.Add(" Bank Account ", 100, HorizontalAlignment.Center);
                lv_employee.Columns.Add(" 13Month Pay ", 100, HorizontalAlignment.Center);
                lv_employee.Columns.Add(" 14Month Pay ", 100, HorizontalAlignment.Center);
                lv_employee.Columns.Add(" 15Month Pay ", 100, HorizontalAlignment.Center);
                lv_employee.Columns.Add(" SSS EE ", 100, HorizontalAlignment.Center);
                lv_employee.Columns.Add(" SSS ER ", 100, HorizontalAlignment.Center);
                lv_employee.Columns.Add(" Pag-Ibig EE ", 100, HorizontalAlignment.Center);
                lv_employee.Columns.Add(" Pag-Ibig ER ", 100, HorizontalAlignment.Center);
                lv_employee.Columns.Add(" Philhealth EE ", 100, HorizontalAlignment.Center);
                lv_employee.Columns.Add(" Philhealth ER ", 100, HorizontalAlignment.Center);
            }

            public void ID(string sql4)
            {
                string myconnection = ConfigurationManager.ConnectionStrings["MyConstring"].ConnectionString;
                MySqlConnection connection = new MySqlConnection(myconnection);

                try
                {
                    connection.Open();
                    cmd = new MySqlCommand();
                    cmd.Connection = connection;
                    cmd.CommandText = sql4;
                    da = new MySqlDataAdapter();
                    da.SelectCommand = cmd;
                    dt = new DataTable();
                    da.Fill(dt);
                    lv_employee.Items.Clear();

                    for (int i = 0; i < dt.Rows.Count; i++)
                    {
                        DataRow dr = dt.Rows[i];
                        ListViewItem listitem = new ListViewItem(dr["other_id"].ToString());

                        listitem.SubItems.Add(dr["emp_no"].ToString());
                        listitem.SubItems.Add(dr["emp_id"].ToString());
                        listitem.SubItems.Add(dr["lname"].ToString());
                        listitem.SubItems.Add(dr["fname"].ToString());
                        listitem.SubItems.Add(dr["mname"].ToString());
                        listitem.SubItems.Add(dr["sss_no"].ToString());
                        listitem.SubItems.Add(dr["pag_ibig_no"].ToString());
                        listitem.SubItems.Add(dr["tin_no"].ToString());
                        listitem.SubItems.Add(dr["philhealth_no"].ToString());

                        lv_employee.Items.Add(listitem);
                    }

                }
                catch (Exception ex)
                {
                    MessageBox.Show(ex.Message);
                }
                finally
                {
                    connection.Close();
                }


                lv_employee.View = View.Details;
                lv_employee.Columns.Add("No.", 0, HorizontalAlignment.Center);
                lv_employee.Columns.Add("Emp No", 0, HorizontalAlignment.Center);
                lv_employee.Columns.Add(" Emp ID ", 125, HorizontalAlignment.Center);
                lv_employee.Columns.Add(" Lastname ", 125, HorizontalAlignment.Center);
                lv_employee.Columns.Add(" Firstname ", 125, HorizontalAlignment.Center);
                lv_employee.Columns.Add(" Middlename ", 125, HorizontalAlignment.Center);
                lv_employee.Columns.Add(" SSS No. ", 125, HorizontalAlignment.Center);
                lv_employee.Columns.Add(" Pag-Ibig No ", 125, HorizontalAlignment.Center);
                lv_employee.Columns.Add(" Tin No ", 125, HorizontalAlignment.Center);
                lv_employee.Columns.Add(" Philhealth No ", 125, HorizontalAlignment.Center);

            }
            public void Loan(string sql5)
            {
                string myconnection = ConfigurationManager.ConnectionStrings["MyConstring"].ConnectionString;
                MySqlConnection connection = new MySqlConnection(myconnection);

                try
                {
                    connection.Open();
                    cmd = new MySqlCommand();
                    cmd.Connection = connection;
                    cmd.CommandText = sql5;
                    da = new MySqlDataAdapter();
                    da.SelectCommand = cmd;
                    dt = new DataTable();
                    da.Fill(dt);
                    lv_employee.Items.Clear();



                    for (int i = 0; i < dt.Rows.Count; i++)
                    {
                        DataRow dr = dt.Rows[i];
                        ListViewItem listitem = new ListViewItem(dr["loan_id"].ToString());

                        listitem.SubItems.Add(dr["emp_no"].ToString());
                        listitem.SubItems.Add(dr["emp_id"].ToString());
                        listitem.SubItems.Add(dr["lname"].ToString());
                        listitem.SubItems.Add(dr["fname"].ToString());
                        listitem.SubItems.Add(dr["mname"].ToString());
                        listitem.SubItems.Add(string.Format("{0:#,##0.00}", Convert.ToDecimal(dr["comp_loan_amnt"])));
                        listitem.SubItems.Add(string.Format("{0:#,##0.00}", Convert.ToDecimal(dr["comp_bal"])));
                        listitem.SubItems.Add(string.Format("{0:#,##0.00}", Convert.ToDecimal(dr["comp_month_ded"])));
                        listitem.SubItems.Add(string.Format("{0:#,##0.00}", Convert.ToDecimal(dr["sss_loan_amnt"])));
                        listitem.SubItems.Add(string.Format("{0:#,##0.00}", Convert.ToDecimal(dr["sss_bal"])));
                        listitem.SubItems.Add(string.Format("{0:#,##0.00}", Convert.ToDecimal(dr["sss_month_ded"])));
                        listitem.SubItems.Add(string.Format("{0:#,##0.00}", Convert.ToDecimal(dr["pag_ibig_loan_amnt"])));
                        listitem.SubItems.Add(string.Format("{0:#,##0.00}", Convert.ToDecimal(dr["pag_ibig_bal"])));
                        listitem.SubItems.Add(string.Format("{0:#,##0.00}", Convert.ToDecimal(dr["pag_ibig_month_ded"])));

                        lv_employee.Items.Add(listitem);
                    }

                }
                catch (Exception ex)
                {
                    MessageBox.Show(ex.Message);
                }
                finally
                {
                    connection.Close();
                }


                lv_employee.View = View.Details;
                lv_employee.Columns.Add("No.", 0, HorizontalAlignment.Center);
                lv_employee.Columns.Add("Emp No", 0, HorizontalAlignment.Center);
                lv_employee.Columns.Add(" Emp ID ", 100, HorizontalAlignment.Center);
                lv_employee.Columns.Add(" Lastname ", 100, HorizontalAlignment.Center);
                lv_employee.Columns.Add(" Firstname ", 100, HorizontalAlignment.Center);
                lv_employee.Columns.Add(" Middlename ", 100, HorizontalAlignment.Center);
                lv_employee.Columns.Add(" Comp Loan Amount ", 100, HorizontalAlignment.Center);
                lv_employee.Columns.Add(" Comp Balance ", 100, HorizontalAlignment.Center);
                lv_employee.Columns.Add(" Comp Month Deduction ", 100, HorizontalAlignment.Center);
                lv_employee.Columns.Add(" SSS Loan Amount ", 100, HorizontalAlignment.Center);
                lv_employee.Columns.Add(" SSS Balance ", 100, HorizontalAlignment.Center);
                lv_employee.Columns.Add(" SSS Month Deduction ", 100, HorizontalAlignment.Center);
                lv_employee.Columns.Add(" Pag-Ibig Loan Amount ", 100, HorizontalAlignment.Center);
                lv_employee.Columns.Add(" Pag-Ibig Balance ", 100, HorizontalAlignment.Center);
                lv_employee.Columns.Add(" Pag-Ibig Month Deduction ", 100, HorizontalAlignment.Center);


            }

            private void tb_search_TextChanged(object sender, EventArgs e)
            {

                if (lbl_category.Text == "General Information" || lbl_category.Text == "Other Information")
                {
                    lv_employee.Columns.Clear();
                    Personal_Information("Select DISTINCT * FROM employee WHERE status like '" + tb_search.Text + "%' or emp_id like '" + tb_search.Text + "%' or lname like '" + tb_search.Text + "%' or fname like '" + tb_search.Text + "%' or  mname like '" + tb_search.Text + "%'  or  email like '" + tb_search.Text + "%' or pob like '" + tb_search.Text + "%' or status_type like '" + tb_search.Text + "%' or contact_no like '" + tb_search.Text + "%' or  cp_email like '" + tb_search.Text + "%' or tel_no like '" + tb_search.Text + "%' or birthdate like '" + tb_search.Text + "%' or gender like '" + tb_search.Text + "%'  or nationality like '" + tb_search.Text + "%' or spouse_name like '" + tb_search.Text + "%' or religion like '" + tb_search.Text + "%' or address like '" + tb_search.Text + "%' or image like '" + tb_search.Text + "%' or elementary like '" + tb_search.Text + "%' or high_school like '" + tb_search.Text + "%' or college like '" + tb_search.Text + "%' or course like '" + tb_search.Text + "%'  or work_experience like '" + tb_search.Text + "%' or mothers_name like '" + tb_search.Text + "%' or mothers_occ like '" + tb_search.Text + "%' or fathers_name like '" + tb_search.Text + "%' or fathers_occ like '" + tb_search.Text + "%' or home_address like '" + tb_search.Text + "%' or primary_con like '" + tb_search.Text + "%' or contact_num like '" + tb_search.Text + "%' or relationship like '" + tb_search.Text + "%' or secondary_con like '" + tb_search.Text + "%' or contact_num2 like '" + tb_search.Text + "%' or relationship2 like '" + tb_search.Text + "%'");
                }
                else if (lbl_category.Text == "Leave")
                {
                    lv_employee.Columns.Clear();
                    leave_balanced("Select DISTINCT * FROM leave_balance LEFT JOIN employee on leave_balance.emp_no = employee.emp_no WHERE  employee.emp_id like '" + tb_search.Text + "%' or   employee.lname like '" + tb_search.Text + "%' or  employee.fname like '" + tb_search.Text + "%'  or  employee.mname like '" + tb_search.Text + "%' or leave_balance.sick_leave like '" + tb_search.Text + "%' or leave_balance.vacation_leave like '" + tb_search.Text + "%'");
                }
                else if (lbl_category.Text == "Salary")
                {
                    lv_employee.Columns.Clear();
                    Salary("Select DISTINCT * FROM salary LEFT JOIN employee on salary.emp_no = employee.emp_no WHERE  employee.emp_id like '" + tb_search.Text + "%'  or  employee.lname like '" + tb_search.Text + "%' or  employee.fname like '" + tb_search.Text + "%' or  employee.mname like '" + tb_search.Text + "%'  or salary.date_hired like '" + tb_search.Text + "%'  or salary.designation like '" + tb_search.Text + "%' or salary.department like '" + tb_search.Text + "%' or salary.con_level like '" + tb_search.Text + "%' or salary.emp_status like '" + tb_search.Text + "%' or salary.date_resign like '" + tb_search.Text + "%' or salary.reason like '" + tb_search.Text + "%' or salary.pay_rate like '" + tb_search.Text + "%' or salary.pay_sched like '" + tb_search.Text + "%' or salary.salary like '" + tb_search.Text + "%' or salary.daily_rate like '" + tb_search.Text + "%' or salary.daily_hour_rate like '" + tb_search.Text + "%' or salary.min_rate like '" + tb_search.Text + "%' or salary.rice_allowance like '" + tb_search.Text + "%' or salary.laundry_allowance like '" + tb_search.Text + "%' or salary.meal_allowance like '" + tb_search.Text + "%' or salary.medical_benefits like '" + tb_search.Text + "%' or salary.pay_mode like '" + tb_search.Text + "%' or salary.pay_mode like '" + tb_search.Text + "%' or salary.bank_name like '" + tb_search.Text + "%' or salary.bank_acc like '" + tb_search.Text + "%' or salary.bank_name2 like '" + tb_search.Text + "%' or salary.bank_acc2 like '" + tb_search.Text + "%' or salary.13thmonthpay like '" + tb_search.Text + "%' or salary.14thmonthpay like '" + tb_search.Text + "%' or salary.15thmonthpay like '" + tb_search.Text + "%'");
                }
                else if (lbl_category.Text == "Other ID")
                {
                    lv_employee.Columns.Clear();
                    ID("Select DISTINCT * FROM others_id LEFT JOIN employee on others_id.emp_no = employee.emp_no WHERE  employee.emp_id like '" + tb_search.Text + "%' or   employee.lname like '" + tb_search.Text + "%' or  employee.fname like '" + tb_search.Text + "%'  or  employee.mname like '" + tb_search.Text + "%' or others_id.sss_no like '" + tb_search.Text + "%' or others_id.pag_ibig_no like '" + tb_search.Text + "%' or others_id.tin_no like '" + tb_search.Text + "%' or others_id.philhealth_no like '" + tb_search.Text + "%'");

                }
                else if (lbl_category.Text == "Loan")
                {
                    lv_employee.Columns.Clear();
                    Loan("Select DISTINCT * FROM loan LEFT JOIN employee on loan.emp_no = employee.emp_no WHERE  employee.emp_id like '" + tb_search.Text + "%' or   employee.lname like '" + tb_search.Text + "%' or  employee.fname like '" + tb_search.Text + "%'  or  employee.mname like '" + tb_search.Text + "%' or loan.comp_loan_amnt like '" + tb_search.Text + "%' or loan.comp_bal like '" + tb_search.Text + "%' or loan.comp_month_ded like '" + tb_search.Text + "%' or loan.sss_loan_amnt like '" + tb_search.Text + "%' or loan.sss_bal like '" + tb_search.Text + "%' or loan.sss_month_ded like '" + tb_search.Text + "%' or loan.pag_ibig_loan_amnt like '" + tb_search.Text + "%' or loan.pag_ibig_bal like '" + tb_search.Text + "%' or loan.pag_ibig_month_ded like '" + tb_search.Text + "%'");

                }

            }

            private void tb_contact_no_KeyPress(object sender, KeyPressEventArgs e)
            {
                e.Handled = !char.IsDigit(e.KeyChar) && !char.IsControl(e.KeyChar);
            }

            public void insert_personal_information()
            {
                string myconnection = ConfigurationManager.ConnectionStrings["MyConstring"].ConnectionString;
                MySqlConnection connection = new MySqlConnection(myconnection);

                MemoryStream ms = new MemoryStream();
                pic_Image.Image.Save(ms, pic_Image.Image.RawFormat);
                byte[] image = ms.ToArray();

                String insertQuery = "INSERT INTO employee(emp_no, status, emp_id, lname, fname, mname, email, pob, status_type, contact_no, cp_email, tel_no, birthdate, gender, nationality, spouse_name, religion, address, image, elementary, high_school, college, course, work_experience, mothers_name, mothers_occ, fathers_name, fathers_occ,  home_address, primary_con, contact_num, relationship, secondary_con, contact_num2, relationship2) VALUES('" + lbl_number_of_employee.Text + "', '" + cb_statustype.Text + "','" + tb_empID.Text + "', '" + tb_lname.Text + "', '" + tb_fname.Text + "', '" + tb_mname.Text + "','" + tb_email.Text + "', '" + cb_pob.Text + "', '" + cb_status.Text + "', '" + tb_cont_no.Text + "', '" + tb_comp_email.Text + "', '" + tb_telphone_no.Text + "', '" + dtp_birthdate.Text + "','" + cb_gender.Text + "', '" + cb_nationality.Text + "', '" + tb_name_of_spouse.Text + "' , '" + cb_religion.Text + "', '" + rtb_address.Text + "',  @img  ,'" + tb_elementary.Text + "','" + tb_highSchool.Text + "', '" + tb_tertiary.Text + "', '" + tb_course.Text + "','" + rtb_work_experience.Text + "', '" + tb_mothers_name.Text + "', '" + tb_occupation_mother.Text + "', '" + tb_fathers_name.Text + "', '" + tb_occupation_father.Text + "', '" + rtb_homeAddress.Text + "', '" + tb_name.Text + "', '" + tb_primary_con.Text + "', '" + tb_relationship.Text + "', '" + tb_name2.Text + "', '" + tb_second_con.Text + "', '" + tb_relationship2.Text + "')";

                connection.Open();
                cmd = new MySqlCommand(insertQuery, connection);
                cmd.Parameters.Add("@img", MySqlDbType.Blob);
                cmd.Parameters["@img"].Value = image;


                try
                {
                    if (cmd.ExecuteNonQuery() == 1)
                    {
                        //MessageBox.Show("Successfully Add Information", "Add Information", MessageBoxButtons.OK, MessageBoxIcon.Information);
                    }
                    else
                    {

                        Personal_Information("Select * from employee");
                    }
                }
                catch (Exception ex)
                {
                    MessageBox.Show("Error" + ex);
                }

                connection.Close();
            }

            public void insert_salary()
            {
                string myconnection = ConfigurationManager.ConnectionStrings["MyConstring"].ConnectionString;
                MySqlConnection connection = new MySqlConnection(myconnection);

                decimal thirteenmonthpay = Convert.ToDecimal(tb_13th_month_pay.Text);
                decimal fourthteenmonthpay = Convert.ToDecimal(tb_14th_month_pay.Text);

                decimal total_bonus = thirteenmonthpay + fourthteenmonthpay;

                decimal non_tax;
                if (total_bonus >= 90000)
                {
                    non_tax = 90000;
                }
                else
                {
                    non_tax = total_bonus;
                }

                decimal salary = Convert.ToDecimal(tb_salary.Text);
                decimal daily_rate = Convert.ToDecimal(tb_daily_rate.Text);
                decimal daily_hour_rate = Convert.ToDecimal(tb_daily_hour_rate.Text);
                decimal daily_min_rate = Convert.ToDecimal(tb_minRate.Text);
                decimal tthmonthpay = Convert.ToDecimal(tb_13th_month_pay.Text);
                decimal ftmonthpay = Convert.ToDecimal(tb_14th_month_pay.Text);
                decimal fthmonthpay = Convert.ToDecimal(tb_15th_month_pay.Text);

                decimal sss = Convert.ToDecimal(tb_sss.Text);
                decimal sss_ee_semi_monthly_cont = sss / 2;
                decimal sss_er = Convert.ToDecimal(label115.Text);
                decimal sss_er_semi_monthly_cont = sss_er / 2;

                decimal pag_ibig = Convert.ToDecimal(tb_pag_ibig.Text);
                decimal pag_ibig_semi_monthly_cont = pag_ibig / 2;
                decimal pag_ibig_er = Convert.ToDecimal(label116.Text);
                decimal pag_ibig_er_semi_monthly_cont = pag_ibig_er / 2;

                decimal philhealth = Convert.ToDecimal(tb_philhealth.Text);
                decimal philhealth_ee_semi_monthly_cont = philhealth / 2;
                decimal philhealth_er = Convert.ToDecimal(label117.Text);
                decimal philhealth_er_semi_monthly_cont = philhealth_er / 2;

                string insertQuery = "INSERT INTO salary(emp_no, date_hired, designation, department, con_level, emp_status, pay_rate, pay_sched, salary, daily_rate, daily_hour_rate, min_rate, rice_allowance, laundry_allowance, meal_allowance, medical_benefits, other_allowance, pay_mode, bank_name, bank_acc, bank_name2, bank_acc2, 13thmonthpay, 14thmonthpay, 15thmonthpay, mntbonus, sss_ee, sss_ee_monthly, sss_er, sss_er_monthly, pag_ibig_ee, pag_ibig_ee_monthly, pag_ibig_er, pag_ibig_er_monthly, philhealth_ee, philhealth_ee_monthly, philhealth_er, philhealth_er_monthly) VALUES('" + label55.Text + "', '" + dtp_date_hired.Text + "', '" + tb_designation.Text + "', '" + cb_department.Text + "', '" + cb_confidentiality.Text + "', '" + cb_employee_status.Text + "', '" + cb_payRate.Text + "', '" + cb_paySchedule.Text + "', '" + salary + "', '" + daily_rate + "',  '" + daily_hour_rate + "', '" + daily_min_rate + "', '" + tb_riceAllowance.Text + "', '" + tb_laundryAllowance.Text + "','" + tb_mealAllowance.Text + "', '" + tb_MedicalBenefits.Text + "', '" + tb_otherAllowance.Text + "', '" + cb_payroll_mode.Text + "' ,  '" + cb_bank_name.Text + "', '" + tb_bank_acc.Text + "',  '" + cb_bank_name2.Text + "', '" + tb_bank_acc2.Text + "', '" + tthmonthpay + "', '" + ftmonthpay + "', '" + fthmonthpay + "', '" + non_tax + "','" + sss_ee_semi_monthly_cont + "', '" + sss + "', '" + sss_er_semi_monthly_cont + "', '" + label115.Text + "', '" + pag_ibig_semi_monthly_cont + "', '" + pag_ibig + "' , '" + pag_ibig_er_semi_monthly_cont + "','" + label116.Text + "', '" + philhealth_ee_semi_monthly_cont + "', '" + philhealth + "', '" + philhealth_er_semi_monthly_cont + "', '" + label117.Text + "')";

                connection.Open();
                MySqlCommand mySqlCommand = new MySqlCommand(insertQuery, connection);


                try
                {
                    if (mySqlCommand.ExecuteNonQuery() == 1)
                    {
                        //MessageBox.Show("Successfully Add Information", "Add Information", MessageBoxButtons.OK, MessageBoxIcon.Information);
                    }
                    else
                    {
                        //MessageBox.Show("Data Not Inserted");
                        Personal_Information("Select * from employee");
                    }
                }
                catch (Exception ex)
                {
                    MessageBox.Show("Error" + ex);
                }

                connection.Close();
            }

            public bool select_religion()
            {

                string myconnection = ConfigurationManager.ConnectionStrings["MyConstring"].ConnectionString;
                MySqlConnection connection = new MySqlConnection(myconnection);

                connection.Open();
                cmd = new MySqlCommand();
                cmd.Connection = connection;
                cmd.CommandText = "Select * FROM other_details_main where religion_name = '" + cb_religion.Text + "'";
                da = new MySqlDataAdapter();
                da.SelectCommand = cmd;
                dt = new DataTable();
                da.Fill(dt);


                for (int i = 0; i < dt.Rows.Count; i++)
                {

                    DataRow dr = dt.Rows[i];

                    return true;
                }
                connection.Close();

                return false;
            }




            public void insert_religion()
            {
                string myconnection = ConfigurationManager.ConnectionStrings["MyConstring"].ConnectionString;
                MySqlConnection connection = new MySqlConnection(myconnection);

                string insertQuery = "INSERT INTO other_details_main(religion_name)VALUES('" + cb_religion.Text + "')";


                connection.Open();
                MySqlCommand mySqlCommand = new MySqlCommand(insertQuery, connection);


                try
                {
                    if (mySqlCommand.ExecuteNonQuery() == 1)
                    {
                        cb_religion.Items.Clear();
                        load_religion("SELECT * FROM other_details_main");
                    }

                }
                catch (Exception ex)
                {
                    MessageBox.Show("Error" + ex);
                }

                connection.Close();
            }


            private void tb_daily_rate_Enter(object sender, EventArgs e)
            {

                compute_daily_rate();

            }
            private void tb_salary_KeyPress(object sender, KeyPressEventArgs e)
            {
                if ((e.KeyChar < 32) || (e.KeyChar >= '0') && (e.KeyChar <= '9') || (e.KeyChar == ',') || (e.KeyChar == '.'))
                    return;

                e.Handled = true;
            }
            private void tb_tax_KeyPress(object sender, KeyPressEventArgs e)
            {
                if (((e.KeyChar < 48 || e.KeyChar > 57) && e.KeyChar != 8 && e.KeyChar != 46))
                {
                    e.Handled = true;
                    return;
                }

                if (e.KeyChar == 46)
                {
                    if ((sender as TextBox).Text.IndexOf(e.KeyChar) != -1)
                        e.Handled = true;
                }
            }

            private void tb_daily_rate_KeyPress(object sender, KeyPressEventArgs e)
            {
                if ((e.KeyChar < 32) || (e.KeyChar >= '0') && (e.KeyChar <= '9') || (e.KeyChar == ',') || (e.KeyChar == '.'))
                    return;

                e.Handled = true;
            }

            private void tb_daily_hour_rate_Enter(object sender, EventArgs e)
            {

                compute_daily_hour_rate();

            }

            private void tb_daily_hour_rate_KeyPress(object sender, KeyPressEventArgs e)
            {
                if ((e.KeyChar < 32) || (e.KeyChar >= '0') && (e.KeyChar <= '9') || (e.KeyChar == ',') || (e.KeyChar == '.'))
                    return;

                e.Handled = true;
            }

            public void calculate_range_of_sss()
            {

                string myconnection = ConfigurationManager.ConnectionStrings["MyConstring"].ConnectionString;
                MySqlConnection connection = new MySqlConnection(myconnection);


                decimal salary = Convert.ToDecimal(tb_salary.Text);


                try
                {
                    connection.Open();
                    cmd = new MySqlCommand();
                    cmd.Connection = connection;
                    cmd.CommandText = "SELECT emply_cont, emplyr_cont FROM sss WHERE min_amnt <= '" + salary + "' AND max_amnt >= '" + salary + "'";
                    da = new MySqlDataAdapter();
                    da.SelectCommand = cmd;
                    dt = new DataTable();
                    da.Fill(dt);

                    for (int i = 0; i < dt.Rows.Count; i++)
                    {
                        DataRow dr = dt.Rows[i];


                        //employee
                        string employee_cont = (dr["emply_cont"].ToString());
                        decimal emply_cont = Convert.ToDecimal(employee_cont);
                        decimal ee_total_contribution = Math.Round(emply_cont, 2, MidpointRounding.AwayFromZero);
                        tb_sss.Text = ee_total_contribution + "";

                        //employer
                        string employer_cont = (dr["emplyr_cont"].ToString());
                        decimal emplyr_cont = Convert.ToDecimal(employer_cont);
                        decimal er_total_contribution = Math.Round(emplyr_cont, 2, MidpointRounding.AwayFromZero);
                        label115.Text = er_total_contribution + "";
                    }

                }
                catch (Exception ex)
                {
                    MessageBox.Show(ex.Message);
                }
                finally
                {
                    connection.Close();
                }


            }

            public void calculate_range_of_pag_ibig()
            {

                decimal salary = Convert.ToDecimal(tb_salary.Text);

                string myconnection = ConfigurationManager.ConnectionStrings["MyConstring"].ConnectionString;
                MySqlConnection connection = new MySqlConnection(myconnection);

                try
                {
                    connection.Open();
                    cmd = new MySqlCommand();
                    cmd.Connection = connection;
                    cmd.CommandText = "SELECT emply_cont FROM pag_ibig WHERE min_amnt <= '" + salary + "' AND max_amnt >= '" + salary + "'";
                    da = new MySqlDataAdapter();
                    da.SelectCommand = cmd;
                    dt = new DataTable();
                    da.Fill(dt);

                    for (int i = 0; i < dt.Rows.Count; i++)
                    {
                        DataRow dr = dt.Rows[i];

                        //employee
                        string employee_cont = (dr["emply_cont"].ToString());
                        decimal emply_cont = Convert.ToDecimal(employee_cont);
                        decimal ee_total_contribution = Math.Round(emply_cont, 2, MidpointRounding.AwayFromZero);

                        tb_pag_ibig.Text = ee_total_contribution + "";

                        //employer
                        label116.Text = tb_pag_ibig.Text;
                        decimal emplyr_cont = Convert.ToDecimal(label116.Text);
                        decimal er_total_contribution = Math.Round(emplyr_cont, 2, MidpointRounding.AwayFromZero);
                        label116.Text = er_total_contribution + "";
                    }

                }
                catch (Exception ex)
                {
                    MessageBox.Show(ex.Message);
                }
                finally
                {
                    connection.Close();
                }
            }

            public void calculate_range_of_philhealth()
            {
                decimal con_salary = Convert.ToDecimal(tb_salary.Text);

                string myconnection = ConfigurationManager.ConnectionStrings["MyConstring"].ConnectionString;
                MySqlConnection connection = new MySqlConnection(myconnection);

                try
                {
                    connection.Open();
                    cmd = new MySqlCommand();
                    cmd.Connection = connection;
                    cmd.CommandText = "SELECT * FROM philhealth WHERE min_amnt <= '" + con_salary + "' AND max_amnt >= '" + con_salary + "'";

                    da = new MySqlDataAdapter();
                    da.SelectCommand = cmd;
                    dt = new DataTable();
                    da.Fill(dt);

                    for (int i = 0; i < dt.Rows.Count; i++)
                    {
                        DataRow dr = dt.Rows[i];

                        string x = dr["min_amnt"].ToString();
                        string y = dr["max_amnt"].ToString();
                        string z = dr["percent"].ToString();
                        string a = dr["emply_cont"].ToString();

                        decimal employee = Convert.ToDecimal(a);
                        decimal min_amnt = Convert.ToDecimal(x);
                        decimal max_amnt = Convert.ToDecimal(y);
                        decimal percent = Convert.ToDecimal(y);
                        decimal salary = Convert.ToDecimal(tb_salary.Text);

                        if (salary <= max_amnt && z == "0.00")

                        {
                            //employee
                            string employee_cont = (dr["emply_cont"].ToString());
                            decimal emply_cont = Convert.ToDecimal(employee_cont);
                            decimal ee_total_contribution = Math.Round(emply_cont, 2, MidpointRounding.AwayFromZero);

                            tb_philhealth.Text = ee_total_contribution + "";

                            //employeer

                            label117.Text = tb_philhealth.Text;

                            decimal emplyr_cont = Convert.ToDecimal(label117.Text);
                            decimal er_total_contribution = Math.Round(emplyr_cont, 2, MidpointRounding.AwayFromZero);
                            label117.Text = er_total_contribution + "";
                        }
                        else
                        {
                            //employee
                            label17.Text = dr["percent"].ToString();
                            decimal value = Convert.ToDecimal(label17.Text);

                            decimal total_salary = Convert.ToDecimal(tb_salary.Text);

                            decimal percentage = value / 100;
                            decimal total_percent = percentage * total_salary;
                            decimal employee_contribution = total_percent / 2;

                            decimal ee_total_contribution = Math.Round(employee_contribution, 2, MidpointRounding.AwayFromZero);
                            tb_philhealth.Text = ee_total_contribution + "";

                            //employer
                            label117.Text = tb_philhealth.Text;

                            decimal emplyr_cont = Convert.ToDecimal(label117.Text);
                            decimal er_total_contribution = Math.Round(emplyr_cont, 2, MidpointRounding.AwayFromZero);
                            label117.Text = er_total_contribution + "";
                        }

                    }

                }
                catch (Exception ex)
                {
                    MessageBox.Show(ex.Message);
                }
                finally
                {
                    connection.Close();
                }
            }

            private void tp_general_information_Enter(object sender, EventArgs e)
            {
                lv_employee.Columns.Clear();
                Personal_Information("Select * from employee");
                lbl_category.Text = "General Information";

                string emp_no = lbl_emp_no.Text;

                emp_details(emp_no);

            }

            private void emp_details(string emp_no)
            {
                string myconnection = ConfigurationManager.ConnectionStrings["MyConstring"].ConnectionString;
                MySqlConnection connection = new MySqlConnection(myconnection);

                try
                {
                    connection.Open();
                    cmd = new MySqlCommand();
                    cmd.Connection = connection;
                    cmd.CommandText = "Select * from employee where emp_no = '" + emp_no + "'";
                    da = new MySqlDataAdapter();
                    da.SelectCommand = cmd;
                    dt = new DataTable();
                    da.Fill(dt);

                    for (int i = 0; i < dt.Rows.Count; i++)
                    {
                        DataRow dr = dt.Rows[i];

                        lbl_emp_no.Text = dr["emp_no"].ToString();
                        label1.Text = dr["emp_no"].ToString();

                        cb_statustype.Text = dr["status"].ToString();

                        tb_empID.Text = dr["emp_id"].ToString();
                        tb_lname.Text = dr["lname"].ToString();
                        tb_fname.Text = dr["fname"].ToString();
                        tb_mname.Text = dr["mname"].ToString();

                        tb_info_empID.Text = dr["emp_id"].ToString();
                        tb_info_lname.Text = dr["lname"].ToString();
                        tb_info_fname.Text = dr["fname"].ToString();
                        tb_info_mname.Text = dr["mname"].ToString();

                        tb_email.Text = dr["email"].ToString();
                        cb_pob.Text = dr["pob"].ToString();
                        cb_status.Text = dr["status_type"].ToString();
                        tb_cont_no.Text = dr["contact_no"].ToString();
                        tb_comp_email.Text = dr["cp_email"].ToString();
                        tb_telphone_no.Text = dr["tel_no"].ToString();
                        cb_gender.Text = dr["gender"].ToString();
                        cb_nationality.Text = dr["nationality"].ToString();
                        tb_name_of_spouse.Text = dr["spouse_name"].ToString();
                        cb_religion.Text = dr["religion"].ToString();
                        rtb_address.Text = dr["address"].ToString();
                        readPhotoEmployee();
                        tb_elementary.Text = dr["elementary"].ToString();
                        tb_highSchool.Text = dr["high_school"].ToString();
                        tb_tertiary.Text = dr["college"].ToString();
                        tb_course.Text = dr["course"].ToString();
                        rtb_work_experience.Text = dr["work_experience"].ToString();
                        tb_mothers_name.Text = dr["mothers_name"].ToString();
                        tb_occupation_mother.Text = dr["mothers_occ"].ToString();
                        tb_fathers_name.Text = dr["fathers_name"].ToString();
                        tb_occupation_father.Text = dr["fathers_occ"].ToString();
                        rtb_homeAddress.Text = dr["home_address"].ToString();
                        tb_name.Text = dr["primary_con"].ToString();
                        tb_primary_con.Text = dr["contact_num"].ToString();
                        tb_relationship.Text = dr["relationship"].ToString();
                        tb_name2.Text = dr["secondary_con"].ToString();
                        tb_second_con.Text = dr["contact_num2"].ToString();
                        tb_relationship2.Text = dr["relationship2"].ToString();

                    }

                }
                catch (Exception ex)
                {
                    MessageBox.Show(ex.Message);
                }
                finally
                {
                    connection.Close();
                }
            }


            private void tp_other_info_Enter(object sender, EventArgs e)
            {

                lv_employee.Columns.Clear();
                Personal_Information("Select * from employee");
                lbl_category.Text = "Other Information";

                lbl_emp_no.Text = label1.Text;
                label83.Text = label1.Text;
                label79.Text = label1.Text;
                label89.Text = label1.Text;
                label88.Text = label1.Text;


                if (label1.Text == lbl_emp_no.Text)
                {
                    tb_info_empID.Text = tb_empID.Text;
                    tb_info_lname.Text = tb_lname.Text;
                    tb_info_fname.Text = tb_fname.Text;
                    tb_info_mname.Text = tb_mname.Text;

                }

                string myconnection = ConfigurationManager.ConnectionStrings["MyConstring"].ConnectionString;
                MySqlConnection connection = new MySqlConnection(myconnection);

                try
                {
                    connection.Open();
                    cmd = new MySqlCommand();
                    cmd.Connection = connection;
                    cmd.CommandText = "Select * from employee where emp_no = '" + label1.Text + "'";
                    da = new MySqlDataAdapter();
                    da.SelectCommand = cmd;
                    dt = new DataTable();
                    da.Fill(dt);

                    for (int i = 0; i < dt.Rows.Count; i++)
                    {
                        DataRow dr = dt.Rows[i];

                        label1.Text = dr["emp_no"].ToString();
                        tb_info_empID.Text = dr["emp_id"].ToString();
                        tb_info_lname.Text = dr["lname"].ToString();
                        tb_info_fname.Text = dr["fname"].ToString();
                        tb_info_mname.Text = dr["mname"].ToString();
                        tb_elementary.Text = dr["elementary"].ToString();
                        tb_highSchool.Text = dr["high_school"].ToString();
                        tb_tertiary.Text = dr["college"].ToString();
                        tb_course.Text = dr["course"].ToString();
                        rtb_work_experience.Text = dr["work_experience"].ToString();
                        tb_mothers_name.Text = dr["mothers_name"].ToString();
                        tb_occupation_mother.Text = dr["mothers_occ"].ToString();
                        tb_fathers_name.Text = dr["fathers_name"].ToString();
                        tb_occupation_father.Text = dr["fathers_occ"].ToString();
                        rtb_homeAddress.Text = dr["home_address"].ToString();
                        tb_name.Text = dr["primary_con"].ToString();
                        tb_primary_con.Text = dr["contact_num"].ToString();
                        tb_relationship.Text = dr["relationship"].ToString();
                        tb_name2.Text = dr["secondary_con"].ToString();
                        tb_second_con.Text = dr["contact_num2"].ToString();
                        tb_relationship2.Text = dr["relationship2"].ToString();

                    }

                }
                catch (Exception ex)
                {
                    MessageBox.Show(ex.Message);
                }
                finally
                {
                    connection.Close();
                }

            }

            private void tp_salary_Enter(object sender, EventArgs e)
            {


                lv_employee.Columns.Clear();
                Salary("SELECT * FROM salary LEFT JOIN employee ON salary.emp_no = employee.emp_no");
                lbl_category.Text = "Salary";

                lbl_emp_no.Text = label83.Text;
                label79.Text = label83.Text;
                label89.Text = label83.Text;
                label88.Text = label83.Text;
                label1.Text = label83.Text;


                if (label83.Text == lbl_emp_no.Text)
                {
                    tb_sal_empID.Text = tb_empID.Text;
                    tb_sal_lname.Text = tb_lname.Text;
                    tb_sal_fname.Text = tb_fname.Text;
                    tb_sal_mname.Text = tb_mname.Text;

                }




                string myconnection = ConfigurationManager.ConnectionStrings["MyConstring"].ConnectionString;
                MySqlConnection connection = new MySqlConnection(myconnection);

                try
                {
                    connection.Open();
                    cmd = new MySqlCommand();
                    cmd.Connection = connection;
                    cmd.CommandText = "Select * FROM SALARY left join employee on salary.emp_no = employee.emp_no where salary.emp_no = '" + label83.Text + "'";
                    da = new MySqlDataAdapter();
                    da.SelectCommand = cmd;
                    dt = new DataTable();
                    da.Fill(dt);

                    for (int i = 0; i < dt.Rows.Count; i++)
                    {
                        DataRow dr = dt.Rows[i];

                        label118.Text = dr["salary_id"].ToString();
                        label83.Text = dr["emp_no"].ToString();
                        tb_sal_empID.Text = dr["emp_id"].ToString();
                        tb_sal_lname.Text = dr["lname"].ToString();
                        tb_sal_fname.Text = dr["fname"].ToString();
                        tb_sal_mname.Text = dr["mname"].ToString();
                        dtp_date_hired.Text = dr["date_hired"].ToString();
                        tb_designation.Text = dr["designation"].ToString();
                        cb_department.SelectedItem = dr["department"].ToString();
                        cb_confidentiality.SelectedItem = dr["con_level"].ToString();
                        cb_employee_status.SelectedItem = dr["emp_status"].ToString();
                        dtp_dateResign.CustomFormat = dr["date_resign"].ToString();
                        cb_reason.SelectedItem = dr["reason"].ToString();
                        cb_payRate.SelectedItem = dr["pay_rate"].ToString();
                        cb_paySchedule.SelectedItem = dr["pay_sched"].ToString();
                        tb_salary.Text = string.Format("{0:n}", decimal.Parse(dr["salary"].ToString()));
                        tb_daily_rate.Text = string.Format("{0:n}", decimal.Parse(dr["daily_rate"].ToString()));
                        tb_daily_hour_rate.Text = string.Format("{0:n}", decimal.Parse(dr["daily_hour_rate"].ToString()));
                        tb_minRate.Text = string.Format("{0:n}", decimal.Parse(dr["min_rate"].ToString()));
                        tb_riceAllowance.Text = string.Format("{0:n}", decimal.Parse(dr["rice_allowance"].ToString()));
                        tb_laundryAllowance.Text = string.Format("{0:n}", decimal.Parse(dr["laundry_allowance"].ToString()));
                        tb_mealAllowance.Text = string.Format("{0:n}", decimal.Parse(dr["meal_allowance"].ToString()));
                        tb_MedicalBenefits.Text = string.Format("{0:n}", decimal.Parse(dr["medical_benefits"].ToString()));
                        tb_otherAllowance.Text = string.Format("{0:n}", decimal.Parse(dr["other_allowance"].ToString()));
                        cb_payroll_mode.SelectedItem = dr["pay_mode"].ToString();
                        cb_bank_name.SelectedItem = dr["bank_name"].ToString();
                        tb_bank_acc.Text = dr["bank_acc"].ToString();
                        cb_bank_name2.SelectedItem = dr["bank_name2"].ToString();
                        tb_bank_acc2.Text = dr["bank_acc2"].ToString();
                        tb_13th_month_pay.Text = string.Format("{0:n}", decimal.Parse(dr["13thmonthpay"].ToString()));
                        tb_14th_month_pay.Text = string.Format("{0:n}", decimal.Parse(dr["14thmonthpay"].ToString()));
                        tb_15th_month_pay.Text = string.Format("{0:n}", decimal.Parse(dr["15thmonthpay"].ToString()));
                        tb_sss.Text = string.Format("{0:n}", decimal.Parse(dr["sss_ee_monthly"].ToString()));
                        label115.Text = string.Format("{0:n}", decimal.Parse(dr["sss_er_monthly"].ToString()));
                        tb_pag_ibig.Text = string.Format("{0:n}", decimal.Parse(dr["pag_ibig_ee_monthly"].ToString()));
                        label116.Text = string.Format("{0:n}", decimal.Parse(dr["pag_ibig_er_monthly"].ToString()));
                        tb_philhealth.Text = string.Format("{0:n}", decimal.Parse(dr["philhealth_ee_monthly"].ToString()));
                        label117.Text = string.Format("{0:n}", decimal.Parse(dr["philhealth_er"].ToString()));

                    //check_benefits();

                    if (cb_payroll_mode.SelectedItem == null || cb_payroll_mode.SelectedIndex == 2)
                        {
                            cb_bank_name.Enabled = false;
                            tb_bank_acc.Enabled = false;
                            cb_bank_name2.Enabled = false;
                            tb_bank_acc2.Enabled = false;

                        }

                    }

                }
                catch (Exception ex)
                {
                    MessageBox.Show(ex.Message);
                }
                finally
                {
                    connection.Close();
                }


            }

        /*private void check_benefits()
        {

            if (tb_sss.Text == "0.00" || tb_sss.Text == "")
            {
                chk_sss.Checked = false;
            }
            else
            {
                chk_sss.Checked = true;
            }

            if (tb_pag_ibig.Text == "0.00" || tb_pag_ibig.Text == "")
            {
                chk_pag_ibig.Checked = false;
            }
            else
            {
                chk_pag_ibig.Checked = true;
            }

            if (tb_philhealth.Text == "0.00" || tb_philhealth.Text == "")
            {
                chk_philhealth.Checked = false;
            }
            else
            {
                chk_philhealth.Checked = true;
            }


            if (tb_13th_month_pay.Text == "0.00" || tb_13th_month_pay.Text == "")
            {
                chb_13month_pay.Checked = false;
            }
            else
            {
                chb_13month_pay.Checked = true;
            }


            if (tb_14th_month_pay.Text == "0.00" || tb_14th_month_pay.Text == "")
            {
                chb_14month_pay.Checked = false;
            }
            else
            {
                chb_14month_pay.Checked = true;
            }



            if (tb_15th_month_pay.Text == "0.00" || tb_15th_month_pay.Text == "")
            {
                chb_15month_pay.Checked = false;
            }
            else
            {
                chb_15month_pay.Checked = true;
            }
        }*/


        public void insert_id()
            {
                string myconnection = ConfigurationManager.ConnectionStrings["MyConstring"].ConnectionString;
                MySqlConnection connection = new MySqlConnection(myconnection);

                string insertQuery = "INSERT INTO others_id(emp_no, sss_no, pag_ibig_no, tin_no, philhealth_no)VALUES('" + label56.Text + "' ,'" + tb_sss_no.Text + "', '" + tb_hdmf_no.Text + "', '" + tb_tin_no.Text + "', '" + tb_philhealth_no.Text + "')";


                connection.Open();
                MySqlCommand mySqlCommand = new MySqlCommand(insertQuery, connection);


                try
                {
                    if (mySqlCommand.ExecuteNonQuery() == 1)
                    {
                        //MessageBox.Show("Successfully Add Information", "Add Information", MessageBoxButtons.OK, MessageBoxIcon.Information);
                    }

                }
                catch (Exception ex)
                {
                    MessageBox.Show("Error" + ex);
                }

                connection.Close();
            }
            public void insert_loan()
            {
                string myconnection = ConfigurationManager.ConnectionStrings["MyConstring"].ConnectionString;
                MySqlConnection connection = new MySqlConnection(myconnection);

                decimal comp_loan_amount = Convert.ToDecimal(tb_comp_loan_amnt.Text);
                decimal comp_loan_month_ded = Convert.ToDecimal(tb_comp_loan_month_ded.Text);
                decimal comp_loan_bal = Convert.ToDecimal(tb_comp_loan_bal.Text);

                decimal sss_loan_amount = Convert.ToDecimal(tb_sss_loan_amnt.Text);
                decimal sss_loan_month_ded = Convert.ToDecimal(tb_sss_loan_month_ded.Text);
                decimal sss_loan_bal = Convert.ToDecimal(tb_sss_loan_bal.Text);

                decimal pag_ibig_loan_amount = Convert.ToDecimal(tb_pag_ibig_loan_amnt.Text);
                decimal pag_ibig_loan_month_ded = Convert.ToDecimal(tb_pag_ibig_loan_month_ded.Text);
                decimal pag_big_loan_bal = Convert.ToDecimal(tb_pag_ibig_loan_bal.Text);

                string insertQuery = "INSERT INTO loan(emp_no, comp_loan_amnt, comp_bal, comp_month_ded, sss_loan_amnt, sss_bal, sss_month_ded,  pag_ibig_loan_amnt, pag_ibig_bal, pag_ibig_month_ded)VALUES('" + label57.Text + "',  '" + comp_loan_amount + "', '" + comp_loan_bal + "', '" + comp_loan_month_ded + "', '" + sss_loan_amount + "', '" + sss_loan_bal + "', '" + sss_loan_month_ded + "', '" + pag_ibig_loan_amount + "', '" + pag_big_loan_bal + "', '" + pag_ibig_loan_month_ded + "')";

                connection.Open();
                MySqlCommand mySqlCommand = new MySqlCommand(insertQuery, connection);


                try
                {
                    if (mySqlCommand.ExecuteNonQuery() == 1)
                    {
                        //MessageBox.Show("Successfully Add Information", "Add Information", MessageBoxButtons.OK, MessageBoxIcon.Information);
                    }

                }
                catch (Exception ex)
                {
                    MessageBox.Show("Error" + ex);
                }

                connection.Close();
            }

            public void add_leave_balanced()
            {
                string myconnection = ConfigurationManager.ConnectionStrings["MyConstring"].ConnectionString;
                MySqlConnection connection = new MySqlConnection(myconnection);

                string insertQuery = "INSERT INTO leave_balance(emp_no, sick_leave, vacation_leave)VALUES('" + label45.Text + "', '" + tb_sick_leave.Text + "', '" + tb_vacation_leave.Text + "')";


                connection.Open();
                MySqlCommand mySqlCommand = new MySqlCommand(insertQuery, connection);


                try
                {
                    if (mySqlCommand.ExecuteNonQuery() == 1)
                    {
                        //MessageBox.Show("Successfully Add Information", "Add Information", MessageBoxButtons.OK, MessageBoxIcon.Information);
                    }

                }
                catch (Exception ex)
                {
                    MessageBox.Show("Error" + ex);
                }

                connection.Close();
            }

            private void tp_Leave_Enter(object sender, EventArgs e)
            {


                lv_employee.Columns.Clear();
                leave_balanced("SELECT * FROM leave_balance LEFT JOIN employee ON leave_balance.emp_no = employee.emp_no");
                lbl_category.Text = "Leave";


                if (label79.Text == lbl_emp_no.Text)
                {
                    tb_leave_empID.Text = tb_empID.Text;
                    tb_leave_lname.Text = tb_lname.Text;
                    tb_leave_fname.Text = tb_fname.Text;
                    tb_leave_mname.Text = tb_mname.Text;
                    label45.Text = lbl_number_of_employee.Text;

                }

                lbl_emp_no.Text = label79.Text;
                label83.Text = label79.Text;
                label89.Text = label79.Text;
                label88.Text = label79.Text;
                label1.Text = label79.Text;

                string myconnection = ConfigurationManager.ConnectionStrings["MyConstring"].ConnectionString;
                MySqlConnection connection = new MySqlConnection(myconnection);


                try
                {
                    connection.Open();
                    cmd = new MySqlCommand();
                    cmd.Connection = connection;
                    cmd.CommandText = "Select * FROM leave_balance left join employee on leave_balance.emp_no = employee.emp_no where leave_balance.emp_no = '" + label79.Text + "'";
                    da = new MySqlDataAdapter();
                    da.SelectCommand = cmd;
                    dt = new DataTable();
                    da.Fill(dt);

                    for (int i = 0; i < dt.Rows.Count; i++)
                    {
                        DataRow dr = dt.Rows[i];

                        tb_leave_empID.Text = dr["emp_id"].ToString();
                        tb_leave_lname.Text = dr["lname"].ToString();
                        tb_leave_fname.Text = dr["fname"].ToString();
                        tb_leave_mname.Text = dr["mname"].ToString();
                        label31.Text = dr["leave_balance_id"].ToString();
                        label79.Text = dr["emp_no"].ToString();
                        tb_sick_leave.Text = dr["sick_leave"].ToString();
                        tb_vacation_leave.Text = dr["vacation_leave"].ToString();

                    }
                }
                catch (Exception ex)
                {
                    MessageBox.Show(ex.Message);
                }
                finally
                {

                    connection.Close();

                }

            }

            private void tp_id_Enter(object sender, EventArgs e)
            {
                lv_employee.Columns.Clear();
                ID("Select * FROM others_id LEFT JOIN employee ON others_id.emp_no = employee.emp_no");
                lbl_category.Text = "Other ID";

                if (label88.Text == lbl_emp_no.Text)
                {
                    tb_id_empID.Text = tb_empID.Text;
                    tb_id_lname.Text = tb_lname.Text;
                    tb_id_fname.Text = tb_fname.Text;
                    tb_id_mname.Text = tb_mname.Text;
                }

                lbl_emp_no.Text = label88.Text;
                label83.Text = label88.Text;
                label79.Text = label88.Text;
                label89.Text = label88.Text;
                label1.Text = label88.Text;


                string myconnection = ConfigurationManager.ConnectionStrings["MyConstring"].ConnectionString;
                MySqlConnection connection = new MySqlConnection(myconnection);

                try
                {
                    connection.Open();
                    cmd = new MySqlCommand();
                    cmd.Connection = connection;
                    cmd.CommandText = "Select * FROM others_id left join employee on others_id.emp_no = employee.emp_no where others_id.emp_no = '" + label88.Text + "'";
                    da = new MySqlDataAdapter();
                    da.SelectCommand = cmd;
                    dt = new DataTable();
                    da.Fill(dt);

                    for (int i = 0; i < dt.Rows.Count; i++)
                    {
                        DataRow dr = dt.Rows[i];


                        tb_id_empID.Text = dr["emp_id"].ToString();
                        tb_id_lname.Text = dr["lname"].ToString();
                        tb_id_fname.Text = dr["fname"].ToString();
                        tb_id_mname.Text = dr["mname"].ToString();
                        label119.Text = dr["other_id"].ToString();
                        label88.Text = dr["emp_no"].ToString();
                        tb_sss_no.Text = dr["sss_no"].ToString();
                        tb_hdmf_no.Text = dr["pag_ibig_no"].ToString();
                        tb_tin_no.Text = dr["tin_no"].ToString();
                        tb_philhealth_no.Text = dr["philhealth_no"].ToString();

                    }

                }
                catch (Exception ex)
                {
                    MessageBox.Show(ex.Message);
                }
                finally
                {
                    connection.Close();
                }

            }

            private void tp_loan_Enter(object sender, EventArgs e)
            {
                lv_employee.Columns.Clear();
                Loan("SELECT * FROM loan LEFT JOIN employee ON loan.emp_no = employee.emp_no");
                lbl_category.Text = "Loan";

                if (label89.Text == lbl_emp_no.Text)
                {
                    tb_loan_empID.Text = tb_empID.Text;
                    tb_loan_lname.Text = tb_lname.Text;
                    tb_loan_fname.Text = tb_fname.Text;
                    tb_loan_mname.Text = tb_mname.Text;
                }

                lbl_emp_no.Text = label89.Text;
                label83.Text = label89.Text;
                label79.Text = label89.Text;
                label88.Text = label89.Text;
                label1.Text = label89.Text;

                string myconnection = ConfigurationManager.ConnectionStrings["MyConstring"].ConnectionString;
                MySqlConnection connection = new MySqlConnection(myconnection);

                try
                {
                    connection.Open();
                    cmd = new MySqlCommand();
                    cmd.Connection = connection;
                    cmd.CommandText = "Select * FROM loan left join employee on loan.emp_no = employee.emp_no where loan.emp_no = '" + label89.Text + "'";
                    da = new MySqlDataAdapter();
                    da.SelectCommand = cmd;
                    dt = new DataTable();
                    da.Fill(dt);

                    for (int i = 0; i < dt.Rows.Count; i++)
                    {
                        DataRow dr = dt.Rows[i];

                       label120.Text = dr["loan_id"].ToString();
                       label89.Text = dr["emp_no"].ToString();
                       tb_loan_empID.Text = dr["emp_id"].ToString();
                       tb_loan_lname.Text = dr["lname"].ToString();
                       tb_loan_fname.Text = dr["fname"].ToString();
                       tb_loan_fname.Text = dr["mname"].ToString();
                       tb_comp_loan_amnt.Text = string.Format("{0:n}", decimal.Parse(dr["comp_loan_amnt"].ToString()));
                       tb_comp_loan_bal.Text = string.Format("{0:n}", decimal.Parse(dr["comp_bal"].ToString()));
                       tb_comp_loan_month_ded.Text = string.Format("{0:n}", decimal.Parse(dr["comp_month_ded"].ToString()));
                       tb_sss_loan_amnt.Text = string.Format("{0:n}", decimal.Parse(dr["sss_loan_amnt"].ToString()));
                       tb_sss_loan_bal.Text = string.Format("{0:n}", decimal.Parse(dr["sss_bal"].ToString()));
                       tb_sss_loan_month_ded.Text = string.Format("{0:n}", decimal.Parse(dr["sss_month_ded"].ToString()));
                       tb_pag_ibig_loan_amnt.Text = string.Format("{0:n}", decimal.Parse(dr["pag_ibig_loan_amnt"].ToString()));
                       tb_pag_ibig_loan_bal.Text = string.Format("{0:n}", decimal.Parse(dr["pag_ibig_bal"].ToString()));
                       tb_pag_ibig_loan_month_ded.Text = string.Format("{0:n}", decimal.Parse(dr["pag_ibig_month_ded"].ToString()));


                    }

                }
                catch (Exception ex)
                {
                    MessageBox.Show(ex.Message);
                }
                finally
                {
                    connection.Close();
                }

            }



            private void chk_sss_CheckedChanged(object sender, EventArgs e)
            {
                if (chk_sss.Checked == true)
                {

                    calculate_range_of_sss();
                    tb_sss.Text = string.Format("{0:n}", decimal.Parse(tb_sss.Text));

                }
                else
                {
                    tb_sss.Text = "0.00";
                    label115.Text = "0.00";
                }

            }

            private void chk_pag_ibig_CheckedChanged(object sender, EventArgs e)
            {
                if (chk_pag_ibig.Checked == true)
                {

                    calculate_range_of_pag_ibig();
                }
                else
                {
                    tb_pag_ibig.Text = "0.00";
                    label116.Text = "0.00";

                }
            }

            private void chk_philhealth_CheckedChanged(object sender, EventArgs e)
            {
                if (chk_philhealth.Checked == true)
                {
                    calculate_range_of_philhealth();
                }
                else
                {
                    tb_philhealth.Text = "0.00";
                    label117.Text = "0.00";


                }
            }
            private void EmployeeID()
            {


                string myconnection = ConfigurationManager.ConnectionStrings["MyConstring"].ConnectionString;
                MySqlConnection connection = new MySqlConnection(myconnection);

                try
                {
                    connection.Open();
                    cmd = new MySqlCommand();
                    cmd.Connection = connection;
                    cmd.CommandText = "Select count(*) FROM employee";
                    da = new MySqlDataAdapter();
                    da.SelectCommand = cmd;
                    dt = new DataTable();
                    da.Fill(dt);

                    for (int i = 0; i < dt.Rows.Count; i++)
                    {
                        DataRow dr = dt.Rows[i];



                        label30.Text = (dr["count(*)"].ToString());
                        int y = Convert.ToInt32(label30.Text);

                        int x = Convert.ToInt32(lbl_number_of_employee.Text);
                        int clientID = y + 1;

                        lbl_number_of_employee.Text = clientID + "";
                        label45.Text = clientID + "";
                        label55.Text = clientID + "";
                        label56.Text = clientID + "";
                        label57.Text = clientID + "";
                        //label114.Text = clientID + "";

                    }

                }
                catch (Exception ex)
                {
                    MessageBox.Show(ex.Message);
                }
                {

                    connection.Close();
                }
            }

            private void tb_sick_leave_KeyPress(object sender, KeyPressEventArgs e)
            {
                if (((e.KeyChar < 48 || e.KeyChar > 57) && e.KeyChar != 8 && e.KeyChar != 46))
                {
                    e.Handled = true;
                    return;
                }

                // checks to make sure only 1 decimal is allowed
                if (e.KeyChar == 46)
                {
                    if ((sender as TextBox).Text.IndexOf(e.KeyChar) != -1)
                        e.Handled = true;
                }
            }

            private void tb_vacation_leave_KeyPress(object sender, KeyPressEventArgs e)
            {
                if (((e.KeyChar < 48 || e.KeyChar > 57) && e.KeyChar != 8 && e.KeyChar != 46))
                {
                    e.Handled = true;
                    return;
                }

                // checks to make sure only 1 decimal is allowed
                if (e.KeyChar == 46)
                {
                    if ((sender as TextBox).Text.IndexOf(e.KeyChar) != -1)
                        e.Handled = true;
                }
            }

            public void compute_regular_pay()
            {
                if (tb_salary.Text == "")
                {

                    tb_salary.Text = "0.00";
                }

                decimal y = Convert.ToDecimal(tb_salary.Text);


                decimal salary = y / 2;
                decimal total_reg_pay = salary;
                decimal total = Math.Round(total_reg_pay, 2, MidpointRounding.AwayFromZero);

            }
            public void compute_daily_rate()
            {
                if (tb_salary.Text == "")
                {
                    tb_salary.Text = "0.00";
                }

                if (cb_payRate.SelectedIndex == 1)
                {
                    tb_daily_rate.Text = tb_salary.Text;

                }
                else
                {


                    string myconnection = ConfigurationManager.ConnectionStrings["MyConstring"].ConnectionString;
                    MySqlConnection connection = new MySqlConnection(myconnection);

                    string comp_details = null;
                    int rate = 0;

                    try
                    {
                        connection.Open();
                        cmd = new MySqlCommand();
                        cmd.Connection = connection;
                        cmd.CommandText = "Select * FROM comp_details";
                        da = new MySqlDataAdapter();
                        da.SelectCommand = cmd;
                        dt = new DataTable();
                        da.Fill(dt);

                        for (int i = 0; i < dt.Rows.Count; i++)
                        {
                            DataRow dr = dt.Rows[i];

                            comp_details = (dr["rate"].ToString());
                        }

                    }
                    catch (Exception ex)
                    {
                        MessageBox.Show(ex.Message);
                    }
                    finally
                    {
                        connection.Close();
                    }

                    if (comp_details == "1")
                    {
                        rate = 365;

                    }
                    else if (comp_details == "2")
                    {
                        rate = 313;

                    }
                    else if (comp_details == "3")
                    {
                        rate = 287;

                    }
                    else if (comp_details == "4")
                    {
                        rate = 261;
                    }

                    decimal salary = Convert.ToDecimal(tb_salary.Text);
                    decimal total_daily_rate = salary * 12 / rate;
                    decimal b = Math.Round(total_daily_rate, 2, MidpointRounding.AwayFromZero);
                    tb_daily_rate.Text = b + "";
                    tb_daily_rate.Text = string.Format("{0:n}", decimal.Parse(tb_daily_rate.Text));
                }

            }
            public void compute_daily_hour_rate()
            {
                if (tb_daily_rate.Text == "")
                {
                    tb_daily_rate.Text = "0.00";
                }

                decimal z = Convert.ToDecimal(tb_daily_rate.Text);
                decimal total_hourly_rate = z / 8;
                decimal c = Math.Round(total_hourly_rate, 2, MidpointRounding.AwayFromZero);
                tb_daily_hour_rate.Text = c + "";
                tb_daily_hour_rate.Text = string.Format("{0:n}", decimal.Parse(tb_daily_hour_rate.Text));
            }

            private void tb_regular_pay_KeyPress(object sender, KeyPressEventArgs e)
            {
                if ((e.KeyChar < 32) || (e.KeyChar >= '0') && (e.KeyChar <= '9') || (e.KeyChar == ',') || (e.KeyChar == '.'))
                    return;

                // only evident errors (like 'A' or '&') are restricted
                e.Handled = true;
            }

            private void tb_sss_KeyPress(object sender, KeyPressEventArgs e)
            {
                if ((e.KeyChar < 32) || (e.KeyChar >= '0') && (e.KeyChar <= '9') || (e.KeyChar == ',') || (e.KeyChar == '.'))
                    return;

                // only evident errors (like 'A' or '&') are restricted
                e.Handled = true;
            }

            private void tb_pag_ibig_KeyPress(object sender, KeyPressEventArgs e)
            {
                if ((e.KeyChar < 32) || (e.KeyChar >= '0') && (e.KeyChar <= '9') || (e.KeyChar == ',') || (e.KeyChar == '.'))
                    return;

                // only evident errors (like 'A' or '&') are restricted
                e.Handled = true;
            }

            private void tb_philhealth_KeyPress(object sender, KeyPressEventArgs e)
            {
                if ((e.KeyChar < 32) || (e.KeyChar >= '0') && (e.KeyChar <= '9') || (e.KeyChar == ',') || (e.KeyChar == '.'))
                    return;

                // only evident errors (like 'A' or '&') are restricted
                e.Handled = true;
            }

            private void tb_total_deduction_KeyPress(object sender, KeyPressEventArgs e)
            {
                if (((e.KeyChar < 48 || e.KeyChar > 57) && e.KeyChar != 8 && e.KeyChar != 46))
                {
                    e.Handled = true;
                    return;
                }


                if (e.KeyChar == 46)
                {
                    if ((sender as TextBox).Text.IndexOf(e.KeyChar) != -1)
                        e.Handled = true;
                }
            }

            private void tb_13th_month_pay_KeyPress(object sender, KeyPressEventArgs e)
            {
                if ((e.KeyChar < 32) || (e.KeyChar >= '0') && (e.KeyChar <= '9') || (e.KeyChar == ',') || (e.KeyChar == '.'))
                    return;

                // only evident errors (like 'A' or '&') are restricted
                e.Handled = true;
            }

            private void tb_14th_month_pay_KeyPress(object sender, KeyPressEventArgs e)
            {
                if ((e.KeyChar < 32) || (e.KeyChar >= '0') && (e.KeyChar <= '9') || (e.KeyChar == ',') || (e.KeyChar == '.'))
                    return;

                // only evident errors (like 'A' or '&') are restricted
                e.Handled = true;
            }

            private void tb_15th_month_pay_KeyPress(object sender, KeyPressEventArgs e)
            {
                if ((e.KeyChar < 32) || (e.KeyChar >= '0') && (e.KeyChar <= '9') || (e.KeyChar == ',') || (e.KeyChar == '.'))
                    return;

                // only evident errors (like 'A' or '&') are restricted
                e.Handled = true;
            }

            private void tb_loan_amount_KeyPress(object sender, KeyPressEventArgs e)
            {
                if ((e.KeyChar < 32) || (e.KeyChar >= '0') && (e.KeyChar <= '9') || (e.KeyChar == ',') || (e.KeyChar == '.'))
                    return;

                // only evident errors (like 'A' or '&') are restricted
                e.Handled = true;
            }

            private void tb_monthly_ded_KeyPress(object sender, KeyPressEventArgs e)
            {
                if ((e.KeyChar < 32) || (e.KeyChar >= '0') && (e.KeyChar <= '9') || (e.KeyChar == ',') || (e.KeyChar == '.'))
                    return;

                // only evident errors (like 'A' or '&') are restricted
                e.Handled = true;

            }

            private void tb_loan_bal_KeyPress(object sender, KeyPressEventArgs e)
            {
                if ((e.KeyChar < 32) || (e.KeyChar >= '0') && (e.KeyChar <= '9') || (e.KeyChar == ',') || (e.KeyChar == '.'))
                    return;

                // only evident errors (like 'A' or '&') are restricted
                e.Handled = true;
            }

            private void tb_sss_loan_amount_KeyPress(object sender, KeyPressEventArgs e)
            {
                if ((e.KeyChar < 32) || (e.KeyChar >= '0') && (e.KeyChar <= '9') || (e.KeyChar == ',') || (e.KeyChar == '.'))
                    return;

                // only evident errors (like 'A' or '&') are restricted
                e.Handled = true;
            }


            private void tb_sss_loan_bal_KeyPress(object sender, KeyPressEventArgs e)
            {
                if ((e.KeyChar < 32) || (e.KeyChar >= '0') && (e.KeyChar <= '9') || (e.KeyChar == ',') || (e.KeyChar == '.'))
                    return;

                // only evident errors (like 'A' or '&') are restricted
                e.Handled = true;
            }

            private void tb_sss_loan_month_ded_KeyPress(object sender, KeyPressEventArgs e)
            {
                if ((e.KeyChar < 32) || (e.KeyChar >= '0') && (e.KeyChar <= '9') || (e.KeyChar == ',') || (e.KeyChar == '.'))
                    return;

                // only evident errors (like 'A' or '&') are restricted
                e.Handled = true;
            }



            private void tb_pag_ibig_loan_bal_KeyPress(object sender, KeyPressEventArgs e)
            {
                if ((e.KeyChar < 32) || (e.KeyChar >= '0') && (e.KeyChar <= '9') || (e.KeyChar == ',') || (e.KeyChar == '.'))
                    return;

                // only evident errors (like 'A' or '&') are restricted
                e.Handled = true;
            }



            private void tb_pag_ibig_loan_month_ded_KeyPress(object sender, KeyPressEventArgs e)
            {
                if ((e.KeyChar < 32) || (e.KeyChar >= '0') && (e.KeyChar <= '9') || (e.KeyChar == ',') || (e.KeyChar == '.'))
                    return;

                // only evident errors (like 'A' or '&') are restricted
                e.Handled = true;
            }



            private void tb_pag_ibig_loan_amnt_KeyPress(object sender, KeyPressEventArgs e)
            {
                if ((e.KeyChar < 32) || (e.KeyChar >= '0') && (e.KeyChar <= '9') || (e.KeyChar == ',') || (e.KeyChar == '.'))
                    return;

                // only evident errors (like 'A' or '&') are restricted
                e.Handled = true;
            }

            private void tb_regular_pay_Enter(object sender, EventArgs e)
            {
                compute_regular_pay();
            }


            public void disable_all()
            {
                btn_add.Image = Properties.Resources.Button_ADD_Red__1___1_;
                btn_update.Image = Properties.Resources.Button_UPDATE_Red__4___1___1_;
                pic_Image.Image = Properties.Resources.attach_photo;
                btn_Delete.Enabled = false;
                lbl_emp_no.Text = "0";

                label79.Text = "0";
                label31.Text = "0";
                label1.Text = "0";
                label118.Text = "0";
                label83.Text = "0";
                label119.Text = "0";
                label88.Text = "0";
                label120.Text = "0";
                label89.Text = "0";

                btn_add.Text = "ADD";
                btn_update.Text = "EDIT";

                btn_add.Enabled = true;               
                btn_update.Enabled = false;
                tb_empID.Text = "";
                tb_leave_empID.Text = "";

                tb_info_empID.Text = "";
                tb_info_lname.Text = "";
                tb_info_fname.Text = "";
                tb_info_mname.Text = "";

                tb_leave_lname.Text = "";
                tb_leave_fname.Text = "";
                tb_leave_mname.Text = "";
                tb_sal_empID.Text = "";
                tb_sal_lname.Text = "";
                tb_sal_fname.Text = "";
                tb_sal_mname.Text = "";
                tb_id_empID.Text = "";
                tb_id_lname.Text = "";
                tb_id_fname.Text = "";
                tb_id_mname.Text = "";
                tb_loan_empID.Text = "";
                tb_loan_lname.Text = "";
                tb_loan_fname.Text = "";
                tb_loan_mname.Text = "";
                tb_lname.Text = "";
                tb_fname.Text = "";
                tb_mname.Text = "";
                tb_email.Text = "";
                cb_pob.SelectedItem = null;
                cb_status.SelectedItem = null;
                tb_cont_no.Text = "";
                tb_comp_email.Text = "";
                tb_telphone_no.Text = "";
                dtp_birthdate.Text = "";
                cb_gender.SelectedItem = null;
                cb_nationality.SelectedItem = null;
                cb_pob.Text = "";
                cb_status.Text = "";
                tb_name_of_spouse.Text = "";
                cb_religion.Text = "";
                tb_course.Text = "";
                rtb_address.Text = "";
                tb_mothers_name.Text = "";
                tb_occupation_mother.Text = "";
                tb_fathers_name.Text = "";
                tb_occupation_father.Text = "";
                tb_elementary.Text = "";
                tb_highSchool.Text = "";
                tb_tertiary.Text = "";
                tb_course.Text = "";
                rtb_work_experience.Text = "";
                tb_sick_leave.Text = "0.00";
                tb_vacation_leave.Text = "0.00";
                cb_payroll_mode.SelectedItem = null;
                cb_payRate.SelectedItem = null;
                cb_paySchedule.SelectedItem = null;
                cb_bank_name2.SelectedItem = null;
                tb_salary.Text = "0.00";
                tb_daily_rate.Text = "0.00";
                tb_daily_hour_rate.Text = "0.00";
                tb_minRate.Text = "0.00";
                tb_riceAllowance.Text = "0.00";
                tb_laundryAllowance.Text = "0.00";
                tb_mealAllowance.Text = "0.00";
                tb_MedicalBenefits.Text = "0.00";
                tb_otherAllowance.Text = "0.00";



                chk_sss.Text = "";
                chk_pag_ibig.Text = "";
                chk_philhealth.Text = "";
                chb_13month_pay.Text = "";
                chb_14month_pay.Text = "";
                chb_15month_pay.Text = "";
                tb_bank_acc.Text = "";
                cb_bank_name.SelectedItem = null;
                tb_bank_acc2.Text = "";
                cb_confidentiality.SelectedItem = null;
                cb_employee_status.SelectedItem = null;
                cb_reason.SelectedItem = null;
                dtp_date_hired.Text = "";
                dtp_dateResign.Text = "";
                tb_designation.Text = "";
                cb_department.SelectedItem = null;
                tb_13th_month_pay.Text = "0.00";
                tb_14th_month_pay.Text = "0.00";
                tb_15th_month_pay.Text = "0.00";
                tb_sss.Text = "0.00";
                tb_pag_ibig.Text = "0.00";
                tb_philhealth.Text = "0.00";

                label115.Text = "0.00";
                label116.Text = "0.00";
                label117.Text = "0.00";
                tb_sss_no.Text = "";
                tb_hdmf_no.Text = "";
                tb_tin_no.Text = "";
                tb_philhealth_no.Text = "";
                tb_comp_loan_amnt.Text = "0.00";
                tb_comp_loan_bal.Text = "0.00";
                tb_comp_loan_month_ded.Text = "0.00";
                tb_sss_loan_amnt.Text = "0.00";
                tb_sss_loan_bal.Text = "0.00";
                tb_sss_loan_month_ded.Text = "0.00";
                tb_pag_ibig_loan_amnt.Text = "0.00";
                tb_pag_ibig_loan_bal.Text = "0.00";
                tb_pag_ibig_loan_month_ded.Text = "0.00";
                tb_name.Text = "";
                tb_primary_con.Text = "";
                tb_relationship.Text = "";
                tb_name2.Text = "";
                tb_second_con.Text = "";
                tb_relationship2.Text = "";
                rtb_homeAddress.Text = "";
                cb_statustype.SelectedItem = "Active";
                cb_statustype.Enabled = false;

                chk_pag_ibig.Checked = false;
                chk_sss.Checked = false;
                chk_philhealth.Checked = false;
                chb_13month_pay.Checked = false;
                chb_14month_pay.Checked = false;
                chb_15month_pay.Checked = false;

                tb_empID.Enabled = false;
                tb_lname.Enabled = false;
                tb_fname.Enabled = false;
                tb_mname.Enabled = false;
                tb_email.Enabled = false;
                cb_pob.Enabled = false;
                cb_status.Enabled = false;
                tb_cont_no.Enabled = false;
                tb_comp_email.Enabled = false;
                tb_telphone_no.Enabled = false;
                dtp_birthdate.Enabled = false;
                cb_gender.Enabled = false;
                cb_nationality.Enabled = false;
                tb_name_of_spouse.Enabled = false;
                cb_religion.Enabled = false;
                rtb_address.Enabled = false;
                tb_mothers_name.Enabled = false;
                tb_occupation_mother.Enabled = false;
                tb_fathers_name.Enabled = false;
                tb_occupation_father.Enabled = false;
                tb_elementary.Enabled = false;
                tb_highSchool.Enabled = false;
                tb_tertiary.Enabled = false;
                tb_course.Enabled = false;
                rtb_work_experience.Enabled = false;
                tb_sick_leave.Enabled = true;
                tb_vacation_leave.Enabled = false;
                cb_payroll_mode.Enabled = false;
                cb_payRate.Enabled = false;
                cb_paySchedule.Enabled = false;
                tb_salary.Enabled = false;
                tb_daily_rate.Enabled = false;
                tb_daily_hour_rate.Enabled = false;

                chk_sss.Enabled = false;
                chk_pag_ibig.Enabled = false;
                chk_philhealth.Enabled = false;
                chb_13month_pay.Enabled = false;
                chb_14month_pay.Enabled = false;
                chb_15month_pay.Enabled = false;
                tb_sick_leave.Enabled = false;

                tb_bank_acc.Enabled = false;
                cb_bank_name.Enabled = false;
                tb_bank_acc2.Enabled = false;
                cb_bank_name2.Enabled = false;


                tb_minRate.Enabled = false;
                cb_confidentiality.Enabled = false;
                cb_employee_status.Enabled = false;
                dtp_date_hired.Enabled = false;
                dtp_dateResign.Enabled = false;
                tb_designation.Enabled = false;
                cb_department.Enabled = false;
                tb_13th_month_pay.Enabled = false;
                tb_14th_month_pay.Enabled = false;
                tb_15th_month_pay.Enabled = false;
                tb_sss.Enabled = false;
                tb_pag_ibig.Enabled = false;
                tb_philhealth.Enabled = false;
                tb_riceAllowance.Enabled = false;
                tb_laundryAllowance.Enabled = false;
                tb_mealAllowance.Enabled = false;
                tb_MedicalBenefits.Enabled = false;
                tb_otherAllowance.Enabled = false;
                cb_reason.Enabled = false;


                tb_sss_no.Enabled = false;
                tb_hdmf_no.Enabled = false;
                tb_tin_no.Enabled = false;
                tb_philhealth_no.Enabled = false;
                tb_comp_loan_amnt.Enabled = false;
                tb_comp_loan_bal.Enabled = false;
                tb_comp_loan_month_ded.Enabled = false;
                tb_sss_loan_amnt.Enabled = false;
                tb_sss_loan_bal.Enabled = false;
                tb_sss_loan_month_ded.Enabled = false;
                tb_pag_ibig_loan_amnt.Enabled = false;
                tb_pag_ibig_loan_bal.Enabled = false;
                tb_pag_ibig_loan_month_ded.Enabled = false;
                tb_name.Enabled = false;
                tb_primary_con.Enabled = false;
                tb_relationship.Enabled = false;
                tb_name2.Enabled = false;
                tb_second_con.Enabled = false;
                tb_relationship2.Enabled = false;
                btn_upload.Enabled = false;
                rtb_homeAddress.Enabled = false;


            }
            public void update_employee()
            {

                string myconnection = ConfigurationManager.ConnectionStrings["MyConstring"].ConnectionString;
                MySqlConnection connection = new MySqlConnection(myconnection);

                MemoryStream ms = new MemoryStream();
                pic_Image.Image.Save(ms, pic_Image.Image.RawFormat);
                byte[] image = ms.ToArray();


                try
                {
                    connection.Open();
                    cmd = new MySqlCommand();
                    cmd.Connection = connection;
                    lv_employee.Columns.Clear();
                    cmd.CommandText = "UPDATE employee SET status = '" + cb_statustype.SelectedItem + "', emp_id= '" + tb_empID.Text + "',  lname = '" + tb_lname.Text + "',  fname = '" + tb_fname.Text + "', mname = '" + tb_mname.Text + "' , email = '" + tb_email.Text + "', pob = '" + cb_pob.Text + "',  status_type = '" + cb_status.Text + "' , contact_no = '" + tb_cont_no.Text + "' , cp_email = '" + tb_comp_email.Text + "' ,  tel_no  = '" + tb_telphone_no.Text + "' , birthdate = '" + dtp_birthdate.Text + "' , gender = '" + cb_gender.Text + "', nationality = '" + cb_nationality.Text + "' , spouse_name = '" + tb_name_of_spouse.Text + "' , religion = '" + cb_religion.Text + "' , address = '" + rtb_address.Text + "' , image =  @img, elementary = '" + tb_elementary.Text + "' , high_school = '" + tb_highSchool.Text + "', college = '" + tb_tertiary.Text + "', course = '" + tb_course.Text + "' , work_experience = '" + rtb_work_experience.Text + "', mothers_name = '" + tb_mothers_name.Text + "', mothers_occ =  '" + tb_occupation_mother.Text + "', fathers_name = '" + tb_fathers_name.Text + "', fathers_occ = '" + tb_occupation_father.Text + "', home_address = '" + rtb_homeAddress.Text + "',  primary_con = '" + tb_name.Text + "', contact_num = '" + tb_primary_con.Text + "', relationship = '" + tb_relationship.Text + "', secondary_con = '" + tb_name2.Text + "', contact_num2 = '" + tb_second_con.Text + "', relationship2 = '" + tb_relationship2.Text + "' WHERE emp_no = '" + lbl_emp_no.Text + "'";
                    cmd.Parameters.Add("@img", MySqlDbType.Blob).Value = image;
                    da = new MySqlDataAdapter();
                    da.SelectCommand = cmd;
                    dt = new DataTable();
                    da.Fill(dt);

                }
                catch (Exception ex)
                {
                    MessageBox.Show(ex.Message);
                }
                finally
                {
                    connection.Close();
                }
            }

            /*public void audit_logs_compare() {

                string myconnection = ConfigurationManager.ConnectionStrings["MyConstring"].ConnectionString;
                MySqlConnection connection = new MySqlConnection(myconnection);

                try
                {
                    connection.Open();
                    cmd = new MySqlCommand();
                    cmd.Connection = connection;
                    cmd.CommandText = "Select * FROM salary left join audit_logs_old_salary on salary.salary_id = audit_logs_old_salary.salary_id where salary.salary_id = '"+label118.Text+"'";
                    da = new MySqlDataAdapter();
                    da.SelectCommand = cmd;
                    dt = new DataTable();
                    da.Fill(dt);

                    for (int i = 0; i < dt.Rows.Count; i++)
                    {
                        DataRow dr = dt.Rows[i];

                        string salary = dr["salary"].ToString();
                        string old_salary = dr["old_salary"].ToString();

                        if (salary != old_salary)
                        {
                            insert_auditlogs(salary, old_salary);
                        }
                    }


                }
                catch (Exception ex)
                {
                    MessageBox.Show(ex.Message);
                }
                finally
                {
                    connection.Close();
                }
            }*/

            private void insert_auditlogs()
            {
                string myconnection = ConfigurationManager.ConnectionStrings["MyConstring"].ConnectionString;
                MySqlConnection connection = new MySqlConnection(myconnection);

                string date_today = DateTime.Now.ToString("M/d/yyyy hh:mm tt");

                string insertQuery = "INSERT INTO audit_logs(user_id,tran_date,transaction)VALUES('" + user_id + "', '" + date_today + "',  '" + "Successfully Update Salary" + "')";


                connection.Open();
                MySqlCommand mySqlCommand = new MySqlCommand(insertQuery, connection);


                try
                {
                    if (mySqlCommand.ExecuteNonQuery() == 1)
                    {
                        //MessageBox.Show("Successfully Add Information", "Add Information", MessageBoxButtons.OK, MessageBoxIcon.Information);
                    }

                }
                catch (Exception ex)
                {
                    MessageBox.Show("Error" + ex);
                }

                connection.Close();
            }

            private void insert_logs_salary()
            {
                string myconnection = ConfigurationManager.ConnectionStrings["MyConstring"].ConnectionString;
                MySqlConnection connection = new MySqlConnection(myconnection);

                string insertQuery = "INSERT INTO audit_logs_old_salary (salary_id,old_salary)SELECT salary_id, salary FROM salary WHERE salary_id = '" + label118.Text + "';";


                connection.Open();
                MySqlCommand mySqlCommand = new MySqlCommand(insertQuery, connection);


                try
                {
                    if (mySqlCommand.ExecuteNonQuery() == 1)
                    {
                        //MessageBox.Show("Successfully Add Information", "Add Information", MessageBoxButtons.OK, MessageBoxIcon.Information);
                    }

                }
                catch (Exception ex)
                {
                    MessageBox.Show("Error" + ex);
                }

                connection.Close();
            }

            public void update_leave()
            {
                string myconnection = ConfigurationManager.ConnectionStrings["MyConstring"].ConnectionString;
                MySqlConnection connection = new MySqlConnection(myconnection);

                try
                {
                    connection.Open();
                    cmd = new MySqlCommand();
                    cmd.Connection = connection;
                    lv_employee.Columns.Clear();
                    cmd.CommandText = "UPDATE leave_balance SET  sick_leave= '" + tb_sick_leave.Text + "',vacation_leave= '" + tb_vacation_leave.Text + "' WHERE leave_balance_id = '" + label31.Text + "'";
                    da = new MySqlDataAdapter();
                    da.SelectCommand = cmd;
                    dt = new DataTable();
                    da.Fill(dt);

                }
                catch (Exception ex)
                {
                    MessageBox.Show(ex.Message);
                }
                finally
                {
                    connection.Close();
                }
            }
            public void update_salary()
            {

                string myconnection = ConfigurationManager.ConnectionStrings["MyConstring"].ConnectionString;
                MySqlConnection connection = new MySqlConnection(myconnection);

                decimal thirteenmonthpay = Convert.ToDecimal(tb_13th_month_pay.Text);
                decimal fourthteenmonthpay = Convert.ToDecimal(tb_14th_month_pay.Text);

                decimal total_bonus = thirteenmonthpay + fourthteenmonthpay;

                decimal non_tax;
                if (total_bonus >= 90000)
                {
                    non_tax = 90000;
                }
                else
                {
                    non_tax = total_bonus;
                }

                decimal salary = Convert.ToDecimal(tb_salary.Text);
                decimal daily_rate = Convert.ToDecimal(tb_daily_rate.Text);
                decimal daily_hour_rate = Convert.ToDecimal(tb_daily_hour_rate.Text);
                decimal daily_min_rate = Convert.ToDecimal(tb_minRate.Text);
                decimal tthmonthpay = Convert.ToDecimal(tb_13th_month_pay.Text);
                decimal ftmonthpay = Convert.ToDecimal(tb_14th_month_pay.Text);
                decimal fthmonthpay = Convert.ToDecimal(tb_15th_month_pay.Text);

                decimal sss = Convert.ToDecimal(tb_sss.Text);
                decimal sss_ee_semi_monthly_cont = sss / 2;
                decimal sss_er = Convert.ToDecimal(label115.Text);
                decimal sss_er_semi_monthly_cont = sss_er / 2;

                decimal pag_ibig = Convert.ToDecimal(tb_pag_ibig.Text);
                decimal pag_ibig_ee_semi_monthly_cont = pag_ibig / 2;
                decimal pag_ibig_er = Convert.ToDecimal(label116.Text);
                decimal pag_ibig_er_semi_monthly_cont = pag_ibig_er / 2;

                decimal philhealth = Convert.ToDecimal(tb_philhealth.Text);
                decimal philhealth_ee_semi_monthly_cont = philhealth / 2;
                decimal philhealth_er = Convert.ToDecimal(label117.Text);
                decimal philhealth_er_semi_monthly_cont = philhealth_er / 2;
                string date_resign = null;

                if (cb_reason.SelectedItem == null)
                {
                    date_resign = "";
                }
                else
                {
                    date_resign = dtp_dateResign.Text;
                }

                try
                {
                    connection.Open();
                    cmd = new MySqlCommand();
                    cmd.Connection = connection;
                    lv_employee.Columns.Clear();
                    cmd.CommandText = "UPDATE salary SET date_hired = '" + dtp_date_hired.Text + "', designation = '" + tb_designation.Text + "',  department = '" + cb_department.Text + "', con_level = '" + cb_confidentiality.Text + "', emp_status = '" + cb_employee_status.Text + "', date_resign = '" + date_resign + "', reason = '" + cb_reason.Text + "', pay_rate = '" + cb_payRate.Text + "', pay_sched = '" + cb_paySchedule.Text + "', salary = '" + salary + "', daily_rate = '" + daily_rate + "', daily_hour_rate = '" + daily_hour_rate + "', min_rate = '" + daily_min_rate + "', rice_allowance = '" + tb_riceAllowance.Text + "', laundry_allowance = '" + tb_laundryAllowance.Text + "', meal_allowance = '" + tb_mealAllowance.Text + "', medical_benefits = '" + tb_MedicalBenefits.Text + "', other_allowance = '" + tb_otherAllowance.Text + "', pay_mode = '" + cb_payroll_mode.Text + "', bank_name = '" + cb_bank_name.Text + "', bank_acc = '" + tb_bank_acc2.Text + "', bank_name2 = '" + cb_bank_name2.Text + "', bank_acc2 = '" + tb_bank_acc2.Text + "', 13thmonthpay = '" + tthmonthpay + "', 14thmonthpay = '" + ftmonthpay + "',15thmonthpay = '" + fthmonthpay + "' , mntbonus = '" + non_tax + "',  sss_ee = '" + sss_ee_semi_monthly_cont + "', sss_ee_monthly = '" + sss + "', sss_er = '" + sss_er_semi_monthly_cont + "', sss_er_monthly = '" + label115.Text + "', pag_ibig_ee = '" + pag_ibig_ee_semi_monthly_cont + "', pag_ibig_ee_monthly = '" + pag_ibig + "', pag_ibig_er = '" + pag_ibig_er_semi_monthly_cont + "', pag_ibig_er_monthly = '" + label116.Text + "', philhealth_ee = '" + philhealth_ee_semi_monthly_cont + "', philhealth_ee_monthly = '" + philhealth + "',philhealth_er = '" + philhealth_er_semi_monthly_cont + "',philhealth_er_monthly = '" + label117.Text + "' WHERE salary_id = '" + label118.Text + "'";
                    da = new MySqlDataAdapter();
                    da.SelectCommand = cmd;
                    dt = new DataTable();
                    da.Fill(dt);

                }
                catch (Exception ex)
                {
                    MessageBox.Show(ex.Message);
                }
                finally
                {
                    connection.Close();
                }
            }
            public void update_other_id()
            {
                string myconnection = ConfigurationManager.ConnectionStrings["MyConstring"].ConnectionString;
                MySqlConnection connection = new MySqlConnection(myconnection);

                try
                {
                    connection.Open();
                    cmd = new MySqlCommand();
                    cmd.Connection = connection;
                    lv_employee.Columns.Clear();
                    cmd.CommandText = "UPDATE others_id SET sss_no = '" + tb_sss_no.Text + "', pag_ibig_no = '" + tb_hdmf_no.Text + "',tin_no  = '" + tb_tin_no.Text + "', philhealth_no = '" + tb_philhealth_no.Text + "'  WHERE other_id = '" + label119.Text + "'";
                    da = new MySqlDataAdapter();
                    da.SelectCommand = cmd;
                    dt = new DataTable();
                    da.Fill(dt);

                }
                catch (Exception ex)
                {
                    MessageBox.Show(ex.Message);
                }
                finally
                {
                    connection.Close();
                }
            }
            public void update_loan()
            {
                string myconnection = ConfigurationManager.ConnectionStrings["MyConstring"].ConnectionString;
                MySqlConnection connection = new MySqlConnection(myconnection);


                decimal comp_loan_amount = Convert.ToDecimal(tb_comp_loan_amnt.Text);
                decimal comp_loan_month_ded = Convert.ToDecimal(tb_comp_loan_month_ded.Text);
                decimal comp_loan_bal = Convert.ToDecimal(tb_comp_loan_bal.Text);

                decimal sss_loan_amount = Convert.ToDecimal(tb_sss_loan_amnt.Text);
                decimal sss_loan_month_ded = Convert.ToDecimal(tb_sss_loan_month_ded.Text);
                decimal sss_loan_bal = Convert.ToDecimal(tb_sss_loan_bal.Text);

                decimal pag_ibig_loan_amount = Convert.ToDecimal(tb_pag_ibig_loan_amnt.Text);
                decimal pag_ibig_loan_month_ded = Convert.ToDecimal(tb_pag_ibig_loan_month_ded.Text);
                decimal pag_big_loan_bal = Convert.ToDecimal(tb_pag_ibig_loan_bal.Text);

                try
                {
                    connection.Open();
                    cmd = new MySqlCommand();
                    cmd.Connection = connection;
                    lv_employee.Columns.Clear();
                    cmd.CommandText = "UPDATE loan SET comp_loan_amnt = '" + comp_loan_amount + "', comp_bal = '" + comp_loan_bal + "', comp_month_ded = '" + comp_loan_month_ded + "',  sss_loan_amnt = '" + sss_loan_amount + "', sss_bal = '" + sss_loan_bal + "',  sss_month_ded = '" + sss_loan_month_ded + "', pag_ibig_loan_amnt = '" + pag_ibig_loan_amount + "', pag_ibig_bal = '" + pag_big_loan_bal + "', pag_ibig_month_ded = '" + pag_ibig_loan_month_ded + "' WHERE loan_id = '" + label120.Text + "'";
                    da = new MySqlDataAdapter();
                    da.SelectCommand = cmd;
                    dt = new DataTable();
                    da.Fill(dt);

                }
                catch (Exception ex)
                {
                    MessageBox.Show(ex.Message);
                }
                finally
                {
                    connection.Close();
                }
            }
            private void chb_13month_pay_CheckedChanged(object sender, EventArgs e)
            {
                if (chb_13month_pay.Checked == true)
                {
                    tb_13th_month_pay.Text = tb_salary.Text;
                    tb_13th_month_pay.Text = string.Format("{0:n}", decimal.Parse(tb_13th_month_pay.Text));
                }
                else
                {
                    tb_13th_month_pay.Text = "0.00";
                }


            }

            private void chb_14month_pay_CheckedChanged(object sender, EventArgs e)
            {
                if (chb_14month_pay.Checked == true)
                {
                    tb_14th_month_pay.Text = tb_salary.Text;
                    tb_14th_month_pay.Text = string.Format("{0:n}", decimal.Parse(tb_14th_month_pay.Text));
                }
                else
                {
                    tb_14th_month_pay.Text = "0.00";
                }


            }

            private void chb_15month_pay_CheckedChanged(object sender, EventArgs e)
            {
                if (chb_15month_pay.Checked == true)
                {
                    tb_15th_month_pay.Text = tb_salary.Text;
                    tb_15th_month_pay.Text = string.Format("{0:n}", decimal.Parse(tb_15th_month_pay.Text));
                }
                else
                {
                    tb_15th_month_pay.Text = "0.00";
                }

            }


            public bool compare_id()
            {

                string myconnection = ConfigurationManager.ConnectionStrings["MyConstring"].ConnectionString;
                MySqlConnection connection = new MySqlConnection(myconnection);

                connection.Open();
                cmd = new MySqlCommand();
                cmd.Connection = connection;
                cmd.CommandText = "Select * from employee where emp_id = '" + tb_empID.Text + "'";
                da = new MySqlDataAdapter();
                da.SelectCommand = cmd;
                dt = new DataTable();
                da.Fill(dt);


                for (int i = 0; i < dt.Rows.Count; i++)
                {

                    DataRow dr = dt.Rows[i];

                    MessageBox.Show("This employee I.D is already assigned.", "Employee Information", MessageBoxButtons.OK, MessageBoxIcon.Error);

                    return false;
                }
                connection.Close();

                return true;
            }

            public bool compare_id2()
            {

                string myconnection = ConfigurationManager.ConnectionStrings["MyConstring"].ConnectionString;
                MySqlConnection connection = new MySqlConnection(myconnection);

                connection.Open();
                cmd = new MySqlCommand();
                cmd.Connection = connection;
                cmd.CommandText = "Select * from employee where emp_id = '" + tb_empID.Text + "'";
                da = new MySqlDataAdapter();
                da.SelectCommand = cmd;
                dt = new DataTable();
                da.Fill(dt);


                for (int i = 0; i < dt.Rows.Count; i++)
                {

                    DataRow dr = dt.Rows[i];

                    string emp_no = dr["emp_no"].ToString();
                    string emp_id = dr["emp_id"].ToString();

                    if (emp_no == lbl_emp_no.Text)
                    {
                        return true;

                    }
                    else if (emp_no == lbl_emp_no.Text || emp_id == tb_empID.Text)
                    {
                        return false;
                    }
                }
                connection.Close();

                return true;
            }


            private void ToExcel()
            {
                StringBuilder sb = new StringBuilder();
                Microsoft.Office.Interop.Excel.Application app = new Microsoft.Office.Interop.Excel.Application();
                app.Visible = true;
                Microsoft.Office.Interop.Excel.Workbook wb = app.Workbooks.Add(1);
                Microsoft.Office.Interop.Excel.Worksheet ws = (Microsoft.Office.Interop.Excel.Worksheet)wb.Worksheets[1];
                int i = 1;
                int i2 = 2;
                int x = 1;
                int x2 = 1;
                foreach (ColumnHeader ch in lv_employee.Columns)
                {
                    ws.Cells[x2, x] = ch.Text;
                    x++;
                }

                foreach (ListViewItem lvi in lv_employee.Items)
                {
                    i = 1;
                    foreach (ListViewItem.ListViewSubItem lvs in lvi.SubItems)
                    {
                        ws.Cells[i2, i] = lvs.Text;
                        ws.Cells.Select();
                        ws.Cells.EntireColumn.AutoFit();
                        i++;
                    }
                    i2++;
                }
            }

            public void load_department()
            {
                string myconnection = ConfigurationManager.ConnectionStrings["MyConstring"].ConnectionString;
                MySqlConnection connection = new MySqlConnection(myconnection);

                try
                {
                    connection.Open();
                    cmd = new MySqlCommand();
                    cmd.Connection = connection;
                    cmd.CommandText = "Select * FROM department";
                    da = new MySqlDataAdapter();
                    da.SelectCommand = cmd;
                    dt = new DataTable();
                    da.Fill(dt);

                    for (int i = 0; i < dt.Rows.Count; i++)
                    {
                        DataRow dr = dt.Rows[i];

                        cb_department.Items.Add((dr["dep_desc"].ToString()));
                    }

                }
                catch (Exception ex)
                {
                    MessageBox.Show(ex.Message);
                }
                {

                    connection.Close();
                }
            }


            public void load_comp_details()
            {


                string myconnection = ConfigurationManager.ConnectionStrings["MyConstring"].ConnectionString;
                MySqlConnection connection = new MySqlConnection(myconnection);

                try
                {
                    connection.Open();
                    cmd = new MySqlCommand();
                    cmd.Connection = connection;
                    cmd.CommandText = "Select * FROM comp_details";
                    da = new MySqlDataAdapter();
                    da.SelectCommand = cmd;
                    dt = new DataTable();
                    da.Fill(dt);

                    for (int i = 0; i < dt.Rows.Count; i++)
                    {
                        DataRow dr = dt.Rows[i];

                        bool monthly = (bool)dr["monthly"];
                        bool semi_monthly = (bool)dr["semi_monthly"];
                        bool weekly = (bool)dr["weekly"];

                        if (monthly == true)
                        {
                            cb_paySchedule.Items.Add("Monthly");
                        }

                        if (semi_monthly == true)
                        {
                            cb_paySchedule.Items.Add("Semi-Monthly");
                        }

                        if (weekly == true)
                        {
                            cb_paySchedule.Items.Add("Weekly");
                        }

                    }

                }
                catch (Exception ex)
                {
                    MessageBox.Show(ex.Message);
                }
                {

                    connection.Close();
                }
            }

            public void load_religion(String sql1)
            {
                string myconnection = ConfigurationManager.ConnectionStrings["MyConstring"].ConnectionString;
                MySqlConnection connection = new MySqlConnection(myconnection);

                try
                {
                    connection.Open();
                    cmd = new MySqlCommand();
                    cmd.Connection = connection;
                    cmd.CommandText = sql1;
                    da = new MySqlDataAdapter();
                    da.SelectCommand = cmd;
                    dt = new DataTable();
                    da.Fill(dt);

                    for (int i = 0; i < dt.Rows.Count; i++)
                    {
                        DataRow dr = dt.Rows[i];

                        cb_religion.Items.Add((dr["religion_name"].ToString()));


                    }

                }
                catch (Exception ex)
                {
                    MessageBox.Show(ex.Message);
                }
                {
                    connection.Close();
                }
            }

            private void button1_Click(object sender, EventArgs e)
            {
                tc_emp_info.SelectedIndex = 0;
            }

            private void tb_other_ded_KeyPress(object sender, KeyPressEventArgs e)
            {
                if (((e.KeyChar < 48 || e.KeyChar > 57) && e.KeyChar != 8 && e.KeyChar != 46))
                {
                    e.Handled = true;
                    return;
                }


                if (e.KeyChar == 46)
                {
                    if ((sender as TextBox).Text.IndexOf(e.KeyChar) != -1)
                        e.Handled = true;
                }
            }

            private void tb_other_inc_KeyPress(object sender, KeyPressEventArgs e)
            {
                if (((e.KeyChar < 48 || e.KeyChar > 57) && e.KeyChar != 8 && e.KeyChar != 46))
                {
                    e.Handled = true;
                    return;
                }


                if (e.KeyChar == 46)
                {
                    if ((sender as TextBox).Text.IndexOf(e.KeyChar) != -1)
                        e.Handled = true;
                }
            }


            private void tb_salary_TextChanged(object sender, EventArgs e)
            {

                compute_daily_rate();
                compute_daily_hour_rate();
                compute_min_rate();
            }

            private void compute_min_rate()
            {
                if (tb_minRate.Text == "")
                {
                    tb_minRate.Text = "0.00";
                }

                decimal x = Convert.ToDecimal(tb_daily_hour_rate.Text);
                decimal total_min_rate = x / 60;
                decimal y = Math.Round(total_min_rate, 2, MidpointRounding.AwayFromZero);
                tb_minRate.Text = y + "";
                tb_minRate.Text = string.Format("{0:n}", decimal.Parse(tb_minRate.Text));
            }



            private void tb_wtax_KeyPress(object sender, KeyPressEventArgs e)
            {
                if ((e.KeyChar < 32) || (e.KeyChar >= '0') && (e.KeyChar <= '9') || (e.KeyChar == ',') || (e.KeyChar == '.'))
                    return;

                // only evident errors (like 'A' or '&') are restricted
                e.Handled = true;
            }

            private void tb_daily_rate_TextChanged(object sender, EventArgs e)
            {
                if (tb_daily_rate.Text == "")
                {
                    tb_daily_rate.Text = "0.00";
                }
            }

            private void tb_daily_hour_rate_TextChanged(object sender, EventArgs e)
            {
                if (tb_daily_hour_rate.Text == "")
                {
                    tb_daily_hour_rate.Text = "0.00";
                }
            }

            private void tb_13th_month_pay_TextChanged(object sender, EventArgs e)
            {
                if (tb_13th_month_pay.Text == "")
                {
                    tb_13th_month_pay.Text = "0.00";
                }
            }

            private void tb_14th_month_pay_TextChanged(object sender, EventArgs e)
            {
                if (tb_14th_month_pay.Text == "")
                {
                    tb_14th_month_pay.Text = "0.00";
                }
            }

            private void tb_15th_month_pay_TextChanged(object sender, EventArgs e)
            {
                if (tb_15th_month_pay.Text == "")
                {
                    tb_15th_month_pay.Text = "0.00";
                }
            }

            private void tb_sss_TextChanged(object sender, EventArgs e)
            {
                if (tb_sss.Text == "")
                {
                    tb_sss.Text = "0.00";
                }
            }

            private void tb_pag_ibig_TextChanged(object sender, EventArgs e)
            {
                if (tb_pag_ibig.Text == "")
                {
                    tb_pag_ibig.Text = "0.00";
                }
            }

            private void tb_philhealth_TextChanged(object sender, EventArgs e)
            {
                if (tb_philhealth.Text == "")
                {
                    tb_philhealth.Text = "0.00";
                }
            }

            private void tb_comp_loan_amount_TextChanged(object sender, EventArgs e)
            {
                if (tb_comp_loan_amnt.Text == "")
                {
                    tb_comp_loan_amnt.Text = "0.00";
                }
            }

            private void tb_comp_loan_bal_TextChanged(object sender, EventArgs e)
            {
                if (tb_comp_loan_bal.Text == "")
                {
                    tb_comp_loan_bal.Text = "0.00";
                }
            }

            private void tb_comp_loan_month_ded_TextChanged(object sender, EventArgs e)
            {
                if (tb_comp_loan_month_ded.Text == "")
                {
                    tb_comp_loan_month_ded.Text = "0.00";
                }
            }

            private void tb_sss_loan_amount_TextChanged(object sender, EventArgs e)
            {
                if (tb_sss_loan_amnt.Text == "")
                {
                    tb_sss_loan_amnt.Text = "0.00";
                }
            }

            private void tb_sss_loan_bal_TextChanged(object sender, EventArgs e)
            {
                if (tb_sss_loan_bal.Text == "")
                {
                    tb_sss_loan_bal.Text = "0.00";
                }
            }

            private void tb_sss_loan_month_ded_TextChanged(object sender, EventArgs e)
            {
                if (tb_sss_loan_month_ded.Text == "")
                {
                    tb_sss_loan_month_ded.Text = "0.00";

                }
            }

            private void tb_pag_ibig_loan_amnt_TextChanged(object sender, EventArgs e)
            {
                if (tb_pag_ibig_loan_amnt.Text == "")
                {
                    tb_pag_ibig_loan_amnt.Text = "0.00";

                }
            }

            private void tb_pag_ibig_loan_bal_TextChanged(object sender, EventArgs e)
            {
                if (tb_pag_ibig_loan_bal.Text == "")
                {
                    tb_pag_ibig_loan_bal.Text = "0.00";

                }
            }

            private void tb_pag_ibig_loan_month_ded_TextChanged(object sender, EventArgs e)
            {
                if (tb_pag_ibig_loan_month_ded.Text == "")
                {
                    tb_pag_ibig_loan_month_ded.Text = "0.00";

                }
            }

            private void tb_sick_leave_TextChanged(object sender, EventArgs e)
            {
                if (tb_sick_leave.Text == "")
                {
                    tb_sick_leave.Text = "0.00";

                }
            }

            private void tb_vacation_leave_TextChanged(object sender, EventArgs e)
            {
                if (tb_vacation_leave.Text == "")
                {
                    tb_vacation_leave.Text = "0.00";

                }
            }


            private void tb_telephone_no_KeyPress(object sender, KeyPressEventArgs e)
            {
                e.Handled = !char.IsDigit(e.KeyChar) && !char.IsControl(e.KeyChar);
            }

            public class RoundButton : Button
            {
                protected override void OnPaint(System.Windows.Forms.PaintEventArgs e)
                {
                    GraphicsPath grPath = new GraphicsPath();
                    grPath.AddEllipse(0, 0, ClientSize.Width, ClientSize.Height);
                    this.Region = new System.Drawing.Region(grPath);
                    base.OnPaint(e);
                }
            }

        private void btn_add_employee_Click(object sender, EventArgs e)
        {
            if (btn_add.Text == "ADD")
            {
                TabPage t = tc_emp_info.TabPages[0];
                tc_emp_info.SelectTab(t); //go to tab

                cb_statustype.SelectedItem = "Active";
                btn_add.Image = Properties.Resources.Button_SAVE_Red__1_;
                cb_gender.SelectedItem = "Male";
                btn_add.Text = "SAVE";
                tb_empID.Enabled = true;
                tb_lname.Enabled = true;
                tb_fname.Enabled = true;
                tb_mname.Enabled = true;
                tb_email.Enabled = true;
                cb_pob.Enabled = true;
                cb_status.Enabled = true;
                tb_cont_no.Enabled = true;
                tb_comp_email.Enabled = true;
                tb_telphone_no.Enabled = true;
                dtp_birthdate.Enabled = true;
                cb_gender.Enabled = true;
                cb_nationality.Enabled = true;
                tb_name_of_spouse.Enabled = true;
                cb_religion.Enabled = true;
                cb_statustype.Enabled = true;
                rtb_address.Enabled = true;
                tb_mothers_name.Enabled = true;
                tb_occupation_mother.Enabled = true;
                tb_fathers_name.Enabled = true;
                tb_occupation_father.Enabled = true;
                tb_elementary.Enabled = true;
                tb_highSchool.Enabled = true;
                tb_tertiary.Enabled = true;
                rtb_work_experience.Enabled = true;
                tb_sick_leave.Enabled = true;
                tb_vacation_leave.Enabled = true;
                cb_payroll_mode.Enabled = true;
                cb_payRate.Enabled = true;
                tb_salary.Enabled = true;
                tb_daily_rate.Enabled = true;
                tb_daily_hour_rate.Enabled = true;
                chk_sss.Enabled = true;
                chk_pag_ibig.Enabled = true;
                chk_philhealth.Enabled = true;
                chb_13month_pay.Enabled = true;
                chb_14month_pay.Enabled = true;
                chb_15month_pay.Enabled = true;
                tb_course.Enabled = true;
                cb_confidentiality.Enabled = true;
                cb_employee_status.Enabled = true;
                dtp_date_hired.Enabled = true;
                tb_designation.Enabled = true;
                cb_department.Enabled = true;
                tb_13th_month_pay.Enabled = true;
                tb_14th_month_pay.Enabled = true;
                tb_15th_month_pay.Enabled = true;
                tb_sss.Enabled = true;
                tb_pag_ibig.Enabled = true;
                tb_philhealth.Enabled = true;
                tb_sss_no.Enabled = true;
                tb_hdmf_no.Enabled = true;
                cb_paySchedule.Enabled = true;
                tb_riceAllowance.Enabled = true;
                tb_laundryAllowance.Enabled = true;
                tb_mealAllowance.Enabled = true;
                tb_MedicalBenefits.Enabled = true;
                tb_otherAllowance.Enabled = true;
                tb_minRate.Enabled = true;

                tb_tin_no.Enabled = true;
                tb_philhealth_no.Enabled = true;
                tb_comp_loan_amnt.Enabled = true;
                tb_comp_loan_bal.Enabled = true;
                tb_comp_loan_month_ded.Enabled = true;
                tb_sss_loan_amnt.Enabled = true;
                tb_sss_loan_bal.Enabled = true;
                tb_sss_loan_month_ded.Enabled = true;
                tb_pag_ibig_loan_amnt.Enabled = true;
                tb_pag_ibig_loan_bal.Enabled = true;
                tb_pag_ibig_loan_month_ded.Enabled = true;
                tb_name.Enabled = true;
                tb_primary_con.Enabled = true;
                tb_relationship.Enabled = true;
                tb_name2.Enabled = true;
                tb_second_con.Enabled = true;
                tb_relationship2.Enabled = true;
                btn_upload.Enabled = true;
                rtb_homeAddress.Enabled = true;

            }

            else if (lbl_emp_no.Text != "0" || label83.Text != "0" || label79.Text != "0" || label89.Text != "0" || label1.Text != "0" || label88.Text != "0")
            {
                if (lbl_category.Text == "General Information" || lbl_category.Text == "Other Information")
                {
                    if (tb_empID.Text == "" || tb_lname.Text == "" || tb_fname.Text == "" || tb_mname.Text == "" || cb_status.Text == "" || tb_comp_email.Text == "")
                    {
                        MessageBox.Show("Please Fill Up All Information Need", "Warning", MessageBoxButtons.OK, MessageBoxIcon.Error);
                    }
                    else
                    {
                        bool employeeid = compare_id2();

                        if (employeeid == true)
                        {
                            DialogResult dialogResult = MessageBox.Show("Are you sure want to update Employee?", "Employee Information", MessageBoxButtons.YesNo, MessageBoxIcon.Question);
                            if (dialogResult == DialogResult.Yes)
                            {

                                update_employee();
                                update_salary();
                                update_leave();
                                update_other_id();
                                update_loan();
                                validate_religion();


                                lv_employee.Columns.Clear();
                                Personal_Information("Select * from employee");
                                MessageBox.Show("Successfully Update Information", "Update Information", MessageBoxButtons.OK, MessageBoxIcon.Information);
                                disable_all();
                            }
                        }
                        else
                        {
                            MessageBox.Show("This employee I.D is already assigned.", "Employee Information", MessageBoxButtons.OK, MessageBoxIcon.Error);
                        }

                    }

                }

                else if (lbl_category.Text == "Leave")
                {
                    DialogResult dialogResult = MessageBox.Show("Are you sure want to update Employee Leave Balance?", "Employee Information", MessageBoxButtons.YesNo, MessageBoxIcon.Question);
                    if (dialogResult == DialogResult.Yes)
                    {
                        string emp_no = label79.Text;
                        emp_details(emp_no);

                        update_employee();
                        update_salary();
                        update_leave();
                        update_other_id();
                        update_loan();
                        select_religion();

                        lv_employee.Columns.Clear();
                        leave_balanced("SELECT * FROM leave_balance LEFT JOIN employee ON leave_balance.emp_no = employee.emp_no");
                        MessageBox.Show("Successfully Update Information", "Leave Information", MessageBoxButtons.OK, MessageBoxIcon.Information);
                        disable_all();
                    }



                }
                else if (lbl_category.Text == "Salary")
                {
                    if (dtp_date_hired.Text == "" || tb_designation.Text == "" || cb_department.Text == "" || cb_employee_status.Text == "" || cb_payRate.Text == "" || cb_paySchedule.Text == "" || tb_salary.Text == "0.00" || cb_payroll_mode.Text == "")
                    {
                        MessageBox.Show("Please Fill Up All Information Need", "Warning", MessageBoxButtons.OK, MessageBoxIcon.Error);
                    }
                    else
                    {
                        DialogResult dialogResult = MessageBox.Show("Are you sure want to update Employee Salary?", "Employee Salary Information", MessageBoxButtons.YesNo, MessageBoxIcon.Question);

                        if (dialogResult == DialogResult.Yes)
                        {
                            string emp_no = label83.Text;
                            emp_details(emp_no);

                            update_employee();
                            insert_logs_salary();
                            update_salary();
                            insert_auditlogs();
                            update_leave();
                            update_other_id();
                            update_loan();
                            select_religion();

                            lv_employee.Columns.Clear();
                            Salary("SELECT * FROM salary LEFT JOIN employee ON salary.emp_no = employee.emp_no");
                            MessageBox.Show("Successfully Update Information", "Salary Information", MessageBoxButtons.OK, MessageBoxIcon.Information);
                            disable_all();
                        }

                    }

                }

                else if (lbl_category.Text == "Other ID")
                {
                    DialogResult dialogResult = MessageBox.Show("Are you sure want to update Employee I.D?", "Employee Information", MessageBoxButtons.YesNo, MessageBoxIcon.Question);
                    if (dialogResult == DialogResult.Yes)
                    {
                        string emp_no = label88.Text;
                        emp_details(emp_no);

                        update_employee();
                        update_salary();
                        update_leave();
                        update_other_id();
                        update_loan();
                        select_religion();

                        lv_employee.Columns.Clear();
                        ID("Select * FROM others_id LEFT JOIN employee ON others_id.emp_no = employee.emp_no");
                        MessageBox.Show("Successfully Update Information", "I.D Information", MessageBoxButtons.OK, MessageBoxIcon.Information);
                        disable_all();
                    }

                }
                else if (lbl_category.Text == "Loan")
                {
                    DialogResult dialogResult = MessageBox.Show("Are you sure want to update Employee Loan?", "Employee Information", MessageBoxButtons.YesNo, MessageBoxIcon.Question);
                    if (dialogResult == DialogResult.Yes)
                    {
                        string emp_no = label89.Text;
                        emp_details(emp_no);

                        update_employee();
                        update_salary();
                        update_leave();
                        update_other_id();
                        update_loan();
                        select_religion();

                        lv_employee.Columns.Clear();
                        Loan("SELECT * FROM loan LEFT JOIN employee ON loan.emp_no = employee.emp_no");
                        MessageBox.Show("Successfully Update Information", "Loan Information", MessageBoxButtons.OK, MessageBoxIcon.Information);
                        disable_all();

                    }
                }               
            }
        
            else
            {

                bool employeeid = compare_id();

                if (employeeid == true)
                {
                    if (tb_empID.Text == "" || tb_lname.Text == "" || tb_fname.Text == "" || tb_mname.Text == "" || cb_status.Text == "" || tb_comp_email.Text == "" || dtp_date_hired.Text == "" || tb_designation.Text == "" || cb_department.Text == "" || cb_employee_status.Text == "" || cb_payRate.Text == "" || cb_paySchedule.Text == "" || tb_salary.Text == "0.00" || cb_payroll_mode.Text == "")
                    {
                        MessageBox.Show("Please Fill Up All Information Need", "Warning", MessageBoxButtons.OK, MessageBoxIcon.Error);
                    }
                    else
                    {

                        DialogResult dialogResult = MessageBox.Show("Are you sure want to Add Employee '" + tb_empID.Text + "'?", "Add Information", MessageBoxButtons.YesNo, MessageBoxIcon.Question);
                        if (dialogResult == DialogResult.Yes)
                        {
                            MessageBox.Show("Employee Successfully Save.", " Add Employee Information", MessageBoxButtons.OK, MessageBoxIcon.Information);
                            insert_personal_information();
                            add_leave_balanced();
                            insert_salary();
                            insert_id();
                            insert_loan();
                            validate_religion();
                            EmployeeID();
                            lv_employee.Columns.Clear();

                            if (lbl_category.Text == "General Information" || lbl_category.Text == "Other Information")
                            {
                                Personal_Information("Select * from employee");
                            }
                            else if (lbl_category.Text == "Leave")
                            {
                                leave_balanced("SELECT * FROM leave_balance LEFT JOIN employee ON leave_balance.emp_no = employee.emp_no");
                            }
                            else if (lbl_category.Text == "Salary")
                            {
                                Salary("SELECT * FROM salary LEFT JOIN employee ON salary.emp_no = employee.emp_no");
                            }
                            else if (lbl_category.Text == "Other ID")
                            {
                                ID("Select * FROM others_id LEFT JOIN employee ON others_id.emp_no = employee.emp_no");
                            }
                            else if (lbl_category.Text == "Loan")
                            {
                                Loan("SELECT * FROM loan LEFT JOIN employee ON loan.emp_no = employee.emp_no");
                            }

                            disable_all();
                        }

                    }
                }
            }

        }

            private void validate_religion()
            {
                if (cb_religion.Text != "")
                {
                    bool religion = select_religion();

                    if (religion == false)
                    {
                        insert_religion();
                    }
                }

            }

            private void btn_update_Click(object sender, EventArgs e)
            {
                if (btn_update.Text == "EDIT")
                {

                    btn_add.Text = "SAVE";
                    btn_update.Enabled = false;
                    btn_add.Enabled = true;
                    btn_add.Image = Properties.Resources.Button_SAVE_Red__1_;

                //Employee
                //btn_update.Image = Properties.Resources.Button_SAVE_Red__1_;
                //btn_update.Text = "SAVE";
                cb_statustype.Enabled = true;
                    tb_empID.Enabled = true;
                    tb_lname.Enabled = true;
                    tb_fname.Enabled = true;
                    tb_mname.Enabled = true;
                    tb_email.Enabled = true;
                    cb_pob.Enabled = true;
                    cb_status.Enabled = true;
                    tb_cont_no.Enabled = true;
                    tb_comp_email.Enabled = true;
                    tb_telphone_no.Enabled = true;
                    dtp_birthdate.Enabled = true;
                    cb_gender.Enabled = true;
                    cb_nationality.Enabled = true;
                    tb_name_of_spouse.Enabled = true;
                    cb_religion.Enabled = true;
                    rtb_address.Enabled = true;
                    tb_mothers_name.Enabled = true;
                    tb_occupation_mother.Enabled = true;
                    tb_fathers_name.Enabled = true;
                    tb_occupation_father.Enabled = true;
                    tb_elementary.Enabled = true;
                    tb_highSchool.Enabled = true;
                    tb_tertiary.Enabled = true;
                    rtb_work_experience.Enabled = true;
                    tb_sick_leave.Enabled = true;
                    tb_vacation_leave.Enabled = true;

                    //Salary
                    cb_payroll_mode.Enabled = true;
                    cb_payRate.Enabled = true;
                    tb_salary.Enabled = true;
                    tb_minRate.Enabled = true;
                    tb_daily_rate.Enabled = true;
                    tb_daily_hour_rate.Enabled = true;
                    cb_reason.Enabled = true;
                    dtp_dateResign.Enabled = true;
                    tb_riceAllowance.Enabled = true;
                    cb_paySchedule.Enabled = true;
                    tb_laundryAllowance.Enabled = true;
                    tb_mealAllowance.Enabled = true;
                    tb_MedicalBenefits.Enabled = true;
                    tb_otherAllowance.Enabled = true;

                    if (cb_payroll_mode.SelectedIndex == 2)
                    {
                        cb_bank_name.Enabled = false;
                        tb_bank_acc.Enabled = false;
                        cb_bank_name2.Enabled = false;
                        tb_bank_acc2.Enabled = false;
                    }
                    else
                    {
                        cb_bank_name.Enabled = true;
                        tb_bank_acc.Enabled = true;
                        cb_bank_name2.Enabled = true;
                        tb_bank_acc2.Enabled = true;
                    }

                    chk_sss.Enabled = true;
                    chk_pag_ibig.Enabled = true;
                    chk_philhealth.Enabled = true;
                    chb_13month_pay.Enabled = true;
                    chb_14month_pay.Enabled = true;
                    chb_15month_pay.Enabled = true;


                    cb_confidentiality.Enabled = true;
                    cb_employee_status.Enabled = true;
                    dtp_date_hired.Enabled = true;
                    tb_designation.Enabled = true;
                    cb_department.Enabled = true;
                    tb_13th_month_pay.Enabled = true;
                    tb_14th_month_pay.Enabled = true;
                    tb_15th_month_pay.Enabled = true;
                    tb_sss.Enabled = true;
                    tb_pag_ibig.Enabled = true;
                    tb_philhealth.Enabled = true;

                    tb_sss_no.Enabled = true;
                    tb_hdmf_no.Enabled = true;
                    tb_tin_no.Enabled = true;
                    tb_philhealth_no.Enabled = true;
                    tb_comp_loan_amnt.Enabled = true;
                    tb_comp_loan_bal.Enabled = true;
                    tb_comp_loan_month_ded.Enabled = true;
                    tb_sss_loan_amnt.Enabled = true;
                    tb_sss_loan_bal.Enabled = true;
                    tb_sss_loan_month_ded.Enabled = true;
                    tb_pag_ibig_loan_amnt.Enabled = true;
                    tb_pag_ibig_loan_bal.Enabled = true;
                    tb_pag_ibig_loan_month_ded.Enabled = true;
                    tb_name.Enabled = true;
                    tb_primary_con.Enabled = true;
                    tb_relationship.Enabled = true;
                    tb_name2.Enabled = true;
                    tb_second_con.Enabled = true;
                    tb_relationship2.Enabled = true;
                    btn_upload.Enabled = true;
                    rtb_homeAddress.Enabled = true;

                    tb_course.Enabled = true;
                }
             
            }


            private void btn_clear_Click(object sender, EventArgs e)
            {
                /*disable_all();

                TabPage t = tc_emp_info.TabPages[0];
                tc_emp_info.SelectTab(t); //go to tab*/

            }

            private void lv_employee_Click(object sender, EventArgs e)
            {

                disable_all();

                btn_update.Text = "EDIT";
                btn_add.Text = "ADD";
                btn_add.Enabled = false;
                btn_update.Enabled = true;
                btn_Delete.Enabled = true;
             

                if (lbl_category.Text == "General Information" || lbl_category.Text == "Other Information")
                {

                    label79.Text = lv_employee.SelectedItems[0].SubItems[0].Text;
                    label83.Text = lv_employee.SelectedItems[0].SubItems[0].Text;
                    label88.Text = lv_employee.SelectedItems[0].SubItems[0].Text;
                    label89.Text = lv_employee.SelectedItems[0].SubItems[0].Text;

                    lbl_emp_no.Text = lv_employee.SelectedItems[0].SubItems[0].Text;

                    label1.Text = lv_employee.SelectedItems[0].SubItems[0].Text;

                    tb_info_empID.Text = lv_employee.SelectedItems[0].SubItems[2].Text;
                    tb_info_lname.Text = lv_employee.SelectedItems[0].SubItems[3].Text;
                    tb_info_fname.Text = lv_employee.SelectedItems[0].SubItems[4].Text;
                    tb_info_mname.Text = lv_employee.SelectedItems[0].SubItems[5].Text;

                    cb_statustype.Text = lv_employee.SelectedItems[0].SubItems[1].Text;
                    tb_empID.Text = lv_employee.SelectedItems[0].SubItems[2].Text;
                    tb_lname.Text = lv_employee.SelectedItems[0].SubItems[3].Text;
                    tb_fname.Text = lv_employee.SelectedItems[0].SubItems[4].Text;
                    tb_mname.Text = lv_employee.SelectedItems[0].SubItems[5].Text;

                    tb_email.Text = lv_employee.SelectedItems[0].SubItems[6].Text;
                    cb_pob.Text = lv_employee.SelectedItems[0].SubItems[7].Text;
                    cb_status.Text = lv_employee.SelectedItems[0].SubItems[8].Text;
                    tb_cont_no.Text = lv_employee.SelectedItems[0].SubItems[9].Text;
                    tb_comp_email.Text = lv_employee.SelectedItems[0].SubItems[10].Text;
                    tb_telphone_no.Text = lv_employee.SelectedItems[0].SubItems[11].Text;
                    dtp_birthdate.Text = lv_employee.SelectedItems[0].SubItems[12].Text;
                    cb_gender.Text = lv_employee.SelectedItems[0].SubItems[13].Text;
                    cb_nationality.Text = lv_employee.SelectedItems[0].SubItems[14].Text;
                    tb_name_of_spouse.Text = lv_employee.SelectedItems[0].SubItems[15].Text;
                    cb_religion.Text = lv_employee.SelectedItems[0].SubItems[16].Text;
                    rtb_address.Text = lv_employee.SelectedItems[0].SubItems[17].Text;
                    readPhotoEmployee();
                    tb_elementary.Text = lv_employee.SelectedItems[0].SubItems[19].Text;
                    tb_highSchool.Text = lv_employee.SelectedItems[0].SubItems[20].Text;
                    tb_tertiary.Text = lv_employee.SelectedItems[0].SubItems[21].Text;
                    tb_course.Text = lv_employee.SelectedItems[0].SubItems[22].Text;
                    rtb_work_experience.Text = lv_employee.SelectedItems[0].SubItems[23].Text;
                    tb_mothers_name.Text = lv_employee.SelectedItems[0].SubItems[24].Text;
                    tb_occupation_mother.Text = lv_employee.SelectedItems[0].SubItems[25].Text;
                    tb_fathers_name.Text = lv_employee.SelectedItems[0].SubItems[26].Text;
                    tb_occupation_father.Text = lv_employee.SelectedItems[0].SubItems[27].Text;
                    rtb_homeAddress.Text = lv_employee.SelectedItems[0].SubItems[28].Text;
                    tb_name.Text = lv_employee.SelectedItems[0].SubItems[29].Text;
                    tb_primary_con.Text = lv_employee.SelectedItems[0].SubItems[30].Text;
                    tb_relationship.Text = lv_employee.SelectedItems[0].SubItems[31].Text;
                    tb_name2.Text = lv_employee.SelectedItems[0].SubItems[32].Text;
                    tb_second_con.Text = lv_employee.SelectedItems[0].SubItems[33].Text;
                    tb_relationship2.Text = lv_employee.SelectedItems[0].SubItems[34].Text;

                }
                else if (lbl_category.Text == "Leave")
                {

                    label31.Text = lv_employee.SelectedItems[0].SubItems[0].Text;
                    label79.Text = lv_employee.SelectedItems[0].SubItems[1].Text;
                    tb_leave_empID.Text = lv_employee.SelectedItems[0].SubItems[2].Text;
                    tb_leave_lname.Text = lv_employee.SelectedItems[0].SubItems[3].Text;
                    tb_leave_fname.Text = lv_employee.SelectedItems[0].SubItems[4].Text;
                    tb_leave_mname.Text = lv_employee.SelectedItems[0].SubItems[5].Text;
                    tb_sick_leave.Text = lv_employee.SelectedItems[0].SubItems[6].Text;
                    tb_vacation_leave.Text = lv_employee.SelectedItems[0].SubItems[7].Text;
                }
                else if (lbl_category.Text == "Salary")
                {

                    label118.Text = lv_employee.SelectedItems[0].SubItems[0].Text;
                    label83.Text = lv_employee.SelectedItems[0].SubItems[1].Text;

                    tb_sal_empID.Text = lv_employee.SelectedItems[0].SubItems[2].Text;
                    tb_sal_lname.Text = lv_employee.SelectedItems[0].SubItems[3].Text;
                    tb_sal_fname.Text = lv_employee.SelectedItems[0].SubItems[4].Text;
                    tb_sal_mname.Text = lv_employee.SelectedItems[0].SubItems[5].Text;
                    dtp_date_hired.Text = lv_employee.SelectedItems[0].SubItems[6].Text;
                    tb_designation.Text = lv_employee.SelectedItems[0].SubItems[7].Text;
                    cb_department.Text = lv_employee.SelectedItems[0].SubItems[8].Text;
                    cb_confidentiality.Text = lv_employee.SelectedItems[0].SubItems[9].Text;
                    cb_employee_status.Text = lv_employee.SelectedItems[0].SubItems[10].Text;
                    dtp_dateResign.CustomFormat = lv_employee.SelectedItems[0].SubItems[11].Text;
                    cb_reason.Text = lv_employee.SelectedItems[0].SubItems[12].Text;
                    cb_payRate.Text = lv_employee.SelectedItems[0].SubItems[13].Text;
                    cb_paySchedule.Text = lv_employee.SelectedItems[0].SubItems[14].Text;
                    tb_salary.Text = lv_employee.SelectedItems[0].SubItems[15].Text;
                    tb_daily_rate.Text = lv_employee.SelectedItems[0].SubItems[16].Text;
                    tb_daily_hour_rate.Text = lv_employee.SelectedItems[0].SubItems[17].Text;
                    tb_minRate.Text = lv_employee.SelectedItems[0].SubItems[18].Text;
                    tb_riceAllowance.Text = lv_employee.SelectedItems[0].SubItems[19].Text;
                    tb_laundryAllowance.Text = lv_employee.SelectedItems[0].SubItems[20].Text;
                    tb_mealAllowance.Text = lv_employee.SelectedItems[0].SubItems[21].Text;
                    tb_MedicalBenefits.Text = lv_employee.SelectedItems[0].SubItems[22].Text;
                    tb_otherAllowance.Text = lv_employee.SelectedItems[0].SubItems[23].Text;
                    cb_payroll_mode.Text = lv_employee.SelectedItems[0].SubItems[24].Text;
                    cb_bank_name.Text = lv_employee.SelectedItems[0].SubItems[25].Text;
                    tb_bank_acc.Text = lv_employee.SelectedItems[0].SubItems[26].Text;
                    cb_bank_name2.Text = lv_employee.SelectedItems[0].SubItems[27].Text;
                    tb_bank_acc2.Text = lv_employee.SelectedItems[0].SubItems[28].Text;
                    tb_13th_month_pay.Text = lv_employee.SelectedItems[0].SubItems[29].Text;
                    tb_14th_month_pay.Text = lv_employee.SelectedItems[0].SubItems[30].Text;
                    tb_15th_month_pay.Text = lv_employee.SelectedItems[0].SubItems[31].Text;
                    tb_sss.Text = lv_employee.SelectedItems[0].SubItems[32].Text;
                    label115.Text = lv_employee.SelectedItems[0].SubItems[33].Text;
                    tb_pag_ibig.Text = lv_employee.SelectedItems[0].SubItems[34].Text;
                    label116.Text = lv_employee.SelectedItems[0].SubItems[35].Text;
                    tb_philhealth.Text = lv_employee.SelectedItems[0].SubItems[36].Text;
                    label117.Text = lv_employee.SelectedItems[0].SubItems[37].Text;
                    

                    //check_benefits();

                }
                else if (lbl_category.Text == "Other ID")
                {

                    label119.Text = lv_employee.SelectedItems[0].SubItems[0].Text;
                    label88.Text = lv_employee.SelectedItems[0].SubItems[1].Text;
                    tb_id_empID.Text = lv_employee.SelectedItems[0].SubItems[2].Text;
                    tb_id_lname.Text = lv_employee.SelectedItems[0].SubItems[3].Text;
                    tb_id_fname.Text = lv_employee.SelectedItems[0].SubItems[4].Text;
                    tb_id_mname.Text = lv_employee.SelectedItems[0].SubItems[5].Text;
                    tb_sss_no.Text = lv_employee.SelectedItems[0].SubItems[6].Text;
                    tb_hdmf_no.Text = lv_employee.SelectedItems[0].SubItems[7].Text;
                    tb_tin_no.Text = lv_employee.SelectedItems[0].SubItems[8].Text;
                    tb_philhealth_no.Text = lv_employee.SelectedItems[0].SubItems[9].Text;

                }
                else if (lbl_category.Text == "Loan")
                {


                    label120.Text = lv_employee.SelectedItems[0].SubItems[0].Text;
                    label89.Text = lv_employee.SelectedItems[0].SubItems[1].Text;
                    tb_loan_empID.Text = lv_employee.SelectedItems[0].SubItems[2].Text;
                    tb_loan_lname.Text = lv_employee.SelectedItems[0].SubItems[3].Text;
                    tb_loan_fname.Text = lv_employee.SelectedItems[0].SubItems[4].Text;
                    tb_loan_mname.Text = lv_employee.SelectedItems[0].SubItems[5].Text;
                    tb_comp_loan_amnt.Text = lv_employee.SelectedItems[0].SubItems[6].Text;
                    tb_comp_loan_bal.Text = lv_employee.SelectedItems[0].SubItems[7].Text;
                    tb_comp_loan_month_ded.Text = lv_employee.SelectedItems[0].SubItems[8].Text;
                    tb_sss_loan_amnt.Text = lv_employee.SelectedItems[0].SubItems[9].Text;
                    tb_sss_loan_bal.Text = lv_employee.SelectedItems[0].SubItems[10].Text;
                    tb_sss_loan_month_ded.Text = lv_employee.SelectedItems[0].SubItems[11].Text;
                    tb_pag_ibig_loan_amnt.Text = lv_employee.SelectedItems[0].SubItems[12].Text;
                    tb_pag_ibig_loan_bal.Text = lv_employee.SelectedItems[0].SubItems[13].Text;
                    tb_pag_ibig_loan_month_ded.Text = lv_employee.SelectedItems[0].SubItems[14].Text;

                }

            }

            private void readPhotoEmployee()
            {
                string myconnection = ConfigurationManager.ConnectionStrings["MyConstring"].ConnectionString;
                MySqlConnection connection = new MySqlConnection(myconnection);

                try
                {

                    String selectQuery = "SELECT image FROM employee WHERE emp_no = '" + lbl_emp_no.Text + "'";

                    cmd = new MySqlCommand(selectQuery, connection);

                    da = new MySqlDataAdapter(cmd);

                    DataTable table = new DataTable();

                    da.Fill(table);


                    byte[] img = (byte[])table.Rows[0][0];

                    MemoryStream ms = new MemoryStream(img);

                    pic_Image.Image = Image.FromStream(ms);

                    da.Dispose();

                }
                catch (Exception ex)
                {
                    MessageBox.Show(ex.Message);
                }
                {
                    connection.Close();
                }
            }
            private void btn_upload_Click(object sender, EventArgs e)
            {
                OpenFileDialog opf = new OpenFileDialog();

                opf.Filter = "Choose Image(*.jpg; * .png; *.gif) | *.jpg; * .png; *.gif";

                if (opf.ShowDialog() == DialogResult.OK)
                {
                    pic_Image.Image = Image.FromFile(opf.FileName);


                }
            }

            private void btn_export_Click(object sender, EventArgs e)
            {
                ToExcel();
            }

            private void cb_payment_SelectedIndexChanged(object sender, EventArgs e)
            {
                tb_salary.Text = "0.00";
                tb_daily_rate.Text = "0.00";
                tb_daily_hour_rate.Text = "0.00";
                tb_minRate.Text = "0.00";
            }

            private void tb_riceAllowance_KeyPress(object sender, KeyPressEventArgs e)
            {
                if ((e.KeyChar < 32) || (e.KeyChar >= '0') && (e.KeyChar <= '9') || (e.KeyChar == ',') || (e.KeyChar == '.'))
                    return;

                // only evident errors (like 'A' or '&') are restricted
                e.Handled = true;
            }

            private void tb_laundryAllowance_KeyPress(object sender, KeyPressEventArgs e)
            {
                if ((e.KeyChar < 32) || (e.KeyChar >= '0') && (e.KeyChar <= '9') || (e.KeyChar == ',') || (e.KeyChar == '.'))
                    return;

                e.Handled = true;
            }

            private void tb_MealAllowance_KeyPress(object sender, KeyPressEventArgs e)
            {
                if ((e.KeyChar < 32) || (e.KeyChar >= '0') && (e.KeyChar <= '9') || (e.KeyChar == ',') || (e.KeyChar == '.'))
                    return;

                e.Handled = true;
            }

            private void tb_MedicalBenefits_KeyPress(object sender, KeyPressEventArgs e)
            {
                if ((e.KeyChar < 32) || (e.KeyChar >= '0') && (e.KeyChar <= '9') || (e.KeyChar == ',') || (e.KeyChar == '.'))
                    return;

                e.Handled = true;
            }

            private void tb_Others_KeyPress(object sender, KeyPressEventArgs e)
            {
                if ((e.KeyChar < 32) || (e.KeyChar >= '0') && (e.KeyChar <= '9') || (e.KeyChar == ',') || (e.KeyChar == '.'))
                    return;

                e.Handled = true;
            }

            private void tb_riceAllowance_TextChanged(object sender, EventArgs e)
            {
                if (tb_riceAllowance.Text == "")
                {
                    tb_riceAllowance.Text = "0.00";
                }
            }

            private void tb_laundryAllowance_TextChanged(object sender, EventArgs e)
            {
                if (tb_laundryAllowance.Text == "")
                {
                    tb_laundryAllowance.Text = "0.00";
                }
            }

            private void tb_MealAllowance_TextChanged(object sender, EventArgs e)
            {
                if (tb_mealAllowance.Text == "")
                {
                    tb_mealAllowance.Text = "0.00";
                }
            }

            private void tb_MedicalBenefits_TextChanged(object sender, EventArgs e)
            {
                if (tb_MedicalBenefits.Text == "")
                {
                    tb_MedicalBenefits.Text = "0.00";
                }
            }

            private void tb_Others_TextChanged(object sender, EventArgs e)
            {
                if (tb_otherAllowance.Text == "")
                {
                    tb_otherAllowance.Text = "0.00";
                }
            }

            private void tb_minRate_TextChanged(object sender, EventArgs e)
            {
                if (tb_minRate.Text == "")
                {
                    tb_minRate.Text = "0.00";
                }
            }

            private void btn_add_MouseEnter(object sender, EventArgs e)
            {
                btn_add.BackgroundColor = ColorTranslator.FromHtml("#FFAFAF");
                btn_add.BackColor = ColorTranslator.FromHtml("#FFAFAF");
                btn_add.ForeColor = Color.White;
            }

            private void btn_add_MouseHover(object sender, EventArgs e)
            {
                btn_add.BackgroundColor = ColorTranslator.FromHtml("#FFAFAF");
                btn_add.BackColor = ColorTranslator.FromHtml("#FFAFAF");
                btn_add.ForeColor = Color.White;
            }

            private void btn_add_MouseLeave(object sender, EventArgs e)
            {
                btn_add.BackgroundColor = Color.Transparent;
                btn_add.BackColor = Color.Transparent;
                btn_add.ForeColor = ColorTranslator.FromHtml("#C00000");
            }

            private void btn_update_MouseEnter(object sender, EventArgs e)
            {
                btn_update.BackgroundColor = ColorTranslator.FromHtml("#FFAFAF");
                btn_update.BackColor = ColorTranslator.FromHtml("#FFAFAF");
                btn_update.ForeColor = Color.White;
            }

            private void btn_update_MouseHover(object sender, EventArgs e)
            {
                btn_update.BackgroundColor = ColorTranslator.FromHtml("#FFAFAF");
                btn_update.BackColor = ColorTranslator.FromHtml("#FFAFAF");
                btn_update.ForeColor = Color.White;
            }

            private void btn_update_MouseLeave(object sender, EventArgs e)
            {
                btn_update.BackgroundColor = Color.Transparent;
                btn_update.BackColor = Color.Transparent;
                btn_update.ForeColor = ColorTranslator.FromHtml("#C00000");
            }

            private void btn_Delete_MouseEnter(object sender, EventArgs e)
            {
                btn_Delete.BackgroundColor = ColorTranslator.FromHtml("#FFAFAF");
                btn_Delete.BackColor = ColorTranslator.FromHtml("#FFAFAF");
                btn_Delete.ForeColor = Color.White;
            }

            private void btn_Delete_MouseHover(object sender, EventArgs e)
            {
                btn_Delete.BackgroundColor = ColorTranslator.FromHtml("#FFAFAF");
                btn_Delete.BackColor = ColorTranslator.FromHtml("#FFAFAF");
                btn_Delete.ForeColor = Color.White;
            }

            private void btn_Delete_MouseLeave(object sender, EventArgs e)
            {
                btn_Delete.BackgroundColor = Color.Transparent;
                btn_Delete.BackColor = Color.Transparent;
                btn_Delete.ForeColor = ColorTranslator.FromHtml("#C00000");
            }
    

            private void cb_payroll_mode_SelectionChangeCommitted(object sender, EventArgs e)
            {
                if (cb_payroll_mode.SelectedIndex == 0)
                {
                    cb_bank_name.Enabled = true;
                    tb_bank_acc.Enabled = true;
                    cb_bank_name2.Enabled = true;
                    tb_bank_acc2.Enabled = true;
                }
                else if (cb_payroll_mode.SelectedIndex == 1)
                {
                    cb_bank_name.Enabled = true;
                    tb_bank_acc.Enabled = true;
                    cb_bank_name2.Enabled = true;
                    tb_bank_acc2.Enabled = true;

                }
                else if (cb_payroll_mode.SelectedIndex == 2)
                {
                    cb_bank_name.Enabled = false;
                    tb_bank_acc.Enabled = false;
                    cb_bank_name2.Enabled = false;
                    tb_bank_acc2.Enabled = false;
                }
                cb_bank_name.SelectedItem = null;
                tb_bank_acc.Text = "";
                cb_bank_name2.SelectedItem = null;
                tb_bank_acc2.Text = "";
            }

            private void btn_Delete_Click(object sender, EventArgs e)
            {
                DialogResult dialogResult = MessageBox.Show("Are you sure want to Delete this Employee '" + tb_empID.Text + "'?", "Delete Employee", MessageBoxButtons.YesNo, MessageBoxIcon.Question);
                if (dialogResult == DialogResult.Yes)
                {
                    delete_employee();
                    disable_all();
                    EmployeeID();
                    MessageBox.Show("Employee Successfully Deleted.");

                }
            }

            private void delete_employee()
            {

                if (lbl_category.Text == "General Information")
                {
                    delete_overtime("DELETE FROM employee WHERE emp_no = '" + lbl_emp_no.Text + "'");
                    delete_overtime("DELETE FROM salary WHERE emp_no = '" + lbl_emp_no.Text + "'");
                    delete_overtime("DELETE FROM leave_balance WHERE emp_no = '" + lbl_emp_no.Text + "'");
                    delete_overtime("DELETE FROM others_id WHERE emp_no = '" + lbl_emp_no.Text + "'");
                    delete_overtime("DELETE FROM loan WHERE emp_no = '" + lbl_emp_no.Text + "'");
                    lv_employee.Columns.Clear();
                    Personal_Information("Select * from employee");
                }
                else if (lbl_category.Text == "Salary")
                {
                    delete_overtime("DELETE FROM employee WHERE emp_no = '" + label83.Text + "'");
                    delete_overtime("DELETE FROM salary WHERE emp_no = '" + label83.Text + "'");
                    delete_overtime("DELETE FROM leave_balance WHERE emp_no = '" + label83.Text + "'");
                    delete_overtime("DELETE FROM others_id WHERE emp_no = '" + label83.Text + "'");
                    delete_overtime("DELETE FROM loan WHERE emp_no = '" + label83.Text + "'");
                    lv_employee.Columns.Clear();
                    Salary("SELECT * FROM salary LEFT JOIN employee ON salary.emp_no = employee.emp_no");

                }

                else if (lbl_category.Text == "Leave")
                {
                    delete_overtime("DELETE FROM employee WHERE emp_no = '" + label79.Text + "'");
                    delete_overtime("DELETE FROM salary WHERE emp_no = '" + label79.Text + "'");
                    delete_overtime("DELETE FROM leave_balance WHERE emp_no = '" + label79.Text + "'");
                    delete_overtime("DELETE FROM others_id WHERE emp_no = '" + label79.Text + "'");
                    delete_overtime("DELETE FROM loan WHERE emp_no = '" + label79.Text + "'");
                    lv_employee.Columns.Clear();
                    leave_balanced("SELECT * FROM leave_balance LEFT JOIN employee ON leave_balance.emp_no = employee.emp_no");

                }

                else if (lbl_category.Text == "Loan")
                {
                    delete_overtime("DELETE FROM employee WHERE emp_no = '" + label89.Text + "'");
                    delete_overtime("DELETE FROM salary WHERE emp_no = '" + label89.Text + "'");
                    delete_overtime("DELETE FROM leave_balance WHERE emp_no = '" + label89.Text + "'");
                    delete_overtime("DELETE FROM others_id WHERE emp_no = '" + label89.Text + "'");
                    delete_overtime("DELETE FROM loan WHERE emp_no = '" + label89 + "'");
                    lv_employee.Columns.Clear();
                    Loan("SELECT * FROM loan LEFT JOIN employee ON loan.emp_no = employee.emp_no");

                }

                else if (lbl_category.Text == "Other ID")
                {
                    delete_overtime("DELETE FROM employee WHERE emp_no = '" + label88.Text + "'");
                    delete_overtime("DELETE FROM salary WHERE emp_no = '" + label88.Text + "'");
                    delete_overtime("DELETE FROM leave_balance WHERE emp_no = '" + label88.Text + "'");
                    delete_overtime("DELETE FROM others_id WHERE emp_no = '" + label88.Text + "'");
                    delete_overtime("DELETE FROM loan WHERE emp_no = '" + label88.Text + "'");
                    lv_employee.Columns.Clear();
                    ID("Select * FROM others_id LEFT JOIN employee ON others_id.emp_no = employee.emp_no");


                }

            }

            private void delete_overtime(string sql)
            {
                string myconnection = ConfigurationManager.ConnectionStrings["MyConstring"].ConnectionString;
                MySqlConnection connection = new MySqlConnection(myconnection);

                try
                {
                    connection.Open();
                    cmd = new MySqlCommand();
                    cmd.Connection = connection;
                    cmd.CommandText = sql;
                    da = new MySqlDataAdapter();
                    da.SelectCommand = cmd;
                    dt = new DataTable();
                    da.Fill(dt);

                }
                catch (Exception ex)
                {
                    MessageBox.Show(ex.Message);
                }
                finally
                {
                    connection.Close();
                }
            }

            private void btn_upload_MouseEnter(object sender, EventArgs e)
            {
                btn_upload.BackgroundColor = ColorTranslator.FromHtml("#FFAFAF");
                btn_upload.BackColor = ColorTranslator.FromHtml("#FFAFAF");
                btn_upload.ForeColor = Color.White;
            }

            private void btn_upload_MouseHover(object sender, EventArgs e)
            {
                btn_upload.BackgroundColor = ColorTranslator.FromHtml("#FFAFAF");
                btn_upload.BackColor = ColorTranslator.FromHtml("#FFAFAF");
                btn_upload.ForeColor = Color.White;
            }

            private void btn_upload_MouseLeave(object sender, EventArgs e)
            {
                btn_upload.BackgroundColor = Color.Transparent;
                btn_upload.BackColor = Color.Transparent;
                btn_upload.ForeColor = ColorTranslator.FromHtml("#C00000");
            }

            private void btn_export_MouseEnter(object sender, EventArgs e)
            {
                btn_export.BackgroundColor = ColorTranslator.FromHtml("#FFAFAF");
                btn_export.BackColor = ColorTranslator.FromHtml("#FFAFAF");
                btn_export.ForeColor = Color.White;
            }

            private void btn_export_MouseHover(object sender, EventArgs e)
            {
                btn_export.BackgroundColor = ColorTranslator.FromHtml("#FFAFAF");
                btn_export.BackColor = ColorTranslator.FromHtml("#FFAFAF");
                btn_export.ForeColor = Color.White;
            }

            private void btn_export_MouseLeave(object sender, EventArgs e)
            {
                btn_export.BackgroundColor = Color.Transparent;
                btn_export.BackColor = Color.Transparent;
                btn_export.ForeColor = ColorTranslator.FromHtml("#C00000");
            }

            private void groupBox2_Enter(object sender, EventArgs e)
            {

            }
        }
    }








