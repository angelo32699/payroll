﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using MySql.Data.MySqlClient;
using System.Configuration;

namespace Payroll
{



    public partial class Add_Overtime : Form
    {
        MySqlCommand cmd;
        MySqlDataAdapter da;
        DataTable dt;

        string myconnection = ConfigurationManager.ConnectionStrings["MyConstring"].ConnectionString;
        MySqlConnection connection;

        public Add_Overtime()
        {
            InitializeComponent();
            connection = new MySqlConnection(myconnection);
        }

        private void LoadOvertimeCategories()
        {

            load_categories("Select * from add_overtime where emp_no = '" + label8.Text + "'");
          
            try
            {
                connection.Open();
                cmd = new MySqlCommand();
                cmd.Connection = connection;
                cmd.CommandText = "Select * FROM overtime";
                da = new MySqlDataAdapter();
                da.SelectCommand = cmd;
                dt = new DataTable();
                da.Fill(dt);

                for (int i = 0; i < dt.Rows.Count; i++)
                {
                    DataRow dr = dt.Rows[i];



                    cb_overtime_categories.Items.Add((dr["ot_description"].ToString()));
                }

            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
            finally
            {
                connection.Close();
            }

        }

        private void LoadOvertimeCodeCategories()
        {

            try
            {
                connection.Open();
                cmd = new MySqlCommand();
                cmd.Connection = connection;
                cmd.CommandText = "Select ot_code, ot_id  FROM overtime where ot_description = '"+cb_overtime_categories.Text+"'";
                da = new MySqlDataAdapter();
                da.SelectCommand = cmd;
                dt = new DataTable();
                da.Fill(dt);

                for (int i = 0; i < dt.Rows.Count; i++)
                {
                    DataRow dr = dt.Rows[i];

                    lbl_code.Text = (dr["ot_code"].ToString());
                    label7.Text = (dr["ot_id"].ToString());

                }

            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
            finally
            {
                connection.Close();
            }

        }



        private void Add_Overtime_Load(object sender, EventArgs e)
        {

            tb_total_pay.Text = string.Format("{0:n}", decimal.Parse(tb_total_pay.Text));

            LoadOvertimeCategories();
            TotalPayOvertime();



            decimal x = Convert.ToDecimal(label5.Text);
            decimal y = Math.Round(x, 2, MidpointRounding.AwayFromZero);
            label5.Text = y + "";



            cb_overtime_categories.SelectedItem = "Ordinary Day (MON-FRI)";
            tb_ot_hours.Text = "0.00";
            tb_ot_pay.Text = "0.00";

        }

     
        private void cb_overtime_categories_SelectedIndexChanged(object sender, EventArgs e)
        {
            LoadOvertimeCodeCategories();

            total_pay_overtime();


            tb_ot_hours.Text = "0.00";
            tb_ot_pay.Text = "0.00";

        }

    

        public void total_pay_overtime()
        {
          

            try
            {
                connection.Open();
                cmd = new MySqlCommand();
                cmd.Connection = connection;

                if (label7.Text == "1")
                {
                    
                    cmd.CommandText = "Select ot_code, ot_id, ot1  FROM overtime where ot_description = '" + cb_overtime_categories.Text + "'";
                }
               
                
                else if(label7.Text == "2")
                {
                    cmd.CommandText = "Select ot_code, ot_id, ot1, ot2  FROM overtime where ot_description = '" + cb_overtime_categories.Text + "'";
                }
                else if (label7.Text == "3")
                {
                    cmd.CommandText = "Select ot_code, ot_id, ot1  FROM overtime where ot_description = '" + cb_overtime_categories.Text + "'";
                }
                else if (label7.Text == "4")
                {
                    cmd.CommandText = "Select ot_code, ot_id, ot1, ot2, ot3  FROM overtime where ot_description = '" + cb_overtime_categories.Text + "'";
                }
                else if (label7.Text == "5")
                {
                    cmd.CommandText = "Select ot_code, ot_id, ot1  FROM overtime where ot_description = '" + cb_overtime_categories.Text + "'";
                }
                else if (label7.Text == "6")
                {
                    cmd.CommandText = "Select ot_code, ot_id, ot1, ot2  FROM overtime where ot_description = '" + cb_overtime_categories.Text + "'";
                }
                else if (label7.Text == "7")
                {
                    cmd.CommandText = "Select ot_code, ot_id, ot1, ot2, ot3  FROM overtime where ot_description = '" + cb_overtime_categories.Text + "'";
                }
                else if (label7.Text == "8")
                {
                    cmd.CommandText = "Select ot_code, ot_id, ot1  FROM overtime where ot_description = '" + cb_overtime_categories.Text + "'";
                }
                else if (label7.Text == "9")
                {
                    cmd.CommandText = "Select ot_code, ot_id, ot1, ot2, ot3  FROM overtime where ot_description = '" + cb_overtime_categories.Text + "'";
                }
                else if (label7.Text == "10")
                {
                    cmd.CommandText = "Select ot_code, ot_id, ot1, ot2  FROM overtime where ot_description = '" + cb_overtime_categories.Text + "'";
                }
                else if (label7.Text == "11")
                {
                    cmd.CommandText = "Select ot_code, ot_id, ot1, ot2, ot3  FROM overtime where ot_description = '" + cb_overtime_categories.Text + "'";
                }
                else if (label7.Text == "12")
                {
                    cmd.CommandText = "Select ot_code, ot_id, ot1, ot2, ot3, ot4  FROM overtime where ot_description = '" + cb_overtime_categories.Text + "'";
                }




                da = new MySqlDataAdapter();
                da.SelectCommand = cmd;
                dt = new DataTable();
                da.Fill(dt);

                for (int i = 0; i < dt.Rows.Count; i++)
                {
                    DataRow dr = dt.Rows[i];

                    if(label7.Text == "1")
                    {
                        lbl_ot1.Text = (dr["ot1"].ToString());
                        decimal ot1 = Convert.ToDecimal(lbl_ot1.Text);
                        decimal hours_of_overtime = Convert.ToDecimal(tb_ot_hours.Text);
                        decimal rate_per_hour = Convert.ToDecimal(label5.Text);
                        decimal overtimepay = ot1 * hours_of_overtime;
                        decimal total = overtimepay * rate_per_hour;
                        decimal x = Math.Round(total, 2, MidpointRounding.AwayFromZero);
                        tb_ot_pay.Text = x + "";
                        tb_ot_pay.ForeColor = Color.Black;

                    }
                    else if(label7.Text == "2")
                    {
                        lbl_ot1.Text = (dr["ot1"].ToString());
                        lbl_ot2.Text = (dr["ot2"].ToString());
                        decimal ot1 = Convert.ToDecimal(lbl_ot1.Text);
                        decimal ot2 = Convert.ToDecimal(lbl_ot2.Text);
                        decimal hours_of_overtime = Convert.ToDecimal(tb_ot_hours.Text);
                        decimal rate_per_hour = Convert.ToDecimal(label5.Text);
                        decimal add_overtime =  rate_per_hour * ot1;
                        decimal overtimepay = add_overtime * ot2;
                        decimal total = overtimepay * hours_of_overtime;
                        decimal x = Math.Round(total, 2, MidpointRounding.AwayFromZero);
                        tb_ot_pay.Text = x + "";
                        tb_ot_pay.ForeColor = Color.Black;
               




                    }
                    else if (label7.Text == "3")
                    {
                        lbl_ot1.Text = (dr["ot1"].ToString());
                        decimal ot1 = Convert.ToDecimal(lbl_ot1.Text);
                        decimal hours_of_overtime = Convert.ToDecimal(tb_ot_hours.Text);
                        decimal rate_per_hour = Convert.ToDecimal(label5.Text);
                        decimal overtimepay = ot1 * hours_of_overtime;
                        decimal total = overtimepay * rate_per_hour;
                        decimal x = Math.Round(total, 2, MidpointRounding.AwayFromZero);
                        tb_ot_pay.Text = x + "";
                        tb_ot_pay.ForeColor = Color.Black;
                    }
                    else if (label7.Text == "4")
                    {
                        lbl_ot1.Text = (dr["ot1"].ToString());
                        lbl_ot2.Text = (dr["ot2"].ToString());
                        lbl_ot3.Text = (dr["ot3"].ToString());
                        decimal ot1 = Convert.ToDecimal(lbl_ot1.Text);
                        decimal ot2 = Convert.ToDecimal(lbl_ot2.Text);
                        decimal ot3 = Convert.ToDecimal(lbl_ot3.Text);
                        decimal hours_of_overtime = Convert.ToDecimal(tb_ot_hours.Text);
                        decimal rate_per_hour = Convert.ToDecimal(label5.Text);
                        decimal add_overtime = ot1 + ot2 + ot3;
                        decimal overtimepay = add_overtime * hours_of_overtime;
                        decimal total = overtimepay * rate_per_hour;
                        decimal x = Math.Round(total, 2, MidpointRounding.AwayFromZero);
                        tb_ot_pay.Text = x + "";
                        tb_ot_pay.ForeColor = Color.Black;

                    }
                    else if (label7.Text == "5")
                    {
                        lbl_ot1.Text = (dr["ot1"].ToString());
                        decimal ot1 = Convert.ToDecimal(lbl_ot1.Text);
                        decimal hours_of_overtime = Convert.ToDecimal(tb_ot_hours.Text);
                        decimal rate_per_hour = Convert.ToDecimal(label5.Text);
                        decimal overtimepay = ot1 * hours_of_overtime;
                        decimal total = overtimepay * rate_per_hour;
                        decimal x = Math.Round(total, 2, MidpointRounding.AwayFromZero);
                        tb_ot_pay.Text = x + "";
                        tb_ot_pay.ForeColor = Color.Black;
                    }
                    else if (label7.Text == "6")
                    {
                        lbl_ot1.Text = (dr["ot1"].ToString());
                        lbl_ot2.Text = (dr["ot2"].ToString());
                        decimal ot1 = Convert.ToDecimal(lbl_ot1.Text);
                        decimal ot2 = Convert.ToDecimal(lbl_ot2.Text);
                        decimal hours_of_overtime = Convert.ToDecimal(tb_ot_hours.Text);
                        decimal rate_per_hour = Convert.ToDecimal(label5.Text);
                        decimal add_overtime = ot1 + ot2;
                        decimal overtimepay = add_overtime * hours_of_overtime;
                        decimal total = overtimepay * rate_per_hour;
                        decimal x = Math.Round(total, 2, MidpointRounding.AwayFromZero);
                        tb_ot_pay.Text = x + "";
                        tb_ot_pay.ForeColor = Color.Black;
                    }
                    else if (label7.Text == "7")
                    {
                        lbl_ot1.Text = (dr["ot1"].ToString());
                        lbl_ot2.Text = (dr["ot2"].ToString());
                        lbl_ot3.Text = (dr["ot3"].ToString());
                        decimal ot1 = Convert.ToDecimal(lbl_ot1.Text);
                        decimal ot2 = Convert.ToDecimal(lbl_ot2.Text);
                        decimal ot3 = Convert.ToDecimal(lbl_ot3.Text);
                        decimal hours_of_overtime = Convert.ToDecimal(tb_ot_hours.Text);
                        decimal rate_per_hour = Convert.ToDecimal(label5.Text);
                        decimal add_overtime = ot1 + ot2 + ot3;
                        decimal overtimepay = add_overtime * hours_of_overtime;
                        decimal total = overtimepay * rate_per_hour;
                        decimal x = Math.Round(total, 2, MidpointRounding.AwayFromZero);
                        tb_ot_pay.Text = x + "";
                        tb_ot_pay.ForeColor = Color.Black;
                    }
                    else if (label7.Text == "8")
                    {
                        lbl_ot1.Text = (dr["ot1"].ToString());
                        decimal ot1 = Convert.ToDecimal(lbl_ot1.Text);
                        decimal hours_of_overtime = Convert.ToDecimal(tb_ot_hours.Text);
                        decimal rate_per_hour = Convert.ToDecimal(label5.Text);
                        decimal overtimepay = ot1 * hours_of_overtime;
                        decimal total = overtimepay * rate_per_hour;
                        decimal x = Math.Round(total, 2, MidpointRounding.AwayFromZero);
                        tb_ot_pay.Text = x + "";
                        tb_ot_pay.ForeColor = Color.Black;
                    }
                    else if (label7.Text == "9")
                    {
                        lbl_ot1.Text = (dr["ot1"].ToString());
                        lbl_ot2.Text = (dr["ot2"].ToString());
                        lbl_ot3.Text = (dr["ot3"].ToString());
                        decimal ot1 = Convert.ToDecimal(lbl_ot1.Text);
                        decimal ot2 = Convert.ToDecimal(lbl_ot2.Text);
                        decimal ot3 = Convert.ToDecimal(lbl_ot3.Text);
                        decimal hours_of_overtime = Convert.ToDecimal(tb_ot_hours.Text);
                        decimal rate_per_hour = Convert.ToDecimal(label5.Text);
                        decimal add_overtime = ot1 + ot2 + ot3;
                        decimal overtimepay = add_overtime * hours_of_overtime;
                        decimal total = overtimepay * rate_per_hour;
                        decimal x = Math.Round(total, 2, MidpointRounding.AwayFromZero);
                        tb_ot_pay.Text = x + "";
                        tb_ot_pay.ForeColor = Color.Black;
                    }
                    else if (label7.Text == "10")
                    {
                        lbl_ot1.Text = (dr["ot1"].ToString());
                        lbl_ot2.Text = (dr["ot2"].ToString());
                        decimal ot1 = Convert.ToDecimal(lbl_ot1.Text);
                        decimal ot2 = Convert.ToDecimal(lbl_ot2.Text);
                        decimal hours_of_overtime = Convert.ToDecimal(tb_ot_hours.Text);
                        decimal rate_per_hour = Convert.ToDecimal(label5.Text);
                        decimal add_overtime = ot1 + ot2;
                        decimal overtimepay = add_overtime * hours_of_overtime;
                        decimal total = overtimepay * rate_per_hour;
                        decimal x = Math.Round(total, 2, MidpointRounding.AwayFromZero);
                        tb_ot_pay.Text = x + "";
                        tb_ot_pay.ForeColor = Color.Black;
                    }
                    else if (label7.Text == "11")
                    {
                        lbl_ot1.Text = (dr["ot1"].ToString());
                        lbl_ot2.Text = (dr["ot2"].ToString());
                        lbl_ot3.Text = (dr["ot3"].ToString());
                        decimal ot1 = Convert.ToDecimal(lbl_ot1.Text);
                        decimal ot2 = Convert.ToDecimal(lbl_ot2.Text);
                        decimal ot3 = Convert.ToDecimal(lbl_ot3.Text);
                        decimal hours_of_overtime = Convert.ToDecimal(tb_ot_hours.Text);
                        decimal rate_per_hour = Convert.ToDecimal(label5.Text);
                        decimal add_overtime = ot1 + ot2 + ot3;
                        decimal overtimepay = add_overtime * hours_of_overtime;
                        decimal total = overtimepay * rate_per_hour;
                        decimal x = Math.Round(total, 2, MidpointRounding.AwayFromZero);
                        tb_ot_pay.Text = x + "";
                        tb_ot_pay.ForeColor = Color.Black;
                    }
                    else if (label7.Text == "12")
                    {
                        lbl_ot1.Text = (dr["ot1"].ToString());
                        lbl_ot2.Text = (dr["ot2"].ToString());
                        lbl_ot3.Text = (dr["ot3"].ToString());
                        lbl_ot4.Text = (dr["ot4"].ToString());
                        decimal ot1 = Convert.ToDecimal(lbl_ot1.Text);
                        decimal ot2 = Convert.ToDecimal(lbl_ot2.Text);
                        decimal ot3 = Convert.ToDecimal(lbl_ot3.Text);
                        decimal ot4 = Convert.ToDecimal(lbl_ot4.Text);
                        decimal hours_of_overtime = Convert.ToDecimal(tb_ot_hours.Text);
                        decimal rate_per_hour = Convert.ToDecimal(label5.Text);
                        decimal add_overtime = ot1 + ot2 + ot3 + ot4;
                        decimal overtimepay = add_overtime * hours_of_overtime;
                        decimal total = overtimepay * rate_per_hour;
                        decimal x = Math.Round(total, 2, MidpointRounding.AwayFromZero);
                        tb_ot_pay.Text = x + "";
                        tb_ot_pay.ForeColor = Color.Black;
                    }


                }

            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
            finally
            {
                connection.Close();
            }
        }

        private void btn_add_Click(object sender, EventArgs e)
        {

            if(btn_add.Text == "ADD")
            {
                cb_overtime_categories.Enabled = true;
                tb_ot_hours.Enabled = true;
                tb_ot_pay.Enabled = true;
                btn_add.Text = "SAVE";
                
               
            }
            else
            {
                if (tb_ot_hours.Text == "" || cb_overtime_categories.Text == "" || tb_ot_pay.Text == "")
                {
                    MessageBox.Show("Please Input Overtime", "Add Overtime", MessageBoxButtons.OK, MessageBoxIcon.Information);
                }
                else
                {
                  
                    add_overtime();
                    lv_overtime.Columns.Clear();
                    load_categories("Select * from add_overtime where emp_no = '" + label8.Text + "'");
                    MessageBox.Show("Successfully Add", "Add Overtime", MessageBoxButtons.OK, MessageBoxIcon.Information);
                    TotalPayOvertime();
                    total_pay_overtime();

                    btn_add.Text = "ADD";

                    tb_ot_hours.Text = "0.00";
                    tb_ot_pay.Text = "0.00";

                    
                    cb_overtime_categories.Enabled = false;
                    tb_ot_hours.Enabled = false;
                    tb_ot_pay.Enabled = false;
                    cb_overtime_categories.SelectedItem = null;


                       


                }
            }
          }
        public void add_overtime()
        {
          
            decimal overtime_pay = Convert.ToDecimal(tb_ot_pay.Text);


            string insertQuery = "INSERT INTO add_overtime(emp_no, code, ot_description, hours_of_ot, overtime_pay, date_to, date_from)Values('" + label8.Text + "', '" + lbl_code.Text + "', '"+cb_overtime_categories.Text+"', '"+ tb_ot_hours.Text+"', '" + overtime_pay + "', '"+label4.Text+"', '"+label10.Text+"')";

            connection.Open();
            MySqlCommand mySqlCommand = new MySqlCommand(insertQuery, connection);


            try
            {
                if (mySqlCommand.ExecuteNonQuery() == 1)
                {
                    //MessageBox.Show("Successfully Add Information", "Add Information", MessageBoxButtons.OK, MessageBoxIcon.Information);
                }

            }
            catch (Exception ex)
            {
                MessageBox.Show("Error" + ex);
            }

            connection.Close();
        }
        public void load_categories(string sql1)
        {
            string myconnection = ConfigurationManager.ConnectionStrings["MyConstring"].ConnectionString;
            MySqlConnection connection = new MySqlConnection(myconnection);

            try
            {
                connection.Open();
                cmd = new MySqlCommand();
                cmd.Connection = connection;
                cmd.CommandText = sql1;
                da = new MySqlDataAdapter();
                da.SelectCommand = cmd;
                dt = new DataTable();
                da.Fill(dt);
                lv_overtime.Items.Clear();



                for (int i = 0; i < dt.Rows.Count; i++)
                {
                    DataRow dr = dt.Rows[i];
                    ListViewItem listitem = new ListViewItem(dr["ao_id"].ToString());
                    listitem.SubItems.Add(dr["code"].ToString());
                    listitem.SubItems.Add(dr["ot_description"].ToString());
                    listitem.SubItems.Add(dr["hours_of_ot"].ToString());
                    //listitem.SubItems.Add(dr["overtime_pay"].ToString());
                    listitem.SubItems.Add(string.Format("{0:#,##0.00}", Convert.ToDecimal(dr["overtime_pay"])));





                    lv_overtime.Items.Add(listitem);
                }

            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
            finally
            {
                connection.Close();
            }

            lv_overtime.View = View.Details;
            lv_overtime.Columns.Add(" No ", 0, HorizontalAlignment.Center);
            lv_overtime.Columns.Add(" Code ", 100, HorizontalAlignment.Center);
            lv_overtime.Columns.Add(" OT_Description ", 0, HorizontalAlignment.Center);
            lv_overtime.Columns.Add(" Hours of Overtime ", 150, HorizontalAlignment.Center);
            lv_overtime.Columns.Add(" Overtime Pay ", 150, HorizontalAlignment.Center);
        }

        private void TotalPayOvertime()
        {
            

            try
            {
                connection.Open();
                cmd = new MySqlCommand();
                cmd.Connection = connection;
                cmd.CommandText = "SELECT SUM(overtime_pay) as test FROM add_overtime WHERE emp_no = '" + label8.Text + "'";
                da = new MySqlDataAdapter();
                da.SelectCommand = cmd;
                dt = new DataTable();
                da.Fill(dt);

                for (int i = 0; i < dt.Rows.Count; i++)
                {
                    DataRow dr = dt.Rows[i];



                    tb_total_pay.Text = (dr["test"].ToString());
                  




                }

            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
            finally
            {
                connection.Close();
            }
        }

       
        private void tb_ot_pay_Enter(object sender, EventArgs e)
        {
           
                total_pay_overtime();
            
        }

           private void btn_apply_Click(object sender, EventArgs e)
           {
            if (tb_ot_pay.Text == "")
            {
                MessageBox.Show("Please complete the field of overtime", "Overtime", MessageBoxButtons.OK, MessageBoxIcon.Information);
            }
            else
            {
                MessageBox.Show("Successfully Apply", "Overtime", MessageBoxButtons.OK, MessageBoxIcon.Information);
                this.Hide();

            }   
          }
      


          private void lv_overtime_Click(object sender, EventArgs e)
        {

            btn_add.Enabled = false;
            btn_update.Enabled = true;
            btn_clear.Enabled = true;
            btn_delete.Enabled = true;

            label9.Text = lv_overtime.SelectedItems[0].SubItems[0].Text;
            cb_overtime_categories.Text = lv_overtime.SelectedItems[0].SubItems[2].Text;
            tb_ot_hours.Text = lv_overtime.SelectedItems[0].SubItems[3].Text;
            tb_ot_pay.Text = lv_overtime.SelectedItems[0].SubItems[4].Text;
        }

        public void delete_overtime()
        {
          

            try
            {
                connection.Open();
                cmd = new MySqlCommand();
                cmd.Connection = connection;
                cmd.CommandText = "Delete From add_overtime where ao_id = '" + label9.Text + "'";
                da = new MySqlDataAdapter();
                da.SelectCommand = cmd;
                dt = new DataTable();
                da.Fill(dt);




            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
            finally
            {
                connection.Close();
            }
        }

        private void btn_delete_Click(object sender, EventArgs e)
        {
            DialogResult result = MessageBox.Show("Do you want to delete?", "Confirmation", MessageBoxButtons.YesNoCancel);
            if (result == DialogResult.Yes)
            {
                delete_overtime();
                lv_overtime.Columns.Clear();
                LoadOvertimeCategories();
                TotalPayOvertime();
                
            }
        }

        private void btn_update_Click(object sender, EventArgs e)
        {

            if(btn_update.Text == "UPDATE")
            {

                cb_overtime_categories.Enabled = true;
                tb_ot_hours.Enabled = true;
                tb_ot_pay.Enabled = true;
                btn_update.Text = "SAVE";
            }
            else
            {
                DialogResult result = MessageBox.Show("Do you want to Update?", "Confirmation", MessageBoxButtons.YesNoCancel);
                if (result == DialogResult.Yes)
                {

                    update_overtime();
                    lv_overtime.Columns.Clear();
                    LoadOvertimeCategories();
                    TotalPayOvertime();
                    MessageBox.Show("Successfully Update", "Update Overtime", MessageBoxButtons.OK, MessageBoxIcon.Information);

                }
            }
           
           
         
        }
        public void update_overtime()
        {
           

            try
            {
                connection.Open();
                cmd = new MySqlCommand();
                cmd.Connection = connection;
                cmd.CommandText = "UPDATE add_overtime SET code  = '" + lbl_code.Text + "', hours_of_ot = '" + tb_ot_hours.Text + "' ,  overtime_pay = '" + tb_ot_pay.Text + "'   WHERE ao_id = '" + label9.Text + "'";

                da = new MySqlDataAdapter();
                da.SelectCommand = cmd;
                dt = new DataTable();
                da.Fill(dt);
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
            finally
            {
                connection.Close();
            }
        }

        private void domainUpDown1_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (((e.KeyChar < 48 || e.KeyChar > 57) && e.KeyChar != 8 && e.KeyChar != 46))
            {
                e.Handled = true;
                return;
            }

          
            if (e.KeyChar == 46)
            {
                if ((sender as TextBox).Text.IndexOf(e.KeyChar) != -1)
                    e.Handled = true;
            }
        }

        private void tb_ot_pay_KeyPress(object sender, KeyPressEventArgs e)
        {
            if ((e.KeyChar < 32) || (e.KeyChar >= '0') && (e.KeyChar <= '9') || (e.KeyChar == ',') || (e.KeyChar == '.'))
                return;

            // only evident errors (like 'A' or '&') are restricted
            e.Handled = true;
        }

        private void tb_total_pay_KeyPress(object sender, KeyPressEventArgs e)
        {

            if ((e.KeyChar < 32) || (e.KeyChar >= '0') && (e.KeyChar <= '9') || (e.KeyChar == ',') || (e.KeyChar == '.'))
                return;

            // only evident errors (like 'A' or '&') are restricted
            e.Handled = true;
        }

        private void domainUpDown1_Enter(object sender, EventArgs e)
        {

            total_pay_overtime();
        }

        private void domainUpDown1_SelectedItemChanged(object sender, EventArgs e)
        {
            total_pay_overtime();

        }

        private void btn_cancel_Click(object sender, EventArgs e)
        {
            btn_add.Text = "ADD";
            btn_update.Text = "UPDATE";

            tb_ot_pay.Text = "0.00";
            tb_ot_hours.Text = "0.00";
            cb_overtime_categories.SelectedItem = null;
            tb_ot_pay.Enabled = false;
            tb_ot_hours.Enabled = false;
            cb_overtime_categories.Enabled = true;

            btn_add.Enabled = true;
            btn_clear.Enabled = false;
            btn_delete.Enabled = false;
            btn_update.Enabled = false;
        }

        private void tb_ot_hours_KeyPress(object sender, KeyPressEventArgs e)
        {
            if ((e.KeyChar < 32) || (e.KeyChar >= '0') && (e.KeyChar <= '9') || (e.KeyChar == ',') || (e.KeyChar == '.'))
                return;

            // only evident errors (like 'A' or '&') are restricted
            e.Handled = true;
        }

    
        private void tb_ot_hours_TextChanged(object sender, EventArgs e)
        {
            if(tb_ot_hours.Text == "")
            {
                tb_ot_hours.Text = "0.00";
            }

            total_pay_overtime();
        }

        private void tb_total_pay_TextChanged(object sender, EventArgs e)
        {
            if(tb_total_pay.Text == "")
            {
                tb_total_pay.Text = "0.00";
            }
        }

        private void tb_ot_pay_TextChanged(object sender, EventArgs e)
        {
            if (tb_ot_pay.Text == "")
            {
                tb_ot_pay.Text = "0.00";
            }
        }
    }
    }

