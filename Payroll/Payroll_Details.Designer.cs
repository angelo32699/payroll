﻿namespace Payroll
{
    partial class Payroll_Details
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(Payroll_Details));
            this.lv_payroll_details = new System.Windows.Forms.ListView();
            this.label27 = new System.Windows.Forms.Label();
            this.tb_search = new System.Windows.Forms.TextBox();
            this.comboBox1 = new System.Windows.Forms.ComboBox();
            this.comboBox2 = new System.Windows.Forms.ComboBox();
            this.label1 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.btn_year_end = new System.Windows.Forms.Button();
            this.btn_print_report = new System.Windows.Forms.Button();
            this.btn_print = new System.Windows.Forms.Button();
            this.SuspendLayout();
            // 
            // lv_payroll_details
            // 
            this.lv_payroll_details.BackColor = System.Drawing.Color.White;
            this.lv_payroll_details.Font = new System.Drawing.Font("Arial", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lv_payroll_details.FullRowSelect = true;
            this.lv_payroll_details.GridLines = true;
            this.lv_payroll_details.HideSelection = false;
            this.lv_payroll_details.Location = new System.Drawing.Point(3, 48);
            this.lv_payroll_details.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.lv_payroll_details.Name = "lv_payroll_details";
            this.lv_payroll_details.Size = new System.Drawing.Size(1265, 560);
            this.lv_payroll_details.TabIndex = 68;
            this.lv_payroll_details.UseCompatibleStateImageBehavior = false;
            this.lv_payroll_details.View = System.Windows.Forms.View.Details;
            // 
            // label27
            // 
            this.label27.AutoSize = true;
            this.label27.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label27.Location = new System.Drawing.Point(960, 25);
            this.label27.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label27.Name = "label27";
            this.label27.Size = new System.Drawing.Size(59, 18);
            this.label27.TabIndex = 72;
            this.label27.Text = "Search:";
            // 
            // tb_search
            // 
            this.tb_search.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.tb_search.ForeColor = System.Drawing.Color.Black;
            this.tb_search.Location = new System.Drawing.Point(1032, 21);
            this.tb_search.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.tb_search.Name = "tb_search";
            this.tb_search.Size = new System.Drawing.Size(236, 24);
            this.tb_search.TabIndex = 71;
            this.tb_search.TextChanged += new System.EventHandler(this.tb_search_TextChanged);
            // 
            // comboBox1
            // 
            this.comboBox1.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.comboBox1.FormattingEnabled = true;
            this.comboBox1.Location = new System.Drawing.Point(84, 18);
            this.comboBox1.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.comboBox1.Name = "comboBox1";
            this.comboBox1.Size = new System.Drawing.Size(139, 24);
            this.comboBox1.TabIndex = 73;
            this.comboBox1.SelectionChangeCommitted += new System.EventHandler(this.comboBox1_SelectionChangeCommitted);
            // 
            // comboBox2
            // 
            this.comboBox2.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.comboBox2.FormattingEnabled = true;
            this.comboBox2.Location = new System.Drawing.Point(301, 16);
            this.comboBox2.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.comboBox2.Name = "comboBox2";
            this.comboBox2.Size = new System.Drawing.Size(113, 24);
            this.comboBox2.TabIndex = 74;
            this.comboBox2.SelectionChangeCommitted += new System.EventHandler(this.comboBox2_SelectionChangeCommitted);
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label1.Location = new System.Drawing.Point(16, 21);
            this.label1.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(54, 18);
            this.label1.TabIndex = 75;
            this.label1.Text = "Month:";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label2.Location = new System.Drawing.Point(232, 21);
            this.label2.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(55, 18);
            this.label2.TabIndex = 76;
            this.label2.Text = "Period:";
            // 
            // btn_year_end
            // 
            this.btn_year_end.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btn_year_end.Image = global::Payroll.Properties.Resources.calendar__3_;
            this.btn_year_end.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.btn_year_end.Location = new System.Drawing.Point(3, 614);
            this.btn_year_end.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.btn_year_end.Name = "btn_year_end";
            this.btn_year_end.Size = new System.Drawing.Size(196, 46);
            this.btn_year_end.TabIndex = 78;
            this.btn_year_end.Text = "YEAR END";
            this.btn_year_end.UseVisualStyleBackColor = true;
            this.btn_year_end.Click += new System.EventHandler(this.btn_year_end_Click);
            // 
            // btn_print_report
            // 
            this.btn_print_report.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btn_print_report.Image = global::Payroll.Properties.Resources.Print_light;
            this.btn_print_report.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.btn_print_report.Location = new System.Drawing.Point(204, 614);
            this.btn_print_report.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.btn_print_report.Name = "btn_print_report";
            this.btn_print_report.Size = new System.Drawing.Size(228, 46);
            this.btn_print_report.TabIndex = 77;
            this.btn_print_report.Text = "PRINT REPORT";
            this.btn_print_report.UseVisualStyleBackColor = true;
            this.btn_print_report.Click += new System.EventHandler(this.btn_print_report_Click);
            // 
            // btn_print
            // 
            this.btn_print.BackColor = System.Drawing.SystemColors.Control;
            this.btn_print.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btn_print.ForeColor = System.Drawing.Color.Black;
            this.btn_print.Image = global::Payroll.Properties.Resources.excel__2_;
            this.btn_print.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.btn_print.Location = new System.Drawing.Point(1016, 610);
            this.btn_print.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.btn_print.Name = "btn_print";
            this.btn_print.Size = new System.Drawing.Size(253, 50);
            this.btn_print.TabIndex = 69;
            this.btn_print.Text = "EXPORT TO EXCEL";
            this.btn_print.UseVisualStyleBackColor = false;
            this.btn_print.Click += new System.EventHandler(this.btn_print_Click);
            // 
            // Payroll_Details
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 16F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.AutoSize = true;
            this.ClientSize = new System.Drawing.Size(1275, 663);
            this.Controls.Add(this.btn_year_end);
            this.Controls.Add(this.btn_print_report);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.comboBox2);
            this.Controls.Add(this.comboBox1);
            this.Controls.Add(this.label27);
            this.Controls.Add(this.tb_search);
            this.Controls.Add(this.btn_print);
            this.Controls.Add(this.lv_payroll_details);
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.MaximizeBox = false;
            this.Name = "Payroll_Details";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Payroll Details";
            this.Load += new System.EventHandler(this.Payroll_Details_Load);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.ListView lv_payroll_details;
        private System.Windows.Forms.Button btn_print;
        private System.Windows.Forms.Label label27;
        private System.Windows.Forms.TextBox tb_search;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label2;
        public System.Windows.Forms.ComboBox comboBox2;
        public System.Windows.Forms.ComboBox comboBox1;
        private System.Windows.Forms.Button btn_print_report;
        private System.Windows.Forms.Button btn_year_end;
    }
}