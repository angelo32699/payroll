﻿namespace Payroll
{
    partial class Bonus
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(Bonus));
            this.cb_bonus = new System.Windows.Forms.ComboBox();
            this.label2 = new System.Windows.Forms.Label();
            this.domainUpDown1 = new System.Windows.Forms.DomainUpDown();
            this.label15 = new System.Windows.Forms.Label();
            this.label1 = new System.Windows.Forms.Label();
            this.dateTimePicker2 = new System.Windows.Forms.DateTimePicker();
            this.dateTimePicker1 = new System.Windows.Forms.DateTimePicker();
            this.label3 = new System.Windows.Forms.Label();
            this.dateTimePicker3 = new System.Windows.Forms.DateTimePicker();
            this.btn_payroll_processing = new System.Windows.Forms.Button();
            this.btn_print_report = new System.Windows.Forms.Button();
            this.btn_print_paylip = new System.Windows.Forms.Button();
            this.SuspendLayout();
            // 
            // cb_bonus
            // 
            this.cb_bonus.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cb_bonus.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.cb_bonus.ForeColor = System.Drawing.Color.Black;
            this.cb_bonus.FormattingEnabled = true;
            this.cb_bonus.Items.AddRange(new object[] {
            "13th Month Pay",
            "14th Month Pay",
            "15th Month Pay"});
            this.cb_bonus.Location = new System.Drawing.Point(338, 23);
            this.cb_bonus.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.cb_bonus.Name = "cb_bonus";
            this.cb_bonus.Size = new System.Drawing.Size(272, 28);
            this.cb_bonus.TabIndex = 185;
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Font = new System.Drawing.Font("Arial", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label2.Location = new System.Drawing.Point(189, 212);
            this.label2.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(133, 22);
            this.label2.TabIndex = 191;
            this.label2.Text = "Payroll Period:";
            // 
            // domainUpDown1
            // 
            this.domainUpDown1.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.domainUpDown1.Items.Add("1");
            this.domainUpDown1.Items.Add("2");
            this.domainUpDown1.Location = new System.Drawing.Point(333, 210);
            this.domainUpDown1.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.domainUpDown1.Name = "domainUpDown1";
            this.domainUpDown1.Size = new System.Drawing.Size(283, 26);
            this.domainUpDown1.TabIndex = 190;
            this.domainUpDown1.Text = "1";
            // 
            // label15
            // 
            this.label15.AutoSize = true;
            this.label15.Font = new System.Drawing.Font("Arial", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label15.Location = new System.Drawing.Point(156, 71);
            this.label15.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label15.Name = "label15";
            this.label15.Size = new System.Drawing.Size(167, 22);
            this.label15.TabIndex = 188;
            this.label15.Text = "Payroll Date From:";
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("Arial", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label1.Location = new System.Drawing.Point(178, 115);
            this.label1.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(143, 22);
            this.label1.TabIndex = 189;
            this.label1.Text = "Payroll Date To:";
            // 
            // dateTimePicker2
            // 
            this.dateTimePicker2.Checked = false;
            this.dateTimePicker2.CustomFormat = "M/d/yyyy";
            this.dateTimePicker2.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.dateTimePicker2.Format = System.Windows.Forms.DateTimePickerFormat.Custom;
            this.dateTimePicker2.Location = new System.Drawing.Point(338, 112);
            this.dateTimePicker2.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.dateTimePicker2.Name = "dateTimePicker2";
            this.dateTimePicker2.Size = new System.Drawing.Size(272, 26);
            this.dateTimePicker2.TabIndex = 187;
            // 
            // dateTimePicker1
            // 
            this.dateTimePicker1.CustomFormat = "M/d/yyyy";
            this.dateTimePicker1.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.dateTimePicker1.Format = System.Windows.Forms.DateTimePickerFormat.Custom;
            this.dateTimePicker1.Location = new System.Drawing.Point(338, 69);
            this.dateTimePicker1.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.dateTimePicker1.Name = "dateTimePicker1";
            this.dateTimePicker1.Size = new System.Drawing.Size(272, 26);
            this.dateTimePicker1.TabIndex = 186;
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Font = new System.Drawing.Font("Arial", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label3.Location = new System.Drawing.Point(140, 167);
            this.label3.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(184, 22);
            this.label3.TabIndex = 192;
            this.label3.Text = "Month Year Process:";
            // 
            // dateTimePicker3
            // 
            this.dateTimePicker3.CustomFormat = "M/d/yyyy";
            this.dateTimePicker3.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.dateTimePicker3.Format = System.Windows.Forms.DateTimePickerFormat.Custom;
            this.dateTimePicker3.Location = new System.Drawing.Point(338, 162);
            this.dateTimePicker3.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.dateTimePicker3.Name = "dateTimePicker3";
            this.dateTimePicker3.Size = new System.Drawing.Size(272, 26);
            this.dateTimePicker3.TabIndex = 193;
            // 
            // btn_payroll_processing
            // 
            this.btn_payroll_processing.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btn_payroll_processing.Image = global::Payroll.Properties.Resources.gui_process_icon_157057__1_;
            this.btn_payroll_processing.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.btn_payroll_processing.Location = new System.Drawing.Point(300, 274);
            this.btn_payroll_processing.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.btn_payroll_processing.Name = "btn_payroll_processing";
            this.btn_payroll_processing.Size = new System.Drawing.Size(268, 50);
            this.btn_payroll_processing.TabIndex = 194;
            this.btn_payroll_processing.Text = "PROCESS";
            this.btn_payroll_processing.UseVisualStyleBackColor = true;
            this.btn_payroll_processing.Click += new System.EventHandler(this.btn_payroll_processing_Click);
            // 
            // btn_print_report
            // 
            this.btn_print_report.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btn_print_report.Image = global::Payroll.Properties.Resources.Print_light;
            this.btn_print_report.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.btn_print_report.Location = new System.Drawing.Point(592, 277);
            this.btn_print_report.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.btn_print_report.Name = "btn_print_report";
            this.btn_print_report.Size = new System.Drawing.Size(276, 46);
            this.btn_print_report.TabIndex = 195;
            this.btn_print_report.Text = "PRINT REPORT";
            this.btn_print_report.UseVisualStyleBackColor = true;
            this.btn_print_report.Click += new System.EventHandler(this.btn_print_report_Click);
            // 
            // btn_print_paylip
            // 
            this.btn_print_paylip.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btn_print_paylip.Image = global::Payroll.Properties.Resources.Print;
            this.btn_print_paylip.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.btn_print_paylip.Location = new System.Drawing.Point(16, 274);
            this.btn_print_paylip.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.btn_print_paylip.Name = "btn_print_paylip";
            this.btn_print_paylip.Size = new System.Drawing.Size(258, 50);
            this.btn_print_paylip.TabIndex = 196;
            this.btn_print_paylip.Text = "PRINT PAYSLIP";
            this.btn_print_paylip.UseVisualStyleBackColor = true;
            this.btn_print_paylip.Click += new System.EventHandler(this.btn_print_paylip_Click);
            // 
            // Bonus
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 16F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(884, 350);
            this.Controls.Add(this.btn_print_paylip);
            this.Controls.Add(this.btn_print_report);
            this.Controls.Add(this.btn_payroll_processing);
            this.Controls.Add(this.dateTimePicker3);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.domainUpDown1);
            this.Controls.Add(this.label15);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.dateTimePicker2);
            this.Controls.Add(this.dateTimePicker1);
            this.Controls.Add(this.cb_bonus);
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.MaximizeBox = false;
            this.Name = "Bonus";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "nTH Month Process Transaction";
            this.Load += new System.EventHandler(this.Bonus_Load);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion
        private System.Windows.Forms.ComboBox cb_bonus;
        private System.Windows.Forms.Label label2;
        public System.Windows.Forms.DomainUpDown domainUpDown1;
        private System.Windows.Forms.Label label15;
        private System.Windows.Forms.Label label1;
        public System.Windows.Forms.DateTimePicker dateTimePicker2;
        private System.Windows.Forms.DateTimePicker dateTimePicker1;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.DateTimePicker dateTimePicker3;
        private System.Windows.Forms.Button btn_payroll_processing;
        private System.Windows.Forms.Button btn_print_report;
        private System.Windows.Forms.Button btn_print_paylip;
    }
}