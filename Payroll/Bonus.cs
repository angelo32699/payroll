﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using MySql.Data.MySqlClient;
using System.Configuration;

namespace Payroll
{
    public partial class Bonus : Form
    {
        MySqlCommand cmd;
        MySqlDataAdapter da;
        DataTable dt;


        string myconnection = ConfigurationManager.ConnectionStrings["MyConstring"].ConnectionString;
        MySqlConnection connection;

        string mmyyyy;
        string date_period;

        public Bonus()
        {
            InitializeComponent();
            connection = new MySqlConnection(myconnection);
        }

        private void btn_payroll_processing_Click(object sender, EventArgs e)
        {

            string date_process = dateTimePicker3.Value.ToString("MMyyyy");

            check_bonus_process();

            if(date_process == mmyyyy && domainUpDown1.Text == date_period)
            {
                MessageBox.Show("This Month Already Process.", "Bonus Payroll Process", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
            else
            {
                DialogResult result = MessageBox.Show("Are you sure you want to process?", "Confirmation", MessageBoxButtons.YesNoCancel, MessageBoxIcon.Question);
                if (result == DialogResult.Yes)
                {

                    delete_data();

                  
                    connection.Open();
                    cmd = new MySqlCommand();
                    cmd.Connection = connection;
                    cmd.CommandText = "Select * from employee left join  salary on employee.emp_no = salary.emp_no where employee.status = 'Active'";
                    da = new MySqlDataAdapter();
                    da.SelectCommand = cmd;
                    dt = new DataTable();
                    da.Fill(dt);


                    for (int i = 0; i < dt.Rows.Count; i++)
                    {
                        DataRow dr = dt.Rows[i];

                        string emp_no = (dr["emp_no"].ToString());
                        string salary_id = (dr["salary_id"].ToString());
                        string lname = (dr["lname"].ToString());
                        string fname = (dr["fname"].ToString());
                        string mname = (dr["mname"].ToString());
                        string emp_id = (dr["emp_id"].ToString());
                        string department = (dr["department"].ToString());
                        string emp_status = (dr["emp_status"].ToString());
                        string date_hired = (dr["date_hired"].ToString());
                        string retrieve_montly_pay = (dr["salary"].ToString());
                        string semi_monthly = (dr["regular_pay"].ToString());
                        string daily_rate  = (dr["daily_rate"].ToString());

                        decimal salary = Convert.ToDecimal(retrieve_montly_pay);

                        DateTime date1 = DateTime.Parse(dateTimePicker2.Text);
                        DateTime date2 = DateTime.Parse(date_hired);

                        int x = (((date1.Year - date2.Year) * 12) + date1.Month - date2.Month);


                        if (x >= 12)
                        {
                            x = 12;

                        }
                        else

                        {
                            x = x + 1;

                        }


                        decimal total = salary / 12;
                        decimal overall_total = total * x;

                       


                        string bonus = "";

                        if (cb_bonus.Text == "14th Month Pay")
                        {
                            bonus = "14TH";
                        }
                        else if (cb_bonus.Text == "15th Month Pay")
                        {
                            bonus = "15TH";
                        }
                        else
                        {
                            bonus = "13TH";
                        }
                        string myconnection = ConfigurationManager.ConnectionStrings["MyConstring"].ConnectionString;
                        MySqlConnection connection = new MySqlConnection(myconnection);

                        string insertQuery2 = "INSERT INTO bonus(bonus, date, date_from, date_to, period, emp_name, salary, department, date_hired, work_mos, gross_amnt, wtax, deduction, net_pay)Values('" + bonus + "', '" + dateTimePicker3.Text + "', '" + dateTimePicker1.Text + "', '" + dateTimePicker2.Text + "', '" + domainUpDown1.Text + "',  '" + lname + " " + fname + " " + mname + "', '" + retrieve_montly_pay + "', '" + department + "',  '" + date_hired + "', '" + x + "', '" + overall_total + "', '" + 0 + "', '" + 0 + "', '" + overall_total + "')";

                        connection.Open();
                        MySqlCommand mySqlCommand2 = new MySqlCommand(insertQuery2, connection);



                        if (mySqlCommand2.ExecuteNonQuery() == 1)
                        {
                            //MessageBox.Show("Successfully Process", "Payroll Processing", MessageBoxButtons.OK, MessageBoxIcon.Information);



                        }
                        connection.Close();


                      

                        string insertQuery3 = "INSERT INTO bonus_details(bonus, yyyymm, date_period, emp_id, emp_no, salary_id,  work_mos, gross_amnt, wtax, deduction, netpay)Values('" + cb_bonus.Text + "', '" + date_process + "', '" + domainUpDown1.Text + "', '"+emp_id+"', '"+emp_no+"', '"+salary_id+"' , '" + x + "', '" + overall_total + "', '" + 0.00 + "' , '" + 0.00 + "', '" + overall_total + "')";


                        connection.Open();
                        MySqlCommand mySqlCommand3 = new MySqlCommand(insertQuery3, connection);




                        if (mySqlCommand3.ExecuteNonQuery() == 1)
                        {
                            // MessageBox.Show("Successfully Process", "Payroll Processing", MessageBoxButtons.OK, MessageBoxIcon.Information);

                        }
                        connection.Close();

                        string insertQuery4 = "INSERT INTO pay_details(yyyymm, date_period, emp_id, fullname, emp_status, department, month_pay, regular_pay, daily_rate, gross_inc, net_pay)Values('" + date_process + "', '" + 0 + "' , '" + emp_id + "', '" + lname + " " + fname + " " + mname + "', '" +emp_status+ "' , '"+department+"', '" + salary + "',  '"+semi_monthly+"', '"+daily_rate+"', '" + overall_total + "', '"+overall_total+"')";


                        connection.Open();
                        MySqlCommand mySqlCommand4 = new MySqlCommand(insertQuery4, connection);

                        


                        if (mySqlCommand4.ExecuteNonQuery() == 1)
                        {
                            // MessageBox.Show("Successfully Process", "Payroll Processing", MessageBoxButtons.OK, MessageBoxIcon.Information);

                        }
                        connection.Close();
                    }

                    MessageBox.Show("Successfully Process", "Payroll Bonus Processing", MessageBoxButtons.OK, MessageBoxIcon.Information);
                    connection.Close();
                }
            }
          }


        public void delete_data()
        {
            

            try
            {
                connection.Open();
                cmd = new MySqlCommand();
                cmd.Connection = connection;
                string query2 = "DELETE  FROM bonus";
                cmd.CommandText = query2;
                da = new MySqlDataAdapter();
                da.SelectCommand = cmd;
                dt = new DataTable();
                da.Fill(dt);


            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
            finally
            {
                connection.Close();
            }
        }
    

        private void Bonus_Load(object sender, EventArgs e)
        {
            cb_bonus.SelectedItem = "13th Month Pay";
        }

        private void btn_print_report_Click(object sender, EventArgs e)
        {
            Bonus_Report bonus_Report = new Bonus_Report();
            bonus_Report.ShowDialog();
        }

        private void btn_print_paylip_Click(object sender, EventArgs e)
        {

                Bonus_Payslip bonus_Payslip = new Bonus_Payslip();
                bonus_Payslip.ShowDialog();            
        }

        public void check_bonus_process()
        {
            

            try
            {
                connection.Open();
                cmd = new MySqlCommand();
                cmd.Connection = connection;
                cmd.CommandText = "Select * FROM bonus_details";
                da = new MySqlDataAdapter();
                da.SelectCommand = cmd;
                dt = new DataTable();
                da.Fill(dt);

                for (int i = 0; i < dt.Rows.Count; i++)
                {
                    DataRow dr = dt.Rows[i];

                    mmyyyy = (dr["yyyymm"].ToString());
                    date_period = (dr["date_period"].ToString());


                }

            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
            finally
            {
                connection.Close();
            }
        }
    }
}
