﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using MySql.Data.MySqlClient;
using System.Configuration;

namespace Payroll
{
    public partial class Tax : Form
    {
        MySqlCommand cmd;
        MySqlDataAdapter da;
        DataTable dt;
        public Tax()
        {
            InitializeComponent();

        }

        private void Tax_Load(object sender, EventArgs e)
        {

            display_tax("Select * from tax");
      
        }
        public void display_tax(string sql1)
        {

            string myconnection = ConfigurationManager.ConnectionStrings["MyConstring"].ConnectionString;
            MySqlConnection connection = new MySqlConnection(myconnection);

            try
            {
                connection.Open();
                cmd = new MySqlCommand();
                cmd.Connection = connection;
                cmd.CommandText = sql1;
                da = new MySqlDataAdapter();
                da.SelectCommand = cmd;
                dt = new DataTable();
                da.Fill(dt);
                lv_Tax.Items.Clear();



                for (int i = 0; i < dt.Rows.Count; i++)
                {
                    DataRow dr = dt.Rows[i];
                    ListViewItem listitem = new ListViewItem(dr["no"].ToString());
                   
                    listitem.SubItems.Add(string.Format("{0:#,##0.00}", Convert.ToDecimal(dr["min_amnt"])));
                   
                    listitem.SubItems.Add(string.Format("{0:#,##0.00}", Convert.ToDecimal(dr["max_amnt"])));
                  
                    listitem.SubItems.Add(string.Format("{0:#,##0.00}", Convert.ToDecimal(dr["tax_due"])));
                  
                    listitem.SubItems.Add(string.Format("{0:#,##0.00}", Convert.ToDecimal(dr["less_amnt"])));
                  
                    listitem.SubItems.Add(dr["percent"].ToString());
                 
                    lv_Tax.Items.Add(listitem);
                }

            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
            finally
            {
                connection.Close();
            }


            lv_Tax.View = View.Details;
            lv_Tax.Columns.Add(" No. ", 0, HorizontalAlignment.Center);
            lv_Tax.Columns.Add(" Minimum Amount ", 170, HorizontalAlignment.Center);
            lv_Tax.Columns.Add(" Maximum Amount ", 170, HorizontalAlignment.Center);
            lv_Tax.Columns.Add(" Tax due ", 115, HorizontalAlignment.Center);
            lv_Tax.Columns.Add(" Less Amount ", 115, HorizontalAlignment.Center);
            lv_Tax.Columns.Add(" Percent ", 100, HorizontalAlignment.Center);
        }

        private void lv_Tax_Click(object sender, EventArgs e)
        {

            btn_add.Enabled = false;
            btn_delete.Enabled = true;
            btn_update.Enabled = true;

            tb_minimum_amount.Enabled = false;
            tb_maximum_amount.Enabled = false;
            tb_tax_due.Enabled = false;
            tb_less_amnt.Enabled = false;
            tb_percent.Enabled = false;

            btn_update.Text = "EDIT";
            btn_add.Text = "ADD";

            lbl_id.Text = lv_Tax.SelectedItems[0].SubItems[0].Text;
            tb_minimum_amount.Text = lv_Tax.SelectedItems[0].SubItems[1].Text;
            tb_maximum_amount.Text = lv_Tax.SelectedItems[0].SubItems[2].Text;
            tb_tax_due.Text = lv_Tax.SelectedItems[0].SubItems[3].Text;
            tb_less_amnt.Text = lv_Tax.SelectedItems[0].SubItems[4].Text;
            tb_percent.Text = lv_Tax.SelectedItems[0].SubItems[5].Text;
        }

        public void insert_Tax()
        {
            string myconnection = ConfigurationManager.ConnectionStrings["MyConstring"].ConnectionString;
            MySqlConnection connection = new MySqlConnection(myconnection);


            decimal min_amount = Convert.ToDecimal(tb_minimum_amount.Text);
            decimal max_amount = Convert.ToDecimal(tb_maximum_amount.Text);
            decimal tax_due = Convert.ToDecimal(tb_tax_due.Text);
            decimal less_amnt = Convert.ToDecimal(tb_less_amnt.Text);


            string insertQuery = "INSERT INTO tax(min_amnt, max_amnt, tax_due, less_amnt, percent) VALUES('" + min_amount + "', '" + max_amount + "', '" + tax_due + "', '" + less_amnt + "', '"+tb_percent.Text+"')";

            connection.Open();
            MySqlCommand mySqlCommand = new MySqlCommand(insertQuery, connection);


            try
            {
                if (mySqlCommand.ExecuteNonQuery() == 1)
                {
                    //MessageBox.Show("Successfully Add Information", "Add Information", MessageBoxButtons.OK, MessageBoxIcon.Information);
                }
            
            }
            catch (Exception ex)
            {
                MessageBox.Show("Error" + ex);
            }

            connection.Close();
        }

    

        private void tb_minimum_amount_KeyPress(object sender, KeyPressEventArgs e)
        {
            if ((e.KeyChar < 32) || (e.KeyChar >= '0') && (e.KeyChar <= '9') || (e.KeyChar == ',') || (e.KeyChar == '.'))
                return;

            // only evident errors (like 'A' or '&') are restricted
            e.Handled = true;
        }

        private void tb_maximum_amount_KeyPress(object sender, KeyPressEventArgs e)
        {
            if ((e.KeyChar < 32) || (e.KeyChar >= '0') && (e.KeyChar <= '9') || (e.KeyChar == ',') || (e.KeyChar == '.'))
                return;

            // only evident errors (like 'A' or '&') are restricted
            e.Handled = true;
        }

        private void tb_tax_due_KeyPress(object sender, KeyPressEventArgs e)
        {
            if ((e.KeyChar < 32) || (e.KeyChar >= '0') && (e.KeyChar <= '9') || (e.KeyChar == ',') || (e.KeyChar == '.'))
                return;

            // only evident errors (like 'A' or '&') are restricted
            e.Handled = true;
        }

        private void tb_less_amnt_KeyPress(object sender, KeyPressEventArgs e)
        {
            if ((e.KeyChar < 32) || (e.KeyChar >= '0') && (e.KeyChar <= '9') || (e.KeyChar == ',') || (e.KeyChar == '.'))
                return;

            // only evident errors (like 'A' or '&') are restricted
            e.Handled = true;
        }

        private void tb_percent_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (((e.KeyChar < 48 || e.KeyChar > 57) && e.KeyChar != 8 && e.KeyChar != 46))
            {
                e.Handled = true;
                return;
            }

            // checks to make sure only 1 decimal is allowed
            if (e.KeyChar == 46)
            {
                if ((sender as TextBox).Text.IndexOf(e.KeyChar) != -1)
                    e.Handled = true;
            }
        }

        private void lv_Tax_KeyDown(object sender, KeyEventArgs e)
        {
          
            lbl_id.Text = lv_Tax.SelectedItems[0].SubItems[0].Text;
            tb_minimum_amount.Text = lv_Tax.SelectedItems[0].SubItems[1].Text;
            tb_maximum_amount.Text = lv_Tax.SelectedItems[0].SubItems[2].Text;
            tb_tax_due.Text = lv_Tax.SelectedItems[0].SubItems[3].Text;
            tb_less_amnt.Text = lv_Tax.SelectedItems[0].SubItems[4].Text;
            tb_percent.Text = lv_Tax.SelectedItems[0].SubItems[5].Text;
        }

        private void lv_Tax_KeyUp(object sender, KeyEventArgs e)
        {
           
            lbl_id.Text = lv_Tax.SelectedItems[0].SubItems[0].Text;
            tb_minimum_amount.Text = lv_Tax.SelectedItems[0].SubItems[1].Text;
            tb_maximum_amount.Text = lv_Tax.SelectedItems[0].SubItems[2].Text;
            tb_tax_due.Text = lv_Tax.SelectedItems[0].SubItems[3].Text;
            tb_less_amnt.Text = lv_Tax.SelectedItems[0].SubItems[4].Text;
            tb_percent.Text = lv_Tax.SelectedItems[0].SubItems[5].Text;
        }
        public void update_tax()
        {
            string myconnection = ConfigurationManager.ConnectionStrings["MyConstring"].ConnectionString;
            MySqlConnection connection = new MySqlConnection(myconnection);

            decimal min_amount = Convert.ToDecimal(tb_minimum_amount.Text);
            decimal max_amount = Convert.ToDecimal(tb_maximum_amount.Text);
            decimal tax_due = Convert.ToDecimal(tb_tax_due.Text);
            decimal less_amnt = Convert.ToDecimal(tb_less_amnt.Text);

            try
            {
                connection.Open();
                cmd = new MySqlCommand();
                cmd.Connection = connection;
                cmd.CommandText = "UPDATE tax SET min_amnt  = '" + min_amount + "', max_amnt = '" + max_amount + "' ,  tax_due = '" + tax_due + "', less_amnt = '" + less_amnt + "' , percent = '" + tb_percent.Text + "'   WHERE no = '" + lbl_id.Text + "'";
                da = new MySqlDataAdapter();
                da.SelectCommand = cmd;
                dt = new DataTable();
                da.Fill(dt);

            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
            finally
            {
                connection.Close();
            }
        }
        public void delete_tax()
        {
            string myconnection = ConfigurationManager.ConnectionStrings["MyConstring"].ConnectionString;
            MySqlConnection connection = new MySqlConnection(myconnection);

            try
            {
                connection.Open();
                cmd = new MySqlCommand();
                cmd.Connection = connection;
                cmd.CommandText = "Delete From tax where no = '" + lbl_id.Text + "'";
                da = new MySqlDataAdapter();
                da.SelectCommand = cmd;
                dt = new DataTable();
                da.Fill(dt);

            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
            finally
            {
                connection.Close();
            }
        }

     
        private void tb_search_TextChanged(object sender, EventArgs e)
        {
            lv_Tax.Columns.Clear();
            display_tax("Select DISTINCT * FROM tax WHERE  tax.min_amnt like '" + tb_search.Text + "%' or tax.max_amnt like '" + tb_search.Text + "%' or  tax.tax_due like '" + tb_search.Text + "%'  or  tax.less_amnt like '" + tb_search.Text + "%' or tax.percent like '" + tb_search.Text + "%'");
        }

        private void tb_minimum_amount_TextChanged(object sender, EventArgs e)
        {
            if (tb_minimum_amount.Text == "") {

                tb_minimum_amount.Text = "0.00";
            }
        }

        private void tb_maximum_amount_TextChanged(object sender, EventArgs e)
        {
            if (tb_maximum_amount.Text == ""){

                tb_maximum_amount.Text = "0.00";
            }
        }

        private void tb_tax_due_TextChanged(object sender, EventArgs e)
        {
            if (tb_tax_due.Text == ""){

                tb_tax_due.Text = "0.00";
            }
        }

        private void tb_less_amnt_TextChanged(object sender, EventArgs e)
        {
            if (tb_less_amnt.Text == ""){

                tb_less_amnt.Text = "0.00";
            }
        }

        private void tb_percent_TextChanged(object sender, EventArgs e)
        {
            if (tb_percent.Text == ""){

                tb_percent.Text = "0.00";
            }
        }

        private void btn_add_Click(object sender, EventArgs e)
        {
            if (btn_add.Text == "ADD")
            {
                btn_add.Image = Properties.Resources.Button_SAVE_Red__1_;
                tb_minimum_amount.Enabled = true;
                tb_maximum_amount.Enabled = true;
                tb_tax_due.Enabled = true;
                tb_less_amnt.Enabled = true;
                tb_percent.Enabled = true;
                btn_add.Text = "SAVE";
            }
            else if(lbl_id.Text != "0")
            {
                DialogResult dialogResult = MessageBox.Show("Are you sure want to update Tax Table?", "Update Tax Table", MessageBoxButtons.YesNo, MessageBoxIcon.Question);
                if (dialogResult == DialogResult.Yes)
                {

                    update_tax();
                    lv_Tax.Columns.Clear();
                    display_tax("Select * from tax");
                    MessageBox.Show("Successfully Update Tax Table", "Update Tax Table", MessageBoxButtons.OK, MessageBoxIcon.Information);
                    disable_all();
                }
            }
            else
            {         
                    DialogResult dialogResult = MessageBox.Show("Are you sure want to Add New Tax?", "Add Tax Information", MessageBoxButtons.YesNo, MessageBoxIcon.Question);
                    if (dialogResult == DialogResult.Yes)
                    {
                        insert_Tax();
                        lv_Tax.Columns.Clear();
                        display_tax("Select * from tax");
                        MessageBox.Show("Successfully Added", "Add Tax Information", MessageBoxButtons.OK, MessageBoxIcon.Information);
                        disable_all();
                 
                }
            }
        }

        private void btn_update_Click(object sender, EventArgs e)
        {
            if (btn_update.Text == "EDIT")
            {

                tb_minimum_amount.Enabled = true;
                tb_maximum_amount.Enabled = true;
                tb_tax_due.Enabled = true;
                tb_less_amnt.Enabled = true;
                tb_percent.Enabled = true;

                btn_update.Enabled = false;
                btn_add.Enabled = true;
                btn_add.Text = "SAVE";
                btn_add.Image = Properties.Resources.Button_SAVE_Red__1_;

            }
            
        }

        private void btn_delete_Click(object sender, EventArgs e)
        {
            DialogResult dialogResult = MessageBox.Show("Are you sure want to delete Tax Table?", "Delete Tax Table", MessageBoxButtons.YesNo, MessageBoxIcon.Question);
            if (dialogResult == DialogResult.Yes)
            {

                delete_tax();
                lv_Tax.Columns.Clear();
                display_tax("Select * from tax");
                MessageBox.Show("Successfully Delete Tax Table", "Delete Tax Table", MessageBoxButtons.OK, MessageBoxIcon.Information);
                disable_all();
            }
        }

        private void disable_all()
        {
            btn_add.Image = Properties.Resources.Button_ADD_Red__1___1_;
            lbl_id.Text = "0";

            btn_add.Text = "ADD";
            btn_update.Text = "EDIT";

            tb_minimum_amount.Text = "";
            tb_maximum_amount.Text = "";
            tb_tax_due.Text = "";
            tb_less_amnt.Text = "";
            tb_percent.Text = "";

            tb_minimum_amount.Enabled = false;
            tb_maximum_amount.Enabled = false;
            tb_tax_due.Enabled = false;
            tb_less_amnt.Enabled = false;
            tb_percent.Enabled = false;

            btn_add.Enabled = true;
            btn_delete.Enabled = false;
            btn_update.Enabled = false;
        }

      

        private void btn_add_MouseEnter(object sender, EventArgs e)
        {
            btn_add.BackgroundColor = ColorTranslator.FromHtml("#FFAFAF");
            btn_add.BackColor = ColorTranslator.FromHtml("#FFAFAF");
            btn_add.ForeColor = Color.White;
        }

        private void btn_add_MouseHover(object sender, EventArgs e)
        {
            btn_add.BackgroundColor = ColorTranslator.FromHtml("#FFAFAF");
            btn_add.BackColor = ColorTranslator.FromHtml("#FFAFAF");
            btn_add.ForeColor = Color.White;
        }

        private void btn_add_MouseLeave(object sender, EventArgs e)
        {
            btn_add.BackgroundColor = Color.Transparent;
            btn_add.BackColor = Color.Transparent;
            btn_add.ForeColor = ColorTranslator.FromHtml("#C00000");
        }

        private void btn_update_MouseEnter(object sender, EventArgs e)
        {
            btn_update.BackgroundColor = ColorTranslator.FromHtml("#FFAFAF");
            btn_update.BackColor = ColorTranslator.FromHtml("#FFAFAF");
            btn_update.ForeColor = Color.White;
        }

        private void btn_update_MouseHover(object sender, EventArgs e)
        {
            btn_update.BackgroundColor = ColorTranslator.FromHtml("#FFAFAF");
            btn_update.BackColor = ColorTranslator.FromHtml("#FFAFAF");
            btn_update.ForeColor = Color.White;
        }

        private void btn_update_MouseLeave(object sender, EventArgs e)
        {
            btn_update.BackgroundColor = Color.Transparent;
            btn_update.BackColor = Color.Transparent;
            btn_update.ForeColor = ColorTranslator.FromHtml("#C00000");
        }

        private void btn_delete_MouseEnter(object sender, EventArgs e)
        {
            btn_delete.BackgroundColor = ColorTranslator.FromHtml("#FFAFAF");
            btn_delete.BackColor = ColorTranslator.FromHtml("#FFAFAF");
            btn_delete.ForeColor = Color.White;
        }

        private void btn_delete_MouseHover(object sender, EventArgs e)
        {
            btn_delete.BackgroundColor = ColorTranslator.FromHtml("#FFAFAF");
            btn_delete.BackColor = ColorTranslator.FromHtml("#FFAFAF");
            btn_delete.ForeColor = Color.White;
        }

        private void btn_delete_MouseLeave(object sender, EventArgs e)
        {
            btn_delete.BackgroundColor = Color.Transparent;
            btn_delete.BackColor = Color.Transparent;
            btn_delete.ForeColor = ColorTranslator.FromHtml("#C00000");
        }

        private void ToExcel()
        {
            StringBuilder sb = new StringBuilder();
            Microsoft.Office.Interop.Excel.Application app = new Microsoft.Office.Interop.Excel.Application();
            app.Visible = true;
            Microsoft.Office.Interop.Excel.Workbook wb = app.Workbooks.Add(1);
            Microsoft.Office.Interop.Excel.Worksheet ws = (Microsoft.Office.Interop.Excel.Worksheet)wb.Worksheets[1];
            int i = 1;
            int i2 = 2;
            int x = 1;
            int x2 = 1;
            foreach (ColumnHeader ch in lv_Tax.Columns)
            {
                ws.Cells[x2, x] = ch.Text;
                x++;
            }

            foreach (ListViewItem lvi in lv_Tax.Items)
            {
                i = 1;
                foreach (ListViewItem.ListViewSubItem lvs in lvi.SubItems)
                {
                    ws.Cells[i2, i] = lvs.Text;
                    ws.Cells.Select();
                    ws.Cells.EntireColumn.AutoFit();
                    i++;
                }
                i2++;
            }
        }

        private void btn_export_Click(object sender, EventArgs e)
        {
            ToExcel();
        }

        private void btn_export_MouseEnter(object sender, EventArgs e)
        {
            btn_export.BackgroundColor = ColorTranslator.FromHtml("#FFAFAF");
            btn_export.BackColor = ColorTranslator.FromHtml("#FFAFAF");
            btn_export.ForeColor = Color.White;
        }

        private void btn_export_MouseHover(object sender, EventArgs e)
        {
            btn_export.BackgroundColor = ColorTranslator.FromHtml("#FFAFAF");
            btn_export.BackColor = ColorTranslator.FromHtml("#FFAFAF");
            btn_export.ForeColor = Color.White;
        }

        private void btn_export_MouseLeave(object sender, EventArgs e)
        {
            btn_export.BackgroundColor = Color.Transparent;
            btn_export.BackColor = Color.Transparent;
            btn_export.ForeColor = ColorTranslator.FromHtml("#C00000");
        }
    }
}
