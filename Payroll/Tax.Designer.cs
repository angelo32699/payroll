﻿namespace Payroll
{
    partial class Tax
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(Tax));
            this.lv_Tax = new System.Windows.Forms.ListView();
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.lbl_id = new System.Windows.Forms.Label();
            this.btn_delete = new Payroll.RJButtons.RJButton();
            this.btn_update = new Payroll.RJButtons.RJButton();
            this.label5 = new System.Windows.Forms.Label();
            this.btn_add = new Payroll.RJButtons.RJButton();
            this.tb_less_amnt = new System.Windows.Forms.TextBox();
            this.label3 = new System.Windows.Forms.Label();
            this.tb_tax_due = new System.Windows.Forms.TextBox();
            this.label2 = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.tb_minimum_amount = new System.Windows.Forms.TextBox();
            this.label1 = new System.Windows.Forms.Label();
            this.tb_percent = new System.Windows.Forms.TextBox();
            this.tb_maximum_amount = new System.Windows.Forms.TextBox();
            this.label13 = new System.Windows.Forms.Label();
            this.tb_search = new System.Windows.Forms.TextBox();
            this.btn_export = new Payroll.RJButtons.RJButton();
            this.groupBox1.SuspendLayout();
            this.SuspendLayout();
            // 
            // lv_Tax
            // 
            this.lv_Tax.Font = new System.Drawing.Font("Arial", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lv_Tax.FullRowSelect = true;
            this.lv_Tax.GridLines = true;
            this.lv_Tax.HideSelection = false;
            this.lv_Tax.Location = new System.Drawing.Point(6, 245);
            this.lv_Tax.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.lv_Tax.Name = "lv_Tax";
            this.lv_Tax.Size = new System.Drawing.Size(1013, 336);
            this.lv_Tax.TabIndex = 68;
            this.lv_Tax.UseCompatibleStateImageBehavior = false;
            this.lv_Tax.View = System.Windows.Forms.View.Details;
            this.lv_Tax.Click += new System.EventHandler(this.lv_Tax_Click);
            this.lv_Tax.KeyDown += new System.Windows.Forms.KeyEventHandler(this.lv_Tax_KeyDown);
            this.lv_Tax.KeyUp += new System.Windows.Forms.KeyEventHandler(this.lv_Tax_KeyUp);
            // 
            // groupBox1
            // 
            this.groupBox1.Controls.Add(this.lbl_id);
            this.groupBox1.Controls.Add(this.btn_delete);
            this.groupBox1.Controls.Add(this.btn_update);
            this.groupBox1.Controls.Add(this.label5);
            this.groupBox1.Controls.Add(this.btn_add);
            this.groupBox1.Controls.Add(this.tb_less_amnt);
            this.groupBox1.Controls.Add(this.label3);
            this.groupBox1.Controls.Add(this.tb_tax_due);
            this.groupBox1.Controls.Add(this.label2);
            this.groupBox1.Controls.Add(this.label4);
            this.groupBox1.Controls.Add(this.tb_minimum_amount);
            this.groupBox1.Controls.Add(this.label1);
            this.groupBox1.Controls.Add(this.tb_percent);
            this.groupBox1.Controls.Add(this.tb_maximum_amount);
            this.groupBox1.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.groupBox1.Location = new System.Drawing.Point(4, 11);
            this.groupBox1.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Padding = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.groupBox1.Size = new System.Drawing.Size(1015, 188);
            this.groupBox1.TabIndex = 73;
            this.groupBox1.TabStop = false;
            this.groupBox1.Text = "Tax Table";
            // 
            // lbl_id
            // 
            this.lbl_id.AutoSize = true;
            this.lbl_id.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lbl_id.Location = new System.Drawing.Point(8, 25);
            this.lbl_id.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.lbl_id.Name = "lbl_id";
            this.lbl_id.Size = new System.Drawing.Size(18, 20);
            this.lbl_id.TabIndex = 79;
            this.lbl_id.Text = "0";
            this.lbl_id.Visible = false;
            // 
            // btn_delete
            // 
            this.btn_delete.BackColor = System.Drawing.Color.Transparent;
            this.btn_delete.BackgroundColor = System.Drawing.Color.Transparent;
            this.btn_delete.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(192)))), ((int)(((byte)(0)))), ((int)(((byte)(0)))));
            this.btn_delete.BorderRadius = 8;
            this.btn_delete.BorderSize = 1;
            this.btn_delete.Enabled = false;
            this.btn_delete.FlatAppearance.BorderSize = 0;
            this.btn_delete.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btn_delete.Font = new System.Drawing.Font("Microsoft Sans Serif", 8F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btn_delete.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(192)))), ((int)(((byte)(0)))), ((int)(((byte)(0)))));
            this.btn_delete.Image = global::Payroll.Properties.Resources.icons8_delete_30;
            this.btn_delete.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.btn_delete.Location = new System.Drawing.Point(603, 118);
            this.btn_delete.Name = "btn_delete";
            this.btn_delete.Size = new System.Drawing.Size(175, 50);
            this.btn_delete.TabIndex = 195;
            this.btn_delete.Text = "DELETE";
            this.btn_delete.TextColor = System.Drawing.Color.FromArgb(((int)(((byte)(192)))), ((int)(((byte)(0)))), ((int)(((byte)(0)))));
            this.btn_delete.UseVisualStyleBackColor = false;
            this.btn_delete.Click += new System.EventHandler(this.btn_delete_Click);
            this.btn_delete.MouseEnter += new System.EventHandler(this.btn_delete_MouseEnter);
            this.btn_delete.MouseLeave += new System.EventHandler(this.btn_delete_MouseLeave);
            this.btn_delete.MouseHover += new System.EventHandler(this.btn_delete_MouseHover);
            // 
            // btn_update
            // 
            this.btn_update.BackColor = System.Drawing.Color.Transparent;
            this.btn_update.BackgroundColor = System.Drawing.Color.Transparent;
            this.btn_update.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(192)))), ((int)(((byte)(0)))), ((int)(((byte)(0)))));
            this.btn_update.BorderRadius = 8;
            this.btn_update.BorderSize = 1;
            this.btn_update.Enabled = false;
            this.btn_update.FlatAppearance.BorderSize = 0;
            this.btn_update.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btn_update.Font = new System.Drawing.Font("Microsoft Sans Serif", 8F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btn_update.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(192)))), ((int)(((byte)(0)))), ((int)(((byte)(0)))));
            this.btn_update.Image = global::Payroll.Properties.Resources.Button_UPDATE_Red__4___1___1_;
            this.btn_update.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.btn_update.Location = new System.Drawing.Point(422, 117);
            this.btn_update.Name = "btn_update";
            this.btn_update.Size = new System.Drawing.Size(175, 50);
            this.btn_update.TabIndex = 193;
            this.btn_update.Text = "EDIT";
            this.btn_update.TextColor = System.Drawing.Color.FromArgb(((int)(((byte)(192)))), ((int)(((byte)(0)))), ((int)(((byte)(0)))));
            this.btn_update.UseVisualStyleBackColor = false;
            this.btn_update.Click += new System.EventHandler(this.btn_update_Click);
            this.btn_update.MouseEnter += new System.EventHandler(this.btn_update_MouseEnter);
            this.btn_update.MouseLeave += new System.EventHandler(this.btn_update_MouseLeave);
            this.btn_update.MouseHover += new System.EventHandler(this.btn_update_MouseHover);
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Font = new System.Drawing.Font("Arial", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label5.Location = new System.Drawing.Point(676, 48);
            this.label5.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(123, 23);
            this.label5.TabIndex = 78;
            this.label5.Text = "Less Amount";
            // 
            // btn_add
            // 
            this.btn_add.BackColor = System.Drawing.Color.Transparent;
            this.btn_add.BackgroundColor = System.Drawing.Color.Transparent;
            this.btn_add.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(192)))), ((int)(((byte)(0)))), ((int)(((byte)(0)))));
            this.btn_add.BorderRadius = 8;
            this.btn_add.BorderSize = 1;
            this.btn_add.FlatAppearance.BorderSize = 0;
            this.btn_add.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btn_add.Font = new System.Drawing.Font("Microsoft Sans Serif", 8F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btn_add.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(192)))), ((int)(((byte)(0)))), ((int)(((byte)(0)))));
            this.btn_add.Image = global::Payroll.Properties.Resources.Button_ADD_Red__1___1_;
            this.btn_add.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.btn_add.Location = new System.Drawing.Point(237, 117);
            this.btn_add.Name = "btn_add";
            this.btn_add.Size = new System.Drawing.Size(175, 50);
            this.btn_add.TabIndex = 192;
            this.btn_add.Text = "ADD";
            this.btn_add.TextColor = System.Drawing.Color.FromArgb(((int)(((byte)(192)))), ((int)(((byte)(0)))), ((int)(((byte)(0)))));
            this.btn_add.UseVisualStyleBackColor = false;
            this.btn_add.Click += new System.EventHandler(this.btn_add_Click);
            this.btn_add.MouseEnter += new System.EventHandler(this.btn_add_MouseEnter);
            this.btn_add.MouseLeave += new System.EventHandler(this.btn_add_MouseLeave);
            this.btn_add.MouseHover += new System.EventHandler(this.btn_add_MouseHover);
            // 
            // tb_less_amnt
            // 
            this.tb_less_amnt.Enabled = false;
            this.tb_less_amnt.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.tb_less_amnt.ForeColor = System.Drawing.Color.Black;
            this.tb_less_amnt.Location = new System.Drawing.Point(673, 76);
            this.tb_less_amnt.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.tb_less_amnt.Name = "tb_less_amnt";
            this.tb_less_amnt.Size = new System.Drawing.Size(184, 28);
            this.tb_less_amnt.TabIndex = 77;
            this.tb_less_amnt.Text = "0.00";
            this.tb_less_amnt.TextChanged += new System.EventHandler(this.tb_less_amnt_TextChanged);
            this.tb_less_amnt.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.tb_less_amnt_KeyPress);
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Font = new System.Drawing.Font("Arial", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label3.Location = new System.Drawing.Point(490, 48);
            this.label3.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(81, 23);
            this.label3.TabIndex = 74;
            this.label3.Text = "Tax Due";
            // 
            // tb_tax_due
            // 
            this.tb_tax_due.Enabled = false;
            this.tb_tax_due.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.tb_tax_due.ForeColor = System.Drawing.Color.Black;
            this.tb_tax_due.Location = new System.Drawing.Point(487, 76);
            this.tb_tax_due.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.tb_tax_due.Name = "tb_tax_due";
            this.tb_tax_due.Size = new System.Drawing.Size(178, 28);
            this.tb_tax_due.TabIndex = 73;
            this.tb_tax_due.Text = "0.00";
            this.tb_tax_due.TextChanged += new System.EventHandler(this.tb_tax_due_TextChanged);
            this.tb_tax_due.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.tb_tax_due_KeyPress);
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Font = new System.Drawing.Font("Arial", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label2.Location = new System.Drawing.Point(245, 48);
            this.label2.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(164, 23);
            this.label2.TabIndex = 72;
            this.label2.Text = "Maximum Amount";
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Font = new System.Drawing.Font("Arial", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label4.Location = new System.Drawing.Point(861, 46);
            this.label4.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(78, 23);
            this.label4.TabIndex = 71;
            this.label4.Text = "Percent";
            // 
            // tb_minimum_amount
            // 
            this.tb_minimum_amount.Enabled = false;
            this.tb_minimum_amount.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.tb_minimum_amount.ForeColor = System.Drawing.Color.Black;
            this.tb_minimum_amount.Location = new System.Drawing.Point(12, 75);
            this.tb_minimum_amount.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.tb_minimum_amount.Name = "tb_minimum_amount";
            this.tb_minimum_amount.Size = new System.Drawing.Size(221, 28);
            this.tb_minimum_amount.TabIndex = 64;
            this.tb_minimum_amount.Text = "0.00";
            this.tb_minimum_amount.TextChanged += new System.EventHandler(this.tb_minimum_amount_TextChanged);
            this.tb_minimum_amount.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.tb_minimum_amount_KeyPress);
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("Arial", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label1.Location = new System.Drawing.Point(9, 48);
            this.label1.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(158, 23);
            this.label1.TabIndex = 68;
            this.label1.Text = "Minimum Amount";
            // 
            // tb_percent
            // 
            this.tb_percent.Enabled = false;
            this.tb_percent.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.tb_percent.ForeColor = System.Drawing.Color.Black;
            this.tb_percent.Location = new System.Drawing.Point(865, 74);
            this.tb_percent.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.tb_percent.Name = "tb_percent";
            this.tb_percent.Size = new System.Drawing.Size(142, 28);
            this.tb_percent.TabIndex = 67;
            this.tb_percent.Text = "0.00";
            this.tb_percent.TextChanged += new System.EventHandler(this.tb_percent_TextChanged);
            this.tb_percent.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.tb_percent_KeyPress);
            // 
            // tb_maximum_amount
            // 
            this.tb_maximum_amount.Enabled = false;
            this.tb_maximum_amount.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.tb_maximum_amount.ForeColor = System.Drawing.Color.Black;
            this.tb_maximum_amount.Location = new System.Drawing.Point(241, 76);
            this.tb_maximum_amount.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.tb_maximum_amount.Name = "tb_maximum_amount";
            this.tb_maximum_amount.Size = new System.Drawing.Size(238, 28);
            this.tb_maximum_amount.TabIndex = 65;
            this.tb_maximum_amount.Text = "0.00";
            this.tb_maximum_amount.TextChanged += new System.EventHandler(this.tb_maximum_amount_TextChanged);
            this.tb_maximum_amount.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.tb_maximum_amount_KeyPress);
            // 
            // label13
            // 
            this.label13.AutoSize = true;
            this.label13.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label13.Location = new System.Drawing.Point(696, 211);
            this.label13.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label13.Name = "label13";
            this.label13.Size = new System.Drawing.Size(72, 22);
            this.label13.TabIndex = 75;
            this.label13.Text = "Search:";
            // 
            // tb_search
            // 
            this.tb_search.Location = new System.Drawing.Point(773, 209);
            this.tb_search.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.tb_search.Name = "tb_search";
            this.tb_search.Size = new System.Drawing.Size(238, 26);
            this.tb_search.TabIndex = 77;
            this.tb_search.TextChanged += new System.EventHandler(this.tb_search_TextChanged);
            // 
            // btn_export
            // 
            this.btn_export.BackColor = System.Drawing.Color.Transparent;
            this.btn_export.BackgroundColor = System.Drawing.Color.Transparent;
            this.btn_export.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(192)))), ((int)(((byte)(0)))), ((int)(((byte)(0)))));
            this.btn_export.BorderRadius = 8;
            this.btn_export.BorderSize = 1;
            this.btn_export.FlatAppearance.BorderSize = 0;
            this.btn_export.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btn_export.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btn_export.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(192)))), ((int)(((byte)(0)))), ((int)(((byte)(0)))));
            this.btn_export.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.btn_export.Location = new System.Drawing.Point(797, 589);
            this.btn_export.Name = "btn_export";
            this.btn_export.Size = new System.Drawing.Size(219, 44);
            this.btn_export.TabIndex = 186;
            this.btn_export.Text = "Export to Excel";
            this.btn_export.TextColor = System.Drawing.Color.FromArgb(((int)(((byte)(192)))), ((int)(((byte)(0)))), ((int)(((byte)(0)))));
            this.btn_export.UseVisualStyleBackColor = false;
            this.btn_export.Click += new System.EventHandler(this.btn_export_Click);
            this.btn_export.MouseEnter += new System.EventHandler(this.btn_export_MouseEnter);
            this.btn_export.MouseLeave += new System.EventHandler(this.btn_export_MouseLeave);
            this.btn_export.MouseHover += new System.EventHandler(this.btn_export_MouseHover);
            // 
            // Tax
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(9F, 20F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1024, 639);
            this.Controls.Add(this.btn_export);
            this.Controls.Add(this.tb_search);
            this.Controls.Add(this.label13);
            this.Controls.Add(this.groupBox1);
            this.Controls.Add(this.lv_Tax);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedToolWindow;
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.MaximizeBox = false;
            this.Name = "Tax";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Tax Table";
            this.Load += new System.EventHandler(this.Tax_Load);
            this.groupBox1.ResumeLayout(false);
            this.groupBox1.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.ListView lv_Tax;
        private System.Windows.Forms.GroupBox groupBox1;
        private System.Windows.Forms.Label label4;
        internal System.Windows.Forms.TextBox tb_minimum_amount;
        private System.Windows.Forms.Label label1;
        internal System.Windows.Forms.TextBox tb_percent;
        internal System.Windows.Forms.TextBox tb_maximum_amount;
        private System.Windows.Forms.Label label2;
        internal System.Windows.Forms.TextBox tb_tax_due;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label label13;
        private System.Windows.Forms.Label label5;
        internal System.Windows.Forms.TextBox tb_less_amnt;
        private System.Windows.Forms.Label lbl_id;
        private System.Windows.Forms.TextBox tb_search;
        private RJButtons.RJButton btn_delete;
        private RJButtons.RJButton btn_update;
        private RJButtons.RJButton btn_add;
        private RJButtons.RJButton btn_export;
    }
}