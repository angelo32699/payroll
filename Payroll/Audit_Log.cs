﻿using MySql.Data.MySqlClient;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Configuration;
using System.Data;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Payroll
{
    public partial class Audit_Log : Form
    {
        MySqlCommand cmd;
        MySqlDataAdapter da;
        MySqlDataReader mdr;
        DataTable dt;
        FileStream fs;
        BinaryReader br;
        public Audit_Log()
        {
            InitializeComponent();
        }

        private void Audit_Log_Load(object sender, EventArgs e)
        {
            View_Details("SELECT * FROM audit_logs LEFT JOIN user_account ON user_account.user_id = audit_logs.user_id");
        }
        private void View_Details(string sql1)
        {
           
                string myconnection = ConfigurationManager.ConnectionStrings["MyConstring"].ConnectionString;
                MySqlConnection connection = new MySqlConnection(myconnection);

                try
                {
                    connection.Open();
                    cmd = new MySqlCommand();
                    cmd.Connection = connection;
                    cmd.CommandText = sql1;
                    da = new MySqlDataAdapter();
                    da.SelectCommand = cmd;
                    dt = new DataTable();
                    da.Fill(dt);
                    lv_AuditLogs.Items.Clear();

                    for (int i = 0; i < dt.Rows.Count; i++)
                    {
                        DataRow dr = dt.Rows[i];
                        ListViewItem listitem = new ListViewItem(dr["tran_date"].ToString());
                        listitem.SubItems.Add(dr["username"].ToString());
                        listitem.SubItems.Add(dr["transaction"].ToString());
                                          
                        lv_AuditLogs.Items.Add(listitem);
                    }

                }
                catch (Exception ex)
                {
                    MessageBox.Show(ex.Message);
                }
                finally
                {
                    connection.Close();
                }


            lv_AuditLogs.View = View.Details;
            lv_AuditLogs.Columns.Add(" Date ", 150, HorizontalAlignment.Center);
            lv_AuditLogs.Columns.Add(" Name ", 180, HorizontalAlignment.Center);
            lv_AuditLogs.Columns.Add(" Transaction ", 360, HorizontalAlignment.Center);
        }
        private void ToExcel()
        {
            StringBuilder sb = new StringBuilder();
            Microsoft.Office.Interop.Excel.Application app = new Microsoft.Office.Interop.Excel.Application();
            app.Visible = true;
            Microsoft.Office.Interop.Excel.Workbook wb = app.Workbooks.Add(1);
            Microsoft.Office.Interop.Excel.Worksheet ws = (Microsoft.Office.Interop.Excel.Worksheet)wb.Worksheets[1];
            int i = 1;
            int i2 = 2;
            int x = 1;
            int x2 = 1;
            foreach (ColumnHeader ch in lv_AuditLogs.Columns)
            {
                ws.Cells[x2, x] = ch.Text;
                x++;
            }

            foreach (ListViewItem lvi in lv_AuditLogs.Items)
            {
                i = 1;
                foreach (ListViewItem.ListViewSubItem lvs in lvi.SubItems)
                {
                    ws.Cells[i2, i] = lvs.Text;
                    ws.Cells.Select();
                    ws.Cells.EntireColumn.AutoFit();
                    i++;
                }
                i2++;
            }
        }

        private void btn_export_Click(object sender, EventArgs e)
        {
            ToExcel();
        }

        private void tb_search_TextChanged(object sender, EventArgs e)
        {
            lv_AuditLogs.Columns.Clear();
            View_Details("Select DISTINCT * FROM audit_logs LEFT JOIN user_account on audit_logs.user_id = user_account.user_id WHERE  audit_logs.tran_date like '" + tb_search.Text + "%' or   user_account.username like '" + tb_search.Text + "%' or  audit_logs.transaction like '" + tb_search.Text + "%'");
        }

        private void btn_export_MouseEnter(object sender, EventArgs e)
        {
            btn_export.BackgroundColor = ColorTranslator.FromHtml("#FFAFAF");
            btn_export.BackColor = ColorTranslator.FromHtml("#FFAFAF");
            btn_export.ForeColor = Color.White;
        }

        private void btn_export_MouseHover(object sender, EventArgs e)
        {
            btn_export.BackgroundColor = ColorTranslator.FromHtml("#FFAFAF");
            btn_export.BackColor = ColorTranslator.FromHtml("#FFAFAF");
            btn_export.ForeColor = Color.White;
        }

        private void btn_export_MouseLeave(object sender, EventArgs e)
        {
            btn_export.BackgroundColor = Color.Transparent;
            btn_export.BackColor = Color.Transparent;
            btn_export.ForeColor = ColorTranslator.FromHtml("#C00000");
        }
    }
}
