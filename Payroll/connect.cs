﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using MySql.Data.MySqlClient;

namespace Payroll
{
    class connect
    {
        public MySqlConnection con;
        public MySqlCommand cmd;
        public MySqlDataReader reader;
        public MySqlDataAdapter mda;

        public void connection()
        {
            con = new MySqlConnection("datasource = localhost; database=dtbase_payroll; port=3306; username = root; password= ");

                                      
            con.Open();

        }
        public void datasend(string sql) //for adding data
        {
            connection();
            cmd = new MySqlCommand(sql, con);
            cmd.ExecuteNonQuery();
            con.Close();
        }
        public void dataupdate(string sql) // for updating data
        {
            connection();
            cmd = new MySqlCommand(sql, con);
            reader = cmd.ExecuteReader();
            con.Close();

        }
        public void datadelete(string sql) // for deleting data
        {
            connection();
            cmd = new MySqlCommand(sql, con);
            reader = cmd.ExecuteReader();
            con.Close();
        }
        public void dataget(string sql)
        {
            connection();
            mda = new MySqlDataAdapter(sql, con);
            con.Close();
        }



    }
}
