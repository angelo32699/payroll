﻿namespace Payroll
{
    partial class Overtime
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(Overtime));
            this.tb_search = new System.Windows.Forms.TextBox();
            this.label13 = new System.Windows.Forms.Label();
            this.lv_Overtime = new System.Windows.Forms.ListView();
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.btn_add = new Payroll.RJButtons.RJButton();
            this.btn_update = new Payroll.RJButtons.RJButton();
            this.btn_delete = new Payroll.RJButtons.RJButton();
            this.label6 = new System.Windows.Forms.Label();
            this.tb_ot_4 = new System.Windows.Forms.TextBox();
            this.label5 = new System.Windows.Forms.Label();
            this.tb_ot_3 = new System.Windows.Forms.TextBox();
            this.label2 = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.tb_ot_1 = new System.Windows.Forms.TextBox();
            this.label3 = new System.Windows.Forms.Label();
            this.tb_ot_code = new System.Windows.Forms.TextBox();
            this.label1 = new System.Windows.Forms.Label();
            this.tb_ot_2 = new System.Windows.Forms.TextBox();
            this.tb_ot_desc = new System.Windows.Forms.TextBox();
            this.label7 = new System.Windows.Forms.Label();
            this.btn_export = new Payroll.RJButtons.RJButton();
            this.groupBox1.SuspendLayout();
            this.SuspendLayout();
            // 
            // tb_search
            // 
            this.tb_search.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.tb_search.ForeColor = System.Drawing.Color.Black;
            this.tb_search.Location = new System.Drawing.Point(758, 189);
            this.tb_search.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.tb_search.Name = "tb_search";
            this.tb_search.Size = new System.Drawing.Size(294, 28);
            this.tb_search.TabIndex = 81;
            this.tb_search.TextChanged += new System.EventHandler(this.tb_search_TextChanged);
            // 
            // label13
            // 
            this.label13.AutoSize = true;
            this.label13.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label13.Location = new System.Drawing.Point(675, 194);
            this.label13.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label13.Name = "label13";
            this.label13.Size = new System.Drawing.Size(72, 22);
            this.label13.TabIndex = 82;
            this.label13.Text = "Search:";
            // 
            // lv_Overtime
            // 
            this.lv_Overtime.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lv_Overtime.FullRowSelect = true;
            this.lv_Overtime.GridLines = true;
            this.lv_Overtime.HideSelection = false;
            this.lv_Overtime.Location = new System.Drawing.Point(4, 226);
            this.lv_Overtime.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.lv_Overtime.Name = "lv_Overtime";
            this.lv_Overtime.Size = new System.Drawing.Size(1056, 396);
            this.lv_Overtime.TabIndex = 79;
            this.lv_Overtime.UseCompatibleStateImageBehavior = false;
            this.lv_Overtime.Click += new System.EventHandler(this.lv_Overtime_Click);
            // 
            // groupBox1
            // 
            this.groupBox1.Controls.Add(this.btn_add);
            this.groupBox1.Controls.Add(this.btn_update);
            this.groupBox1.Controls.Add(this.btn_delete);
            this.groupBox1.Controls.Add(this.label6);
            this.groupBox1.Controls.Add(this.tb_ot_4);
            this.groupBox1.Controls.Add(this.label5);
            this.groupBox1.Controls.Add(this.tb_ot_3);
            this.groupBox1.Controls.Add(this.label2);
            this.groupBox1.Controls.Add(this.label4);
            this.groupBox1.Controls.Add(this.tb_ot_1);
            this.groupBox1.Controls.Add(this.label3);
            this.groupBox1.Controls.Add(this.tb_ot_code);
            this.groupBox1.Controls.Add(this.label1);
            this.groupBox1.Controls.Add(this.tb_ot_2);
            this.groupBox1.Controls.Add(this.tb_ot_desc);
            this.groupBox1.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.groupBox1.Location = new System.Drawing.Point(7, 9);
            this.groupBox1.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Padding = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.groupBox1.Size = new System.Drawing.Size(1051, 175);
            this.groupBox1.TabIndex = 84;
            this.groupBox1.TabStop = false;
            this.groupBox1.Text = "Overtime Table";
            // 
            // btn_add
            // 
            this.btn_add.BackColor = System.Drawing.Color.Transparent;
            this.btn_add.BackgroundColor = System.Drawing.Color.Transparent;
            this.btn_add.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(192)))), ((int)(((byte)(0)))), ((int)(((byte)(0)))));
            this.btn_add.BorderRadius = 8;
            this.btn_add.BorderSize = 1;
            this.btn_add.FlatAppearance.BorderSize = 0;
            this.btn_add.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btn_add.Font = new System.Drawing.Font("Microsoft Sans Serif", 8F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btn_add.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(192)))), ((int)(((byte)(0)))), ((int)(((byte)(0)))));
            this.btn_add.Image = global::Payroll.Properties.Resources.Button_ADD_Red__1___1_;
            this.btn_add.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.btn_add.Location = new System.Drawing.Point(238, 103);
            this.btn_add.Name = "btn_add";
            this.btn_add.Size = new System.Drawing.Size(175, 50);
            this.btn_add.TabIndex = 184;
            this.btn_add.Text = "ADD";
            this.btn_add.TextColor = System.Drawing.Color.FromArgb(((int)(((byte)(192)))), ((int)(((byte)(0)))), ((int)(((byte)(0)))));
            this.btn_add.UseVisualStyleBackColor = false;
            this.btn_add.Click += new System.EventHandler(this.btn_add_Click);
            this.btn_add.MouseEnter += new System.EventHandler(this.btn_add_MouseEnter);
            this.btn_add.MouseLeave += new System.EventHandler(this.btn_add_MouseLeave);
            this.btn_add.MouseHover += new System.EventHandler(this.btn_add_MouseHover);
            // 
            // btn_update
            // 
            this.btn_update.BackColor = System.Drawing.Color.Transparent;
            this.btn_update.BackgroundColor = System.Drawing.Color.Transparent;
            this.btn_update.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(192)))), ((int)(((byte)(0)))), ((int)(((byte)(0)))));
            this.btn_update.BorderRadius = 8;
            this.btn_update.BorderSize = 1;
            this.btn_update.Enabled = false;
            this.btn_update.FlatAppearance.BorderSize = 0;
            this.btn_update.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btn_update.Font = new System.Drawing.Font("Microsoft Sans Serif", 8F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btn_update.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(192)))), ((int)(((byte)(0)))), ((int)(((byte)(0)))));
            this.btn_update.Image = global::Payroll.Properties.Resources.Button_UPDATE_Red__4___1___1_;
            this.btn_update.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.btn_update.Location = new System.Drawing.Point(443, 105);
            this.btn_update.Name = "btn_update";
            this.btn_update.Size = new System.Drawing.Size(175, 50);
            this.btn_update.TabIndex = 185;
            this.btn_update.Text = "EDIT";
            this.btn_update.TextColor = System.Drawing.Color.FromArgb(((int)(((byte)(192)))), ((int)(((byte)(0)))), ((int)(((byte)(0)))));
            this.btn_update.UseVisualStyleBackColor = false;
            this.btn_update.Click += new System.EventHandler(this.btn_update_Click);
            this.btn_update.MouseEnter += new System.EventHandler(this.btn_update_MouseEnter);
            this.btn_update.MouseLeave += new System.EventHandler(this.btn_update_MouseLeave);
            this.btn_update.MouseHover += new System.EventHandler(this.btn_update_MouseHover);
            // 
            // btn_delete
            // 
            this.btn_delete.BackColor = System.Drawing.Color.Transparent;
            this.btn_delete.BackgroundColor = System.Drawing.Color.Transparent;
            this.btn_delete.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(192)))), ((int)(((byte)(0)))), ((int)(((byte)(0)))));
            this.btn_delete.BorderRadius = 8;
            this.btn_delete.BorderSize = 1;
            this.btn_delete.Enabled = false;
            this.btn_delete.FlatAppearance.BorderSize = 0;
            this.btn_delete.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btn_delete.Font = new System.Drawing.Font("Microsoft Sans Serif", 8F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btn_delete.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(192)))), ((int)(((byte)(0)))), ((int)(((byte)(0)))));
            this.btn_delete.Image = global::Payroll.Properties.Resources.icons8_delete_30;
            this.btn_delete.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.btn_delete.Location = new System.Drawing.Point(638, 106);
            this.btn_delete.Name = "btn_delete";
            this.btn_delete.Size = new System.Drawing.Size(175, 50);
            this.btn_delete.TabIndex = 187;
            this.btn_delete.Text = "DELETE";
            this.btn_delete.TextColor = System.Drawing.Color.FromArgb(((int)(((byte)(192)))), ((int)(((byte)(0)))), ((int)(((byte)(0)))));
            this.btn_delete.UseVisualStyleBackColor = false;
            this.btn_delete.Click += new System.EventHandler(this.btn_Delete_Click);
            this.btn_delete.MouseEnter += new System.EventHandler(this.btn_delete_MouseEnter);
            this.btn_delete.MouseLeave += new System.EventHandler(this.btn_delete_MouseLeave);
            this.btn_delete.MouseHover += new System.EventHandler(this.btn_delete_MouseHover);
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Font = new System.Drawing.Font("Arial", 9.75F);
            this.label6.Location = new System.Drawing.Point(868, 32);
            this.label6.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(55, 23);
            this.label6.TabIndex = 85;
            this.label6.Text = "OT-4";
            // 
            // tb_ot_4
            // 
            this.tb_ot_4.Enabled = false;
            this.tb_ot_4.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.tb_ot_4.ForeColor = System.Drawing.Color.Black;
            this.tb_ot_4.Location = new System.Drawing.Point(872, 61);
            this.tb_ot_4.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.tb_ot_4.Name = "tb_ot_4";
            this.tb_ot_4.Size = new System.Drawing.Size(138, 28);
            this.tb_ot_4.TabIndex = 84;
            this.tb_ot_4.Text = "0.00";
            this.tb_ot_4.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.tb_ot_4_KeyPress);
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Font = new System.Drawing.Font("Arial", 9.75F);
            this.label5.Location = new System.Drawing.Point(722, 32);
            this.label5.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(55, 23);
            this.label5.TabIndex = 83;
            this.label5.Text = "OT-3";
            // 
            // tb_ot_3
            // 
            this.tb_ot_3.Enabled = false;
            this.tb_ot_3.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.tb_ot_3.ForeColor = System.Drawing.Color.Black;
            this.tb_ot_3.Location = new System.Drawing.Point(726, 61);
            this.tb_ot_3.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.tb_ot_3.Name = "tb_ot_3";
            this.tb_ot_3.Size = new System.Drawing.Size(138, 28);
            this.tb_ot_3.TabIndex = 82;
            this.tb_ot_3.Text = "0.00";
            this.tb_ot_3.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.tb_ot_3_KeyPress);
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Font = new System.Drawing.Font("Arial", 9.75F);
            this.label2.Location = new System.Drawing.Point(236, 28);
            this.label2.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(142, 23);
            this.label2.TabIndex = 69;
            this.label2.Text = "OT Description";
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Font = new System.Drawing.Font("Arial", 9.75F);
            this.label4.Location = new System.Drawing.Point(605, 32);
            this.label4.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(55, 23);
            this.label4.TabIndex = 71;
            this.label4.Text = "OT-2";
            // 
            // tb_ot_1
            // 
            this.tb_ot_1.Enabled = false;
            this.tb_ot_1.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.tb_ot_1.ForeColor = System.Drawing.Color.Black;
            this.tb_ot_1.Location = new System.Drawing.Point(485, 61);
            this.tb_ot_1.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.tb_ot_1.Name = "tb_ot_1";
            this.tb_ot_1.Size = new System.Drawing.Size(107, 28);
            this.tb_ot_1.TabIndex = 66;
            this.tb_ot_1.Text = "0.00";
            this.tb_ot_1.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.tb_ot_1_KeyPress);
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Font = new System.Drawing.Font("Arial", 9.75F);
            this.label3.Location = new System.Drawing.Point(489, 28);
            this.label3.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(55, 23);
            this.label3.TabIndex = 70;
            this.label3.Text = "OT-1";
            // 
            // tb_ot_code
            // 
            this.tb_ot_code.Enabled = false;
            this.tb_ot_code.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.tb_ot_code.ForeColor = System.Drawing.Color.Black;
            this.tb_ot_code.Location = new System.Drawing.Point(65, 63);
            this.tb_ot_code.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.tb_ot_code.Name = "tb_ot_code";
            this.tb_ot_code.Size = new System.Drawing.Size(157, 28);
            this.tb_ot_code.TabIndex = 64;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("Arial", 9.75F);
            this.label1.Location = new System.Drawing.Point(60, 32);
            this.label1.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(91, 23);
            this.label1.TabIndex = 68;
            this.label1.Text = "OT Code";
            // 
            // tb_ot_2
            // 
            this.tb_ot_2.Enabled = false;
            this.tb_ot_2.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.tb_ot_2.ForeColor = System.Drawing.Color.Black;
            this.tb_ot_2.Location = new System.Drawing.Point(601, 61);
            this.tb_ot_2.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.tb_ot_2.Name = "tb_ot_2";
            this.tb_ot_2.Size = new System.Drawing.Size(112, 28);
            this.tb_ot_2.TabIndex = 67;
            this.tb_ot_2.Text = "0.00";
            this.tb_ot_2.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.tb_ot_2_KeyPress);
            // 
            // tb_ot_desc
            // 
            this.tb_ot_desc.Enabled = false;
            this.tb_ot_desc.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.tb_ot_desc.ForeColor = System.Drawing.Color.Black;
            this.tb_ot_desc.Location = new System.Drawing.Point(240, 60);
            this.tb_ot_desc.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.tb_ot_desc.Name = "tb_ot_desc";
            this.tb_ot_desc.Size = new System.Drawing.Size(228, 28);
            this.tb_ot_desc.TabIndex = 65;
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Location = new System.Drawing.Point(12, 189);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(18, 20);
            this.label7.TabIndex = 86;
            this.label7.Text = "0";
            this.label7.Visible = false;
            // 
            // btn_export
            // 
            this.btn_export.BackColor = System.Drawing.Color.Transparent;
            this.btn_export.BackgroundColor = System.Drawing.Color.Transparent;
            this.btn_export.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(192)))), ((int)(((byte)(0)))), ((int)(((byte)(0)))));
            this.btn_export.BorderRadius = 8;
            this.btn_export.BorderSize = 1;
            this.btn_export.FlatAppearance.BorderSize = 0;
            this.btn_export.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btn_export.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btn_export.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(192)))), ((int)(((byte)(0)))), ((int)(((byte)(0)))));
            this.btn_export.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.btn_export.Location = new System.Drawing.Point(839, 630);
            this.btn_export.Name = "btn_export";
            this.btn_export.Size = new System.Drawing.Size(219, 44);
            this.btn_export.TabIndex = 186;
            this.btn_export.Text = "Export to Excel";
            this.btn_export.TextColor = System.Drawing.Color.FromArgb(((int)(((byte)(192)))), ((int)(((byte)(0)))), ((int)(((byte)(0)))));
            this.btn_export.UseVisualStyleBackColor = false;
            this.btn_export.Click += new System.EventHandler(this.btn_export_Click);
            // 
            // Overtime
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(9F, 20F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1067, 683);
            this.Controls.Add(this.btn_export);
            this.Controls.Add(this.label7);
            this.Controls.Add(this.groupBox1);
            this.Controls.Add(this.tb_search);
            this.Controls.Add(this.label13);
            this.Controls.Add(this.lv_Overtime);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedToolWindow;
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.MaximizeBox = false;
            this.Name = "Overtime";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Overtime Table";
            this.Load += new System.EventHandler(this.Overtime_Load);
            this.groupBox1.ResumeLayout(false);
            this.groupBox1.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion
        private System.Windows.Forms.TextBox tb_search;
        private System.Windows.Forms.Label label13;
        private System.Windows.Forms.ListView lv_Overtime;
        private System.Windows.Forms.GroupBox groupBox1;
        private System.Windows.Forms.Label label6;
        internal System.Windows.Forms.TextBox tb_ot_4;
        private System.Windows.Forms.Label label5;
        internal System.Windows.Forms.TextBox tb_ot_3;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label4;
        internal System.Windows.Forms.TextBox tb_ot_1;
        private System.Windows.Forms.Label label3;
        internal System.Windows.Forms.TextBox tb_ot_code;
        private System.Windows.Forms.Label label1;
        internal System.Windows.Forms.TextBox tb_ot_2;
        internal System.Windows.Forms.TextBox tb_ot_desc;
        private System.Windows.Forms.Label label7;
        private RJButtons.RJButton btn_delete;
        private RJButtons.RJButton btn_update;
        private RJButtons.RJButton btn_add;
        private RJButtons.RJButton btn_export;
    }
}