﻿using CrystalDecisions.CrystalReports.Engine;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Payroll
{
    public partial class Payroll_Details_Report : Form
    {

        ReportDocument report = new ReportDocument();
        connect con = new connect();

        public Payroll_Details_Report()
        {
            InitializeComponent();
        }

        private void Payroll_Details_Report_Load(object sender, EventArgs e)
        {
            report.Load(@"C:\Temp\Payroll\Payroll\Reports\Payroll_Register.rpt");
            DataSet ds = new DataSet();
            con.dataget("select * from payslip");
            con.mda.Fill(ds, "payslip");
            report.SetDataSource(ds);
            crystalReportViewer1.ReportSource = report;
        }
    }
}
