﻿namespace Payroll
{
    partial class Homepage
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(Homepage));
            this.timer1 = new System.Windows.Forms.Timer(this.components);
            this.menuStrip1 = new System.Windows.Forms.MenuStrip();
            this.MaintenanceMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.SystemSetupItem = new System.Windows.Forms.ToolStripMenuItem();
            this.AuditLogItem = new System.Windows.Forms.ToolStripMenuItem();
            this.OvertimeTableItem = new System.Windows.Forms.ToolStripMenuItem();
            this.DepartmentItem = new System.Windows.Forms.ToolStripMenuItem();
            this.TaxTableItem = new System.Windows.Forms.ToolStripMenuItem();
            this.SSSTableItem = new System.Windows.Forms.ToolStripMenuItem();
            this.PhilhealthTableItem = new System.Windows.Forms.ToolStripMenuItem();
            this.HDMFTableItem = new System.Windows.Forms.ToolStripMenuItem();
            this.EmployeeMenuListItem = new System.Windows.Forms.ToolStripMenuItem();
            this.EmployeeMasterFileItem = new System.Windows.Forms.ToolStripMenuItem();
            this.PayrolTransactionMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.PostingTransactionItem = new System.Windows.Forms.ToolStripMenuItem();
            this.PayrollTransactionItem = new System.Windows.Forms.ToolStripMenuItem();
            this.PayrollProcessingItem = new System.Windows.Forms.ToolStripMenuItem();
            this.BonusProcessItem = new System.Windows.Forms.ToolStripMenuItem();
            this.BonusRegisterItem = new System.Windows.Forms.ToolStripMenuItem();
            this.ReportsMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.PayslipReportItem = new System.Windows.Forms.ToolStripMenuItem();
            this.PayrollRegisterItem = new System.Windows.Forms.ToolStripMenuItem();
            this.RegisterMonthlyReportItem = new System.Windows.Forms.ToolStripMenuItem();
            this.BankRemittanceReportItem = new System.Windows.Forms.ToolStripMenuItem();
            this.BonusRemittanceItem = new System.Windows.Forms.ToolStripMenuItem();
            this.YearEndProcessItem = new System.Windows.Forms.ToolStripMenuItem();
            this.BIRAphalistItem = new System.Windows.Forms.ToolStripMenuItem();
            this.lbl_time = new System.Windows.Forms.Label();
            this.lbl_date = new System.Windows.Forms.Label();
            this.panel1 = new System.Windows.Forms.Panel();
            this.panel2 = new System.Windows.Forms.Panel();
            this.panel3 = new System.Windows.Forms.Panel();
            this.panel4 = new System.Windows.Forms.Panel();
            this.panel5 = new System.Windows.Forms.Panel();
            this.lbl_logout = new System.Windows.Forms.Label();
            this.pictureBox1 = new System.Windows.Forms.PictureBox();
            this.panel6 = new System.Windows.Forms.Panel();
            this.lbl_UserType = new System.Windows.Forms.Label();
            this.lbl_CompName = new System.Windows.Forms.Label();
            this.lbl_user = new System.Windows.Forms.Label();
            this.label8 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.panel7 = new System.Windows.Forms.Panel();
            this.panel8 = new System.Windows.Forms.Panel();
            this.label5 = new System.Windows.Forms.Label();
            this.pictureBox2 = new System.Windows.Forms.PictureBox();
            this.toolStrip1 = new System.Windows.Forms.ToolStrip();
            this.menuStrip1.SuspendLayout();
            this.panel5.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).BeginInit();
            this.panel6.SuspendLayout();
            this.panel7.SuspendLayout();
            this.panel8.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox2)).BeginInit();
            this.SuspendLayout();
            // 
            // timer1
            // 
            this.timer1.Enabled = true;
            this.timer1.Tick += new System.EventHandler(this.timer1_Tick);
            // 
            // menuStrip1
            // 
            this.menuStrip1.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(192)))), ((int)(((byte)(0)))), ((int)(((byte)(0)))));
            this.menuStrip1.Font = new System.Drawing.Font("Segoe UI", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.menuStrip1.GripMargin = new System.Windows.Forms.Padding(2, 2, 0, 2);
            this.menuStrip1.ImageScalingSize = new System.Drawing.Size(24, 24);
            this.menuStrip1.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.MaintenanceMenuItem,
            this.EmployeeMenuListItem,
            this.PayrolTransactionMenuItem,
            this.ReportsMenuItem});
            this.menuStrip1.Location = new System.Drawing.Point(4, 105);
            this.menuStrip1.Name = "menuStrip1";
            this.menuStrip1.Size = new System.Drawing.Size(1916, 45);
            this.menuStrip1.TabIndex = 8;
            this.menuStrip1.Text = "menuStrip1";
            // 
            // MaintenanceMenuItem
            // 
            this.MaintenanceMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.SystemSetupItem,
            this.AuditLogItem,
            this.OvertimeTableItem,
            this.DepartmentItem,
            this.TaxTableItem,
            this.SSSTableItem,
            this.PhilhealthTableItem,
            this.HDMFTableItem});
            this.MaintenanceMenuItem.Font = new System.Drawing.Font("Segoe UI Semibold", 13.2F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.MaintenanceMenuItem.ForeColor = System.Drawing.Color.White;
            this.MaintenanceMenuItem.Name = "MaintenanceMenuItem";
            this.MaintenanceMenuItem.Size = new System.Drawing.Size(274, 41);
            this.MaintenanceMenuItem.Text = "Setup/Maintenance";
            // 
            // SystemSetupItem
            // 
            this.SystemSetupItem.Margin = new System.Windows.Forms.Padding(0, 5, 0, 0);
            this.SystemSetupItem.Name = "SystemSetupItem";
            this.SystemSetupItem.Size = new System.Drawing.Size(317, 46);
            this.SystemSetupItem.Text = "System Setup";
            this.SystemSetupItem.Click += new System.EventHandler(this.companyUsersToolStripMenuItem_Click);
            this.SystemSetupItem.MouseEnter += new System.EventHandler(this.SystemSetupItem_MouseEnter);
            this.SystemSetupItem.MouseLeave += new System.EventHandler(this.SystemSetupItem_MouseLeave);
            this.SystemSetupItem.MouseHover += new System.EventHandler(this.SystemSetupItem_MouseHover);
            this.SystemSetupItem.Paint += new System.Windows.Forms.PaintEventHandler(this.SystemSetupItem_Paint);
            // 
            // AuditLogItem
            // 
            this.AuditLogItem.BackColor = System.Drawing.Color.White;
            this.AuditLogItem.ForeColor = System.Drawing.Color.Black;
            this.AuditLogItem.Name = "AuditLogItem";
            this.AuditLogItem.Size = new System.Drawing.Size(317, 46);
            this.AuditLogItem.Text = "Audit Log";
            this.AuditLogItem.Click += new System.EventHandler(this.userManagementToolStripMenuItem_Click);
            this.AuditLogItem.MouseEnter += new System.EventHandler(this.AuditLogItem_MouseEnter);
            this.AuditLogItem.MouseLeave += new System.EventHandler(this.AuditLogItem_MouseLeave);
            this.AuditLogItem.MouseHover += new System.EventHandler(this.AuditLogItem_MouseHover);
            this.AuditLogItem.Paint += new System.Windows.Forms.PaintEventHandler(this.AuditLogItem_Paint);
            // 
            // OvertimeTableItem
            // 
            this.OvertimeTableItem.BackColor = System.Drawing.Color.White;
            this.OvertimeTableItem.ForeColor = System.Drawing.Color.Black;
            this.OvertimeTableItem.Name = "OvertimeTableItem";
            this.OvertimeTableItem.Size = new System.Drawing.Size(317, 46);
            this.OvertimeTableItem.Text = "Overtime Table";
            this.OvertimeTableItem.Click += new System.EventHandler(this.overtimeToolStripMenuItem_Click);
            this.OvertimeTableItem.MouseEnter += new System.EventHandler(this.OvertimeTableItem_MouseEnter);
            this.OvertimeTableItem.MouseLeave += new System.EventHandler(this.OvertimeTableItem_MouseLeave);
            this.OvertimeTableItem.MouseHover += new System.EventHandler(this.OvertimeTableItem_MouseHover);
            this.OvertimeTableItem.Paint += new System.Windows.Forms.PaintEventHandler(this.OvertimeTableItem_Paint);
            // 
            // DepartmentItem
            // 
            this.DepartmentItem.BackColor = System.Drawing.Color.White;
            this.DepartmentItem.ForeColor = System.Drawing.Color.Black;
            this.DepartmentItem.Name = "DepartmentItem";
            this.DepartmentItem.Size = new System.Drawing.Size(317, 46);
            this.DepartmentItem.Text = "Department";
            this.DepartmentItem.Click += new System.EventHandler(this.departmentToolStripMenuItem_Click);
            this.DepartmentItem.MouseEnter += new System.EventHandler(this.DepartmentItem_MouseEnter);
            this.DepartmentItem.MouseLeave += new System.EventHandler(this.DepartmentItem_MouseLeave);
            this.DepartmentItem.MouseHover += new System.EventHandler(this.DepartmentItem_MouseHover);
            this.DepartmentItem.Paint += new System.Windows.Forms.PaintEventHandler(this.DepartmentItem_Paint);
            // 
            // TaxTableItem
            // 
            this.TaxTableItem.BackColor = System.Drawing.Color.White;
            this.TaxTableItem.ForeColor = System.Drawing.Color.Black;
            this.TaxTableItem.Name = "TaxTableItem";
            this.TaxTableItem.Size = new System.Drawing.Size(317, 46);
            this.TaxTableItem.Text = "Tax Table";
            this.TaxTableItem.Click += new System.EventHandler(this.taxTableToolStripMenuItem_Click);
            this.TaxTableItem.MouseEnter += new System.EventHandler(this.TaxTableItem_MouseEnter);
            this.TaxTableItem.MouseLeave += new System.EventHandler(this.TaxTableItem_MouseLeave);
            this.TaxTableItem.MouseHover += new System.EventHandler(this.TaxTableItem_MouseHover);
            this.TaxTableItem.Paint += new System.Windows.Forms.PaintEventHandler(this.TaxTableItem_Paint);
            // 
            // SSSTableItem
            // 
            this.SSSTableItem.BackColor = System.Drawing.Color.White;
            this.SSSTableItem.ForeColor = System.Drawing.Color.Black;
            this.SSSTableItem.Name = "SSSTableItem";
            this.SSSTableItem.Size = new System.Drawing.Size(317, 46);
            this.SSSTableItem.Text = "SSS Table";
            this.SSSTableItem.Click += new System.EventHandler(this.sSSTableToolStripMenuItem_Click);
            this.SSSTableItem.MouseDown += new System.Windows.Forms.MouseEventHandler(this.SSSTableItem_MouseDown);
            this.SSSTableItem.MouseEnter += new System.EventHandler(this.SSSTableItem_MouseEnter);
            this.SSSTableItem.MouseLeave += new System.EventHandler(this.SSSTableItem_MouseLeave);
            this.SSSTableItem.MouseHover += new System.EventHandler(this.SSSTableItem_MouseHover);
            this.SSSTableItem.MouseMove += new System.Windows.Forms.MouseEventHandler(this.SSSTableItem_MouseMove);
            this.SSSTableItem.MouseUp += new System.Windows.Forms.MouseEventHandler(this.SSSTableItem_MouseUp);
            this.SSSTableItem.Paint += new System.Windows.Forms.PaintEventHandler(this.SSSTableItem_Paint);
            // 
            // PhilhealthTableItem
            // 
            this.PhilhealthTableItem.BackColor = System.Drawing.Color.White;
            this.PhilhealthTableItem.ForeColor = System.Drawing.Color.Black;
            this.PhilhealthTableItem.Name = "PhilhealthTableItem";
            this.PhilhealthTableItem.Size = new System.Drawing.Size(317, 46);
            this.PhilhealthTableItem.Text = "Philhealth Table";
            this.PhilhealthTableItem.Click += new System.EventHandler(this.philhealthTableToolStripMenuItem_Click);
            this.PhilhealthTableItem.MouseEnter += new System.EventHandler(this.PhilhealthTableItem_MouseEnter);
            this.PhilhealthTableItem.MouseLeave += new System.EventHandler(this.PhilhealthTableItem_MouseLeave);
            this.PhilhealthTableItem.MouseHover += new System.EventHandler(this.PhilhealthTableItem_MouseHover);
            this.PhilhealthTableItem.Paint += new System.Windows.Forms.PaintEventHandler(this.PhilhealthTableItem_Paint);
            // 
            // HDMFTableItem
            // 
            this.HDMFTableItem.BackColor = System.Drawing.Color.White;
            this.HDMFTableItem.ForeColor = System.Drawing.Color.Black;
            this.HDMFTableItem.Name = "HDMFTableItem";
            this.HDMFTableItem.Size = new System.Drawing.Size(317, 46);
            this.HDMFTableItem.Text = "HDMF Table";
            this.HDMFTableItem.Click += new System.EventHandler(this.pagIbigHDMFToolStripMenuItem_Click);
            this.HDMFTableItem.MouseEnter += new System.EventHandler(this.HDMFTableItem_MouseEnter);
            this.HDMFTableItem.MouseLeave += new System.EventHandler(this.HDMFTableItem_MouseLeave);
            this.HDMFTableItem.MouseHover += new System.EventHandler(this.HDMFTableItem_MouseHover);
            this.HDMFTableItem.Paint += new System.Windows.Forms.PaintEventHandler(this.HDMFTableItem_Paint);
            // 
            // EmployeeMenuListItem
            // 
            this.EmployeeMenuListItem.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(192)))), ((int)(((byte)(0)))), ((int)(((byte)(0)))));
            this.EmployeeMenuListItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.EmployeeMasterFileItem});
            this.EmployeeMenuListItem.Font = new System.Drawing.Font("Segoe UI Semibold", 13.2F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.EmployeeMenuListItem.ForeColor = System.Drawing.Color.White;
            this.EmployeeMenuListItem.Name = "EmployeeMenuListItem";
            this.EmployeeMenuListItem.Size = new System.Drawing.Size(201, 41);
            this.EmployeeMenuListItem.Text = "Employee List";
            // 
            // EmployeeMasterFileItem
            // 
            this.EmployeeMasterFileItem.Margin = new System.Windows.Forms.Padding(0, 5, 0, 0);
            this.EmployeeMasterFileItem.Name = "EmployeeMasterFileItem";
            this.EmployeeMasterFileItem.Size = new System.Drawing.Size(384, 46);
            this.EmployeeMasterFileItem.Text = "Employee Master File";
            this.EmployeeMasterFileItem.Click += new System.EventHandler(this.EmployeeMasterFileItem_Click);
            this.EmployeeMasterFileItem.MouseEnter += new System.EventHandler(this.EmployeeMasterFileItem_MouseEnter);
            this.EmployeeMasterFileItem.MouseLeave += new System.EventHandler(this.EmployeeMasterFileItem_MouseLeave);
            this.EmployeeMasterFileItem.MouseHover += new System.EventHandler(this.EmployeeMasterFileItem_MouseHover);
            this.EmployeeMasterFileItem.Paint += new System.Windows.Forms.PaintEventHandler(this.EmployeeMasterFileItem_Paint);
            // 
            // PayrolTransactionMenuItem
            // 
            this.PayrolTransactionMenuItem.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(192)))), ((int)(((byte)(0)))), ((int)(((byte)(0)))));
            this.PayrolTransactionMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.PostingTransactionItem,
            this.PayrollTransactionItem,
            this.PayrollProcessingItem,
            this.BonusProcessItem,
            this.BonusRegisterItem});
            this.PayrolTransactionMenuItem.Font = new System.Drawing.Font("Segoe UI Semibold", 13.2F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.PayrolTransactionMenuItem.ForeColor = System.Drawing.Color.White;
            this.PayrolTransactionMenuItem.Name = "PayrolTransactionMenuItem";
            this.PayrolTransactionMenuItem.Size = new System.Drawing.Size(264, 41);
            this.PayrolTransactionMenuItem.Text = "Payroll Transaction";
            // 
            // PostingTransactionItem
            // 
            this.PostingTransactionItem.Margin = new System.Windows.Forms.Padding(0, 5, 0, 0);
            this.PostingTransactionItem.Name = "PostingTransactionItem";
            this.PostingTransactionItem.Size = new System.Drawing.Size(390, 46);
            this.PostingTransactionItem.Text = "Posting Transaction";
            this.PostingTransactionItem.Click += new System.EventHandler(this.postingTransactionToolStripMenuItem1_Click);
            this.PostingTransactionItem.MouseEnter += new System.EventHandler(this.PostingTransactionItem_MouseEnter);
            this.PostingTransactionItem.MouseLeave += new System.EventHandler(this.PostingTransactionItem_MouseLeave);
            this.PostingTransactionItem.MouseHover += new System.EventHandler(this.PostingTransactionItem_MouseHover);
            this.PostingTransactionItem.Paint += new System.Windows.Forms.PaintEventHandler(this.PostingTransactionItem_Paint);
            // 
            // PayrollTransactionItem
            // 
            this.PayrollTransactionItem.Name = "PayrollTransactionItem";
            this.PayrollTransactionItem.Size = new System.Drawing.Size(390, 46);
            this.PayrollTransactionItem.Text = "Payroll Transaction";
            this.PayrollTransactionItem.Click += new System.EventHandler(this.payrollTransactionToolStripMenuItem_Click);
            this.PayrollTransactionItem.MouseEnter += new System.EventHandler(this.PayrollTransactionItem_MouseEnter);
            this.PayrollTransactionItem.MouseLeave += new System.EventHandler(this.PayrollTransactionItem_MouseLeave);
            this.PayrollTransactionItem.MouseHover += new System.EventHandler(this.PayrollTransactionItem_MouseHover);
            this.PayrollTransactionItem.Paint += new System.Windows.Forms.PaintEventHandler(this.PayrollTransactionItem_Paint);
            // 
            // PayrollProcessingItem
            // 
            this.PayrollProcessingItem.Name = "PayrollProcessingItem";
            this.PayrollProcessingItem.Size = new System.Drawing.Size(390, 46);
            this.PayrollProcessingItem.Text = "Payroll Processing";
            this.PayrollProcessingItem.Click += new System.EventHandler(this.payrollProcessingToolStripMenuItem_Click);
            this.PayrollProcessingItem.MouseEnter += new System.EventHandler(this.PayrollProcessingItem_MouseEnter);
            this.PayrollProcessingItem.MouseLeave += new System.EventHandler(this.PayrollProcessingItem_MouseLeave);
            this.PayrollProcessingItem.MouseHover += new System.EventHandler(this.PayrollProcessingItem_MouseHover);
            this.PayrollProcessingItem.Paint += new System.Windows.Forms.PaintEventHandler(this.PayrollProcessingItem_Paint);
            // 
            // BonusProcessItem
            // 
            this.BonusProcessItem.Name = "BonusProcessItem";
            this.BonusProcessItem.Size = new System.Drawing.Size(390, 46);
            this.BonusProcessItem.Text = "Month Bonus Process";
            this.BonusProcessItem.Click += new System.EventHandler(this.monToolStripMenuItem_Click);
            this.BonusProcessItem.MouseEnter += new System.EventHandler(this.BonusProcessItem_MouseEnter);
            this.BonusProcessItem.MouseLeave += new System.EventHandler(this.BonusProcessItem_MouseLeave);
            this.BonusProcessItem.MouseHover += new System.EventHandler(this.BonusProcessItem_MouseHover);
            this.BonusProcessItem.Paint += new System.Windows.Forms.PaintEventHandler(this.BonusProcessItem_Paint);
            // 
            // BonusRegisterItem
            // 
            this.BonusRegisterItem.Name = "BonusRegisterItem";
            this.BonusRegisterItem.Size = new System.Drawing.Size(390, 46);
            this.BonusRegisterItem.Text = "Bonus Register";
            this.BonusRegisterItem.Click += new System.EventHandler(this.bonusRegisterToolStripMenuItem_Click);
            this.BonusRegisterItem.MouseEnter += new System.EventHandler(this.BonusRegisterItem_MouseEnter);
            this.BonusRegisterItem.MouseLeave += new System.EventHandler(this.BonusRegisterItem_MouseLeave);
            this.BonusRegisterItem.MouseHover += new System.EventHandler(this.BonusRegisterItem_MouseHover);
            this.BonusRegisterItem.Paint += new System.Windows.Forms.PaintEventHandler(this.BonusRegisterItem_Paint);
            // 
            // ReportsMenuItem
            // 
            this.ReportsMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.PayslipReportItem,
            this.PayrollRegisterItem,
            this.RegisterMonthlyReportItem,
            this.BankRemittanceReportItem,
            this.BonusRemittanceItem,
            this.YearEndProcessItem,
            this.BIRAphalistItem});
            this.ReportsMenuItem.Font = new System.Drawing.Font("Segoe UI Semibold", 13.2F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.ReportsMenuItem.ForeColor = System.Drawing.Color.White;
            this.ReportsMenuItem.Name = "ReportsMenuItem";
            this.ReportsMenuItem.Size = new System.Drawing.Size(128, 41);
            this.ReportsMenuItem.Text = "Reports";
            // 
            // PayslipReportItem
            // 
            this.PayslipReportItem.Margin = new System.Windows.Forms.Padding(0, 5, 0, 0);
            this.PayslipReportItem.Name = "PayslipReportItem";
            this.PayslipReportItem.Size = new System.Drawing.Size(395, 46);
            this.PayslipReportItem.Text = "Payslip";
            this.PayslipReportItem.Click += new System.EventHandler(this.payslipToolStripMenuItem1_Click);
            this.PayslipReportItem.MouseEnter += new System.EventHandler(this.PayslipReportItem_MouseEnter);
            this.PayslipReportItem.MouseLeave += new System.EventHandler(this.PayslipReportItem_MouseLeave);
            this.PayslipReportItem.MouseHover += new System.EventHandler(this.PayslipReportItem_MouseHover);
            this.PayslipReportItem.Paint += new System.Windows.Forms.PaintEventHandler(this.PayslipReportItem_Paint);
            // 
            // PayrollRegisterItem
            // 
            this.PayrollRegisterItem.Name = "PayrollRegisterItem";
            this.PayrollRegisterItem.Size = new System.Drawing.Size(395, 46);
            this.PayrollRegisterItem.Text = "Payroll Register";
            this.PayrollRegisterItem.Click += new System.EventHandler(this.payrollRegisterToolStripMenuItem_Click);
            this.PayrollRegisterItem.MouseEnter += new System.EventHandler(this.PayrollRegisterItem_MouseEnter);
            this.PayrollRegisterItem.MouseLeave += new System.EventHandler(this.PayrollRegisterItem_MouseLeave);
            this.PayrollRegisterItem.MouseHover += new System.EventHandler(this.PayrollRegisterItem_MouseHover);
            this.PayrollRegisterItem.Paint += new System.Windows.Forms.PaintEventHandler(this.PayrollRegisterItem_Paint);
            // 
            // RegisterMonthlyReportItem
            // 
            this.RegisterMonthlyReportItem.Name = "RegisterMonthlyReportItem";
            this.RegisterMonthlyReportItem.Size = new System.Drawing.Size(395, 46);
            this.RegisterMonthlyReportItem.Text = "Register Monthly";
            this.RegisterMonthlyReportItem.MouseEnter += new System.EventHandler(this.RegisterMonthlyReportItem_MouseEnter);
            this.RegisterMonthlyReportItem.MouseLeave += new System.EventHandler(this.RegisterMonthlyReportItem_MouseLeave);
            this.RegisterMonthlyReportItem.MouseHover += new System.EventHandler(this.RegisterMonthlyReportItem_MouseHover);
            this.RegisterMonthlyReportItem.Paint += new System.Windows.Forms.PaintEventHandler(this.RegisterMonthlyReportItem_Paint);
            // 
            // BankRemittanceReportItem
            // 
            this.BankRemittanceReportItem.Name = "BankRemittanceReportItem";
            this.BankRemittanceReportItem.Size = new System.Drawing.Size(395, 46);
            this.BankRemittanceReportItem.Text = "Bank Remittance";
            this.BankRemittanceReportItem.Click += new System.EventHandler(this.BankRemittanceReportItem_Click);
            this.BankRemittanceReportItem.MouseEnter += new System.EventHandler(this.BankRemittanceReportItem_MouseEnter);
            this.BankRemittanceReportItem.MouseLeave += new System.EventHandler(this.BankRemittanceReportItem_MouseLeave);
            this.BankRemittanceReportItem.MouseHover += new System.EventHandler(this.BankRemittanceReportItem_MouseHover);
            this.BankRemittanceReportItem.Paint += new System.Windows.Forms.PaintEventHandler(this.BankRemittanceReportItem_Paint);
            // 
            // BonusRemittanceItem
            // 
            this.BonusRemittanceItem.Name = "BonusRemittanceItem";
            this.BonusRemittanceItem.Size = new System.Drawing.Size(395, 46);
            this.BonusRemittanceItem.Text = "Bonus Remittance";
            this.BonusRemittanceItem.Click += new System.EventHandler(this.BonusRemittanceItem_Click);
            this.BonusRemittanceItem.MouseEnter += new System.EventHandler(this.BonusRemittanceItem_MouseEnter);
            this.BonusRemittanceItem.MouseLeave += new System.EventHandler(this.BonusRemittanceItem_MouseLeave);
            this.BonusRemittanceItem.MouseHover += new System.EventHandler(this.BonusRemittanceItem_MouseHover);
            this.BonusRemittanceItem.Paint += new System.Windows.Forms.PaintEventHandler(this.BonusRemittanceItem_Paint);
            // 
            // YearEndProcessItem
            // 
            this.YearEndProcessItem.Name = "YearEndProcessItem";
            this.YearEndProcessItem.Size = new System.Drawing.Size(395, 46);
            this.YearEndProcessItem.Text = "Year End Process";
            this.YearEndProcessItem.MouseEnter += new System.EventHandler(this.YearEndProcessItem_MouseEnter);
            this.YearEndProcessItem.MouseLeave += new System.EventHandler(this.YearEndProcessItem_MouseLeave);
            this.YearEndProcessItem.MouseHover += new System.EventHandler(this.YearEndProcessItem_MouseHover);
            this.YearEndProcessItem.Paint += new System.Windows.Forms.PaintEventHandler(this.YearEndProcessItem_Paint);
            // 
            // BIRAphalistItem
            // 
            this.BIRAphalistItem.Name = "BIRAphalistItem";
            this.BIRAphalistItem.Size = new System.Drawing.Size(395, 46);
            this.BIRAphalistItem.Text = "BIR Alphalist and 2316";
            this.BIRAphalistItem.Click += new System.EventHandler(this.BIRAphalistItem_Click);
            this.BIRAphalistItem.MouseEnter += new System.EventHandler(this.BIRAphalistItem_MouseEnter);
            this.BIRAphalistItem.MouseLeave += new System.EventHandler(this.BIRAphalistItem_MouseLeave);
            this.BIRAphalistItem.MouseHover += new System.EventHandler(this.BIRAphalistItem_MouseHover);
            this.BIRAphalistItem.Paint += new System.Windows.Forms.PaintEventHandler(this.BIRAphalistItem_Paint);
            // 
            // lbl_time
            // 
            this.lbl_time.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.lbl_time.AutoSize = true;
            this.lbl_time.Font = new System.Drawing.Font("Arial", 11F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lbl_time.Location = new System.Drawing.Point(1719, 60);
            this.lbl_time.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.lbl_time.Name = "lbl_time";
            this.lbl_time.Size = new System.Drawing.Size(60, 25);
            this.lbl_time.TabIndex = 9;
            this.lbl_time.Text = "Time";
            // 
            // lbl_date
            // 
            this.lbl_date.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.lbl_date.AutoSize = true;
            this.lbl_date.Font = new System.Drawing.Font("Arial", 11F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lbl_date.Location = new System.Drawing.Point(1455, 60);
            this.lbl_date.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.lbl_date.Name = "lbl_date";
            this.lbl_date.Size = new System.Drawing.Size(58, 25);
            this.lbl_date.TabIndex = 10;
            this.lbl_date.Text = "Date";
            // 
            // panel1
            // 
            this.panel1.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(192)))), ((int)(((byte)(0)))), ((int)(((byte)(0)))));
            this.panel1.Dock = System.Windows.Forms.DockStyle.Top;
            this.panel1.Location = new System.Drawing.Point(4, 0);
            this.panel1.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(1916, 5);
            this.panel1.TabIndex = 12;
            // 
            // panel2
            // 
            this.panel2.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(192)))), ((int)(((byte)(0)))), ((int)(((byte)(0)))));
            this.panel2.Dock = System.Windows.Forms.DockStyle.Left;
            this.panel2.Location = new System.Drawing.Point(0, 0);
            this.panel2.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.panel2.Name = "panel2";
            this.panel2.Size = new System.Drawing.Size(4, 1045);
            this.panel2.TabIndex = 13;
            // 
            // panel3
            // 
            this.panel3.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(192)))), ((int)(((byte)(0)))), ((int)(((byte)(0)))));
            this.panel3.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.panel3.Location = new System.Drawing.Point(0, 1045);
            this.panel3.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.panel3.Name = "panel3";
            this.panel3.Size = new System.Drawing.Size(1924, 5);
            this.panel3.TabIndex = 14;
            // 
            // panel4
            // 
            this.panel4.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(192)))), ((int)(((byte)(0)))), ((int)(((byte)(0)))));
            this.panel4.Dock = System.Windows.Forms.DockStyle.Right;
            this.panel4.Location = new System.Drawing.Point(1920, 0);
            this.panel4.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.panel4.Name = "panel4";
            this.panel4.Size = new System.Drawing.Size(4, 1045);
            this.panel4.TabIndex = 15;
            // 
            // panel5
            // 
            this.panel5.BackColor = System.Drawing.Color.White;
            this.panel5.Controls.Add(this.lbl_logout);
            this.panel5.Controls.Add(this.pictureBox1);
            this.panel5.Dock = System.Windows.Forms.DockStyle.Top;
            this.panel5.Location = new System.Drawing.Point(4, 5);
            this.panel5.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.panel5.Name = "panel5";
            this.panel5.Size = new System.Drawing.Size(1916, 100);
            this.panel5.TabIndex = 16;
            // 
            // lbl_logout
            // 
            this.lbl_logout.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.lbl_logout.AutoSize = true;
            this.lbl_logout.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lbl_logout.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.lbl_logout.Location = new System.Drawing.Point(1793, 35);
            this.lbl_logout.Name = "lbl_logout";
            this.lbl_logout.Size = new System.Drawing.Size(87, 29);
            this.lbl_logout.TabIndex = 1;
            this.lbl_logout.Text = "Logout";
            this.lbl_logout.Click += new System.EventHandler(this.lbl_logout_Click);
            // 
            // pictureBox1
            // 
            this.pictureBox1.Image = ((System.Drawing.Image)(resources.GetObject("pictureBox1.Image")));
            this.pictureBox1.Location = new System.Drawing.Point(28, 8);
            this.pictureBox1.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.pictureBox1.Name = "pictureBox1";
            this.pictureBox1.Size = new System.Drawing.Size(298, 100);
            this.pictureBox1.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom;
            this.pictureBox1.TabIndex = 0;
            this.pictureBox1.TabStop = false;
            // 
            // panel6
            // 
            this.panel6.Controls.Add(this.lbl_UserType);
            this.panel6.Controls.Add(this.lbl_CompName);
            this.panel6.Controls.Add(this.lbl_user);
            this.panel6.Controls.Add(this.label8);
            this.panel6.Controls.Add(this.label3);
            this.panel6.Controls.Add(this.label4);
            this.panel6.Controls.Add(this.lbl_time);
            this.panel6.Controls.Add(this.lbl_date);
            this.panel6.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.panel6.Location = new System.Drawing.Point(4, 940);
            this.panel6.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.panel6.Name = "panel6";
            this.panel6.Size = new System.Drawing.Size(1916, 105);
            this.panel6.TabIndex = 18;
            // 
            // lbl_UserType
            // 
            this.lbl_UserType.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.lbl_UserType.AutoSize = true;
            this.lbl_UserType.Font = new System.Drawing.Font("Arial", 11F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lbl_UserType.Location = new System.Drawing.Point(1455, 25);
            this.lbl_UserType.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.lbl_UserType.Name = "lbl_UserType";
            this.lbl_UserType.Size = new System.Drawing.Size(112, 25);
            this.lbl_UserType.TabIndex = 17;
            this.lbl_UserType.Text = "User Type";
            // 
            // lbl_CompName
            // 
            this.lbl_CompName.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.lbl_CompName.AutoSize = true;
            this.lbl_CompName.Font = new System.Drawing.Font("Arial", 11F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lbl_CompName.Location = new System.Drawing.Point(30, 59);
            this.lbl_CompName.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.lbl_CompName.Name = "lbl_CompName";
            this.lbl_CompName.Size = new System.Drawing.Size(174, 26);
            this.lbl_CompName.TabIndex = 16;
            this.lbl_CompName.Text = "Company Name";
            // 
            // lbl_user
            // 
            this.lbl_user.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.lbl_user.AutoSize = true;
            this.lbl_user.Font = new System.Drawing.Font("Arial", 11F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lbl_user.Location = new System.Drawing.Point(1714, 25);
            this.lbl_user.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.lbl_user.Name = "lbl_user";
            this.lbl_user.Size = new System.Drawing.Size(88, 25);
            this.lbl_user.TabIndex = 12;
            this.lbl_user.Text = "User_Id";
            // 
            // label8
            // 
            this.label8.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.label8.AutoSize = true;
            this.label8.Font = new System.Drawing.Font("Arial", 11F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label8.Location = new System.Drawing.Point(29, 25);
            this.label8.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(129, 25);
            this.label8.TabIndex = 14;
            this.label8.Text = "Licensed to:";
            // 
            // label3
            // 
            this.label3.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.label3.AutoSize = true;
            this.label3.Font = new System.Drawing.Font("Arial", 11F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label3.Location = new System.Drawing.Point(1329, 60);
            this.label3.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(118, 25);
            this.label3.TabIndex = 13;
            this.label3.Text = "Date/Time:";
            // 
            // label4
            // 
            this.label4.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.label4.AutoSize = true;
            this.label4.Font = new System.Drawing.Font("Arial", 11F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label4.Location = new System.Drawing.Point(1329, 25);
            this.label4.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(100, 25);
            this.label4.TabIndex = 11;
            this.label4.Text = "Login as:";
            // 
            // panel7
            // 
            this.panel7.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.panel7.Controls.Add(this.panel8);
            this.panel7.Controls.Add(this.toolStrip1);
            this.panel7.Font = new System.Drawing.Font("Segoe UI", 13F, System.Drawing.FontStyle.Bold);
            this.panel7.Location = new System.Drawing.Point(0, 10);
            this.panel7.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.panel7.MaximumSize = new System.Drawing.Size(1924, 1050);
            this.panel7.Name = "panel7";
            this.panel7.Size = new System.Drawing.Size(1924, 1031);
            this.panel7.TabIndex = 19;
            this.panel7.Paint += new System.Windows.Forms.PaintEventHandler(this.panel7_Paint);
            // 
            // panel8
            // 
            this.panel8.Controls.Add(this.label5);
            this.panel8.Controls.Add(this.pictureBox2);
            this.panel8.Location = new System.Drawing.Point(576, 258);
            this.panel8.Name = "panel8";
            this.panel8.Size = new System.Drawing.Size(773, 567);
            this.panel8.TabIndex = 19;
            // 
            // label5
            // 
            this.label5.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.label5.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.label5.Font = new System.Drawing.Font("Segoe UI", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label5.Location = new System.Drawing.Point(314, 502);
            this.label5.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(145, 36);
            this.label5.TabIndex = 12;
            this.label5.Text = "Version 1.2";
            // 
            // pictureBox2
            // 
            this.pictureBox2.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.pictureBox2.BackColor = System.Drawing.Color.Transparent;
            this.pictureBox2.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.pictureBox2.Image = ((System.Drawing.Image)(resources.GetObject("pictureBox2.Image")));
            this.pictureBox2.Location = new System.Drawing.Point(180, 28);
            this.pictureBox2.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.pictureBox2.Name = "pictureBox2";
            this.pictureBox2.Size = new System.Drawing.Size(413, 469);
            this.pictureBox2.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom;
            this.pictureBox2.TabIndex = 17;
            this.pictureBox2.TabStop = false;
            // 
            // toolStrip1
            // 
            this.toolStrip1.ImageScalingSize = new System.Drawing.Size(20, 20);
            this.toolStrip1.Location = new System.Drawing.Point(0, 0);
            this.toolStrip1.Name = "toolStrip1";
            this.toolStrip1.Size = new System.Drawing.Size(1924, 25);
            this.toolStrip1.TabIndex = 18;
            this.toolStrip1.Text = "toolStrip1";
            // 
            // Homepage
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(9F, 20F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.SystemColors.ButtonHighlight;
            this.ClientSize = new System.Drawing.Size(1924, 1050);
            this.Controls.Add(this.panel6);
            this.Controls.Add(this.menuStrip1);
            this.Controls.Add(this.panel5);
            this.Controls.Add(this.panel1);
            this.Controls.Add(this.panel4);
            this.Controls.Add(this.panel2);
            this.Controls.Add(this.panel3);
            this.Controls.Add(this.panel7);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None;
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.MainMenuStrip = this.menuStrip1;
            this.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.Name = "Homepage";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Homepage";
            this.WindowState = System.Windows.Forms.FormWindowState.Maximized;
            this.Load += new System.EventHandler(this.Homepage_Load);
            this.menuStrip1.ResumeLayout(false);
            this.menuStrip1.PerformLayout();
            this.panel5.ResumeLayout(false);
            this.panel5.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).EndInit();
            this.panel6.ResumeLayout(false);
            this.panel6.PerformLayout();
            this.panel7.ResumeLayout(false);
            this.panel7.PerformLayout();
            this.panel8.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox2)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion
        private System.Windows.Forms.Timer timer1;
        private System.Windows.Forms.ToolStripMenuItem MaintenanceMenuItem;
        private System.Windows.Forms.ToolStripMenuItem AuditLogItem;
        private System.Windows.Forms.ToolStripMenuItem DepartmentItem;
        private System.Windows.Forms.ToolStripMenuItem SSSTableItem;
        private System.Windows.Forms.ToolStripMenuItem TaxTableItem;
        private System.Windows.Forms.ToolStripMenuItem PhilhealthTableItem;
        private System.Windows.Forms.ToolStripMenuItem HDMFTableItem;
        private System.Windows.Forms.ToolStripMenuItem OvertimeTableItem;
        private System.Windows.Forms.ToolStripMenuItem EmployeeMenuListItem;
        private System.Windows.Forms.ToolStripMenuItem PayrolTransactionMenuItem;
        private System.Windows.Forms.MenuStrip menuStrip1;
        private System.Windows.Forms.Label lbl_time;
        private System.Windows.Forms.Label lbl_date;
        private System.Windows.Forms.Panel panel1;
        private System.Windows.Forms.Panel panel2;
        private System.Windows.Forms.Panel panel3;
        private System.Windows.Forms.Panel panel4;
        private System.Windows.Forms.Panel panel5;
        private System.Windows.Forms.ToolStripMenuItem ReportsMenuItem;
        private System.Windows.Forms.Panel panel6;
        private System.Windows.Forms.Panel panel7;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Label label5;
        public System.Windows.Forms.Label lbl_user;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.ToolStripMenuItem SystemSetupItem;
        private System.Windows.Forms.ToolStripMenuItem EmployeeMasterFileItem;
        private System.Windows.Forms.ToolStripMenuItem PayslipReportItem;
        private System.Windows.Forms.ToolStripMenuItem PayrollRegisterItem;
        private System.Windows.Forms.ToolStripMenuItem RegisterMonthlyReportItem;
        private System.Windows.Forms.ToolStripMenuItem BankRemittanceReportItem;
        private System.Windows.Forms.ToolStripMenuItem BonusRemittanceItem;
        private System.Windows.Forms.ToolStripMenuItem YearEndProcessItem;
        private System.Windows.Forms.ToolStripMenuItem BIRAphalistItem;
        private System.Windows.Forms.ToolStripMenuItem PostingTransactionItem;
        private System.Windows.Forms.ToolStripMenuItem PayrollTransactionItem;
        private System.Windows.Forms.ToolStripMenuItem PayrollProcessingItem;
        private System.Windows.Forms.ToolStripMenuItem BonusProcessItem;
        private System.Windows.Forms.ToolStripMenuItem BonusRegisterItem;
        private System.Windows.Forms.Label label8;
        public System.Windows.Forms.Label lbl_UserType;
        private System.Windows.Forms.ToolStrip toolStrip1;
        public System.Windows.Forms.Label lbl_logout;
        private System.Windows.Forms.PictureBox pictureBox1;
        private System.Windows.Forms.PictureBox pictureBox2;
        public System.Windows.Forms.Label lbl_CompName;
        private System.Windows.Forms.Panel panel8;
    }
}