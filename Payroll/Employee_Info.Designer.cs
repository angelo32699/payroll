﻿namespace Payroll
{
    partial class Employee_Info
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(Employee_Info));
            this.tb_search = new System.Windows.Forms.TextBox();
            this.label13 = new System.Windows.Forms.Label();
            this.lbl_category = new System.Windows.Forms.Label();
            this.lv_employee = new System.Windows.Forms.ListView();
            this.tp_id = new System.Windows.Forms.TabPage();
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.tb_sss_no = new System.Windows.Forms.TextBox();
            this.label103 = new System.Windows.Forms.Label();
            this.tb_hdmf_no = new System.Windows.Forms.TextBox();
            this.label88 = new System.Windows.Forms.Label();
            this.tb_tin_no = new System.Windows.Forms.TextBox();
            this.label104 = new System.Windows.Forms.Label();
            this.tb_philhealth_no = new System.Windows.Forms.TextBox();
            this.label43 = new System.Windows.Forms.Label();
            this.label105 = new System.Windows.Forms.Label();
            this.label42 = new System.Windows.Forms.Label();
            this.tb_id_lname = new System.Windows.Forms.TextBox();
            this.label40 = new System.Windows.Forms.Label();
            this.label119 = new System.Windows.Forms.Label();
            this.tb_id_fname = new System.Windows.Forms.TextBox();
            this.label56 = new System.Windows.Forms.Label();
            this.tb_id_mname = new System.Windows.Forms.TextBox();
            this.tb_id_empID = new System.Windows.Forms.TextBox();
            this.label41 = new System.Windows.Forms.Label();
            this.label106 = new System.Windows.Forms.Label();
            this.tp_other_info = new System.Windows.Forms.TabPage();
            this.label49 = new System.Windows.Forms.Label();
            this.groupBox6 = new System.Windows.Forms.GroupBox();
            this.groupBox15 = new System.Windows.Forms.GroupBox();
            this.tb_name = new System.Windows.Forms.TextBox();
            this.label63 = new System.Windows.Forms.Label();
            this.label64 = new System.Windows.Forms.Label();
            this.tb_relationship = new System.Windows.Forms.TextBox();
            this.label65 = new System.Windows.Forms.Label();
            this.tb_primary_con = new System.Windows.Forms.TextBox();
            this.label96 = new System.Windows.Forms.Label();
            this.tb_name2 = new System.Windows.Forms.TextBox();
            this.label95 = new System.Windows.Forms.Label();
            this.tb_relationship2 = new System.Windows.Forms.TextBox();
            this.label93 = new System.Windows.Forms.Label();
            this.tb_second_con = new System.Windows.Forms.TextBox();
            this.groupBox14 = new System.Windows.Forms.GroupBox();
            this.tb_mothers_name = new System.Windows.Forms.TextBox();
            this.tb_occupation_mother = new System.Windows.Forms.TextBox();
            this.tb_fathers_name = new System.Windows.Forms.TextBox();
            this.tb_occupation_father = new System.Windows.Forms.TextBox();
            this.label34 = new System.Windows.Forms.Label();
            this.label35 = new System.Windows.Forms.Label();
            this.label73 = new System.Windows.Forms.Label();
            this.label5 = new System.Windows.Forms.Label();
            this.rtb_homeAddress = new System.Windows.Forms.RichTextBox();
            this.label14 = new System.Windows.Forms.Label();
            this.groupBox13 = new System.Windows.Forms.GroupBox();
            this.label1 = new System.Windows.Forms.Label();
            this.tb_elementary = new System.Windows.Forms.TextBox();
            this.label53 = new System.Windows.Forms.Label();
            this.rtb_work_experience = new System.Windows.Forms.RichTextBox();
            this.label48 = new System.Windows.Forms.Label();
            this.label52 = new System.Windows.Forms.Label();
            this.label50 = new System.Windows.Forms.Label();
            this.label114 = new System.Windows.Forms.Label();
            this.tb_highSchool = new System.Windows.Forms.TextBox();
            this.tb_tertiary = new System.Windows.Forms.TextBox();
            this.tb_course = new System.Windows.Forms.TextBox();
            this.label134 = new System.Windows.Forms.Label();
            this.label135 = new System.Windows.Forms.Label();
            this.label136 = new System.Windows.Forms.Label();
            this.tb_info_lname = new System.Windows.Forms.TextBox();
            this.tb_info_fname = new System.Windows.Forms.TextBox();
            this.tb_info_mname = new System.Windows.Forms.TextBox();
            this.tb_info_empID = new System.Windows.Forms.TextBox();
            this.label137 = new System.Windows.Forms.Label();
            this.tp_loan = new System.Windows.Forms.TabPage();
            this.lbl_comp_loan = new System.Windows.Forms.GroupBox();
            this.label109 = new System.Windows.Forms.Label();
            this.label120 = new System.Windows.Forms.Label();
            this.label57 = new System.Windows.Forms.Label();
            this.label107 = new System.Windows.Forms.Label();
            this.label108 = new System.Windows.Forms.Label();
            this.tb_loan_lname = new System.Windows.Forms.TextBox();
            this.tb_loan_fname = new System.Windows.Forms.TextBox();
            this.tb_loan_mname = new System.Windows.Forms.TextBox();
            this.tb_loan_empID = new System.Windows.Forms.TextBox();
            this.label110 = new System.Windows.Forms.Label();
            this.groupBox7 = new System.Windows.Forms.GroupBox();
            this.label89 = new System.Windows.Forms.Label();
            this.label60 = new System.Windows.Forms.Label();
            this.tb_comp_loan_month_ded = new System.Windows.Forms.TextBox();
            this.tb_comp_loan_bal = new System.Windows.Forms.TextBox();
            this.label67 = new System.Windows.Forms.Label();
            this.tb_comp_loan_amnt = new System.Windows.Forms.TextBox();
            this.label61 = new System.Windows.Forms.Label();
            this.groupBox9 = new System.Windows.Forms.GroupBox();
            this.label94 = new System.Windows.Forms.Label();
            this.tb_sss_loan_month_ded = new System.Windows.Forms.TextBox();
            this.label78 = new System.Windows.Forms.Label();
            this.tb_sss_loan_bal = new System.Windows.Forms.TextBox();
            this.tb_sss_loan_amnt = new System.Windows.Forms.TextBox();
            this.label66 = new System.Windows.Forms.Label();
            this.groupBox10 = new System.Windows.Forms.GroupBox();
            this.label68 = new System.Windows.Forms.Label();
            this.tb_pag_ibig_loan_month_ded = new System.Windows.Forms.TextBox();
            this.tb_pag_ibig_loan_bal = new System.Windows.Forms.TextBox();
            this.tb_pag_ibig_loan_amnt = new System.Windows.Forms.TextBox();
            this.label81 = new System.Windows.Forms.Label();
            this.label82 = new System.Windows.Forms.Label();
            this.tp_Leave = new System.Windows.Forms.TabPage();
            this.groupBox8 = new System.Windows.Forms.GroupBox();
            this.label79 = new System.Windows.Forms.Label();
            this.label87 = new System.Windows.Forms.Label();
            this.label31 = new System.Windows.Forms.Label();
            this.label45 = new System.Windows.Forms.Label();
            this.label85 = new System.Windows.Forms.Label();
            this.label86 = new System.Windows.Forms.Label();
            this.tb_leave_lname = new System.Windows.Forms.TextBox();
            this.tb_leave_fname = new System.Windows.Forms.TextBox();
            this.tb_leave_mname = new System.Windows.Forms.TextBox();
            this.tb_leave_empID = new System.Windows.Forms.TextBox();
            this.label84 = new System.Windows.Forms.Label();
            this.tb_vacation_leave = new System.Windows.Forms.TextBox();
            this.label70 = new System.Windows.Forms.Label();
            this.label71 = new System.Windows.Forms.Label();
            this.tb_sick_leave = new System.Windows.Forms.TextBox();
            this.tp_salary = new System.Windows.Forms.TabPage();
            this.groupBox5 = new System.Windows.Forms.GroupBox();
            this.groupBox12 = new System.Windows.Forms.GroupBox();
            this.tb_sss = new System.Windows.Forms.TextBox();
            this.chk_sss = new System.Windows.Forms.CheckBox();
            this.chk_pag_ibig = new System.Windows.Forms.CheckBox();
            this.chk_philhealth = new System.Windows.Forms.CheckBox();
            this.label32 = new System.Windows.Forms.Label();
            this.tb_philhealth = new System.Windows.Forms.TextBox();
            this.label33 = new System.Windows.Forms.Label();
            this.label38 = new System.Windows.Forms.Label();
            this.label115 = new System.Windows.Forms.Label();
            this.label117 = new System.Windows.Forms.Label();
            this.label116 = new System.Windows.Forms.Label();
            this.tb_pag_ibig = new System.Windows.Forms.TextBox();
            this.label17 = new System.Windows.Forms.Label();
            this.groupBox11 = new System.Windows.Forms.GroupBox();
            this.tb_13th_month_pay = new System.Windows.Forms.TextBox();
            this.chb_13month_pay = new System.Windows.Forms.CheckBox();
            this.chb_14month_pay = new System.Windows.Forms.CheckBox();
            this.chb_15month_pay = new System.Windows.Forms.CheckBox();
            this.tb_14th_month_pay = new System.Windows.Forms.TextBox();
            this.label12 = new System.Windows.Forms.Label();
            this.label74 = new System.Windows.Forms.Label();
            this.tb_15th_month_pay = new System.Windows.Forms.TextBox();
            this.label75 = new System.Windows.Forms.Label();
            this.groupBox4 = new System.Windows.Forms.GroupBox();
            this.tb_otherAllowance = new System.Windows.Forms.TextBox();
            this.label92 = new System.Windows.Forms.Label();
            this.label102 = new System.Windows.Forms.Label();
            this.label111 = new System.Windows.Forms.Label();
            this.label112 = new System.Windows.Forms.Label();
            this.label113 = new System.Windows.Forms.Label();
            this.tb_riceAllowance = new System.Windows.Forms.TextBox();
            this.tb_laundryAllowance = new System.Windows.Forms.TextBox();
            this.tb_mealAllowance = new System.Windows.Forms.TextBox();
            this.tb_MedicalBenefits = new System.Windows.Forms.TextBox();
            this.groupBox3 = new System.Windows.Forms.GroupBox();
            this.cb_payRate = new System.Windows.Forms.ComboBox();
            this.label29 = new System.Windows.Forms.Label();
            this.label131 = new System.Windows.Forms.Label();
            this.tb_salary = new System.Windows.Forms.TextBox();
            this.label130 = new System.Windows.Forms.Label();
            this.label37 = new System.Windows.Forms.Label();
            this.label129 = new System.Windows.Forms.Label();
            this.label69 = new System.Windows.Forms.Label();
            this.tb_daily_rate = new System.Windows.Forms.TextBox();
            this.label39 = new System.Windows.Forms.Label();
            this.tb_daily_hour_rate = new System.Windows.Forms.TextBox();
            this.label10 = new System.Windows.Forms.Label();
            this.tb_minRate = new System.Windows.Forms.TextBox();
            this.cb_paySchedule = new System.Windows.Forms.ComboBox();
            this.label72 = new System.Windows.Forms.Label();
            this.label132 = new System.Windows.Forms.Label();
            this.label128 = new System.Windows.Forms.Label();
            this.label127 = new System.Windows.Forms.Label();
            this.label126 = new System.Windows.Forms.Label();
            this.label121 = new System.Windows.Forms.Label();
            this.label36 = new System.Windows.Forms.Label();
            this.cb_bank_name2 = new System.Windows.Forms.ComboBox();
            this.cb_reason = new System.Windows.Forms.ComboBox();
            this.label26 = new System.Windows.Forms.Label();
            this.label83 = new System.Windows.Forms.Label();
            this.dtp_dateResign = new System.Windows.Forms.DateTimePicker();
            this.label76 = new System.Windows.Forms.Label();
            this.label122 = new System.Windows.Forms.Label();
            this.tb_bank_acc2 = new System.Windows.Forms.TextBox();
            this.label99 = new System.Windows.Forms.Label();
            this.label118 = new System.Windows.Forms.Label();
            this.cb_department = new System.Windows.Forms.ComboBox();
            this.label21 = new System.Windows.Forms.Label();
            this.tb_designation = new System.Windows.Forms.TextBox();
            this.label20 = new System.Windows.Forms.Label();
            this.dtp_date_hired = new System.Windows.Forms.DateTimePicker();
            this.cb_employee_status = new System.Windows.Forms.ComboBox();
            this.label19 = new System.Windows.Forms.Label();
            this.label18 = new System.Windows.Forms.Label();
            this.label97 = new System.Windows.Forms.Label();
            this.label98 = new System.Windows.Forms.Label();
            this.tb_sal_lname = new System.Windows.Forms.TextBox();
            this.tb_sal_fname = new System.Windows.Forms.TextBox();
            this.tb_sal_mname = new System.Windows.Forms.TextBox();
            this.tb_sal_empID = new System.Windows.Forms.TextBox();
            this.label100 = new System.Windows.Forms.Label();
            this.label91 = new System.Windows.Forms.Label();
            this.tb_bank_acc = new System.Windows.Forms.TextBox();
            this.label58 = new System.Windows.Forms.Label();
            this.cb_bank_name = new System.Windows.Forms.ComboBox();
            this.label27 = new System.Windows.Forms.Label();
            this.cb_payroll_mode = new System.Windows.Forms.ComboBox();
            this.label28 = new System.Windows.Forms.Label();
            this.cb_confidentiality = new System.Windows.Forms.ComboBox();
            this.label55 = new System.Windows.Forms.Label();
            this.tp_general_information = new System.Windows.Forms.TabPage();
            this.groupBox2 = new System.Windows.Forms.GroupBox();
            this.label133 = new System.Windows.Forms.Label();
            this.label77 = new System.Windows.Forms.Label();
            this.label16 = new System.Windows.Forms.Label();
            this.btn_upload = new Payroll.RJButtons.RJButton();
            this.cb_statustype = new System.Windows.Forms.ComboBox();
            this.label62 = new System.Windows.Forms.Label();
            this.label125 = new System.Windows.Forms.Label();
            this.label124 = new System.Windows.Forms.Label();
            this.label123 = new System.Windows.Forms.Label();
            this.lbl_emp_no = new System.Windows.Forms.Label();
            this.rtb_address = new System.Windows.Forms.RichTextBox();
            this.cb_nationality = new System.Windows.Forms.ComboBox();
            this.tb_name_of_spouse = new System.Windows.Forms.TextBox();
            this.cb_religion = new System.Windows.Forms.ComboBox();
            this.label46 = new System.Windows.Forms.Label();
            this.label44 = new System.Windows.Forms.Label();
            this.label51 = new System.Windows.Forms.Label();
            this.cb_gender = new System.Windows.Forms.ComboBox();
            this.label8 = new System.Windows.Forms.Label();
            this.label11 = new System.Windows.Forms.Label();
            this.lbl_number_of_employee = new System.Windows.Forms.Label();
            this.label30 = new System.Windows.Forms.Label();
            this.dtp_birthdate = new System.Windows.Forms.DateTimePicker();
            this.label15 = new System.Windows.Forms.Label();
            this.tb_comp_email = new System.Windows.Forms.TextBox();
            this.tb_telphone_no = new System.Windows.Forms.TextBox();
            this.label24 = new System.Windows.Forms.Label();
            this.label23 = new System.Windows.Forms.Label();
            this.tb_empID = new System.Windows.Forms.TextBox();
            this.label9 = new System.Windows.Forms.Label();
            this.label54 = new System.Windows.Forms.Label();
            this.cb_pob = new System.Windows.Forms.ComboBox();
            this.cb_status = new System.Windows.Forms.ComboBox();
            this.label47 = new System.Windows.Forms.Label();
            this.label6 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.tb_lname = new System.Windows.Forms.TextBox();
            this.tb_fname = new System.Windows.Forms.TextBox();
            this.tb_mname = new System.Windows.Forms.TextBox();
            this.tb_email = new System.Windows.Forms.TextBox();
            this.label4 = new System.Windows.Forms.Label();
            this.pic_Image = new System.Windows.Forms.PictureBox();
            this.tb_cont_no = new System.Windows.Forms.TextBox();
            this.label7 = new System.Windows.Forms.Label();
            this.tc_emp_info = new System.Windows.Forms.TabControl();
            this.btn_export = new Payroll.RJButtons.RJButton();
            this.btn_Delete = new Payroll.RJButtons.RJButton();
            this.btn_update = new Payroll.RJButtons.RJButton();
            this.btn_add = new Payroll.RJButtons.RJButton();
            this.tp_id.SuspendLayout();
            this.groupBox1.SuspendLayout();
            this.tp_other_info.SuspendLayout();
            this.groupBox6.SuspendLayout();
            this.groupBox15.SuspendLayout();
            this.groupBox14.SuspendLayout();
            this.groupBox13.SuspendLayout();
            this.tp_loan.SuspendLayout();
            this.lbl_comp_loan.SuspendLayout();
            this.groupBox7.SuspendLayout();
            this.groupBox9.SuspendLayout();
            this.groupBox10.SuspendLayout();
            this.tp_Leave.SuspendLayout();
            this.groupBox8.SuspendLayout();
            this.tp_salary.SuspendLayout();
            this.groupBox5.SuspendLayout();
            this.groupBox12.SuspendLayout();
            this.groupBox11.SuspendLayout();
            this.groupBox4.SuspendLayout();
            this.groupBox3.SuspendLayout();
            this.tp_general_information.SuspendLayout();
            this.groupBox2.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pic_Image)).BeginInit();
            this.tc_emp_info.SuspendLayout();
            this.SuspendLayout();
            // 
            // tb_search
            // 
            this.tb_search.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.tb_search.ForeColor = System.Drawing.Color.Black;
            this.tb_search.Location = new System.Drawing.Point(1356, 689);
            this.tb_search.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.tb_search.Name = "tb_search";
            this.tb_search.Size = new System.Drawing.Size(290, 28);
            this.tb_search.TabIndex = 14;
            this.tb_search.TextChanged += new System.EventHandler(this.tb_search_TextChanged);
            // 
            // label13
            // 
            this.label13.AutoSize = true;
            this.label13.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label13.Location = new System.Drawing.Point(1281, 689);
            this.label13.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label13.Name = "label13";
            this.label13.Size = new System.Drawing.Size(72, 22);
            this.label13.TabIndex = 58;
            this.label13.Text = "Search:";
            // 
            // lbl_category
            // 
            this.lbl_category.AutoSize = true;
            this.lbl_category.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lbl_category.Location = new System.Drawing.Point(12, 686);
            this.lbl_category.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.lbl_category.Name = "lbl_category";
            this.lbl_category.Size = new System.Drawing.Size(201, 25);
            this.lbl_category.TabIndex = 74;
            this.lbl_category.Text = "General Information";
            // 
            // lv_employee
            // 
            this.lv_employee.BackColor = System.Drawing.Color.White;
            this.lv_employee.Font = new System.Drawing.Font("Arial", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lv_employee.FullRowSelect = true;
            this.lv_employee.GridLines = true;
            this.lv_employee.HideSelection = false;
            this.lv_employee.LabelWrap = false;
            this.lv_employee.Location = new System.Drawing.Point(13, 727);
            this.lv_employee.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.lv_employee.Name = "lv_employee";
            this.lv_employee.Size = new System.Drawing.Size(1632, 243);
            this.lv_employee.TabIndex = 75;
            this.lv_employee.UseCompatibleStateImageBehavior = false;
            this.lv_employee.View = System.Windows.Forms.View.Details;
            this.lv_employee.Click += new System.EventHandler(this.lv_employee_Click);
            // 
            // tp_id
            // 
            this.tp_id.Controls.Add(this.groupBox1);
            this.tp_id.Location = new System.Drawing.Point(4, 31);
            this.tp_id.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.tp_id.Name = "tp_id";
            this.tp_id.Padding = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.tp_id.Size = new System.Drawing.Size(1624, 584);
            this.tp_id.TabIndex = 8;
            this.tp_id.Text = "Other ID";
            this.tp_id.UseVisualStyleBackColor = true;
            this.tp_id.Enter += new System.EventHandler(this.tp_id_Enter);
            // 
            // groupBox1
            // 
            this.groupBox1.Controls.Add(this.tb_sss_no);
            this.groupBox1.Controls.Add(this.label103);
            this.groupBox1.Controls.Add(this.tb_hdmf_no);
            this.groupBox1.Controls.Add(this.label88);
            this.groupBox1.Controls.Add(this.tb_tin_no);
            this.groupBox1.Controls.Add(this.label104);
            this.groupBox1.Controls.Add(this.tb_philhealth_no);
            this.groupBox1.Controls.Add(this.label43);
            this.groupBox1.Controls.Add(this.label105);
            this.groupBox1.Controls.Add(this.label42);
            this.groupBox1.Controls.Add(this.tb_id_lname);
            this.groupBox1.Controls.Add(this.label40);
            this.groupBox1.Controls.Add(this.label119);
            this.groupBox1.Controls.Add(this.tb_id_fname);
            this.groupBox1.Controls.Add(this.label56);
            this.groupBox1.Controls.Add(this.tb_id_mname);
            this.groupBox1.Controls.Add(this.tb_id_empID);
            this.groupBox1.Controls.Add(this.label41);
            this.groupBox1.Controls.Add(this.label106);
            this.groupBox1.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.groupBox1.Location = new System.Drawing.Point(9, 5);
            this.groupBox1.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Padding = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.groupBox1.Size = new System.Drawing.Size(1607, 563);
            this.groupBox1.TabIndex = 153;
            this.groupBox1.TabStop = false;
            this.groupBox1.Text = "Other ID";
            // 
            // tb_sss_no
            // 
            this.tb_sss_no.Enabled = false;
            this.tb_sss_no.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.tb_sss_no.ForeColor = System.Drawing.Color.Black;
            this.tb_sss_no.Location = new System.Drawing.Point(879, 31);
            this.tb_sss_no.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.tb_sss_no.Name = "tb_sss_no";
            this.tb_sss_no.Size = new System.Drawing.Size(302, 28);
            this.tb_sss_no.TabIndex = 1;
            // 
            // label103
            // 
            this.label103.AutoSize = true;
            this.label103.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label103.Location = new System.Drawing.Point(212, 135);
            this.label103.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label103.Name = "label103";
            this.label103.Size = new System.Drawing.Size(102, 22);
            this.label103.TabIndex = 151;
            this.label103.Text = "First Name:";
            // 
            // tb_hdmf_no
            // 
            this.tb_hdmf_no.Enabled = false;
            this.tb_hdmf_no.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.tb_hdmf_no.ForeColor = System.Drawing.Color.Black;
            this.tb_hdmf_no.Location = new System.Drawing.Point(879, 75);
            this.tb_hdmf_no.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.tb_hdmf_no.Name = "tb_hdmf_no";
            this.tb_hdmf_no.Size = new System.Drawing.Size(302, 28);
            this.tb_hdmf_no.TabIndex = 2;
            // 
            // label88
            // 
            this.label88.AutoSize = true;
            this.label88.Location = new System.Drawing.Point(7, 532);
            this.label88.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label88.Name = "label88";
            this.label88.Size = new System.Drawing.Size(21, 22);
            this.label88.TabIndex = 201;
            this.label88.Text = "0";
            this.label88.Visible = false;
            // 
            // tb_tin_no
            // 
            this.tb_tin_no.Enabled = false;
            this.tb_tin_no.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.tb_tin_no.ForeColor = System.Drawing.Color.Black;
            this.tb_tin_no.Location = new System.Drawing.Point(879, 119);
            this.tb_tin_no.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.tb_tin_no.Name = "tb_tin_no";
            this.tb_tin_no.Size = new System.Drawing.Size(302, 28);
            this.tb_tin_no.TabIndex = 3;
            // 
            // label104
            // 
            this.label104.AutoSize = true;
            this.label104.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label104.Location = new System.Drawing.Point(214, 89);
            this.label104.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label104.Name = "label104";
            this.label104.Size = new System.Drawing.Size(101, 22);
            this.label104.TabIndex = 150;
            this.label104.Text = "Last Name:";
            // 
            // tb_philhealth_no
            // 
            this.tb_philhealth_no.Enabled = false;
            this.tb_philhealth_no.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.tb_philhealth_no.ForeColor = System.Drawing.Color.Black;
            this.tb_philhealth_no.Location = new System.Drawing.Point(879, 165);
            this.tb_philhealth_no.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.tb_philhealth_no.Name = "tb_philhealth_no";
            this.tb_philhealth_no.Size = new System.Drawing.Size(302, 28);
            this.tb_philhealth_no.TabIndex = 4;
            // 
            // label43
            // 
            this.label43.AutoSize = true;
            this.label43.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label43.Location = new System.Drawing.Point(749, 171);
            this.label43.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label43.Name = "label43";
            this.label43.Size = new System.Drawing.Size(122, 22);
            this.label43.TabIndex = 107;
            this.label43.Text = "Philhealth No:";
            // 
            // label105
            // 
            this.label105.AutoSize = true;
            this.label105.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label105.Location = new System.Drawing.Point(196, 186);
            this.label105.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label105.Name = "label105";
            this.label105.Size = new System.Drawing.Size(119, 22);
            this.label105.TabIndex = 154;
            this.label105.Text = "Middle Name:";
            // 
            // label42
            // 
            this.label42.AutoSize = true;
            this.label42.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label42.Location = new System.Drawing.Point(785, 125);
            this.label42.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label42.Name = "label42";
            this.label42.Size = new System.Drawing.Size(69, 22);
            this.label42.TabIndex = 106;
            this.label42.Text = "Tin No:";
            // 
            // tb_id_lname
            // 
            this.tb_id_lname.Enabled = false;
            this.tb_id_lname.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.tb_id_lname.ForeColor = System.Drawing.Color.Black;
            this.tb_id_lname.Location = new System.Drawing.Point(347, 86);
            this.tb_id_lname.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.tb_id_lname.Name = "tb_id_lname";
            this.tb_id_lname.Size = new System.Drawing.Size(302, 28);
            this.tb_id_lname.TabIndex = 147;
            this.tb_id_lname.TabStop = false;
            // 
            // label40
            // 
            this.label40.AutoSize = true;
            this.label40.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label40.Location = new System.Drawing.Point(771, 35);
            this.label40.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label40.Name = "label40";
            this.label40.Size = new System.Drawing.Size(79, 22);
            this.label40.TabIndex = 104;
            this.label40.Text = "SSS No:";
            // 
            // label119
            // 
            this.label119.AutoSize = true;
            this.label119.Location = new System.Drawing.Point(4, 58);
            this.label119.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label119.Name = "label119";
            this.label119.Size = new System.Drawing.Size(21, 22);
            this.label119.TabIndex = 200;
            this.label119.Text = "0";
            this.label119.Visible = false;
            // 
            // tb_id_fname
            // 
            this.tb_id_fname.Enabled = false;
            this.tb_id_fname.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.tb_id_fname.ForeColor = System.Drawing.Color.Black;
            this.tb_id_fname.Location = new System.Drawing.Point(347, 132);
            this.tb_id_fname.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.tb_id_fname.Name = "tb_id_fname";
            this.tb_id_fname.Size = new System.Drawing.Size(302, 28);
            this.tb_id_fname.TabIndex = 148;
            this.tb_id_fname.TabStop = false;
            // 
            // label56
            // 
            this.label56.AutoSize = true;
            this.label56.Location = new System.Drawing.Point(4, 26);
            this.label56.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label56.Name = "label56";
            this.label56.Size = new System.Drawing.Size(21, 22);
            this.label56.TabIndex = 157;
            this.label56.Text = "0";
            this.label56.Visible = false;
            // 
            // tb_id_mname
            // 
            this.tb_id_mname.Enabled = false;
            this.tb_id_mname.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.tb_id_mname.ForeColor = System.Drawing.Color.Black;
            this.tb_id_mname.Location = new System.Drawing.Point(347, 186);
            this.tb_id_mname.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.tb_id_mname.Name = "tb_id_mname";
            this.tb_id_mname.Size = new System.Drawing.Size(302, 28);
            this.tb_id_mname.TabIndex = 149;
            this.tb_id_mname.TabStop = false;
            // 
            // tb_id_empID
            // 
            this.tb_id_empID.Enabled = false;
            this.tb_id_empID.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.tb_id_empID.ForeColor = System.Drawing.Color.Black;
            this.tb_id_empID.Location = new System.Drawing.Point(346, 40);
            this.tb_id_empID.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.tb_id_empID.Name = "tb_id_empID";
            this.tb_id_empID.Size = new System.Drawing.Size(304, 28);
            this.tb_id_empID.TabIndex = 145;
            this.tb_id_empID.TabStop = false;
            // 
            // label41
            // 
            this.label41.AutoSize = true;
            this.label41.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label41.Location = new System.Drawing.Point(780, 81);
            this.label41.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label41.Name = "label41";
            this.label41.Size = new System.Drawing.Size(94, 22);
            this.label41.TabIndex = 105;
            this.label41.Text = "HDMF No:";
            // 
            // label106
            // 
            this.label106.AutoSize = true;
            this.label106.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label106.Location = new System.Drawing.Point(197, 46);
            this.label106.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label106.Name = "label106";
            this.label106.Size = new System.Drawing.Size(116, 22);
            this.label106.TabIndex = 146;
            this.label106.Text = "Employee ID:";
            // 
            // tp_other_info
            // 
            this.tp_other_info.Controls.Add(this.label49);
            this.tp_other_info.Controls.Add(this.groupBox6);
            this.tp_other_info.Location = new System.Drawing.Point(4, 31);
            this.tp_other_info.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.tp_other_info.Name = "tp_other_info";
            this.tp_other_info.Padding = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.tp_other_info.Size = new System.Drawing.Size(1624, 584);
            this.tp_other_info.TabIndex = 1;
            this.tp_other_info.Text = "Other Information";
            this.tp_other_info.UseVisualStyleBackColor = true;
            this.tp_other_info.Enter += new System.EventHandler(this.tp_other_info_Enter);
            // 
            // label49
            // 
            this.label49.AutoSize = true;
            this.label49.Font = new System.Drawing.Font("Arial", 11.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label49.Location = new System.Drawing.Point(790, -35);
            this.label49.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label49.Name = "label49";
            this.label49.Size = new System.Drawing.Size(282, 27);
            this.label49.TabIndex = 80;
            this.label49.Text = "Educational Attaintment";
            // 
            // groupBox6
            // 
            this.groupBox6.Controls.Add(this.groupBox15);
            this.groupBox6.Controls.Add(this.groupBox14);
            this.groupBox6.Controls.Add(this.groupBox13);
            this.groupBox6.Controls.Add(this.label134);
            this.groupBox6.Controls.Add(this.label135);
            this.groupBox6.Controls.Add(this.label136);
            this.groupBox6.Controls.Add(this.tb_info_lname);
            this.groupBox6.Controls.Add(this.tb_info_fname);
            this.groupBox6.Controls.Add(this.tb_info_mname);
            this.groupBox6.Controls.Add(this.tb_info_empID);
            this.groupBox6.Controls.Add(this.label137);
            this.groupBox6.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.groupBox6.Location = new System.Drawing.Point(10, 5);
            this.groupBox6.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.groupBox6.Name = "groupBox6";
            this.groupBox6.Padding = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.groupBox6.Size = new System.Drawing.Size(1606, 569);
            this.groupBox6.TabIndex = 140;
            this.groupBox6.TabStop = false;
            this.groupBox6.Text = "Other Information";
            // 
            // groupBox15
            // 
            this.groupBox15.Controls.Add(this.tb_name);
            this.groupBox15.Controls.Add(this.label63);
            this.groupBox15.Controls.Add(this.label64);
            this.groupBox15.Controls.Add(this.tb_relationship);
            this.groupBox15.Controls.Add(this.label65);
            this.groupBox15.Controls.Add(this.tb_primary_con);
            this.groupBox15.Controls.Add(this.label96);
            this.groupBox15.Controls.Add(this.tb_name2);
            this.groupBox15.Controls.Add(this.label95);
            this.groupBox15.Controls.Add(this.tb_relationship2);
            this.groupBox15.Controls.Add(this.label93);
            this.groupBox15.Controls.Add(this.tb_second_con);
            this.groupBox15.Location = new System.Drawing.Point(1057, 116);
            this.groupBox15.Name = "groupBox15";
            this.groupBox15.Size = new System.Drawing.Size(542, 445);
            this.groupBox15.TabIndex = 3;
            this.groupBox15.TabStop = false;
            this.groupBox15.Text = "Emergency Contacts";
            // 
            // tb_name
            // 
            this.tb_name.Enabled = false;
            this.tb_name.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.tb_name.ForeColor = System.Drawing.Color.Black;
            this.tb_name.Location = new System.Drawing.Point(194, 57);
            this.tb_name.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.tb_name.Name = "tb_name";
            this.tb_name.Size = new System.Drawing.Size(301, 28);
            this.tb_name.TabIndex = 11;
            // 
            // label63
            // 
            this.label63.AutoSize = true;
            this.label63.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label63.Location = new System.Drawing.Point(40, 63);
            this.label63.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label63.Name = "label63";
            this.label63.Size = new System.Drawing.Size(143, 22);
            this.label63.TabIndex = 200;
            this.label63.Text = "Primary Contact:";
            // 
            // label64
            // 
            this.label64.AutoSize = true;
            this.label64.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label64.Location = new System.Drawing.Point(63, 147);
            this.label64.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label64.Name = "label64";
            this.label64.Size = new System.Drawing.Size(114, 22);
            this.label64.TabIndex = 202;
            this.label64.Text = "Relationship:";
            // 
            // tb_relationship
            // 
            this.tb_relationship.Enabled = false;
            this.tb_relationship.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.tb_relationship.ForeColor = System.Drawing.Color.Black;
            this.tb_relationship.Location = new System.Drawing.Point(194, 147);
            this.tb_relationship.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.tb_relationship.Name = "tb_relationship";
            this.tb_relationship.Size = new System.Drawing.Size(301, 28);
            this.tb_relationship.TabIndex = 13;
            // 
            // label65
            // 
            this.label65.AutoSize = true;
            this.label65.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label65.Location = new System.Drawing.Point(36, 102);
            this.label65.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label65.Name = "label65";
            this.label65.Size = new System.Drawing.Size(145, 22);
            this.label65.TabIndex = 203;
            this.label65.Text = "Contact Number:";
            // 
            // tb_primary_con
            // 
            this.tb_primary_con.Enabled = false;
            this.tb_primary_con.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.tb_primary_con.ForeColor = System.Drawing.Color.Black;
            this.tb_primary_con.Location = new System.Drawing.Point(194, 102);
            this.tb_primary_con.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.tb_primary_con.MaxLength = 11;
            this.tb_primary_con.Name = "tb_primary_con";
            this.tb_primary_con.Size = new System.Drawing.Size(302, 28);
            this.tb_primary_con.TabIndex = 12;
            // 
            // label96
            // 
            this.label96.AutoSize = true;
            this.label96.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label96.Location = new System.Drawing.Point(25, 212);
            this.label96.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label96.Name = "label96";
            this.label96.Size = new System.Drawing.Size(163, 22);
            this.label96.TabIndex = 206;
            this.label96.Text = "Secondary Contact";
            // 
            // tb_name2
            // 
            this.tb_name2.Enabled = false;
            this.tb_name2.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.tb_name2.ForeColor = System.Drawing.Color.Black;
            this.tb_name2.Location = new System.Drawing.Point(196, 209);
            this.tb_name2.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.tb_name2.Name = "tb_name2";
            this.tb_name2.Size = new System.Drawing.Size(301, 28);
            this.tb_name2.TabIndex = 14;
            // 
            // label95
            // 
            this.label95.AutoSize = true;
            this.label95.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label95.Location = new System.Drawing.Point(65, 307);
            this.label95.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label95.Name = "label95";
            this.label95.Size = new System.Drawing.Size(114, 22);
            this.label95.TabIndex = 208;
            this.label95.Text = "Relationship:";
            // 
            // tb_relationship2
            // 
            this.tb_relationship2.Enabled = false;
            this.tb_relationship2.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.tb_relationship2.ForeColor = System.Drawing.Color.Black;
            this.tb_relationship2.Location = new System.Drawing.Point(196, 302);
            this.tb_relationship2.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.tb_relationship2.Name = "tb_relationship2";
            this.tb_relationship2.Size = new System.Drawing.Size(301, 28);
            this.tb_relationship2.TabIndex = 16;
            // 
            // label93
            // 
            this.label93.AutoSize = true;
            this.label93.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label93.Location = new System.Drawing.Point(43, 256);
            this.label93.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label93.Name = "label93";
            this.label93.Size = new System.Drawing.Size(145, 22);
            this.label93.TabIndex = 209;
            this.label93.Text = "Contact Number:";
            // 
            // tb_second_con
            // 
            this.tb_second_con.Enabled = false;
            this.tb_second_con.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.tb_second_con.ForeColor = System.Drawing.Color.Black;
            this.tb_second_con.Location = new System.Drawing.Point(196, 253);
            this.tb_second_con.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.tb_second_con.MaxLength = 11;
            this.tb_second_con.Name = "tb_second_con";
            this.tb_second_con.Size = new System.Drawing.Size(302, 28);
            this.tb_second_con.TabIndex = 15;
            // 
            // groupBox14
            // 
            this.groupBox14.Controls.Add(this.tb_mothers_name);
            this.groupBox14.Controls.Add(this.tb_occupation_mother);
            this.groupBox14.Controls.Add(this.tb_fathers_name);
            this.groupBox14.Controls.Add(this.tb_occupation_father);
            this.groupBox14.Controls.Add(this.label34);
            this.groupBox14.Controls.Add(this.label35);
            this.groupBox14.Controls.Add(this.label73);
            this.groupBox14.Controls.Add(this.label5);
            this.groupBox14.Controls.Add(this.rtb_homeAddress);
            this.groupBox14.Controls.Add(this.label14);
            this.groupBox14.Location = new System.Drawing.Point(519, 116);
            this.groupBox14.Name = "groupBox14";
            this.groupBox14.Size = new System.Drawing.Size(532, 445);
            this.groupBox14.TabIndex = 2;
            this.groupBox14.TabStop = false;
            this.groupBox14.Text = "Other Information";
            // 
            // tb_mothers_name
            // 
            this.tb_mothers_name.BackColor = System.Drawing.Color.White;
            this.tb_mothers_name.Enabled = false;
            this.tb_mothers_name.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.tb_mothers_name.ForeColor = System.Drawing.Color.Black;
            this.tb_mothers_name.Location = new System.Drawing.Point(200, 37);
            this.tb_mothers_name.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.tb_mothers_name.Name = "tb_mothers_name";
            this.tb_mothers_name.Size = new System.Drawing.Size(298, 28);
            this.tb_mothers_name.TabIndex = 6;
            // 
            // tb_occupation_mother
            // 
            this.tb_occupation_mother.BackColor = System.Drawing.Color.White;
            this.tb_occupation_mother.Enabled = false;
            this.tb_occupation_mother.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.tb_occupation_mother.ForeColor = System.Drawing.Color.Black;
            this.tb_occupation_mother.Location = new System.Drawing.Point(200, 86);
            this.tb_occupation_mother.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.tb_occupation_mother.Name = "tb_occupation_mother";
            this.tb_occupation_mother.Size = new System.Drawing.Size(298, 28);
            this.tb_occupation_mother.TabIndex = 7;
            // 
            // tb_fathers_name
            // 
            this.tb_fathers_name.BackColor = System.Drawing.Color.White;
            this.tb_fathers_name.Enabled = false;
            this.tb_fathers_name.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.tb_fathers_name.ForeColor = System.Drawing.Color.Black;
            this.tb_fathers_name.Location = new System.Drawing.Point(200, 136);
            this.tb_fathers_name.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.tb_fathers_name.Name = "tb_fathers_name";
            this.tb_fathers_name.Size = new System.Drawing.Size(298, 28);
            this.tb_fathers_name.TabIndex = 8;
            // 
            // tb_occupation_father
            // 
            this.tb_occupation_father.BackColor = System.Drawing.Color.White;
            this.tb_occupation_father.Enabled = false;
            this.tb_occupation_father.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.tb_occupation_father.ForeColor = System.Drawing.Color.Black;
            this.tb_occupation_father.Location = new System.Drawing.Point(200, 178);
            this.tb_occupation_father.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.tb_occupation_father.Name = "tb_occupation_father";
            this.tb_occupation_father.Size = new System.Drawing.Size(298, 28);
            this.tb_occupation_father.TabIndex = 9;
            // 
            // label34
            // 
            this.label34.AutoSize = true;
            this.label34.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label34.Location = new System.Drawing.Point(72, 91);
            this.label34.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label34.Name = "label34";
            this.label34.Size = new System.Drawing.Size(106, 22);
            this.label34.TabIndex = 144;
            this.label34.Text = "Occupation:";
            // 
            // label35
            // 
            this.label35.AutoSize = true;
            this.label35.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label35.Location = new System.Drawing.Point(38, 140);
            this.label35.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label35.Name = "label35";
            this.label35.Size = new System.Drawing.Size(141, 22);
            this.label35.TabIndex = 145;
            this.label35.Text = "Fathers\'s Name:";
            // 
            // label73
            // 
            this.label73.AutoSize = true;
            this.label73.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label73.Location = new System.Drawing.Point(72, 183);
            this.label73.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label73.Name = "label73";
            this.label73.Size = new System.Drawing.Size(106, 22);
            this.label73.TabIndex = 146;
            this.label73.Text = "Occupation:";
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label5.Location = new System.Drawing.Point(56, 228);
            this.label5.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(133, 22);
            this.label5.TabIndex = 154;
            this.label5.Text = "Home Address:";
            // 
            // rtb_homeAddress
            // 
            this.rtb_homeAddress.Enabled = false;
            this.rtb_homeAddress.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.rtb_homeAddress.ForeColor = System.Drawing.Color.Black;
            this.rtb_homeAddress.Location = new System.Drawing.Point(198, 224);
            this.rtb_homeAddress.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.rtb_homeAddress.Name = "rtb_homeAddress";
            this.rtb_homeAddress.Size = new System.Drawing.Size(300, 101);
            this.rtb_homeAddress.TabIndex = 10;
            this.rtb_homeAddress.Text = "";
            // 
            // label14
            // 
            this.label14.AutoSize = true;
            this.label14.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label14.Location = new System.Drawing.Point(38, 42);
            this.label14.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label14.Name = "label14";
            this.label14.Size = new System.Drawing.Size(135, 22);
            this.label14.TabIndex = 198;
            this.label14.Text = "Mother\'s Name:";
            // 
            // groupBox13
            // 
            this.groupBox13.Controls.Add(this.label1);
            this.groupBox13.Controls.Add(this.tb_elementary);
            this.groupBox13.Controls.Add(this.label53);
            this.groupBox13.Controls.Add(this.rtb_work_experience);
            this.groupBox13.Controls.Add(this.label48);
            this.groupBox13.Controls.Add(this.label52);
            this.groupBox13.Controls.Add(this.label50);
            this.groupBox13.Controls.Add(this.label114);
            this.groupBox13.Controls.Add(this.tb_highSchool);
            this.groupBox13.Controls.Add(this.tb_tertiary);
            this.groupBox13.Controls.Add(this.tb_course);
            this.groupBox13.Location = new System.Drawing.Point(7, 116);
            this.groupBox13.Name = "groupBox13";
            this.groupBox13.Size = new System.Drawing.Size(506, 445);
            this.groupBox13.TabIndex = 1;
            this.groupBox13.TabStop = false;
            this.groupBox13.Text = "Educational/Work";
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(8, 412);
            this.label1.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(21, 22);
            this.label1.TabIndex = 215;
            this.label1.Text = "0";
            this.label1.Visible = false;
            // 
            // tb_elementary
            // 
            this.tb_elementary.BackColor = System.Drawing.Color.White;
            this.tb_elementary.Enabled = false;
            this.tb_elementary.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.tb_elementary.ForeColor = System.Drawing.Color.Black;
            this.tb_elementary.Location = new System.Drawing.Point(171, 41);
            this.tb_elementary.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.tb_elementary.Name = "tb_elementary";
            this.tb_elementary.Size = new System.Drawing.Size(307, 28);
            this.tb_elementary.TabIndex = 1;
            // 
            // label53
            // 
            this.label53.AutoSize = true;
            this.label53.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label53.Location = new System.Drawing.Point(23, 36);
            this.label53.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label53.Name = "label53";
            this.label53.Size = new System.Drawing.Size(105, 22);
            this.label53.TabIndex = 88;
            this.label53.Text = "Elementary:";
            // 
            // rtb_work_experience
            // 
            this.rtb_work_experience.Enabled = false;
            this.rtb_work_experience.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.rtb_work_experience.ForeColor = System.Drawing.Color.Black;
            this.rtb_work_experience.Location = new System.Drawing.Point(166, 213);
            this.rtb_work_experience.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.rtb_work_experience.Name = "rtb_work_experience";
            this.rtb_work_experience.Size = new System.Drawing.Size(312, 99);
            this.rtb_work_experience.TabIndex = 5;
            this.rtb_work_experience.Text = "";
            // 
            // label48
            // 
            this.label48.AutoSize = true;
            this.label48.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label48.Location = new System.Drawing.Point(12, 216);
            this.label48.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label48.Name = "label48";
            this.label48.Size = new System.Drawing.Size(152, 22);
            this.label48.TabIndex = 100;
            this.label48.Text = "Work Experience:";
            // 
            // label52
            // 
            this.label52.AutoSize = true;
            this.label52.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label52.Location = new System.Drawing.Point(39, 84);
            this.label52.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label52.Name = "label52";
            this.label52.Size = new System.Drawing.Size(112, 22);
            this.label52.TabIndex = 87;
            this.label52.Text = "High School:";
            // 
            // label50
            // 
            this.label50.AutoSize = true;
            this.label50.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label50.Location = new System.Drawing.Point(63, 125);
            this.label50.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label50.Name = "label50";
            this.label50.Size = new System.Drawing.Size(77, 22);
            this.label50.TabIndex = 81;
            this.label50.Text = "Tertiary:";
            // 
            // label114
            // 
            this.label114.AutoSize = true;
            this.label114.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label114.Location = new System.Drawing.Point(63, 166);
            this.label114.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label114.Name = "label114";
            this.label114.Size = new System.Drawing.Size(73, 22);
            this.label114.TabIndex = 214;
            this.label114.Text = "Course:";
            // 
            // tb_highSchool
            // 
            this.tb_highSchool.BackColor = System.Drawing.Color.White;
            this.tb_highSchool.Enabled = false;
            this.tb_highSchool.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.tb_highSchool.ForeColor = System.Drawing.Color.Black;
            this.tb_highSchool.Location = new System.Drawing.Point(171, 84);
            this.tb_highSchool.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.tb_highSchool.Name = "tb_highSchool";
            this.tb_highSchool.Size = new System.Drawing.Size(307, 28);
            this.tb_highSchool.TabIndex = 2;
            // 
            // tb_tertiary
            // 
            this.tb_tertiary.BackColor = System.Drawing.Color.White;
            this.tb_tertiary.Enabled = false;
            this.tb_tertiary.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.tb_tertiary.ForeColor = System.Drawing.Color.Black;
            this.tb_tertiary.Location = new System.Drawing.Point(171, 125);
            this.tb_tertiary.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.tb_tertiary.Name = "tb_tertiary";
            this.tb_tertiary.Size = new System.Drawing.Size(307, 28);
            this.tb_tertiary.TabIndex = 3;
            // 
            // tb_course
            // 
            this.tb_course.BackColor = System.Drawing.Color.White;
            this.tb_course.Enabled = false;
            this.tb_course.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.tb_course.ForeColor = System.Drawing.Color.Black;
            this.tb_course.Location = new System.Drawing.Point(171, 166);
            this.tb_course.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.tb_course.Name = "tb_course";
            this.tb_course.Size = new System.Drawing.Size(307, 28);
            this.tb_course.TabIndex = 4;
            // 
            // label134
            // 
            this.label134.AutoSize = true;
            this.label134.Font = new System.Drawing.Font("Arial", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label134.Location = new System.Drawing.Point(1142, 29);
            this.label134.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label134.Name = "label134";
            this.label134.Size = new System.Drawing.Size(120, 21);
            this.label134.TabIndex = 226;
            this.label134.Text = "Middle Name:";
            // 
            // label135
            // 
            this.label135.AutoSize = true;
            this.label135.Enabled = false;
            this.label135.Font = new System.Drawing.Font("Arial", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label135.Location = new System.Drawing.Point(823, 29);
            this.label135.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label135.Name = "label135";
            this.label135.Size = new System.Drawing.Size(102, 21);
            this.label135.TabIndex = 225;
            this.label135.Text = "First Name:";
            // 
            // label136
            // 
            this.label136.AutoSize = true;
            this.label136.Enabled = false;
            this.label136.Font = new System.Drawing.Font("Arial", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label136.Location = new System.Drawing.Point(507, 32);
            this.label136.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label136.Name = "label136";
            this.label136.Size = new System.Drawing.Size(101, 21);
            this.label136.TabIndex = 224;
            this.label136.Text = "Last Name:";
            // 
            // tb_info_lname
            // 
            this.tb_info_lname.Enabled = false;
            this.tb_info_lname.Font = new System.Drawing.Font("Arial", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.tb_info_lname.ForeColor = System.Drawing.Color.Black;
            this.tb_info_lname.Location = new System.Drawing.Point(511, 59);
            this.tb_info_lname.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.tb_info_lname.Name = "tb_info_lname";
            this.tb_info_lname.Size = new System.Drawing.Size(302, 28);
            this.tb_info_lname.TabIndex = 221;
            this.tb_info_lname.TabStop = false;
            // 
            // tb_info_fname
            // 
            this.tb_info_fname.Enabled = false;
            this.tb_info_fname.Font = new System.Drawing.Font("Arial", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.tb_info_fname.ForeColor = System.Drawing.Color.Black;
            this.tb_info_fname.Location = new System.Drawing.Point(824, 59);
            this.tb_info_fname.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.tb_info_fname.Name = "tb_info_fname";
            this.tb_info_fname.Size = new System.Drawing.Size(302, 28);
            this.tb_info_fname.TabIndex = 222;
            this.tb_info_fname.TabStop = false;
            // 
            // tb_info_mname
            // 
            this.tb_info_mname.Enabled = false;
            this.tb_info_mname.Font = new System.Drawing.Font("Arial", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.tb_info_mname.ForeColor = System.Drawing.Color.Black;
            this.tb_info_mname.Location = new System.Drawing.Point(1137, 59);
            this.tb_info_mname.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.tb_info_mname.Name = "tb_info_mname";
            this.tb_info_mname.Size = new System.Drawing.Size(302, 28);
            this.tb_info_mname.TabIndex = 223;
            this.tb_info_mname.TabStop = false;
            // 
            // tb_info_empID
            // 
            this.tb_info_empID.Enabled = false;
            this.tb_info_empID.Font = new System.Drawing.Font("Arial", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.tb_info_empID.ForeColor = System.Drawing.Color.Black;
            this.tb_info_empID.Location = new System.Drawing.Point(197, 61);
            this.tb_info_empID.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.tb_info_empID.Name = "tb_info_empID";
            this.tb_info_empID.Size = new System.Drawing.Size(304, 28);
            this.tb_info_empID.TabIndex = 219;
            this.tb_info_empID.TabStop = false;
            // 
            // label137
            // 
            this.label137.AutoSize = true;
            this.label137.Enabled = false;
            this.label137.Font = new System.Drawing.Font("Arial", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label137.Location = new System.Drawing.Point(194, 32);
            this.label137.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label137.Name = "label137";
            this.label137.Size = new System.Drawing.Size(116, 21);
            this.label137.TabIndex = 220;
            this.label137.Text = "Employee ID:";
            // 
            // tp_loan
            // 
            this.tp_loan.Controls.Add(this.lbl_comp_loan);
            this.tp_loan.Location = new System.Drawing.Point(4, 31);
            this.tp_loan.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.tp_loan.Name = "tp_loan";
            this.tp_loan.Size = new System.Drawing.Size(1624, 584);
            this.tp_loan.TabIndex = 6;
            this.tp_loan.Text = "Loan";
            this.tp_loan.UseVisualStyleBackColor = true;
            this.tp_loan.Enter += new System.EventHandler(this.tp_loan_Enter);
            // 
            // lbl_comp_loan
            // 
            this.lbl_comp_loan.Controls.Add(this.label109);
            this.lbl_comp_loan.Controls.Add(this.label120);
            this.lbl_comp_loan.Controls.Add(this.label57);
            this.lbl_comp_loan.Controls.Add(this.label107);
            this.lbl_comp_loan.Controls.Add(this.label108);
            this.lbl_comp_loan.Controls.Add(this.tb_loan_lname);
            this.lbl_comp_loan.Controls.Add(this.tb_loan_fname);
            this.lbl_comp_loan.Controls.Add(this.tb_loan_mname);
            this.lbl_comp_loan.Controls.Add(this.tb_loan_empID);
            this.lbl_comp_loan.Controls.Add(this.label110);
            this.lbl_comp_loan.Controls.Add(this.groupBox7);
            this.lbl_comp_loan.Controls.Add(this.groupBox9);
            this.lbl_comp_loan.Controls.Add(this.groupBox10);
            this.lbl_comp_loan.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lbl_comp_loan.Location = new System.Drawing.Point(9, 5);
            this.lbl_comp_loan.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.lbl_comp_loan.Name = "lbl_comp_loan";
            this.lbl_comp_loan.Padding = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.lbl_comp_loan.Size = new System.Drawing.Size(1611, 564);
            this.lbl_comp_loan.TabIndex = 81;
            this.lbl_comp_loan.TabStop = false;
            this.lbl_comp_loan.Text = "Loan";
            // 
            // label109
            // 
            this.label109.AutoSize = true;
            this.label109.Font = new System.Drawing.Font("Arial", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label109.Location = new System.Drawing.Point(1077, 42);
            this.label109.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label109.Name = "label109";
            this.label109.Size = new System.Drawing.Size(120, 21);
            this.label109.TabIndex = 201;
            this.label109.Text = "Middle Name:";
            // 
            // label120
            // 
            this.label120.AutoSize = true;
            this.label120.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label120.Location = new System.Drawing.Point(6, 55);
            this.label120.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label120.Name = "label120";
            this.label120.Size = new System.Drawing.Size(21, 22);
            this.label120.TabIndex = 200;
            this.label120.Text = "0";
            this.label120.Visible = false;
            // 
            // label57
            // 
            this.label57.AutoSize = true;
            this.label57.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label57.Location = new System.Drawing.Point(6, 25);
            this.label57.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label57.Name = "label57";
            this.label57.Size = new System.Drawing.Size(21, 22);
            this.label57.TabIndex = 157;
            this.label57.Text = "0";
            this.label57.Visible = false;
            // 
            // label107
            // 
            this.label107.AutoSize = true;
            this.label107.Enabled = false;
            this.label107.Font = new System.Drawing.Font("Arial", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label107.Location = new System.Drawing.Point(758, 42);
            this.label107.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label107.Name = "label107";
            this.label107.Size = new System.Drawing.Size(102, 21);
            this.label107.TabIndex = 151;
            this.label107.Text = "First Name:";
            // 
            // label108
            // 
            this.label108.AutoSize = true;
            this.label108.Enabled = false;
            this.label108.Font = new System.Drawing.Font("Arial", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label108.Location = new System.Drawing.Point(442, 45);
            this.label108.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label108.Name = "label108";
            this.label108.Size = new System.Drawing.Size(101, 21);
            this.label108.TabIndex = 150;
            this.label108.Text = "Last Name:";
            // 
            // tb_loan_lname
            // 
            this.tb_loan_lname.Enabled = false;
            this.tb_loan_lname.Font = new System.Drawing.Font("Arial", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.tb_loan_lname.ForeColor = System.Drawing.Color.Black;
            this.tb_loan_lname.Location = new System.Drawing.Point(446, 72);
            this.tb_loan_lname.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.tb_loan_lname.Name = "tb_loan_lname";
            this.tb_loan_lname.Size = new System.Drawing.Size(302, 28);
            this.tb_loan_lname.TabIndex = 147;
            this.tb_loan_lname.TabStop = false;
            // 
            // tb_loan_fname
            // 
            this.tb_loan_fname.Enabled = false;
            this.tb_loan_fname.Font = new System.Drawing.Font("Arial", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.tb_loan_fname.ForeColor = System.Drawing.Color.Black;
            this.tb_loan_fname.Location = new System.Drawing.Point(759, 72);
            this.tb_loan_fname.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.tb_loan_fname.Name = "tb_loan_fname";
            this.tb_loan_fname.Size = new System.Drawing.Size(302, 28);
            this.tb_loan_fname.TabIndex = 148;
            this.tb_loan_fname.TabStop = false;
            // 
            // tb_loan_mname
            // 
            this.tb_loan_mname.Enabled = false;
            this.tb_loan_mname.Font = new System.Drawing.Font("Arial", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.tb_loan_mname.ForeColor = System.Drawing.Color.Black;
            this.tb_loan_mname.Location = new System.Drawing.Point(1072, 72);
            this.tb_loan_mname.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.tb_loan_mname.Name = "tb_loan_mname";
            this.tb_loan_mname.Size = new System.Drawing.Size(302, 28);
            this.tb_loan_mname.TabIndex = 149;
            this.tb_loan_mname.TabStop = false;
            // 
            // tb_loan_empID
            // 
            this.tb_loan_empID.Enabled = false;
            this.tb_loan_empID.Font = new System.Drawing.Font("Arial", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.tb_loan_empID.ForeColor = System.Drawing.Color.Black;
            this.tb_loan_empID.Location = new System.Drawing.Point(132, 74);
            this.tb_loan_empID.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.tb_loan_empID.Name = "tb_loan_empID";
            this.tb_loan_empID.Size = new System.Drawing.Size(304, 28);
            this.tb_loan_empID.TabIndex = 145;
            this.tb_loan_empID.TabStop = false;
            // 
            // label110
            // 
            this.label110.AutoSize = true;
            this.label110.Enabled = false;
            this.label110.Font = new System.Drawing.Font("Arial", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label110.Location = new System.Drawing.Point(129, 45);
            this.label110.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label110.Name = "label110";
            this.label110.Size = new System.Drawing.Size(116, 21);
            this.label110.TabIndex = 146;
            this.label110.Text = "Employee ID:";
            // 
            // groupBox7
            // 
            this.groupBox7.Controls.Add(this.label89);
            this.groupBox7.Controls.Add(this.label60);
            this.groupBox7.Controls.Add(this.tb_comp_loan_month_ded);
            this.groupBox7.Controls.Add(this.tb_comp_loan_bal);
            this.groupBox7.Controls.Add(this.label67);
            this.groupBox7.Controls.Add(this.tb_comp_loan_amnt);
            this.groupBox7.Controls.Add(this.label61);
            this.groupBox7.Font = new System.Drawing.Font("Arial", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.groupBox7.Location = new System.Drawing.Point(16, 115);
            this.groupBox7.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.groupBox7.Name = "groupBox7";
            this.groupBox7.Padding = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.groupBox7.Size = new System.Drawing.Size(494, 428);
            this.groupBox7.TabIndex = 1;
            this.groupBox7.TabStop = false;
            this.groupBox7.Text = "Company Loan";
            // 
            // label89
            // 
            this.label89.AutoSize = true;
            this.label89.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label89.Location = new System.Drawing.Point(14, 402);
            this.label89.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label89.Name = "label89";
            this.label89.Size = new System.Drawing.Size(21, 22);
            this.label89.TabIndex = 202;
            this.label89.Text = "0";
            this.label89.UseWaitCursor = true;
            this.label89.Visible = false;
            // 
            // label60
            // 
            this.label60.AutoSize = true;
            this.label60.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label60.Location = new System.Drawing.Point(14, 244);
            this.label60.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label60.Name = "label60";
            this.label60.Size = new System.Drawing.Size(113, 22);
            this.label60.TabIndex = 203;
            this.label60.Text = "Payday Ded:";
            // 
            // tb_comp_loan_month_ded
            // 
            this.tb_comp_loan_month_ded.Enabled = false;
            this.tb_comp_loan_month_ded.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.tb_comp_loan_month_ded.ForeColor = System.Drawing.Color.Black;
            this.tb_comp_loan_month_ded.Location = new System.Drawing.Point(156, 239);
            this.tb_comp_loan_month_ded.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.tb_comp_loan_month_ded.Name = "tb_comp_loan_month_ded";
            this.tb_comp_loan_month_ded.Size = new System.Drawing.Size(307, 28);
            this.tb_comp_loan_month_ded.TabIndex = 3;
            this.tb_comp_loan_month_ded.Text = "0.00";
            this.tb_comp_loan_month_ded.TextChanged += new System.EventHandler(this.tb_comp_loan_month_ded_TextChanged);
            this.tb_comp_loan_month_ded.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.tb_monthly_ded_KeyPress);
            // 
            // tb_comp_loan_bal
            // 
            this.tb_comp_loan_bal.Enabled = false;
            this.tb_comp_loan_bal.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.tb_comp_loan_bal.ForeColor = System.Drawing.Color.Black;
            this.tb_comp_loan_bal.Location = new System.Drawing.Point(156, 174);
            this.tb_comp_loan_bal.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.tb_comp_loan_bal.Name = "tb_comp_loan_bal";
            this.tb_comp_loan_bal.Size = new System.Drawing.Size(306, 28);
            this.tb_comp_loan_bal.TabIndex = 2;
            this.tb_comp_loan_bal.Text = "0.00";
            this.tb_comp_loan_bal.TextChanged += new System.EventHandler(this.tb_comp_loan_bal_TextChanged);
            this.tb_comp_loan_bal.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.tb_loan_bal_KeyPress);
            // 
            // label67
            // 
            this.label67.AutoSize = true;
            this.label67.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label67.Location = new System.Drawing.Point(2, 178);
            this.label67.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label67.Name = "label67";
            this.label67.Size = new System.Drawing.Size(125, 22);
            this.label67.TabIndex = 65;
            this.label67.Text = "Loan Balance:";
            // 
            // tb_comp_loan_amnt
            // 
            this.tb_comp_loan_amnt.Enabled = false;
            this.tb_comp_loan_amnt.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.tb_comp_loan_amnt.ForeColor = System.Drawing.Color.Black;
            this.tb_comp_loan_amnt.Location = new System.Drawing.Point(158, 104);
            this.tb_comp_loan_amnt.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.tb_comp_loan_amnt.Name = "tb_comp_loan_amnt";
            this.tb_comp_loan_amnt.Size = new System.Drawing.Size(306, 28);
            this.tb_comp_loan_amnt.TabIndex = 1;
            this.tb_comp_loan_amnt.Text = "0.00";
            this.tb_comp_loan_amnt.TextChanged += new System.EventHandler(this.tb_comp_loan_amount_TextChanged);
            this.tb_comp_loan_amnt.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.tb_loan_amount_KeyPress);
            // 
            // label61
            // 
            this.label61.AutoSize = true;
            this.label61.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label61.Location = new System.Drawing.Point(10, 104);
            this.label61.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label61.Name = "label61";
            this.label61.Size = new System.Drawing.Size(121, 22);
            this.label61.TabIndex = 1;
            this.label61.Text = "Loan Amount:";
            // 
            // groupBox9
            // 
            this.groupBox9.Controls.Add(this.label94);
            this.groupBox9.Controls.Add(this.tb_sss_loan_month_ded);
            this.groupBox9.Controls.Add(this.label78);
            this.groupBox9.Controls.Add(this.tb_sss_loan_bal);
            this.groupBox9.Controls.Add(this.tb_sss_loan_amnt);
            this.groupBox9.Controls.Add(this.label66);
            this.groupBox9.Font = new System.Drawing.Font("Arial", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.groupBox9.Location = new System.Drawing.Point(526, 118);
            this.groupBox9.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.groupBox9.Name = "groupBox9";
            this.groupBox9.Padding = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.groupBox9.Size = new System.Drawing.Size(519, 425);
            this.groupBox9.TabIndex = 2;
            this.groupBox9.TabStop = false;
            this.groupBox9.Text = "SSS Loan";
            // 
            // label94
            // 
            this.label94.AutoSize = true;
            this.label94.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label94.Location = new System.Drawing.Point(24, 241);
            this.label94.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label94.Name = "label94";
            this.label94.Size = new System.Drawing.Size(113, 22);
            this.label94.TabIndex = 205;
            this.label94.Text = "Payday Ded:";
            // 
            // tb_sss_loan_month_ded
            // 
            this.tb_sss_loan_month_ded.Enabled = false;
            this.tb_sss_loan_month_ded.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.tb_sss_loan_month_ded.ForeColor = System.Drawing.Color.Black;
            this.tb_sss_loan_month_ded.Location = new System.Drawing.Point(164, 236);
            this.tb_sss_loan_month_ded.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.tb_sss_loan_month_ded.Name = "tb_sss_loan_month_ded";
            this.tb_sss_loan_month_ded.Size = new System.Drawing.Size(306, 28);
            this.tb_sss_loan_month_ded.TabIndex = 6;
            this.tb_sss_loan_month_ded.Text = "0.00";
            this.tb_sss_loan_month_ded.TextChanged += new System.EventHandler(this.tb_sss_loan_month_ded_TextChanged);
            this.tb_sss_loan_month_ded.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.tb_sss_loan_month_ded_KeyPress);
            // 
            // label78
            // 
            this.label78.AutoSize = true;
            this.label78.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label78.Location = new System.Drawing.Point(14, 171);
            this.label78.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label78.Name = "label78";
            this.label78.Size = new System.Drawing.Size(125, 22);
            this.label78.TabIndex = 163;
            this.label78.Text = "Loan Balance:";
            // 
            // tb_sss_loan_bal
            // 
            this.tb_sss_loan_bal.Enabled = false;
            this.tb_sss_loan_bal.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.tb_sss_loan_bal.ForeColor = System.Drawing.Color.Black;
            this.tb_sss_loan_bal.Location = new System.Drawing.Point(164, 168);
            this.tb_sss_loan_bal.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.tb_sss_loan_bal.Name = "tb_sss_loan_bal";
            this.tb_sss_loan_bal.Size = new System.Drawing.Size(306, 28);
            this.tb_sss_loan_bal.TabIndex = 5;
            this.tb_sss_loan_bal.Text = "0.00";
            this.tb_sss_loan_bal.TextChanged += new System.EventHandler(this.tb_sss_loan_bal_TextChanged);
            this.tb_sss_loan_bal.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.tb_sss_loan_bal_KeyPress);
            // 
            // tb_sss_loan_amnt
            // 
            this.tb_sss_loan_amnt.Enabled = false;
            this.tb_sss_loan_amnt.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.tb_sss_loan_amnt.ForeColor = System.Drawing.Color.Black;
            this.tb_sss_loan_amnt.Location = new System.Drawing.Point(164, 96);
            this.tb_sss_loan_amnt.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.tb_sss_loan_amnt.Name = "tb_sss_loan_amnt";
            this.tb_sss_loan_amnt.Size = new System.Drawing.Size(306, 28);
            this.tb_sss_loan_amnt.TabIndex = 4;
            this.tb_sss_loan_amnt.Text = "0.00";
            this.tb_sss_loan_amnt.TextChanged += new System.EventHandler(this.tb_sss_loan_amount_TextChanged);
            this.tb_sss_loan_amnt.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.tb_sss_loan_amount_KeyPress);
            // 
            // label66
            // 
            this.label66.AutoSize = true;
            this.label66.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label66.Location = new System.Drawing.Point(21, 102);
            this.label66.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label66.Name = "label66";
            this.label66.Size = new System.Drawing.Size(121, 22);
            this.label66.TabIndex = 158;
            this.label66.Text = "Loan Amount:";
            // 
            // groupBox10
            // 
            this.groupBox10.Controls.Add(this.label68);
            this.groupBox10.Controls.Add(this.tb_pag_ibig_loan_month_ded);
            this.groupBox10.Controls.Add(this.tb_pag_ibig_loan_bal);
            this.groupBox10.Controls.Add(this.tb_pag_ibig_loan_amnt);
            this.groupBox10.Controls.Add(this.label81);
            this.groupBox10.Controls.Add(this.label82);
            this.groupBox10.Font = new System.Drawing.Font("Arial", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.groupBox10.Location = new System.Drawing.Point(1063, 118);
            this.groupBox10.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.groupBox10.Name = "groupBox10";
            this.groupBox10.Padding = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.groupBox10.Size = new System.Drawing.Size(525, 425);
            this.groupBox10.TabIndex = 3;
            this.groupBox10.TabStop = false;
            this.groupBox10.Text = "Pag-Ibig Loan";
            // 
            // label68
            // 
            this.label68.AutoSize = true;
            this.label68.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label68.Location = new System.Drawing.Point(34, 239);
            this.label68.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label68.Name = "label68";
            this.label68.Size = new System.Drawing.Size(113, 22);
            this.label68.TabIndex = 204;
            this.label68.Text = "Payday Ded:";
            // 
            // tb_pag_ibig_loan_month_ded
            // 
            this.tb_pag_ibig_loan_month_ded.Enabled = false;
            this.tb_pag_ibig_loan_month_ded.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.tb_pag_ibig_loan_month_ded.ForeColor = System.Drawing.Color.Black;
            this.tb_pag_ibig_loan_month_ded.Location = new System.Drawing.Point(174, 235);
            this.tb_pag_ibig_loan_month_ded.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.tb_pag_ibig_loan_month_ded.Name = "tb_pag_ibig_loan_month_ded";
            this.tb_pag_ibig_loan_month_ded.Size = new System.Drawing.Size(307, 28);
            this.tb_pag_ibig_loan_month_ded.TabIndex = 9;
            this.tb_pag_ibig_loan_month_ded.Text = "0.00";
            this.tb_pag_ibig_loan_month_ded.TextChanged += new System.EventHandler(this.tb_pag_ibig_loan_month_ded_TextChanged);
            this.tb_pag_ibig_loan_month_ded.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.tb_pag_ibig_loan_month_ded_KeyPress);
            // 
            // tb_pag_ibig_loan_bal
            // 
            this.tb_pag_ibig_loan_bal.Enabled = false;
            this.tb_pag_ibig_loan_bal.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.tb_pag_ibig_loan_bal.ForeColor = System.Drawing.Color.Black;
            this.tb_pag_ibig_loan_bal.Location = new System.Drawing.Point(174, 165);
            this.tb_pag_ibig_loan_bal.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.tb_pag_ibig_loan_bal.Name = "tb_pag_ibig_loan_bal";
            this.tb_pag_ibig_loan_bal.Size = new System.Drawing.Size(306, 28);
            this.tb_pag_ibig_loan_bal.TabIndex = 8;
            this.tb_pag_ibig_loan_bal.Text = "0.00";
            this.tb_pag_ibig_loan_bal.TextChanged += new System.EventHandler(this.tb_pag_ibig_loan_bal_TextChanged);
            this.tb_pag_ibig_loan_bal.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.tb_pag_ibig_loan_bal_KeyPress);
            // 
            // tb_pag_ibig_loan_amnt
            // 
            this.tb_pag_ibig_loan_amnt.Enabled = false;
            this.tb_pag_ibig_loan_amnt.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.tb_pag_ibig_loan_amnt.ForeColor = System.Drawing.Color.Black;
            this.tb_pag_ibig_loan_amnt.Location = new System.Drawing.Point(174, 92);
            this.tb_pag_ibig_loan_amnt.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.tb_pag_ibig_loan_amnt.Name = "tb_pag_ibig_loan_amnt";
            this.tb_pag_ibig_loan_amnt.Size = new System.Drawing.Size(306, 28);
            this.tb_pag_ibig_loan_amnt.TabIndex = 7;
            this.tb_pag_ibig_loan_amnt.Text = "0.00";
            this.tb_pag_ibig_loan_amnt.TextChanged += new System.EventHandler(this.tb_pag_ibig_loan_amnt_TextChanged);
            this.tb_pag_ibig_loan_amnt.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.tb_pag_ibig_loan_amnt_KeyPress);
            // 
            // label81
            // 
            this.label81.AutoSize = true;
            this.label81.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label81.Location = new System.Drawing.Point(22, 95);
            this.label81.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label81.Name = "label81";
            this.label81.Size = new System.Drawing.Size(121, 22);
            this.label81.TabIndex = 167;
            this.label81.Text = "Loan Amount:";
            // 
            // label82
            // 
            this.label82.AutoSize = true;
            this.label82.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label82.Location = new System.Drawing.Point(24, 170);
            this.label82.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label82.Name = "label82";
            this.label82.Size = new System.Drawing.Size(125, 22);
            this.label82.TabIndex = 172;
            this.label82.Text = "Loan Balance:";
            // 
            // tp_Leave
            // 
            this.tp_Leave.Controls.Add(this.groupBox8);
            this.tp_Leave.Location = new System.Drawing.Point(4, 31);
            this.tp_Leave.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.tp_Leave.Name = "tp_Leave";
            this.tp_Leave.Size = new System.Drawing.Size(1624, 584);
            this.tp_Leave.TabIndex = 7;
            this.tp_Leave.Text = "Leave";
            this.tp_Leave.UseVisualStyleBackColor = true;
            this.tp_Leave.Enter += new System.EventHandler(this.tp_Leave_Enter);
            // 
            // groupBox8
            // 
            this.groupBox8.Controls.Add(this.label79);
            this.groupBox8.Controls.Add(this.label87);
            this.groupBox8.Controls.Add(this.label31);
            this.groupBox8.Controls.Add(this.label45);
            this.groupBox8.Controls.Add(this.label85);
            this.groupBox8.Controls.Add(this.label86);
            this.groupBox8.Controls.Add(this.tb_leave_lname);
            this.groupBox8.Controls.Add(this.tb_leave_fname);
            this.groupBox8.Controls.Add(this.tb_leave_mname);
            this.groupBox8.Controls.Add(this.tb_leave_empID);
            this.groupBox8.Controls.Add(this.label84);
            this.groupBox8.Controls.Add(this.tb_vacation_leave);
            this.groupBox8.Controls.Add(this.label70);
            this.groupBox8.Controls.Add(this.label71);
            this.groupBox8.Controls.Add(this.tb_sick_leave);
            this.groupBox8.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.groupBox8.Location = new System.Drawing.Point(12, 5);
            this.groupBox8.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.groupBox8.Name = "groupBox8";
            this.groupBox8.Padding = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.groupBox8.Size = new System.Drawing.Size(1595, 560);
            this.groupBox8.TabIndex = 5;
            this.groupBox8.TabStop = false;
            this.groupBox8.Text = "Leave";
            // 
            // label79
            // 
            this.label79.AutoSize = true;
            this.label79.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label79.Location = new System.Drawing.Point(8, 525);
            this.label79.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label79.Name = "label79";
            this.label79.Size = new System.Drawing.Size(21, 22);
            this.label79.TabIndex = 174;
            this.label79.Text = "0";
            this.label79.Visible = false;
            // 
            // label87
            // 
            this.label87.AutoSize = true;
            this.label87.Font = new System.Drawing.Font("Arial", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label87.Location = new System.Drawing.Point(240, 213);
            this.label87.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label87.Name = "label87";
            this.label87.Size = new System.Drawing.Size(120, 21);
            this.label87.TabIndex = 159;
            this.label87.Text = "Middle Name:";
            // 
            // label31
            // 
            this.label31.AutoSize = true;
            this.label31.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label31.Location = new System.Drawing.Point(9, 58);
            this.label31.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label31.Name = "label31";
            this.label31.Size = new System.Drawing.Size(21, 22);
            this.label31.TabIndex = 158;
            this.label31.Text = "0";
            this.label31.Visible = false;
            // 
            // label45
            // 
            this.label45.AutoSize = true;
            this.label45.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label45.Location = new System.Drawing.Point(9, 28);
            this.label45.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label45.Name = "label45";
            this.label45.Size = new System.Drawing.Size(21, 22);
            this.label45.TabIndex = 157;
            this.label45.Text = "0";
            this.label45.Visible = false;
            // 
            // label85
            // 
            this.label85.AutoSize = true;
            this.label85.Font = new System.Drawing.Font("Arial", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label85.Location = new System.Drawing.Point(264, 153);
            this.label85.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label85.Name = "label85";
            this.label85.Size = new System.Drawing.Size(102, 21);
            this.label85.TabIndex = 143;
            this.label85.Text = "First Name:";
            // 
            // label86
            // 
            this.label86.AutoSize = true;
            this.label86.Font = new System.Drawing.Font("Arial", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label86.Location = new System.Drawing.Point(266, 107);
            this.label86.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label86.Name = "label86";
            this.label86.Size = new System.Drawing.Size(101, 21);
            this.label86.TabIndex = 142;
            this.label86.Text = "Last Name:";
            // 
            // tb_leave_lname
            // 
            this.tb_leave_lname.Enabled = false;
            this.tb_leave_lname.Font = new System.Drawing.Font("Arial", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.tb_leave_lname.ForeColor = System.Drawing.Color.Gray;
            this.tb_leave_lname.Location = new System.Drawing.Point(399, 104);
            this.tb_leave_lname.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.tb_leave_lname.Name = "tb_leave_lname";
            this.tb_leave_lname.Size = new System.Drawing.Size(302, 28);
            this.tb_leave_lname.TabIndex = 139;
            this.tb_leave_lname.TabStop = false;
            // 
            // tb_leave_fname
            // 
            this.tb_leave_fname.Enabled = false;
            this.tb_leave_fname.Font = new System.Drawing.Font("Arial", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.tb_leave_fname.ForeColor = System.Drawing.Color.Gray;
            this.tb_leave_fname.Location = new System.Drawing.Point(399, 150);
            this.tb_leave_fname.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.tb_leave_fname.Name = "tb_leave_fname";
            this.tb_leave_fname.Size = new System.Drawing.Size(302, 28);
            this.tb_leave_fname.TabIndex = 140;
            this.tb_leave_fname.TabStop = false;
            // 
            // tb_leave_mname
            // 
            this.tb_leave_mname.Enabled = false;
            this.tb_leave_mname.Font = new System.Drawing.Font("Arial", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.tb_leave_mname.ForeColor = System.Drawing.Color.Gray;
            this.tb_leave_mname.Location = new System.Drawing.Point(399, 204);
            this.tb_leave_mname.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.tb_leave_mname.Name = "tb_leave_mname";
            this.tb_leave_mname.Size = new System.Drawing.Size(302, 28);
            this.tb_leave_mname.TabIndex = 141;
            this.tb_leave_mname.TabStop = false;
            // 
            // tb_leave_empID
            // 
            this.tb_leave_empID.Enabled = false;
            this.tb_leave_empID.Font = new System.Drawing.Font("Arial", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.tb_leave_empID.ForeColor = System.Drawing.Color.Gray;
            this.tb_leave_empID.Location = new System.Drawing.Point(398, 58);
            this.tb_leave_empID.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.tb_leave_empID.Name = "tb_leave_empID";
            this.tb_leave_empID.Size = new System.Drawing.Size(304, 28);
            this.tb_leave_empID.TabIndex = 137;
            this.tb_leave_empID.TabStop = false;
            // 
            // label84
            // 
            this.label84.AutoSize = true;
            this.label84.Font = new System.Drawing.Font("Arial", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label84.Location = new System.Drawing.Point(249, 64);
            this.label84.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label84.Name = "label84";
            this.label84.Size = new System.Drawing.Size(116, 21);
            this.label84.TabIndex = 138;
            this.label84.Text = "Employee ID:";
            // 
            // tb_vacation_leave
            // 
            this.tb_vacation_leave.Enabled = false;
            this.tb_vacation_leave.Font = new System.Drawing.Font("Arial", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.tb_vacation_leave.ForeColor = System.Drawing.Color.Black;
            this.tb_vacation_leave.Location = new System.Drawing.Point(879, 104);
            this.tb_vacation_leave.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.tb_vacation_leave.Name = "tb_vacation_leave";
            this.tb_vacation_leave.Size = new System.Drawing.Size(304, 28);
            this.tb_vacation_leave.TabIndex = 2;
            this.tb_vacation_leave.Text = "0.00";
            this.tb_vacation_leave.TextChanged += new System.EventHandler(this.tb_vacation_leave_TextChanged);
            this.tb_vacation_leave.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.tb_vacation_leave_KeyPress);
            // 
            // label70
            // 
            this.label70.AutoSize = true;
            this.label70.Font = new System.Drawing.Font("Arial", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label70.Location = new System.Drawing.Point(712, 113);
            this.label70.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label70.Name = "label70";
            this.label70.Size = new System.Drawing.Size(137, 21);
            this.label70.TabIndex = 2;
            this.label70.Text = "Vacation Leave:";
            // 
            // label71
            // 
            this.label71.AutoSize = true;
            this.label71.Font = new System.Drawing.Font("Arial", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label71.Location = new System.Drawing.Point(753, 67);
            this.label71.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label71.Name = "label71";
            this.label71.Size = new System.Drawing.Size(103, 21);
            this.label71.TabIndex = 3;
            this.label71.Text = "Sick Leave:";
            // 
            // tb_sick_leave
            // 
            this.tb_sick_leave.Enabled = false;
            this.tb_sick_leave.Font = new System.Drawing.Font("Arial", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.tb_sick_leave.ForeColor = System.Drawing.Color.Black;
            this.tb_sick_leave.Location = new System.Drawing.Point(879, 63);
            this.tb_sick_leave.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.tb_sick_leave.Name = "tb_sick_leave";
            this.tb_sick_leave.Size = new System.Drawing.Size(304, 28);
            this.tb_sick_leave.TabIndex = 1;
            this.tb_sick_leave.Text = "0.00";
            this.tb_sick_leave.TextChanged += new System.EventHandler(this.tb_sick_leave_TextChanged);
            this.tb_sick_leave.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.tb_sick_leave_KeyPress);
            // 
            // tp_salary
            // 
            this.tp_salary.Controls.Add(this.groupBox5);
            this.tp_salary.Location = new System.Drawing.Point(4, 31);
            this.tp_salary.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.tp_salary.Name = "tp_salary";
            this.tp_salary.Padding = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.tp_salary.Size = new System.Drawing.Size(1624, 584);
            this.tp_salary.TabIndex = 3;
            this.tp_salary.Text = "Salary";
            this.tp_salary.UseVisualStyleBackColor = true;
            this.tp_salary.Enter += new System.EventHandler(this.tp_salary_Enter);
            // 
            // groupBox5
            // 
            this.groupBox5.Controls.Add(this.groupBox12);
            this.groupBox5.Controls.Add(this.groupBox11);
            this.groupBox5.Controls.Add(this.groupBox4);
            this.groupBox5.Controls.Add(this.groupBox3);
            this.groupBox5.Controls.Add(this.label132);
            this.groupBox5.Controls.Add(this.label128);
            this.groupBox5.Controls.Add(this.label127);
            this.groupBox5.Controls.Add(this.label126);
            this.groupBox5.Controls.Add(this.label121);
            this.groupBox5.Controls.Add(this.label36);
            this.groupBox5.Controls.Add(this.cb_bank_name2);
            this.groupBox5.Controls.Add(this.cb_reason);
            this.groupBox5.Controls.Add(this.label26);
            this.groupBox5.Controls.Add(this.label83);
            this.groupBox5.Controls.Add(this.dtp_dateResign);
            this.groupBox5.Controls.Add(this.label76);
            this.groupBox5.Controls.Add(this.label122);
            this.groupBox5.Controls.Add(this.tb_bank_acc2);
            this.groupBox5.Controls.Add(this.label99);
            this.groupBox5.Controls.Add(this.label118);
            this.groupBox5.Controls.Add(this.cb_department);
            this.groupBox5.Controls.Add(this.label21);
            this.groupBox5.Controls.Add(this.tb_designation);
            this.groupBox5.Controls.Add(this.label20);
            this.groupBox5.Controls.Add(this.dtp_date_hired);
            this.groupBox5.Controls.Add(this.cb_employee_status);
            this.groupBox5.Controls.Add(this.label19);
            this.groupBox5.Controls.Add(this.label18);
            this.groupBox5.Controls.Add(this.label97);
            this.groupBox5.Controls.Add(this.label98);
            this.groupBox5.Controls.Add(this.tb_sal_lname);
            this.groupBox5.Controls.Add(this.tb_sal_fname);
            this.groupBox5.Controls.Add(this.tb_sal_mname);
            this.groupBox5.Controls.Add(this.tb_sal_empID);
            this.groupBox5.Controls.Add(this.label100);
            this.groupBox5.Controls.Add(this.label91);
            this.groupBox5.Controls.Add(this.tb_bank_acc);
            this.groupBox5.Controls.Add(this.label58);
            this.groupBox5.Controls.Add(this.cb_bank_name);
            this.groupBox5.Controls.Add(this.label27);
            this.groupBox5.Controls.Add(this.cb_payroll_mode);
            this.groupBox5.Controls.Add(this.label28);
            this.groupBox5.Controls.Add(this.cb_confidentiality);
            this.groupBox5.Controls.Add(this.label55);
            this.groupBox5.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.groupBox5.Location = new System.Drawing.Point(8, 5);
            this.groupBox5.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.groupBox5.Name = "groupBox5";
            this.groupBox5.Padding = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.groupBox5.Size = new System.Drawing.Size(1608, 568);
            this.groupBox5.TabIndex = 30;
            this.groupBox5.TabStop = false;
            this.groupBox5.Text = "Salary";
            // 
            // groupBox12
            // 
            this.groupBox12.Controls.Add(this.tb_sss);
            this.groupBox12.Controls.Add(this.chk_sss);
            this.groupBox12.Controls.Add(this.chk_pag_ibig);
            this.groupBox12.Controls.Add(this.chk_philhealth);
            this.groupBox12.Controls.Add(this.label32);
            this.groupBox12.Controls.Add(this.tb_philhealth);
            this.groupBox12.Controls.Add(this.label33);
            this.groupBox12.Controls.Add(this.label38);
            this.groupBox12.Controls.Add(this.label115);
            this.groupBox12.Controls.Add(this.label117);
            this.groupBox12.Controls.Add(this.label116);
            this.groupBox12.Controls.Add(this.tb_pag_ibig);
            this.groupBox12.Controls.Add(this.label17);
            this.groupBox12.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.groupBox12.Location = new System.Drawing.Point(1052, 392);
            this.groupBox12.Name = "groupBox12";
            this.groupBox12.Size = new System.Drawing.Size(535, 178);
            this.groupBox12.TabIndex = 16;
            this.groupBox12.TabStop = false;
            this.groupBox12.Text = "Deductions";
            // 
            // tb_sss
            // 
            this.tb_sss.Enabled = false;
            this.tb_sss.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.tb_sss.ForeColor = System.Drawing.Color.Black;
            this.tb_sss.Location = new System.Drawing.Point(183, 30);
            this.tb_sss.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.tb_sss.Name = "tb_sss";
            this.tb_sss.Size = new System.Drawing.Size(306, 28);
            this.tb_sss.TabIndex = 30;
            this.tb_sss.Text = "0.00";
            this.tb_sss.TextChanged += new System.EventHandler(this.tb_sss_TextChanged);
            this.tb_sss.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.tb_sss_KeyPress);
            // 
            // chk_sss
            // 
            this.chk_sss.AutoSize = true;
            this.chk_sss.Enabled = false;
            this.chk_sss.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.chk_sss.Location = new System.Drawing.Point(497, 29);
            this.chk_sss.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.chk_sss.Name = "chk_sss";
            this.chk_sss.Size = new System.Drawing.Size(22, 21);
            this.chk_sss.TabIndex = 31;
            this.chk_sss.UseVisualStyleBackColor = true;
            this.chk_sss.CheckedChanged += new System.EventHandler(this.chk_sss_CheckedChanged);
            // 
            // chk_pag_ibig
            // 
            this.chk_pag_ibig.AutoSize = true;
            this.chk_pag_ibig.Enabled = false;
            this.chk_pag_ibig.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.chk_pag_ibig.Location = new System.Drawing.Point(497, 72);
            this.chk_pag_ibig.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.chk_pag_ibig.Name = "chk_pag_ibig";
            this.chk_pag_ibig.Size = new System.Drawing.Size(22, 21);
            this.chk_pag_ibig.TabIndex = 33;
            this.chk_pag_ibig.UseVisualStyleBackColor = true;
            this.chk_pag_ibig.CheckedChanged += new System.EventHandler(this.chk_pag_ibig_CheckedChanged);
            // 
            // chk_philhealth
            // 
            this.chk_philhealth.AutoSize = true;
            this.chk_philhealth.Enabled = false;
            this.chk_philhealth.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.chk_philhealth.Location = new System.Drawing.Point(497, 124);
            this.chk_philhealth.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.chk_philhealth.Name = "chk_philhealth";
            this.chk_philhealth.Size = new System.Drawing.Size(22, 21);
            this.chk_philhealth.TabIndex = 35;
            this.chk_philhealth.UseVisualStyleBackColor = true;
            this.chk_philhealth.CheckedChanged += new System.EventHandler(this.chk_philhealth_CheckedChanged);
            // 
            // label32
            // 
            this.label32.AutoSize = true;
            this.label32.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label32.Location = new System.Drawing.Point(122, 33);
            this.label32.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label32.Name = "label32";
            this.label32.Size = new System.Drawing.Size(51, 22);
            this.label32.TabIndex = 156;
            this.label32.Text = "SSS:";
            // 
            // tb_philhealth
            // 
            this.tb_philhealth.Enabled = false;
            this.tb_philhealth.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.tb_philhealth.ForeColor = System.Drawing.Color.Black;
            this.tb_philhealth.Location = new System.Drawing.Point(184, 119);
            this.tb_philhealth.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.tb_philhealth.Name = "tb_philhealth";
            this.tb_philhealth.Size = new System.Drawing.Size(304, 28);
            this.tb_philhealth.TabIndex = 34;
            this.tb_philhealth.Text = "0.00";
            this.tb_philhealth.TextChanged += new System.EventHandler(this.tb_philhealth_TextChanged);
            this.tb_philhealth.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.tb_philhealth_KeyPress);
            // 
            // label33
            // 
            this.label33.AutoSize = true;
            this.label33.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label33.Location = new System.Drawing.Point(107, 79);
            this.label33.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label33.Name = "label33";
            this.label33.Size = new System.Drawing.Size(66, 22);
            this.label33.TabIndex = 158;
            this.label33.Text = "HDMF:";
            // 
            // label38
            // 
            this.label38.AutoSize = true;
            this.label38.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label38.Location = new System.Drawing.Point(79, 125);
            this.label38.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label38.Name = "label38";
            this.label38.Size = new System.Drawing.Size(94, 22);
            this.label38.TabIndex = 160;
            this.label38.Text = "Philhealth:";
            // 
            // label115
            // 
            this.label115.AutoSize = true;
            this.label115.Font = new System.Drawing.Font("Microsoft Sans Serif", 8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label115.Location = new System.Drawing.Point(49, 28);
            this.label115.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label115.Name = "label115";
            this.label115.Size = new System.Drawing.Size(18, 20);
            this.label115.TabIndex = 196;
            this.label115.Text = "0";
            this.label115.Visible = false;
            // 
            // label117
            // 
            this.label117.AutoSize = true;
            this.label117.Font = new System.Drawing.Font("Microsoft Sans Serif", 8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label117.Location = new System.Drawing.Point(48, 127);
            this.label117.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label117.Name = "label117";
            this.label117.Size = new System.Drawing.Size(18, 20);
            this.label117.TabIndex = 198;
            this.label117.Text = "0";
            this.label117.Visible = false;
            // 
            // label116
            // 
            this.label116.AutoSize = true;
            this.label116.Font = new System.Drawing.Font("Microsoft Sans Serif", 8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label116.Location = new System.Drawing.Point(48, 74);
            this.label116.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label116.Name = "label116";
            this.label116.Size = new System.Drawing.Size(18, 20);
            this.label116.TabIndex = 197;
            this.label116.Text = "0";
            this.label116.Visible = false;
            // 
            // tb_pag_ibig
            // 
            this.tb_pag_ibig.Enabled = false;
            this.tb_pag_ibig.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.tb_pag_ibig.ForeColor = System.Drawing.Color.Black;
            this.tb_pag_ibig.Location = new System.Drawing.Point(184, 75);
            this.tb_pag_ibig.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.tb_pag_ibig.Name = "tb_pag_ibig";
            this.tb_pag_ibig.Size = new System.Drawing.Size(304, 28);
            this.tb_pag_ibig.TabIndex = 32;
            this.tb_pag_ibig.Text = "0.00";
            this.tb_pag_ibig.TextChanged += new System.EventHandler(this.tb_pag_ibig_TextChanged);
            this.tb_pag_ibig.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.tb_pag_ibig_KeyPress);
            // 
            // label17
            // 
            this.label17.AutoSize = true;
            this.label17.Font = new System.Drawing.Font("Microsoft Sans Serif", 8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label17.Location = new System.Drawing.Point(47, 147);
            this.label17.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label17.Name = "label17";
            this.label17.Size = new System.Drawing.Size(18, 20);
            this.label17.TabIndex = 166;
            this.label17.Text = "0";
            this.label17.Visible = false;
            // 
            // groupBox11
            // 
            this.groupBox11.Controls.Add(this.tb_13th_month_pay);
            this.groupBox11.Controls.Add(this.chb_13month_pay);
            this.groupBox11.Controls.Add(this.chb_14month_pay);
            this.groupBox11.Controls.Add(this.chb_15month_pay);
            this.groupBox11.Controls.Add(this.tb_14th_month_pay);
            this.groupBox11.Controls.Add(this.label12);
            this.groupBox11.Controls.Add(this.label74);
            this.groupBox11.Controls.Add(this.tb_15th_month_pay);
            this.groupBox11.Controls.Add(this.label75);
            this.groupBox11.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.groupBox11.Location = new System.Drawing.Point(1054, 225);
            this.groupBox11.Name = "groupBox11";
            this.groupBox11.Size = new System.Drawing.Size(533, 162);
            this.groupBox11.TabIndex = 15;
            this.groupBox11.TabStop = false;
            this.groupBox11.Text = "Bonus";
            // 
            // tb_13th_month_pay
            // 
            this.tb_13th_month_pay.Enabled = false;
            this.tb_13th_month_pay.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.tb_13th_month_pay.ForeColor = System.Drawing.Color.Black;
            this.tb_13th_month_pay.Location = new System.Drawing.Point(181, 28);
            this.tb_13th_month_pay.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.tb_13th_month_pay.Name = "tb_13th_month_pay";
            this.tb_13th_month_pay.Size = new System.Drawing.Size(304, 28);
            this.tb_13th_month_pay.TabIndex = 24;
            this.tb_13th_month_pay.Text = "0.00";
            this.tb_13th_month_pay.TextChanged += new System.EventHandler(this.tb_13th_month_pay_TextChanged);
            this.tb_13th_month_pay.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.tb_13th_month_pay_KeyPress);
            // 
            // chb_13month_pay
            // 
            this.chb_13month_pay.AutoSize = true;
            this.chb_13month_pay.Enabled = false;
            this.chb_13month_pay.Font = new System.Drawing.Font("Microsoft Sans Serif", 8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.chb_13month_pay.Location = new System.Drawing.Point(494, 30);
            this.chb_13month_pay.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.chb_13month_pay.Name = "chb_13month_pay";
            this.chb_13month_pay.Size = new System.Drawing.Size(22, 21);
            this.chb_13month_pay.TabIndex = 25;
            this.chb_13month_pay.UseVisualStyleBackColor = true;
            this.chb_13month_pay.CheckedChanged += new System.EventHandler(this.chb_13month_pay_CheckedChanged);
            // 
            // chb_14month_pay
            // 
            this.chb_14month_pay.AutoSize = true;
            this.chb_14month_pay.Enabled = false;
            this.chb_14month_pay.Font = new System.Drawing.Font("Microsoft Sans Serif", 8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.chb_14month_pay.Location = new System.Drawing.Point(493, 69);
            this.chb_14month_pay.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.chb_14month_pay.Name = "chb_14month_pay";
            this.chb_14month_pay.Size = new System.Drawing.Size(22, 21);
            this.chb_14month_pay.TabIndex = 27;
            this.chb_14month_pay.UseVisualStyleBackColor = true;
            this.chb_14month_pay.CheckedChanged += new System.EventHandler(this.chb_14month_pay_CheckedChanged);
            // 
            // chb_15month_pay
            // 
            this.chb_15month_pay.AutoSize = true;
            this.chb_15month_pay.Enabled = false;
            this.chb_15month_pay.Font = new System.Drawing.Font("Microsoft Sans Serif", 8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.chb_15month_pay.Location = new System.Drawing.Point(494, 118);
            this.chb_15month_pay.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.chb_15month_pay.Name = "chb_15month_pay";
            this.chb_15month_pay.Size = new System.Drawing.Size(22, 21);
            this.chb_15month_pay.TabIndex = 29;
            this.chb_15month_pay.UseVisualStyleBackColor = true;
            this.chb_15month_pay.CheckedChanged += new System.EventHandler(this.chb_15month_pay_CheckedChanged);
            // 
            // tb_14th_month_pay
            // 
            this.tb_14th_month_pay.Enabled = false;
            this.tb_14th_month_pay.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.tb_14th_month_pay.ForeColor = System.Drawing.Color.Black;
            this.tb_14th_month_pay.Location = new System.Drawing.Point(181, 69);
            this.tb_14th_month_pay.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.tb_14th_month_pay.Name = "tb_14th_month_pay";
            this.tb_14th_month_pay.Size = new System.Drawing.Size(304, 28);
            this.tb_14th_month_pay.TabIndex = 26;
            this.tb_14th_month_pay.Text = "0.00";
            this.tb_14th_month_pay.TextChanged += new System.EventHandler(this.tb_14th_month_pay_TextChanged);
            this.tb_14th_month_pay.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.tb_14th_month_pay_KeyPress);
            // 
            // label12
            // 
            this.label12.AutoSize = true;
            this.label12.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label12.Location = new System.Drawing.Point(48, 31);
            this.label12.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label12.Name = "label12";
            this.label12.Size = new System.Drawing.Size(125, 22);
            this.label12.TabIndex = 39;
            this.label12.Text = "13 Month Pay:";
            // 
            // label74
            // 
            this.label74.AutoSize = true;
            this.label74.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label74.Location = new System.Drawing.Point(48, 73);
            this.label74.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label74.Name = "label74";
            this.label74.Size = new System.Drawing.Size(125, 22);
            this.label74.TabIndex = 41;
            this.label74.Text = "14 Month Pay:";
            // 
            // tb_15th_month_pay
            // 
            this.tb_15th_month_pay.Enabled = false;
            this.tb_15th_month_pay.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.tb_15th_month_pay.ForeColor = System.Drawing.Color.Black;
            this.tb_15th_month_pay.Location = new System.Drawing.Point(181, 114);
            this.tb_15th_month_pay.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.tb_15th_month_pay.Name = "tb_15th_month_pay";
            this.tb_15th_month_pay.Size = new System.Drawing.Size(304, 28);
            this.tb_15th_month_pay.TabIndex = 28;
            this.tb_15th_month_pay.Text = "0.00";
            this.tb_15th_month_pay.TextChanged += new System.EventHandler(this.tb_15th_month_pay_TextChanged);
            this.tb_15th_month_pay.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.tb_15th_month_pay_KeyPress);
            // 
            // label75
            // 
            this.label75.AutoSize = true;
            this.label75.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label75.Location = new System.Drawing.Point(45, 121);
            this.label75.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label75.Name = "label75";
            this.label75.Size = new System.Drawing.Size(125, 22);
            this.label75.TabIndex = 43;
            this.label75.Text = "15 Month Pay:";
            // 
            // groupBox4
            // 
            this.groupBox4.Controls.Add(this.tb_otherAllowance);
            this.groupBox4.Controls.Add(this.label92);
            this.groupBox4.Controls.Add(this.label102);
            this.groupBox4.Controls.Add(this.label111);
            this.groupBox4.Controls.Add(this.label112);
            this.groupBox4.Controls.Add(this.label113);
            this.groupBox4.Controls.Add(this.tb_riceAllowance);
            this.groupBox4.Controls.Add(this.tb_laundryAllowance);
            this.groupBox4.Controls.Add(this.tb_mealAllowance);
            this.groupBox4.Controls.Add(this.tb_MedicalBenefits);
            this.groupBox4.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.groupBox4.Location = new System.Drawing.Point(519, 301);
            this.groupBox4.Name = "groupBox4";
            this.groupBox4.Size = new System.Drawing.Size(507, 258);
            this.groupBox4.TabIndex = 9;
            this.groupBox4.TabStop = false;
            this.groupBox4.Text = "De Minimis Benefits";
            // 
            // tb_otherAllowance
            // 
            this.tb_otherAllowance.Enabled = false;
            this.tb_otherAllowance.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F);
            this.tb_otherAllowance.Location = new System.Drawing.Point(190, 216);
            this.tb_otherAllowance.Name = "tb_otherAllowance";
            this.tb_otherAllowance.Size = new System.Drawing.Size(306, 28);
            this.tb_otherAllowance.TabIndex = 18;
            this.tb_otherAllowance.Text = "0.00";
            this.tb_otherAllowance.TextChanged += new System.EventHandler(this.tb_Others_TextChanged);
            this.tb_otherAllowance.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.tb_Others_KeyPress);
            // 
            // label92
            // 
            this.label92.AutoSize = true;
            this.label92.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label92.Location = new System.Drawing.Point(36, 39);
            this.label92.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label92.Name = "label92";
            this.label92.Size = new System.Drawing.Size(138, 22);
            this.label92.TabIndex = 238;
            this.label92.Text = "Rice Allowance:";
            // 
            // label102
            // 
            this.label102.AutoSize = true;
            this.label102.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label102.Location = new System.Drawing.Point(6, 83);
            this.label102.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label102.Name = "label102";
            this.label102.Size = new System.Drawing.Size(167, 22);
            this.label102.TabIndex = 239;
            this.label102.Text = "Laundry Allowance:";
            // 
            // label111
            // 
            this.label111.AutoSize = true;
            this.label111.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label111.Location = new System.Drawing.Point(33, 127);
            this.label111.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label111.Name = "label111";
            this.label111.Size = new System.Drawing.Size(140, 22);
            this.label111.TabIndex = 240;
            this.label111.Text = "Meal Allowance:";
            // 
            // label112
            // 
            this.label112.AutoSize = true;
            this.label112.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label112.Location = new System.Drawing.Point(28, 172);
            this.label112.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label112.Name = "label112";
            this.label112.Size = new System.Drawing.Size(146, 22);
            this.label112.TabIndex = 241;
            this.label112.Text = "Medical Benefits:";
            // 
            // label113
            // 
            this.label113.AutoSize = true;
            this.label113.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label113.Location = new System.Drawing.Point(104, 218);
            this.label113.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label113.Name = "label113";
            this.label113.Size = new System.Drawing.Size(69, 22);
            this.label113.TabIndex = 242;
            this.label113.Text = "Others:";
            // 
            // tb_riceAllowance
            // 
            this.tb_riceAllowance.Enabled = false;
            this.tb_riceAllowance.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.tb_riceAllowance.Location = new System.Drawing.Point(190, 34);
            this.tb_riceAllowance.Name = "tb_riceAllowance";
            this.tb_riceAllowance.Size = new System.Drawing.Size(306, 28);
            this.tb_riceAllowance.TabIndex = 14;
            this.tb_riceAllowance.Text = "0.00";
            this.tb_riceAllowance.TextChanged += new System.EventHandler(this.tb_riceAllowance_TextChanged);
            this.tb_riceAllowance.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.tb_riceAllowance_KeyPress);
            // 
            // tb_laundryAllowance
            // 
            this.tb_laundryAllowance.Enabled = false;
            this.tb_laundryAllowance.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F);
            this.tb_laundryAllowance.Location = new System.Drawing.Point(190, 80);
            this.tb_laundryAllowance.Name = "tb_laundryAllowance";
            this.tb_laundryAllowance.Size = new System.Drawing.Size(306, 28);
            this.tb_laundryAllowance.TabIndex = 15;
            this.tb_laundryAllowance.Text = "0.00";
            this.tb_laundryAllowance.TextChanged += new System.EventHandler(this.tb_laundryAllowance_TextChanged);
            this.tb_laundryAllowance.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.tb_laundryAllowance_KeyPress);
            // 
            // tb_mealAllowance
            // 
            this.tb_mealAllowance.Enabled = false;
            this.tb_mealAllowance.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F);
            this.tb_mealAllowance.Location = new System.Drawing.Point(190, 125);
            this.tb_mealAllowance.Name = "tb_mealAllowance";
            this.tb_mealAllowance.Size = new System.Drawing.Size(306, 28);
            this.tb_mealAllowance.TabIndex = 16;
            this.tb_mealAllowance.Text = "0.00";
            this.tb_mealAllowance.TextChanged += new System.EventHandler(this.tb_MealAllowance_TextChanged);
            this.tb_mealAllowance.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.tb_MealAllowance_KeyPress);
            // 
            // tb_MedicalBenefits
            // 
            this.tb_MedicalBenefits.Enabled = false;
            this.tb_MedicalBenefits.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F);
            this.tb_MedicalBenefits.Location = new System.Drawing.Point(190, 172);
            this.tb_MedicalBenefits.Name = "tb_MedicalBenefits";
            this.tb_MedicalBenefits.Size = new System.Drawing.Size(306, 28);
            this.tb_MedicalBenefits.TabIndex = 17;
            this.tb_MedicalBenefits.Text = "0.00";
            this.tb_MedicalBenefits.TextChanged += new System.EventHandler(this.tb_MedicalBenefits_TextChanged);
            this.tb_MedicalBenefits.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.tb_MedicalBenefits_KeyPress);
            // 
            // groupBox3
            // 
            this.groupBox3.Controls.Add(this.cb_payRate);
            this.groupBox3.Controls.Add(this.label29);
            this.groupBox3.Controls.Add(this.label131);
            this.groupBox3.Controls.Add(this.tb_salary);
            this.groupBox3.Controls.Add(this.label130);
            this.groupBox3.Controls.Add(this.label37);
            this.groupBox3.Controls.Add(this.label129);
            this.groupBox3.Controls.Add(this.label69);
            this.groupBox3.Controls.Add(this.tb_daily_rate);
            this.groupBox3.Controls.Add(this.label39);
            this.groupBox3.Controls.Add(this.tb_daily_hour_rate);
            this.groupBox3.Controls.Add(this.label10);
            this.groupBox3.Controls.Add(this.tb_minRate);
            this.groupBox3.Controls.Add(this.cb_paySchedule);
            this.groupBox3.Controls.Add(this.label72);
            this.groupBox3.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.groupBox3.Location = new System.Drawing.Point(519, 12);
            this.groupBox3.Name = "groupBox3";
            this.groupBox3.Size = new System.Drawing.Size(507, 283);
            this.groupBox3.TabIndex = 8;
            this.groupBox3.TabStop = false;
            this.groupBox3.Text = "Compensation Details";
            // 
            // cb_payRate
            // 
            this.cb_payRate.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cb_payRate.Enabled = false;
            this.cb_payRate.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.cb_payRate.ForeColor = System.Drawing.Color.Black;
            this.cb_payRate.FormattingEnabled = true;
            this.cb_payRate.Items.AddRange(new object[] {
            "Monthly",
            "Daily"});
            this.cb_payRate.Location = new System.Drawing.Point(168, 29);
            this.cb_payRate.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.cb_payRate.Name = "cb_payRate";
            this.cb_payRate.Size = new System.Drawing.Size(310, 30);
            this.cb_payRate.TabIndex = 8;
            this.cb_payRate.SelectedIndexChanged += new System.EventHandler(this.cb_payment_SelectedIndexChanged);
            // 
            // label29
            // 
            this.label29.AutoSize = true;
            this.label29.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label29.Location = new System.Drawing.Point(86, 119);
            this.label29.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label29.Name = "label29";
            this.label29.Size = new System.Drawing.Size(66, 22);
            this.label29.TabIndex = 4;
            this.label29.Text = "Salary:";
            // 
            // label131
            // 
            this.label131.AutoSize = true;
            this.label131.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label131.ForeColor = System.Drawing.Color.Red;
            this.label131.Location = new System.Drawing.Point(485, 117);
            this.label131.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label131.Name = "label131";
            this.label131.Size = new System.Drawing.Size(15, 20);
            this.label131.TabIndex = 258;
            this.label131.Text = "*";
            // 
            // tb_salary
            // 
            this.tb_salary.Enabled = false;
            this.tb_salary.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.tb_salary.ForeColor = System.Drawing.Color.Black;
            this.tb_salary.Location = new System.Drawing.Point(171, 113);
            this.tb_salary.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.tb_salary.Name = "tb_salary";
            this.tb_salary.Size = new System.Drawing.Size(306, 28);
            this.tb_salary.TabIndex = 10;
            this.tb_salary.Text = "0.00";
            this.tb_salary.TextChanged += new System.EventHandler(this.tb_salary_TextChanged);
            this.tb_salary.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.tb_salary_KeyPress);
            // 
            // label130
            // 
            this.label130.AutoSize = true;
            this.label130.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label130.ForeColor = System.Drawing.Color.Red;
            this.label130.Location = new System.Drawing.Point(485, 72);
            this.label130.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label130.Name = "label130";
            this.label130.Size = new System.Drawing.Size(15, 20);
            this.label130.TabIndex = 257;
            this.label130.Text = "*";
            // 
            // label37
            // 
            this.label37.AutoSize = true;
            this.label37.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label37.Location = new System.Drawing.Point(67, 32);
            this.label37.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label37.Name = "label37";
            this.label37.Size = new System.Drawing.Size(89, 22);
            this.label37.TabIndex = 21;
            this.label37.Text = "Pay Rate:";
            // 
            // label129
            // 
            this.label129.AutoSize = true;
            this.label129.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label129.ForeColor = System.Drawing.Color.Red;
            this.label129.Location = new System.Drawing.Point(485, 32);
            this.label129.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label129.Name = "label129";
            this.label129.Size = new System.Drawing.Size(15, 20);
            this.label129.TabIndex = 256;
            this.label129.Text = "*";
            // 
            // label69
            // 
            this.label69.AutoSize = true;
            this.label69.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label69.Location = new System.Drawing.Point(59, 157);
            this.label69.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label69.Name = "label69";
            this.label69.Size = new System.Drawing.Size(98, 22);
            this.label69.TabIndex = 31;
            this.label69.Text = "Daily Rate:";
            // 
            // tb_daily_rate
            // 
            this.tb_daily_rate.Enabled = false;
            this.tb_daily_rate.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.tb_daily_rate.ForeColor = System.Drawing.Color.Black;
            this.tb_daily_rate.Location = new System.Drawing.Point(172, 154);
            this.tb_daily_rate.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.tb_daily_rate.Name = "tb_daily_rate";
            this.tb_daily_rate.Size = new System.Drawing.Size(306, 28);
            this.tb_daily_rate.TabIndex = 11;
            this.tb_daily_rate.Text = "0.00";
            this.tb_daily_rate.TextChanged += new System.EventHandler(this.tb_daily_rate_TextChanged);
            this.tb_daily_rate.Enter += new System.EventHandler(this.tb_daily_rate_Enter);
            this.tb_daily_rate.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.tb_daily_rate_KeyPress);
            // 
            // label39
            // 
            this.label39.AutoSize = true;
            this.label39.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label39.Location = new System.Drawing.Point(55, 201);
            this.label39.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label39.Name = "label39";
            this.label39.Size = new System.Drawing.Size(110, 22);
            this.label39.TabIndex = 35;
            this.label39.Text = "Hourly Rate:";
            // 
            // tb_daily_hour_rate
            // 
            this.tb_daily_hour_rate.Enabled = false;
            this.tb_daily_hour_rate.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.tb_daily_hour_rate.ForeColor = System.Drawing.Color.Black;
            this.tb_daily_hour_rate.Location = new System.Drawing.Point(172, 198);
            this.tb_daily_hour_rate.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.tb_daily_hour_rate.Name = "tb_daily_hour_rate";
            this.tb_daily_hour_rate.Size = new System.Drawing.Size(306, 28);
            this.tb_daily_hour_rate.TabIndex = 12;
            this.tb_daily_hour_rate.Text = "0.00";
            this.tb_daily_hour_rate.TextChanged += new System.EventHandler(this.tb_daily_hour_rate_TextChanged);
            this.tb_daily_hour_rate.Enter += new System.EventHandler(this.tb_daily_hour_rate_Enter);
            this.tb_daily_hour_rate.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.tb_daily_hour_rate_KeyPress);
            // 
            // label10
            // 
            this.label10.AutoSize = true;
            this.label10.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label10.Location = new System.Drawing.Point(36, 75);
            this.label10.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label10.Name = "label10";
            this.label10.Size = new System.Drawing.Size(126, 22);
            this.label10.TabIndex = 230;
            this.label10.Text = "Pay Schedule:";
            // 
            // tb_minRate
            // 
            this.tb_minRate.Enabled = false;
            this.tb_minRate.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.tb_minRate.ForeColor = System.Drawing.Color.Black;
            this.tb_minRate.Location = new System.Drawing.Point(172, 241);
            this.tb_minRate.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.tb_minRate.Name = "tb_minRate";
            this.tb_minRate.Size = new System.Drawing.Size(306, 28);
            this.tb_minRate.TabIndex = 13;
            this.tb_minRate.Text = "0.00";
            this.tb_minRate.TextChanged += new System.EventHandler(this.tb_minRate_TextChanged);
            // 
            // cb_paySchedule
            // 
            this.cb_paySchedule.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cb_paySchedule.Enabled = false;
            this.cb_paySchedule.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.cb_paySchedule.ForeColor = System.Drawing.Color.Black;
            this.cb_paySchedule.FormattingEnabled = true;
            this.cb_paySchedule.Location = new System.Drawing.Point(169, 72);
            this.cb_paySchedule.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.cb_paySchedule.Name = "cb_paySchedule";
            this.cb_paySchedule.Size = new System.Drawing.Size(309, 30);
            this.cb_paySchedule.TabIndex = 9;
            // 
            // label72
            // 
            this.label72.AutoSize = true;
            this.label72.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label72.Location = new System.Drawing.Point(54, 244);
            this.label72.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label72.Name = "label72";
            this.label72.Size = new System.Drawing.Size(111, 22);
            this.label72.TabIndex = 250;
            this.label72.Text = "Minute Rate:";
            // 
            // label132
            // 
            this.label132.AutoSize = true;
            this.label132.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label132.ForeColor = System.Drawing.Color.Red;
            this.label132.Location = new System.Drawing.Point(1546, 28);
            this.label132.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label132.Name = "label132";
            this.label132.Size = new System.Drawing.Size(15, 20);
            this.label132.TabIndex = 259;
            this.label132.Text = "*";
            // 
            // label128
            // 
            this.label128.AutoSize = true;
            this.label128.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label128.ForeColor = System.Drawing.Color.Red;
            this.label128.Location = new System.Drawing.Point(481, 385);
            this.label128.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label128.Name = "label128";
            this.label128.Size = new System.Drawing.Size(15, 20);
            this.label128.TabIndex = 255;
            this.label128.Text = "*";
            // 
            // label127
            // 
            this.label127.AutoSize = true;
            this.label127.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label127.ForeColor = System.Drawing.Color.Red;
            this.label127.Location = new System.Drawing.Point(481, 292);
            this.label127.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label127.Name = "label127";
            this.label127.Size = new System.Drawing.Size(15, 20);
            this.label127.TabIndex = 254;
            this.label127.Text = "*";
            // 
            // label126
            // 
            this.label126.AutoSize = true;
            this.label126.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label126.ForeColor = System.Drawing.Color.Red;
            this.label126.Location = new System.Drawing.Point(481, 252);
            this.label126.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label126.Name = "label126";
            this.label126.Size = new System.Drawing.Size(15, 20);
            this.label126.TabIndex = 253;
            this.label126.Text = "*";
            // 
            // label121
            // 
            this.label121.AutoSize = true;
            this.label121.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label121.ForeColor = System.Drawing.Color.Red;
            this.label121.Location = new System.Drawing.Point(481, 212);
            this.label121.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label121.Name = "label121";
            this.label121.Size = new System.Drawing.Size(15, 20);
            this.label121.TabIndex = 252;
            this.label121.Text = "*";
            // 
            // label36
            // 
            this.label36.AutoSize = true;
            this.label36.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label36.Location = new System.Drawing.Point(1092, 150);
            this.label36.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label36.Name = "label36";
            this.label36.Size = new System.Drawing.Size(133, 22);
            this.label36.TabIndex = 235;
            this.label36.Text = "Alternate Bank:";
            // 
            // cb_bank_name2
            // 
            this.cb_bank_name2.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cb_bank_name2.Enabled = false;
            this.cb_bank_name2.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.cb_bank_name2.ForeColor = System.Drawing.Color.Black;
            this.cb_bank_name2.FormattingEnabled = true;
            this.cb_bank_name2.Items.AddRange(new object[] {
            "BDO Unibank, Inc.",
            "United Coconut Planters Bank",
            "Bank of the Philippine Islands",
            "Metropolitan Bank and Trust Company",
            "Philippine National Bank",
            "Security Bank Corp",
            "China Banking Corp",
            "Development Bank of the Philippines",
            "Union Bank of the Philippines"});
            this.cb_bank_name2.Location = new System.Drawing.Point(1237, 145);
            this.cb_bank_name2.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.cb_bank_name2.Name = "cb_bank_name2";
            this.cb_bank_name2.Size = new System.Drawing.Size(301, 30);
            this.cb_bank_name2.TabIndex = 13;
            // 
            // cb_reason
            // 
            this.cb_reason.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cb_reason.Enabled = false;
            this.cb_reason.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.cb_reason.FormattingEnabled = true;
            this.cb_reason.Items.AddRange(new object[] {
            "Terminated",
            "Transferred",
            "Retirement",
            "Death"});
            this.cb_reason.Location = new System.Drawing.Point(171, 473);
            this.cb_reason.Name = "cb_reason";
            this.cb_reason.Size = new System.Drawing.Size(303, 33);
            this.cb_reason.TabIndex = 7;
            // 
            // label26
            // 
            this.label26.AutoSize = true;
            this.label26.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label26.Location = new System.Drawing.Point(64, 475);
            this.label26.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label26.Name = "label26";
            this.label26.Size = new System.Drawing.Size(77, 22);
            this.label26.TabIndex = 232;
            this.label26.Text = "Reason:";
            // 
            // label83
            // 
            this.label83.AutoSize = true;
            this.label83.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label83.Location = new System.Drawing.Point(8, 538);
            this.label83.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label83.Name = "label83";
            this.label83.Size = new System.Drawing.Size(21, 22);
            this.label83.TabIndex = 201;
            this.label83.Text = "0";
            this.label83.Visible = false;
            // 
            // dtp_dateResign
            // 
            this.dtp_dateResign.CustomFormat = "M/d/yyyy";
            this.dtp_dateResign.Enabled = false;
            this.dtp_dateResign.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.dtp_dateResign.Format = System.Windows.Forms.DateTimePickerFormat.Short;
            this.dtp_dateResign.Location = new System.Drawing.Point(172, 429);
            this.dtp_dateResign.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.dtp_dateResign.Name = "dtp_dateResign";
            this.dtp_dateResign.Size = new System.Drawing.Size(302, 28);
            this.dtp_dateResign.TabIndex = 6;
            // 
            // label76
            // 
            this.label76.AutoSize = true;
            this.label76.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label76.Location = new System.Drawing.Point(11, 428);
            this.label76.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label76.Name = "label76";
            this.label76.Size = new System.Drawing.Size(134, 22);
            this.label76.TabIndex = 194;
            this.label76.Text = "Date Resigned:";
            // 
            // label122
            // 
            this.label122.AutoSize = true;
            this.label122.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label122.Location = new System.Drawing.Point(1099, 192);
            this.label122.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label122.Name = "label122";
            this.label122.Size = new System.Drawing.Size(126, 22);
            this.label122.TabIndex = 202;
            this.label122.Text = "Bank Account:";
            // 
            // tb_bank_acc2
            // 
            this.tb_bank_acc2.Enabled = false;
            this.tb_bank_acc2.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.tb_bank_acc2.ForeColor = System.Drawing.Color.Black;
            this.tb_bank_acc2.Location = new System.Drawing.Point(1239, 189);
            this.tb_bank_acc2.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.tb_bank_acc2.Name = "tb_bank_acc2";
            this.tb_bank_acc2.Size = new System.Drawing.Size(301, 28);
            this.tb_bank_acc2.TabIndex = 14;
            // 
            // label99
            // 
            this.label99.AutoSize = true;
            this.label99.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label99.Location = new System.Drawing.Point(19, 157);
            this.label99.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label99.Name = "label99";
            this.label99.Size = new System.Drawing.Size(119, 22);
            this.label99.TabIndex = 200;
            this.label99.Text = "Middle Name:";
            // 
            // label118
            // 
            this.label118.AutoSize = true;
            this.label118.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label118.Location = new System.Drawing.Point(3, 60);
            this.label118.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label118.Name = "label118";
            this.label118.Size = new System.Drawing.Size(21, 22);
            this.label118.TabIndex = 199;
            this.label118.Text = "0";
            this.label118.Visible = false;
            // 
            // cb_department
            // 
            this.cb_department.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cb_department.Enabled = false;
            this.cb_department.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.cb_department.ForeColor = System.Drawing.Color.Black;
            this.cb_department.FormattingEnabled = true;
            this.cb_department.Location = new System.Drawing.Point(170, 289);
            this.cb_department.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.cb_department.Name = "cb_department";
            this.cb_department.Size = new System.Drawing.Size(302, 30);
            this.cb_department.TabIndex = 3;
            // 
            // label21
            // 
            this.label21.AutoSize = true;
            this.label21.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label21.Location = new System.Drawing.Point(31, 287);
            this.label21.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label21.Name = "label21";
            this.label21.Size = new System.Drawing.Size(108, 22);
            this.label21.TabIndex = 193;
            this.label21.Text = "Department:";
            // 
            // tb_designation
            // 
            this.tb_designation.Enabled = false;
            this.tb_designation.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.tb_designation.ForeColor = System.Drawing.Color.Black;
            this.tb_designation.Location = new System.Drawing.Point(171, 249);
            this.tb_designation.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.tb_designation.Name = "tb_designation";
            this.tb_designation.Size = new System.Drawing.Size(302, 28);
            this.tb_designation.TabIndex = 2;
            // 
            // label20
            // 
            this.label20.AutoSize = true;
            this.label20.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label20.Location = new System.Drawing.Point(31, 246);
            this.label20.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label20.Name = "label20";
            this.label20.Size = new System.Drawing.Size(110, 22);
            this.label20.TabIndex = 188;
            this.label20.Text = "Designation:";
            // 
            // dtp_date_hired
            // 
            this.dtp_date_hired.Enabled = false;
            this.dtp_date_hired.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.dtp_date_hired.Format = System.Windows.Forms.DateTimePickerFormat.Short;
            this.dtp_date_hired.Location = new System.Drawing.Point(169, 207);
            this.dtp_date_hired.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.dtp_date_hired.Name = "dtp_date_hired";
            this.dtp_date_hired.Size = new System.Drawing.Size(304, 28);
            this.dtp_date_hired.TabIndex = 1;
            // 
            // cb_employee_status
            // 
            this.cb_employee_status.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cb_employee_status.Enabled = false;
            this.cb_employee_status.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.cb_employee_status.ForeColor = System.Drawing.Color.Black;
            this.cb_employee_status.FormattingEnabled = true;
            this.cb_employee_status.Items.AddRange(new object[] {
            "Regular",
            "Casual",
            "Contractual/Project-Based",
            "Seasonal",
            "Probationary",
            "Apprentices/Learners"});
            this.cb_employee_status.Location = new System.Drawing.Point(169, 381);
            this.cb_employee_status.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.cb_employee_status.Name = "cb_employee_status";
            this.cb_employee_status.Size = new System.Drawing.Size(304, 30);
            this.cb_employee_status.TabIndex = 5;
            // 
            // label19
            // 
            this.label19.AutoSize = true;
            this.label19.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label19.Location = new System.Drawing.Point(40, 200);
            this.label19.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label19.Name = "label19";
            this.label19.Size = new System.Drawing.Size(101, 22);
            this.label19.TabIndex = 187;
            this.label19.Text = "Date Hired:";
            // 
            // label18
            // 
            this.label18.AutoSize = true;
            this.label18.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label18.Location = new System.Drawing.Point(38, 380);
            this.label18.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label18.Name = "label18";
            this.label18.Size = new System.Drawing.Size(112, 22);
            this.label18.TabIndex = 186;
            this.label18.Text = "Emp. Status:";
            // 
            // label97
            // 
            this.label97.AutoSize = true;
            this.label97.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label97.Location = new System.Drawing.Point(31, 116);
            this.label97.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label97.Name = "label97";
            this.label97.Size = new System.Drawing.Size(102, 22);
            this.label97.TabIndex = 151;
            this.label97.Text = "First Name:";
            // 
            // label98
            // 
            this.label98.AutoSize = true;
            this.label98.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label98.Location = new System.Drawing.Point(38, 70);
            this.label98.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label98.Name = "label98";
            this.label98.Size = new System.Drawing.Size(101, 22);
            this.label98.TabIndex = 150;
            this.label98.Text = "Last Name:";
            // 
            // tb_sal_lname
            // 
            this.tb_sal_lname.Enabled = false;
            this.tb_sal_lname.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.tb_sal_lname.ForeColor = System.Drawing.Color.Black;
            this.tb_sal_lname.Location = new System.Drawing.Point(170, 73);
            this.tb_sal_lname.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.tb_sal_lname.Name = "tb_sal_lname";
            this.tb_sal_lname.Size = new System.Drawing.Size(302, 28);
            this.tb_sal_lname.TabIndex = 147;
            this.tb_sal_lname.TabStop = false;
            // 
            // tb_sal_fname
            // 
            this.tb_sal_fname.Enabled = false;
            this.tb_sal_fname.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.tb_sal_fname.ForeColor = System.Drawing.Color.Black;
            this.tb_sal_fname.Location = new System.Drawing.Point(170, 113);
            this.tb_sal_fname.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.tb_sal_fname.Name = "tb_sal_fname";
            this.tb_sal_fname.Size = new System.Drawing.Size(302, 28);
            this.tb_sal_fname.TabIndex = 148;
            this.tb_sal_fname.TabStop = false;
            // 
            // tb_sal_mname
            // 
            this.tb_sal_mname.Enabled = false;
            this.tb_sal_mname.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.tb_sal_mname.ForeColor = System.Drawing.Color.Black;
            this.tb_sal_mname.Location = new System.Drawing.Point(170, 162);
            this.tb_sal_mname.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.tb_sal_mname.Name = "tb_sal_mname";
            this.tb_sal_mname.Size = new System.Drawing.Size(302, 28);
            this.tb_sal_mname.TabIndex = 149;
            // 
            // tb_sal_empID
            // 
            this.tb_sal_empID.Enabled = false;
            this.tb_sal_empID.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.tb_sal_empID.ForeColor = System.Drawing.Color.Black;
            this.tb_sal_empID.Location = new System.Drawing.Point(170, 29);
            this.tb_sal_empID.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.tb_sal_empID.Name = "tb_sal_empID";
            this.tb_sal_empID.Size = new System.Drawing.Size(304, 28);
            this.tb_sal_empID.TabIndex = 145;
            this.tb_sal_empID.TabStop = false;
            // 
            // label100
            // 
            this.label100.AutoSize = true;
            this.label100.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label100.Location = new System.Drawing.Point(20, 35);
            this.label100.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label100.Name = "label100";
            this.label100.Size = new System.Drawing.Size(116, 22);
            this.label100.TabIndex = 146;
            this.label100.Text = "Employee ID:";
            // 
            // label91
            // 
            this.label91.AutoSize = true;
            this.label91.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label91.Location = new System.Drawing.Point(1092, 112);
            this.label91.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label91.Name = "label91";
            this.label91.Size = new System.Drawing.Size(126, 22);
            this.label91.TabIndex = 47;
            this.label91.Text = "Bank Account:";
            // 
            // tb_bank_acc
            // 
            this.tb_bank_acc.Enabled = false;
            this.tb_bank_acc.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.tb_bank_acc.ForeColor = System.Drawing.Color.Black;
            this.tb_bank_acc.Location = new System.Drawing.Point(1236, 106);
            this.tb_bank_acc.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.tb_bank_acc.Name = "tb_bank_acc";
            this.tb_bank_acc.Size = new System.Drawing.Size(301, 28);
            this.tb_bank_acc.TabIndex = 12;
            // 
            // label58
            // 
            this.label58.AutoSize = true;
            this.label58.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label58.Location = new System.Drawing.Point(1096, 72);
            this.label58.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label58.Name = "label58";
            this.label58.Size = new System.Drawing.Size(122, 22);
            this.label58.TabIndex = 32;
            this.label58.Text = "Primary Bank:";
            // 
            // cb_bank_name
            // 
            this.cb_bank_name.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cb_bank_name.Enabled = false;
            this.cb_bank_name.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.cb_bank_name.ForeColor = System.Drawing.Color.Black;
            this.cb_bank_name.FormattingEnabled = true;
            this.cb_bank_name.Items.AddRange(new object[] {
            "BDO Unibank, Inc.",
            "United Coconut Planters Bank",
            "Bank of the Philippine Islands",
            "Metropolitan Bank and Trust Company",
            "Philippine National Bank",
            "Security Bank Corp",
            "China Banking Corp",
            "Development Bank of the Philippines",
            "Union Bank of the Philippines"});
            this.cb_bank_name.Location = new System.Drawing.Point(1237, 67);
            this.cb_bank_name.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.cb_bank_name.Name = "cb_bank_name";
            this.cb_bank_name.Size = new System.Drawing.Size(301, 30);
            this.cb_bank_name.TabIndex = 11;
            // 
            // label27
            // 
            this.label27.AutoSize = true;
            this.label27.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label27.Location = new System.Drawing.Point(1103, 28);
            this.label27.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label27.Name = "label27";
            this.label27.Size = new System.Drawing.Size(119, 22);
            this.label27.TabIndex = 0;
            this.label27.Text = "Payroll Mode:";
            // 
            // cb_payroll_mode
            // 
            this.cb_payroll_mode.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cb_payroll_mode.Enabled = false;
            this.cb_payroll_mode.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.cb_payroll_mode.ForeColor = System.Drawing.Color.Black;
            this.cb_payroll_mode.FormattingEnabled = true;
            this.cb_payroll_mode.Items.AddRange(new object[] {
            "ATM",
            "Bank Account",
            "Cash"});
            this.cb_payroll_mode.Location = new System.Drawing.Point(1236, 24);
            this.cb_payroll_mode.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.cb_payroll_mode.Name = "cb_payroll_mode";
            this.cb_payroll_mode.Size = new System.Drawing.Size(302, 30);
            this.cb_payroll_mode.TabIndex = 10;
            this.cb_payroll_mode.SelectionChangeCommitted += new System.EventHandler(this.cb_payroll_mode_SelectionChangeCommitted);
            // 
            // label28
            // 
            this.label28.AutoSize = true;
            this.label28.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label28.Location = new System.Drawing.Point(35, 333);
            this.label28.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label28.Name = "label28";
            this.label28.Size = new System.Drawing.Size(105, 22);
            this.label28.TabIndex = 2;
            this.label28.Text = "Rank/Level:";
            // 
            // cb_confidentiality
            // 
            this.cb_confidentiality.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cb_confidentiality.Enabled = false;
            this.cb_confidentiality.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.cb_confidentiality.ForeColor = System.Drawing.Color.Black;
            this.cb_confidentiality.FormattingEnabled = true;
            this.cb_confidentiality.Items.AddRange(new object[] {
            "Rank and File",
            "Supervisor",
            "Manager"});
            this.cb_confidentiality.Location = new System.Drawing.Point(171, 335);
            this.cb_confidentiality.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.cb_confidentiality.Name = "cb_confidentiality";
            this.cb_confidentiality.Size = new System.Drawing.Size(304, 30);
            this.cb_confidentiality.TabIndex = 4;
            // 
            // label55
            // 
            this.label55.AutoSize = true;
            this.label55.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label55.Location = new System.Drawing.Point(2, 14);
            this.label55.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label55.Name = "label55";
            this.label55.Size = new System.Drawing.Size(21, 22);
            this.label55.TabIndex = 171;
            this.label55.Text = "0";
            this.label55.Visible = false;
            // 
            // tp_general_information
            // 
            this.tp_general_information.Controls.Add(this.groupBox2);
            this.tp_general_information.Font = new System.Drawing.Font("Calibri", 11F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.tp_general_information.Location = new System.Drawing.Point(4, 31);
            this.tp_general_information.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.tp_general_information.Name = "tp_general_information";
            this.tp_general_information.Padding = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.tp_general_information.Size = new System.Drawing.Size(1624, 584);
            this.tp_general_information.TabIndex = 0;
            this.tp_general_information.Text = "General Information";
            this.tp_general_information.UseVisualStyleBackColor = true;
            this.tp_general_information.Enter += new System.EventHandler(this.tp_general_information_Enter);
            // 
            // groupBox2
            // 
            this.groupBox2.Controls.Add(this.label133);
            this.groupBox2.Controls.Add(this.label77);
            this.groupBox2.Controls.Add(this.label16);
            this.groupBox2.Controls.Add(this.btn_upload);
            this.groupBox2.Controls.Add(this.cb_statustype);
            this.groupBox2.Controls.Add(this.label62);
            this.groupBox2.Controls.Add(this.label125);
            this.groupBox2.Controls.Add(this.label124);
            this.groupBox2.Controls.Add(this.label123);
            this.groupBox2.Controls.Add(this.lbl_emp_no);
            this.groupBox2.Controls.Add(this.rtb_address);
            this.groupBox2.Controls.Add(this.cb_nationality);
            this.groupBox2.Controls.Add(this.tb_name_of_spouse);
            this.groupBox2.Controls.Add(this.cb_religion);
            this.groupBox2.Controls.Add(this.label46);
            this.groupBox2.Controls.Add(this.label44);
            this.groupBox2.Controls.Add(this.label51);
            this.groupBox2.Controls.Add(this.cb_gender);
            this.groupBox2.Controls.Add(this.label8);
            this.groupBox2.Controls.Add(this.label11);
            this.groupBox2.Controls.Add(this.lbl_number_of_employee);
            this.groupBox2.Controls.Add(this.label30);
            this.groupBox2.Controls.Add(this.dtp_birthdate);
            this.groupBox2.Controls.Add(this.label15);
            this.groupBox2.Controls.Add(this.tb_comp_email);
            this.groupBox2.Controls.Add(this.tb_telphone_no);
            this.groupBox2.Controls.Add(this.label24);
            this.groupBox2.Controls.Add(this.label23);
            this.groupBox2.Controls.Add(this.tb_empID);
            this.groupBox2.Controls.Add(this.label9);
            this.groupBox2.Controls.Add(this.label54);
            this.groupBox2.Controls.Add(this.cb_pob);
            this.groupBox2.Controls.Add(this.cb_status);
            this.groupBox2.Controls.Add(this.label47);
            this.groupBox2.Controls.Add(this.label6);
            this.groupBox2.Controls.Add(this.label3);
            this.groupBox2.Controls.Add(this.label2);
            this.groupBox2.Controls.Add(this.tb_lname);
            this.groupBox2.Controls.Add(this.tb_fname);
            this.groupBox2.Controls.Add(this.tb_mname);
            this.groupBox2.Controls.Add(this.tb_email);
            this.groupBox2.Controls.Add(this.label4);
            this.groupBox2.Controls.Add(this.pic_Image);
            this.groupBox2.Controls.Add(this.tb_cont_no);
            this.groupBox2.Controls.Add(this.label7);
            this.groupBox2.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.groupBox2.Location = new System.Drawing.Point(9, 5);
            this.groupBox2.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.groupBox2.Name = "groupBox2";
            this.groupBox2.Padding = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.groupBox2.Size = new System.Drawing.Size(1607, 569);
            this.groupBox2.TabIndex = 75;
            this.groupBox2.TabStop = false;
            this.groupBox2.Text = "General Information";
            this.groupBox2.Enter += new System.EventHandler(this.groupBox2_Enter);
            // 
            // label133
            // 
            this.label133.AutoSize = true;
            this.label133.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label133.ForeColor = System.Drawing.Color.Red;
            this.label133.Location = new System.Drawing.Point(646, 207);
            this.label133.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label133.Name = "label133";
            this.label133.Size = new System.Drawing.Size(15, 20);
            this.label133.TabIndex = 187;
            this.label133.Text = "*";
            // 
            // label77
            // 
            this.label77.AutoSize = true;
            this.label77.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label77.ForeColor = System.Drawing.Color.Red;
            this.label77.Location = new System.Drawing.Point(1228, 29);
            this.label77.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label77.Name = "label77";
            this.label77.Size = new System.Drawing.Size(15, 20);
            this.label77.TabIndex = 186;
            this.label77.Text = "*";
            // 
            // label16
            // 
            this.label16.AutoSize = true;
            this.label16.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label16.ForeColor = System.Drawing.Color.Red;
            this.label16.Location = new System.Drawing.Point(645, 346);
            this.label16.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label16.Name = "label16";
            this.label16.Size = new System.Drawing.Size(15, 20);
            this.label16.TabIndex = 185;
            this.label16.Text = "*";
            // 
            // btn_upload
            // 
            this.btn_upload.BackColor = System.Drawing.Color.Transparent;
            this.btn_upload.BackgroundColor = System.Drawing.Color.Transparent;
            this.btn_upload.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(192)))), ((int)(((byte)(0)))), ((int)(((byte)(0)))));
            this.btn_upload.BorderRadius = 8;
            this.btn_upload.BorderSize = 1;
            this.btn_upload.FlatAppearance.BorderSize = 0;
            this.btn_upload.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btn_upload.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btn_upload.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(192)))), ((int)(((byte)(0)))), ((int)(((byte)(0)))));
            this.btn_upload.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.btn_upload.Location = new System.Drawing.Point(1288, 231);
            this.btn_upload.Name = "btn_upload";
            this.btn_upload.Size = new System.Drawing.Size(219, 37);
            this.btn_upload.TabIndex = 184;
            this.btn_upload.Text = "Attach Photo";
            this.btn_upload.TextColor = System.Drawing.Color.FromArgb(((int)(((byte)(192)))), ((int)(((byte)(0)))), ((int)(((byte)(0)))));
            this.btn_upload.UseVisualStyleBackColor = false;
            this.btn_upload.Click += new System.EventHandler(this.btn_upload_Click);
            this.btn_upload.MouseEnter += new System.EventHandler(this.btn_upload_MouseEnter);
            this.btn_upload.MouseLeave += new System.EventHandler(this.btn_upload_MouseLeave);
            this.btn_upload.MouseHover += new System.EventHandler(this.btn_upload_MouseHover);
            // 
            // cb_statustype
            // 
            this.cb_statustype.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cb_statustype.Enabled = false;
            this.cb_statustype.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.cb_statustype.ForeColor = System.Drawing.Color.Black;
            this.cb_statustype.FormattingEnabled = true;
            this.cb_statustype.Items.AddRange(new object[] {
            "Active",
            "InActive"});
            this.cb_statustype.Location = new System.Drawing.Point(338, 32);
            this.cb_statustype.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.cb_statustype.Name = "cb_statustype";
            this.cb_statustype.Size = new System.Drawing.Size(301, 30);
            this.cb_statustype.TabIndex = 1;
            // 
            // label62
            // 
            this.label62.AutoSize = true;
            this.label62.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label62.Location = new System.Drawing.Point(217, 40);
            this.label62.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label62.Name = "label62";
            this.label62.Size = new System.Drawing.Size(112, 22);
            this.label62.TabIndex = 179;
            this.label62.Text = "Status Type:";
            // 
            // label125
            // 
            this.label125.AutoSize = true;
            this.label125.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label125.ForeColor = System.Drawing.Color.Red;
            this.label125.Location = new System.Drawing.Point(645, 165);
            this.label125.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label125.Name = "label125";
            this.label125.Size = new System.Drawing.Size(15, 20);
            this.label125.TabIndex = 176;
            this.label125.Text = "*";
            // 
            // label124
            // 
            this.label124.AutoSize = true;
            this.label124.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label124.ForeColor = System.Drawing.Color.Red;
            this.label124.Location = new System.Drawing.Point(645, 120);
            this.label124.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label124.Name = "label124";
            this.label124.Size = new System.Drawing.Size(15, 20);
            this.label124.TabIndex = 175;
            this.label124.Text = "*";
            // 
            // label123
            // 
            this.label123.AutoSize = true;
            this.label123.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label123.ForeColor = System.Drawing.Color.Red;
            this.label123.Location = new System.Drawing.Point(647, 81);
            this.label123.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label123.Name = "label123";
            this.label123.Size = new System.Drawing.Size(15, 20);
            this.label123.TabIndex = 174;
            this.label123.Text = "*";
            // 
            // lbl_emp_no
            // 
            this.lbl_emp_no.AutoSize = true;
            this.lbl_emp_no.Location = new System.Drawing.Point(9, 536);
            this.lbl_emp_no.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.lbl_emp_no.Name = "lbl_emp_no";
            this.lbl_emp_no.Size = new System.Drawing.Size(21, 22);
            this.lbl_emp_no.TabIndex = 172;
            this.lbl_emp_no.Text = "0";
            this.lbl_emp_no.Visible = false;
            // 
            // rtb_address
            // 
            this.rtb_address.Enabled = false;
            this.rtb_address.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.rtb_address.ForeColor = System.Drawing.Color.Black;
            this.rtb_address.Location = new System.Drawing.Point(921, 340);
            this.rtb_address.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.rtb_address.Name = "rtb_address";
            this.rtb_address.Size = new System.Drawing.Size(301, 88);
            this.rtb_address.TabIndex = 17;
            this.rtb_address.Text = "";
            // 
            // cb_nationality
            // 
            this.cb_nationality.AutoCompleteMode = System.Windows.Forms.AutoCompleteMode.SuggestAppend;
            this.cb_nationality.AutoCompleteSource = System.Windows.Forms.AutoCompleteSource.ListItems;
            this.cb_nationality.Enabled = false;
            this.cb_nationality.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.cb_nationality.ForeColor = System.Drawing.Color.Black;
            this.cb_nationality.FormattingEnabled = true;
            this.cb_nationality.Items.AddRange(new object[] {
            "Filipino"});
            this.cb_nationality.Location = new System.Drawing.Point(918, 202);
            this.cb_nationality.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.cb_nationality.Name = "cb_nationality";
            this.cb_nationality.Size = new System.Drawing.Size(301, 30);
            this.cb_nationality.TabIndex = 14;
            // 
            // tb_name_of_spouse
            // 
            this.tb_name_of_spouse.Enabled = false;
            this.tb_name_of_spouse.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.tb_name_of_spouse.ForeColor = System.Drawing.Color.Black;
            this.tb_name_of_spouse.Location = new System.Drawing.Point(918, 249);
            this.tb_name_of_spouse.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.tb_name_of_spouse.Name = "tb_name_of_spouse";
            this.tb_name_of_spouse.Size = new System.Drawing.Size(301, 28);
            this.tb_name_of_spouse.TabIndex = 15;
            // 
            // cb_religion
            // 
            this.cb_religion.AutoCompleteMode = System.Windows.Forms.AutoCompleteMode.SuggestAppend;
            this.cb_religion.AutoCompleteSource = System.Windows.Forms.AutoCompleteSource.ListItems;
            this.cb_religion.Enabled = false;
            this.cb_religion.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.cb_religion.ForeColor = System.Drawing.Color.Black;
            this.cb_religion.FormattingEnabled = true;
            this.cb_religion.Location = new System.Drawing.Point(918, 292);
            this.cb_religion.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.cb_religion.Name = "cb_religion";
            this.cb_religion.Size = new System.Drawing.Size(301, 30);
            this.cb_religion.TabIndex = 16;
            // 
            // label46
            // 
            this.label46.AutoSize = true;
            this.label46.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label46.Location = new System.Drawing.Point(789, 208);
            this.label46.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label46.Name = "label46";
            this.label46.Size = new System.Drawing.Size(99, 22);
            this.label46.TabIndex = 165;
            this.label46.Text = "Nationality:";
            // 
            // label44
            // 
            this.label44.AutoSize = true;
            this.label44.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label44.Location = new System.Drawing.Point(734, 252);
            this.label44.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label44.Name = "label44";
            this.label44.Size = new System.Drawing.Size(148, 22);
            this.label44.TabIndex = 168;
            this.label44.Text = "Name of Spouse:";
            // 
            // label51
            // 
            this.label51.AutoSize = true;
            this.label51.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label51.Location = new System.Drawing.Point(812, 300);
            this.label51.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label51.Name = "label51";
            this.label51.Size = new System.Drawing.Size(80, 22);
            this.label51.TabIndex = 169;
            this.label51.Text = "Religion:";
            // 
            // cb_gender
            // 
            this.cb_gender.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cb_gender.Enabled = false;
            this.cb_gender.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.cb_gender.ForeColor = System.Drawing.Color.Black;
            this.cb_gender.FormattingEnabled = true;
            this.cb_gender.Items.AddRange(new object[] {
            "Male",
            "Female"});
            this.cb_gender.Location = new System.Drawing.Point(918, 156);
            this.cb_gender.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.cb_gender.Name = "cb_gender";
            this.cb_gender.Size = new System.Drawing.Size(301, 30);
            this.cb_gender.TabIndex = 13;
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label8.Location = new System.Drawing.Point(816, 162);
            this.label8.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(75, 22);
            this.label8.TabIndex = 163;
            this.label8.Text = "Gender:";
            // 
            // label11
            // 
            this.label11.AutoSize = true;
            this.label11.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label11.Location = new System.Drawing.Point(806, 346);
            this.label11.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label11.Name = "label11";
            this.label11.Size = new System.Drawing.Size(81, 22);
            this.label11.TabIndex = 164;
            this.label11.Text = "Address:";
            // 
            // lbl_number_of_employee
            // 
            this.lbl_number_of_employee.AutoSize = true;
            this.lbl_number_of_employee.Location = new System.Drawing.Point(9, 58);
            this.lbl_number_of_employee.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.lbl_number_of_employee.Name = "lbl_number_of_employee";
            this.lbl_number_of_employee.Size = new System.Drawing.Size(21, 22);
            this.lbl_number_of_employee.TabIndex = 156;
            this.lbl_number_of_employee.Text = "0";
            this.lbl_number_of_employee.Visible = false;
            // 
            // label30
            // 
            this.label30.AutoSize = true;
            this.label30.Location = new System.Drawing.Point(9, 28);
            this.label30.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label30.Name = "label30";
            this.label30.Size = new System.Drawing.Size(21, 22);
            this.label30.TabIndex = 155;
            this.label30.Text = "0";
            this.label30.Visible = false;
            // 
            // dtp_birthdate
            // 
            this.dtp_birthdate.Enabled = false;
            this.dtp_birthdate.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.dtp_birthdate.Format = System.Windows.Forms.DateTimePickerFormat.Short;
            this.dtp_birthdate.Location = new System.Drawing.Point(921, 115);
            this.dtp_birthdate.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.dtp_birthdate.Name = "dtp_birthdate";
            this.dtp_birthdate.Size = new System.Drawing.Size(298, 28);
            this.dtp_birthdate.TabIndex = 12;
            // 
            // label15
            // 
            this.label15.AutoSize = true;
            this.label15.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label15.Location = new System.Drawing.Point(808, 116);
            this.label15.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label15.Name = "label15";
            this.label15.Size = new System.Drawing.Size(87, 22);
            this.label15.TabIndex = 153;
            this.label15.Text = "Birthdate:";
            // 
            // tb_comp_email
            // 
            this.tb_comp_email.Enabled = false;
            this.tb_comp_email.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.tb_comp_email.ForeColor = System.Drawing.Color.Black;
            this.tb_comp_email.Location = new System.Drawing.Point(920, 24);
            this.tb_comp_email.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.tb_comp_email.Name = "tb_comp_email";
            this.tb_comp_email.Size = new System.Drawing.Size(300, 28);
            this.tb_comp_email.TabIndex = 10;
            // 
            // tb_telphone_no
            // 
            this.tb_telphone_no.Enabled = false;
            this.tb_telphone_no.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.tb_telphone_no.ForeColor = System.Drawing.Color.Black;
            this.tb_telphone_no.Location = new System.Drawing.Point(920, 69);
            this.tb_telphone_no.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.tb_telphone_no.Name = "tb_telphone_no";
            this.tb_telphone_no.Size = new System.Drawing.Size(300, 28);
            this.tb_telphone_no.TabIndex = 11;
            this.tb_telphone_no.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.tb_telephone_no_KeyPress);
            // 
            // label24
            // 
            this.label24.AutoSize = true;
            this.label24.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label24.Location = new System.Drawing.Point(765, 72);
            this.label24.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label24.Name = "label24";
            this.label24.Size = new System.Drawing.Size(129, 22);
            this.label24.TabIndex = 132;
            this.label24.Text = "Telephone No:";
            // 
            // label23
            // 
            this.label23.AutoSize = true;
            this.label23.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label23.Location = new System.Drawing.Point(748, 29);
            this.label23.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label23.Name = "label23";
            this.label23.Size = new System.Drawing.Size(144, 22);
            this.label23.TabIndex = 131;
            this.label23.Text = "Corporate Email:";
            // 
            // tb_empID
            // 
            this.tb_empID.Enabled = false;
            this.tb_empID.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.tb_empID.ForeColor = System.Drawing.Color.Black;
            this.tb_empID.Location = new System.Drawing.Point(338, 76);
            this.tb_empID.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.tb_empID.Name = "tb_empID";
            this.tb_empID.Size = new System.Drawing.Size(301, 28);
            this.tb_empID.TabIndex = 2;
            // 
            // label9
            // 
            this.label9.AutoSize = true;
            this.label9.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label9.Location = new System.Drawing.Point(214, 81);
            this.label9.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(116, 22);
            this.label9.TabIndex = 122;
            this.label9.Text = "Employee ID:";
            // 
            // label54
            // 
            this.label54.AutoSize = true;
            this.label54.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label54.Location = new System.Drawing.Point(210, 292);
            this.label54.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label54.Name = "label54";
            this.label54.Size = new System.Drawing.Size(122, 22);
            this.label54.TabIndex = 93;
            this.label54.Text = "Place of Birth:";
            // 
            // cb_pob
            // 
            this.cb_pob.AutoCompleteMode = System.Windows.Forms.AutoCompleteMode.SuggestAppend;
            this.cb_pob.AutoCompleteSource = System.Windows.Forms.AutoCompleteSource.ListItems;
            this.cb_pob.Enabled = false;
            this.cb_pob.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.cb_pob.ForeColor = System.Drawing.Color.Black;
            this.cb_pob.FormattingEnabled = true;
            this.cb_pob.Items.AddRange(new object[] {
            "Makati City"});
            this.cb_pob.Location = new System.Drawing.Point(337, 292);
            this.cb_pob.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.cb_pob.Name = "cb_pob";
            this.cb_pob.Size = new System.Drawing.Size(300, 30);
            this.cb_pob.TabIndex = 7;
            // 
            // cb_status
            // 
            this.cb_status.AutoCompleteMode = System.Windows.Forms.AutoCompleteMode.SuggestAppend;
            this.cb_status.AutoCompleteSource = System.Windows.Forms.AutoCompleteSource.ListItems;
            this.cb_status.Enabled = false;
            this.cb_status.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.cb_status.ForeColor = System.Drawing.Color.Black;
            this.cb_status.FormattingEnabled = true;
            this.cb_status.Items.AddRange(new object[] {
            "Single",
            "Married",
            "Widowed"});
            this.cb_status.Location = new System.Drawing.Point(336, 340);
            this.cb_status.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.cb_status.Name = "cb_status";
            this.cb_status.Size = new System.Drawing.Size(300, 30);
            this.cb_status.TabIndex = 8;
            // 
            // label47
            // 
            this.label47.AutoSize = true;
            this.label47.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label47.Location = new System.Drawing.Point(248, 340);
            this.label47.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label47.Name = "label47";
            this.label47.Size = new System.Drawing.Size(66, 22);
            this.label47.TabIndex = 91;
            this.label47.Text = "Status:";
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label6.Location = new System.Drawing.Point(255, 248);
            this.label6.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(59, 22);
            this.label6.TabIndex = 33;
            this.label6.Text = "Email:";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label3.Location = new System.Drawing.Point(208, 162);
            this.label3.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(102, 22);
            this.label3.TabIndex = 31;
            this.label3.Text = "First Name:";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label2.Location = new System.Drawing.Point(212, 119);
            this.label2.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(101, 22);
            this.label2.TabIndex = 30;
            this.label2.Text = "Last Name:";
            // 
            // tb_lname
            // 
            this.tb_lname.Enabled = false;
            this.tb_lname.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.tb_lname.ForeColor = System.Drawing.Color.Black;
            this.tb_lname.Location = new System.Drawing.Point(339, 116);
            this.tb_lname.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.tb_lname.Name = "tb_lname";
            this.tb_lname.Size = new System.Drawing.Size(300, 28);
            this.tb_lname.TabIndex = 3;
            // 
            // tb_fname
            // 
            this.tb_fname.Enabled = false;
            this.tb_fname.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.tb_fname.ForeColor = System.Drawing.Color.Black;
            this.tb_fname.Location = new System.Drawing.Point(338, 158);
            this.tb_fname.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.tb_fname.Name = "tb_fname";
            this.tb_fname.Size = new System.Drawing.Size(300, 28);
            this.tb_fname.TabIndex = 4;
            // 
            // tb_mname
            // 
            this.tb_mname.Enabled = false;
            this.tb_mname.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.tb_mname.ForeColor = System.Drawing.Color.Black;
            this.tb_mname.Location = new System.Drawing.Point(338, 205);
            this.tb_mname.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.tb_mname.Name = "tb_mname";
            this.tb_mname.Size = new System.Drawing.Size(300, 28);
            this.tb_mname.TabIndex = 5;
            // 
            // tb_email
            // 
            this.tb_email.Enabled = false;
            this.tb_email.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.tb_email.ForeColor = System.Drawing.Color.Black;
            this.tb_email.Location = new System.Drawing.Point(338, 249);
            this.tb_email.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.tb_email.Name = "tb_email";
            this.tb_email.Size = new System.Drawing.Size(301, 28);
            this.tb_email.TabIndex = 6;
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label4.Location = new System.Drawing.Point(201, 205);
            this.label4.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(119, 22);
            this.label4.TabIndex = 32;
            this.label4.Text = "Middle Name:";
            // 
            // pic_Image
            // 
            this.pic_Image.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.pic_Image.Image = ((System.Drawing.Image)(resources.GetObject("pic_Image.Image")));
            this.pic_Image.Location = new System.Drawing.Point(1288, 20);
            this.pic_Image.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.pic_Image.Name = "pic_Image";
            this.pic_Image.Size = new System.Drawing.Size(219, 207);
            this.pic_Image.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom;
            this.pic_Image.TabIndex = 52;
            this.pic_Image.TabStop = false;
            // 
            // tb_cont_no
            // 
            this.tb_cont_no.Enabled = false;
            this.tb_cont_no.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.tb_cont_no.ForeColor = System.Drawing.Color.Black;
            this.tb_cont_no.Location = new System.Drawing.Point(336, 390);
            this.tb_cont_no.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.tb_cont_no.MaxLength = 11;
            this.tb_cont_no.Name = "tb_cont_no";
            this.tb_cont_no.Size = new System.Drawing.Size(300, 28);
            this.tb_cont_no.TabIndex = 9;
            this.tb_cont_no.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.tb_contact_no_KeyPress);
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label7.Location = new System.Drawing.Point(210, 390);
            this.label7.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(105, 22);
            this.label7.TabIndex = 35;
            this.label7.Text = "Contact No:";
            // 
            // tc_emp_info
            // 
            this.tc_emp_info.Controls.Add(this.tp_general_information);
            this.tc_emp_info.Controls.Add(this.tp_salary);
            this.tc_emp_info.Controls.Add(this.tp_Leave);
            this.tc_emp_info.Controls.Add(this.tp_loan);
            this.tc_emp_info.Controls.Add(this.tp_other_info);
            this.tc_emp_info.Controls.Add(this.tp_id);
            this.tc_emp_info.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.tc_emp_info.Location = new System.Drawing.Point(18, 18);
            this.tc_emp_info.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.tc_emp_info.Name = "tc_emp_info";
            this.tc_emp_info.SelectedIndex = 0;
            this.tc_emp_info.Size = new System.Drawing.Size(1632, 619);
            this.tc_emp_info.TabIndex = 66;
            // 
            // btn_export
            // 
            this.btn_export.BackColor = System.Drawing.Color.Transparent;
            this.btn_export.BackgroundColor = System.Drawing.Color.Transparent;
            this.btn_export.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(192)))), ((int)(((byte)(0)))), ((int)(((byte)(0)))));
            this.btn_export.BorderRadius = 8;
            this.btn_export.BorderSize = 1;
            this.btn_export.FlatAppearance.BorderSize = 0;
            this.btn_export.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btn_export.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btn_export.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(192)))), ((int)(((byte)(0)))), ((int)(((byte)(0)))));
            this.btn_export.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.btn_export.Location = new System.Drawing.Point(1427, 978);
            this.btn_export.Name = "btn_export";
            this.btn_export.Size = new System.Drawing.Size(219, 44);
            this.btn_export.TabIndex = 185;
            this.btn_export.Text = "Export to Excel";
            this.btn_export.TextColor = System.Drawing.Color.FromArgb(((int)(((byte)(192)))), ((int)(((byte)(0)))), ((int)(((byte)(0)))));
            this.btn_export.UseVisualStyleBackColor = false;
            this.btn_export.Click += new System.EventHandler(this.btn_export_Click);
            this.btn_export.MouseEnter += new System.EventHandler(this.btn_export_MouseEnter);
            this.btn_export.MouseLeave += new System.EventHandler(this.btn_export_MouseLeave);
            this.btn_export.MouseHover += new System.EventHandler(this.btn_export_MouseHover);
            // 
            // btn_Delete
            // 
            this.btn_Delete.BackColor = System.Drawing.Color.Transparent;
            this.btn_Delete.BackgroundColor = System.Drawing.Color.Transparent;
            this.btn_Delete.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(192)))), ((int)(((byte)(0)))), ((int)(((byte)(0)))));
            this.btn_Delete.BorderRadius = 8;
            this.btn_Delete.BorderSize = 1;
            this.btn_Delete.Enabled = false;
            this.btn_Delete.FlatAppearance.BorderSize = 0;
            this.btn_Delete.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btn_Delete.Font = new System.Drawing.Font("Microsoft Sans Serif", 8F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btn_Delete.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(192)))), ((int)(((byte)(0)))), ((int)(((byte)(0)))));
            this.btn_Delete.Image = global::Payroll.Properties.Resources.icons8_delete_30;
            this.btn_Delete.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.btn_Delete.Location = new System.Drawing.Point(927, 641);
            this.btn_Delete.Name = "btn_Delete";
            this.btn_Delete.Size = new System.Drawing.Size(175, 55);
            this.btn_Delete.TabIndex = 183;
            this.btn_Delete.Text = "DELETE";
            this.btn_Delete.TextColor = System.Drawing.Color.FromArgb(((int)(((byte)(192)))), ((int)(((byte)(0)))), ((int)(((byte)(0)))));
            this.btn_Delete.UseVisualStyleBackColor = false;
            this.btn_Delete.Click += new System.EventHandler(this.btn_Delete_Click);
            this.btn_Delete.MouseEnter += new System.EventHandler(this.btn_Delete_MouseEnter);
            this.btn_Delete.MouseLeave += new System.EventHandler(this.btn_Delete_MouseLeave);
            this.btn_Delete.MouseHover += new System.EventHandler(this.btn_Delete_MouseHover);
            // 
            // btn_update
            // 
            this.btn_update.BackColor = System.Drawing.Color.Transparent;
            this.btn_update.BackgroundColor = System.Drawing.Color.Transparent;
            this.btn_update.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(192)))), ((int)(((byte)(0)))), ((int)(((byte)(0)))));
            this.btn_update.BorderRadius = 8;
            this.btn_update.BorderSize = 1;
            this.btn_update.Enabled = false;
            this.btn_update.FlatAppearance.BorderSize = 0;
            this.btn_update.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btn_update.Font = new System.Drawing.Font("Microsoft Sans Serif", 8F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btn_update.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(192)))), ((int)(((byte)(0)))), ((int)(((byte)(0)))));
            this.btn_update.Image = global::Payroll.Properties.Resources.Button_UPDATE_Red__4___1___1_;
            this.btn_update.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.btn_update.Location = new System.Drawing.Point(740, 641);
            this.btn_update.Name = "btn_update";
            this.btn_update.Size = new System.Drawing.Size(175, 55);
            this.btn_update.TabIndex = 181;
            this.btn_update.Text = "EDIT";
            this.btn_update.TextColor = System.Drawing.Color.FromArgb(((int)(((byte)(192)))), ((int)(((byte)(0)))), ((int)(((byte)(0)))));
            this.btn_update.UseVisualStyleBackColor = false;
            this.btn_update.Click += new System.EventHandler(this.btn_update_Click);
            this.btn_update.MouseEnter += new System.EventHandler(this.btn_update_MouseEnter);
            this.btn_update.MouseLeave += new System.EventHandler(this.btn_update_MouseLeave);
            this.btn_update.MouseHover += new System.EventHandler(this.btn_update_MouseHover);
            // 
            // btn_add
            // 
            this.btn_add.BackColor = System.Drawing.Color.Transparent;
            this.btn_add.BackgroundColor = System.Drawing.Color.Transparent;
            this.btn_add.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(192)))), ((int)(((byte)(0)))), ((int)(((byte)(0)))));
            this.btn_add.BorderRadius = 8;
            this.btn_add.BorderSize = 1;
            this.btn_add.FlatAppearance.BorderSize = 0;
            this.btn_add.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btn_add.Font = new System.Drawing.Font("Microsoft Sans Serif", 8F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btn_add.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(192)))), ((int)(((byte)(0)))), ((int)(((byte)(0)))));
            this.btn_add.Image = global::Payroll.Properties.Resources.Button_ADD_Red__1___1_;
            this.btn_add.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.btn_add.Location = new System.Drawing.Point(547, 641);
            this.btn_add.Name = "btn_add";
            this.btn_add.Size = new System.Drawing.Size(175, 55);
            this.btn_add.TabIndex = 180;
            this.btn_add.Text = "ADD";
            this.btn_add.TextColor = System.Drawing.Color.FromArgb(((int)(((byte)(192)))), ((int)(((byte)(0)))), ((int)(((byte)(0)))));
            this.btn_add.UseVisualStyleBackColor = false;
            this.btn_add.Click += new System.EventHandler(this.btn_add_employee_Click);
            this.btn_add.MouseEnter += new System.EventHandler(this.btn_add_MouseEnter);
            this.btn_add.MouseLeave += new System.EventHandler(this.btn_add_MouseLeave);
            this.btn_add.MouseHover += new System.EventHandler(this.btn_add_MouseHover);
            // 
            // Employee_Info
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(9F, 20F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.AutoSize = true;
            this.BackColor = System.Drawing.SystemColors.Control;
            this.ClientSize = new System.Drawing.Size(1657, 1034);
            this.Controls.Add(this.btn_export);
            this.Controls.Add(this.btn_Delete);
            this.Controls.Add(this.btn_update);
            this.Controls.Add(this.btn_add);
            this.Controls.Add(this.lv_employee);
            this.Controls.Add(this.lbl_category);
            this.Controls.Add(this.label13);
            this.Controls.Add(this.tb_search);
            this.Controls.Add(this.tc_emp_info);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedToolWindow;
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.Location = new System.Drawing.Point(298, 41);
            this.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.MaximizeBox = false;
            this.Name = "Employee_Info";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Employee Information";
            this.Load += new System.EventHandler(this.Employee_Info_Load);
            this.tp_id.ResumeLayout(false);
            this.groupBox1.ResumeLayout(false);
            this.groupBox1.PerformLayout();
            this.tp_other_info.ResumeLayout(false);
            this.tp_other_info.PerformLayout();
            this.groupBox6.ResumeLayout(false);
            this.groupBox6.PerformLayout();
            this.groupBox15.ResumeLayout(false);
            this.groupBox15.PerformLayout();
            this.groupBox14.ResumeLayout(false);
            this.groupBox14.PerformLayout();
            this.groupBox13.ResumeLayout(false);
            this.groupBox13.PerformLayout();
            this.tp_loan.ResumeLayout(false);
            this.lbl_comp_loan.ResumeLayout(false);
            this.lbl_comp_loan.PerformLayout();
            this.groupBox7.ResumeLayout(false);
            this.groupBox7.PerformLayout();
            this.groupBox9.ResumeLayout(false);
            this.groupBox9.PerformLayout();
            this.groupBox10.ResumeLayout(false);
            this.groupBox10.PerformLayout();
            this.tp_Leave.ResumeLayout(false);
            this.groupBox8.ResumeLayout(false);
            this.groupBox8.PerformLayout();
            this.tp_salary.ResumeLayout(false);
            this.groupBox5.ResumeLayout(false);
            this.groupBox5.PerformLayout();
            this.groupBox12.ResumeLayout(false);
            this.groupBox12.PerformLayout();
            this.groupBox11.ResumeLayout(false);
            this.groupBox11.PerformLayout();
            this.groupBox4.ResumeLayout(false);
            this.groupBox4.PerformLayout();
            this.groupBox3.ResumeLayout(false);
            this.groupBox3.PerformLayout();
            this.tp_general_information.ResumeLayout(false);
            this.groupBox2.ResumeLayout(false);
            this.groupBox2.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pic_Image)).EndInit();
            this.tc_emp_info.ResumeLayout(false);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion
        private System.Windows.Forms.TextBox tb_search;
        private System.Windows.Forms.Label label13;
        private System.Windows.Forms.Label lbl_category;
        private System.Windows.Forms.ListView lv_employee;
        private System.Windows.Forms.TabPage tp_id;
        private System.Windows.Forms.Label label103;
        private System.Windows.Forms.Label label104;
        private System.Windows.Forms.TextBox tb_id_lname;
        private System.Windows.Forms.TextBox tb_id_fname;
        private System.Windows.Forms.TextBox tb_id_mname;
        private System.Windows.Forms.TextBox tb_id_empID;
        private System.Windows.Forms.TextBox tb_sss_no;
        private System.Windows.Forms.TextBox tb_hdmf_no;
        private System.Windows.Forms.TextBox tb_tin_no;
        private System.Windows.Forms.TextBox tb_philhealth_no;
        private System.Windows.Forms.Label label106;
        private System.Windows.Forms.Label label43;
        private System.Windows.Forms.Label label42;
        private System.Windows.Forms.Label label41;
        private System.Windows.Forms.Label label40;
        private System.Windows.Forms.GroupBox groupBox1;
        private System.Windows.Forms.Label label88;
        private System.Windows.Forms.Label label105;
        private System.Windows.Forms.Label label119;
        private System.Windows.Forms.Label label56;
        private System.Windows.Forms.TabPage tp_other_info;
        private System.Windows.Forms.Label label49;
        private System.Windows.Forms.GroupBox groupBox6;
        private System.Windows.Forms.TextBox tb_second_con;
        private System.Windows.Forms.Label label93;
        private System.Windows.Forms.TextBox tb_relationship2;
        private System.Windows.Forms.Label label95;
        private System.Windows.Forms.TextBox tb_name2;
        private System.Windows.Forms.Label label96;
        private System.Windows.Forms.TextBox tb_primary_con;
        private System.Windows.Forms.Label label65;
        private System.Windows.Forms.TextBox tb_relationship;
        private System.Windows.Forms.Label label64;
        private System.Windows.Forms.TextBox tb_name;
        private System.Windows.Forms.Label label63;
        private System.Windows.Forms.Label label14;
        private System.Windows.Forms.RichTextBox rtb_homeAddress;
        private System.Windows.Forms.Label label50;
        private System.Windows.Forms.Label label52;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.Label label73;
        private System.Windows.Forms.Label label35;
        private System.Windows.Forms.Label label34;
        private System.Windows.Forms.TextBox tb_occupation_father;
        private System.Windows.Forms.TextBox tb_fathers_name;
        private System.Windows.Forms.TextBox tb_occupation_mother;
        private System.Windows.Forms.TextBox tb_mothers_name;
        private System.Windows.Forms.Label label48;
        private System.Windows.Forms.RichTextBox rtb_work_experience;
        private System.Windows.Forms.Label label53;
        private System.Windows.Forms.TabPage tp_loan;
        private System.Windows.Forms.GroupBox lbl_comp_loan;
        private System.Windows.Forms.Label label109;
        private System.Windows.Forms.Label label120;
        private System.Windows.Forms.Label label57;
        private System.Windows.Forms.Label label107;
        private System.Windows.Forms.Label label108;
        private System.Windows.Forms.TextBox tb_loan_lname;
        private System.Windows.Forms.TextBox tb_loan_fname;
        private System.Windows.Forms.TextBox tb_loan_mname;
        private System.Windows.Forms.TextBox tb_loan_empID;
        private System.Windows.Forms.Label label110;
        private System.Windows.Forms.GroupBox groupBox7;
        private System.Windows.Forms.Label label89;
        private System.Windows.Forms.Label label60;
        private System.Windows.Forms.TextBox tb_comp_loan_month_ded;
        private System.Windows.Forms.TextBox tb_comp_loan_bal;
        private System.Windows.Forms.Label label67;
        private System.Windows.Forms.TextBox tb_comp_loan_amnt;
        private System.Windows.Forms.Label label61;
        private System.Windows.Forms.GroupBox groupBox9;
        private System.Windows.Forms.Label label94;
        private System.Windows.Forms.TextBox tb_sss_loan_month_ded;
        private System.Windows.Forms.Label label78;
        private System.Windows.Forms.TextBox tb_sss_loan_bal;
        private System.Windows.Forms.TextBox tb_sss_loan_amnt;
        private System.Windows.Forms.Label label66;
        private System.Windows.Forms.GroupBox groupBox10;
        private System.Windows.Forms.Label label68;
        private System.Windows.Forms.TextBox tb_pag_ibig_loan_month_ded;
        private System.Windows.Forms.TextBox tb_pag_ibig_loan_bal;
        private System.Windows.Forms.TextBox tb_pag_ibig_loan_amnt;
        private System.Windows.Forms.Label label81;
        private System.Windows.Forms.Label label82;
        private System.Windows.Forms.TabPage tp_Leave;
        private System.Windows.Forms.GroupBox groupBox8;
        private System.Windows.Forms.Label label79;
        private System.Windows.Forms.Label label87;
        private System.Windows.Forms.Label label31;
        private System.Windows.Forms.Label label45;
        private System.Windows.Forms.Label label85;
        private System.Windows.Forms.Label label86;
        private System.Windows.Forms.TextBox tb_leave_lname;
        private System.Windows.Forms.TextBox tb_leave_fname;
        private System.Windows.Forms.TextBox tb_leave_mname;
        private System.Windows.Forms.TextBox tb_leave_empID;
        private System.Windows.Forms.Label label84;
        private System.Windows.Forms.TextBox tb_vacation_leave;
        private System.Windows.Forms.Label label70;
        private System.Windows.Forms.Label label71;
        private System.Windows.Forms.TextBox tb_sick_leave;
        private System.Windows.Forms.TabPage tp_salary;
        private System.Windows.Forms.GroupBox groupBox5;
        private System.Windows.Forms.Label label83;
        private System.Windows.Forms.DateTimePicker dtp_dateResign;
        private System.Windows.Forms.Label label122;
        private System.Windows.Forms.TextBox tb_bank_acc2;
        private System.Windows.Forms.Label label99;
        private System.Windows.Forms.Label label118;
        private System.Windows.Forms.Label label76;
        private System.Windows.Forms.ComboBox cb_department;
        private System.Windows.Forms.Label label21;
        private System.Windows.Forms.TextBox tb_designation;
        private System.Windows.Forms.Label label20;
        private System.Windows.Forms.DateTimePicker dtp_date_hired;
        private System.Windows.Forms.ComboBox cb_employee_status;
        private System.Windows.Forms.Label label19;
        private System.Windows.Forms.Label label18;
        private System.Windows.Forms.Label label97;
        private System.Windows.Forms.Label label98;
        private System.Windows.Forms.TextBox tb_sal_lname;
        private System.Windows.Forms.TextBox tb_sal_fname;
        private System.Windows.Forms.TextBox tb_sal_mname;
        private System.Windows.Forms.TextBox tb_sal_empID;
        private System.Windows.Forms.Label label100;
        private System.Windows.Forms.Label label91;
        private System.Windows.Forms.TextBox tb_bank_acc;
        private System.Windows.Forms.TextBox tb_daily_hour_rate;
        private System.Windows.Forms.Label label39;
        internal System.Windows.Forms.TextBox tb_daily_rate;
        private System.Windows.Forms.Label label69;
        private System.Windows.Forms.Label label58;
        private System.Windows.Forms.ComboBox cb_bank_name;
        private System.Windows.Forms.ComboBox cb_payRate;
        private System.Windows.Forms.Label label37;
        private System.Windows.Forms.Label label27;
        private System.Windows.Forms.ComboBox cb_payroll_mode;
        private System.Windows.Forms.Label label28;
        private System.Windows.Forms.ComboBox cb_confidentiality;
        private System.Windows.Forms.TextBox tb_salary;
        private System.Windows.Forms.Label label29;
        private System.Windows.Forms.Label label55;
        private System.Windows.Forms.Label label115;
        private System.Windows.Forms.Label label116;
        private System.Windows.Forms.Label label117;
        private System.Windows.Forms.TextBox tb_philhealth;
        private System.Windows.Forms.Label label17;
        private System.Windows.Forms.CheckBox chk_philhealth;
        private System.Windows.Forms.TextBox tb_pag_ibig;
        private System.Windows.Forms.CheckBox chk_pag_ibig;
        private System.Windows.Forms.Label label38;
        private System.Windows.Forms.Label label33;
        private System.Windows.Forms.TextBox tb_sss;
        private System.Windows.Forms.Label label32;
        private System.Windows.Forms.CheckBox chk_sss;
        private System.Windows.Forms.TextBox tb_15th_month_pay;
        private System.Windows.Forms.CheckBox chb_15month_pay;
        private System.Windows.Forms.Label label75;
        private System.Windows.Forms.TextBox tb_14th_month_pay;
        private System.Windows.Forms.CheckBox chb_14month_pay;
        private System.Windows.Forms.Label label74;
        private System.Windows.Forms.TextBox tb_13th_month_pay;
        private System.Windows.Forms.CheckBox chb_13month_pay;
        private System.Windows.Forms.Label label12;
        private System.Windows.Forms.TabPage tp_general_information;
        private System.Windows.Forms.GroupBox groupBox2;
        private System.Windows.Forms.ComboBox cb_statustype;
        private System.Windows.Forms.Label label62;
        private System.Windows.Forms.Label label125;
        private System.Windows.Forms.Label label124;
        private System.Windows.Forms.Label label123;
        private System.Windows.Forms.Label lbl_emp_no;
        private System.Windows.Forms.RichTextBox rtb_address;
        private System.Windows.Forms.ComboBox cb_nationality;
        private System.Windows.Forms.TextBox tb_name_of_spouse;
        private System.Windows.Forms.ComboBox cb_religion;
        private System.Windows.Forms.Label label46;
        private System.Windows.Forms.Label label44;
        private System.Windows.Forms.Label label51;
        private System.Windows.Forms.ComboBox cb_gender;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.Label label11;
        private System.Windows.Forms.Label lbl_number_of_employee;
        private System.Windows.Forms.Label label30;
        private System.Windows.Forms.DateTimePicker dtp_birthdate;
        private System.Windows.Forms.Label label15;
        private System.Windows.Forms.TextBox tb_comp_email;
        private System.Windows.Forms.TextBox tb_telphone_no;
        private System.Windows.Forms.Label label24;
        private System.Windows.Forms.Label label23;
        private System.Windows.Forms.TextBox tb_empID;
        private System.Windows.Forms.Label label9;
        private System.Windows.Forms.Label label54;
        private System.Windows.Forms.ComboBox cb_pob;
        private System.Windows.Forms.ComboBox cb_status;
        private System.Windows.Forms.Label label47;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.TextBox tb_lname;
        private System.Windows.Forms.TextBox tb_fname;
        private System.Windows.Forms.TextBox tb_mname;
        private System.Windows.Forms.TextBox tb_email;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.PictureBox pic_Image;
        private System.Windows.Forms.TextBox tb_cont_no;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.TabControl tc_emp_info;
        private System.Windows.Forms.ComboBox cb_paySchedule;
        private System.Windows.Forms.Label label10;
        private RJButtons.RJButton btn_add;
        private RJButtons.RJButton btn_update;
        private System.Windows.Forms.Label label26;
        private System.Windows.Forms.ComboBox cb_reason;
        private System.Windows.Forms.Label label36;
        private System.Windows.Forms.ComboBox cb_bank_name2;
        private System.Windows.Forms.TextBox tb_laundryAllowance;
        private System.Windows.Forms.TextBox tb_riceAllowance;
        private System.Windows.Forms.Label label113;
        private System.Windows.Forms.Label label112;
        private System.Windows.Forms.Label label111;
        private System.Windows.Forms.Label label102;
        private System.Windows.Forms.Label label92;
        private System.Windows.Forms.TextBox tb_otherAllowance;
        private System.Windows.Forms.TextBox tb_MedicalBenefits;
        private System.Windows.Forms.TextBox tb_mealAllowance;
        private System.Windows.Forms.Label label114;
        private RJButtons.RJButton btn_Delete;
        private System.Windows.Forms.TextBox tb_elementary;
        private System.Windows.Forms.TextBox tb_highSchool;
        private System.Windows.Forms.TextBox tb_tertiary;
        private System.Windows.Forms.TextBox tb_course;
        private RJButtons.RJButton btn_upload;
        private RJButtons.RJButton btn_export;
        private System.Windows.Forms.TextBox tb_minRate;
        private System.Windows.Forms.Label label72;
        private System.Windows.Forms.Label label131;
        private System.Windows.Forms.Label label130;
        private System.Windows.Forms.Label label129;
        private System.Windows.Forms.Label label128;
        private System.Windows.Forms.Label label127;
        private System.Windows.Forms.Label label126;
        private System.Windows.Forms.Label label121;
        private System.Windows.Forms.Label label77;
        private System.Windows.Forms.Label label16;
        private System.Windows.Forms.Label label132;
        private System.Windows.Forms.Label label133;
        private System.Windows.Forms.Label label134;
        private System.Windows.Forms.Label label135;
        private System.Windows.Forms.Label label136;
        private System.Windows.Forms.TextBox tb_info_lname;
        private System.Windows.Forms.TextBox tb_info_fname;
        private System.Windows.Forms.TextBox tb_info_mname;
        private System.Windows.Forms.TextBox tb_info_empID;
        private System.Windows.Forms.Label label137;
        private System.Windows.Forms.GroupBox groupBox3;
        private System.Windows.Forms.GroupBox groupBox4;
        private System.Windows.Forms.GroupBox groupBox11;
        private System.Windows.Forms.GroupBox groupBox12;
        private System.Windows.Forms.GroupBox groupBox13;
        private System.Windows.Forms.GroupBox groupBox15;
        private System.Windows.Forms.GroupBox groupBox14;
        private System.Windows.Forms.Label label1;
    }
}