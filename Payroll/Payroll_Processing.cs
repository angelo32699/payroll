﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using MySql.Data.MySqlClient;
using System.Configuration;
using System.Globalization;

namespace Payroll
{


    public partial class Payroll_Processing : Form
    {
        MySqlCommand cmd;
        MySqlDataAdapter da;
        DataTable dt;




        string mmyyyy;
        string date_period;
        string ctr;

        public Payroll_Processing()
        {
            InitializeComponent();

        }
        
        


        public void processing_payroll_process()
        {
            string myconnection = ConfigurationManager.ConnectionStrings["MyConstring"].ConnectionString;
            MySqlConnection connection = new MySqlConnection(myconnection);

            try
            {
                connection.Open();
                cmd = new MySqlCommand();
                cmd.Connection = connection;
                cmd.CommandText = "UPDATE pay_trans SET process_tag  = 'True' where process_tag = 'False'";

                da = new MySqlDataAdapter();
                da.SelectCommand = cmd;
                dt = new DataTable();
                da.Fill(dt);
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
            finally
            {
                connection.Close();
            }

        }
        public void reverse()
        {

            string myconnection = ConfigurationManager.ConnectionStrings["MyConstring"].ConnectionString;
            MySqlConnection connection = new MySqlConnection(myconnection);


            try
            {
                connection.Open();
                cmd = new MySqlCommand();
                cmd.Connection = connection;
                cmd.CommandText = "UPDATE pay_trans SET process_tag = 'False' where date_from = '" + dateTimePicker2.Text + "' and date_to = '" + dateTimePicker1.Text + "' and date_period = '" + domainUpDown1.Text + "'";

                da = new MySqlDataAdapter();
                da.SelectCommand = cmd;
                dt = new DataTable();
                da.Fill(dt);





            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
            finally
            {
                connection.Close();
            }




        }

        public void delete_pay_detail()
        {
            string myconnection = ConfigurationManager.ConnectionStrings["MyConstring"].ConnectionString;
            MySqlConnection connection = new MySqlConnection(myconnection);

            string date_from_month = dateTimePicker2.Value.ToString("MMyyyy");

            try
            {
                connection.Open();
                cmd = new MySqlCommand();
                cmd.Connection = connection;
                cmd.CommandText = "Delete from pay_details where yyyymm  = '" + date_from_month + "' and date_period = '" + domainUpDown1.Text + "'";
                da = new MySqlDataAdapter();
                da.SelectCommand = cmd;
                dt = new DataTable();
                da.Fill(dt);

            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
            finally
            {
                connection.Close();
            }


        }
        public void delete_rec_tax()
        {
            string myconnection = ConfigurationManager.ConnectionStrings["MyConstring"].ConnectionString;
            MySqlConnection connection = new MySqlConnection(myconnection);

           

            try
            {
                connection.Open();
                cmd = new MySqlCommand();
                cmd.Connection = connection;
                cmd.CommandText = "Delete from rec_tax";
                da = new MySqlDataAdapter();
                da.SelectCommand = cmd;
                dt = new DataTable();
                da.Fill(dt);

            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
            finally
            {
                connection.Close();
            }
        }

        public void delete_tax_inc()
        {
            string myconnection = ConfigurationManager.ConnectionStrings["MyConstring"].ConnectionString;
            MySqlConnection connection = new MySqlConnection(myconnection);



            try
            {
                connection.Open();
                cmd = new MySqlCommand();
                cmd.Connection = connection;
                cmd.CommandText = "Delete from tax_inc";
                da = new MySqlDataAdapter();
                da.SelectCommand = cmd;
                dt = new DataTable();
                da.Fill(dt);

            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
            finally
            {

            }
        }

        public void delete_wtax_year_end()
        {
            string myconnection = ConfigurationManager.ConnectionStrings["MyConstring"].ConnectionString;
            MySqlConnection connection = new MySqlConnection(myconnection);



            try
            {
                connection.Open();
                cmd = new MySqlCommand();
                cmd.Connection = connection;
                cmd.CommandText = "Delete from update_wtax_year_end";
                da = new MySqlDataAdapter();
                da.SelectCommand = cmd;
                dt = new DataTable();
                da.Fill(dt);

            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
            finally
            {

            }
        }


        public void insert_pay_details()
        {

            string myconnection = ConfigurationManager.ConnectionStrings["MyConstring"].ConnectionString;
            MySqlConnection connection = new MySqlConnection(myconnection);




            string date_from_month = dateTimePicker2.Value.ToString("MMyyyy");


            connection.Open();
            cmd = new MySqlCommand();
            cmd.Connection = connection;
            cmd.CommandText = "Select * from pay_trans left join  employee on employee.emp_no = pay_trans.emp_no left join salary on pay_trans.sal_id = salary.salary_id left join loan on pay_trans.loan_id = loan.loan_id where process_tag = 'False'";
            da = new MySqlDataAdapter();
            da.SelectCommand = cmd;
            dt = new DataTable();
            da.Fill(dt);



            for (int i = 0; i < dt.Rows.Count; i++)
            {
                DataRow dr = dt.Rows[i];

                string emp_id = (dr["emp_id"].ToString());
                string lname = (dr["lname"].ToString());
                string fname = (dr["fname"].ToString());
                string mname = (dr["mname"].ToString());
                string department = (dr["department"].ToString());
                string emp_status = (dr["emp_status"].ToString());
                string absences = (dr["absences"].ToString());

                string retrieve_montly_pay = (dr["salary"].ToString());
                string retrieve_reg_pay_trans = (dr["regular_pay"].ToString());
                string daily_rate = (dr["daily_rate"].ToString());
                string retrieve_tax_sub_pay_trans = (dr["tax_sub"].ToString());
                string retrieve_other_inc = (dr["other_inc"].ToString());
                string retrieve_deductions = (dr["deductions"].ToString());
                string retrieve_ot_pay_trans = (dr["ot_pay"].ToString());
                string retrieve_gross_inc = (dr["gross_pay"].ToString());
                string retrieve_gross_ded = (dr["gross_ded"].ToString());
                string retrieve_net_pay = (dr["net_pay"].ToString());
                string retrieve_wtax = (dr["wtax"].ToString());
                string retrieve_other_pay_trans = (dr["other_ded"].ToString());
                string retrieve_company_loan_pay_trans = (dr["comp_month_ded"].ToString());
                string retrieve_sss_loan_pay_trans = (dr["sss_month_ded"].ToString());
                string retrieve_pag_ibig_loan_pay_trans = (dr["pag_ibig_month_ded"].ToString());
                string comp_loan_bal = (dr["comp_bal"].ToString());
                string sss_loan_bal = (dr["sss_bal"].ToString());
                string pag_ibig_loan_bal = (dr["pag_ibig_bal"].ToString());
                string retrieve_sss_sal = (dr["sss_ee"].ToString());
                string retrieve_pag_ibig_sal = (dr["pag_ibig_ee"].ToString());
                string retrieve_philhealth_sal = (dr["philhealth_ee"].ToString());
                string retrieve_sss_er_sal = (dr["sss_er"].ToString());
                string retrieve_pag_ibig_er_sal = (dr["pag_ibig_er"].ToString());
                string retrieve_philhealth_er_sal = (dr["philhealth_er"].ToString());
                string bank_account = (dr["bank_acc"].ToString());




                //insert pay details
                string myconnection2 = ConfigurationManager.ConnectionStrings["MyConstring"].ConnectionString;
                MySqlConnection connection2 = new MySqlConnection(myconnection2);




                string insertQuery2 = "INSERT INTO pay_details(yyyymm, date_period, emp_id, fullname, department, emp_status, absences, month_pay, regular_pay, daily_rate, tax_sub, others_pay_det, deductions, overtime_pay_det, gross_inc, gross_ded, net_pay, wtax, other_ded_det, sss_ee, sss_er, philhealth_ee, philhealth_er, pag_ibig_ee, pag_ibig_er, company_loan, pag_ibig_loan, sss_loan, comp_bal, sss_bal, pag_ibig_bal)Values('" + date_from_month + "', '" + domainUpDown1.Text + "', '" + emp_id + "',  '" + lname + " " + fname + " " + mname + "', '" + department + "',  '" + emp_status + "', '" + absences + "', '" + retrieve_montly_pay + "', '" + retrieve_reg_pay_trans + "', '" + daily_rate + "', '" + retrieve_tax_sub_pay_trans + "',  '" + retrieve_other_inc + "', '" + retrieve_deductions + "', '" + retrieve_ot_pay_trans + "', '" + retrieve_gross_inc + "', '" + retrieve_gross_ded + "',  '" + retrieve_net_pay + "', '" + retrieve_wtax + "',  '" + retrieve_other_pay_trans + "', '" + retrieve_sss_sal + "' , '" + retrieve_sss_er_sal + "' , '" + retrieve_philhealth_sal + "' , '" + retrieve_philhealth_er_sal + "', '" + retrieve_pag_ibig_sal + "', '" + retrieve_pag_ibig_er_sal + "', '" + retrieve_company_loan_pay_trans + "', '" + retrieve_pag_ibig_loan_pay_trans + "', '" + retrieve_sss_loan_pay_trans + "', '" + comp_loan_bal + "', '" + sss_loan_bal + "', '" + pag_ibig_loan_bal + "')";



                connection2.Open();
                MySqlCommand mySqlCommand2 = new MySqlCommand(insertQuery2, connection2);



                if (mySqlCommand2.ExecuteNonQuery() == 1)
                {
                    //MessageBox.Show("Successfully Process", "Payroll Processing", MessageBoxButtons.OK, MessageBoxIcon.Information);



                }
                connection2.Close();





                //insert payslip

                string myconnection3 = ConfigurationManager.ConnectionStrings["MyConstring"].ConnectionString;
                MySqlConnection connection3 = new MySqlConnection(myconnection3);




                string insertQuery3 = "INSERT INTO payslip(emp_id, fullname, date_from, date_to, pay_date, department, absences, monthly_pay, basic_pay, tax_sub, ot_pay, other_inc, gross_pay, adj_refund, net_pay, daily_rate, wtax, sss_ee, pag_ibig_ee, philhealth_ee, other_ded, comp_loan, sss_loan, pag_ibig_loan, gross_ded, comp_bal, sss_bal, pag_ibig_bal)Values('" + emp_id + "', '" + lname + " " + fname + " " + mname + "', '" + dateTimePicker2.Text + "', '" + dateTimePicker1.Text + "', '" + label6.Text + "', '" + department + "',  '" + absences + "', '" + retrieve_montly_pay + "', '" + retrieve_reg_pay_trans + "', '" + retrieve_tax_sub_pay_trans + "', '" + retrieve_ot_pay_trans + "',  '" + retrieve_other_inc + "', '" + retrieve_gross_inc + "', '" + retrieve_deductions + "', '" + retrieve_net_pay + "', '" + daily_rate + "', '" + retrieve_wtax + "', '" + retrieve_sss_sal + "', '" + retrieve_pag_ibig_sal + "', '" + retrieve_philhealth_sal + "', '" + retrieve_other_pay_trans + "'  , '" + retrieve_company_loan_pay_trans + "' , '" + retrieve_sss_loan_pay_trans + "', '" + retrieve_pag_ibig_loan_pay_trans + "', '" + retrieve_gross_ded + "', '" + comp_loan_bal + "', '" + sss_loan_bal + "', '" + pag_ibig_loan_bal + "')";


                connection3.Open();
                MySqlCommand mySqlCommand3 = new MySqlCommand(insertQuery3, connection3);



                if (mySqlCommand3.ExecuteNonQuery() == 1)
                {
                    // MessageBox.Show("Successfully Process", "Payroll Processing", MessageBoxButtons.OK, MessageBoxIcon.Information);

                }
                connection3.Close();






                //insert bank remittance

                string myconnection4 = ConfigurationManager.ConnectionStrings["MyConstring"].ConnectionString;
                MySqlConnection connection4 = new MySqlConnection(myconnection4);




                string insertQuery4 = "INSERT INTO bank_remittance(emp_id, emp_name, bank_acc, net_pay)Values('" + emp_id + "','" + lname + " " + fname + " " + mname + "', '" + bank_account + "', '" + retrieve_net_pay + "')";


                connection4.Open();
                MySqlCommand mySqlCommand4 = new MySqlCommand(insertQuery4, connection4);



                if (mySqlCommand4.ExecuteNonQuery() == 1)
                {
                    // MessageBox.Show("Successfully Process", "Payroll Processing", MessageBoxButtons.OK, MessageBoxIcon.Information);

                }
                connection4.Close();





            }


            delete_monthly_tax();




            connection.Close();
        }
        public void sum_ytd()
        {
            string myconnection = ConfigurationManager.ConnectionStrings["MyConstring"].ConnectionString;
            MySqlConnection connection = new MySqlConnection(myconnection);

            try
            {
                connection.Open();
                cmd = new MySqlCommand();
                cmd.Connection = connection;
                cmd.CommandText = "UPDATE payslip SET payslip.ytd_tax = (SELECT SUM(pay_details.wtax) as wtax from pay_details WHERE pay_details.fullname = payslip.fullname AND pay_details.year_end = 'False')";
                da = new MySqlDataAdapter();
                da.SelectCommand = cmd;
                dt = new DataTable();
                da.Fill(dt);

            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
            finally
            {
                connection.Close();
            }



            try
            {
                connection.Open();
                cmd = new MySqlCommand();
                cmd.Connection = connection;
                cmd.CommandText = "UPDATE payslip SET payslip.ytd_gross = (SELECT SUM(pay_details.gross_inc) as gross_inc FROM pay_details WHERE pay_details.fullname = payslip.fullname AND pay_details.year_end = 'False')";
                da = new MySqlDataAdapter();
                da.SelectCommand = cmd;
                dt = new DataTable();
                da.Fill(dt);

            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
            finally
            {
                connection.Close();
            }
        }


        public void delete_monthly_tax()
        {
            string myconnection = ConfigurationManager.ConnectionStrings["MyConstring"].ConnectionString;
            MySqlConnection connection = new MySqlConnection(myconnection);

            try
            {
                connection.Open();
                cmd = new MySqlCommand();
                cmd.Connection = connection;
                string query2 = "DELETE FROM monthly_tax";
                cmd.CommandText = query2;
                da = new MySqlDataAdapter();
                da.SelectCommand = cmd;
                dt = new DataTable();
                da.Fill(dt);


            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
            finally
            {
                connection.Close();
            }
        }


      
        public void date_process()
        {
            string myconnection = ConfigurationManager.ConnectionStrings["MyConstring"].ConnectionString;
            MySqlConnection connection = new MySqlConnection(myconnection);

            try
            {
                connection.Open();
                cmd = new MySqlCommand();
                cmd.Connection = connection;
                cmd.CommandText = "Select * from pay_trans where process_tag = 'False'";
                da = new MySqlDataAdapter();
                da.SelectCommand = cmd;
                dt = new DataTable();
                da.Fill(dt);

                for (int i = 0; i < dt.Rows.Count; i++)
                {
                    DataRow dr = dt.Rows[i];

                    dateTimePicker2.Text = (dr["date_from"].ToString());
                    dateTimePicker1.Text = (dr["date_to"].ToString());
                    domainUpDown1.Text = (dr["date_period"].ToString());

                }

            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
            finally
            {
                connection.Close();
            }
        }
        public void delete_payslip()
        {
            string myconnection = ConfigurationManager.ConnectionStrings["MyConstring"].ConnectionString;
            MySqlConnection connection = new MySqlConnection(myconnection);

            try
            {
                connection.Open();
                cmd = new MySqlCommand();
                cmd.Connection = connection;
                string query2 = "DELETE FROM payslip";
                cmd.CommandText = query2;
                da = new MySqlDataAdapter();
                da.SelectCommand = cmd;
                dt = new DataTable();
                da.Fill(dt);


            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
            finally
            {
                connection.Close();
            }
        }

        public void delete_bank_remittance()
        {
            string myconnection = ConfigurationManager.ConnectionStrings["MyConstring"].ConnectionString;
            MySqlConnection connection = new MySqlConnection(myconnection);

            try
            {
                connection.Open();
                cmd = new MySqlCommand();
                cmd.Connection = connection;
                string query2 = "DELETE FROM bank_remittance";
                cmd.CommandText = query2;
                da = new MySqlDataAdapter();
                da.SelectCommand = cmd;
                dt = new DataTable();
                da.Fill(dt);


            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
            finally
            {
                connection.Close();
            }
        }




        public void check_payroll_process()
        {
            string myconnection = ConfigurationManager.ConnectionStrings["MyConstring"].ConnectionString;
            MySqlConnection connection = new MySqlConnection(myconnection);

            try
            {
                connection.Open();
                cmd = new MySqlCommand();
                cmd.Connection = connection;
                cmd.CommandText = "Select * FROM pay_details";
                da = new MySqlDataAdapter();
                da.SelectCommand = cmd;
                dt = new DataTable();
                da.Fill(dt);

                for (int i = 0; i < dt.Rows.Count; i++)
                {
                    DataRow dr = dt.Rows[i];

                    mmyyyy = (dr["yyyymm"].ToString());
                    date_period = (dr["date_period"].ToString());

                }

            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
            finally
            {
                connection.Close();
            }
        }

        public void find_max_ctr()
        {
            string myconnection = ConfigurationManager.ConnectionStrings["MyConstring"].ConnectionString;
            MySqlConnection connection = new MySqlConnection(myconnection);

            try
            {
                connection.Open();
                cmd = new MySqlCommand();
                cmd.Connection = connection;
                cmd.CommandText = "SELECT MAX(ctr) as ctr from pay_details";
                da = new MySqlDataAdapter();
                da.SelectCommand = cmd;
                dt = new DataTable();
                da.Fill(dt);

                for (int i = 0; i < dt.Rows.Count; i++)
                {
                    DataRow dr = dt.Rows[i];


                    ctr = (dr["ctr"].ToString());

                }

            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
            finally
            {
                connection.Close();
            }
        }

            public void update_ctr()
           {
            string myconnection = ConfigurationManager.ConnectionStrings["MyConstring"].ConnectionString;
            MySqlConnection connection = new MySqlConnection(myconnection);



            string date_from_month = dateTimePicker2.Value.ToString("MMyyyy");


            int ctr_max = Convert.ToInt32(ctr);
            int ctr_total = ctr_max + 1;


            try
            {
                connection.Open();
                cmd = new MySqlCommand();
                cmd.Connection = connection;
                cmd.CommandText = "Update pay_details set ctr = '" + ctr_total + "' where yyyymm = '" + date_from_month + "' and date_period = '" + domainUpDown1.Text + "' ";
                da = new MySqlDataAdapter();
                da.SelectCommand = cmd;
                dt = new DataTable();
                da.Fill(dt);

            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
            finally
            {
                connection.Close();
            }
        }
        public void update_zero_wtax()
        {



            string myconnection = ConfigurationManager.ConnectionStrings["MyConstring"].ConnectionString;
            MySqlConnection connection = new MySqlConnection(myconnection);

            try
            {
                connection.Open();
                cmd = new MySqlCommand();
                cmd.Connection = connection;
                cmd.CommandText = "UPDATE pay_details SET wtax = 0 where emp_status = 'Regular' and  ctr = 24";
                da = new MySqlDataAdapter();
                da.SelectCommand = cmd;
                dt = new DataTable();
                da.Fill(dt);

            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
            finally
            {
                connection.Close();
            }
        }

        

        public void correct_tax()
        {
            string myconnection = ConfigurationManager.ConnectionStrings["MyConstring"].ConnectionString;
            MySqlConnection connection = new MySqlConnection(myconnection);

            try
            {
                connection.Open();
                cmd = new MySqlCommand();
                cmd.Connection = connection;
                cmd.CommandText = "SELECT * FROM pay_details where ctr = 24";
                da = new MySqlDataAdapter();
                da.SelectCommand = cmd;
                dt = new DataTable();
                da.Fill(dt);

                for (int i = 0; i < 1; i++)
                {
                    DataRow dr = dt.Rows[i];

                   //1
                    update_zero_wtax();
                    //2
                    insert_rec_wtax();
                    total_non_tax();
                    //3
                    YTD_TAX();
                    //4
                    update_wtax();
                    //5
                    //Consultant_tax();
                    //6
                    update_exact_tax();
                    //7
                    update_payslip_wtax();
                    //8
                    update_payslip_gross_ded();
                    //
                    update_paydetails_gross_ded();

                }
            }
            catch (Exception ex)
            {
                /*MessageBox.Show(ex.Message);*/
            }
            finally
            {
                connection.Close();
            }
        }

        public void insert_rec_wtax()
        {
            string myconnection3 = ConfigurationManager.ConnectionStrings["MyConstring"].ConnectionString;
            MySqlConnection connection3 = new MySqlConnection(myconnection3);


            connection3.Open();
            cmd = new MySqlCommand();
            cmd.Connection = connection3;
            cmd.CommandText = "SELECT salary.emp_id as emp_id, salary.mntbonus as non_tax_inc, SUM(pay_details.gross_inc) as gross_inc, SUM(pay_details.sss_ee) as sss_ee, SUM(pay_details.philhealth_ee) as philhealth_ee, SUM(pay_details.pag_ibig_ee) as pag_ibig_ee, SUM(pay_details.wtax) as wtax FROM salary LEFT JOIN pay_details ON salary.emp_id = pay_details.emp_id where salary.emp_status = 'Contractual' or salary.emp_status = 'Regular' or salary.emp_status = 'Probationary' GROUP BY salary.emp_id";
            da = new MySqlDataAdapter();
            da.SelectCommand = cmd;
            dt = new DataTable();
            da.Fill(dt);

            for (int i = 0; i < dt.Rows.Count; i++)
            {
                DataRow dr = dt.Rows[i];

                string emp_id = (dr["emp_id"].ToString());
                string non_tax_income = (dr["non_tax_inc"].ToString());
                string gross_inc = (dr["gross_inc"].ToString());
                string sss_ee = (dr["sss_ee"].ToString());
                string philhealth_ee = (dr["philhealth_ee"].ToString());
                string pag_ibig_ee = (dr["pag_ibig_ee"].ToString());
                string wtax = (dr["wtax"].ToString());

                string myconnection4 = ConfigurationManager.ConnectionStrings["MyConstring"].ConnectionString;
                MySqlConnection connection4 = new MySqlConnection(myconnection4);

                string insertQuery3 = "INSERT INTO rec_tax(emp_id, non_tax, sss_annual, philhealth_annual, pag_ibig_annual, gross_inc_annual, wtax_annual_temp)Values('" + emp_id + "', '" + non_tax_income + "', '" + sss_ee + "', '" + philhealth_ee + "', '" + pag_ibig_ee + "', '" + gross_inc + "', '"+wtax+"')";

                connection4.Open();
                MySqlCommand mySqlCommand3 = new MySqlCommand(insertQuery3, connection4);



                if (mySqlCommand3.ExecuteNonQuery() == 1)
                {
                    //MessageBox.Show("Successfully Process", "Payroll Processing", MessageBoxButtons.OK, MessageBoxIcon.Information);

                }
                connection4.Close();

            }
            connection3.Close();


        }

        public void total_non_tax()
        {
            string myconnection = ConfigurationManager.ConnectionStrings["MyConstring"].ConnectionString;
            MySqlConnection connection = new MySqlConnection(myconnection);

            try
            {
                connection.Open();
                cmd = new MySqlCommand();
                cmd.Connection = connection;
                cmd.CommandText = "UPDATE rec_tax SET total_non_tax_inc = sss_annual + philhealth_annual +  pag_ibig_annual + non_tax, tax_inc = gross_inc_annual - total_non_tax_inc";
                da = new MySqlDataAdapter();
                da.SelectCommand = cmd;
                dt = new DataTable();
                da.Fill(dt);

            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
            finally
            {
                connection.Close();
            }
        }

        public void YTD_TAX()
        {

            string myconnection3 = ConfigurationManager.ConnectionStrings["MyConstring"].ConnectionString;
            MySqlConnection connection3 = new MySqlConnection(myconnection3);



            cmd = new MySqlCommand();
            cmd.Connection = connection3;
            cmd.CommandText = "SELECT * FROM tax join rec_tax WHERE tax.min_amnt <= rec_tax.tax_inc AND tax.max_amnt >= rec_tax.tax_inc";
            da = new MySqlDataAdapter();
            da.SelectCommand = cmd;
            dt = new DataTable();
            da.Fill(dt);


            for (int x = 0; x < dt.Rows.Count; x++)
            {
                DataRow dr2 = dt.Rows[x];


                string emp_id = (dr2["emp_id"].ToString());
                string tax_due = (dr2["tax_due"].ToString());
                string less_amnt = (dr2["less_amnt"].ToString());
                string percent = (dr2["percent"].ToString());

                string tax_income = (dr2["tax_inc"].ToString());

                decimal total_tax_due = Convert.ToDecimal(tax_due);
                decimal total_less_amnt = Convert.ToDecimal(less_amnt);
                decimal total_percentage = Convert.ToDecimal(percent);
                decimal total_taxable_income = Convert.ToDecimal(tax_income);

                decimal get_percentage = total_percentage / 100;

                decimal temp_tax1 = (total_taxable_income - total_less_amnt) * get_percentage;
                decimal total_annual_tax_inc = temp_tax1 + total_tax_due;




                //MessageBox.Show(emp_id + " " +total_annual_tax_inc);


                string myconnection4 = ConfigurationManager.ConnectionStrings["MyConstring"].ConnectionString;
                MySqlConnection connection4 = new MySqlConnection(myconnection4);


                string insertQuery3 = "INSERT INTO tax_inc(emp_id, tax_inc)Values('" + emp_id + "', '" + total_annual_tax_inc + "')";


                connection4.Open();
                MySqlCommand mySqlCommand3 = new MySqlCommand(insertQuery3, connection4);



                if (mySqlCommand3.ExecuteNonQuery() == 1)
                {
                    //MessageBox.Show("Successfully Process", "Payroll Processing", MessageBoxButtons.OK, MessageBoxIcon.Information);

                }
                connection4.Close();


            }
            connection3.Close();



        }
        public void update_wtax()
        {
            string myconnection3 = ConfigurationManager.ConnectionStrings["MyConstring"].ConnectionString;
            MySqlConnection connection3 = new MySqlConnection(myconnection3);



            cmd = new MySqlCommand();
            cmd.Connection = connection3;
            cmd.CommandText = "SELECT tax_inc.emp_id as emp_id, tax_inc.tax_inc - rec_tax.wtax_annual_temp as total FROM tax_inc LEFT join rec_tax on tax_inc.emp_id = rec_tax.emp_id GROUP BY tax_inc.emp_id";
            da = new MySqlDataAdapter();
            da.SelectCommand = cmd;
            dt = new DataTable();
            da.Fill(dt);


            for (int x = 0; x < dt.Rows.Count; x++)
            {
                DataRow dr2 = dt.Rows[x];



                string emp_id = (dr2["emp_id"].ToString());
                string total_wtax = (dr2["total"].ToString());

                //MessageBox.Show(emp_id + " " +total_annual_tax_inc);


                string myconnection4 = ConfigurationManager.ConnectionStrings["MyConstring"].ConnectionString;
                MySqlConnection connection4 = new MySqlConnection(myconnection4);


                string insertQuery3 = "INSERT INTO update_wtax_year_end(emp_id, total_wtax)Values('" + emp_id + "', '" + total_wtax + "')";


                connection4.Open();
                MySqlCommand mySqlCommand3 = new MySqlCommand(insertQuery3, connection4);



                if (mySqlCommand3.ExecuteNonQuery() == 1)
                {
                    //MessageBox.Show("Successfully Process", "Payroll Processing", MessageBoxButtons.OK, MessageBoxIcon.Information);

                }
                connection4.Close();


            }
            connection3.Close();
        }

      
        public void update_exact_tax()
        {
          
                string myconnection = ConfigurationManager.ConnectionStrings["MyConstring"].ConnectionString;
                MySqlConnection connection = new MySqlConnection(myconnection);

                try
                {
                    connection.Open();
                    cmd = new MySqlCommand();
                    cmd.Connection = connection;
                    cmd.CommandText = "UPDATE pay_details LEFT JOIN update_wtax_year_end ON pay_details.emp_id = update_wtax_year_end.emp_id SET pay_details.wtax = update_wtax_year_end.total_wtax WHERE pay_details.ctr = 24";
                    da = new MySqlDataAdapter();
                    da.SelectCommand = cmd;
                    dt = new DataTable();
                    da.Fill(dt);

                }
                catch (Exception ex)
                {
                    MessageBox.Show(ex.Message);
                }
                finally
                {
                    connection.Close();
                }
            }

        public void update_payslip_wtax()
        {
            string myconnection = ConfigurationManager.ConnectionStrings["MyConstring"].ConnectionString;
            MySqlConnection connection = new MySqlConnection(myconnection);

            try
            {
                connection.Open();
                cmd = new MySqlCommand();
                cmd.Connection = connection;
                cmd.CommandText = "UPDATE payslip left JOIN pay_details ON payslip.emp_id = pay_details.emp_id SET payslip.wtax =  pay_details.wtax WHERE pay_details.ctr = 24";
                da = new MySqlDataAdapter();
                da.SelectCommand = cmd;
                dt = new DataTable();
                da.Fill(dt);

            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
            finally
            {
                connection.Close();
            }
        }

         public void update_payslip_gross_ded()
        {
            //with netpay update
            string myconnection = ConfigurationManager.ConnectionStrings["MyConstring"].ConnectionString;
            MySqlConnection connection = new MySqlConnection(myconnection);

            try
            {
                connection.Open();
                cmd = new MySqlCommand();
                cmd.Connection = connection;
                cmd.CommandText = "UPDATE payslip SET gross_ded = sss_ee + philhealth_ee + pag_ibig_ee + comp_loan + sss_loan + pag_ibig_loan + other_ded + wtax, net_pay = gross_pay - gross_ded";
                da = new MySqlDataAdapter();
                da.SelectCommand = cmd;
                dt = new DataTable();
                da.Fill(dt);

            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
            finally
            {
                connection.Close();
            }
        }
        public void update_paydetails_gross_ded()
        {
            //with netpay update
            string myconnection = ConfigurationManager.ConnectionStrings["MyConstring"].ConnectionString;
            MySqlConnection connection = new MySqlConnection(myconnection);

            try
            {
                connection.Open();
                cmd = new MySqlCommand();
                cmd.Connection = connection;
                cmd.CommandText = "UPDATE pay_details SET gross_ded = sss_ee + philhealth_ee + pag_ibig_ee + company_loan + sss_loan + pag_ibig_loan + other_ded_det + wtax, net_pay = gross_inc - gross_ded WHERE ctr = 24";
                da = new MySqlDataAdapter();
                da.SelectCommand = cmd;
                dt = new DataTable();
                da.Fill(dt);

            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
            finally
            {
                connection.Close();
            }

        }
       

        private void btn_reverse_Click(object sender, EventArgs e)
        {
            DialogResult result = MessageBox.Show("Are you sure you want to reverse?", "Confirmation", MessageBoxButtons.YesNoCancel, MessageBoxIcon.Question);
            if (result == DialogResult.Yes)
            {
                delete_payslip();
                delete_monthly_tax();
                reverse();
                delete_pay_detail();
                delete_bank_remittance();
                delete_rec_tax();
                delete_tax_inc();
                delete_wtax_year_end();

                MessageBox.Show("Successfully Reverse", "Reverse Payroll", MessageBoxButtons.OK, MessageBoxIcon.Information);
            }
        }

        private void btn_payroll_processing_Click(object sender, EventArgs e)
        {
            string process_date = dateTimePicker2.Value.ToString("MMyyyy");

            check_payroll_process();

            if (process_date == mmyyyy && date_period == domainUpDown1.Text)
            {
                MessageBox.Show("This Month Already Process.", "Payroll Process", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
            else
            {
                DialogResult result = MessageBox.Show("Are you sure you want to process?", "Confirmation", MessageBoxButtons.YesNoCancel, MessageBoxIcon.Question);
                if (result == DialogResult.Yes)
                {


                    delete_payslip();
                    insert_pay_details();
                    processing_payroll_process();
                    find_max_ctr();
                    update_ctr();
                    correct_tax();
                    sum_ytd();

                    MessageBox.Show("Successfully Process", "Payroll Processing", MessageBoxButtons.OK, MessageBoxIcon.Information);

                }
            }
        }

        private void btn_print_paylip_Click(object sender, EventArgs e)
        {
            Print_Payslip payslip = new Print_Payslip();
            payslip.ShowDialog();
        }

        private void Payroll_Processing_Load(object sender, EventArgs e)
        {
            
                DateTime dt = DateTime.Now;
                string s = dt.ToString("M/d/yyyy");
                label6.Text = s;

                date_process();

            }
        }
    }
 
    





          




      

       


























