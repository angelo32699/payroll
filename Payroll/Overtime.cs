﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using MySql.Data.MySqlClient;
using System.Configuration;

namespace Payroll
{
    public partial class Overtime : Form
    {
        MySqlCommand cmd;
        MySqlDataAdapter da;
        DataTable dt;
        public Overtime()
        {
            InitializeComponent();
        }
        public void display_overtime(string sql1)
        {

            string myconnection = ConfigurationManager.ConnectionStrings["MyConstring"].ConnectionString;
            MySqlConnection connection = new MySqlConnection(myconnection);

            try
            {
                connection.Open();
                cmd = new MySqlCommand();
                cmd.Connection = connection;
                cmd.CommandText = sql1;
                da = new MySqlDataAdapter();
                da.SelectCommand = cmd;
                dt = new DataTable();
                da.Fill(dt);
                lv_Overtime.Items.Clear();



                for (int i = 0; i < dt.Rows.Count; i++)
                {
                    DataRow dr = dt.Rows[i];
                    ListViewItem listitem = new ListViewItem(dr["ot_id"].ToString());

                    listitem.SubItems.Add(dr["ot_code"].ToString());
                    listitem.SubItems.Add(dr["ot_desc"].ToString());
                    listitem.SubItems.Add(dr["ot1"].ToString());
                    listitem.SubItems.Add(dr["ot2"].ToString());
                    listitem.SubItems.Add(dr["ot3"].ToString());
                    listitem.SubItems.Add(dr["ot4"].ToString());


                    lv_Overtime.Items.Add(listitem);
                }

            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
            finally
            {
                connection.Close();
            }


            lv_Overtime.View = View.Details;
            lv_Overtime.Columns.Add(" ID ", 0, HorizontalAlignment.Center);
            lv_Overtime.Columns.Add(" OT Code ", 150, HorizontalAlignment.Center);
            lv_Overtime.Columns.Add(" OT Description ", 150, HorizontalAlignment.Center);
            lv_Overtime.Columns.Add(" OT1 ", 95, HorizontalAlignment.Center);
            lv_Overtime.Columns.Add(" OT2 ", 95, HorizontalAlignment.Center);
            lv_Overtime.Columns.Add(" OT3 ", 95, HorizontalAlignment.Center);
            lv_Overtime.Columns.Add(" OT4 ", 95, HorizontalAlignment.Center);

        }

        private void Overtime_Load(object sender, EventArgs e)
        {
            display_overtime("Select * from overtime");
        }


        private void tb_search_TextChanged(object sender, EventArgs e)
        {
            lv_Overtime.Columns.Clear();
            display_overtime("Select DISTINCT * FROM overtime WHERE ot_id like '" + tb_search.Text + "%' or ot_code like '" + tb_search.Text + "%' or ot_desc like '" + tb_search.Text + "%' or  ot1 like '" + tb_search.Text + "%'  or  ot2 like '" + tb_search.Text + "%' or ot3 like '" + tb_search.Text + "%' or ot4 like '" + tb_search.Text + "%'");
        }

       

        private void insert_overtime()
        {
            string myconnection = ConfigurationManager.ConnectionStrings["MyConstring"].ConnectionString;
            MySqlConnection connection = new MySqlConnection(myconnection);

            decimal ot1 = Convert.ToDecimal(tb_ot_1.Text);
            decimal ot2 = Convert.ToDecimal(tb_ot_2.Text);
            decimal ot3 = Convert.ToDecimal(tb_ot_3.Text);
            decimal ot4 = Convert.ToDecimal(tb_ot_4.Text);

            string insertQuery = "INSERT INTO overtime(ot_code, ot_desc, ot1, ot2, ot3, ot4) VALUES('" + tb_ot_code.Text + "', '" + tb_ot_desc.Text + "', '" + ot1 + "', '" + ot2 + "', '"+ot3+"', '"+ot4+"')";

            connection.Open();
            MySqlCommand mySqlCommand = new MySqlCommand(insertQuery, connection);


            try
            {
                if (mySqlCommand.ExecuteNonQuery() == 1)
                {
                    //MessageBox.Show("Successfully Add Information", "Add Information", MessageBoxButtons.OK, MessageBoxIcon.Information);
                }
                else
                {

                }
            }
            catch (Exception ex)
            {
                MessageBox.Show("Error" + ex);
            }

            connection.Close();
        }

        private void lv_Overtime_Click(object sender, EventArgs e)
        {
            tb_ot_code.Enabled = false;
            tb_ot_desc.Enabled = false;
            tb_ot_1.Enabled = false;
            tb_ot_2.Enabled = false;
            tb_ot_3.Enabled = false;
            tb_ot_4.Enabled = false;

            btn_add.Enabled = false;
            btn_update.Enabled = true;
            btn_delete.Enabled = true;
            btn_update.Text = "EDIT";
            btn_add.Text = "ADD";

            label7.Text = lv_Overtime.SelectedItems[0].SubItems[0].Text;
            tb_ot_code.Text = lv_Overtime.SelectedItems[0].SubItems[1].Text;
            tb_ot_desc.Text = lv_Overtime.SelectedItems[0].SubItems[2].Text;
            tb_ot_1.Text = lv_Overtime.SelectedItems[0].SubItems[3].Text;
            tb_ot_2.Text = lv_Overtime.SelectedItems[0].SubItems[4].Text;
            tb_ot_3.Text = lv_Overtime.SelectedItems[0].SubItems[5].Text;
            tb_ot_4.Text = lv_Overtime.SelectedItems[0].SubItems[6].Text;
        }

        private void update_ot()
        {
            string myconnection = ConfigurationManager.ConnectionStrings["MyConstring"].ConnectionString;
            MySqlConnection connection = new MySqlConnection(myconnection);



            decimal ot1 = Convert.ToDecimal(tb_ot_1.Text);
            decimal ot2 = Convert.ToDecimal(tb_ot_2.Text);
            decimal ot3 = Convert.ToDecimal(tb_ot_3.Text);
            decimal ot4 = Convert.ToDecimal(tb_ot_4.Text);


            try
            {
                connection.Open();
                cmd = new MySqlCommand();
                cmd.Connection = connection;
                cmd.CommandText = "UPDATE overtime SET ot_code  = '" + tb_ot_code.Text + "', ot_desc = '" + tb_ot_desc.Text + "' ,  ot1 = '" + ot1 + "', ot2 = '" + ot2 + "', ot3 = '" + ot3 + "', ot4 = '" + ot4 + "'   WHERE ot_id = '" + label7.Text + "'";
                da = new MySqlDataAdapter();
                da.SelectCommand = cmd;
                dt = new DataTable();
                da.Fill(dt);

            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
        }

        private void delete_overtime()
        {
            string myconnection = ConfigurationManager.ConnectionStrings["MyConstring"].ConnectionString;
            MySqlConnection connection = new MySqlConnection(myconnection);

            try
            {
                connection.Open();
                cmd = new MySqlCommand();
                cmd.Connection = connection;
                cmd.CommandText = "Delete From overtime where ot_id = '" + label7.Text + "'";
                da = new MySqlDataAdapter();
                da.SelectCommand = cmd;
                dt = new DataTable();
                da.Fill(dt);

            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
            finally
            {
                connection.Close();
            }
        }

        private void tb_ot_1_KeyPress(object sender, KeyPressEventArgs e)
        {
            if ((e.KeyChar < 32) || (e.KeyChar >= '0') && (e.KeyChar <= '9') || (e.KeyChar == ',') || (e.KeyChar == '.'))
                return;

            // only evident errors (like 'A' or '&') are restricted
            e.Handled = true;
        }

        private void tb_ot_2_KeyPress(object sender, KeyPressEventArgs e)
        {
            if ((e.KeyChar < 32) || (e.KeyChar >= '0') && (e.KeyChar <= '9') || (e.KeyChar == ',') || (e.KeyChar == '.'))
                return;

            // only evident errors (like 'A' or '&') are restricted
            e.Handled = true;
        }

        private void tb_ot_3_KeyPress(object sender, KeyPressEventArgs e)
        {
            if ((e.KeyChar < 32) || (e.KeyChar >= '0') && (e.KeyChar <= '9') || (e.KeyChar == ',') || (e.KeyChar == '.'))
                return;

            // only evident errors (like 'A' or '&') are restricted
            e.Handled = true;
        }

        private void tb_ot_4_KeyPress(object sender, KeyPressEventArgs e)
        {
            if ((e.KeyChar < 32) || (e.KeyChar >= '0') && (e.KeyChar <= '9') || (e.KeyChar == ',') || (e.KeyChar == '.'))
                return;

            // only evident errors (like 'A' or '&') are restricted
            e.Handled = true;
        }

        private void btn_add_Click(object sender, EventArgs e)
        {
            if (btn_add.Text == "ADD")
            {
                btn_add.Image = Properties.Resources.Button_SAVE_Red__1_;
                tb_ot_code.Enabled = true;
                tb_ot_desc.Enabled = true;
                tb_ot_1.Enabled = true;
                tb_ot_2.Enabled = true;
                tb_ot_3.Enabled = true;
                tb_ot_4.Enabled = true;
                btn_add.Text = "SAVE";
            }
            else if(label7.Text  != "0")
            {
                if (tb_ot_code.Text == "" || tb_ot_desc.Text == "")
                {
                    MessageBox.Show("Please Fill Up All Data Need", "Warning", MessageBoxButtons.OK, MessageBoxIcon.Error);
                }else
                {
                    DialogResult dialogResult = MessageBox.Show("Are you sure want to update SSS Table?", "Update SSS Table", MessageBoxButtons.YesNo, MessageBoxIcon.Question);
                    if (dialogResult == DialogResult.Yes)
                    {

                        update_ot();
                        lv_Overtime.Columns.Clear();
                        display_overtime("Select * from overtime");
                        MessageBox.Show("Successfully Update Overtime Table", "Update SSS Table", MessageBoxButtons.OK, MessageBoxIcon.Information);
                        disable_all();
                    }
                }            
            }
            else
            {
                if (tb_ot_code.Text == "" || tb_ot_desc.Text == "" || tb_ot_1.Text == "0.00")
                {
                    MessageBox.Show("Please Fill Up All Data Need", "Warning", MessageBoxButtons.OK, MessageBoxIcon.Error);
                }
                else
                {

                    insert_overtime();
                    lv_Overtime.Columns.Clear();
                    display_overtime("Select * from overtime");
                    MessageBox.Show("Successfully Added", "Add Overtime Information", MessageBoxButtons.OK, MessageBoxIcon.Information);
                    disable_all();
                }
            }
        }

        private void disable_all()
        {

            btn_add.Image = Properties.Resources.Button_ADD_Red__1___1_;

            label7.Text = "0";
            btn_add.Text = "ADD";
            btn_update.Text = "EDIT";

            tb_ot_code.Text = "";
            tb_ot_desc.Text = "";
            tb_ot_1.Text = "0.00";
            tb_ot_2.Text = "0.00";
            tb_ot_3.Text = "0.00";
            tb_ot_4.Text = "0.00";
            tb_ot_code.Enabled = false;
            tb_ot_desc.Enabled = false;
            tb_ot_1.Enabled = false;
            tb_ot_2.Enabled = false;
            tb_ot_3.Enabled = false;
            tb_ot_4.Enabled = false;

            btn_add.Enabled = true;
            btn_update.Enabled = false;
            btn_delete.Enabled = false;
        }

        private void btn_update_Click(object sender, EventArgs e)
        {
            if (btn_update.Text == "EDIT")
            {
                tb_ot_code.Enabled = true;
                tb_ot_desc.Enabled = true;
                tb_ot_1.Enabled = true;
                tb_ot_2.Enabled = true;
                tb_ot_3.Enabled = true;
                tb_ot_4.Enabled = true;

                btn_update.Enabled = false;
                btn_add.Enabled = true;
                btn_add.Text = "SAVE";
                btn_add.Image = Properties.Resources.Button_SAVE_Red__1_;


            }          
        }

        private void btn_Delete_Click(object sender, EventArgs e)
        {
            DialogResult dialogResult = MessageBox.Show("Are you sure want to delete Overtime Table?", "Delete Overtime Table", MessageBoxButtons.YesNo, MessageBoxIcon.Question);
            if (dialogResult == DialogResult.Yes)
            {

                delete_overtime();
                lv_Overtime.Columns.Clear();
                display_overtime("Select * from overtime");
                MessageBox.Show("Successfully Delete Overtime Table", "Delete SSS Table", MessageBoxButtons.OK, MessageBoxIcon.Information);
                disable_all();
            }
        }


        private void btn_add_MouseEnter(object sender, EventArgs e)
        {
            btn_add.BackgroundColor = ColorTranslator.FromHtml("#FFAFAF");
            btn_add.BackColor = ColorTranslator.FromHtml("#FFAFAF");
            btn_add.ForeColor = Color.White;
        }

        private void btn_add_MouseHover(object sender, EventArgs e)
        {
            btn_add.BackgroundColor = ColorTranslator.FromHtml("#FFAFAF");
            btn_add.BackColor = ColorTranslator.FromHtml("#FFAFAF");
            btn_add.ForeColor = Color.White;
        }

        private void btn_add_MouseLeave(object sender, EventArgs e)
        {
            btn_add.BackgroundColor = Color.Transparent;
            btn_add.BackColor = Color.Transparent;
            btn_add.ForeColor = ColorTranslator.FromHtml("#C00000");
        }

        private void btn_update_MouseEnter(object sender, EventArgs e)
        {
            btn_update.BackgroundColor = ColorTranslator.FromHtml("#FFAFAF");
            btn_update.BackColor = ColorTranslator.FromHtml("#FFAFAF");
            btn_update.ForeColor = Color.White;
        }

        private void btn_update_MouseHover(object sender, EventArgs e)
        {
            btn_update.BackgroundColor = ColorTranslator.FromHtml("#FFAFAF");
            btn_update.BackColor = ColorTranslator.FromHtml("#FFAFAF");
            btn_update.ForeColor = Color.White;
        }

        private void btn_update_MouseLeave(object sender, EventArgs e)
        {
            btn_update.BackgroundColor = Color.Transparent;
            btn_update.BackColor = Color.Transparent;
            btn_update.ForeColor = ColorTranslator.FromHtml("#C00000");
        }

        private void btn_delete_MouseEnter(object sender, EventArgs e)
        {
            btn_delete.BackgroundColor = ColorTranslator.FromHtml("#FFAFAF");
            btn_delete.BackColor = ColorTranslator.FromHtml("#FFAFAF");
            btn_delete.ForeColor = Color.White;
        }

        private void btn_delete_MouseHover(object sender, EventArgs e)
        {
            btn_delete.BackgroundColor = ColorTranslator.FromHtml("#FFAFAF");
            btn_delete.BackColor = ColorTranslator.FromHtml("#FFAFAF");
            btn_delete.ForeColor = Color.White;
        }

        private void btn_delete_MouseLeave(object sender, EventArgs e)
        {
            btn_delete.BackgroundColor = Color.Transparent;
            btn_delete.BackColor = Color.Transparent;
            btn_delete.ForeColor = ColorTranslator.FromHtml("#C00000");
        }

        private void ToExcel()
        {
            StringBuilder sb = new StringBuilder();
            Microsoft.Office.Interop.Excel.Application app = new Microsoft.Office.Interop.Excel.Application();
            app.Visible = true;
            Microsoft.Office.Interop.Excel.Workbook wb = app.Workbooks.Add(1);
            Microsoft.Office.Interop.Excel.Worksheet ws = (Microsoft.Office.Interop.Excel.Worksheet)wb.Worksheets[1];
            int i = 1;
            int i2 = 2;
            int x = 1;
            int x2 = 1;
            foreach (ColumnHeader ch in lv_Overtime.Columns)
            {
                ws.Cells[x2, x] = ch.Text;
                x++;
            }

            foreach (ListViewItem lvi in lv_Overtime.Items)
            {
                i = 1;
                foreach (ListViewItem.ListViewSubItem lvs in lvi.SubItems)
                {
                    ws.Cells[i2, i] = lvs.Text;
                    ws.Cells.Select();
                    ws.Cells.EntireColumn.AutoFit();
                    i++;
                }
                i2++;
            }
        }

        private void btn_export_Click(object sender, EventArgs e)
        {
            ToExcel();
        }
    }
}


