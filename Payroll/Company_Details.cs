﻿using MySql.Data.MySqlClient;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Configuration;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web.UI.WebControls;
using System.Windows.Forms;

namespace Payroll
{
    public partial class Company_Details : Form
    {
        MySqlCommand cmd;
        MySqlDataAdapter da;
        DataTable dt;
        string myconnection = ConfigurationManager.ConnectionStrings["MyConstring"].ConnectionString;
        MySqlConnection connection;



        public Company_Details()
        {
            InitializeComponent();
            connection = new MySqlConnection(myconnection);
            

        }

        private void Company_Details_Load(object sender, EventArgs e)
        {
            Display_Company_Details();        
            load_user_type();
            cb_UserType.SelectedIndex = 0;
            User_Setup_Details();


        }
        public void load_user_type()
        {
            string myconnection = ConfigurationManager.ConnectionStrings["MyConstring"].ConnectionString;
            MySqlConnection connection = new MySqlConnection(myconnection);

            try
            {
                connection.Open();
                cmd = new MySqlCommand();
                cmd.Connection = connection;
                cmd.CommandText = "Select * FROM user_type";
                da = new MySqlDataAdapter();
                da.SelectCommand = cmd;
                dt = new DataTable();
                da.Fill(dt);

                for (int i = 0; i < dt.Rows.Count; i++)
                {
                    DataRow dr = dt.Rows[i];

                    cb_UserType.Items.Add((dr["user_type"].ToString()));
                }

            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
            {

                connection.Close();
            }
        }


        public void Display_Company_Details()
        {
         
            try
            {
                connection.Open();
                cmd = new MySqlCommand();
                cmd.Connection = connection;
                cmd.CommandText = "Select * FROM comp_details";
                da = new MySqlDataAdapter();
                da.SelectCommand = cmd;
                dt = new DataTable();
                da.Fill(dt);

                for (int i = 0; i < dt.Rows.Count; i++)
                {
                    DataRow dr = dt.Rows[i];

                    tb_CompName.Text = (dr["comp_name"].ToString());
                    rtb_Address.Text = (dr["address"].ToString());
                    tb_TelNum.Text = (dr["tphone_num"].ToString());
                    tb_ConPerson.Text = (dr["con_person"].ToString());
                    tb_Weblink.Text = (dr["weblink"].ToString());
                    tb_Tin.Text = (dr["tin"].ToString());
                    tb_SSS.Text = (dr["sss"].ToString());
                    tb_Philhealth.Text = (dr["philhealth"].ToString());
                    tb_HDMF.Text = (dr["HDMF"].ToString());
                    tb_Maker.Text = (dr["maker"].ToString());
                    tb_Checker.Text = (dr["checker"].ToString());
                    tb_Approver.Text = (dr["approver"].ToString());
                    string rate = (dr["rate"].ToString());
                    string monthly = (dr["monthly"].ToString());
                    string semi_monthly = (dr["semi_monthly"].ToString());
                    string weekly = (dr["weekly"].ToString());


                    if (rate == "1")
                    {                     
                       rb_FactorDay1.Checked = true;
                    }

                    if (rate == "2")
                    {
                        rb_FactorDay2.Checked = true;
                    }

                    if (rate == "3")
                    {
                        rb_FactorDay3.Checked = true;
                    }

                    if (rate == "4")
                    {
                        rb_FactorDay4.Checked = true;
                    }

                    if(monthly == "True")
                    {
                        cb_Monthly.Checked = true;
                    }

                    if (semi_monthly == "True")
                    {
                        cb_SemiMonthly.Checked = true;
                    }

                    if (weekly == "True")
                    {
                        cb_Weekly.Checked = true;
                    }
                }

            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
            finally
            {
                connection.Close();
            }

        }

        public void User_Setup_Details()
        {
            int x = UpdateUser_account();
            try
            {
                connection.Open();
                cmd = new MySqlCommand();
                cmd.Connection = connection;
                cmd.CommandText = "Select * FROM user_account where user_id = '" + x + "'";
                da = new MySqlDataAdapter();
                da.SelectCommand = cmd;
                dt = new DataTable();
                da.Fill(dt);

                for (int i = 0; i < dt.Rows.Count; i++)
                {
                    DataRow dr = dt.Rows[i];

           
                        tb_name.Text = (dr["emp_name"].ToString());
                        tb_EmployeeID.Text = (dr["emp_id"].ToString());
                        tb_Username.Text = (dr["username"].ToString());
                        tb_Password.Text = (dr["password"].ToString());
                    }
                }

            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
            finally
            {
                connection.Close();
            }

        }

        private void btn_Update_Click(object sender, EventArgs e)
        {
           
        }

       

        public void update_comp_details()
            {
                 int rate = 0;
                 int monthly = 0;
                 int semi_monthly = 0;
                 int weekly = 0;

            if (rb_FactorDay1.Checked)
            {
                rate = 1;
            }


            if (rb_FactorDay2.Checked)
            {
                rate = 2;
            }

            if (rb_FactorDay3.Checked)
            {
                rate = 3;
            }

            if (rb_FactorDay4.Checked)
            {
                rate = 4;
            }


            if (cb_Monthly.Checked)
            {
                monthly = 1;
            }

            if (cb_SemiMonthly.Checked)
            {
                semi_monthly = 1;
            }

            if (cb_Weekly.Checked)
            {
                weekly = 1;
            }

            try
                {
                    connection.Open();
                    cmd = new MySqlCommand();
                    cmd.Connection = connection;
                    cmd.CommandText = "UPDATE comp_details SET comp_name = '" + tb_CompName.Text + "', address = '"+rtb_Address.Text+"', tphone_num = '"+tb_TelNum.Text+"', con_person = '"+tb_ConPerson.Text+"', weblink = '"+tb_Weblink.Text+"', tin = '"+tb_Tin.Text+"', sss = '"+tb_SSS.Text+"', philhealth = '"+tb_Philhealth.Text+"', HDMF = '"+tb_HDMF.Text+"', maker = '"+tb_Maker.Text+"', checker = '"+tb_Checker.Text+"', approver = '"+tb_Approver.Text+"', rate = '"+rate+"', monthly = '"+monthly+"', semi_monthly = '"+semi_monthly+"', weekly = '"+weekly+"'";
                    da = new MySqlDataAdapter();
                    da.SelectCommand = cmd;
                    dt = new DataTable();
                    da.Fill(dt);

                }
                catch (Exception ex)
                {
                    MessageBox.Show(ex.Message);
                }
                finally
                {
                    connection.Close();
                }
            }
        public int UpdateUser_account()
        {
            int x = 0;
            if (cb_UserType.SelectedIndex == 0)
            {
                x = 1;
            }
            else if (cb_UserType.SelectedIndex == 1)
            {
                x = 2;
            }
            else if (cb_UserType.SelectedIndex == 2)
            {
                x = 3;
            }

            return x;
        }

        public void update_user()
        {
            int x = UpdateUser_account();


            try
                {
                connection.Open();
                cmd = new MySqlCommand();
                cmd.Connection = connection;
                cmd.CommandText = "UPDATE user_account SET emp_id = '"+tb_EmployeeID.Text+"',  emp_name = '" + tb_name.Text + "', username = '" + tb_Username.Text + "', password = '" + tb_Password.Text + "' where user_id = '"+x+"'";
                da = new MySqlDataAdapter();
                da.SelectCommand = cmd;
                dt = new DataTable();
                da.Fill(dt);

            }
                catch (Exception ex)
                {
                MessageBox.Show(ex.Message);
            }
                finally
                {
                connection.Close();
            }
        }

        private void cb_UserType_SelectedIndexChanged(object sender, EventArgs e)
        {
            UpdateUser_account();
            User_Setup_Details();
        }

        private void tabPage1_Click(object sender, EventArgs e)
        {

        }

        private void btn_Edit_Click(object sender, EventArgs e)
        {
            if (btn_Edit.Text == "EDIT")
            {
                btn_Edit.Image = Properties.Resources.Button_SAVE_Red__1_;
                tb_CompName.Enabled = true;
                rtb_Address.Enabled = true;
                tb_TelNum.Enabled = true;
                tb_ConPerson.Enabled = true;
                tb_Weblink.Enabled = true;
                tb_Tin.Enabled = true;
                tb_SSS.Enabled = true;
                tb_Philhealth.Enabled = true;
                tb_HDMF.Enabled = true;
                tb_Maker.Enabled = true;
                tb_Checker.Enabled = true;
                tb_Approver.Enabled = true;
                rb_FactorDay1.Enabled = true;
                rb_FactorDay2.Enabled = true;
                rb_FactorDay3.Enabled = true;
                rb_FactorDay4.Enabled = true;
                cb_Monthly.Enabled = true;
                cb_Weekly.Enabled = true;
                cb_SemiMonthly.Enabled = true;             
                btn_Edit.Text = "SAVE";

            }
            else if (btn_Edit.Text == "SAVE")
            {
                if (tb_CompName.Text == "" || rtb_Address.Text == "" || tb_TelNum.Text == "" || tb_ConPerson.Text == "" || tb_Weblink.Text == "" || tb_Tin.Text == "" || tb_SSS.Text == "" || tb_Philhealth.Text == "" || tb_HDMF.Text == "" || tb_Maker.Text == "" || tb_Checker.Text == "" || tb_Approver.Text == "" || tb_name.Text == "" || tb_EmployeeID.Text == "" || tb_Username.Text == "" || tb_Password.Text == "")
                {
                    MessageBox.Show("Please Fill Up All Information Need", "Warning", MessageBoxButtons.OK, MessageBoxIcon.Error);

                }
                else if (cb_Weekly.Checked == false && cb_SemiMonthly.Checked == false && cb_Monthly.Checked == false)
                {
                    MessageBox.Show("Please Fill Up All Information Need", "Warning", MessageBoxButtons.OK, MessageBoxIcon.Error);
                }
                else
                {
                    DialogResult dialogResult = MessageBox.Show("Are you sure want to update Company Details/User Setup?", "Company Details/User Setup", MessageBoxButtons.YesNo, MessageBoxIcon.Question);
                    if (dialogResult == DialogResult.Yes)
                    {
                        update_comp_details();             
                        MessageBox.Show("Successfully Update Company Details and User Details", "Company Details and User Details", MessageBoxButtons.OK, MessageBoxIcon.Information);

                        btn_Edit.Image = Properties.Resources.Button_UPDATE_Red__4___1___1_;
                        tb_CompName.Enabled = false;
                        rtb_Address.Enabled = false;
                        tb_TelNum.Enabled = false;
                        tb_ConPerson.Enabled = false;
                        tb_Weblink.Enabled = false;
                        tb_Tin.Enabled = false;
                        tb_SSS.Enabled = false;
                        tb_Philhealth.Enabled = false;
                        tb_HDMF.Enabled = false;
                        tb_Maker.Enabled = false;
                        tb_Checker.Enabled = false;
                        tb_Approver.Enabled = false;
                        rb_FactorDay1.Enabled = false;
                        rb_FactorDay2.Enabled = false;
                        rb_FactorDay3.Enabled = false;
                        rb_FactorDay4.Enabled = false;
                        cb_Monthly.Enabled = false;
                        cb_Weekly.Enabled = false;
                        cb_SemiMonthly.Enabled = false;
                        btn_Edit.Text = "EDIT";
                    }
                }
            }
        }

        private void btn_Edit2_Click(object sender, EventArgs e)
        {
            if (btn_Edit2.Text == "EDIT")
            {            
                tb_name.Enabled = true;
                tb_EmployeeID.Enabled = true;
                tb_Username.Enabled = true;
                tb_Password.Enabled = true;
                cb_UserType.Enabled = true;
                btn_Edit2.Text = "SAVE";

            }
            else if (btn_Edit2.Text == "SAVE")
            {
                if (tb_name.Text == "" || tb_EmployeeID.Text == "" || tb_Username.Text == "" || tb_Password.Text == "")
                {
                    MessageBox.Show("Please Fill Up All Information Need", "Warning", MessageBoxButtons.OK, MessageBoxIcon.Error);

                }
                else if (cb_Weekly.Checked == false && cb_SemiMonthly.Checked == false && cb_Monthly.Checked == false)
                {
                    MessageBox.Show("Please Fill Up All Information Need", "Warning", MessageBoxButtons.OK, MessageBoxIcon.Error);
                }
                else
                {
                    DialogResult dialogResult = MessageBox.Show("Are you sure want to update User Setup?", "User Setup", MessageBoxButtons.YesNo, MessageBoxIcon.Question);
                    if (dialogResult == DialogResult.Yes)
                    {                      
                        update_user();
                        MessageBox.Show("Successfully Update User Details", "User Details", MessageBoxButtons.OK, MessageBoxIcon.Information);

                        btn_Edit2.Image = Properties.Resources.Button_UPDATE_Red__4___1___1_;
                        tb_name.Enabled = false;
                        tb_EmployeeID.Enabled = false;
                        tb_Username.Enabled = false;
                        tb_Password.Enabled = false;
                        cb_UserType.Enabled = false;
                        btn_Edit2.Text = "EDIT";
                    }
                }
            }
        }

        private void btn_Edit_MouseEnter(object sender, EventArgs e)
        {
            btn_Edit.BackgroundColor = ColorTranslator.FromHtml("#FFAFAF");
            btn_Edit.BackColor = ColorTranslator.FromHtml("#FFAFAF");
            btn_Edit.ForeColor = Color.White;
        }

        private void btn_Edit_MouseHover(object sender, EventArgs e)
        {
            btn_Edit.BackgroundColor = ColorTranslator.FromHtml("#FFAFAF");
            btn_Edit.BackColor = ColorTranslator.FromHtml("#FFAFAF");
            btn_Edit.ForeColor = Color.White;
        }

        private void btn_Edit_MouseLeave(object sender, EventArgs e)
        {
            btn_Edit.BackgroundColor = Color.Transparent;
            btn_Edit.BackColor = Color.Transparent;
            btn_Edit.ForeColor = ColorTranslator.FromHtml("#C00000");
        }

        private void btn_Edit2_MouseEnter(object sender, EventArgs e)
        {
            btn_Edit2.BackgroundColor = ColorTranslator.FromHtml("#FFAFAF");
            btn_Edit2.BackColor = ColorTranslator.FromHtml("#FFAFAF");
            btn_Edit2.ForeColor = Color.White;
        }

        private void btn_Edit2_MouseHover(object sender, EventArgs e)
        {
            btn_Edit2.BackgroundColor = ColorTranslator.FromHtml("#FFAFAF");
            btn_Edit2.BackColor = ColorTranslator.FromHtml("#FFAFAF");
            btn_Edit2.ForeColor = Color.White;
        }

        private void btn_Edit2_MouseLeave(object sender, EventArgs e)
        {
            btn_Edit2.BackgroundColor = Color.Transparent;
            btn_Edit2.BackColor = Color.Transparent;
            btn_Edit2.ForeColor = ColorTranslator.FromHtml("#C00000");
        }
    }
    }

