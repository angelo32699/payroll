﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using MySql.Data.MySqlClient;
using System.Configuration;

namespace Payroll
{
    public partial class Department : Form
    {
        MySqlCommand cmd;
        MySqlDataAdapter da;
        DataTable dt;

        string myconnection = ConfigurationManager.ConnectionStrings["MyConstring"].ConnectionString;
        MySqlConnection connection;
        public Department()
        {
            InitializeComponent();
            connection = new MySqlConnection(myconnection);
        }

        private void Department_Load(object sender, EventArgs e)
        {
            display_department("Select * from department");
        }
        public void display_department(string sql1)
        {
            try
            {
                connection.Open();
                cmd = new MySqlCommand();
                cmd.Connection = connection;
                cmd.CommandText = sql1;
                da = new MySqlDataAdapter();
                da.SelectCommand = cmd;
                dt = new DataTable();
                da.Fill(dt);
                lv_department.Items.Clear();

                for (int i = 0; i < dt.Rows.Count; i++)
                {
                    DataRow dr = dt.Rows[i];
                    ListViewItem listitem = new ListViewItem(dr["dep_id"].ToString());
                    listitem.SubItems.Add(dr["dep_code"].ToString());
                    listitem.SubItems.Add(dr["dep_desc"].ToString());
                    lv_department.Items.Add(listitem);
                }

            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
            finally
            {
                connection.Close();
            }

            lv_department.View = View.Details;
            lv_department.Columns.Add("  Id ", 0, HorizontalAlignment.Center);
            lv_department.Columns.Add("  Code ", 245, HorizontalAlignment.Center);
            lv_department.Columns.Add(" Description ", 250, HorizontalAlignment.Center);
        }

        public void insertDepartment()
        {



            string insertQuery = "INSERT INTO department(dep_CODE, dep_desc) VALUES('" + tb_code.Text + "', '" + tb_description.Text + "')";

            connection.Open();
            MySqlCommand mySqlCommand = new MySqlCommand(insertQuery, connection);


            try
            {
                if (mySqlCommand.ExecuteNonQuery() == 1)
                {
                    //MessageBox.Show("Successfully Add Information", "Add Information", MessageBoxButtons.OK, MessageBoxIcon.Information);
                }

            }
            catch (Exception ex)
            {
                MessageBox.Show("Error" + ex);
            }

            connection.Close();

        }

        public void delete_department()
        {


            try
            {
                connection.Open();
                cmd = new MySqlCommand();
                cmd.Connection = connection;
                cmd.CommandText = "Delete From department where dep_id = '" + lbl_id.Text + "'";
                da = new MySqlDataAdapter();
                da.SelectCommand = cmd;
                dt = new DataTable();
                da.Fill(dt);

            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
            finally
            {
                connection.Close();
            }
        }

        private void update_department()
        {


            try
            {
                connection.Open();
                cmd = new MySqlCommand();
                cmd.Connection = connection;
                cmd.CommandText = "UPDATE department SET dep_code  = '" + tb_code.Text + "', dep_desc = '" + tb_description.Text + "'  WHERE dep_id = '" + lbl_id.Text + "'";
                da = new MySqlDataAdapter();
                da.SelectCommand = cmd;
                dt = new DataTable();
                da.Fill(dt);

            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
            finally
            {
                connection.Close();
            }
        }

        private void lv_department_Click(object sender, EventArgs e)
        {
            tb_code.Enabled = false;
            tb_description.Enabled = false;

            btn_add.Enabled = false;
            btn_update.Enabled = true;
            btn_delete.Enabled = true;

            btn_update.Text = "EDIT";
            btn_add.Text = "ADD";

            lbl_id.Text = lv_department.SelectedItems[0].SubItems[0].Text;
            tb_code.Text = lv_department.SelectedItems[0].SubItems[1].Text;
            tb_description.Text = lv_department.SelectedItems[0].SubItems[2].Text;
        }


        private void btn_add_Click(object sender, EventArgs e)
        {
            if (btn_add.Text == "ADD")
            {
                btn_add.Image = Properties.Resources.Button_SAVE_Red__1_;
                tb_code.Enabled = true;
                tb_description.Enabled = true;
                btn_add.Text = "SAVE";
            }
            else if(lbl_id.Text != "0")
            {
                if (tb_description.Text == "" || tb_code.Text == "")
                {
                    MessageBox.Show("Please Fill Up All Data Need", "Warning", MessageBoxButtons.OK, MessageBoxIcon.Error);
                }else
                {
                    DialogResult dialogResult = MessageBox.Show("Are you sure want to update Department", "Update Department", MessageBoxButtons.YesNo, MessageBoxIcon.Question);
                    if (dialogResult == DialogResult.Yes)
                    {

                        update_department();
                        lv_department.Columns.Clear();
                        display_department("Select * from department");
                        MessageBox.Show("Successfully Update Department", "Update Department Table", MessageBoxButtons.OK, MessageBoxIcon.Information);
                        disable_all();
                    }
                }
            }
            else
            {
                if (tb_description.Text == "" || tb_code.Text == "")
                {
                    MessageBox.Show("Please Fill Up All Data Need", "Warning", MessageBoxButtons.OK, MessageBoxIcon.Error);
                }
                else
                {
                    DialogResult dialogResult = MessageBox.Show("Are you sure want to Add New Deparment?", "Add Department Information", MessageBoxButtons.YesNo, MessageBoxIcon.Question);
                    if (dialogResult == DialogResult.Yes)
                    {
                        insertDepartment();
                        lv_department.Columns.Clear();
                        display_department("Select * from department");
                        MessageBox.Show("Successfully Added Department", "Add New Department", MessageBoxButtons.OK, MessageBoxIcon.Information);
                        disable_all();
                    }
                }
            }
        }

        private void btn_delete_Click(object sender, EventArgs e)
        {
            DialogResult dialogResult = MessageBox.Show("Are you sure want to delete Department", "Delete SSS Table", MessageBoxButtons.YesNo, MessageBoxIcon.Question);
            if (dialogResult == DialogResult.Yes)
            {

                delete_department();
                lv_department.Columns.Clear();
                display_department("Select * from department");
                MessageBox.Show("Successfully Delete SSS", "Delete SSS", MessageBoxButtons.OK, MessageBoxIcon.Information);
                disable_all();
            }
        }

        private void btn_update_Click(object sender, EventArgs e)
        {
            if (btn_update.Text == "EDIT")
            {
                tb_code.Enabled = true;
                tb_description.Enabled = true;
                btn_update.Enabled = false;
                btn_add.Enabled = true;
                btn_add.Text = "SAVE";
                btn_add.Image = Properties.Resources.Button_SAVE_Red__1_;
            }

           
        }

        private void disable_all()
        {
            btn_add.Image = Properties.Resources.Button_ADD_Red__1___1_;

            lbl_id.Text = "0";
            btn_add.Text = "ADD";
            btn_update.Text = "EDIT";

            tb_code.Text = "";
            tb_description.Text = "";

            tb_code.Enabled = false;
            tb_description.Enabled = false;

            btn_add.Enabled = true;
            btn_delete.Enabled = false;
            btn_update.Enabled = false;
        }

        private void btn_add_MouseEnter(object sender, EventArgs e)
        {
            btn_add.BackgroundColor = ColorTranslator.FromHtml("#FFAFAF");
            btn_add.BackColor = ColorTranslator.FromHtml("#FFAFAF");
            btn_add.ForeColor = Color.White;

        }

        private void btn_add_MouseHover(object sender, EventArgs e)
        {
            btn_add.BackgroundColor = ColorTranslator.FromHtml("#FFAFAF");
            btn_add.BackColor = ColorTranslator.FromHtml("#FFAFAF");
            btn_add.ForeColor = Color.White;
        }

        private void btn_add_MouseLeave(object sender, EventArgs e)
        {
            btn_add.BackgroundColor = Color.Transparent;
            btn_add.BackColor = Color.Transparent;
            btn_add.ForeColor = ColorTranslator.FromHtml("#C00000");
        }

        private void btn_update_MouseEnter(object sender, EventArgs e)
        {
            btn_update.BackgroundColor = ColorTranslator.FromHtml("#FFAFAF");
            btn_update.BackColor = ColorTranslator.FromHtml("#FFAFAF");
            btn_update.ForeColor = Color.White;
        }

        private void btn_update_MouseHover(object sender, EventArgs e)
        {

            btn_update.BackgroundColor = ColorTranslator.FromHtml("#FFAFAF");
            btn_update.BackColor = ColorTranslator.FromHtml("#FFAFAF");
            btn_update.ForeColor = Color.White;

        }

        private void btn_update_MouseLeave(object sender, EventArgs e)
        {
            btn_update.BackgroundColor = Color.Transparent;
            btn_update.BackColor = Color.Transparent;
            btn_update.ForeColor = ColorTranslator.FromHtml("#C00000");


        }

        private void btn_delete_MouseEnter(object sender, EventArgs e)
        {
            btn_delete.BackgroundColor = ColorTranslator.FromHtml("#FFAFAF");
            btn_delete.BackColor = ColorTranslator.FromHtml("#FFAFAF");
            btn_delete.ForeColor = Color.White;


        }

        private void btn_delete_MouseHover(object sender, EventArgs e)
        {

            btn_delete.BackgroundColor = ColorTranslator.FromHtml("#FFAFAF");
            btn_delete.BackColor = ColorTranslator.FromHtml("#FFAFAF");
            btn_delete.ForeColor = Color.White;
        }

        private void btn_delete_MouseLeave(object sender, EventArgs e)
        {
            btn_delete.BackgroundColor = Color.Transparent;
            btn_delete.BackColor = Color.Transparent;
            btn_delete.ForeColor = ColorTranslator.FromHtml("#C00000");

        }

        private void btn_export_MouseEnter(object sender, EventArgs e)
        {
            btn_export.BackgroundColor = ColorTranslator.FromHtml("#FFAFAF");
            btn_export.BackColor = ColorTranslator.FromHtml("#FFAFAF");
            btn_export.ForeColor = Color.White;
        }

        private void btn_export_MouseHover(object sender, EventArgs e)
        {
            btn_export.BackgroundColor = ColorTranslator.FromHtml("#FFAFAF");
            btn_export.BackColor = ColorTranslator.FromHtml("#FFAFAF");
            btn_export.ForeColor = Color.White;
        }

        private void btn_export_MouseLeave(object sender, EventArgs e)
        {
            btn_export.BackgroundColor = Color.Transparent;
            btn_export.BackColor = Color.Transparent;
            btn_export.ForeColor = ColorTranslator.FromHtml("#C00000");
        }

        private void ToExcel()
        {
            StringBuilder sb = new StringBuilder();
            Microsoft.Office.Interop.Excel.Application app = new Microsoft.Office.Interop.Excel.Application();
            app.Visible = true;
            Microsoft.Office.Interop.Excel.Workbook wb = app.Workbooks.Add(1);
            Microsoft.Office.Interop.Excel.Worksheet ws = (Microsoft.Office.Interop.Excel.Worksheet)wb.Worksheets[1];
            int i = 1;
            int i2 = 2;
            int x = 1;
            int x2 = 1;
            foreach (ColumnHeader ch in lv_department.Columns)
            {
                ws.Cells[x2, x] = ch.Text;
                x++;
            }

            foreach (ListViewItem lvi in lv_department.Items)
            {
                i = 1;
                foreach (ListViewItem.ListViewSubItem lvs in lvi.SubItems)
                {
                    ws.Cells[i2, i] = lvs.Text;
                    ws.Cells.Select();
                    ws.Cells.EntireColumn.AutoFit();
                    i++;
                }
                i2++;
            }
        }

        private void btn_export_Click(object sender, EventArgs e)
        {
            ToExcel();
        }
    }
}

