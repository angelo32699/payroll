﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using CrystalDecisions.CrystalReports.Engine;
using CrystalDecisions.Windows.Forms;

namespace Payroll
{
    public partial class Bonus_Report : Form
    {

        ReportDocument report = new ReportDocument();
        connect con = new connect();


        public Bonus_Report()
        {
            InitializeComponent();
        }

        private void Bonus_Report_Load(object sender, EventArgs e)
        {
            crystalReportViewer1.ToolPanelView = ToolPanelViewType.None;

            report.Load(@"C:\Temp\Payroll\Payroll\Reports\Bonus.rpt");
            DataSet ds = new DataSet();
            con.dataget("select * from Bonus");
            con.mda.Fill(ds, "bonus");
            report.SetDataSource(ds);
            crystalReportViewer1.ReportSource = report;
        }
    }
}
