﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using CrystalDecisions.CrystalReports.Engine;
using CrystalDecisions.Windows.Forms;

namespace Payroll
{
    public partial class Bank_Remittance : Form
    {

        ReportDocument report = new ReportDocument();
        connect con = new connect();

        public Bank_Remittance()
        {
            InitializeComponent();
        }

        private void Bank_Remittance_Load(object sender, EventArgs e)
        {
            crystalReportViewer1.ToolPanelView = ToolPanelViewType.None;

            report.Load(@"C:\Temp\Payroll\Payroll\Reports\Bank_Rtance.rpt");
            DataSet ds = new DataSet();
            con.dataget("select * from bank_remittance");
            con.mda.Fill(ds, "bank_remittance");
            report.SetDataSource(ds);
            crystalReportViewer1.ReportSource = report;

        }
    }
}
